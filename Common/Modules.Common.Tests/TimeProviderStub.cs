using System;
using Qdarc.Modules.Common.QueryGeneration;

namespace Modules.Common.Tests
{
    class TimeProviderStub : ITimeProvider
    {
        private readonly DateTime _time;

        public TimeProviderStub(DateTime time)
        {
            _time = time;
        }

        public DateTime Now => _time;
    }
}