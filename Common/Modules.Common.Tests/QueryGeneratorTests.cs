using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;

namespace Modules.Common.Tests
{
    [TestClass]
    public class QueryGeneratorTests
    {
        private TableQueryProvider _tableQueryProvider;
        private TimeProviderStub _timeProvider;
        private DateTime _timeValue;

        [TestMethod]
        public void DeleteWhereSimpleComparision()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.Id == 5);

            var text = _tableQueryProvider.GetDeleteQueryText(query.Expression);
            Assert.AreEqual("DELETE"
                + Environment.NewLine + "FROM [UserEntity]"
                + Environment.NewLine + "WHERE ([Id] = @param1) @param1:5", text);
        }

        [TestMethod]
        public void BinaryAnd()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => (x.Context & Context.Value1) == Context.Value1);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE (([UE1].[Context] & @param1) = @param2) @param2:0, @param1:0", text);
        }

        [TestMethod]
        public void UnionAll()
        {
            ITableQuery<UserEntity> firstQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> secondQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = firstQuery.Select(x => x.Id)
                    .UnionAll(secondQuery.Select(x => x.Id));

            var text = query.ToString();
            Assert.AreEqual("(SELECT [UE1].[Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1])"
                + Environment.NewLine + "UNION ALL"
                + Environment.NewLine + "(SELECT [CE1].[Id]"
                + Environment.NewLine + "FROM [ContactEntity] AS [CE1])",
                text);
        }

        [TestMethod]
        public void UnionAllWithWhere()
        {
            ITableQuery<UserEntity> firstQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> secondQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = firstQuery
                    .Where(x => x.Id > 1)
                    .Select(x => x.Id)
                    .UnionAll(secondQuery.Select(x => x.Id));

            var text = query.ToString();
            Assert.AreEqual("(SELECT [UE1].[Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE ([UE1].[Id] > @param1))"
                + Environment.NewLine + "UNION ALL"
                + Environment.NewLine + "(SELECT [CE1].[Id]"
                + Environment.NewLine + "FROM [ContactEntity] AS [CE1]) @param1:1",
                text);
        }

        [TestMethod]
        public void Union()
        {
            ITableQuery<UserEntity> firstQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> secondQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = firstQuery.Select(x => x.Id)
                    .Union(secondQuery.Select(x => x.Id));

            var text = query.ToString();
            Assert.AreEqual("(SELECT [UE1].[Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1])"
                + Environment.NewLine + "UNION"
                + Environment.NewLine + "(SELECT [CE1].[Id]"
                + Environment.NewLine + "FROM [ContactEntity] AS [CE1])",
                text);
        }

        [TestMethod]
        public void UnionWithWhere()
        {
            ITableQuery<UserEntity> firstQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> secondQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = firstQuery
                    .Where(x => x.Id > 1)
                    .Select(x => x.Id)
                    .Union(secondQuery.Select(x => x.Id));

            var text = query.ToString();
            Assert.AreEqual("(SELECT [UE1].[Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE ([UE1].[Id] > @param1))"
                + Environment.NewLine + "UNION"
                + Environment.NewLine + "(SELECT [CE1].[Id]"
                + Environment.NewLine + "FROM [ContactEntity] AS [CE1]) @param1:1",
                text);
        }

        [TestMethod]
        public void Intersect()
        {
            ITableQuery<UserEntity> firstQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> secondQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = firstQuery.Select(x => x.Id)
                    .Intersect(secondQuery.Select(x => x.Id));

            var text = query.ToString();
            Assert.AreEqual("(SELECT [UE1].[Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1])"
                + Environment.NewLine + "INTERSECT"
                + Environment.NewLine + "(SELECT [CE1].[Id]"
                + Environment.NewLine + "FROM [ContactEntity] AS [CE1])",
                text);
        }

        [TestMethod]
        public void IntersectWithWhere()
        {
            ITableQuery<UserEntity> firstQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> secondQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = firstQuery
                    .Where(x => x.Id > 1)
                    .Select(x => x.Id)
                    .Intersect(secondQuery.Select(x => x.Id));

            var text = query.ToString();
            Assert.AreEqual("(SELECT [UE1].[Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE ([UE1].[Id] > @param1))"
                + Environment.NewLine + "INTERSECT"
                + Environment.NewLine + "(SELECT [CE1].[Id]"
                + Environment.NewLine + "FROM [ContactEntity] AS [CE1]) @param1:1",
                text);
        }
        [TestMethod]
        public void Except()
        {
            ITableQuery<UserEntity> firstQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> secondQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = firstQuery.Select(x => x.Id)
                    .Except(secondQuery.Select(x => x.Id));

            var text = query.ToString();
            Assert.AreEqual("(SELECT [UE1].[Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1])"
                + Environment.NewLine + "EXCEPT"
                + Environment.NewLine + "(SELECT [CE1].[Id]"
                + Environment.NewLine + "FROM [ContactEntity] AS [CE1])",
                text);
        }

        [TestMethod]
        public void ExceptWithWhere()
        {
            ITableQuery<UserEntity> firstQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> secondQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = firstQuery
                    .Where(x => x.Id > 1)
                    .Select(x => x.Id)
                    .Except(secondQuery.Select(x => x.Id));

            var text = query.ToString();
            Assert.AreEqual("(SELECT [UE1].[Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE ([UE1].[Id] > @param1))"
                + Environment.NewLine + "EXCEPT"
                + Environment.NewLine + "(SELECT [CE1].[Id]"
                + Environment.NewLine + "FROM [ContactEntity] AS [CE1]) @param1:1",
                text);
        }
        [TestMethod]
        public void Coalesce()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new { Id = x.ParentId ?? x.Id });

            var text = query.ToString();
            Assert.AreEqual("SELECT Coalesce([UE1].[ParentId], [UE1].[Id]) AS [Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void CoalesceWithMarker()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new { Id = x.Parent ?? x });

            var text = query.ToString();
            Assert.AreEqual("SELECT Coalesce([UE1].[Parent], [UE1].[Id]) AS [Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void OrderByGeneratedColumn()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new { Id = 0 }).OrderBy(x => x.Id);

            var text = query.ToString();
            Assert.AreEqual("SELECT @param1 AS [Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "ORDER BY [Id] @param1:0", text);
        }

        [TestMethod]
        public void FilterAfterProjection()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new { Name = x.LastName }).Where(x => x.Name == "a");

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].[LastName] AS [Name]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE ([UE1].[LastName] = @param1) @param1:a", text);
        }

        [TestMethod]
        public void TextConcatenation()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => x.FirstName + " " + x.LastName);

            var text = query.ToString();
            Assert.AreEqual("SELECT (([UE1].[FirstName] + @param1) + [UE1].[LastName])"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1] @param1: ", text);
        }

        [TestMethod]
        public void TextConcatenationInProjection()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new { FullName = x.FirstName + " " + x.LastName, Id = x.Id });

            var text = query.ToString();
            Assert.AreEqual("SELECT (([UE1].[FirstName] + @param1) + [UE1].[LastName]) AS [FullName], [UE1].[Id] AS [Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1] @param1: ", text);
        }

        [TestMethod]
        public void ProjectionWithAttachedColumn()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new { Attached = true, Id = x.Id });

            var text = query.ToString();
            Assert.AreEqual("SELECT @param1 AS [Attached], [UE1].[Id] AS [Id]"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1] @param1:1", text);
        }

        [TestMethod]
        public void CombineMultipleWhereStatements()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => null != x.FirstName)
                .Where(x => !x.IsActive);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE (([UE1].[FirstName] IS NOT NULL) AND ([UE1].[IsActive] = 0))", text);
        }

        [TestMethod]
        public void CombineMultipleWhereStatements_Querable()
        {
            IQueryable<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => null != x.FirstName)
                .Where(x => !x.IsActive);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE (([UE1].[FirstName] IS NOT NULL) AND ([UE1].[IsActive] = 0))", text);
        }

        [TestMethod]
        public void ComplexProjection()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new
            {
                FirstName = x.FirstName,
                LastName = x.LastName,
            });

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].[FirstName] AS [FirstName], [UE1].[LastName] AS [LastName]"
                            + Environment.NewLine
                            + "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void ContainsOnSubQuery()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> baseSubQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var subQuery = baseSubQuery.Where(x => x.VersionEndDate == null)
                   .Join(new TableQuery<UserContactRelation>(_tableQueryProvider), x => x.Id, x => x.Contact, (x, y) => y.User);
            var query = baseQuery.Where(x => subQuery.Contains(x.Id));
            var text = query.ToString();

            Assert.AreEqual("SELECT [UE1].*" + Environment.NewLine +
                            "FROM [UserEntity] AS [UE1]" + Environment.NewLine +
                            "WHERE [UE1].[Id] IN (SELECT [UCR1].[User]" + Environment.NewLine +
                                                    "FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
                                                    "JOIN [UserContactRelation] AS [UCR1] ON ([CE1].[Id] = [UCR1].[Contact])" + Environment.NewLine +
                                                    "WHERE ([CE1].[VersionEndDate] IS NULL))", text);
        }

        [TestMethod]
        public void Count()
        {
            ITableQuery<UserEntity> query = new TableQuery<UserEntity>(_tableQueryProvider);
            var countMethod = ExpressionExtensions.GetMethod((ITableQuery<UserEntity> x) => x.Count());
            var text = ((TableQueryProvider)query.Provider).GetQueryText(Expression.Call(countMethod, query.Expression));

            Assert.AreEqual("SELECT Count(*) AS [RowCount]" + Environment.NewLine + "FROM [UserEntity] AS [UE1]",
                text);
        }

        [TestMethod]
        public void Any()
        {
            ITableQuery<UserEntity> query = new TableQuery<UserEntity>(_tableQueryProvider);
            var anyMethod = ExpressionExtensions.GetMethod((ITableQuery<UserEntity> x) => x.Any());
            var text = ((TableQueryProvider)query.Provider).GetQueryText(Expression.Call(anyMethod, query.Expression));

            Assert.AreEqual("SELECT CASE WHEN EXISTS (" + Environment.NewLine +
                "SELECT [UE1].*" + Environment.NewLine +
                "FROM [UserEntity] AS [UE1])" + Environment.NewLine +
                "THEN CAST( 1 As bit) ELSE CAST( 0 As bit) END",
                text);
        }

        [TestMethod]
        public void AnyWithSubQueryParameters()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.FirstName == "Jon");
            var anyMethod = ExpressionExtensions.GetMethod((ITableQuery<UserEntity> x) => x.Any());
            var text = ((TableQueryProvider)query.Provider).GetQueryText(Expression.Call(anyMethod, query.Expression));

            Assert.AreEqual("SELECT CASE WHEN EXISTS (" + Environment.NewLine +
                "SELECT [UE1].*" + Environment.NewLine +
                "FROM [UserEntity] AS [UE1]" + Environment.NewLine +
                "WHERE ([UE1].[FirstName] = @param1))" + Environment.NewLine +
                "THEN CAST( 1 As bit) ELSE CAST( 0 As bit) END @param1:Jon",
                text);
        }

        [TestMethod]
        public void DoubleJoin()
        {
            ITableQuery<UserContactRelation> baseQuery = new TableQuery<UserContactRelation>(_tableQueryProvider);
            var query = baseQuery.Join(
                 new TableQuery<ContactEntity>(_tableQueryProvider),
                 x => x.Contact,
                 x => x.Id,
                 (x, y) => new { Relation = x, Contact = y })
             .Join(
                 new TableQuery<UserEntity>(_tableQueryProvider),
                 x => x.Relation.User,
                 x => x.Id,
                 (x, y) => x.Contact);

            var text = query.ToString();
            Assert.AreEqual(
"SELECT [CE1].*" + Environment.NewLine +
"FROM [UserContactRelation] AS [UCR1]" + Environment.NewLine +
"JOIN [ContactEntity] AS [CE1] ON ([UCR1].[Contact] = [CE1].[Id])" + Environment.NewLine +
"JOIN [UserEntity] AS [UE1] ON ([UCR1].[User] = [UE1].[Id])", text);
        }

        [TestMethod]
        public void JoinWithSubqueryWithAttachedColumn()
        {
            ITableQuery<UserContactRelation> baseQuery = new TableQuery<UserContactRelation>(_tableQueryProvider);
            ITableQuery<UserEntity> subQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Join(
                  subQuery.Select(x => new { x.Id, Attached = true }),
                  x => x.User,
                  x => x.Id,
                  (x, y) => new { x.Contact, y.Attached });

            var text = query.ToString();
            Assert.AreEqual(
"SELECT [Join1].[Attached] AS [Attached], [UCR1].[Contact] AS [Contact]" + Environment.NewLine +
"FROM [UserContactRelation] AS [UCR1]" + Environment.NewLine +
"JOIN (SELECT @param1 AS [Attached], [UE1].[Id] AS [Id]" + Environment.NewLine +
"FROM [UserEntity] AS [UE1]) AS [Join1] ON ([UCR1].[User] = [Join1].[Id]) @param1:1", text);
        }

        [TestMethod]
        public void FilterByDate()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x =>
                     _timeValue >= x.VersionStartDate && x.VersionEndDate >= _timeValue);

            var text = query.ToString();
            Assert.AreEqual("SELECT [CE1].*" + Environment.NewLine +
                            "FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
                            "WHERE (([CE1].[VersionStartDate] <= @param1) " +
                            "AND (IFNULL([CE1].[VersionEndDate], @CurrentDate) >= @param1)) @param1:"
                            + _timeValue.ToString(CultureInfo.InvariantCulture) + ", @CurrentDate:" + _timeValue.ToString(CultureInfo.InvariantCulture),
                            text);
        }

        [TestInitialize]
        public void Initialize()
        {
            _timeValue = DateTime.UtcNow;
            _timeProvider = new TimeProviderStub(_timeValue);
            _tableQueryProvider = new TableQueryProvider(null, () => new QueryTranslator(new TableNameResolver(), _timeProvider), null, null);
        }

        [TestMethod]
        public void Join()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Join(
                 new TableQuery<UserContactRelation>(_tableQueryProvider),
                 x => x.Id,
                 x => x.Contact,
                 (x, y) => x);
            var text = query.ToString();
            Assert.AreEqual(
"SELECT [CE1].*" + Environment.NewLine +
"FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
"JOIN [UserContactRelation] AS [UCR1] ON ([CE1].[Id] = [UCR1].[Contact])", text);
        }

        [TestMethod]
        public void JoinWithAlreadyJoinedSet()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Join(
                 new TableQuery<UserContactRelation>(_tableQueryProvider),
                 x => x.Id,
                 x => x.Contact,
                 (x, y) => x);

            ITableQuery<ContactEntity> outer = new TableQuery<ContactEntity>(_tableQueryProvider);
            var finalQuery = outer.Join(query, x => x.Id, x => x.ParentId, (x, y) => x);

            var text = finalQuery.ToString();

            Assert.AreEqual(
"SELECT [CE1].*" + Environment.NewLine +
"FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
"JOIN (SELECT [CE2].*" + Environment.NewLine +
"FROM [ContactEntity] AS [CE2]" + Environment.NewLine +
"JOIN [UserContactRelation] AS [UCR1] ON ([CE2].[Id] = [UCR1].[Contact])) AS [Join1] ON ([CE1].[Id] = [Join1].[ParentId])",
text);
        }

        [TestMethod]
        public void OrderAfterJoin()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Join(
                     new TableQuery<UserContactRelation>(_tableQueryProvider),
                     x => x.Id,
                     x => x.Contact,
                     (x, y) => x)
                 .OrderBy(x => x.Name);

            var text = query.ToString();
            Assert.AreEqual(
"SELECT [CE1].*" + Environment.NewLine +
"FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
"JOIN [UserContactRelation] AS [UCR1] ON ([CE1].[Id] = [UCR1].[Contact])" + Environment.NewLine +
 "ORDER BY [CE1].[Name]", text);
        }

        [TestMethod]
        public void OrderAfterJoinWithProjection()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Join(
                     new TableQuery<UserContactRelation>(_tableQueryProvider),
                     x => x.Id,
                     x => x.Contact,
                     (x, y) => new { ContactName = x.Name, x.Id })
                 .OrderBy(x => x.ContactName);

            var text = query.ToString();
            Assert.AreEqual(
"SELECT [CE1].[Name] AS [ContactName], [CE1].[Id] AS [Id]" + Environment.NewLine +
"FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
"JOIN [UserContactRelation] AS [UCR1] ON ([CE1].[Id] = [UCR1].[Contact])" + Environment.NewLine +
 "ORDER BY [ContactName]", text);
        }

        [TestMethod]
        public void JoinNull()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Join(
                new TableQuery<UserAccountRelation>(_tableQueryProvider),
                x => x.Account,
                x => x.Account,
                (x, y) => x);
            var text = query.ToString();
            Assert.AreEqual(
"SELECT [CE1].*" + Environment.NewLine +
"FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
"JOIN [UserAccountRelation] AS [UAR1] ON ([CE1].[Account] = [UAR1].[Account])", text);
        }

        [TestMethod]
        public void JoinWithSelectionList()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            ITableQuery<UserContactRelation> subQuery = new TableQuery<UserContactRelation>(_tableQueryProvider);
            var query = baseQuery.Join(
                     subQuery.Where(x => x.User == 5),
                   x => x.Id,
                   x => x.Contact,
                   (x, y) => new { ContactId = x.Id, ContactName = x.Name, RelationId = y.Id });
            var text = query.ToString();
            Assert.AreEqual(
"SELECT [CE1].[Id] AS [ContactId], [CE1].[Name] AS [ContactName], [UCR1].[Id] AS [RelationId]" + Environment.NewLine +
"FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
"JOIN [UserContactRelation] AS [UCR1] ON ([CE1].[Id] = [UCR1].[Contact]) AND ([UCR1].[User] = @param1) @param1:5", text);
        }

        [TestMethod]
        public void JoinWithSimpleSelection()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Join(
                new TableQuery<UserContactRelation>(_tableQueryProvider),
                x => x.Id,
                x => x.Contact,
                (x, y) => x.Name);

            var text = query.ToString();
            Assert.AreEqual(
                            "SELECT [CE1].[Name]" + Environment.NewLine +
                            "FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
                            "JOIN [UserContactRelation] AS [UCR1] " +
                            "ON ([CE1].[Id] = [UCR1].[Contact])", text);
        }

        [TestMethod]
        public void NegatedBooleanInWhereStatement()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => !x.IsActive);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE ([UE1].[IsActive] = 0)", text);
        }

        [TestMethod]
        public void NegatedComparisionInWhereStatement()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => !(x.IsActive && x.FirstName == "D"));

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "WHERE (NOT (([UE1].[IsActive] = 1) AND ([UE1].[FirstName] = @param1))) @param1:D", text);
        }

        [TestMethod]
        public void OrderBy()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.OrderBy(x => x.FirstName);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "ORDER BY [UE1].[FirstName]", text);
        }

        [TestMethod]
        public void OrderByDescending()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.OrderByDescending(x => x.FirstName);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "ORDER BY [UE1].[FirstName] DESC", text);
        }

        [TestMethod]
        public void OrderByDescendingThenByDescending()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.OrderByDescending(x => x.FirstName)
             .OrderByDescending(x => x.LastName);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "ORDER BY [UE1].[FirstName] DESC, [UE1].[LastName] DESC", text);
        }

        [TestMethod]
        public void OrderByThenBy()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.OrderBy(x => x.FirstName)
              .OrderBy(x => x.LastName);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                + Environment.NewLine + "ORDER BY [UE1].[FirstName], [UE1].[LastName]", text);
        }

        [TestMethod]
        public void ProjectionWithMembersInitalizer()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new ProjectionWithMembersInitalizerInstance
            {
                Name = x.FirstName,
                Id = x.Id
            });
            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].[Id] AS [Id], [UE1].[FirstName] AS [Name]" + Environment.NewLine + "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void ProjectionWithNestedMembersInitalizer()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => new
            {
                Name = x.FirstName,
                Parent = new
                {
                    Id = x.ParentId,
                    ChildId = x.Id,
                }
            });
            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].[FirstName] AS [Name], [UE1].[Id] AS [Parent.ChildId], [UE1].[ParentId] AS [Parent.Id]" + Environment.NewLine +
                "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void RecursiveWith()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> subQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.VersionEndDate == null && new[] { 4, 3 }.Contains(x.Id))
             .UnionAllRecursive(
                 subQuery
                     .Where(x => x.VersionEndDate == null),
                     z => z.Id, z => z.ParentId, z => z);
            var text = query.ToString();

            Assert.AreEqual("WITH [VIEW1R] AS (" + Environment.NewLine +
                            "SELECT [CE1].*" + Environment.NewLine +
                                    "FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
                                    "WHERE (([CE1].[VersionEndDate] IS NULL) " +
                                    "AND ([CE1].[Id] IN (@param2_0, @param2_1)))" + Environment.NewLine +
                            "UNION ALL" + Environment.NewLine +
                                    "SELECT [CE2].*" + Environment.NewLine +
                                    "FROM [VIEW1R] AS [VIEW1R1]" + Environment.NewLine +
                                    "JOIN [ContactEntity] AS [CE2] " +
                                    "ON ([VIEW1R1].[Id] = [CE2].[ParentId]) AND ([CE2].[VersionEndDate] IS NULL))" + Environment.NewLine + Environment.NewLine +
                            "SELECT [VIEW1R1].*" + Environment.NewLine +
                            "FROM [VIEW1R] AS [VIEW1R1] @param2_0:4, @param2_1:3", text);
        }

        [TestMethod]
        public void RecursiveWithInCount()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> subQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.VersionEndDate == null && new[] { 4, 3 }.Contains(x.Id))
            .UnionAllRecursive(
              subQuery.Where(x => x.VersionEndDate == null),
                    z => z.Id, z => z.ParentId, z => z);

            var count = ExpressionExtensions.GetMethod<ITableQuery<ContactEntity>>(x => x.Count());
            var expression = Expression.Call(count, query.Expression);
            var text = ((TableQueryProvider)query.Provider).GetQueryText(expression);

            Assert.AreEqual("WITH [VIEW1R] AS (" + Environment.NewLine +
                            "SELECT [CE1].*" + Environment.NewLine +
                                    "FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
                                    "WHERE (([CE1].[VersionEndDate] IS NULL) " +
                                    "AND ([CE1].[Id] IN (@param2_0, @param2_1)))" + Environment.NewLine +
                            "UNION ALL" + Environment.NewLine +
                                    "SELECT [CE2].*" + Environment.NewLine +
                                    "FROM [VIEW1R] AS [VIEW1R1]" + Environment.NewLine +
                                    "JOIN [ContactEntity] AS [CE2] " +
                                    "ON ([VIEW1R1].[Id] = [CE2].[ParentId]) AND ([CE2].[VersionEndDate] IS NULL))" + Environment.NewLine + Environment.NewLine +
                            "SELECT Count(*) AS [RowCount]" + Environment.NewLine +
                            "FROM [VIEW1R] AS [VIEW1R1] @param2_0:4, @param2_1:3", text);
        }

        [TestMethod]
        public void RecursiveWithInCountAndSelectionList()
        {
            ITableQuery<ContactEntity> baseQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> subQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.VersionEndDate == null && new[] { 4, 3 }.Contains(x.Id))
            .UnionAllRecursive(
               subQuery
                    .Where(x => x.VersionEndDate == null),
                    z => z.Id, z => z.ParentId, z => z)
            .Join(
            new TableQuery<UserContactRelation>(_tableQueryProvider),
            x => x.Id,
            x => x.Contact,
            (x, y) => x);

            var count = ExpressionExtensions.GetMethod<ITableQuery<ContactEntity>>(x => x.Count());
            var expression = Expression.Call(count, query.Expression);
            var text = ((TableQueryProvider)query.Provider).GetQueryText(expression);

            Assert.AreEqual("WITH [VIEW1R] AS (" + Environment.NewLine +
                            "SELECT [CE1].*" + Environment.NewLine +
                                    "FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
                                    "WHERE (([CE1].[VersionEndDate] IS NULL) " +
                                    "AND ([CE1].[Id] IN (@param2_0, @param2_1)))" + Environment.NewLine +
                            "UNION ALL" + Environment.NewLine +
                                    "SELECT [CE2].*" + Environment.NewLine +
                                    "FROM [VIEW1R] AS [VIEW1R1]" + Environment.NewLine +
                                    "JOIN [ContactEntity] AS [CE2] " +
                                    "ON ([VIEW1R1].[Id] = [CE2].[ParentId]) AND ([CE2].[VersionEndDate] IS NULL))" + Environment.NewLine + Environment.NewLine +
                            "SELECT Count(*) AS [RowCount]" + Environment.NewLine +
                            "FROM [VIEW1R] AS [VIEW1R1]" + Environment.NewLine +
                            "JOIN [UserContactRelation] AS [UCR1] ON ([VIEW1R1].[Id] = [UCR1].[Contact]) @param2_0:4, @param2_1:3", text);
        }

        [TestMethod]
        public void RecursiveWithInSubQuery()
        {
            ITableQuery<ContactEntity> baseSubQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            ITableQuery<ContactEntity> baseInnerSubQuery = new TableQuery<ContactEntity>(_tableQueryProvider);
            var subQuery = baseSubQuery.Where(x => x.VersionEndDate == null && new[] { 4, 3 }.Contains(x.Id))
              .UnionAllRecursive(
                  baseInnerSubQuery
                      .Where(x => x.VersionEndDate == null),
                      z => z.Id, z => z.ParentId, z => z);

            ITableQuery<UserContactRelation> baseQuery = new TableQuery<UserContactRelation>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.User == 5)
             .Join(subQuery, x => x.Contact, x => x.Id, (x, y) => y);

            var text = query.ToString();

            Assert.AreEqual("WITH [VIEW1R] AS (" + Environment.NewLine +
                            "SELECT [CE1].*" + Environment.NewLine +
                                    "FROM [ContactEntity] AS [CE1]" + Environment.NewLine +
                                    "WHERE (([CE1].[VersionEndDate] IS NULL) " +
                                    "AND ([CE1].[Id] IN (@param3_0, @param3_1)))" + Environment.NewLine +
                            "UNION ALL" + Environment.NewLine +
                                    "SELECT [CE2].*" + Environment.NewLine +
                                    "FROM [VIEW1R] AS [VIEW1R1]" + Environment.NewLine +
                                    "JOIN [ContactEntity] AS [CE2] " +
                                    "ON ([VIEW1R1].[Id] = [CE2].[ParentId]) AND ([CE2].[VersionEndDate] IS NULL))" + Environment.NewLine + Environment.NewLine +
                            "SELECT [VIEW1R1].*" + Environment.NewLine +
                            "FROM [UserContactRelation] AS [UCR1]" + Environment.NewLine +
                            "JOIN [VIEW1R] AS [VIEW1R1] ON ([UCR1].[Contact] = [VIEW1R1].[Id])" + Environment.NewLine +
                            "WHERE ([UCR1].[User] = @param1) @param3_0:4, @param3_1:3, @param1:5", text);
        }

        [TestMethod]
        public void SimpleSelect()
        {
            ITableQuery<UserEntity> query = new TableQuery<UserEntity>(_tableQueryProvider);
            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*" + Environment.NewLine + "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void SimpleGroupBy()
        {
            ITableQuery<UserEntity> query = new TableQuery<UserEntity>(_tableQueryProvider);
            var text = query.GroupBy(x => x.Id, set => set.Key).ToString();
            Assert.AreEqual("SELECT [UE1].[Id]" + Environment.NewLine +
                "FROM [UserEntity] AS [UE1]" + Environment.NewLine + 
                "GROUP BY [UE1].[Id]", text);
        }

        [TestMethod]
        public void SimpleSqlMathSelect()
        {
            ITableQuery<UserEntity> query = new TableQuery<UserEntity>(_tableQueryProvider);

            var text = query.Select(x => SqlMath.Min(x.WorkEnd)).ToString();
            Assert.AreEqual("SELECT MIN([UE1].[WorkEnd])" + Environment.NewLine + "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void Values()
        {
            ITableQuery<UserEntity> query = new ValuesQuery<UserEntity>(_tableQueryProvider,
                new[]
                {
                    new UserEntity
                    {
                        FirstName = "A",
                        LastName = "AA",
                        WorkEnd = DateTime.Today
                    }
                });
            var text = query.ToString();
            Assert.AreEqual("SELECT [V1].*"
                + Environment.NewLine + "FROM (VALUES (Cast(N'A' AS nvarchar(25)), Cast(N'AA' AS nvarchar(25)), 0, Cast(0 AS BIT), Cast(N'2015-11-16T00:00:00' AS DateTime), Cast(0 AS int), Cast(null AS int), Cast(null AS int))"
                + Environment.NewLine + ") AS V1([FirstName], [LastName], [Id], [IsActive], [WorkEnd], [Context], [ParentId], [Parent])", text);
        }

        [TestMethod]
        public void SelectWithCast()
        {
            ITableQuery<IWithName> query = new TableQuery<UserEntity>(_tableQueryProvider);
            var text = query
                .Select(x => new { x.FirstName, ((IActivable)x).IsActive })
                .ToString();
            Assert.AreEqual("SELECT [UE1].[FirstName] AS [FirstName], [UE1].[IsActive] AS [IsActive]" + Environment.NewLine + "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void SinglePropertyProjection()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Select(x => x.FirstName);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].[FirstName]" + Environment.NewLine + "FROM [UserEntity] AS [UE1]", text);
        }

        [TestMethod]
        public void WhereIsNotNullLeft()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => null != x.FirstName);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                            + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                            + Environment.NewLine + "WHERE ([UE1].[FirstName] IS NOT NULL)", text);
        }

        [TestMethod]
        public void WhereIsNotNullRight()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.FirstName != null);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                            + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                            + Environment.NewLine + "WHERE ([UE1].[FirstName] IS NOT NULL)", text);
        }

        [TestMethod]
        public void WhereIsNullLeft()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => null == x.FirstName);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                            + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                            + Environment.NewLine + "WHERE ([UE1].[FirstName] IS NULL)", text);
        }

        [TestMethod]
        public void WhereIsNullRight()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.FirstName == null);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                            + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                            + Environment.NewLine + "WHERE ([UE1].[FirstName] IS NULL)", text);
        }

        [TestMethod]
        public void WhereNotEqualExplicitValueRight()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.FirstName != "Name");

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                            + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                            + Environment.NewLine + "WHERE ([UE1].[FirstName] != @param1) @param1:Name", text);
        }

        [TestMethod]
        public void WhereStringStartsWith()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.FirstName.StartsWith("A"));

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*" + Environment.NewLine +
                            "FROM [UserEntity] AS [UE1]" + Environment.NewLine +
                            "WHERE ([UE1].[FirstName] LIKE (@param1 + '%') ESCAPE '_') @param1:A", text);
        }

        [TestMethod]
        public void WhereStringStartsWithContainsPercent()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.FirstName.StartsWith("A%"));

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*" + Environment.NewLine +
                            "FROM [UserEntity] AS [UE1]" + Environment.NewLine +
                            "WHERE ([UE1].[FirstName] LIKE (@param1 + '%') ESCAPE '_') @param1:A_%", text);
        }

        [TestMethod]
        public void WhereValueInCollection()
        {
            var conditions = new[] { 1, 23 };
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => conditions.Contains(x.Id));

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                            + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                            + Environment.NewLine + "WHERE ([UE1].[Id] IN (@param1_0, @param1_1)) @param1_0:1, @param1_1:23", text);
        }

        [TestMethod]
        public void WhereOnColumnIncludedInSelectionList()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery
                .Select(x => new { x.Id })
                .Where(x => x.Id > 5);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].[Id] AS [Id]"
                            + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                            + Environment.NewLine + "WHERE ([UE1].[Id] > @param1) @param1:5", text);
        }

        [TestMethod]
        public void WhereWithBooleanProperty()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.IsActive);

            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*" + Environment.NewLine +
                            "FROM [UserEntity] AS [UE1]" + Environment.NewLine +
                            "WHERE ([UE1].[IsActive] = 1)", text);
        }

        [TestMethod]
        public void WhereWithDateTimeNowDate()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var query = baseQuery.Where(x => x.WorkEnd > DateTime.Now.Date);
            var text = query.ToString();
            Assert.AreEqual("SELECT [UE1].*"
                            + Environment.NewLine + "FROM [UserEntity] AS [UE1]"
                            + Environment.NewLine + "WHERE ([UE1].[WorkEnd] > @param1) @param1:"
                            + DateTime.UtcNow.Date.ToString(CultureInfo.InvariantCulture), text);
        }

        [TestMethod]
        public void FunctionCallGenerator()
        {
            ITableQuery<UserEntity> baseQuery = new FunctionQuery<UserEntity>(_tableQueryProvider, "MyFunction",
                new[] { Expression.Constant(4) });
            var query = baseQuery.Where(x => x.WorkEnd > DateTime.Now.Date);

            var text = query.ToString();
            Assert.AreEqual("SELECT [MF1].*"
                            + Environment.NewLine + "FROM MyFunction(@param1) AS [MF1]"
                            + Environment.NewLine + "WHERE ([MF1].[WorkEnd] > @param2) @param1:4, @param2:"
                            + DateTime.UtcNow.Date.ToString(CultureInfo.InvariantCulture), text);
        }

        private class ProjectionWithMembersInitalizerInstance
        {
            public int Id { get; set; }

            public string Name { get; set; }
        }
    }
}