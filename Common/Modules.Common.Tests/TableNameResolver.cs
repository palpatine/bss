﻿using System;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;

namespace Modules.Common.Tests
{
    public class TableNameResolver : ITableNameResolver
    {
        public string GetTableName(Type type)
        {
            return string.Format("[{0}]", type.Name);
        }

        public string GetColumnName(ITableReferenceProvider tableReferenceProvider, System.Reflection.PropertyInfo property)
        {
            return "[" + property.Name + "]";
        }
    }
}
