﻿using System;
using System.Data;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

namespace Modules.Common.Tests
{
    interface IActivable
    {
        bool IsActive { get; set; }
    }

    interface IWithName
    {
        string FirstName { get; set; }
        string LastName { get; set; }

    }

    class UserEntity : IUser, IActivable, IWithName
    {
        [SqlType(SqlDbType.NVarChar, false, 25)]
        public string FirstName { get; set; }

        [SqlType(SqlDbType.NVarChar, false, 25)]
        public string LastName { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; set; }

        [SqlType(SqlDbType.Bit, false, 1)]
        public bool IsActive { get; set; }

        [SqlType(SqlDbType.DateTime, false, 10)]
        public DateTime WorkEnd { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        public Context Context { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        public int? ParentId { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        public IUser Parent { get; set; }
    }

    interface IMarker
    {
        int Id { get; set; }
    }


    interface IUser : IMarker
    {
    }

    class UserContactRelation
    {
        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; set; }
        [SqlType(SqlDbType.Int, false, 10)]
        public int User { get; set; }
        [SqlType(SqlDbType.Int, false, 10)]
        public int Contact { get; set; }

    }

    class UserAccountRelation
    {
        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; set; }
        [SqlType(SqlDbType.Int, true, 10)]
        public int? User { get; set; }
        [SqlType(SqlDbType.Int, true, 10)]
        public int? Account { get; set; }
    }

    internal class ContactEntity
    {
        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; set; }

        [SqlType(SqlDbType.NVarChar, false, 25)]
        public string Name { get; set; }

        [SqlType(SqlDbType.DateTime, false, 10)]
        public DateTime VersionStartDate { get; set; }

        [SqlType(SqlDbType.DateTime, true, 10)]
        public DateTime? VersionEndDate { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        public int ParentId { get; set; }

        [SqlType(SqlDbType.Int, true, 10)]
        public int? Account { get; set; }
    }

    [Flags]
    enum Context
    {
        Value1,
        Value2,
        Value3,
    }
}