﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;

namespace Modules.Common.Tests
{
    [TestClass]
    public class QueryHandlerTests
    {
        private TableQueryProvider _tableQueryProvider;
        private TimeProviderStub _timeProvider;
        private DateTime _timeValue;
        private QueryTranslationContext _context;

        [TestInitialize]
        public void Initialize()
        {
            _timeValue = DateTime.UtcNow;
            _timeProvider = new TimeProviderStub(_timeValue);
            var namesResolver = new TableNameResolver();
            var translator = new QueryTranslator(namesResolver, _timeProvider);
            _tableQueryProvider = new TableQueryProvider(null, () => translator, null, null);
            _context = translator.Context;
        }

        [TestMethod]
        public void BasicQueryIsSimple()
        {
            var tableQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var handler = new QueryHandler(_context);
            handler.VisitQuery(tableQuery.Expression);
            var query = (Query)_context.Stack.Pop();
            Assert.IsTrue(query.IsSimpleSubQuery);
        }

        [TestMethod]
        public void QueryWithAttachedColumnIsNotSimple()
        {
            ITableQuery<UserEntity> baseQuery = new TableQuery<UserEntity>(_tableQueryProvider);
            var tableQuery = baseQuery.Select(x => new { x.Id, Attached = true });
            var handler = new QueryHandler(_context);
            handler.VisitQuery(tableQuery.Expression);
            var query = (Query)_context.Stack.Pop();
            Assert.IsFalse(query.IsSimpleSubQuery);
        }

        [TestMethod]
        public void SimpleJoinIsSimpleQuery()
        {
            ITableQuery<UserContactRelation> baseQuery = new TableQuery<UserContactRelation>(_tableQueryProvider);
            var tableQuery = baseQuery.Join(
                     new TableQuery<ContactEntity>(_tableQueryProvider),
                     x => x.Contact,
                     x => x.Id,
                     (x, y) => new { Relation = x, Contact = y });

            var handler = new QueryHandler(_context);
            handler.VisitQuery(tableQuery.Expression);
            var query = (Query)_context.Stack.Pop();
            Assert.IsTrue(query.IsSimpleSubQuery);

        }
    }
}
