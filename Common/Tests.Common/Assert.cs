﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qdarc.Tests.Common
{
    // [DebuggerStepThrough]
    public static class Assert
    {
        public static void ShouldBeTrue(this bool actual, string additonalMessage = "")
        {
            ShouldBeEqual(actual, true, additonalMessage);
        }

        public static void ShouldBeFalse(this bool actual, string additonalMessage = "")
        {
            ShouldBeEqual(actual, false, additonalMessage);
        }

        public static void AllCorrespondingElementsShouldBeEqual<T>(
            this IEnumerable<T> collection,
            IEnumerable<T> other)
        {
            if (!collection.SequenceEqual(other))
                throw new AssertException("Colections should be sequence equal.");
        }

        public static void ShouldBeEqual<T>(this T actual, T expected, string additonalMessage = "")
        {
            if (expected == null && actual == null)
                return;
            var t = ((object)actual ?? expected).GetType();
            if (t.IsClass && t != typeof(string))
            {
                if (actual == null || expected == null)
                {
                    string message = string.Format(
                                       "Should be equal assert failed. One of the objects is null. {0}",
                                       additonalMessage);
                    throw new AssertException(message);
                }
                foreach (var property in typeof(T).GetProperties())
                {
                    string message = string.Format(
                                       "{0}. {1}",
                                       property.Name,
                                       additonalMessage);
                    property.GetValue(actual).ShouldBeEqual(property.GetValue(expected), message);
                }
            }
            else if ((actual != null && !actual.Equals(expected)) || !expected.Equals(actual))
            {
                string message = string.Format(
                                       "Should be equal assert failed. Expected: {0}, actual {1}. {2}",
                                       expected,
                                       actual,
                                       additonalMessage);
                throw new AssertException(message);
            }
        }

        public static void ShouldNotBeNull<T>(this T? value, string additonalMessage = "")
            where T : struct
        {
            if (value == null)
            {
                string message = string.Format("Should not be null assert failed. {0}", additonalMessage);
                throw new AssertException(message);
            }
        }

        public static void ShouldBeNull<T>(this T value, string additonalMessage = "")
        {
            if (value != null)
            {
                string message = string.Format("Should be null assert failed. {0}", additonalMessage);
                throw new AssertException(message);
            }
        }

        public static void ShouldNotBeNull<T>(this T value, string additonalMessage = "")
            where T : class
        {
            if (value == null)
            {
                string message = string.Format("Should not be null assert failed. {0}", additonalMessage);
                throw new AssertException(message);
            }
        }

        public static void ShouldThrow<T>(Action action)
        where T : Exception
        {
            try
            {
                action();
            }
            catch (T)
            {
                return;
            }
            catch (Exception ex)
            {
                throw new AssertException("Exception was not thrown by given action.", ex);
            }

            throw new AssertException("Exception was not thrown by given action.");
        }

        public static void ShouldBeInstanceOf<TExpected>(this object actual, string additionalMessage = "")
        {
            if (!typeof(TExpected).IsAssignableFrom(actual.GetType()))
            {
                string message = string.Format("Should be instance of {0} assert failed. {1}",
                                  typeof(TExpected).FullName, additionalMessage);
                throw new AssertException(message);
            }
        }

        public static void ShouldNotBeInstanceOf<TNotExpected>(this object actual, string additionalMessage = "")
        {
            if (typeof(TNotExpected).IsAssignableFrom(actual.GetType()))
            {
                string message = string.Format("Should not be instance of {0} assert failed. {1}",
                                  typeof(TNotExpected).FullName, additionalMessage);
                throw new AssertException(message);
            }
        }

        public static void ShouldBeTheSameInstance<T>(this T first, T second, string additonalMessage = "")
            where T : class
        {
            if (!ReferenceEquals(first, second))
            {
                string message = string.Format("Should be same assert failed. {0}", additonalMessage);
                throw new AssertException(message);
            }
        }
    }
}
