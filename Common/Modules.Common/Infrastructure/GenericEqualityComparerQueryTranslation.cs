using System.Linq.Expressions;
using Qdarc.Modules.Common.QueryGeneration;

namespace Qdarc.Modules.Common.Infrastructure
{
    sealed class GenericEqualityComparerQueryTranslation : IQueryTranslationHandler
    {
        public void HandleExpression(
            Expression node, 
            QueryTranslationContext context)
        {
            var alias = context.AliasManager.GetParameterAlias();
            var parameter = new QueryParameter
            {
                Alias = alias,
                Expression = node,
                QueryText = string.Format(QueryTranslationContext.BraceTemplate, alias)
            };

            context.Stack.Push(parameter);
        }
    }
}