﻿using System;
using System.Collections.Generic;

namespace Qdarc.Modules.Common.Infrastructure
{
    /// <summary>
    /// Type non specific equailty comparer.
    /// </summary>
    /// <typeparam name="T">Type to compare.</typeparam>
    internal sealed class GenericEqualityComparer<T> : IEqualityComparer<T>
    {
        /// <summary>
        /// The compare function.
        /// </summary>
        private readonly Func<T, T, bool> _compareFunction;

        /// <summary>
        /// The hash function.
        /// </summary>
        private readonly Func<T, int> _hashFunction;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericEqualityComparer{T}"/> class.
        /// </summary>
        /// <param name="compareFunction">The compare function.</param>
        /// <param name="hashFunction">The hash function.</param>
        public GenericEqualityComparer(
            Func<T, T, bool> compareFunction, 
            Func<T, int> hashFunction)
        {
            _compareFunction = compareFunction;
            _hashFunction = hashFunction;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericEqualityComparer{T}"/> class.
        /// </summary>
        /// <param name="compareFunction">The compare function.</param>
        public GenericEqualityComparer(Func<T, T, bool> compareFunction)
            : this(compareFunction, x => x.GetHashCode())
        {
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
        /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
        /// <returns>
        /// True if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(
            T x, 
            T y)
        {
            return _compareFunction(x, y);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public int GetHashCode(T obj)
        {
            return _hashFunction(obj);
        }
    }
}
