﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Modules.Common.QueryGeneration;

namespace Qdarc.Modules.Common.Infrastructure
{
    /// <summary>
    /// Factory class for generic equality compareres.
    /// </summary>
    public static class GenericEqualityComparer
    {
        /// <summary>
        /// Creates the specified compare function.
        /// </summary>
        /// <typeparam name="T">Type of compared entity.</typeparam>
        /// <param name="compareFunction">The compare function.</param>
        /// <param name="hashFunction">The hash function.</param>
        /// <returns>Equality comparer.</returns>
        public static IEqualityComparer<T> Create<T>(
            Func<T, T, bool> compareFunction,
            Func<T, int> hashFunction)
        {
            return new GenericEqualityComparer<T>(compareFunction, hashFunction);
        }

        /// <summary>
        /// Creates the specified compare function.
        /// </summary>
        /// <typeparam name="T">Type of compared entity.</typeparam>
        /// <param name="compareFunction">The compare function.</param>
        /// <returns>Equality comparer.</returns>
        public static IEqualityComparer<T> Create<T>(Func<T, T, bool> compareFunction)
        {
            return new GenericEqualityComparer<T>(compareFunction);
        }

        /// <summary>
        /// Retrives distinct set of elements determined by given identity retriver.
        /// </summary>
        /// <typeparam name="TCollection">Type of collection.</typeparam>
        /// <typeparam name="TCompared">Type to compare.</typeparam>
        /// <param name="set">The set.</param>
        /// <param name="identityRetriever">The compare function.</param>
        /// <returns>
        /// Distinct set of elements.
        /// </returns>
        [QueryTranslationHander(typeof(GenericEqualityComparerQueryTranslation))]
        public static IEnumerable<TCollection> Distinct<TCollection, TCompared>(
            this IEnumerable<TCollection> set,
            Func<TCollection, TCompared> identityRetriever)
        {
            return set.Distinct(Create<TCollection>((x, y) => Equals(identityRetriever(x), identityRetriever(y)), x => identityRetriever(x).GetHashCode()));
        }

        /// <summary>
        /// Retrives distinct set of elements determined by given identity retriver.
        /// </summary>
        /// <typeparam name="TCollection">Type of collection.</typeparam>
        /// <typeparam name="TCompared">Type to compare.</typeparam>
        /// <param name="set">The set.</param>
        /// <param name="second">The second.</param>
        /// <param name="identityRetriever">The compare function.</param>
        /// <returns>
        /// Distinct set of elements.
        /// </returns>
        public static IEnumerable<TCollection> Except<TCollection, TCompared>(
            this IEnumerable<TCollection> set,
            IEnumerable<TCollection> second,
            Func<TCollection, TCompared> identityRetriever)
        {
            return set.Except(second, Create<TCollection>((x, y) => Equals(identityRetriever(x), identityRetriever(y)), x => identityRetriever(x).GetHashCode()));
        }

        public static bool Contains<TCollection, TCompared>(
            this IEnumerable<TCollection> set,
            TCollection second,
            Func<TCollection, TCompared> identityRetriever)
        {
            return set.Contains(second, Create<TCollection>((x, y) => Equals(identityRetriever(x), identityRetriever(y)), x => identityRetriever(x).GetHashCode()));
        }
    }
}