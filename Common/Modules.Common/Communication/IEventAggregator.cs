﻿using System;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.Communication
{
    /// <summary>
    /// Center point for events/messages exchange in application.
    /// </summary>
    public interface IEventAggregator
    {
        /// <summary>
        /// Creates observable object and attaches handler to it.
        /// </summary>
        /// <typeparam name="TEvent">Type of event that will trigger observable object. This type
        /// can be base class of event or interface implemented by event.</typeparam>
        /// <param name="handler">The handler.</param>
        void On<TEvent>(Func<TEvent, Task> handler)
            where TEvent : IEvent;

        /// <summary>
        /// Publishes event.
        /// </summary>
        /// <typeparam name="TEvent">Type of event that is published.</typeparam>
        /// <param name="args">Instance of event.</param>
        Task PublishAsync<TEvent>(TEvent args)
            where TEvent : IEvent;
    }
}