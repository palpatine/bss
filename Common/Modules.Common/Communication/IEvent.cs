﻿namespace Qdarc.Modules.Common.Communication
{
    /// <summary>
    /// Mark interface that determines classes that can be published through event aggregator.
    /// </summary>
    public interface IEvent
    {
    }
}