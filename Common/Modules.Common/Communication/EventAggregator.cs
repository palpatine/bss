﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.Communication
{
    /// <summary>
    /// Center point for events/messages exchange in application.
    /// </summary>
    internal class EventAggregator : IEventAggregator
    {
        private readonly ConcurrentDictionary<Type, Func<object, Task>> _listeners
            = new ConcurrentDictionary<Type, Func<object, Task>>();

        public void On<TEvent>(Func<TEvent, Task> handler) where TEvent : IEvent
        {
            _listeners.AddOrUpdate(
                typeof(TEvent),
                x => handler((TEvent)x),
                (x, y) => async @event =>
                {
                    await y(@event);
                    await handler((TEvent)@event);
                });
        }

        public async Task PublishAsync<TEvent>(TEvent args) where TEvent : IEvent
        {
            Func<object, Task> handler;
            if (_listeners.TryGetValue(typeof(TEvent), out handler))
            {
                await handler(args);
            }
        }
    }
}