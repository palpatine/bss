using System.Data;
using System.Data.SqlClient;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public class SqlParameterCollectionWrapper : ISqlParameterCollection
    {
        private readonly SqlParameterCollection _sqlParameterCollection;

        public SqlParameterCollectionWrapper(SqlParameterCollection sqlParameterCollection)
        {
            _sqlParameterCollection = sqlParameterCollection;
        }

        public ISqlParameter AddWithValue(string name, object value)
        {
            return new SqlParameterWrapper(_sqlParameterCollection.AddWithValue(name, value));
        }

        public ISqlParameter Add(string name, SqlDbType sqlDbType)
        {
            return new SqlParameterWrapper(_sqlParameterCollection.Add(name, sqlDbType));
        }

        public ISqlParameter Add(string name, SqlDbType sqlDbType, int size)
        {
            return new SqlParameterWrapper(_sqlParameterCollection.Add(name, sqlDbType, size));
        }

        public ISqlParameter this[string name] {
            get { return new SqlParameterWrapper(_sqlParameterCollection[name]); }
            set { _sqlParameterCollection[name] = ((SqlParameterWrapper) value).SqlParameter; }
        }
    }
}