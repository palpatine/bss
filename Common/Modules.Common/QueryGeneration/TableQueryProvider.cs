using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    internal class TableQueryProvider : ITableQueryProvider, IQueryProvider
    {
        private readonly Func<ISqlConnection> _connectionFactory;

        private readonly IObjectMapper _mapper;

        private readonly IQueryParameterValueConverterFactory _queryParameterValueConverterFactory;

        private readonly Func<QueryTranslator> _queryTranslatorFactory;

        public TableQueryProvider(
            Func<ISqlConnection> connectionFactory,
            Func<QueryTranslator> queryTranslatorFactory,
            IObjectMapper mapper,
            IQueryParameterValueConverterFactory queryParameterValueConverterFactory)
        {
            _connectionFactory = connectionFactory;
            _queryTranslatorFactory = queryTranslatorFactory;
            _mapper = mapper;
            _queryParameterValueConverterFactory = queryParameterValueConverterFactory;
        }

        public IGroupingTableQuery<TKey, TSource, TProjection> CreateGroupingQuery<TKey, TSource, TProjection>(
            Expression expression)
        {
            return new GroupingTableQuery<TKey, TSource, TProjection>(this, expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            var result = Execute(expression);
            if (result is TResult)
            {
                return (TResult)result;
            }

            return (TResult)Convert.ChangeType(result, typeof(TResult));
        }

        public object Execute(Expression expression)
        {
            var connection = _connectionFactory();
            try
            {
                var part = ParseQueryExpression(expression);

                var listType = typeof(List<>).MakeGenericType(TypeSystem.GetElementType(expression.Type));
                var result = (IList)Activator.CreateInstance(listType);

                if (connection.Trace)
                {
                    Debug.WriteLine("query: " + part);
                }

                var sqlCommand = connection.CreateSqlCommand(part.FullQueryText);

                BindParameters(part, sqlCommand);

                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Type selectionType = null;
                        if (part.SelectionList != null)
                        {
                            selectionType = part.SelectionList.Type;
                        }
                        else if (part.From != null && part.From.References.Count() == 1)
                        {
                            selectionType = part.From.References.Single().Type;
                        }

                        var element = _mapper.MapToObject(
                            TypeSystem.GetElementType(expression.Type),
                            selectionType,
                            reader,
                            part.SelectionList == null ? null : part.SelectionList.Columns);

                        result.Add(element);
                    }
                }

                if (IsCollection(expression.Type))
                {
                    if (connection.Trace)
                    {
                        Debug.WriteLine("retrived: " + result.Count);
                    }

                    return result;
                }

                if (expression.NodeType == ExpressionType.Call)
                {
                    var call = (MethodCallExpression)expression;
                    if (Equals(typeof(TableQuery).GetTypeInfo(), call.Method.DeclaringType.GetTypeInfo())
                        || Equals(typeof(Queryable).GetTypeInfo(), call.Method.DeclaringType.GetTypeInfo()))
                    {
                        var resultList = result.Cast<object>();
                        if (call.Method.Name == "First")
                        {
                            return resultList.First();
                        }
                        if (call.Method.Name == "FirstOrDefault")
                        {
                            return resultList.FirstOrDefault();
                        }
                        if (call.Method.Name == "Single" || call.Method.Name == "Count" || call.Method.Name == "Any" || call.Method.Name == "All")
                        {
                            return resultList.Single();
                        }
                        if (call.Method.Name == "SingleOrDefault")
                        {
                            return resultList.SingleOrDefault();
                        }
                    }

                    throw new NotSupportedException(call.Method.Name);
                }

                var list = result.Cast<object>();
                return list.SingleOrDefault();
            }
            finally
            {
                DisposeOfConnection(connection);
            }
        }

        public ITableQuery<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new TableQuery<TElement>(this, expression);
        }

        private static bool IsCollection(Type type)
        {
            return type.IsConstructedGenericType
                   && (typeof(IEnumerable<>) == type.GetGenericTypeDefinition()
                       || typeof(IOrderedEnumerable<>) == type.GetGenericTypeDefinition()
                       || typeof(IQueryable<>) == type.GetGenericTypeDefinition()
                       || typeof(IOrderedQueryable<>) == type.GetGenericTypeDefinition()
                       || typeof(IList<>) == type.GetGenericTypeDefinition()
                       || typeof(ICollection<>) == type.GetGenericTypeDefinition()
                       || typeof(TableQuery<>) == type.GetGenericTypeDefinition()
                       || typeof(FunctionQuery<>) == type.GetGenericTypeDefinition()
                       || typeof(ITableQuery<>) == type.GetGenericTypeDefinition()
                       || typeof(Collection<>) == type.GetGenericTypeDefinition()
                       || typeof(List<>) == type.GetGenericTypeDefinition());
        }

        private void BindParameters(QueryPartDescriptor part, ISqlCommand sqlCommand)
        {
            foreach (var parameter in part.ParameterValues.EmptyIfNull())
            {
                var converter =
                    _queryParameterValueConverterFactory.Resolve(parameter.Item2.GetType());
                var value = parameter.Item2;

                if (converter != null)
                {
                    value = converter.Convert(value);
                }

                sqlCommand.Parameters.AddWithValue(parameter.Item1, value);
            }
        }

        public int ExecuteDelete(Expression expression)
        {
            var connection = _connectionFactory();
            try
            {
                var part = ParseDeleteExpression(expression);
                var sqlCommand = connection.CreateSqlCommand(part.FullQueryText);
                BindParameters(part, sqlCommand);

                return sqlCommand.ExecuteNonQuery();
            }
            finally
            {
                DisposeOfConnection(connection);
            }
        }

        public string GetQueryText(Expression expression)
        {
            var part = ParseQueryExpression(expression);
            return part.ToString();
        }

        public string GetDeleteQueryText(Expression expression)
        {
            var part = ParseDeleteExpression(expression);
            return part.ToString();
        }

        public Query ParseDeleteExpression(Expression expression)
        {
            var generator = _queryTranslatorFactory();
            var part = generator.TranslateDelete(expression);
            return part;
        }

        public Query ParseQueryExpression(Expression expression)
        {
            var generator = _queryTranslatorFactory();
            var part = generator.Translate(expression);
            return part;
        }

        private void DisposeOfConnection(ISqlConnection connection)
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }

        public IQueryProvider AsQueryProvider()
        {
            return this;
        }

        IQueryable<TElement> IQueryProvider.CreateQuery<TElement>(Expression expression)
        {
            return new TableQuery<TElement>(this, expression);
        }

        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            var type = TypeSystem.GetElementType(expression.Type);
            var queryType = typeof(TableQuery<>).MakeGenericType(type);

            var constructor = queryType.GetConstructor(
                BindingFlags.NonPublic | BindingFlags.Instance,
                null,
                new[] { typeof(TableQueryProvider), typeof(Expression) },
                null);

            return (IQueryable)constructor.Invoke(
                new object[] { this, expression });
        }
    }
}