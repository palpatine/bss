﻿using System;
using System.Collections.Generic;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IObjectMapper
    {
        object MapToObject(Type targetType, Type selectionType, ISqlDataReader reader, IEnumerable<ColumnReference> columns);
    }
}