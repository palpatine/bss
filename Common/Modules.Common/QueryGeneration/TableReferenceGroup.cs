using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public sealed class TableReferenceGroup : QueryPartDescriptor, ITableReferenceProvider, IColumnReferenceProvider
    {
        public override QueryPartKind Kind
        {
            get
            {
                return QueryPartKind.TableReferenceGroup;
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public IEnumerable<TableReference> References { get; set; }

        public Type Type { get; set; }

        public ColumnReference GenerateColumnReference(string columnName = null, PropertyInfo property = null)
        {
            var innerTableReference = References.Single(x => x.Type == Type);
            return innerTableReference.GenerateColumnReference(columnName, property);
        }

        public ColumnReference WrapInColumnReference()
        {
            return GenerateColumnReference();
        }

        public SelectionList SelectionList { get; set; }

        public bool HandleTransparently { get; set; }
    }
}