using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Table reference helper.
    /// </summary>
    internal sealed class SelectionListHandler
    {
        private readonly QueryTranslationContext _context;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SelectionListHandler" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public SelectionListHandler(
            QueryTranslationContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Handles the selection list.
        /// </summary>
        /// <param name="selector"> The selector. </param>
        public void HandleSelectionList(Expression selector)
        {
            var selectionList = (SelectionList)_context.Stack.TakeWhile(x => x.Kind != QueryPartKind.Query)
                .FirstOrDefault(x => x.Kind == QueryPartKind.SelectionList);

            if (selectionList == null)
            {
                selectionList = new SelectionList();
                _context.Stack.Push(selectionList);
                ProcessSelector(selector, selectionList);
                _context.Stack.Pop();

                var references =
                _context.PopUntil(
                    x => x.Kind == QueryPartKind.TableReference || x.Kind == QueryPartKind.TableReferenceGroup)
                    .ToArray();

                var providers = references.Cast<ITableReferenceProvider>()
                        .ToArray();

                var matchingReference = providers.SingleOrDefault(x => x.Type == selectionList.Type);

                if (matchingReference != null)
                {
                    _context.Stack.Push((QueryPartDescriptor)matchingReference);
                }
                else
                {
                    var groupsReferences = references.OfType<TableReferenceGroup>().SelectMany(x => x.References).ToArray();

                    var reference = groupsReferences.SingleOrDefault(x => x.Type == selectionList.Type);
                    if (reference != null)
                    {
                        _context.Stack.Push(reference);
                    }
                    else
                    {
                        var directReferences = references.OfType<TableReference>().ToArray();
                        var referenceGroup = new TableReferenceGroup
                        {
                            References = directReferences.Concat(groupsReferences).ToArray(),
                            Type = selectionList.Type,
                            SelectionList = selectionList
                        };
                        _context.Stack.Push(referenceGroup);
                    }
                }

                _context.Stack.Push(selectionList);
            }
            else
            {
                var group = (TableReferenceGroup)_context.TryFindElement(QueryPartKind.TableReferenceGroup);
                group.HandleTransparently = true;
                ProcessSelector(selector, selectionList);
                group.HandleTransparently = false;
                group.Type = selectionList.Type;
            }
        }

        private void ProcessSelector(Expression selector, SelectionList selectionList)
        {
            _context.Stack.Push(new QueryPartDescriptor { Kind = QueryPartKind.SelectionListMapping });

            _context.Visitor.Visit(selector);

            var columnReferences = new List<ColumnReference>();

            while (_context.Stack.Peek().Kind != QueryPartKind.SelectionListMapping)
            {
                var element = _context.Stack.Pop();
                if (element.Kind == QueryPartKind.ColumnReference)
                {
                    columnReferences.Add((ColumnReference)element);
                }
                else
                {
                    var aritmetic = ((IWrappableInColumn)element).WrapInColumnReference();
                    columnReferences.Add(aritmetic);
                }
            }

            _context.Stack.Pop();

            columnReferences.Reverse();
            columnReferences = columnReferences.OrderBy(x => x.Alias).ToList();
            selectionList.QueryText = string.Join(
                ", ",
                columnReferences.Select(x => x.Alias == null ? x.QueryText : string.Format("{0} AS {1}", x.QueryText, x.Alias)));
            selectionList.Columns = columnReferences;
            selectionList.Type = selector.Type.GetGenericArguments()[0].GetGenericArguments().Last();
            selectionList.ParameterValues = QueryParametersManager.CombineParameters(columnReferences);
        }
    }
}