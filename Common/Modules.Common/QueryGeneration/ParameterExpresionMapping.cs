using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public sealed class LambdaExpresionMapping : QueryPartDescriptor
    {
        public override QueryPartKind Kind
        {
            get { return QueryPartKind.LambdaExpresionMapping; }

            set { }
        }
    }

    public sealed class ParameterExpresionMapping : QueryPartDescriptor
    {
        public override QueryPartKind Kind
        {
            get { return QueryPartKind.ParameterExpresionMapping; }

            set { }
        }

        public ParameterExpression ParameterExpression { get; set; }

        public ITableReferenceProvider TableReferenceProvider { get; set; }
    }
}