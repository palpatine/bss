using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    /// Query extensions.
    /// </summary>
    public static class AsynchronousTableQueryExtensions
    {
        /// <summary>
        /// Counts the asynchronous.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <returns>Count of elemetns.</returns>
        public static async Task<int> CountAsync<T>(this ITableQuery<T> queryable)
        {
            return await Task.Run(() => queryable.Count());
        }

        /// <summary>
        /// Determines whether a sequence contains any elements.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <returns>True if the source sequence contains any elements; otherwise, false.</returns>
        public static async Task<bool> AnyAsync<T>(this ITableQuery<T> queryable)
        {
            return await Task.Run(() => queryable.Any());
        }

        /// <summary>
        /// Determines whether a sequence contains any elements.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns>
        /// True if the source sequence contains any elements; otherwise, false.
        /// </returns>
        public static async Task<bool> AnyAsync<T>(
            this ITableQuery<T> queryable,
            Expression<Func<T, bool>> predicate)
        {
            return await Task.Run(() => queryable.Any(predicate));
        }

        /// <summary>
        /// Gets frist element.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <returns>True if the source sequence contains any elements; otherwise, false.</returns>
        public static async Task<T> SingleAsync<T>(this ITableQuery<T> queryable)
        {
            return await Task.Run(() => queryable.Single());
        }

        /// <summary>
        /// Gets frist element or default value.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <returns>True if the source sequence contains any elements; otherwise, false.</returns>
        public static async Task<T> SingleOrDefaultAsync<T>(this ITableQuery<T> queryable)
        {
            return await Task.Run(() => queryable.SingleOrDefault());
        }

        /// <summary>
        /// Gets frist element or default value.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns>
        /// True if the source sequence contains any elements; otherwise, false.
        /// </returns>
        public static async Task<T> SingleOrDefaultAsync<T>(this ITableQuery<T> queryable, Expression<Func<T, bool>> predicate)
        {
            return await Task.Run(() => queryable.SingleOrDefault(predicate));
        }

        /// <summary>
        /// Projects collection by given function.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <typeparam name="TProjection">The type of the projection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <param name="projection">The projection.</param>
        /// <returns>
        /// True if the source sequence contains any elements; otherwise, false.
        /// </returns>
        public static async Task<List<TProjection>> SelectAsync<T, TProjection>(
            this ITableQuery<T> queryable,
            Expression<Func<T, TProjection>> projection)
        {
            return await Task.Run(() => queryable.Select(projection).ToList());
        }

        /// <summary>
        /// Gets frist element.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns>
        /// True if the source sequence contains any elements; otherwise, false.
        /// </returns>
        public static async Task<T> SingleAsync<T>(this ITableQuery<T> queryable, Expression<Func<T, bool>> predicate)
        {
            return await Task.Run(() => queryable.Single(predicate));
        }

        /// <summary>
        /// Elements at asynchronous.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <param name="index">The index.</param>
        /// <returns>Element at index.</returns>
        public static async Task<T> ElementAtAsync<T>(
            this ITableQuery<T> queryable,
            int index)
        {
            return await Task.Run(() => queryable.ElementAt(index));
        }

        /// <summary>
        /// Firsts the asynchronous.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <returns>First element.</returns>
        public static async Task<T> FirstAsync<T>(this ITableQuery<T> queryable)
        {
            return await Task.Run(() => queryable.First());
        }

        /// <summary>
        /// Firsts the or default asynchronous.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <returns>First element or null.</returns>
        public static async Task<T> FirstOrDefaultAsync<T>(this ITableQuery<T> queryable)
        {
            return await Task.Run(() => queryable.FirstOrDefault());
        }

        /// <summary>
        /// To the list asynchronous.
        /// </summary>
        /// <typeparam name="T">Type of collection.</typeparam>
        /// <param name="queryable">The queryable.</param>
        /// <returns>List of elements.</returns>
        public static async Task<List<T>> ToListAsync<T>(this ITableQuery<T> queryable)
        {
            return await Task.Run(() => queryable.ToList());
        }

        public static async Task<List<TSource>> PageAsync<TSource>(
           this ITableQuery<TSource> queryable, int skip, int limit)
        {
            return await Task.Run(() => queryable.Page(skip, limit));
        }

        /// <summary>
        /// Enables query generator to create "WITH RECUSIVE" sql clause.
        /// </summary>
        /// <typeparam name="TResult">Type of element.</typeparam>
        /// <typeparam name="TInner">The type of the inner.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="initial">The source.</param>
        /// <param name="recursive">The recursive.</param>
        /// <param name="initialKeyRetriever">The initial key retriever.</param>
        /// <param name="recursiveKeyRetriever">The recursive key retriever.</param>
        /// <param name="resultRetriever">The result retriever.</param>
        /// <returns>
        /// Nothing.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">Extensions method for sql query generation.</exception>
        public static ITableQuery<TResult> UnionAllRecursive<TResult, TInner, TKey>(
            this ITableQuery<TResult> initial,
            ITableQuery<TInner> recursive,
            Expression<Func<TResult, TKey>> initialKeyRetriever,
            Expression<Func<TInner, TKey>> recursiveKeyRetriever,
            Expression<Func<TInner, TResult>> resultRetriever)
        {
            var method = ExpressionExtensions.GetMethod(() => UnionAllRecursive<TResult, TInner, TKey>(null, null, null, null, null));
            var arguments = new[]
            {
                initial.Expression,
                recursive.Expression,
                Expression.Quote(initialKeyRetriever),
                Expression.Quote(recursiveKeyRetriever),
                Expression.Quote(resultRetriever)
            };

            return initial.Provider.CreateQuery<TResult>(Expression.Call(
                null,
                method,
                arguments));
        }
    }
}