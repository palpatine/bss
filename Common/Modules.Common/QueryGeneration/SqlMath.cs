﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public static class SqlMath
    {
        public static TValue Min<TValue>(TValue value)
        {
            throw new InvalidOperationException("This method is an sql expression tree marker. It is not supposed to be executed.");
        }

        public static TValue Max<TValue>(TValue value)
        {
            throw new InvalidOperationException("This method is an sql expression tree marker. It is not supposed to be executed.");
        }

        public static TValue Avg<TValue>(TValue value)
        {
            throw new InvalidOperationException("This method is an sql expression tree marker. It is not supposed to be executed.");
        }

        public static TValue AvgDistinct<TValue>(TValue value)
        {
            throw new InvalidOperationException("This method is an sql expression tree marker. It is not supposed to be executed.");
        }

        public static TValue CountDistinct<TValue>(TValue value)
        {
            throw new InvalidOperationException("This method is an sql expression tree marker. It is not supposed to be executed.");
        }

        public static int Count<TValue>(TValue value)
        {
            throw new InvalidOperationException("This method is an sql expression tree marker. It is not supposed to be executed.");
        }

        public static int Count()
        {
            throw new InvalidOperationException("This method is an sql expression tree marker. It is not supposed to be executed.");
        }
    }
}
