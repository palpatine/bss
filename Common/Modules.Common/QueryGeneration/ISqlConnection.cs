﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ISqlConnection
        : IDisposable
    {
        bool Trace { get; }
        SqlTransaction BeginTransaction();

        ISqlCommand CreateSqlCommand(
            string commandText,
            CommandType commandtype = CommandType.Text);
    }
}