using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Filtering handler.
    /// </summary>
    internal sealed class FilteringHandler
    {
        /// <summary>
        ///     The context.
        /// </summary>
        private readonly QueryTranslationContext _context;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FilteringHandler" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public FilteringHandler(
            QueryTranslationContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Handles the filtering.
        /// </summary>
        /// <param name="node"> The node. </param>
        public void HandleFiltering(Expression node)
        {
            var part = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Where,
            };

            if (_context.TryFindElement(QueryPartKind.GroupBySelectionList) != null)
            {
                part.Kind = QueryPartKind.Having;
            }

            var selectionList = _context.TryFindSelectionList();
            TableReferenceGroup group = null;

            if (selectionList != null)
            {
                group = _context.TryFindElement<TableReferenceGroup>(QueryPartKind.TableReferenceGroup, x => true);
                group.HandleTransparently = true;
            }

            _context.Stack.Push(part);
            _context.Visitor.Visit(node);

            if (group != null)
            {
                group.HandleTransparently = false;
            }

            var filters = new List<QueryPartDescriptor>();

            while (_context.Stack.Peek() != part)
            {
                filters.Add(_context.Stack.Pop());
            }

            QueryPartDescriptor filter = filters.Single();

            if (!QueryComparisionHandler.TryConvertToComparision(filter, out filter))
            {
                throw new InvalidOperationException();
            }

            part.QueryText = filter.QueryText;
            part.ParameterValues = filter.ParameterValues;
        }
    }
}