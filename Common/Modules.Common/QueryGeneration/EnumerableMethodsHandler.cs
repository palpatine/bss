using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Threading;
using System.Web.UI.WebControls;
using Microsoft.Practices.ServiceLocation;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    /// Enumerable methods handler.
    /// </summary>
    internal sealed class EnumerableMethodsHandler
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly QueryTranslationContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableMethodsHandler" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public EnumerableMethodsHandler(
            QueryTranslationContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Handles the enumerable methods.
        /// </summary>
        /// <param name="node"> The node. </param>
        /// <exception cref="System.NotSupportedException"> Thrown if query is not supported. </exception>
        public void HandleEnumerableMethods(MethodCallExpression node)
        {

            if (node.Method.Name == "Contains")
            {
                HandleContains(node);
            }
            else if (node.Method.Name == "Select")
            {
                HandleSelect(node);
            }
            else
            {
                throw new NotSupportedException(node.ToString());
            }
        }

        /// <summary>
        /// Handles the select.
        /// </summary>
        /// <param name="node">The node.</param>
        private void HandleSelect(MethodCallExpression node)
        {
            var alias = _context.AliasManager.GetParameterAlias();
            var parameter = new QueryParameter
            {
                Alias = alias,
                Expression = node,
                QueryText = string.Format(QueryTranslationContext.BraceTemplate, alias)
            };

            _context.Stack.Push(parameter);
        }

        /// <summary>
        /// Handles the contains.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandleContains(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);
            var parameter = (QueryParameter)_context.Stack.Pop();
            _context.Visitor.Visit(node.Arguments[1]);

            var current = _context.Stack.Pop();
            var columnReference = current as ColumnReference;

            if (columnReference == null)
            {
                var tableReference = current as TableReference;
                if (tableReference == null)
                {
                    throw new InvalidOperationException();
                }

                columnReference = tableReference.GenerateColumnReference("Id", tableReference.Type.GetProperty("Id"));
            }

            parameter.RelatedEntityProperty = columnReference.RelatedEntityProperty;
            if (parameter.ParameterValues.Any())
            {
                var part = new Comparission
                {
                    QueryText = string.Format(
                        CultureInfo.InvariantCulture,
                        "({0} IN ({1}))",
                        columnReference.QueryText,
                        string.Join(", ", parameter.ParameterValues.Select(x => x.Item1))),
                    ParameterValues = QueryParametersManager.CombineParameters(columnReference, parameter)
                };
                _context.Stack.Push(part);
            }
            else
            {
                var part = new Comparission
                {
                    QueryText = "(1 = 0)"
                };
                _context.Stack.Push(part);
            }
        }
    }
}