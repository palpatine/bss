﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    internal class MemberAccessHandler
    {
        private readonly QueryTranslationContext _context;

        public MemberAccessHandler(QueryTranslationContext context)
        {
            _context = context;
        }

        public Expression HandleMemberAccess(MemberExpression node)
        {
            if (node.Expression == null)
            {
                if (Equals(node.Member, ExpressionExtensions.GetMember(() => DateTime.UtcNow))
                     || Equals(node.Member, ExpressionExtensions.GetMember(() => DateTime.Now)))
                {
                    return HandleDateTimeAccess();
                }
                else if (Equals(node.Member, ExpressionExtensions.GetMember(() => DateTime.Today)))
                {
                    HandleDateAccess();
                    return node;
                }
                else if (Equals(node.Member, ExpressionExtensions.GetMember(() => DateTime.MaxValue)))
                {
                    HandleMaxDateAccess();
                    return node;
                }
                else if (Equals(node.Member, ExpressionExtensions.GetMember(() => DateTime.MinValue)))
                {
                    HandleMinDateAccess();
                    return node;
                }
                else
                {
                    var parameter = new QueryParameter
                    {
                        Alias = QueryParametersManager.CurrentDateParameterAlias,
                        Expression = node
                    };
                    _context.Stack.Push(parameter);

                    return node;
                }
            }

            var nodeExpression = _context.Visitor.Visit(node.Expression);
            var part = _context.Stack.Peek();

            if (part.Kind == QueryPartKind.ColumnReference)
            {
                HandleMemberAccessToColumnReference(node, part);
            }
            else if (part.Kind == QueryPartKind.Parameter)
            {
                HandleMemberAccessToParameter(node, part, nodeExpression);
            }
            else if (part.Kind == QueryPartKind.TableReference)
            {
                HandleTableReference(node, (TableReference)part);
            }
            else if (part.Kind == QueryPartKind.TableReferenceGroup)
            {
                var property = (PropertyInfo)node.Member;
                var tableReferenceGroup = (TableReferenceGroup)part;
                TableReference tableReference;

                if (tableReferenceGroup.HandleTransparently)
                {
                    if (tableReferenceGroup.SelectionList == null || tableReferenceGroup.SelectionList.Columns == null)
                    {
                        tableReference =
                            tableReferenceGroup.References.SingleOrDefault(x => x.Type == tableReferenceGroup.Type);

                        if (tableReference == null)
                        {
                            tableReference =
                                tableReferenceGroup.References.SingleOrDefault(
                                    x => property.DeclaringType.IsAssignableFrom(x.Type));
                        }

                        if (tableReference == null)
                        {
                            throw new InvalidOperationException();
                        }
                    }
                    else
                    {
                        var innerColumnReference = tableReferenceGroup.SelectionList.Columns
                            .Where(x =>
                                x.RelatedIntermediateTypeProperty.MetadataToken == property.MetadataToken
                                && x.RelatedIntermediateTypeProperty.Module == property.Module)
                            .ToArray();

                        if (innerColumnReference.Count() == 1)
                        {
                            _context.Stack.Pop();
                            var columnReference = innerColumnReference.Single().Clone();
                            _context.Stack.Push(columnReference);
                            return Expression.MakeMemberAccess(nodeExpression, node.Member);
                        }
                        else if (!innerColumnReference.Any())
                        {
                            throw new InvalidOperationException();
                        }
                        else
                        {
                            _context.Stack.Pop();
                            _context.Stack.Push(new CompoundPropertyAccess(innerColumnReference));
                            return Expression.MakeMemberAccess(nodeExpression, node.Member);
                        }
                    }
                }
                else if (property.DeclaringType.IsAssignableFrom(tableReferenceGroup.Type))
                {
                    return CreateReferenceToColumnInTableReferenceGroup(node, property, nodeExpression);
                }
                else
                {
                    tableReference =
                        tableReferenceGroup.References.SingleOrDefault(
                            x => property.DeclaringType.IsAssignableFrom(x.Type));
                }

                if (tableReference != null)
                {
                    HandleTableReference(node, tableReference);
                }
                else if (_context.TryFindElement(QueryPartKind.Join) != null)
                {
                    _context.Stack.Pop();

                    var columnName = _context.TableNameResolver.GetColumnName(tableReferenceGroup, property);
                    var column = tableReferenceGroup.GenerateColumnReference(columnName, property);
                    _context.Stack.Push(column);
                }
                else
                {
                    throw new NotSupportedException(node.ToString());
                }
            }
            else if (part.Kind == QueryPartKind.CompoundPropertyAccess)
            {
                _context.Stack.Pop();
                var property = (PropertyInfo)node.Member;
                var compoundPropertyAccess = (CompoundPropertyAccess)part;

                var column = compoundPropertyAccess.Candidates.Single(x =>
                    x.Alias.EndsWith("." + property.Name + "]"));
                _context.Stack.Push(column);
            }
            else
            {
                throw new NotSupportedException(node.ToString());
            }

            return Expression.MakeMemberAccess(nodeExpression, node.Member);
        }

        private void HandleDateAccess()
        {
            var parameter = new QueryParameter
            {
                Alias = QueryParametersManager.CurrentDateParameterAlias,
                Expression = Expression.Constant(_context.QueryParametersManager.CurrnetDateTime.Date)
            };
            _context.Stack.Push(parameter);
        }

        private void HandleMaxDateAccess()
        {
            var parameter = new QueryParameter
            {
                Alias = QueryParametersManager.MaximalDateParameterAlias,
                Expression = Expression.Constant(_context.QueryParametersManager.MaximalAccepableDate)
            };
            _context.Stack.Push(parameter);
        }

        private void HandleMinDateAccess()
        {
            var parameter = new QueryParameter
            {
                Alias = QueryParametersManager.MinimalDateParameterAlias,
                Expression = Expression.Constant(_context.QueryParametersManager.MinimalAccepableDate)
            };
            _context.Stack.Push(parameter);
        }

        private Expression CreateReferenceToColumnInTableReferenceGroup(MemberExpression node, PropertyInfo property,
            Expression nodeExpression)
        {
            var group = (TableReferenceGroup)_context.Stack.Pop();
            var columnReference = new ColumnReference
            {
                QueryText = string.Format(QueryTranslationContext.BraceTemplate, property.Name),
                RelatedIntermediateTypeProperty = property,
                TableReference = null,
                TableReferenceGroup = group,
                Type = property.PropertyType
            };

            _context.Stack.Push(columnReference);
            return Expression.MakeMemberAccess(nodeExpression, node.Member);
        }

        private Expression HandleDateTimeAccess()
        {
            var parameter = new QueryParameter
            {
                Alias = QueryParametersManager.CurrentDateParameterAlias,
                Expression = Expression.Constant(_context.QueryParametersManager.CurrnetDateTime)
            };
            _context.Stack.Push(parameter);

            var utcNowProperty = ExpressionExtensions.GetMember(() => DateTime.UtcNow);

            return Expression.MakeMemberAccess(null, utcNowProperty);
        }

        private void HandleMemberAccessToColumnReference(
            MemberExpression node,
            QueryPartDescriptor part)
        {
            var columnReference = (ColumnReference)part;

            if (columnReference.TableReferenceGroup != null)
            {
                columnReference.RelatedIntermediateTypeProperty = (PropertyInfo)node.Member;
                columnReference.QueryText = string.Format(
                    CultureInfo.InvariantCulture, QueryTranslationContext.BraceTemplate,
                    string.Join(".", Regex.Match(columnReference.QueryText, @"\[(.*?)\]").Groups[1].Value,
                        node.Member.Name));
            }
            else if (node.Expression.Type.IsAssignableFrom(columnReference.TableReference.Type)
                && (node.Member.Name != "Id" || columnReference.RelatedEntityProperty == null))
            {
                columnReference.Type = columnReference.TableReference.Type;
                columnReference.RelatedEntityProperty = (PropertyInfo)node.Member;
                columnReference.IsStarReference = false;
                var columnName = _context.TableNameResolver.GetColumnName(columnReference.TableReference, (PropertyInfo)node.Member);

                if (columnReference.TableReference.Alias == null)
                {
                    columnReference.QueryText = string.Format(
                        CultureInfo.InvariantCulture,
                        QueryTranslationContext.BraceTemplate,
                        columnName);
                }
                else
                {
                    columnReference.QueryText = string.Format(
                        CultureInfo.InvariantCulture,
                        QueryTranslationContext.PrefixedTemplate,
                        columnReference.TableReference.Alias,
                        columnName);
                }
                if (columnReference.Alias == null)
                {
                    columnReference.Alias = string.Format(CultureInfo.InvariantCulture, QueryTranslationContext.BraceTemplate, node.Member.Name);
                }
            }
            else if (node.Member.Name == "Date" && node.Member.DeclaringType == typeof(DateTime))
            {
                columnReference.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "CAST({0} AS DATE)",
                    columnReference.QueryText);
            }
            else if (node.Member.Name == "Value" && node.Member.DeclaringType.IsGenericType && node.Member.DeclaringType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                //do nothing
            }
            else if (node.Member.Name == "HasValue" && node.Member.DeclaringType.IsGenericType && node.Member.DeclaringType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                columnReference.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "CAST(IIF({0} IS NOT NULL, 1, 0) AS BIT)",
                    columnReference.QueryText);
                columnReference.Type = typeof(bool);
            }
            else if (node.Member.Name != "Id")
            {
                throw new InvalidOperationException();
            }
        }

        private void HandleMemberAccessToParameter(
            MemberExpression node,
            QueryPartDescriptor part,
            Expression nodeExpression)
        {
            if (typeof(ITableQuery).IsAssignableFrom(node.Type))
            {
                _context.Stack.Pop();
                var retriver = Expression.Lambda(node).Compile();
                var tableQuery = (ITableQuery)retriver.DynamicInvoke();
                var handler = new QueryHandler(_context);
                handler.VisitQuery(tableQuery.Expression);
                var query = (Query)_context.Stack.Peek();
                if (query.TemporaryViews != null)
                {
                    foreach (var view in query.TemporaryViews)
                    {
                        _context.Stack.Push(view);
                    }
                }
            }
            else
            {
                var parameter = (QueryParameter)part;
                parameter.Expression = Expression.MakeMemberAccess(nodeExpression, node.Member);
                if (parameter.Alias == QueryParametersManager.CurrentDateParameterAlias)
                {
                    var alias = _context.AliasManager.GetParameterAlias();
                    parameter.Alias = alias;
                }
            }
        }

        private void HandleTableReference(MemberExpression node, TableReference reference)
        {
            var property = (PropertyInfo)node.Member;
            var columnName = _context.TableNameResolver.GetColumnName(reference, property);
            var columnReference = reference.GenerateColumnReference(columnName ?? string.Format(QueryTranslationContext.BraceTemplate, property.Name), property);
            _context.Stack.Pop();
            _context.Stack.Push(columnReference);
        }
    }
}