﻿namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ISqlParameter
    {
        object Value { get; set; }
        string TypeName { get; set; }
    }
}