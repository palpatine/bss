using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IGroup<TKey, TSource>
    {
        TKey Key { get; }

        int Count();
    }

    public interface IGroupingTableQuery<TKey, TSource, TSelection>
    {
        Type ElementType { get; }

        Expression Expression { get; }

        ITableQueryProvider Provider { get; }
    }

    public static class TableQuery
    {
        public static IQueryable<TElement> AsQueryable<TElement>(this ITableQuery<TElement> query)
        {
            var method = ExpressionExtensions.GetMethod(() => AsQueryable<TElement>(null));
            return (IQueryable<TElement>)query.Provider.CreateQuery<TElement>(Expression.Call(method, query.Expression));
        }

        public static IGroupingTableQuery<TKey, TSource, TProjection> GroupBy<TSource, TKey, TProjection>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, TKey>> keyRetriver,
            Expression<Func<IGroup<TKey, TSource>, TProjection>> projection)
        {
            var method = ExpressionExtensions.GetMethod(() => GroupBy<TSource, TKey, TProjection>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(keyRetriver),
                Expression.Quote(projection)
            };

            return query.Provider.CreateGroupingQuery<TKey, TSource, TProjection>(Expression.Call(
                null,
                method,
                arguments));
        }

        public static ITableQuery<TSource> Where<TSource>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, bool>> predicate)
        {
            var method = ExpressionExtensions.GetMethod(() => Where<TSource>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(predicate)
            };

            return query.Provider.CreateQuery<TSource>(Expression.Call(
                null,
                method,
                arguments));
        }

        public static bool Contains<TSource>(
            this ITableQuery<TSource> query,
            TSource value)
        {
            var method = ExpressionExtensions.GetMethod(() => Contains<TSource>(null, default(TSource)));
            var arguments = new[]
            {
                query.Expression,
                Expression.Constant(value)
            };

            var containsQuery = query.Provider.CreateQuery<TSource>(Expression.Call(
                null,
                method,
                arguments));

            return containsQuery.Provider.Execute<bool>(containsQuery.Expression);
        }

        public static ITableQuery<TResult> UnionAll<TResult>(this ITableQuery<TResult> first, ITableQuery<TResult> second)
        {
            var method = ExpressionExtensions.GetMethod(() => UnionAll<TResult>(null, null));
            var arguments = new[]
            {
                first.Expression,
                second.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return first.Provider.CreateQuery<TResult>(expression);
        }

        public static ITableQuery<TResult> Union<TResult>(this ITableQuery<TResult> first, ITableQuery<TResult> second)
        {
            var method = ExpressionExtensions.GetMethod(() => Union<TResult>(null, null));
            var arguments = new[]
            {
                first.Expression,
                second.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return first.Provider.CreateQuery<TResult>(expression);
        }

        public static ITableQuery<TResult> Intersect<TResult>(this ITableQuery<TResult> first, ITableQuery<TResult> second)
        {
            var method = ExpressionExtensions.GetMethod(() => Intersect<TResult>(null, null));
            var arguments = new[]
            {
                first.Expression,
                second.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return first.Provider.CreateQuery<TResult>(expression);
        }

        public static ITableQuery<TResult> Except<TResult>(this ITableQuery<TResult> first, ITableQuery<TResult> second)
        {
            var method = ExpressionExtensions.GetMethod(() => Except<TResult>(null, null));
            var arguments = new[]
            {
                first.Expression,
                second.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return first.Provider.CreateQuery<TResult>(expression);
        }

        public static ITableQuery<TProjection> Join<TFirst, TSecond, TKey, TProjection>(
           this ITableQuery<TFirst> query,
            ITableQuery<TSecond> other,
            Expression<Func<TFirst, TKey>> firstKey,
            Expression<Func<TSecond, TKey>> secondKey,
            Expression<Func<TFirst, TSecond, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => Join<TFirst, TSecond, TKey, TProjection>(null, null, null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(firstKey),
                Expression.Quote(secondKey),
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<TProjection> LeftJoin<TFirst, TSecond, TKey, TProjection>(
           this ITableQuery<TFirst> query,
            ITableQuery<TSecond> other,
            Expression<Func<TFirst, TKey>> firstKey,
            Expression<Func<TSecond, TKey>> secondKey,
            Expression<Func<TFirst, TSecond, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => LeftJoin<TFirst, TSecond, TKey, TProjection>(null, null, null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(firstKey),
                Expression.Quote(secondKey),
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<TProjection> Select<TSource, TProjection>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, TProjection>> projection)
        {
            var method = ExpressionExtensions.GetMethod(() => Select<TSource, TProjection>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(projection)
            };

            return query.Provider.CreateQuery<TProjection>(Expression.Call(
                null,
                method,
                arguments));
        }

        public static TSource First<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => First<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static List<TSource> Page<TSource>(
           this ITableQuery<TSource> query, int skip, int limit)
        {
            var pagedQuery = query.PageInner(skip, limit);

            return query.Provider.Execute<List<TSource>>(pagedQuery.Expression);
        }

        internal static ITableQuery<TSource> PageInner<TSource>(
           this ITableQuery<TSource> query, int skip, int limit)
        {
            var method = ExpressionExtensions.GetMethod(() => PageInner<TSource>(null, 0, 0));
            var arguments = new[]
            {
                query.Expression,
                Expression.Constant(skip),
                Expression.Constant(limit)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TSource>(expression);
        }

        public static bool Any<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => Any<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<bool>(expression);
        }

        public static bool Any<TSource>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, bool>> predicate)
        {
            var method = ExpressionExtensions.GetMethod(() => Any<TSource>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(predicate)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<bool>(expression);
        }

        public static TSource Single<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => Single<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static TSource SingleOrDefault<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => SingleOrDefault<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static TSource Single<TSource>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, bool>> predicate)
        {
            var method = ExpressionExtensions.GetMethod(() => Single<TSource>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(predicate)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static TSource SingleOrDefault<TSource>(
           this ITableQuery<TSource> query,
           Expression<Func<TSource, bool>> predicate)
        {
            var method = ExpressionExtensions.GetMethod(() => SingleOrDefault<TSource>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(predicate)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static List<TSource> ToList<TSource>(
            this ITableQuery<TSource> query)
        {
            return query.Provider.Execute<List<TSource>>(query.Expression);
        }

        public static ITableQuery<TSource> OrderBy<TSource, TOdered>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, TOdered>> property)
        {
            var method = ExpressionExtensions.GetMethod(() => OrderBy<TSource, TOdered>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(property)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TSource>(expression);
        }

        public static ITableQuery<TSource> OrderByDescending<TSource, TOdered>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, TOdered>> property)
        {
            var method = ExpressionExtensions.GetMethod(() => OrderByDescending<TSource, TOdered>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(property)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TSource>(expression);
        }

        public static TSource ElementAt<TSource>(
            this ITableQuery<TSource> query,
            int index)
        {
            var method = ExpressionExtensions.GetMethod(() => ElementAt<TSource>(null, 0));
            var arguments = new[]
            {
                query.Expression,
                Expression.Constant(index)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static int Count<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => Count<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<int>(expression);
        }

        public static TSource FirstOrDefault<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => FirstOrDefault<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static ITableQuery Create(
            Type resultType,
            string query,
            IEnumerable<Tuple<string, object>> arguments,
            Func<ISqlConnection> connectionFactory)
        {
            var type = typeof(TableQuery<>).MakeGenericType(resultType);
            return (ITableQuery)Activator.CreateInstance(type, new object[] { connectionFactory, query, arguments });
        }
    }
}