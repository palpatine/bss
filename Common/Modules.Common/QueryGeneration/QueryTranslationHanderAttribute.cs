﻿using System;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public class QueryTranslationHanderAttribute : Attribute
    {
        public QueryTranslationHanderAttribute(Type handerType)
        {
            if (!typeof(IQueryTranslationHandler).IsAssignableFrom(handerType))
            {
                throw new InvalidOperationException();
            }

            HanderType = handerType;
        }

        public Type HanderType { get; private set; }
    }
}