using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public sealed class ValuesQuery<TEntity> : ITableQuery<TEntity>, IDeletionQuery<TEntity>, IValuesQuery
    {
        public IEnumerable Values { get; }

        private readonly Expression _expression;

        private readonly TableQueryProvider _provider;

        public ValuesQuery(
            Func<ISqlConnection> connectionFactory,
            IEnumerable<TEntity> values)
            : this(new TableQueryProvider(
                connectionFactory,
                ServiceLocator.Current.GetInstance<Func<QueryTranslator>>(),
                ServiceLocator.Current.GetInstance<IObjectMapper>(),
                ServiceLocator.Current.GetInstance<IQueryParameterValueConverterFactory>()),
                values)
        {
        }

        internal ValuesQuery(TableQueryProvider provider,
            IEnumerable<TEntity> values)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            Values = values;

            _provider = provider;
            _expression = Expression.Constant(this);
        }

        internal ValuesQuery(TableQueryProvider provider,
            Expression expression)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            if (!typeof(ITableQuery<TEntity>).GetTypeInfo().IsAssignableFrom(expression.Type.GetTypeInfo()))
            {
                throw new ArgumentOutOfRangeException("expression");
            }

            _provider = provider;
            _expression = expression;
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns> Deletion query. </returns>
        int IDeletionQuery<TEntity>.Execute()
        {
            return _provider.ExecuteDelete(Expression);
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns> Deletion query. </returns>
        Task<int> IDeletionQuery<TEntity>.ExecuteAsync()
        {
            return Task.Run(() => _provider.ExecuteDelete(Expression));
        }

        /// <summary>
        /// Wheres the specified predicate.
        /// </summary>
        /// <param name="predicate"> The predicate. </param>
        /// <returns> Deleteion query. </returns>
        IDeletionQuery<TEntity> IDeletionQuery<TEntity>.Where(Expression<Func<TEntity, bool>> predicate)
        {
            return (TableQuery<TEntity>)this.Where(predicate);
        }

        /// <summary>
        /// Gets the type of the element(s) that are returned when the expression tree associated
        /// with this instance of <see cref="T:System.Linq.IQueryable" /> is executed.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Type" /> that represents the type of the element(s) that are
        /// returned when the expression tree associated with this object is executed.
        /// </returns>
        public Type ElementType => typeof(TEntity);

        /// <summary>
        /// Gets the expression tree that is associated with the instance of <see
        /// cref="T:System.Linq.IQueryable" /> .
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Linq.Expressions.Expression" /> that is associated with this
        /// instance of <see cref="T:System.Linq.IQueryable" />.
        /// </returns>
        public Expression Expression => _expression;

        /// <summary>
        /// Gets the query provider that is associated with this data source.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Linq.IQueryProvider" /> that is associated with this data source.
        /// </returns>
        public ITableQueryProvider Provider => _provider;

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate
        /// through the collection.
        /// </returns>
        public IEnumerator<TEntity> GetEnumerator()
        {
            return ((IEnumerable<TEntity>)_provider.Execute(_expression)).GetEnumerator();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns> A <see cref="System.String" /> that represents this instance. </returns>
        public override string ToString()
        {
            return _provider.GetQueryText(_expression);
        }
    }
}