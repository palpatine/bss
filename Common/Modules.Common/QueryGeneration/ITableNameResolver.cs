﻿using System;
using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ITableNameResolver
    {
        string GetTableName(Type type);

        string GetColumnName(ITableReferenceProvider tableReferenceProvider, PropertyInfo property);
    }
}