using System.Collections.Generic;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     From clause.
    /// </summary>
    public class FromClause : QueryPartDescriptor
    {
        /// <summary>
        ///     Gets or sets the alias.
        /// </summary>
        /// <value> The alias. </value>
        public string Alias { get; set; }

        /// <summary>
        ///     Gets or sets the kind.
        /// </summary>
        /// <value> The kind. </value>
        public override QueryPartKind Kind
        {
            get { return QueryPartKind.From; }
            set { }
        }

        /// <summary>
        ///     Gets or sets the references.
        /// </summary>
        /// <value> The references. </value>
        public IEnumerable<TableReference> References { get; set; }
    }
}