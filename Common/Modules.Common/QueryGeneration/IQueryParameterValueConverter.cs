using System;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IQueryParameterValueConverter
    {
        Type HandledType { get; }
        
        object Convert(object value);
    }
}