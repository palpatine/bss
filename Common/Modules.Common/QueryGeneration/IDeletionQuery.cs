using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Enables linq like creation of deletion query.
    /// </summary>
    /// <typeparam name="TEntity"> The type of the entity. </typeparam>
    public interface IDeletionQuery<TEntity>
    {
        /// <summary>
        ///     Executes deletion query.
        /// </summary>
        /// <returns> Count of affected objects. </returns>
        int Execute();
       
        /// <summary>
        ///     Executes deletion query.
        /// </summary>
        /// <returns> Count of affected objects. </returns>
        Task<int> ExecuteAsync();

        /// <summary>
        ///     Filters by specified predicate.
        /// </summary>
        /// <param name="predicate"> The predicate. </param>
        /// <returns> Filtered version of query. </returns>
        IDeletionQuery<TEntity> Where(Expression<Func<TEntity, bool>> predicate);
    }
}