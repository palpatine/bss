using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Type system.
    /// </summary>
    internal static class TypeSystem
    {
        /// <summary>
        ///     Gets the type of the element.
        /// </summary>
        /// <param name="seqType"> Type of the seq. </param>
        /// <returns> Element type. </returns>
        internal static Type GetElementType(Type seqType)
        {
            Type ienum = FindTableQuery(seqType);

            if (ienum == null)
            {
                return seqType;
            }

            return ienum.GenericTypeArguments[0];
        }

        /// <summary>
        ///     Finds the i enumerable.
        /// </summary>
        /// <param name="seqType"> Type of the seq. </param>
        /// <returns> Inner type. </returns>
        private static Type FindTableQuery(Type seqType)
        {
            if (seqType == null || seqType == typeof(string))
            {
                return null;
            }

            if (seqType.IsArray)
            {
                return typeof(ITableQuery<>).MakeGenericType(seqType.GetElementType());
            }

            if (seqType.GetTypeInfo().IsGenericType)
            {
                foreach (Type arg in seqType.GenericTypeArguments)
                {
                    Type ienum = typeof(ITableQuery<>).MakeGenericType(arg);
                    if (ienum.GetTypeInfo().IsAssignableFrom(seqType.GetTypeInfo()))
                    {
                        return ienum;
                    }

                    ienum = typeof(IQueryable<>).MakeGenericType(arg);
                    if (ienum.GetTypeInfo().IsAssignableFrom(seqType.GetTypeInfo()))
                    {
                        return ienum;
                    }
                }
            }

            IEnumerable<Type> ifaces = seqType.GetTypeInfo().ImplementedInterfaces;
            if (ifaces != null && ifaces.Any())
            {
                foreach (Type iface in ifaces)
                {
                    Type ienum = FindTableQuery(iface);
                    if (ienum != null)
                    {
                        return ienum;
                    }
                }
            }

            if (seqType.GetTypeInfo().BaseType != null && seqType.GetTypeInfo().BaseType != typeof(object))
            {
                return FindTableQuery(seqType.GetTypeInfo().BaseType);
            }

            return null;
        }
    }
}