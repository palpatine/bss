using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Query handler.
    /// </summary>
    internal sealed class QueryHandler
    {
        /// <summary>
        ///     The context.
        /// </summary>
        private readonly QueryTranslationContext _context;

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryHandler" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public QueryHandler(
            QueryTranslationContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Visits the query.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="externalFilter">The external filter.</param>
        public void VisitQuery(Expression expression, Expression externalFilter = null)
        {
            var part = new Query();
            _context.Stack.Push(part);
            var methodCall = expression as MethodCallExpression;
            if (methodCall != null && TableQueryMethodsHandler.IsSetOperator(methodCall))
            {
                var handler = new TableQueryMethodsHandler(_context);
                handler.HandleSetOperator(methodCall);
            }
            else
            {
                part.QueryText = "SELECT";

                var from = new FromClause();
                part.From = from;

                _context.Stack.Push(from);

                _context.Visitor.Visit(expression);

                if (externalFilter != null)
                {
                    var handler = new FilteringHandler(_context);
                    handler.HandleFiltering(externalFilter);
                }

                SelectionList wrappingSelectionList = FillQuery(part);

                FormatQueryText(part, from);

                if (wrappingSelectionList != null)
                {
                    _context.Stack.Pop();
                    var wrappingQuery = new Query();
                    wrappingQuery.SelectionList = wrappingSelectionList;
                    wrappingQuery.From = new FromClause();
                    TableReference tableReference = part.WrapInTableReference(_context.AliasManager);
                    wrappingQuery.From.References = new[] { tableReference };
                    wrappingQuery.QueryText = string.Format(
                        "SELECT {0} FROM {1} AS {2}",
                        wrappingSelectionList.QueryText,
                        tableReference.QueryText,
                        _context.AliasManager.GetAlias("TBL"));
                    _context.Stack.Push(wrappingQuery);
                    wrappingQuery.ParameterValues = part.ParameterValues;
                }
            }
        }

        /// <summary>
        ///     Visits the query.
        /// </summary>
        /// <param name="expression"> The expression. </param>
        public void VisitDeleteQuery(Expression expression)
        {
            var part = new Query();
            part.QueryText = "DELETE";
            _context.Stack.Push(part);

            var from = new FromClause();
            part.From = from;

            _context.Stack.Push(from);

            _context.Visitor.Visit(expression);

            FillQuery(part);

            if (part.SelectionList != null)
            {
                throw new InvalidOperationException();
            }

            part.SelectionList = new SelectionList
            {
                QueryText = string.Empty
            };

            FormatQueryText(part, from);
        }

        /// <summary>
        ///     Visits the query.
        /// </summary>
        /// <param name="orderStatements"> The order statements. </param>
        /// <returns> Combined order statement. </returns>
        private static QueryPartDescriptor CombineOrderStatements(
            IEnumerable<QueryPartDescriptor> orderStatements)
        {
            return CombineStatements(orderStatements, "{0}, {1}");
        }

        /// <summary>
        ///     Combines the statements.
        /// </summary>
        /// <param name="statementsToCombine"> The statements to combine. </param>
        /// <param name="format"> The format. </param>
        /// <returns> Combined statemetns. </returns>
        private static QueryPartDescriptor CombineStatements(
            IEnumerable<QueryPartDescriptor> statementsToCombine,
            string format)
        {
            QueryPartDescriptor[] statements = statementsToCombine.Reverse().ToArray();

            if (!statements.Any())
            {
                return null;
            }

            if (statements.Length == 1)
            {
                return statements[0];
            }

            var result = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Where,
                QueryText = statements[0].QueryText,
                ParameterValues = QueryParametersManager.CombineParameters(statements)
            };

            foreach (QueryPartDescriptor queryPartDescriptor in statements.Skip(1))
            {
                result.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    format,
                    result.QueryText,
                    queryPartDescriptor.QueryText);
            }

            return result;
        }

        /// <summary>
        ///     Combines the where statements.
        /// </summary>
        /// <param name="whereStatements"> The where statements. </param>
        /// <returns> Combined statemetns. </returns>
        private static QueryPartDescriptor CombineWhereStatements(
            IEnumerable<QueryPartDescriptor> whereStatements)
        {
            return CombineStatements(whereStatements, "({0} AND {1})");
        }

        /// <summary>
        ///     Formats the distinct.
        /// </summary>
        /// <param name="part"> The part. </param>
        private static void FormatDistinct(Query part)
        {
            if (part.IsDistinct)
            {
                part.QueryText += " DISTINCT";
            }
        }

        /// <summary>
        ///     Formats the order by.
        /// </summary>
        /// <param name="part"> The part. </param>
        private static void FormatOrderBy(Query part)
        {
            if (part.Order != null)
            {
                part.QueryText += Environment.NewLine + "ORDER BY " + part.Order.QueryText;

                if (part.Paging != null)
                {
                    part.QueryText += Environment.NewLine + part.Paging.QueryText;
                    part.ParameterValues = QueryParametersManager.CombineParameters(part, part.Paging);
                }
            }
        }

        /// <summary>
        ///     Formats the query text.
        /// </summary>
        /// <param name="part"> The part. </param>
        /// <param name="from"> From. </param>
        private static void FormatQueryText(
            Query part,
            FromClause from)
        {
            FormatWithClause(part);
            FormatTop(part);
            FormatDistinct(part);
            FormatSelectionList(part, from);
            FormatWhere(part);
            FormatGroupBy(part);
            FormatHaving(part);
            FormatOrderBy(part);
        }

        private static void FormatGroupBy(Query part)
        {
            if (part.GroupBy != null)
            {
                part.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0}" + Environment.NewLine + "GROUP BY {1}",
                    part.QueryText,
                    part.GroupBy.QueryText);
                part.ParameterValues =
                    QueryParametersManager.CombineParameters(new QueryPartDescriptor[] { part, part.GroupBy });
            }
        }

        /// <summary>
        ///     Formats the selection list.
        /// </summary>
        /// <param name="part"> The part. </param>
        /// <param name="from"> From. </param>
        private static void FormatSelectionList(
            Query part,
            FromClause from)
        {
            if (part.SelectionList == null)
            {
                if (from.References == null || !from.References.Any())
                {
                    throw new InvalidOperationException();
                }

                part.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} {1}.*" + Environment.NewLine + "FROM {2}",
                    part.QueryText,
                    from.References.First().Alias,
                    from.QueryText);
                part.Type = from.References.First().Type;
                part.ParameterValues = QueryParametersManager.CombineParameters(new QueryPartDescriptor[] { part, from });
            }
            else if (from.References == null || !from.References.Any())
            {
                part.QueryText = string.Format(
                        CultureInfo.InvariantCulture,
                        "{0} {1}",
                        part.QueryText,
                        part.SelectionList.QueryText);
                part.ParameterValues =
                    QueryParametersManager.CombineParameters(new QueryPartDescriptor[] { part, part.SelectionList, from });
                part.Type = part.SelectionList.Type;
            }
            else
            {
                part.QueryText = string.Format(
                            CultureInfo.InvariantCulture,
                            "{0} {1}",
                            part.QueryText,
                            part.SelectionList.QueryText).Trim() +
                        Environment.NewLine +
                        string.Format(
                            CultureInfo.InvariantCulture,
                            "FROM {0}",
                            from.QueryText);
                part.ParameterValues =
                    QueryParametersManager.CombineParameters(new QueryPartDescriptor[] { part, part.SelectionList, from });
                part.Type = part.SelectionList.Type;
            }
        }

        /// <summary>
        ///     Formats the top.
        /// </summary>
        /// <param name="part"> The part. </param>
        private static void FormatTop(Query part)
        {
            if (part.Top != null)
            {
                part.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} TOP {1}",
                    part.QueryText,
                    part.Top.QueryText);
            }
        }

        /// <summary>
        ///     Formats the where.
        /// </summary>
        /// <param name="part"> The part. </param>
        private static void FormatWhere(Query part)
        {
            if (part.Where != null)
            {
                part.QueryText += Environment.NewLine + "WHERE " + part.Where.QueryText;
                part.ParameterValues = QueryParametersManager.CombineParameters(part, part.Where);
            }
        }

        /// <summary>
        ///     Formats the where.
        /// </summary>
        /// <param name="part"> The part. </param>
        private static void FormatHaving(Query part)
        {
            if (part.Having != null)
            {
                part.QueryText += Environment.NewLine + "HAVING " + part.Having.QueryText;
                part.ParameterValues = QueryParametersManager.CombineParameters(part, part.Having);
            }
        }

        /// <summary>
        ///     Formats the with clause.
        /// </summary>
        /// <param name="part"> The part. </param>
        private static void FormatWithClause(Query part)
        {
            if (part.TemporaryViews != null && part.TemporaryViews.Any())
            {
                string withText = string.Join("," + Environment.NewLine, part.TemporaryViews.Select(x => x.QueryText));
                part.ParameterValues = QueryParametersManager.CombineParameters(new[] { part }.Concat(part.TemporaryViews));
                part.QueryPrefix = string.Format(CultureInfo.InvariantCulture, "WITH {0}" + Environment.NewLine + "{1}", withText,
                    part.QueryPrefix);
            }
        }

        /// <summary>
        ///     Fills the query.
        /// </summary>
        /// <param name="part"> The part. </param>
        private SelectionList FillQuery(Query part)
        {
            var parts = new List<QueryPartDescriptor>();
            while (_context.Stack.Peek().Kind != QueryPartKind.Query)
            {
                parts.Add(_context.Stack.Pop());
            }

            SelectionList[] selectionLists =
                parts.Where(x => x.Kind == QueryPartKind.SelectionList).Cast<SelectionList>().ToArray();
            part.SelectionList = selectionLists.LastOrDefault();
            IEnumerable<QueryPartDescriptor> whereStatements = parts.Where(x => x.Kind == QueryPartKind.Where);
            QueryPartDescriptor where = CombineWhereStatements(whereStatements);
            part.Where = @where;
            IEnumerable<QueryPartDescriptor> orderStatements = parts.Where(x => x.Kind == QueryPartKind.Order);
            QueryPartDescriptor order = CombineOrderStatements(orderStatements);
            part.Order = order;
            QueryPartDescriptor top = parts.SingleOrDefault(x => x.Kind == QueryPartKind.Top);
            part.Top = top;
            QueryPartDescriptor skip = parts.SingleOrDefault(x => x.Kind == QueryPartKind.Skip);
            if (skip != null && part.Top == null)
            {
                throw new InvalidOperationException();
            }
            if (skip != null && part.Top != null)
            {
                part.Paging = new QueryPartDescriptor
                {
                    ParameterValues = QueryParametersManager.CombineParameters(part.Top, skip),
                    QueryText = string.Format(
                        CultureInfo.InvariantCulture,
                        "OFFSET {0} Rows FETCH NEXT {1} ROWS ONLY",
                        skip.ParameterValues.Single().Item1,
                        part.Top.ParameterValues.Single().Item1)
                };

                part.Top = null;
            }

            bool distinct = parts.Any(x => x.Kind == QueryPartKind.Distinct);
            part.IsDistinct = distinct;
            QueryPartDescriptor[] with = parts.Where(x => x.Kind == QueryPartKind.WithRecursive).ToArray();
            part.TemporaryViews = with;
            var groupBy =
                (GroupBySelectionList)parts.SingleOrDefault(x => x.Kind == QueryPartKind.GroupBySelectionList);
            part.GroupBy = groupBy;
            IEnumerable<QueryPartDescriptor> havingStatements = parts.Where(x => x.Kind == QueryPartKind.Having);
            QueryPartDescriptor having = CombineWhereStatements(havingStatements);
            part.Having = having;
            var paging = parts.SingleOrDefault(x => x.Kind == QueryPartKind.Paging);

            if (part.Paging != null && paging != null)
            {
                throw new InvalidOperationException();
            }

            if (paging != null)
            {
                part.Paging = paging;
            }

            if (part.Order == null && part.Paging != null)
            {
                throw new InvalidOperationException("Query must be ordered to be pageable.");
            }

            if (selectionLists.Count() > 1)
            {
                return selectionLists.First();
            }

            return null;
        }
    }
}