using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    /// Table query.
    /// </summary>
    /// <typeparam name="TEntity"> The type of the entity. </typeparam>
    public sealed class TableQuery<TEntity> : ITableQuery<TEntity>, IDeletionQuery<TEntity>, IOrderedQueryable<TEntity>
    {
        /// <summary>
        /// Gets the query.
        /// </summary>
        /// <value>
        /// The query.
        /// </value>
        public string Query { get; private set; }

        /// <summary>
        /// Gets the arguments.
        /// </summary>
        /// <value>
        /// The arguments.
        /// </value>
        public IEnumerable<Tuple<string, object>> Arguments { get; private set; }

        /// <summary>
        /// The expression.
        /// </summary>
        private readonly Expression _expression;

        /// <summary>
        /// The provider.
        /// </summary>
        private readonly TableQueryProvider _provider;

        /// <summary>
        /// Initializes a new instance of the <see cref="TableQuery{TEntity}" /> class.
        /// </summary>
        /// <param name="connectionFactory"> The connection factory. </param>
        public TableQuery(
            Func<ISqlConnection> connectionFactory)
            : this(new TableQueryProvider(
                connectionFactory,
                ServiceLocator.Current.GetInstance<Func<QueryTranslator>>(),
                ServiceLocator.Current.GetInstance<IObjectMapper>(),
                ServiceLocator.Current.GetInstance<IQueryParameterValueConverterFactory>()))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableQuery{TEntity}"/> class.
        /// </summary>
        /// <param name="connectionFactory">The connection factory.</param>
        /// <param name="query">The query.</param>
        /// <param name="arguments">The arguments.</param>
        internal TableQuery(
            Func<ISqlConnection> connectionFactory,
            string query,
            IEnumerable<Tuple<string, object>> arguments)
            : this(connectionFactory)
        {
            Query = query;
            Arguments = arguments;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableQuery{TEntity}" /> class.
        /// </summary>
        /// <param name="provider"> The provider. </param>
        /// <exception cref="System.ArgumentNullException"> Provider. </exception>
        internal TableQuery(TableQueryProvider provider)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            _provider = provider;
            _expression = Expression.Constant(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableQuery{TEntity}" /> class.
        /// </summary>
        /// <param name="provider"> The provider. </param>
        /// <param name="expression"> The expression. </param>
        /// <exception cref="System.ArgumentNullException"> Provider or expression. </exception>
        /// <exception cref="System.ArgumentOutOfRangeException"> Expression. </exception>
        internal TableQuery(
            TableQueryProvider provider,
            Expression expression)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            if (!typeof(ITableQuery<TEntity>).GetTypeInfo().IsAssignableFrom(expression.Type.GetTypeInfo())
                && !typeof(IQueryable<TEntity>).GetTypeInfo().IsAssignableFrom(expression.Type.GetTypeInfo()))
            {
                throw new ArgumentOutOfRangeException("expression");
            }

            _provider = provider;
            _expression = expression;
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns> Deletion query. </returns>
        int IDeletionQuery<TEntity>.Execute()
        {
            return _provider.ExecuteDelete(Expression);
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns> Deletion query. </returns>
        Task<int> IDeletionQuery<TEntity>.ExecuteAsync()
        {
            return Task.Run(() => _provider.ExecuteDelete(Expression));
        }

        /// <summary>
        /// Wheres the specified predicate.
        /// </summary>
        /// <param name="predicate"> The predicate. </param>
        /// <returns> Deleteion query. </returns>
        IDeletionQuery<TEntity> IDeletionQuery<TEntity>.Where(Expression<Func<TEntity, bool>> predicate)
        {
            return (TableQuery<TEntity>)this.Where(predicate);
        }

        /// <summary>
        /// Gets the type of the element(s) that are returned when the expression tree associated
        /// with this instance of <see cref="T:System.Linq.IQueryable" /> is executed.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Type" /> that represents the type of the element(s) that are
        /// returned when the expression tree associated with this object is executed.
        /// </returns>
        public Type ElementType => typeof(TEntity);

        /// <summary>
        /// Gets the expression tree that is associated with the instance of <see
        /// cref="T:System.Linq.IQueryable" /> .
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Linq.Expressions.Expression" /> that is associated with this
        /// instance of <see cref="T:System.Linq.IQueryable" />.
        /// </returns>
        public Expression Expression => _expression;

        /// <summary>
        /// Gets the query provider that is associated with this data source.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Linq.IQueryProvider" /> that is associated with this data source.
        /// </returns>
        public ITableQueryProvider Provider => _provider;

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate
        /// through the collection.
        /// </returns>
        public IEnumerator<TEntity> GetEnumerator()
        {
            return ((IEnumerable<TEntity>)_provider.Execute(_expression)).GetEnumerator();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns> A <see cref="System.String" /> that represents this instance. </returns>
        public override string ToString()
        {
            return _provider.GetQueryText(_expression);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        IQueryProvider IQueryable.Provider => Provider.AsQueryProvider();
    }
}