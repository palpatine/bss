using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.Infrastructure;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    /// Translates linq query to sql.
    /// </summary>
    internal sealed class QueryTranslator
        : ExpressionVisitor
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly QueryTranslationContext _context;

#if DEBUG

        internal QueryTranslationContext Context => _context;

#endif

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryTranslator"/> class.
        /// </summary>
        /// <param name="tableNameResolver">The table name resolver.</param>
        public QueryTranslator(ITableNameResolver tableNameResolver)
            : this(tableNameResolver, new TimeProvider())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryTranslator"/> class.
        /// </summary>
        /// <param name="tableNameResolver">The table name resolver.</param>
        /// <param name="timeProvider">The time provider.</param>
        internal QueryTranslator(
            ITableNameResolver tableNameResolver,
            ITimeProvider timeProvider)
        {
            _context = new QueryTranslationContext(timeProvider, this, tableNameResolver);
        }

        /// <summary>
        /// Translates the specified expression.
        /// </summary>
        /// <param name="expression"> The expression. </param>
        /// <returns> Qurey descriptor. </returns>
        /// <exception cref="System.InvalidOperationException"> Query parsing failed. </exception>
        internal Query Translate(
            Expression expression)
        {
            var handler = new QueryHandler(_context);
            handler.VisitQuery(expression);

            var result = _context.Stack.Pop();
            if (_context.Stack.Any())
            {
                throw new InvalidOperationException("Query parsing failed");
            }

            result.ParameterValues = result.ParameterValues
                .EmptyIfNull()
                .Distinct(x => x.Item1)
                .ToArray();

            return (Query)result;
        }

        /// <summary>
        /// Translates the delete.
        /// </summary>
        /// <param name="expression"> The expression. </param>
        /// <returns> Query descriptor. </returns>
        internal Query TranslateDelete(Expression expression)
        {
            _context.AliasManager.ForbidTableAliases();
            var handler = new QueryHandler(_context);
            handler.VisitDeleteQuery(expression);

            var result = (Query)_context.Stack.Pop();
            if (_context.Stack.Any())
            {
                throw new InvalidOperationException("Query parsing failed");
            }

            result.ParameterValues = result.ParameterValues.EmptyIfNull()
                .Distinct(x => x.Item1).ToArray();
            return result;
        }

        /// <summary>
        /// Visits the children of the <see cref="T:System.Linq.Expressions.BinaryExpression" />.
        /// </summary>
        /// <param name="node"> The expression to visit. </param>
        /// <returns> Original expression. </returns>
        protected override Expression VisitBinary(
            BinaryExpression node)
        {
            Visit(node.Left);
            var left = _context.Stack.Pop();
            Visit(node.Right);
            var right = _context.Stack.Pop();

            if (left.Kind == QueryPartKind.TableReference || left.Kind == QueryPartKind.TableReferenceGroup)
            {
                var reference = (ITableReferenceProvider)left;
                left = ((IColumnReferenceProvider)left).GenerateColumnReference("[Id]", reference.Type.GetProperty("Id"));
            }

            if (right.Kind == QueryPartKind.TableReference || right.Kind == QueryPartKind.TableReferenceGroup)
            {
                var reference = (ITableReferenceProvider)right;
                right = ((IColumnReferenceProvider)right).GenerateColumnReference("[Id]", reference.Type.GetProperty("Id"));
            }

            var queryComparisionHandler = new QueryComparisionHandler(_context.Stack, _context.QueryParametersManager);

            if (queryComparisionHandler.TryHandleComparison(node, left, right))
            {
                return node;
            }

            if (node.NodeType == ExpressionType.Coalesce)
            {
                if (left.Kind == QueryPartKind.Parameter && right.Kind == QueryPartKind.Parameter)
                {
                    var leftParameter = (QueryParameter)left;
                    leftParameter.IgnoreRelatedProperty = true;

                    var rightParameter = (QueryParameter)right;
                    rightParameter.IgnoreRelatedProperty = true;
                }
                else if (left.Kind == QueryPartKind.Parameter)
                {
                    var rightAsRelatedPropertyProvider = right as IRelatedPropertyProvider;
                    var parameter = (QueryParameter)left;
                    if (rightAsRelatedPropertyProvider != null)
                    {
                        parameter.RelatedEntityProperty = rightAsRelatedPropertyProvider.RelatedEntityProperty;
                    }
                    else
                    {
                        parameter.IgnoreRelatedProperty = true;
                    }
                }
                else if (right.Kind == QueryPartKind.Parameter)
                {
                    var leftAsRelatedPropertyProvider = left as IRelatedPropertyProvider;
                    var parameter = (QueryParameter)right;
                    if (leftAsRelatedPropertyProvider != null)
                    {
                        parameter.RelatedEntityProperty = leftAsRelatedPropertyProvider.RelatedEntityProperty;
                    }
                    else
                    {
                        parameter.IgnoreRelatedProperty = true;
                    }
                }

                var query = new QueryArithmetic
                {
                    ParameterValues = QueryParametersManager.CombineParameters(left, right),
                    QueryText = string.Format("Coalesce({0}, {1})", left.QueryText, right.QueryText)
                };
                _context.Stack.Push(query);
                return node;
            }
            else
            {
                var queryArithmeticHandler = new QueryArithmeticHandler(_context);
                if (queryArithmeticHandler.TryHandleArithmetic(node, left, right))
                {
                    return node;
                }
            }

            throw new NotSupportedException(node.ToString());
        }

        /// <summary>
        /// Visits the constant.
        /// </summary>
        /// <param name="node"> The node. </param>
        /// <returns> Original expression. </returns>
        protected override Expression VisitConstant(
            ConstantExpression node)
        {
            var functionQuery = node.Value as IFunctionQuery;

            if (functionQuery != null)
            {
                var alias = _context.AliasManager.GetAliasForTable(functionQuery.FunctionName);

                var parameters = new List<QueryParameter>();
                foreach (var argument in functionQuery.Arguments)
                {
                    Visit(argument);
                    var parameter = (QueryParameter)_context.Stack.Pop();
                    parameter.IgnoreRelatedProperty = true;
                    parameters.Add(parameter);
                }

                var parametersValues = parameters.SelectMany(x => x.ParameterValues).ToArray();
                var parametersList = string.Join(",", parametersValues.Select(x => x.Item1));

                var part = new TableReference
                {
                    Type = functionQuery.ElementType,
                    Alias = alias == null ? null : string.Format(CultureInfo.InvariantCulture, QueryTranslationContext.BraceTemplate, alias),
                    ParameterValues = parametersValues,
                    QueryText = string.Format("{0}({1})", functionQuery.FunctionName, parametersList)
                };

                _context.Stack.Push(part);
                var fromClauseHandler = new FromClauseHandler(_context.Stack, _context.AliasManager);
                fromClauseHandler.AddToFrom(part);

                return node;
            }

            var valuesQuery = node.Value as IValuesQuery;

            if (valuesQuery != null)
            {
                var values = valuesQuery.Values.Cast<object>().ToArray();
                var type = valuesQuery.ElementType;
                if (!values.Any())
                {
                    throw new InvalidOperationException("Values must contain at least one item.");
                }

                var valueRetrivers = GetValuesRetrivers(type);
                var valuesData = new StringBuilder();
                foreach (var value in values)
                {
                    var row = string.Join(", ", valueRetrivers.Select(x =>
                    {
                        var propertyValue = x.Item2(value);
                        if (propertyValue == null)
                        {
                            return $"Cast(null AS {x.Item3})";
                        }
                        if (propertyValue is bool)
                        {
                            return $"Cast({(((bool)propertyValue) ? 1 : 0)} AS BIT)";
                        }
                        if (propertyValue.GetType().IsEnum)
                        {
                            return $"Cast({((int)propertyValue).ToString()} AS {x.Item3})";
                        }
                        if (propertyValue is string)
                        {
                            return $"Cast(N'{propertyValue.ToString().Replace("'", "''")}' AS {x.Item3})";
                        }
                        if (propertyValue is DateTime)
                        {
                            return $"Cast(N'{((DateTime)propertyValue).ToString("s", CultureInfo.InvariantCulture)}' AS DateTime)";
                        }
                        if (propertyValue is DateTimeOffset)
                        {
                            return $"Cast(N'{((DateTimeOffset)propertyValue).ToString("s", CultureInfo.InvariantCulture)}' AS DateTime)";
                        }

                        return propertyValue;
                    }));

                    valuesData.AppendFormat("({0})", row);
                    valuesData.AppendLine();
                    valuesData.Append(",");
                }

                valuesData.Length = valuesData.Length - 1;
                var alias = _context.AliasManager.GetAliasForTable("V");
                var queryText = $"(VALUES {valuesData}) AS {alias}({string.Join(", ", valueRetrivers.Select(x => $"[{x.Item1}]"))})";

                var part = new TableReference
                {
                    Type = valuesQuery.ElementType,
                    Alias = alias == null ? null : string.Format(CultureInfo.InvariantCulture, QueryTranslationContext.BraceTemplate, alias),
                    QueryText = queryText,
                    IsValues = true
                };

                _context.Stack.Push(part);
                var fromClauseHandler = new FromClauseHandler(_context.Stack, _context.AliasManager);
                fromClauseHandler.AddToFrom(part);

                return node;
            }

            var query = node.Value as ITableQuery;
            if (query != null)
            {
                var tableName = _context.TableNameResolver.GetTableName(query.ElementType);
                var alias = _context.AliasManager.GetAliasForTable(tableName);

                var part = new TableReference
                {
                    Type = query.ElementType,
                    Alias = alias == null ? null : string.Format(CultureInfo.InvariantCulture, QueryTranslationContext.BraceTemplate, alias),
                    QueryText = tableName
                };

                _context.Stack.Push(part);
                var fromClauseHandler = new FromClauseHandler(_context.Stack, _context.AliasManager);
                fromClauseHandler.AddToFrom(part);
            }
            else
            {
                var part = new QueryParameter
                {
                    Expression = node,
                    Alias = _context.AliasManager.GetParameterAlias()
                };
                _context.Stack.Push(part);
            }

            return node;
        }

        private static string GetSqlType(PropertyInfo property)
        {
            var attribute = property.GetCustomAttribute<SqlTypeAttribute>();
            if (attribute != null)
            {
                return attribute.GetSqlTypeDeclaration();
            }
            if (property.PropertyType == typeof(int))
            {
                return "int";
            }
            if (property.PropertyType == typeof(bool))
            {
                return "bit";
            }
            if (property.PropertyType == typeof(string))
            {
                return "varchar(max)";
            }
            if (property.PropertyType == typeof(DateTime))
            {
                return "DateTime";
            }
            if (property.PropertyType == typeof(DateTimeOffset))
            {
                return "DateTime";
            }

            throw new NotImplementedException();
        }

        private static List<Tuple<string, Func<object, object>, string>> GetValuesRetrivers(
            Type type,
            ParameterExpression parameter = null,
            Expression member = null,
            string prefix = null)
        {
            var properties = type.GetProperties();
            var valueRetrivers = new List<Tuple<string, Func<object, object>, string>>();

            foreach (var propertyInfo in properties)
            {
                if (member == null)
                {
                    parameter = Expression.Parameter(typeof(object));
                    member = Expression.Convert(parameter, type);
                }

                var current = Expression.Property(member, propertyInfo);

                string columnName;
                if (prefix != null)
                    columnName = prefix + "." + propertyInfo.Name;
                else
                {
                    columnName = propertyInfo.Name;
                }
                if (propertyInfo.PropertyType != typeof(string) && propertyInfo.PropertyType.IsClass)
                {
                    valueRetrivers.AddRange(GetValuesRetrivers(propertyInfo.PropertyType, parameter, current, columnName));
                }
                else
                {
                    var body = Expression.Convert(current, typeof(object));
                    var function = Expression.Lambda<Func<object, object>>(body, parameter).Compile();
                    valueRetrivers.Add(Tuple.Create(propertyInfo.Name, function, GetSqlType(propertyInfo)));
                }
            }

            return valueRetrivers;
        }

        /// <summary>
        /// Visits the lambda.
        /// </summary>
        /// <typeparam name="T"> Type of query. </typeparam>
        /// <param name="node"> The node. </param>
        /// <returns> Original expression. </returns>
        protected override Expression VisitLambda<T>(
            Expression<T> node)
        {
            var elements = _context.Stack.TakeWhile(x => x.Kind != QueryPartKind.Query).ToArray();
            var position = 0;
            var lambdaExpresionMapping = new LambdaExpresionMapping();
            _context.Stack.Push(lambdaExpresionMapping);

            foreach (var parameterExpression in node.Parameters.Reverse())
            {
                while (elements[position].Kind != QueryPartKind.TableReference && elements[position].Kind != QueryPartKind.TableReferenceGroup)
                {
                    position++;
                }

                var reference = (ITableReferenceProvider)elements[position];

                if (!parameterExpression.Type.IsAssignableFrom(reference.Type))
                {
                    throw new InvalidOperationException();
                }

                var mapping = new ParameterExpresionMapping
                {
                    ParameterExpression = parameterExpression,
                    TableReferenceProvider = reference
                };

                _context.Stack.Push(mapping);
                position++;
            }

            Visit(node.Body);

            var items = _context.PopUntil(QueryPartKind.LambdaExpresionMapping);

            _context.Stack.Pop();

            foreach (var item in items)
            {
                if (item.Kind != QueryPartKind.ParameterExpresionMapping)
                {
                    _context.Stack.Push(item);
                }
            }

            return node;
        }

        /// <summary>
        /// Visits the children of the <see cref="T:System.Linq.Expressions.UnaryExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>
        /// The modified expression, if it or any subexpression was modified; otherwise, returns the original expression.
        /// </returns>
        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Not)
            {
                Visit(node.Operand);
                var part = _context.Stack.Pop();
                if (part.Kind == QueryPartKind.ColumnReference)
                {
                    var comparision = new Comparission
                    {
                        QueryText = string.Format("({0} = 0)", part.QueryText),
                        ParameterValues = part.ParameterValues
                    };

                    _context.Stack.Push(comparision);
                }
                else if (part.Kind == QueryPartKind.Comparision)
                {
                    var comparision = new Comparission
                    {
                        QueryText = string.Format("(NOT {0})", part.QueryText),
                        ParameterValues = part.ParameterValues
                    };

                    _context.Stack.Push(comparision);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            else
            {
                return base.VisitUnary(node);
            }

            return node;
        }

        /// <summary>
        /// Visits the children of the <see cref="T:System.Linq.Expressions.MemberExpression" />.
        /// </summary>
        /// <param name="node"> The expression to visit. </param>
        /// <returns> Original expression. </returns>
        /// <exception cref="System.NotSupportedException"> Thrown when query is not supported. </exception>
        protected override Expression VisitMember(
            MemberExpression node)
        {
            var handler = new MemberAccessHandler(_context);
            return handler.HandleMemberAccess(node);
        }

        private void HandleAggregationFunctions(MethodCallExpression node)
        {
            if (node.Method.Name == "Count")
            {
                var columnReference = new ColumnReference
                {
                    QueryText = "COUNT(*)",
                    Type = typeof(int),
                    IsAggregation = true
                };

                _context.Stack.Push(columnReference);
            }
        }

        protected override Expression VisitConditional(ConditionalExpression node)
        {
            Visit(node.Test);
            var test = _context.Stack.Pop();
            Visit(node.IfTrue);
            var ifTrue = _context.Stack.Pop();
            Visit(node.IfFalse);
            var ifFalse = _context.Stack.Pop();

            if (ifTrue.Kind == QueryPartKind.Parameter)
            {
                var ifTrueParameter = (QueryParameter)ifTrue;
                ifTrueParameter.IgnoreRelatedProperty = true;
            }

            if (ifFalse.Kind == QueryPartKind.Parameter)
            {
                var ifFalseParameter = (QueryParameter)ifFalse;
                ifFalseParameter.IgnoreRelatedProperty = true;
            }

            var query = new QueryArithmetic
            {
                ParameterValues = QueryParametersManager.CombineParameters(test, ifTrue, ifFalse),
                QueryText = string.Format(CultureInfo.InvariantCulture, "IIF({0},{1},{2})", test.QueryText, ifTrue.QueryText, ifFalse.QueryText)
            };

            _context.Stack.Push(query);
            return node;
        }

        /// <summary>
        /// Visits the children of the <see cref="T:System.Linq.Expressions.MemberInitExpression" />.
        /// </summary>
        /// <returns>
        /// The modified expression, if it or any subexpression was modified; otherwise, returns the
        /// original expression.
        /// </returns>
        /// <param name="node"> The expression to visit. </param>
        protected override Expression VisitMemberInit(MemberInitExpression node)
        {
            var columns = new List<ColumnReference>();
            foreach (var memberBinding in node.Bindings)
            {
                _context.Stack.Push(new QueryPartDescriptor { Kind = QueryPartKind.Argument });

                var assignment = (MemberAssignment)memberBinding;
                Visit(assignment.Expression);

                var argumentElements = _context.PopUntil(QueryPartKind.Argument).ToArray();
                _context.Stack.Pop();

                if (assignment.Expression.NodeType == ExpressionType.MemberInit || assignment.Expression.NodeType == ExpressionType.New)
                {
                    foreach (var column in argumentElements)
                    {
                        ColumnReference reference;
                        if (column.Kind == QueryPartKind.ColumnReference)
                        {
                            reference = (ColumnReference)column;
                        }
                        else
                        {
                            reference = ((IWrappableInColumn)column).WrapInColumnReference();
                        }

                        reference.Alias = string.Format(
                            CultureInfo.InvariantCulture,
                            "[{0}.{1}]",
                            memberBinding.Member.Name, Regex.Match(reference.Alias, @"\[(.*?)\]").Groups[1]);

                        reference.RelatedIntermediateTypeProperty = (PropertyInfo)memberBinding.Member;
                        columns.Add(reference);
                    }
                }
                else
                {
                    var column = argumentElements.Single();
                    ColumnReference reference;
                    if (column.Kind == QueryPartKind.ColumnReference)
                    {
                        reference = (ColumnReference)column;
                    }
                    else
                    {
                        reference = ((IWrappableInColumn)column).WrapInColumnReference();
                    }

                    reference.Alias = string.Format(
                        CultureInfo.InvariantCulture,
                        QueryTranslationContext.BraceTemplate,
                        memberBinding.Member.Name);
                    reference.QueryText = string.Format(
                        CultureInfo.InvariantCulture,
                        "{0}",
                        reference.QueryText);
                    reference.RelatedIntermediateTypeProperty = (PropertyInfo)memberBinding.Member;
                    columns.Add(reference);
                }
            }

            //var selectionList = (SelectionList)_context.TryFindElement(QueryPartKind.SelectionList);
            //selectionList.Type = node.Type;
            columns.Reverse();
            foreach (var columnReference in columns)
            {
                _context.Stack.Push(columnReference);
            }

            return node;
        }

        /// <summary>
        /// Visits the children of the <see cref="T:System.Linq.Expressions.MethodCallExpression" />.
        /// </summary>
        /// <param name="node"> The expression to visit. </param>
        /// <returns> Original expression. </returns>
        /// <exception cref="System.NotSupportedException"> Thrown when query is not supported. </exception>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType.IsGenericType &&
                node.Method.DeclaringType.GetGenericTypeDefinition() == typeof(IGroup<,>))
            {
                HandleAggregationFunctions(node);
            }
            else if (node.Method.DeclaringType == typeof(GroupingTableQuery))
            {
                var handler = new GroupingTableQueryMethodsHandler(_context);
                handler.HandleQueryableMethods(node);
            }
            else if (node.Method.DeclaringType == typeof(TableQuery)
                || node.Method.DeclaringType == typeof(Queryable))
            {
                var handler = new TableQueryMethodsHandler(_context);
                handler.HandleQueryableMethods(node);
            }
            else if (node.Method.DeclaringType == typeof(Enumerable))
            {
                var handler = new EnumerableMethodsHandler(_context);
                handler.HandleEnumerableMethods(node);
            }
            else if (node.Method.DeclaringType == typeof(AsynchronousTableQueryExtensions))
            {
                HandleQueryableExtensionsMethods(node);
            }
            else if (node.Method.DeclaringType == typeof(string))
            {
                var handler = new StringMethodsHandler(_context.Stack, this);
                handler.HandleStringMethods(node);
            }
            else if (typeof(IQueryable).IsAssignableFrom(node.Type))
            {
                var retriver = Expression.Lambda(node).Compile();
                var query = (IQueryable)retriver.DynamicInvoke();
                Visit(query.Expression);
            }
            else if (typeof(SqlMath).IsAssignableFrom(node.Method.DeclaringType))
            {
                HandleSqlMathMethods(node);
            }
            else
            {
                var handlerAttribute = node.Method.GetCustomAttribute<QueryTranslationHanderAttribute>();

                if (handlerAttribute == null)
                {
                    throw new NotSupportedException(node.ToString());
                }

                var handler = (IQueryTranslationHandler)ServiceLocator.Current.GetInstance(handlerAttribute.HanderType);
                handler.HandleExpression(node, _context);
            }

            return node;
        }

        private void HandleSqlMathMethods(MethodCallExpression node)
        {
            var expression = new QueryArithmetic();
            _context.Stack.Push(expression);
            if (node.Method.Name == "Avg")
            {
                Visit(node.Arguments[0]);
                var column = (ColumnReference)_context.Stack.Pop();

                expression.QueryText = $"AVG({column.QueryText})";
            }
            else if (node.Method.Name == "Count")
            {
                if (node.Arguments.Count == 0)
                {
                    expression.QueryText = "COUNT(*)";
                }
                else
                {
                    Visit(node.Arguments[0]);
                    var column = (ColumnReference)_context.Stack.Pop();

                    expression.QueryText = $"COUNT({column.QueryText})";
                }
            }
            else if (node.Method.Name == "AvgDistinct")
            {
                Visit(node.Arguments[0]);
                var column = (ColumnReference)_context.Stack.Pop();

                expression.QueryText = $"AVG(DISTINCT {column.QueryText})";
            }
            else if (node.Method.Name == "CountDistinct")
            {
                Visit(node.Arguments[0]);
                var column = (ColumnReference)_context.Stack.Pop();

                expression.QueryText = $"COUNT (DISTINCT {column.QueryText})";
            }
            else if (node.Method.Name == "Min")
            {
                Visit(node.Arguments[0]);
                var column = (ColumnReference)_context.Stack.Pop();

                expression.QueryText = $"MIN({column.QueryText})";
            }
            else if (node.Method.Name == "Max")
            {
                Visit(node.Arguments[0]);
                var column = (ColumnReference)_context.Stack.Pop();

                expression.QueryText = $"MAX({column.QueryText})";
            }
        }

        /// <summary>
        /// Visits the children of the <see cref="T:System.Linq.Expressions.NewExpression" />.
        /// </summary>
        /// <param name="node"> The expression to visit. </param>
        /// <returns> Original expression. </returns>
        protected override Expression VisitNew(NewExpression node)
        {
            var parameters = node.Constructor.GetParameters();

            var columns = new List<ColumnReference>();

            using (var enumerator = node.Arguments.GetEnumerator())
            {
                foreach (ParameterInfo parameter in parameters)
                {
                    enumerator.MoveNext();

                    _context.Stack.Push(new QueryPartDescriptor { Kind = QueryPartKind.Argument });

                    Visit(enumerator.Current);

                    var argumentElements = _context.PopUntil(QueryPartKind.Argument).ToArray();
                    _context.Stack.Pop();

                    if (enumerator.Current.NodeType == ExpressionType.MemberInit || enumerator.Current.NodeType == ExpressionType.New)
                    {
                        foreach (var column in argumentElements)
                        {
                            ColumnReference reference;
                            if (column.Kind == QueryPartKind.ColumnReference)
                            {
                                reference = (ColumnReference)column;
                            }
                            else
                            {
                                reference = ((IWrappableInColumn)column).WrapInColumnReference();
                            }

                            reference.Alias = string.Format(
                                CultureInfo.InvariantCulture,
                                "[{0}.{1}]",
                                parameter.Name, Regex.Match(reference.Alias, @"\[(.*?)\]").Groups[1]);

                            reference.RelatedIntermediateTypeProperty = (PropertyInfo)node.Members.Single(x => x.Name == parameter.Name);
                            columns.Add(reference);
                        }
                    }
                    else
                    {
                        ColumnReference reference;
                        var column = argumentElements.Single();
                        if (column.Kind == QueryPartKind.ColumnReference)
                        {
                            reference = (ColumnReference)column;
                        }
                        else
                        {
                            reference = ((IWrappableInColumn)column).WrapInColumnReference();
                        }

                        reference.Alias = string.Format(
                            CultureInfo.InvariantCulture,
                            QueryTranslationContext.BraceTemplate,
                            parameter.Name);

                        if (node.Members != null)
                        {
                            reference.RelatedIntermediateTypeProperty = (PropertyInfo)node.Members.Single(x => x.Name == parameter.Name);
                        }
                        else
                        {
                            reference.RelatedIntermediateTypeProperty = node.Type.GetProperties()
                                .Single(x => String.Compare(x.Name, parameter.Name, StringComparison.OrdinalIgnoreCase) == 0);
                        }

                        columns.Add(reference);
                    }
                }
            }

            var selectionList = (SelectionList)_context.TryFindElement(QueryPartKind.SelectionList);
            selectionList.Type = node.Type;
            columns.Reverse();
            foreach (var columnReference in columns)
            {
                _context.Stack.Push(columnReference);
            }

            return node;
        }

        /// <summary>
        /// Visits the children of the <see cref="T:System.Linq.Expressions.NewArrayExpression" />.
        /// </summary>
        /// <returns>
        /// The modified expression, if it or any subexpression was modified; otherwise, returns the
        /// original expression.
        /// </returns>
        /// <param name="node"> The expression to visit. </param>
        protected override Expression VisitNewArray(NewArrayExpression node)
        {
            var part = new QueryParameter
            {
                Expression = node,
                Alias = _context.AliasManager.GetParameterAlias()
            };
            _context.Stack.Push(part);
            return node;
        }

        /// <summary>
        /// Visits the <see cref="T:System.Linq.Expressions.ParameterExpression" />.
        /// </summary>
        /// <param name="node"> The expression to visit. </param>
        /// <returns> Original expression. </returns>
        protected override Expression VisitParameter(ParameterExpression node)
        {
            var mapping = _context.TryFindElement<ParameterExpresionMapping>(
                 QueryPartKind.ParameterExpresionMapping,
                 x => x.ParameterExpression == node);

            var tableReferenceProvider = mapping.TableReferenceProvider;
            _context.Stack.Push((QueryPartDescriptor)tableReferenceProvider);

            return node;
        }

        /// <summary>
        /// Handles the queryable extensions methods.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandleQueryableExtensionsMethods(MethodCallExpression node)
        {
            if (node.Method.Name == "UnionAllRecursive")
            {
                var handler = new UnionAllRecursivelyHanlder(_context);
                handler.HandleUnionAllRecursively(node);
            }
            else
            {
                throw new NotSupportedException(node.ToString());
            }
        }
    }
}