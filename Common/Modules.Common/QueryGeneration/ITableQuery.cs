using System;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ITableQuery
    {
        Type ElementType { get; }

        Expression Expression { get; }

        ITableQueryProvider Provider { get; }
    }
}