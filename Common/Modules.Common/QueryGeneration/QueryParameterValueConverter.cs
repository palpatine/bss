namespace Qdarc.Modules.Common.QueryGeneration
{
    public abstract class QueryParameterValueConverter<TConverted> : IQueryParameterValueConverter<TConverted>
    {
        public abstract object Convert(TConverted value);

        public object Convert(object value)
        {
            return Convert((TConverted) value);
        }

        public System.Type HandledType => typeof(TConverted);
    }
}