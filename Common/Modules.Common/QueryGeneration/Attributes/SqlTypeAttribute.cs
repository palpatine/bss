﻿using System;
using System.Data;

namespace Qdarc.Modules.Common.QueryGeneration.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SqlTypeAttribute : Attribute
    {
        public SqlTypeAttribute(SqlDbType type, bool isNullable)
        {
            Type = type;
            Capacity = -1;
            IsNullable = isNullable;
        }

        public SqlTypeAttribute(SqlDbType type, bool isNullable, int capacity)
        {
            Type = type;
            Capacity = capacity;
            IsNullable = isNullable;
        }

        public SqlTypeAttribute(SqlDbType type, bool isNullable, int capacity, int precision)
        {
            Type = type;
            Capacity = capacity;
            IsNullable = isNullable;
            Precision = precision;
        }

        public int Capacity { get; private set; }

        public bool IsNullable { get; private set; }

        public SqlDbType Type { get; private set; }

        public int Precision { get; private set; }

        public string GetSqlTypeDeclaration()
        {
            if (Type == SqlDbType.VarChar)
            {
                return $"varchar({Capacity})";
            }
            if (Type == SqlDbType.Int)
            {
                return "int";
            }
            if (Type == SqlDbType.DateTime)
            {
                return "DateTime";
            }
            if (Type == SqlDbType.NVarChar)
            {
                return $"nvarchar({(Capacity == 0 ? "MAX" : Capacity.ToString())})";
            }
            if (Type == SqlDbType.SmallDateTime)
            {
                return "SmallDateTime";
            }
            if (Type == SqlDbType.Bit)
            {
                return "Bit";
            }

            throw new NotImplementedException();
        }
    }
}