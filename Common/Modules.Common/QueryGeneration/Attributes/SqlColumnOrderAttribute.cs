﻿using System;

namespace Qdarc.Modules.Common.QueryGeneration.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SqlColumnOrderAttribute : Attribute
    {
        public SqlColumnOrderAttribute(int order)
        {
            Order = order;
        }

        public int Order { get; private set; }
    }
}