﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Qdarc.Modules.Common.QueryGeneration
{
    class QueryArithmeticHandler
    {
        private QueryTranslationContext _context;

        public QueryArithmeticHandler(QueryTranslationContext context)
        {
            _context = context;
        }

        public bool TryHandleArithmetic(
            BinaryExpression node,
            QueryPartDescriptor left,
            QueryPartDescriptor right)
        {
            if (node.NodeType == ExpressionType.And)
            {
                HandleArithmetic(left, right, "&");
                return true;
            }
            if (node.NodeType == ExpressionType.Or)
            {
                HandleArithmetic(left, right, "|");
                return true;
            }
            if (node.NodeType == ExpressionType.Add)
            {
                HandleArithmetic(left, right, "+");
                return true;
            }
            if (node.NodeType == ExpressionType.Subtract)
            {
                HandleArithmetic(left, right, "|");
                return true;
            }
            return false;
        }

        private void HandleArithmetic(
            QueryPartDescriptor left,
            QueryPartDescriptor right,
            string valueOperator)
        {
            if (left.Kind == QueryPartKind.Parameter)
            {
                ((QueryParameter) left).IgnoreRelatedProperty = true;
            }

            if (right.Kind == QueryPartKind.Parameter)
            {
                ((QueryParameter)right).IgnoreRelatedProperty = true;
            }

            var part = new QueryArithmetic
            {
                QueryText = string.Format(  
                    CultureInfo.InvariantCulture,
                    "({0} {1} {2})",
                    left.QueryText,
                    valueOperator,
                    right.QueryText),
                ParameterValues = QueryParametersManager.CombineParameters(left, right),
            };

            _context.Stack.Push(part);
        }
    }
}
