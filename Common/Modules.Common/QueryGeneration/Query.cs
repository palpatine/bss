using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Query.
    /// </summary>
    public class Query : QueryPartDescriptor
    {
        /// <summary>
        ///     Gets or sets the alias.
        /// </summary>
        /// <value> The alias. </value>
        public string Alias { get; set; }

        /// <summary>
        ///     Gets or sets from.
        /// </summary>
        /// <value> From. </value>
        public FromClause From { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is distinct.
        /// </summary>
        /// <value> <c>true</c> if this instance is distinct; otherwise, <c>false</c>. </value>
        public bool IsDistinct { get; set; }

        /// <summary>
        ///     Gets a value indicating whether this instance is simple sub query.
        /// </summary>
        /// <value> <c>true</c> if this instance is simple sub query; otherwise, <c>false</c>. </value>
        public bool IsSimpleSubQuery => !IsDistinct
                                        && Order == null
                                        && Top == null
                                        && (SelectionList == null || SelectionList.IsJoinSelectionList);

        /// <summary>
        ///     Gets a value indicating whether this instance is table reference.
        /// </summary>
        /// <value> <c>true</c> if this instance is table reference; otherwise, <c>false</c>. </value>
        public bool IsSqlTableReference => !IsDistinct
                                           && Where == null
                                           &&
                                           (SelectionList == null ||
                                            (From.References.Count() == 1 &&
                                             (From.References.Single().Alias != null && SelectionList.QueryText == From.References.Single().Alias + ".*"
                                              || From.References.Single().Alias == null && SelectionList.QueryText == "*")));

        /// <summary>
        ///     Gets or sets the kind.
        /// </summary>
        /// <value> The kind. </value>
        public override QueryPartKind Kind
        {
            get { return QueryPartKind.Query; }

            set { }
        }

        /// <summary>
        ///     Gets or sets the order.
        /// </summary>
        /// <value> The order. </value>
        public QueryPartDescriptor Order { get; set; }

        /// <summary>
        ///     Gets or sets the selection list.
        /// </summary>
        /// <value> The selection list. </value>
        public SelectionList SelectionList { get; set; }

        /// <summary>
        ///     Gets or sets the temporary views.
        /// </summary>
        /// <value> The temporary views. </value>
        public IEnumerable<QueryPartDescriptor> TemporaryViews { get; set; }

        /// <summary>
        ///     Gets or sets the top.
        /// </summary>
        /// <value> The top. </value>
        public QueryPartDescriptor Top { get; set; }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        /// <value> The type. </value>
        public Type Type { get; set; }

        /// <summary>
        ///     Gets or sets the where.
        /// </summary>
        /// <value> The where. </value>
        public QueryPartDescriptor Where { get; set; }

        public GroupBySelectionList GroupBy { get; set; }

        public QueryPartDescriptor Having { get; set; }

        /// <summary>
        ///     Wraps the in table reference.
        /// </summary>
        /// <param name="aliasManager"> The alias manager. </param>
        /// <returns> Table reference. </returns>
        public TableReference WrapInTableReference(QueryElementAliasManager aliasManager)
        {
            TableReference tableReference;
            if (IsSqlTableReference)
            {
                tableReference = From.References.Single();
            }
            else
            {
                tableReference = new TableReference
                {
                    Alias = string.Format(CultureInfo.InvariantCulture, "[{0}]", aliasManager.GetAlias("Join")),
                    ParameterValues = ParameterValues,
                    QueryText = string.Format(CultureInfo.InvariantCulture, "({0})", QueryText),
                    Type = Type,
                };

                if (SelectionList != null && SelectionList.Columns != null && SelectionList.Columns.Count() == 1)
                {
                    ColumnReference singleColumn = SelectionList.Columns.Single();
                    tableReference.IsSingleColumn = true;
                    tableReference.Column = new ColumnReference
                    {
                        Alias = singleColumn.Alias,
                        QueryText =
                            string.Format(CultureInfo.InvariantCulture, "{0}.{1}", tableReference.Alias,
                                singleColumn.Alias),
                        TableReference = tableReference,
                        Type = singleColumn.Type
                    };
                }
            }

            return tableReference;
        }

        public override string ToString()
        {
            if (ParameterValues != null && ParameterValues.Any())
            {
                return string.Format("{0} {1}", FullQueryText,
                    string.Join(", ",
                        ParameterValues
                            .Select(x => string.Format(CultureInfo.InvariantCulture, "{0}:{1}", x.Item1, x.Item2))));
            }
            else
            {
                return FullQueryText;
            }
        }

        public QueryPartDescriptor Paging { get; set; }

        public string QueryPrefix { get; set; }

        public string FullQueryText
        {
            get
            {
                if (QueryPrefix != null)
                    return string.Format("{0}{1}{2}", QueryPrefix, Environment.NewLine, QueryText);
                else
                    return QueryText;
            }
        }
    }
}