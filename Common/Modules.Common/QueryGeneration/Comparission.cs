using System;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public class Comparission : QueryPartDescriptor, IWrappableInColumn
    {

        public override QueryPartKind Kind
        {
            get { return QueryPartKind.Comparision; }
            set { throw new InvalidOperationException(); }
        }

        public ColumnReference WrapInColumnReference()
        {
            return new ColumnReference
            {
                WrappedPart = this,
                QueryText = string.Format("CAST (IIF({0}, 1, 0) AS BIT)", QueryText)
            };
        }
    }
}