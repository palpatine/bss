﻿using System.Data.SqlClient;

namespace Qdarc.Modules.Common.QueryGeneration
{
    class SqlParameterWrapper : ISqlParameter
    {
        private readonly SqlParameter _sqlParameter;

        public SqlParameterWrapper(SqlParameter sqlParameter)
        {
            _sqlParameter = sqlParameter;
        }

        public object Value
        {
            get { return _sqlParameter.Value; }
            set { _sqlParameter.Value = value; }
        }

        public string TypeName
        {
            get { return _sqlParameter.TypeName; }
            set { _sqlParameter.TypeName = value; }
        }

        internal SqlParameter SqlParameter => _sqlParameter;
    }
}