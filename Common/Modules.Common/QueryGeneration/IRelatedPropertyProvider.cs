using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IRelatedPropertyProvider
    {
        bool ShouldIgnore { get; }

        PropertyInfo RelatedEntityProperty { get; set; }
    }
}