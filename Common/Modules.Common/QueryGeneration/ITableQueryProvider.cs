using System;
using System.Linq;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ITableQueryProvider
    {
        ITableQuery<TElement> CreateQuery<TElement>(Expression expression);

        IGroupingTableQuery<TKey, TSource, TProjection> CreateGroupingQuery<TKey, TSource, TProjection>(
            Expression expression);

        object Execute(Expression expression);

        TResult Execute<TResult>(Expression expression);

        IQueryProvider AsQueryProvider();

        string GetQueryText(Expression expression);

        string GetDeleteQueryText(Expression expression);

        Query ParseDeleteExpression(Expression expression);

        Query ParseQueryExpression(Expression expression);
    }
}