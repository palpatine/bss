﻿using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IQueryTranslationHandler
    {
        void HandleExpression(Expression node, QueryTranslationContext context);
    }
}