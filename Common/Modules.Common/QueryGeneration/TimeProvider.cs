using System;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Time provider.
    /// </summary>
    internal sealed class TimeProvider : ITimeProvider
    {
        /// <summary>
        ///     Gets the now.
        /// </summary>
        /// <value> The now. </value>
        public DateTime Now => DateTime.UtcNow;
    }
}