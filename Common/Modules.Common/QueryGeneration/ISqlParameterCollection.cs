﻿using System.Data;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ISqlParameterCollection
    {
        ISqlParameter AddWithValue(string name, object value);

        ISqlParameter Add(string name, SqlDbType sqlDbType);

        ISqlParameter Add(string name, SqlDbType sqlDbType, int size);

        ISqlParameter this[string name] { get; set; }
    }
}