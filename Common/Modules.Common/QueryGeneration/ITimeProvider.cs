using System;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Time provider.
    /// </summary>
    public interface ITimeProvider
    {
        /// <summary>
        ///     Gets the now.
        /// </summary>
        /// <value>
        ///     The now.
        /// </value>
        DateTime Now { get; }
    }
}