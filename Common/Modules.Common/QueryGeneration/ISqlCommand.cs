﻿using System.Threading.Tasks;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ISqlCommand
    {
        int ExecuteNonQuery();

        object ExecuteScalar();

        ISqlParameterCollection Parameters { get; }

        ISqlDataReader ExecuteReader();

        Task<int> ExecuteNonQueryAsync();

        Task<ISqlDataReader> ExecuteReaderAsync();

        Task<object> ExecuteScalarAsync();
    }
}