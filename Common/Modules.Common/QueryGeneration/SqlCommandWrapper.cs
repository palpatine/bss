using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public class SqlCommandWrapper : ISqlCommand
    {
        private const string NoLockError = "BSS001:";
        private readonly SqlCommand _sqlCommand;

        public SqlCommandWrapper(SqlCommand sqlCommand)
        {
            _sqlCommand = sqlCommand;
        }

        public ISqlParameterCollection Parameters => new SqlParameterCollectionWrapper(_sqlCommand.Parameters);

        public int ExecuteNonQuery()
        {
            try
            {
                return _sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                ProcessSqlException(e);
                throw;
            }
        }

        public async Task<int> ExecuteNonQueryAsync()
        {
            try
            {
                return await _sqlCommand.ExecuteNonQueryAsync();
            }
            catch (SqlException e)
            {
                ProcessSqlException(e);
                throw;
            }
        }

        public ISqlDataReader ExecuteReader()
        {
            return new SqlDataReaderWrapper(_sqlCommand.ExecuteReader());
        }

        public async Task<ISqlDataReader> ExecuteReaderAsync()
        {
            try
            {
                return new SqlDataReaderWrapper(await _sqlCommand.ExecuteReaderAsync());
            }
            catch (SqlException e)
            {
                ProcessSqlException(e);
                throw;
            }
        }

        public object ExecuteScalar()
        {
            try
            {
                return _sqlCommand.ExecuteScalar();
            }
            catch (SqlException e)
            {
                ProcessSqlException(e);
                throw;
            }
        }

        public async Task<object> ExecuteScalarAsync()
        {
            try
            {
                return await _sqlCommand.ExecuteScalarAsync();
            }
            catch (SqlException e)
            {
                ProcessSqlException(e);
                throw;
            }
        }

        private void ProcessSqlException(SqlException sqlException)
        {
            if (sqlException.Message.StartsWith(NoLockError))
            {
                throw new SqlValidationException("NoLockError", sqlException.Message, sqlException);
            }
        }
    }
}