﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ISqlDataReader : IDisposable
    {
        bool Read();

        object this[int i] { get; }

        object this[string name] { get; }

        Task<bool> ReadAsync();

        DataTable GetSchemaTable();
    }
}