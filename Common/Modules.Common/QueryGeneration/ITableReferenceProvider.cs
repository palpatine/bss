using System;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface ITableReferenceProvider
    {
        Type Type { get; }

        QueryPartKind Kind { get; }
    }
}