using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IFunctionQuery
    {
        Type ElementType { get; }

        string FunctionName { get; }

        IEnumerable<ConstantExpression> Arguments { get; }
    }
}