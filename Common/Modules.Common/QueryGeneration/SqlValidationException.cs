using System;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace Qdarc.Modules.Common.QueryGeneration
{
    [Serializable]
    public class SqlValidationException : Exception
    {
        public string ErrorCode { get; private set; }

        public SqlValidationException(string errorCode, string message, SqlException innerException)
            : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        protected SqlValidationException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}