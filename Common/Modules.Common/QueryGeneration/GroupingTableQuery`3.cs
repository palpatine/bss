﻿using System;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Grouping table query.
    /// </summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <typeparam name="TSource">The type of the source.</typeparam>
    /// <typeparam name="TProjection">The type of the projection.</typeparam>
    internal class GroupingTableQuery<TKey, TSource, TProjection> : IGroupingTableQuery<TKey, TSource, TProjection>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="GroupingTableQuery{TKey, TSource, TProjection}" /> class.
        /// </summary>
        /// <param name="groupingTableQueryProvider">The grouping table query provider.</param>
        /// <param name="expression">The expression.</param>
        public GroupingTableQuery(
            ITableQueryProvider groupingTableQueryProvider,
            Expression expression)
        {
            Provider = groupingTableQueryProvider;
            Expression = expression;
        }

        /// <summary>
        ///     Gets the type of the element(s) that are returned when the expression tree associated
        ///     with this instance of <see cref="T:System.Linq.IQueryable" /> is executed.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.Type" /> that represents the type of the element(s) that are
        ///     returned when the expression tree associated with this object is executed.
        /// </returns>
        public Type ElementType => typeof(TProjection);

         public Expression Expression { get; }

        public ITableQueryProvider Provider { get; }

        public override string ToString()
        {
            return Provider.GetQueryText(Expression);
        }
    }
}