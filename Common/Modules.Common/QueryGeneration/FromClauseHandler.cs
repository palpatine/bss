using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Form clause handler.
    /// </summary>
    public sealed class FromClauseHandler
    {
        /// <summary>
        ///     The alias manager.
        /// </summary>
        private readonly QueryElementAliasManager _aliasManager;

        /// <summary>
        ///     The stack.
        /// </summary>
        private readonly Stack<QueryPartDescriptor> _stack;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FromClauseHandler" /> class.
        /// </summary>
        /// <param name="context"> The context. </param>
        public FromClauseHandler(QueryTranslationContext context)
        {
            _stack = context.Stack;
            _aliasManager = context.AliasManager;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FromClauseHandler" /> class.
        /// </summary>
        /// <param name="stack">The stack.</param>
        /// <param name="aliasManager">The alias manager.</param>
        internal FromClauseHandler(
            Stack<QueryPartDescriptor> stack,
            QueryElementAliasManager aliasManager)
        {
            _stack = stack;
            _aliasManager = aliasManager;
        }

        /// <summary>
        /// Adds to from.
        /// </summary>
        /// <param name="firstReferenceGroup">The first reference group.</param>
        /// <param name="secondTableReference">The second table reference.</param>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <param name="firstTableFilter">The first table filter.</param>
        /// <param name="secondTableFilter">The second table filter.</param>
        /// <param name="isLeftJoin">if set to <c>true</c> [is left join].</param>
        /// <exception cref="System.InvalidOperationException">
        /// </exception>
        public void AddToFrom(
            TableReferenceGroup firstReferenceGroup,
            TableReference secondTableReference,
            ColumnReference first,
            ColumnReference second,
            QueryPartDescriptor firstTableFilter = null,
            QueryPartDescriptor secondTableFilter = null,
            bool isLeftJoin = false)
        {
            var sqlConnector = isLeftJoin ? "LEFT JOIN" : "JOIN";
            if (first.QueryText == null
                || second.QueryText == null)
            {
                throw new InvalidOperationException();
            }

            var from = (FromClause)_stack.First(x => x.Kind == QueryPartKind.From);
            string text;

            if (firstReferenceGroup.References.Count() > 1)
            {
                if (from.References == null)
                {
                    text = string.Format(
                        CultureInfo.InvariantCulture,
                        "{0}" + Environment.NewLine + "{1} {2} AS {3} ON ({4} = {5})",
                        firstReferenceGroup.QueryText,
                        sqlConnector,
                        secondTableReference.QueryText,
                        secondTableReference.Alias,
                        first.QueryText,
                        second.QueryText);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            else
            {
                var firstReference = firstReferenceGroup.References.Single();
                if (from.References == null
                    || (@from.References.All(x => x != firstReference) && @from.References.All(x => x != secondTableReference)))
                {
                    text = string.Format(
                        CultureInfo.InvariantCulture,
                        "{0} AS {1}" + Environment.NewLine + "{2} {3} AS {4} ON ({5} = {6})",
                        firstReference.QueryText,
                        firstReference.Alias,
                        sqlConnector,
                        secondTableReference.QueryText,
                        secondTableReference.Alias,
                        first.QueryText,
                        second.QueryText);
                }
                else if (from.References.All(x => x != firstReference))
                {
                    text = string.Format(
                        CultureInfo.InvariantCulture,
                        "{0} AS {1} ON ({2} = {3})",
                        firstReference.QueryText,
                        firstReference.Alias,
                        first.QueryText,
                        second.QueryText);
                }
                else if (from.References.All(x => x != secondTableReference))
                {
                    text = string.Format(
                        CultureInfo.InvariantCulture,
                        "{0} AS {1} ON ({2} = {3})",
                        secondTableReference.QueryText,
                        secondTableReference.Alias,
                        first.QueryText,
                        second.QueryText);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }


            if (secondTableFilter != null)
            {
                text = string.Format(CultureInfo.InvariantCulture, "{0} AND {1}", text, secondTableFilter.QueryText);
            }

            if (firstTableFilter != null)
            {
                _stack.Push(firstTableFilter);
            }

            if (from.References != null)
            {
                from.References =
                    from.References.Concat(firstReferenceGroup.References).Concat(new[] { secondTableReference }).Distinct().ToArray();
                from.QueryText = string.Format(CultureInfo.InvariantCulture, "{0}" + Environment.NewLine + "{1} {2}",
                    from.QueryText, sqlConnector, text);
            }
            else
            {
                from.References = firstReferenceGroup.References.Concat(new[] { secondTableReference }).Distinct().ToArray();
                from.QueryText = text;
            }

            from.ParameterValues = QueryParametersManager.CombineParameters(firstReferenceGroup, secondTableReference);
        }

        /// <summary>
        ///     Adds to from. This method can be used only for first table reference in query.
        /// </summary>
        /// <param name="tableReference"> The table reference. </param>
        public void AddToFrom(
            TableReference tableReference)
        {
            var from = (FromClause)_stack.First(x => x.Kind == QueryPartKind.From);
            if (from.References != null && from.References.Any())
            {
                throw new InvalidOperationException();
            }
            if (_aliasManager.AreTableAliasesFormbidden || tableReference.IsValues)
            {
                @from.References = new[] { tableReference };
                @from.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0}",
                    tableReference.QueryText);
            }
            else
            {
                @from.References = new[] { tableReference };
                @from.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} AS {1}",
                    tableReference.QueryText,
                    tableReference.Alias);
            }

            from.ParameterValues = tableReference.ParameterValues;
        }
    }
}