namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IQueryParameterValueConverter<in TConverted> : IQueryParameterValueConverter
    {
        object Convert(TConverted value);
    }
}