using System;
using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public class ColumnReference : QueryPartDescriptor, IRelatedPropertyProvider
    {
        public ColumnReference()
        {
        }

        public string Alias { get; set; }

        bool IRelatedPropertyProvider.ShouldIgnore => IsAggregation;

        public bool IsAggregation { get; set; }

        public override QueryPartKind Kind
        {
            get { return QueryPartKind.ColumnReference; }

            set { }
        }

        public bool IsStarReference { get; set; }

        public PropertyInfo RelatedEntityProperty { get; set; }

        public PropertyInfo RelatedIntermediateTypeProperty { get; set; }

        public TableReference TableReference { get; set; }

        public TableReferenceGroup TableReferenceGroup { get; set; }

        public Type Type { get; set; }

        public QueryPartDescriptor WrappedPart { get; set; }

        internal ColumnReference Clone()
        {
            return new ColumnReference
            {
                IsStarReference =  IsStarReference,
                IsAggregation = IsAggregation,
                ParameterValues = ParameterValues,
                QueryText = QueryText,
                RelatedEntityProperty = RelatedEntityProperty,
                RelatedIntermediateTypeProperty = RelatedIntermediateTypeProperty,
                Type = Type,
                TableReference = TableReference,
                WrappedPart = WrappedPart,
            };
        }
    }
}