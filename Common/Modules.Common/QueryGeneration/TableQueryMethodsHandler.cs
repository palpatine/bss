using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    /// Querable methos handler.
    /// </summary>
    internal sealed class TableQueryMethodsHandler
    {
        private readonly QueryTranslationContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="TableQueryMethodsHandler" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public TableQueryMethodsHandler(
            QueryTranslationContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Handles the queryable methods.
        /// </summary>
        /// <param name="node"> The node. </param>
        /// <exception cref="System.NotSupportedException"> Unspported query. </exception>
        public void HandleQueryableMethods(MethodCallExpression node)
        {
            if (node.Method.Name == "AsQueryable")
            {
                _context.Visitor.Visit(node.Arguments[0]);
            }
            else if (node.Method.Name == "Select")
            {
                _context.Visitor.Visit(node.Arguments[0]);
                HandleProjection(node);
            }
            else if (node.Method.Name == "Distinct")
            {
                _context.Visitor.Visit(node.Arguments[0]);
                var part = new QueryPartDescriptor
                {
                    Kind = QueryPartKind.Distinct
                };
                _context.Stack.Push(part);
            }
            else if (node.Method.Name == "Where")
            {
                _context.Visitor.Visit(node.Arguments[0]);
                var handler = new FilteringHandler(_context);
                handler.HandleFiltering(node.Arguments[1]);
            }
            else if (node.Method.Name == "OrderBy"
                     || node.Method.Name == "OrderByDescending"
                     || node.Method.Name == "ThenBy"
                     || node.Method.Name == "ThenByDescending")
            {
                _context.Visitor.Visit(node.Arguments[0]);
                HandleOrdering(node);
            }
            else if (node.Method.Name == "Join")
            {
                HandleJoin(node);
            }
            else if (node.Method.Name == "LeftJoin")
            {
                HandleLeftJoin(node);
            }
            else if (node.Method.Name == "GroupBy")
            {
                HandleGroupBy(node);
            }
            else if (node.Method.Name == "Count" || node.Method.Name == "LongCount")
            {
                HandleCount(node);
            }
            else if (node.Method.Name == "FirstOrDefault"
                     || node.Method.Name == "First")
            {
                HandleFirst(node);
            }
            else if (node.Method.Name == "Take")
            {
                HanldeTake(node);
            }
            else if (node.Method.Name == "Skip")
            {
                HanldeSkip(node);
            }
            else if (node.Method.Name == "Any")
            {
                HandeAny(node);
            }
            else if (node.Method.Name == "Contains")
            {
                HandleContains(node);
            }
            else if (node.Method.Name == "PageInner")
            {
                HandlePaging(node);
            }
            else if (node.Method.Name == "Single" || node.Method.Name == "SingleOrDefault")
            {
                HandeSingle(node);
            }
            else
            {
                throw new NotSupportedException(node.ToString());
            }
        }

        public static bool IsSetOperator(MethodCallExpression node)
        {
            return (node.Method.DeclaringType == typeof(TableQuery) || node.Method.DeclaringType == typeof(Queryable))
                && (node.Method.Name == "UnionAll"
                    || node.Method.Name == "Union"
                    || node.Method.Name == "Intersect"
                    || node.Method.Name == "Except");
        }

        public void HandleSetOperator(MethodCallExpression node)
        {
            if (node.Method.Name == "UnionAll")
            {
                HandleSetOperator(node, "UNION ALL");
            }
            else if (node.Method.Name == "Union")
            {
                HandleSetOperator(node, "UNION");
            }
            else if (node.Method.Name == "Intersect")
            {
                HandleSetOperator(node, "INTERSECT");
            }
            else if (node.Method.Name == "Except")
            {
                HandleSetOperator(node, "EXCEPT");
            }
        }

        private void HandleSetOperator(MethodCallExpression node, string setOperator)
        {
            var firstQueryHanlder = new QueryHandler(_context);
            firstQueryHanlder.VisitQuery(node.Arguments[0]);
            var first = (Query)_context.Stack.Pop();

            var secondQueryHanlder = new QueryHandler(_context);
            secondQueryHanlder.VisitQuery(node.Arguments[1]);
            var second = (Query)_context.Stack.Pop();

            var alias = _context.AliasManager.GetAliasForTable("SetOperator");
            var part = (Query)_context.Stack.Peek();

            part.Type = first.Type;
            part.Alias = alias == null ? null : string.Format(CultureInfo.InvariantCulture, QueryTranslationContext.BraceTemplate, alias);
            part.QueryText = string.Format("({0}){3}{2}{3}({1})", first.QueryText, second.QueryText, setOperator, Environment.NewLine);
            part.ParameterValues = QueryParametersManager.CombineParameters(first, second);
        }

        private void HandlePaging(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);
            _context.Visitor.Visit(node.Arguments[1]);
            var skip = (QueryParameter)_context.Stack.Pop();
            skip.IgnoreRelatedProperty = true;
            _context.Visitor.Visit(node.Arguments[2]);
            var limit = (QueryParameter)_context.Stack.Pop();
            limit.IgnoreRelatedProperty = true;

            var part = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Paging,
                QueryText =
                    string.Format(
                    CultureInfo.InvariantCulture,
                    "OFFSET {0} Rows FETCH NEXT {1} ROWS ONLY",
                    skip.ParameterValues.Single().Item1,
                    limit.ParameterValues.Single().Item1),
                ParameterValues = QueryParametersManager.CombineParameters(skip, limit)
            };
            _context.Stack.Push(part);
        }

        private void HandleGroupBy(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);
            var groupBySelectionList = new GroupBySelectionList();
            _context.Stack.Push(groupBySelectionList);
            _context.Visitor.Visit(node.Arguments[1]);

            var references = new List<ColumnReference>();

            while (_context.Stack.Peek() != groupBySelectionList)
            {
                references.Add((ColumnReference)_context.Stack.Pop());
            }

            references.Reverse();

            groupBySelectionList.Columns = references;
            groupBySelectionList.Type = node.Arguments[1].Type;
            groupBySelectionList.QueryText = string.Join(
                ", ",
                references.Select(x => x.QueryText));

            var handler = new SelectionListHandler(_context);
            handler.HandleSelectionList(node.Arguments[2]);
        }

        /// <summary>
        /// Handes any.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandeAny(MethodCallExpression node)
        {
            var queryHandler = new QueryHandler(_context);
            queryHandler.VisitQuery(node.Arguments[0], node.Arguments.ElementAtOrDefault(1));
            var query = (Query)_context.Stack.Pop();

            if (query.TemporaryViews != null)
            {
                foreach (var queryPartDescriptor in query.TemporaryViews)
                {
                    _context.Stack.Push(queryPartDescriptor);
                }
            }

            var selectionList = new SelectionList
            {
                QueryText = string.Format("CASE WHEN EXISTS ({0}{1}){0}THEN CAST( 1 As bit) ELSE CAST( 0 As bit) END", Environment.NewLine, query.QueryText),
                ParameterValues = query.ParameterValues
            };

            _context.Stack.Push(selectionList);
        }

        /// <summary>
        /// Handes single.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandeSingle(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);

            if (node.Arguments.Count > 1)
            {
                var handler = new FilteringHandler(_context);
                handler.HandleFiltering(node.Arguments[1]);
            }

            var top = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Top,
                QueryText = "2"
            };
            _context.Stack.Push(top);
        }

        /// <summary>
        /// Handles the contains.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandleContains(MethodCallExpression node)
        {
            var retriver = Expression.Lambda(node.Arguments[0]).Compile();
            var query = (ITableQuery)retriver.DynamicInvoke();
            var queryHanlder = new QueryHandler(_context);
            queryHanlder.VisitQuery(query.Expression);
            var subQuery = (Query)_context.Stack.Pop();

            if (subQuery.TemporaryViews != null)
            {
                foreach (var queryPartDescriptor in subQuery.TemporaryViews)
                {
                    _context.Stack.Push(queryPartDescriptor);
                }
            }

            _context.Visitor.Visit(node.Arguments[1]);
            var columnReference = _context.Stack.Pop();
            var comparision = new Comparission
            {
                QueryText =
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "{0} IN ({1})",
                        columnReference.QueryText,
                        subQuery.QueryText),
                ParameterValues = QueryParametersManager.CombineParameters(subQuery, columnReference)
            };
            _context.Stack.Push(comparision);
        }

        /// <summary>
        /// Handles the count.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandleCount(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);
            if (node.Method.GetParameters().Length == 2)
            {
                var handler = new FilteringHandler(_context);
                handler.HandleFiltering(node.Arguments[1]);
            }

            var selectionList = _context.TryFindSelectionList();
            if (selectionList == null)
            {
                selectionList = new SelectionList();
                _context.Stack.Push(selectionList);
            }

            selectionList.Type = null;
            selectionList.ParameterValues = null;
            selectionList.QueryText = "Count(*) AS [RowCount]";

            selectionList.Columns = new[]
                {
                    new ColumnReference
                    {
                        Alias = "[RowCount]",
                    }
            };
        }

        /// <summary>
        /// Handles the first.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandleFirst(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);
            var part = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Top,
                QueryText = "1"
            };
            _context.Stack.Push(part);
        }

        /// <summary>
        /// Handles the join.
        /// </summary>
        /// <param name="node"> The node. </param>
        /// <exception cref="System.InvalidOperationException"> Unsuported query. </exception>
        private void HandleLeftJoin(MethodCallExpression node)
        {
            HandleJoin(node, isLeft: true);
        }

        /// <summary>
        /// Handles the join.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="isLeft">if set to <c>true</c> [is left].</param>
        /// <exception cref="System.InvalidOperationException">Unsuported query.</exception>
        private void HandleJoin(MethodCallExpression node, bool isLeft = false)
        {
            var join = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Join
            };
            _context.Stack.Push(join);

            QueryPartDescriptor firstTableFilter;
            var firstTableReference = HandleFirstJoinedQuery(node.Arguments[0], out firstTableFilter);
            QueryPartDescriptor secondTableFilter;
            var secondTableReferenceGroup = HandleSecondJoinedQuery(node.Arguments[1], out secondTableFilter);
            if (secondTableReferenceGroup.References.Count() != 1)
            {
                throw new InvalidOperationException();
            }
            var secondTableReference = secondTableReferenceGroup.References.Single();
            secondTableReference.ParameterValues = secondTableReferenceGroup.ParameterValues;

            _context.Stack.Push(firstTableReference);

            firstTableReference.HandleTransparently = true;
            _context.Visitor.Visit(node.Arguments[2]);
            firstTableReference.HandleTransparently = false;

            var first = _context.Stack.Pop();
            ColumnReference firstColumnReference;

            if (first.Kind == QueryPartKind.ColumnReference)
            {
                firstColumnReference = (ColumnReference)first;
            }
            else if (first.Kind == QueryPartKind.TableReference || first.Kind == QueryPartKind.TableReferenceGroup)
            {
                var reference = (ITableReferenceProvider)first;
                firstColumnReference = ((IColumnReferenceProvider)first).GenerateColumnReference("[Id]",
                    reference.Type.GetProperty("Id"));
            }
            else
            {
                firstColumnReference = ((IWrappableInColumn)first).WrapInColumnReference();
            }

            if (firstColumnReference.IsStarReference)
            {
                if (firstColumnReference.TableReference != null)
                {
                    firstColumnReference =
                        firstColumnReference.TableReference.GenerateColumnReference("Id",
                            firstColumnReference.TableReference.Type.GetProperty("Id"));
                }
                else
                {
                    firstColumnReference =
                        firstColumnReference.TableReferenceGroup.GenerateColumnReference("Id",
                            firstColumnReference.TableReferenceGroup.Type.GetProperty("Id"));
                }
            }

            _context.Stack.Pop();

            _context.Stack.Push(secondTableReference);
            _context.Visitor.Visit(node.Arguments[3]);
            var second = _context.Stack.Pop();
            ColumnReference secondColumnReference;

            if (second.Kind == QueryPartKind.ColumnReference)
            {
                secondColumnReference = (ColumnReference)second;
            }
            else if (second.Kind == QueryPartKind.TableReference || second.Kind == QueryPartKind.TableReferenceGroup)
            {
                var reference = (ITableReferenceProvider)second;
                secondColumnReference = ((IColumnReferenceProvider)second).GenerateColumnReference("[Id]",
                    reference.Type.GetProperty("Id"));
            }
            else
            {
                secondColumnReference = ((IWrappableInColumn)second).WrapInColumnReference();
            }

            if (secondColumnReference.IsStarReference)
            {
                if (secondColumnReference.TableReference != null)
                {
                    secondColumnReference =
                        secondColumnReference.TableReference.GenerateColumnReference("Id",
                            secondColumnReference.TableReference.Type.GetProperty("Id"));
                }
                else
                {
                    secondColumnReference =
                        secondColumnReference.TableReferenceGroup.GenerateColumnReference("Id",
                            secondColumnReference.TableReferenceGroup.Type.GetProperty("Id"));
                }
            }

            _context.Stack.Pop();

            var fromClauseHandler = new FromClauseHandler(_context);
            fromClauseHandler.AddToFrom(
                firstTableReference,
                secondTableReference,
                firstColumnReference,
                secondColumnReference,
                firstTableFilter,
                secondTableFilter,
                isLeft);

            firstTableReference.HandleTransparently = true;
            HandleJoinedTablesSelectionList(node, firstTableReference, secondTableReference);
            firstTableReference.HandleTransparently = false;

            var parts = _context.PopUntil(QueryPartKind.Join).ToArray();

            _context.Stack.Pop();

            foreach (var part in parts.Reverse())
            {
                _context.Stack.Push(part);
            }
        }

        /// <summary>
        /// Handles the joined query.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="tableFilter">The table filter.</param>
        /// <returns>
        /// Table reference pointing to joined table or subquery.
        /// </returns>
        private TableReferenceGroup HandleSecondJoinedQuery(
            Expression node,
            out QueryPartDescriptor tableFilter)
        {
            var queryHanlder = new QueryHandler(_context);
            queryHanlder.VisitQuery(node);
            var query = (Query)_context.Stack.Pop();


            if (query.IsSimpleSubQuery && query.From.References.Count() == 1)
            {
                return SimplifyToTableReference(
                    query,
                    out tableFilter);
            }

            tableFilter = null;
            return WrapInTableReference(query);
        }

        private TableReferenceGroup HandleFirstJoinedQuery(
           Expression node,
           out QueryPartDescriptor tableFilter)
        {
            var queryHanlder = new QueryHandler(_context);
            queryHanlder.VisitQuery(node);
            var query = (Query)_context.Stack.Pop();


            if (query.IsSimpleSubQuery)
            {
                return SimplifyToTableReference(
                    query,
                    out tableFilter);
            }

            tableFilter = null;
            return WrapInTableReference(query);
        }

        private TableReferenceGroup WrapInTableReference(Query query)
        {
            var reference = query.WrapInTableReference(_context.AliasManager);
            if (query.TemporaryViews != null)
            {
                foreach (var view in query.TemporaryViews)
                {
                    _context.Stack.Push(view);
                }
            }
            return reference.ToReferenceGroup();
        }

        private TableReferenceGroup SimplifyToTableReference(
            Query query,
            out QueryPartDescriptor tableFilter)
        {
            var references = query.From.References.ToArray();
            var tableReferenceGroup = new TableReferenceGroup
            {
                References = references,
                ParameterValues = query.ParameterValues,
                QueryText = query.From.QueryText,
                Type = query.Type,
                SelectionList = query.SelectionList
            };
            tableFilter = query.Where;

            if (query.TemporaryViews != null)
            {
                foreach (var view in query.TemporaryViews)
                {
                    _context.Stack.Push(view);
                }
            }

            return tableReferenceGroup;
        }

        /// <summary>
        /// Handles the joined tables selection list.
        /// </summary>
        /// <param name="node"> The node. </param>
        /// <param name="firstTableReference"> The first table reference. </param>
        /// <param name="secondTableReference"> The second table reference. </param>
        private void HandleJoinedTablesSelectionList(
            MethodCallExpression node,
            TableReferenceGroup firstTableReference,
            TableReference secondTableReference)
        {
            var parameterExpression =
                ((LambdaExpression)((UnaryExpression)node.Arguments[4]).Operand).Body as ParameterExpression;
            if (parameterExpression != null)
            {
                TableReference selectedTableReference;
                if (parameterExpression.Type == firstTableReference.Type)
                {
                    selectedTableReference = firstTableReference.References.Single();
                }
                else if (parameterExpression.Type == secondTableReference.Type)
                {
                    selectedTableReference = secondTableReference;
                }
                else
                {
                    throw new InvalidOperationException();
                }


                var selectionList = new SelectionList
                {
                    IsJoinSelectionList = true,
                    Kind = QueryPartKind.SelectionList,
                    QueryText = selectedTableReference.Alias == null ? "*" : selectedTableReference.Alias + ".*",
                    Type = selectedTableReference.Type
                };

                _context.Stack.Push(selectedTableReference);
                _context.Stack.Push(selectionList);
            }
            else
            {
                _context.Stack.Push(firstTableReference);
                _context.Stack.Push(secondTableReference);
                var handler = new SelectionListHandler(_context);
                handler.HandleSelectionList(node.Arguments[4]);
                var selectionList = (SelectionList)_context.Stack.Peek();
                selectionList.IsJoinSelectionList = true;
            }
        }

        /// <summary>
        /// Handles the ordering.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandleOrdering(MethodCallExpression node)
        {
            var part = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Order
            };

            _context.Stack.Push(part);
            _context.Visitor.Visit(node.Arguments[1]);

            var references = new List<QueryPartDescriptor>();
            while (_context.Stack.Peek() != part)
            {
                references.Add(_context.Stack.Pop());
            }

            var reference = references.Single();
            part.QueryText = reference.QueryText;

            if (node.Method.Name == "OrderByDescending"
                || node.Method.Name == "ThenByDescending")
            {
                part.QueryText += " DESC";
            }
        }

        /// <summary>
        /// Handles the projection.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandleProjection(MethodCallExpression node)
        {
            var handler = new SelectionListHandler(_context);
            handler.HandleSelectionList(node.Arguments[1]);
        }

        /// <summary>
        /// Hanldes the take.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HanldeTake(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);
            _context.Visitor.Visit(node.Arguments[1]);
            var parameter = (QueryParameter)_context.Stack.Pop();
            parameter.IgnoreRelatedProperty = true;
            var count = parameter.ParameterValues.Single().Item2;
            var part = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Top,
                QueryText = count.ToString(),
                ParameterValues = parameter.ParameterValues
            };
            _context.Stack.Push(part);
        }

        /// <summary>
        /// Hanldes the skip.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HanldeSkip(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);
            _context.Visitor.Visit(node.Arguments[1]);
            var parameter = (QueryParameter)_context.Stack.Pop();
            parameter.IgnoreRelatedProperty = true;
            var count = parameter.ParameterValues.Single().Item2;
            var part = new QueryPartDescriptor
            {
                Kind = QueryPartKind.Skip,
                QueryText = count.ToString(),
                ParameterValues = parameter.ParameterValues
            };
            _context.Stack.Push(part);
        }
    }
}