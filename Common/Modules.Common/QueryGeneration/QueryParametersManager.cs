using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    /// Query parameters manager.
    /// </summary>
    public sealed class QueryParametersManager
    {
        /// <summary>
        /// The current date parameter alias.
        /// </summary>
        public const string CurrentDateParameterAlias = "@CurrentDate";
        public const string MaximalDateParameterAlias = "@MaxDate";
        public const string MinimalDateParameterAlias = "@MinDate";

        /// <summary>
        /// The parameters.
        /// </summary>
        private readonly List<QueryParameter> _parameters = new List<QueryParameter>();

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParametersManager" /> class.
        /// </summary>
        /// <param name="timeProvider"> The time provider. </param>
        public QueryParametersManager(ITimeProvider timeProvider)
        {
            CurrnetDateTime = timeProvider.Now.ToUniversalTime();
        }

        /// <summary>
        /// The currnet date time.
        /// </summary>
        public DateTime CurrnetDateTime { get; private set; }

        public DateTime MaximalAccepableDate { get; } = new DateTime(2079, 6, 6);

        public DateTime MinimalAccepableDate { get; } = new DateTime(1900, 1, 1);

        /// <summary>
        /// Combines the parameters.
        /// </summary>
        /// <param name="descriptors"> The descriptors. </param>
        /// <returns> Combined parameters. </returns>
        public static IEnumerable<Tuple<string, object>> CombineParameters(
            IEnumerable<QueryPartDescriptor> descriptors)
        {
            return descriptors.Select(x => x.ParameterValues)
                .Where(x => x != null)
                .SelectMany(x => x)
                .Distinct()
                .ToArray();
        }

        /// <summary>
        /// Combines the parameters.
        /// </summary>
        /// <param name="right"> The right. </param>
        /// <param name="left"> The left. </param>
        /// <returns> Combined parameters. </returns>
        public static IEnumerable<Tuple<string, object>> CombineParameters(
            QueryPartDescriptor right,
            QueryPartDescriptor left)
        {
            return right.ParameterValues.EmptyIfNull()
                .Concat(left.ParameterValues.EmptyIfNull())
                .Distinct()
                .ToArray();
        }

        /// <summary>
        /// Combines the parameters.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <param name="third">The third.</param>
        /// <returns>
        /// Combined parameters.
        /// </returns>
        public static IEnumerable<Tuple<string, object>> CombineParameters(
            QueryPartDescriptor first,
            QueryPartDescriptor second,
            QueryPartDescriptor third)
        {
            return first.ParameterValues.EmptyIfNull()
                .Concat(second.ParameterValues.EmptyIfNull())
                .Concat(third.ParameterValues.EmptyIfNull())
                .Distinct()
                .ToArray();
        }

        /// <summary>
        /// Optimizes the parameters.
        /// </summary>
        /// <param name="parameter"> The parameter. </param>
        /// <returns> Query parameter. </returns>
        public QueryParameter OptimizeParameters(QueryParameter parameter)
        {
            foreach (var queryParameter in _parameters)
            {
                if (queryParameter.ParameterValues.Select(x => x.Item2)
                    .SequenceEqual(parameter.ParameterValues.Select(x => x.Item2)))
                {
                    return queryParameter;
                }
            }

            _parameters.Add(parameter);
            return parameter;
        }
    }
}