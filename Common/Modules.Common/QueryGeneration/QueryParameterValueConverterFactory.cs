using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace Qdarc.Modules.Common.QueryGeneration
{
    internal sealed class QueryParameterValueConverterFactory : IQueryParameterValueConverterFactory
    {
        private readonly ConcurrentDictionary<Type, IQueryParameterValueConverter> _converters
            = new ConcurrentDictionary<Type, IQueryParameterValueConverter>();

        public QueryParameterValueConverterFactory(IEnumerable<IQueryParameterValueConverter> converters)
        {
            foreach (var queryParameterValueConverter in converters)
            {
                if (!_converters.TryAdd(queryParameterValueConverter.HandledType, queryParameterValueConverter))
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public IQueryParameterValueConverter<TElement> Resolve<TElement>()
        {
            return (IQueryParameterValueConverter<TElement>)Resolve(typeof(TElement));
        }

        public IQueryParameterValueConverter Resolve(Type type)
        {
            var converterType = typeof(IQueryParameterValueConverter<>).MakeGenericType(type);
            if (_converters.ContainsKey(converterType))
            {
                return _converters[converterType];
            }

            var interfaces = type.GetInterfaces();
            return interfaces.Select(Resolve)
                .FirstOrDefault(converter => converter != null);
        }
    }
}