using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public static class GroupingTableQuery
    {
        public static IGroupingTableQuery<TKey, TSource, TProjection> Having<TKey, TSource, TProjection>(
            this IGroupingTableQuery<TKey, TSource, TProjection> source,
            Expression<Func<IGroup<TKey, TSource>>> filter)
        {
            MethodInfo method = ExpressionExtensions.GetMethod(() => Having<TKey, TSource, TProjection>(null, null));
            var arguments = new[]
            {
                source.Expression,
                Expression.Quote(filter)
            };

            return source.Provider.CreateGroupingQuery<TKey, TSource, TProjection>(Expression.Call(
                null,
                method,
                arguments));
        }

        public static bool Any<TKey, TSource, TProjection>(
            this IGroupingTableQuery<TKey, TSource, TProjection> source,
            Expression<Func<IGroup<TKey, TSource>, bool>> filter)
        {
            MethodInfo method = ExpressionExtensions.GetMethod(() => Any<TKey, TSource, TProjection>(null, null));
            var arguments = new[]
            {
                source.Expression,
                Expression.Quote(filter)
            };

            MethodCallExpression expression = Expression.Call(
                null,
                method,
                arguments);

            return source.Provider.Execute<int>(expression) > 0;
        }

        public static bool Any<TKey, TSource, TProjection>(
            this IGroupingTableQuery<TKey, TSource, TProjection> source)
        {
            MethodInfo method = ExpressionExtensions.GetMethod(() => Any<TKey, TSource, TProjection>(null));
            var arguments = new[]
            {
                source.Expression
            };

            MethodCallExpression expression = Expression.Call(
                null,
                method,
                arguments);

            return source.Provider.Execute<int>(expression) > 0;
        }

        public static Task<bool> AnyAsync<TKey, TSource, TProjection>(
            this IGroupingTableQuery<TKey, TSource, TProjection> source,
            Expression<Func<IGroup<TKey, TSource>, bool>> filter)
        {
            return Task.Run(() => source.Any(filter));
        }

        public static Task<bool> AnyAsync<TKey, TSource, TProjection>(
            this IGroupingTableQuery<TKey, TSource, TProjection> source)
        {
            return Task.Run(() => source.Any());
        }

        public static Task<List<TProjection>> ToListAsync<TKey, TSource, TProjection>(
            this IGroupingTableQuery<TKey, TSource, TProjection> source)
        {
            return Task.Run(() => source.ToList());
        }

        public static List<TProjection> ToList<TKey, TSource, TProjection>(
            this IGroupingTableQuery<TKey, TSource, TProjection> source)
        {
            return source.Provider.Execute<List<TProjection>>(source.Expression);
        }
    }
}