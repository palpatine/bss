﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Qdarc.Modules.Common.QueryGeneration
{
    class GroupingTableQueryMethodsHandler
    {
        private readonly QueryTranslationContext _context;

        public GroupingTableQueryMethodsHandler(QueryTranslationContext context)
        {
            _context = context;
        }

        public void HandleQueryableMethods(MethodCallExpression node)
        {
            if (node.Method.Name == "Any")
            {
                HandeAny(node);
            }
            else if (node.Method.Name == "Having")
            {
                _context.Visitor.Visit(node.Arguments[0]);
                var handler = new FilteringHandler(_context);
                handler.HandleFiltering(node);
            }
        }

        /// <summary>
        /// Handes any.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandeAny(MethodCallExpression node)
        {
            _context.Visitor.Visit(node.Arguments[0]);

            if (node.Arguments.Count > 1)
            {
                var handler = new FilteringHandler(_context);
                handler.HandleFiltering(node.Arguments[1]);
            }

            var selectionList = new SelectionList
            {
                QueryText = "Count(*)"
            };

            _context.Stack.Push(selectionList);
        }
    }
}
