using System;
using System.Collections;
using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Table reference.
    /// </summary>
    public sealed class TableReference : QueryPartDescriptor, ITableReferenceProvider, IColumnReferenceProvider
    {
        /// <summary>
        ///     Gets or sets the alias.
        /// </summary>
        /// <value>
        ///     The alias.
        /// </value>
        public string Alias { get; set; }

        /// <summary>
        ///     Gets or sets the kind.
        /// </summary>
        /// <value>
        ///     The kind.
        /// </value>
        public override QueryPartKind Kind
        {
            get { return QueryPartKind.TableReference; }

            set { }
        }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        /// <value>
        ///     The type.
        /// </value>
        public Type Type { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is single column.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is single column; otherwise, <c>false</c>.
        /// </value>
        public bool IsSingleColumn { get; set; }

        /// <summary>
        ///     Gets or sets the column.
        /// </summary>
        /// <value>
        ///     The column.
        /// </value>
        public ColumnReference Column { get; set; }

        public bool IsValues { get; set; }

        /// <summary>
        /// To the reference group.
        /// </summary>
        /// <returns>Table references group.</returns>
        public TableReferenceGroup ToReferenceGroup()
        {
            return new TableReferenceGroup
             {
                 References = new[] { this },
                 QueryText = QueryText,
                 Type = Type,
                 ParameterValues = ParameterValues,
             };
        }

        /// <summary>
        /// Wraps the in column reference.
        /// </summary>
        /// <returns>Column reference.</returns>
        public ColumnReference GenerateColumnReference(string columnName = null, PropertyInfo property = null)
        {
            if (IsSingleColumn)
            {
                return Column;
            }
            else
            {
                var part = new ColumnReference
                {
                    TableReference = this,
                    QueryText = Alias == null ?
                                    columnName ?? "*" : 
                                    string.Format(
                                        QueryTranslationContext.PrefixedTemplate,
                                        Alias,
                                        columnName ?? "*"),
                    RelatedEntityProperty = property,
                    WrappedPart = this,
                    IsStarReference = columnName == null
                };
                return part;
            }
        }

        public ColumnReference WrapInColumnReference()
        {
            return GenerateColumnReference();
        }
    }
}