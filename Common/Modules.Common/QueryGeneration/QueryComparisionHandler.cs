using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Query Comparision handler.
    /// </summary>
    internal sealed class QueryComparisionHandler
    {
        /// <summary>
        ///     The parameters manager.
        /// </summary>
        private readonly QueryParametersManager _parametersManager;

        /// <summary>
        ///     The stack.
        /// </summary>
        private readonly Stack<QueryPartDescriptor> _stack;

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryComparisionHandler" /> class.
        /// </summary>
        /// <param name="stack"> The stack. </param>
        /// <param name="parametersManager"> The parameters manager. </param>
        public QueryComparisionHandler(
            Stack<QueryPartDescriptor> stack,
            QueryParametersManager parametersManager)
        {
            _stack = stack;
            _parametersManager = parametersManager;
        }

        public static bool TryConvertToComparision(QueryPartDescriptor filter, out QueryPartDescriptor result)
        {
            var columnReference = filter as ColumnReference;
            if (columnReference != null)
            {
                if (columnReference.Type != typeof(bool) &&
                    (columnReference.RelatedEntityProperty == null
                    || (columnReference.RelatedEntityProperty.PropertyType != typeof(bool))))
                {
                    result = null;
                    return false;
                }

                var comparision = new Comparission
                {
                    QueryText = string.Format(CultureInfo.InvariantCulture, "({0} = 1)", columnReference.QueryText)
                };

                filter = comparision;
            }

            var parameter = filter as QueryParameter;
            if (parameter != null)
            {
                if (parameter.Expression.Type != typeof(bool))
                {
                    throw new InvalidOperationException();
                }

                parameter.IgnoreRelatedProperty = true;

                var comparision = new Comparission
                {
                    QueryText = string.Format(CultureInfo.InvariantCulture, "({0} = 1)", parameter.QueryText),
                    ParameterValues = parameter.ParameterValues
                };

                filter = comparision;
            }

            result = filter;
            return true;
        }

        /// <summary>
        ///     Tries the handle comparison.
        /// </summary>
        /// <param name="node"> The node. </param>
        /// <param name="left"> The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> True if handled. </returns>
        public bool TryHandleComparison(
            BinaryExpression node,
            QueryPartDescriptor left,
            QueryPartDescriptor right)
        {
            if (node.NodeType == ExpressionType.NotEqual)
            {
                HandleComparison(left, right, "IS NOT", "!=");
                return true;
            }
            if (node.NodeType == ExpressionType.Equal)
            {
                HandleComparison(left, right, "IS", "=");
                return true;
            }
            if (node.NodeType == ExpressionType.LessThan)
            {
                HandleComparison(left, right, null, "<");
                return true;
            }
            if (node.NodeType == ExpressionType.GreaterThan)
            {
                HandleComparison(left, right, null, ">");
                return true;
            }
            if (node.NodeType == ExpressionType.LessThanOrEqual)
            {
                HandleComparison(left, right, null, "<=");
                return true;
            }
            if (node.NodeType == ExpressionType.GreaterThanOrEqual)
            {
                HandleComparison(left, right, null, ">=");
                return true;
            }
            if (node.NodeType == ExpressionType.AndAlso)
            {
                HandleComparison(left, right, null, "AND");
                return true;
            }
            if (node.NodeType == ExpressionType.OrElse)
            {
                if (left.Kind != QueryPartKind.Comparision)
                {
                    if (!TryConvertToComparision(left, out left))
                    {
                        throw new InvalidOperationException();
                    }
                }
                if (right.Kind != QueryPartKind.Comparision)
                {
                    if (!TryConvertToComparision(right, out right))
                    {
                        throw new InvalidOperationException();
                    }
                }
                HandleComparison(left, right, null, "OR");
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Reverses the operator. Ths metod will retrun operator that should be applayed when left
        ///     side of comparision is switched with right side. So for operations that are symmetric
        ///     the same operator will be returned.
        /// </summary>
        /// <param name="operator"> The operator. </param>
        /// <returns> Reversed operator. </returns>
        private static string ReverseAsymetricOperator(string @operator)
        {
            switch (@operator)
            {
                case "!=":
                    {
                        return "!=";
                    }

                case "=":
                    {
                        return "=";
                    }

                case "&":
                    {
                        return "&";
                    }

                case ">":
                    {
                        return "<";
                    }

                case "<":
                    {
                        return ">";
                    }

                case ">=":
                    {
                        return "<=";
                    }

                case "<=":
                    {
                        return ">=";
                    }
            }

            throw new NotSupportedException();
        }

        /// <summary>
        /// Handles the comparison.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <param name="nullOperator">The null operator.</param>
        /// <param name="valueOperator">The value operator.</param>
        private void HandleComparison(
            QueryPartDescriptor left,
            QueryPartDescriptor right,
            string nullOperator,
            string valueOperator)
        {
            if (left.Kind == QueryPartKind.TableReference || left.Kind == QueryPartKind.TableReferenceGroup)
            {
                var reference = (ITableReferenceProvider)left;
                left = ((IColumnReferenceProvider)left).GenerateColumnReference("[Id]", reference.Type.GetProperty("Id"));
            }

            if (right.Kind == QueryPartKind.TableReference || right.Kind == QueryPartKind.TableReferenceGroup)
            {
                var reference = (ITableReferenceProvider)right;
                right = ((IColumnReferenceProvider)right).GenerateColumnReference("[Id]", reference.Type.GetProperty("Id"));
            }

            if (left.Kind == QueryPartKind.Parameter)
            {
                var parameter = (QueryParameter)left;
                CreateComparision(right, parameter, nullOperator, ReverseAsymetricOperator(valueOperator));
            }
            else if (right.Kind == QueryPartKind.Parameter)
            {
                var parameter = (QueryParameter)right;
                CreateComparision(left, parameter, nullOperator, valueOperator);
            }
            else
            {
                var part = new Comparission();
                _stack.Push(part);
                QueryPartDescriptor converted;
                if (TryConvertToComparision(left, out converted))
                {
                    left = converted;
                }

                if (TryConvertToComparision(right, out converted))
                {
                    right = converted;
                }
                part.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "({0} {1} {2})",
                    left.QueryText,
                    valueOperator,
                    right.QueryText);
                part.ParameterValues = QueryParametersManager.CombineParameters(left, right);
            }
        }

        private void CreateComparision(
            QueryPartDescriptor compared,
            QueryParameter parameter,
            string nullOperator,
            string valueOperator)
        {
            if (compared.Kind == QueryPartKind.Comparision)
            {
                parameter.IgnoreRelatedProperty = true;
                var value = parameter.ParameterValues.Single().Item2;
                if (value is bool)
                {
                    var boolValue = (bool)value;
                    if (boolValue)
                    {
                        _stack.Push(compared);
                        return;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }

                if (value is int)
                {
                    var intValue = (int)value;
                    if (intValue == 1)
                    {
                        _stack.Push(compared);
                        return;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
            }

            var part = new Comparission();

            _stack.Push(part);

            if (compared.Kind == QueryPartKind.Arithmetic || compared.Kind == QueryPartKind.Comparision)
            {
                parameter.IgnoreRelatedProperty = true;
            }
            else if (compared.Kind == QueryPartKind.CompoundPropertyAccess)
            {
                var compoundPropertyAccess = (CompoundPropertyAccess)compared;
                var column =
                    compoundPropertyAccess.Candidates.First();
                compared = column;
                parameter.RelatedEntityProperty = column.RelatedEntityProperty;
            }
            else
            {
                var relatedPropertyProvider = (IRelatedPropertyProvider)compared;
                if (relatedPropertyProvider.ShouldIgnore)
                {
                    parameter.IgnoreRelatedProperty = true;
                }
                else
                {
                    parameter.RelatedEntityProperty = relatedPropertyProvider.RelatedEntityProperty;
                }
            }

            if (parameter.ParameterValues.Single().Item2 == null)
            {
                part.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "({0} {1} NULL)",
                    compared.QueryText,
                    nullOperator);
                part.ParameterValues = compared.ParameterValues;
            }
            else
            {
                parameter = _parametersManager.OptimizeParameters(parameter);
                var columnReference = compared as ColumnReference;
                if (columnReference != null)
                {
                    WrapVersionDateInIfNull(columnReference);
                }

                part.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "({0} {1} {2})",
                    compared.QueryText,
                    valueOperator,
                    parameter.Alias);
                part.ParameterValues = QueryParametersManager.CombineParameters(parameter, compared);
            }
        }

        /// <summary>
        ///     Wraps the version date in if null.
        /// </summary>
        /// <param name="columnReference"> The column reference. </param>
        private void WrapVersionDateInIfNull(ColumnReference columnReference)
        {
            if (columnReference.RelatedEntityProperty != null &&
                columnReference.RelatedEntityProperty.Name.EndsWith("VersionEndDate", StringComparison.Ordinal))
            {
                columnReference.QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "IFNULL({0}, {1})",
                    columnReference.QueryText,
                    QueryParametersManager.CurrentDateParameterAlias);

                columnReference.ParameterValues = new[]
                {
                    Tuple.Create<string, object>(QueryParametersManager.CurrentDateParameterAlias,
                        _parametersManager.CurrnetDateTime)
                };
            }
        }
    }
}