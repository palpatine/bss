namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Query part kind.
    /// </summary>
    public enum QueryPartKind
    {
        /// <summary>
        ///     The none.
        /// </summary>
        None = 0,

        /// <summary>
        ///     The selection list.
        /// </summary>
        SelectionList,

        /// <summary>
        ///     The query.
        /// </summary>
        Query,

        /// <summary>
        ///     From.
        /// </summary>
        From,

        /// <summary>
        ///     The table reference.
        /// </summary>
        TableReference,

        /// <summary>
        ///     The where.
        /// </summary>
        Where,

        /// <summary>
        ///     The column reference.
        /// </summary>
        ColumnReference,

        /// <summary>
        ///     The comparision.
        /// </summary>
        Comparision,

        /// <summary>
        ///     The parameter.
        /// </summary>
        Parameter,

        /// <summary>
        ///     The join.
        /// </summary>
        Join,

        /// <summary>
        ///     The order.
        /// </summary>
        Order,

        /// <summary>
        ///     The top.
        /// </summary>
        Top,

        /// <summary>
        ///     The distinct.
        /// </summary>
        Distinct,

        /// <summary>
        ///     The with recursive.
        /// </summary>
        WithRecursive,
        GroupBySelectionList,
        Having,
        Paging,
        TableReferenceGroup,
        Arithmetic,
        Skip,
        ParameterExpresionMapping,
        LambdaExpresionMapping,
        SelectionListMapping,
        Argument,
        CompoundPropertyAccess
    }
}