using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Query translation context.
    /// </summary>
    public class QueryTranslationContext
    {
        /// <summary>
        ///     The brace template.
        /// </summary>
        public const string BraceTemplate = "[{0}]";

        /// <summary>
        ///     The prefixed template.
        /// </summary>
        public const string PrefixedTemplate = "{0}.{1}";

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryTranslationContext" /> class.
        /// </summary>
        /// <param name="timeProvider">The time provider.</param>
        /// <param name="visitor">The visitor.</param>
        /// <param name="tableNameResolver">The table name resolver.</param>
        internal QueryTranslationContext(
            ITimeProvider timeProvider,
            ExpressionVisitor visitor,
            ITableNameResolver tableNameResolver)
        {
            Stack = new Stack<QueryPartDescriptor>();
            AliasManager = new QueryElementAliasManager();
            QueryParametersManager = new QueryParametersManager(timeProvider);
            Visitor = visitor;
            TableNameResolver = tableNameResolver;
        }

        /// <summary>
        ///     The alias manager.
        /// </summary>
        public QueryElementAliasManager AliasManager { get; private set; }

        /// <summary>
        ///     The query parameters manager.
        /// </summary>
        public QueryParametersManager QueryParametersManager { get; private set; }

        /// <summary>
        ///     The stack.
        /// </summary>
        public Stack<QueryPartDescriptor> Stack { get; }

        /// <summary>
        ///     Gets the visitor.
        /// </summary>
        /// <value>
        ///     The visitor.
        /// </value>
        public ExpressionVisitor Visitor { get; private set; }

        /// <summary>
        ///     Gets or sets the table name resolver.
        /// </summary>
        /// <value>
        ///     The table name resolver.
        /// </value>
        public ITableNameResolver TableNameResolver { get; private set; }

        public ITableReferenceProvider FindTableReference(Type type)
        {
            var items =
                Stack
                .Where(x => x.Kind == QueryPartKind.TableReference || x.Kind == QueryPartKind.TableReferenceGroup)
                .SelectMany(x =>
                {
                    if (x.Kind == QueryPartKind.TableReference)
                    {
                        return new[] { (ITableReferenceProvider)x };
                    }
                    else
                    {
                        var group = (TableReferenceGroup)x;
                        return new[] { group }.Cast<ITableReferenceProvider>().Concat(group.References);
                    }
                })
                .ToArray();

            var reference =
                items.SingleOrDefault(
                    x => type.IsAssignableFrom(x.Type));

            return reference;
        }

        public SelectionList TryFindSelectionList()
        {
            return (SelectionList)TryFindElement(QueryPartKind.SelectionList);
        }

        public TQueryPartDescriptor TryFindElement<TQueryPartDescriptor>(QueryPartKind kind, Func<TQueryPartDescriptor, bool> filter)
           where TQueryPartDescriptor : QueryPartDescriptor
        {
            using (var enumerator = Stack.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.Kind == QueryPartKind.Query)
                    {
                        return null;
                    }

                    if (enumerator.Current.Kind == kind)
                    {
                        var element = (TQueryPartDescriptor)enumerator.Current;
                        if (filter(element))
                            return element;
                    }
                }
            }

            return null;
        }

        public QueryPartDescriptor TryFindElement(QueryPartKind kind)
        {
            using (var enumerator = Stack.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.Kind == QueryPartKind.Query)
                    {
                        return null;
                    }

                    if (enumerator.Current.Kind == kind)
                    {
                        return enumerator.Current;
                    }
                }
            }

            return null;
        }

        public ColumnReference TryFindColumnReference(MemberExpression node)
        {
            var selectionList = (SelectionList)TryFindElement(QueryPartKind.SelectionList);
            if (selectionList == null)
            {
                return null;
            }

            ColumnReference column = selectionList.Columns.EmptyIfNull()
                .FirstOrDefault(x => x.RelatedEntityProperty != null
                                            && x.RelatedEntityProperty.MetadataToken == node.Member.MetadataToken
                                            && x.RelatedEntityProperty.Module == node.Member.Module
                    || x.RelatedIntermediateTypeProperty != null
                                            && x.RelatedIntermediateTypeProperty.MetadataToken == node.Member.MetadataToken
                                            && x.RelatedIntermediateTypeProperty.Module == node.Member.Module);
            if (column == null)
            {
                return null;
            }

            bool isOuterScope = false;
            foreach (var item in Stack)
            {
                if (item.Kind == QueryPartKind.Query) break;

                isOuterScope = item.Kind == QueryPartKind.Order
                                || item.Kind == QueryPartKind.Having;

                if (isOuterScope)
                    break;
            }

            if (isOuterScope)
            {
                var reference = new ColumnReference
                {
                    QueryText = column.Alias,
                    Type = column.Type,
                    //TableReference = Query as table reference,
                    RelatedEntityProperty = (PropertyInfo)node.Member,
                    ParameterValues = column.ParameterValues
                };
                return reference;
            }
            else
            {
                ColumnReference template;
                if (column.WrappedPart == null)
                {
                    template = column;
                }
                else
                {
                    template = column.WrappedPart as ColumnReference;
                    if (template == null)
                    {
                        template = ((IWrappableInColumn)column.WrappedPart).WrapInColumnReference();
                    }
                }

                var reference = new ColumnReference
                {
                    QueryText = column.QueryText,
                    Type = template.Type,
                    TableReference = template.TableReference,
                    RelatedEntityProperty = (PropertyInfo)node.Member,
                    ParameterValues = template.ParameterValues
                };

                return reference;
            }
        }

        internal IEnumerable<QueryPartDescriptor> PopUntil(QueryPartKind kind)
        {
            var parts = new List<QueryPartDescriptor>();
            while (Stack.Peek().Kind != kind)
            {
                parts.Add(Stack.Pop());
            }

            return parts;
        }

        internal IEnumerable<QueryPartDescriptor> PopUntil(Func<QueryPartDescriptor, bool> condition)
        {
            var parts = new List<QueryPartDescriptor>();
            while (condition(Stack.Peek()))
            {
                parts.Add(Stack.Pop());
            }

            return parts;
        }
    }
}