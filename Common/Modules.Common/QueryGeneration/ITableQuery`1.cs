using System.Linq;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Table query.
    /// </summary>
    /// <typeparam name="TEntity"> The type of the entity. </typeparam>
    public interface ITableQuery<out TEntity> : ITableQuery
    {
    }
}