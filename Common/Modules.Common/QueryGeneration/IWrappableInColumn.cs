using System.Reflection;

namespace Qdarc.Modules.Common.QueryGeneration
{
    interface IWrappableInColumn
    {
        ColumnReference WrapInColumnReference();
    }

    interface IColumnReferenceProvider : IWrappableInColumn
    {
        ColumnReference GenerateColumnReference(string columnName = null, PropertyInfo property = null);
    }
}