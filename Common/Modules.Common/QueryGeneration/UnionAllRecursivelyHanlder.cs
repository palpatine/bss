using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Handles recursive with clause using union all statement.
    /// </summary>
    internal sealed class UnionAllRecursivelyHanlder
    {
        private readonly QueryTranslationContext _context;

        /// <summary>
        ///     Initializes a new instance of the <see cref="UnionAllRecursivelyHanlder" /> class.
        /// </summary>
        /// <param name="context">The _context.</param>
        public UnionAllRecursivelyHanlder(
            QueryTranslationContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Handles the union all recursively.
        /// </summary>
        /// <param name="node"> The node. </param>
        public void HandleUnionAllRecursively(MethodCallExpression node)
        {
            var withPart = new QueryPartDescriptor
            {
                Kind = QueryPartKind.WithRecursive,
            };
            _context.Stack.Push(withPart);

            var firstQueryHandler = new QueryHandler(_context);
            firstQueryHandler.VisitQuery(node.Arguments[0]);
            var firstQuery = (Query)_context.Stack.Pop();

            if (firstQuery.TemporaryViews != null)
            {
                foreach (var view in firstQuery.TemporaryViews)
                {
                    _context.Stack.Push(view);
                }
            }

            var secondQueryHandler = new QueryHandler(_context);
            secondQueryHandler.VisitQuery(node.Arguments[1]);
            var secondQuery = (Query)_context.Stack.Pop();

            if (secondQuery.TemporaryViews != null)
            {
                foreach (var view in secondQuery.TemporaryViews)
                {
                    _context.Stack.Push(view);
                }
            }

            var name = _context.AliasManager.GetAlias("VIEW") + "R";
            var alias = _context.AliasManager.GetAlias(name);

            var outerSelectionList = firstQuery.SelectionList;
            var columnsList = string.Empty;
            if (outerSelectionList != null && outerSelectionList.Columns != null && outerSelectionList.Columns.Any())
            {
                columnsList = string.Join(", ", outerSelectionList.Columns.Select(x => x.Alias));
                columnsList = string.Format(CultureInfo.InvariantCulture, "({0})", columnsList);
            }

            var tableReference = new TableReference
            {
                Alias = string.Format(CultureInfo.InvariantCulture, "[{0}]", alias),
                QueryText = string.Format(CultureInfo.InvariantCulture, "[{0}]", name),
                Type = TypeSystem.GetElementType(node.Type),
                IsSingleColumn = outerSelectionList != null && outerSelectionList.Columns != null && outerSelectionList.Columns.Count() == 1,
            };

            if (tableReference.IsSingleColumn)
            {
                var singleColumn = outerSelectionList.Columns.Single();
                tableReference.Column = new ColumnReference
                {
                    Alias = singleColumn.Alias,
                    QueryText =
                        string.Format(CultureInfo.InvariantCulture, "{0}.{1}", tableReference.Alias, singleColumn.Alias),
                    TableReference = tableReference,
                    Type = singleColumn.Type
                };
            }

            var from = new FromClause();
            _context.Stack.Push(@from);

            TableReference secondTableReference;
            QueryPartDescriptor secondFilter = null;
            if (secondQuery.IsSimpleSubQuery)
            {
                secondTableReference = secondQuery.From.References.Single();
                secondFilter = secondQuery.Where;
            }
            else
            {
                secondTableReference = secondQuery.WrapInTableReference(_context.AliasManager);
            }

            _context.Stack.Push(tableReference);
            _context.Visitor.Visit(node.Arguments[2]);
            var first = _context.Stack.Pop();
            ColumnReference firstColumnReference;
            if (first.Kind == QueryPartKind.ColumnReference)
            {
                firstColumnReference = (ColumnReference)first;
            }
            else if (first.Kind == QueryPartKind.TableReference || first.Kind == QueryPartKind.TableReferenceGroup)
            {
                var reference = (ITableReferenceProvider)first;
                firstColumnReference = ((IColumnReferenceProvider)first).GenerateColumnReference("[Id]",
                    reference.Type.GetProperty("Id"));
            }
            else
            {
                var wrappable = (IWrappableInColumn)first;
                firstColumnReference = wrappable.WrapInColumnReference();
            }

            if (firstColumnReference.IsStarReference)
            {
                if (firstColumnReference.TableReference != null)
                {
                    firstColumnReference =
                        firstColumnReference.TableReference.GenerateColumnReference("Id",
                            firstColumnReference.TableReference.Type.GetProperty("Id"));
                }
                else
                {
                    firstColumnReference =
                        firstColumnReference.TableReferenceGroup.GenerateColumnReference("Id",
                            firstColumnReference.TableReferenceGroup.Type.GetProperty("Id"));
                }
            }

            _context.Stack.Pop();

            _context.Stack.Push(secondTableReference);
            _context.Visitor.Visit(node.Arguments[3]);
            var second = _context.Stack.Pop();
            ColumnReference secondColumnReference;
            if (second.Kind == QueryPartKind.ColumnReference)
            {
                secondColumnReference = (ColumnReference)second;
            }
            else if (second.Kind == QueryPartKind.TableReference || second.Kind == QueryPartKind.TableReferenceGroup)
            {
                var reference = (ITableReferenceProvider)second;
                secondColumnReference = ((IColumnReferenceProvider)second).GenerateColumnReference("[Id]",
                    reference.Type.GetProperty("Id"));
            }
            else
            {
                secondColumnReference = ((IWrappableInColumn)second).WrapInColumnReference();
            }

            if (secondColumnReference.IsStarReference)
            {
                if (secondColumnReference.TableReference != null)
                {
                    secondColumnReference =
                        secondColumnReference.TableReference.GenerateColumnReference("Id",
                            secondColumnReference.TableReference.Type.GetProperty("Id"));
                }
                else
                {
                    secondColumnReference =
                        secondColumnReference.TableReferenceGroup.GenerateColumnReference("Id",
                            secondColumnReference.TableReferenceGroup.Type.GetProperty("Id"));
                }
            }


            _context.Stack.Pop();
            var fromClauseHandler = new FromClauseHandler(_context.Stack, _context.AliasManager);

            fromClauseHandler.AddToFrom(
                tableReference.ToReferenceGroup(),
                secondTableReference,
                firstColumnReference,
                secondColumnReference,
                null,
                secondFilter);

            _context.Stack.Pop();

            var parameterExpression =
                ((LambdaExpression)((UnaryExpression)node.Arguments[4]).Operand).Body as ParameterExpression;
            SelectionList selectionList;
            if (parameterExpression != null)
            {
                TableReference selectedTableReference;
                if (parameterExpression.Type == secondTableReference.Type)
                {
                    selectedTableReference = secondTableReference;
                }
                else
                {
                    throw new InvalidOperationException();
                }

                selectionList = new SelectionList
                {
                    Kind = QueryPartKind.SelectionList,
                    QueryText = selectedTableReference.Alias + ".*",
                    Type = selectedTableReference.Type
                };
            }
            else
            {
                _context.Stack.Push(tableReference);
                _context.Stack.Push(secondTableReference);
                var selectionListHandler = new SelectionListHandler(_context);
                selectionListHandler.HandleSelectionList(node.Arguments[4]);
                selectionList = (SelectionList)_context.Stack.Pop();
            }

            while (_context.Stack.Peek() != withPart)
            {
                _context.Stack.Pop();
            }

            withPart.QueryText = string.Format(
                CultureInfo.InvariantCulture,
                "[{0}]{1} AS (" + Environment.NewLine + "{2}" + Environment.NewLine + "UNION ALL" + Environment.NewLine + "SELECT {3}" + Environment.NewLine + "FROM {4})",
                name,
                columnsList,
                firstQuery.QueryText,
                selectionList.QueryText,
                @from.QueryText);
            _context.Stack.Push(tableReference);
            var fromClauseHandler2 = new FromClauseHandler(_context.Stack, _context.AliasManager);
            fromClauseHandler2.AddToFrom(tableReference);
            withPart.ParameterValues = QueryParametersManager.CombineParameters(
                new QueryPartDescriptor[]
                {
                    firstQuery,
                    secondTableReference
                });
        }
    }
}