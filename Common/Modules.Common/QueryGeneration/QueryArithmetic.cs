﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Qdarc.Modules.Common.QueryGeneration
{
    class QueryArithmetic : QueryPartDescriptor, IWrappableInColumn
    {

        public override QueryPartKind Kind
        {
            get { return QueryPartKind.Arithmetic; }
            set { throw new InvalidOperationException(); }
        }

        public ColumnReference WrapInColumnReference()
        {
            var columnReference = new ColumnReference
            {
                QueryText = QueryText,
                ParameterValues = ParameterValues,
                WrappedPart = this,
            };

            return columnReference;
        }
    }

    class CompoundPropertyAccess : QueryPartDescriptor
    {
        public CompoundPropertyAccess(IEnumerable<ColumnReference> candidates)
        {
            Candidates = candidates;
        }

        public override QueryPartKind Kind
        {
            get { return QueryPartKind.CompoundPropertyAccess; }
            set { throw new InvalidOperationException(); }
        }

        public IEnumerable<ColumnReference> Candidates { get; private set; }
    }
}
