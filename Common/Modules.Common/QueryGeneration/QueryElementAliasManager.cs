using System;
using System.Collections.Generic;
using System.Text;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    /// Query element alias manager.
    /// </summary>
    public sealed class QueryElementAliasManager
    {
        /// <summary>
        /// The parameter alias base.
        /// </summary>
        private const string ParameterAliasBase = "@param";

        /// <summary>
        /// The alias counters.
        /// </summary>
        private readonly Dictionary<string, int> _aliasCounters = new Dictionary<string, int>();

        /// <summary>
        /// Indicates if table aliases are formbidden.
        /// </summary>
        public bool AreTableAliasesFormbidden { get; private set; }

        /// <summary>
        /// Forbids the table aliases.
        /// </summary>
        public void ForbidTableAliases()
        {
            AreTableAliasesFormbidden = true;
        }

        /// <summary>
        /// Gets the alias.
        /// </summary>
        /// <param name="name"> The name. </param>
        /// <returns> Alias for given name. </returns>
        public string GetAlias(string name)
        {
            var id = GetAliasId(name);

            if (AreTableAliasesFormbidden)
            {
                if (id != 1)
                {
                    throw new InvalidOperationException("Aliases are forbidden");
                }

                return null;
            }

            return name + id;
        }

        /// <summary>
        /// Gets the alias.
        /// </summary>
        /// <param name="type"> The type. </param>
        /// <returns> Alias for given name. </returns>
        public string GetAlias(Type type)
        {
            var id = GetAliasId(type);

            if (AreTableAliasesFormbidden)
            {
                if (id != 1)
                {
                    throw new InvalidOperationException("Aliases are forbidden");
                }

                return null;
            }

            return type.Name + id;
        }

        /// <summary>
        /// Gets the parameter alias.
        /// </summary>
        /// <returns> Parameter alias. </returns>
        public string GetParameterAlias()
        {
            var id = GetAliasId(ParameterAliasBase);

            return ParameterAliasBase + id;
        }

        /// <summary>
        /// Gets the alias identifier.
        /// </summary>
        /// <param name="type"> The type. </param>
        /// <returns> Alias id. </returns>
        private int GetAliasId(Type type)
        {
            return GetAliasId(type.FullName);
        }

        /// <summary>
        /// Gets the alias identifier.
        /// </summary>
        /// <param name="name"> Full name of the aliased element. </param>
        /// <returns> Alias id. </returns>
        private int GetAliasId(string name)
        {
            if (!_aliasCounters.ContainsKey(name))
            {
                _aliasCounters.Add(name, 0);
            }

            var aliasId = _aliasCounters[name] + 1;
            _aliasCounters[name] = aliasId;
            return aliasId;
        }

        /// <summary>
        /// Gets the alias for table.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <returns>Alias for table.</returns>
        public string GetAliasForTable(string tableName)
        {
            var reslut = new StringBuilder();
            foreach (var letter in tableName)
            {
                if (Char.IsUpper(letter))
                {
                    reslut.Append(letter);
                }
            }

            return GetAlias(reslut.ToString());
        }
    }
}