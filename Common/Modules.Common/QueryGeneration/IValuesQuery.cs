using System;
using System.Collections;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IValuesQuery
    {
        IEnumerable Values { get; }
        Type ElementType { get; }
    }
}