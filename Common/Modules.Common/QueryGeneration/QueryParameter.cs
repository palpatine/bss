using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml.Schema;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public class QueryParameter : QueryPartDescriptor, IRelatedPropertyProvider, IWrappableInColumn
    {
        private bool _isValueSet;

        private IEnumerable<Tuple<string, object>> _value;

        private Expression _expression;

        private PropertyInfo _relatedEntityProperty;
        private string _alias;

        public string Alias
        {
            get
            {
                return _alias;
            }
            set
            {
                _alias = value;
                QueryText = value;
            }
        }

        public Expression Expression
        {
            get
            {
                return _expression;
            }
            set
            {
                _expression = value;
                _isValueSet = false;
            }
        }

        public PropertyInfo RelatedEntityProperty
        {
            get
            {
                return _relatedEntityProperty;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException();
                }

                _relatedEntityProperty = value;
                _isValueSet = false;
            }
        }

        public override QueryPartKind Kind
        {
            get
            {
                return QueryPartKind.Parameter;
            }

            set
            {
            }
        }

        public override IEnumerable<Tuple<string, object>> ParameterValues
        {
            get
            {
                return GetValue();
            }

            set
            {
                throw new InvalidOperationException();
            }
        }

        public bool IgnoreRelatedProperty { get; set; }

        private IEnumerable<Tuple<string, object>> GetValue()
        {
            if (_isValueSet)
            {
                return _value;
            }

            if (!IgnoreRelatedProperty)
            {
                //todo: fix related entity property to point to entity property not projection property
                //var attribute = RelatedEntityProperty.GetCustomAttribute<SqlTypeAttribute>();
                //if (attribute == null)
                //{
                //    var message = string.Format(
                //        CultureInfo.InvariantCulture,
                //        "Property {0} cannot be used in sql query as it does not have corresponding column in database.",
                //        RelatedEntityProperty.Name);
                //    throw new InvalidOperationException(message);
                //}

            }

            _isValueSet = true;
            var method = Expression.Lambda<Func<object>>(Expression.Convert(Expression, typeof(object))).Compile();
            var value = method();

            if (value is DateTime)
            {
                if (Expression.NodeType == ExpressionType.MemberAccess)
                {
                    var memberAccess = (MemberExpression)Expression;
                    if (memberAccess.Member == ExpressionExtensions.GetMember(() => DateTime.Now))
                    {
                        value = DateTime.UtcNow;
                    }
                    if (memberAccess.Member == ExpressionExtensions.GetMember(() => DateTime.Today))
                    {
                        value = DateTime.UtcNow.Date;
                    }
                }
                var date = (DateTime)value;
                if (date.Kind == DateTimeKind.Unspecified)
                {
                    value = date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
                }

                if (date.Kind != DateTimeKind.Utc)
                {
                    throw new InvalidOperationException();
                }

                _value = new[] { Tuple.Create(Alias, value) };
                return _value;
            }

            if (value is string)
            {
                _value = new[] { Tuple.Create(Alias, value) };
                return _value;
            }

            if (value != null && value.GetType().IsEnum)
            {
                _value = new[] { Tuple.Create(Alias, (object)(int)value) };
                return _value;
            }

            if (value is bool)
            {
                _value = new[] { Tuple.Create(Alias, (object)(((bool)value) ? 1 : 0)) };
                return _value;
            }

            var collection = value as IEnumerable;

            if (collection != null)
            {
                _value = collection.Cast<object>().Select((x, i) => Tuple.Create(Alias + "_" + i, x)).ToArray();
                return _value;
            }

            _value = new[] { Tuple.Create(Alias, value) };
            return _value;
        }

        public bool ShouldIgnore => false;

        public ColumnReference WrapInColumnReference()
        {
            IgnoreRelatedProperty = true;
            var columnReference = new ColumnReference
            {
                QueryText = QueryText,
                ParameterValues = ParameterValues,
                WrappedPart = this
            };

            return columnReference;
        }
    }
}