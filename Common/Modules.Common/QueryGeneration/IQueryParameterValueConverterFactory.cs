using System;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public interface IQueryParameterValueConverterFactory
    {
        IQueryParameterValueConverter<TElement> Resolve<TElement>();

        IQueryParameterValueConverter Resolve(Type type);
    }
}