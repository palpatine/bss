﻿using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.QueryGeneration
{
    public class SqlDataReaderWrapper : ISqlDataReader
    {
        private readonly SqlDataReader _sqlDataReader;

        public SqlDataReaderWrapper(SqlDataReader sqlDataReader)
        {
            _sqlDataReader = sqlDataReader;
        }

        public void Dispose()
        {
            _sqlDataReader.Dispose();
        }

        public bool Read()
        {
            return _sqlDataReader.Read();
        }

        public object this[int i] => _sqlDataReader[i];

        public object this[string name] => _sqlDataReader[name];

        public async Task<bool> ReadAsync()
        {
            return await _sqlDataReader.ReadAsync();
        }

        public DataTable GetSchemaTable()
        {
            return _sqlDataReader.GetSchemaTable();
        }
    }
}