using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Query part descriptor.
    /// </summary>
    [DebuggerDisplay("{Kind}")]
    public class QueryPartDescriptor
    {
        /// <summary>
        ///     Gets or sets the kind.
        /// </summary>
        /// <value>
        ///     The kind.
        /// </value>
        public virtual QueryPartKind Kind { get; set; }

        /// <summary>
        ///     Gets or sets the parameter values.
        /// </summary>
        /// <value>
        ///     The parameter values.
        /// </value>
        public virtual IEnumerable<Tuple<string, object>> ParameterValues { get; set; }

        /// <summary>
        ///     Gets or sets the text.
        /// </summary>
        /// <value>
        ///     The text.
        /// </value>
        public string QueryText { get; set; }
    }
}