using System;
using System.Collections.Generic;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Selection list.
    /// </summary>
    public sealed class SelectionList : QueryPartDescriptor
    {
        /// <summary>
        ///     Gets or sets the kind.
        /// </summary>
        /// <value>
        ///     The kind.
        /// </value>
        public override QueryPartKind Kind
        {
            get { return QueryPartKind.SelectionList; }

            set { }
        }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        /// <value>
        ///     The type.
        /// </value>
        public Type Type { get; set; }

        /// <summary>
        ///     Gets or sets the columns.
        /// </summary>
        /// <value>
        ///     The columns.
        /// </value>
        public IEnumerable<ColumnReference> Columns { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is join selection list.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is join selection list; otherwise, <c>false</c>.
        /// </value>
        public bool IsJoinSelectionList { get; set; }
    }

    /// <summary>
    ///     Selection list for group by clause.
    /// </summary>
    public sealed class GroupBySelectionList : QueryPartDescriptor
    {
        /// <summary>
        ///     Gets or sets the kind.
        /// </summary>
        /// <value>
        ///     The kind.
        /// </value>
        public override QueryPartKind Kind
        {
            get { return QueryPartKind.GroupBySelectionList; }

            set { }
        }

        /// <summary>
        ///     Gets or sets the columns.
        /// </summary>
        /// <value>
        ///     The columns.
        /// </value>
        public IEnumerable<ColumnReference> Columns { get; set; }

        public Type Type { get; set; }
    }
}