using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.QueryGeneration
{
    /// <summary>
    ///     Handles string methods.
    /// </summary>
    internal sealed class StringMethodsHandler
    {
        /// <summary>
        ///     The stack.
        /// </summary>
        private readonly Stack<QueryPartDescriptor> _stack;

        /// <summary>
        ///     The visitor.
        /// </summary>
        private readonly QueryTranslator _visitor;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StringMethodsHandler" /> class.
        /// </summary>
        /// <param name="stack"> The stack. </param>
        /// <param name="visitor"> The visitor. </param>
        public StringMethodsHandler(
            Stack<QueryPartDescriptor> stack,
            QueryTranslator visitor)
        {
            _stack = stack;
            _visitor = visitor;
        }

        /// <summary>
        ///     Handles the string methods.
        /// </summary>
        /// <param name="node"> The node. </param>
        public void HandleStringMethods(MethodCallExpression node)
        {
            if (node.Method.Name == "StartsWith")
            {
                HandleStartsWith(node);
            }
            else
            {
                throw new NotSupportedException(node.ToString());
            }
        }

        /// <summary>
        ///     Handles the starts with.
        /// </summary>
        /// <param name="node"> The node. </param>
        private void HandleStartsWith(MethodCallExpression node)
        {
            _visitor.Visit(node.Object);
            var value = (ColumnReference)_stack.Pop();

            _visitor.Visit(node.Arguments[0]);
            var parameterPart = (QueryParameter)_stack.Pop();
            parameterPart.RelatedEntityProperty = value.RelatedEntityProperty;
            var parameter = parameterPart.ParameterValues.Single();
            parameter = new Tuple<string, object>(parameter.Item1, parameter.Item2.ToString()
                .Replace("_", "__")
                .Replace("%", "_%")
                .Replace("[", "_[")
                .Replace("]", "_]"));
            var part = new Comparission
            {
                ParameterValues = value.ParameterValues.EmptyIfNull().Concat(new[] { parameter }),
                QueryText =
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "({0} LIKE ({1} + '%') ESCAPE '_')",
                        value.QueryText,
                        parameter.Item1)
            };

            _stack.Push(part);
        }
    }
}