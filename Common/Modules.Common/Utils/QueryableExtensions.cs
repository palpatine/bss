﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.Utils
{
    public static class QueryableExtensions
    {
        public static IQueryable RemovePagingAndSorting(this IQueryable queryable)
        {
            var visitor = new RemovePagingAndSortingQueryVisitor();
            var expression = visitor.Visit(queryable.Expression);
            return queryable.Provider.CreateQuery(expression);
        }
    }

    class RemovePagingAndSortingQueryVisitor : ExpressionVisitor
    {
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.Name == "OrderBy"
                || node.Method.Name == "OrderByDescending"
                || node.Method.Name == "ThenBy"
                || node.Method.Name == "ThenByDescending"
                || node.Method.Name == "Skip"
                || node.Method.Name == "Top")
            {
                return node.Arguments[0];
            }

            return base.VisitMethodCall(node);
        }
    }
}
