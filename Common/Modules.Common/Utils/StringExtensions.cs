﻿using System.IO;

namespace Qdarc.Modules.Common.Utils
{
    public static class StringExtensions
    {
        public static Stream ToStream(this string text)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(text);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}