﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.Utils
{
    /// <summary>
    /// Contains extension methods that operate on expressions.
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Gets the property.
        /// </summary>
        /// <typeparam name="TArgument">The type of the argument.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="expression">The expression.</param>
        /// <returns>Object describing referenced property.</returns>
        public static PropertyInfo GetProperty<TArgument, TResult>(this TArgument instance, Expression<Func<TArgument, TResult>> expression)
        {
            return expression.GetProperty();
        }

        /// <summary>
        /// Gets the parent property expression.
        /// </summary>
        /// <typeparam name="TRoot">The type of the root.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="baseExpression">The base expression.</param>
        /// <returns></returns>
        public static LambdaExpression GetParentPropertyExpression<TRoot, TResult>(
            this Expression<Func<TRoot, TResult>> baseExpression)
        {
            var member = (MemberExpression)baseExpression.Body;
            var lambda = Expression.Lambda(member.Expression, baseExpression.Parameters);
            return lambda;
        }

        /// <summary>
        /// Gets the referenced member.
        /// </summary>
        /// <typeparam name="TArgument">The type of the argument.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="expression">The expression.</param>
        /// <returns>Object describing referenced member.</returns>
        public static MemberInfo GetMember<TResult>(this Expression<Func<TResult>> expression)
        {
            return GetMember((LambdaExpression)expression);
        }
        /// <summary>
        /// Retrieves object describing constructor referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type constructed in expression.</typeparam>
        /// <param name="expression">Expression that references member.</param>
        /// <returns>Object describing referenced member.</returns>
        public static ConstructorInfo GetConstructor<TArgument>(this Expression<Func<TArgument>> expression)
        {
            var constructor = GetMember((LambdaExpression)expression) as ConstructorInfo;

            if (constructor == null)
            {
                throw new ArgumentException("Provided expression does not refer to constructor");
            }

            return constructor;
        }

        /// <summary>
        /// Retrieves object describing constructor referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Argument used while constructing object.</typeparam>
        /// <typeparam name="TResult">Type constructed in expression.</typeparam>
        /// <param name="expression">Expression that references member.</param>
        /// <returns>Object describing referenced member.</returns>
        public static ConstructorInfo GetConstructor<TArgument, TResult>(this Expression<Func<TArgument, TResult>> expression)
        {
            var constructor = GetMember((LambdaExpression)expression) as ConstructorInfo;

            if (constructor == null)
            {
                throw new ArgumentException("Provided expression does not refer to constructor");
            }

            return constructor;
        }

        /// <summary>
        /// Retrieves object describing static declared property referenced by expression.
        /// </summary>
        /// <remarks>
        /// This method allow only one level of property reference.
        /// </remarks>
        /// <exception cref="ArgumentException">If expression is not referencing declared
        /// property.</exception>
        /// <param name="propertyExpression">Expression referencing property.</param>
        /// <returns>Object describing referenced property.</returns>
        public static PropertyInfo GetDeclaredProperty(this Expression<Func<object>> propertyExpression)
        {
            MemberExpression body = GetMemberExpression((LambdaExpression)propertyExpression);

            if (!(body.Expression is ConstantExpression))
            {
                throw new ArgumentException("Messages.PropertyIsNotDeclaredOnTypeProvidingExpression");
            }

            return (PropertyInfo)body.Member;
        }

        /// <summary>
        /// Retrieves object describing instance, declared property referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type declaring property.</typeparam>
        /// <typeparam name="TResult">Type returned by property.</typeparam>
        /// <param name="propertyExpression">Expression that references property.</param>
        /// <exception cref="ArgumentException">If expression is not referencing given
        /// parameter.</exception>
        /// <exception cref="ArgumentException">If expression is not referencing declared
        /// property.</exception>
        /// <returns>Object describing referenced property.</returns>
        public static PropertyInfo GetDeclaredProperty<TArgument, TResult>(this Expression<Func<TArgument, TResult>> propertyExpression)
        {
            return GetDeclaredProperty(propertyExpression.Body);
        }

        /// <summary>
        /// Retrieves object describing instance, declared property referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references property.</param>
        /// <returns>Object describing referenced property.</returns>
        public static PropertyInfo GetDeclaredProperty(LambdaExpression expression)
        {
            return GetDeclaredProperty(expression.Body);
        }

        /// <summary>
        /// Retrieves object describing instance, declared property referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references property.</param>
        /// <exception cref="ArgumentException">If expression is not referencing given
        /// parameter.</exception>
        /// <exception cref="ArgumentException">If expression is not referencing declared
        /// property.</exception>
        /// <returns>Object describing referenced property.</returns>
        public static PropertyInfo GetDeclaredProperty(Expression expression)
        {
            var lamblda = expression as LambdaExpression;

            if (lamblda != null)
            {
                expression = lamblda.Body;
            }

            MemberExpression body = GetMemberExpression(expression);

            var parameterExpression = body.Expression as ParameterExpression;

            if (parameterExpression == null)
            {
                throw new ArgumentException("Expression has to refer to parameter.");
            }

            var property = body.Member as PropertyInfo;

            if (property == null)
            {
                throw new ArgumentException("Referenced member is not a property.");
            }

            return property;
        }

        /// <summary>
        /// Retrieves object describing instance, declared property referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type declaring property.</typeparam>
        /// <param name="propertyExpression">Expression that references property.</param>
        /// <returns>Object describing referenced property.</returns>
        public static PropertyInfo GetDeclaredProperty<TArgument>(this Expression<Func<TArgument, object>> propertyExpression)
        {
            return GetDeclaredProperty<TArgument, object>(propertyExpression);
        }

        /// <summary>
        /// Retrieves object describing member referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type declaring member.</typeparam>
        /// <typeparam name="TResult">Type returned by member.</typeparam>
        /// <param name="expression">Expression that references member.</param>
        /// <returns>Object describing referenced member.</returns>
        public static MemberInfo GetMember<TArgument, TResult>(this Expression<Func<TArgument, TResult>> expression)
        {
            return GetMember((LambdaExpression)expression);
        }

        /// <summary>
        /// Retrieves object describing method referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type declaring method.</typeparam>
        /// <typeparam name="TResult">Type returned by method.</typeparam>
        /// <param name="expression">Expression that references method.</param>
        /// <returns>Object describing referenced method.</returns>
        public static MethodInfo GetMethod<TArgument, TResult>(this Expression<Func<TArgument, TResult>> expression)
        {
            return GetMethod((LambdaExpression)expression);
        }

        /// <summary>
        /// Retrieves object describing static void method referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references method.</param>
        /// <returns>Object describing referenced method.</returns>
        public static MethodInfo GetMethod(this Expression<Action> expression)
        {
            return GetMethod((LambdaExpression)expression);
        }

        /// <summary>
        /// Retrieves object describing declared void method referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">>Type returned by method.</typeparam>
        /// <param name="expression">Expression that references method.</param>
        /// <returns>Object describing referenced method.</returns>
        public static MethodInfo GetMethod<TArgument>(this Expression<Action<TArgument>> expression)
        {
            return GetMethod((LambdaExpression)expression);
        }

        /// <summary>
        /// Retrieves object describing declared void method referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type declaring method.</typeparam>
        /// <typeparam name="TResult">Type returned by method.</typeparam>
        /// <param name="expression">Expression that references method.</param>
        /// <returns>Object describing referenced method.</returns>
        public static MethodInfo GetAsyncMethod<TArgument, TResult>(this Expression<Func<TArgument, Task<TResult>>> expression)
        {
            return GetMethod((LambdaExpression)expression);
        }

        /// <summary>
        /// Retrieves object describing static returning T method referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type returned by method.</typeparam>
        /// <param name="expression">Expression that references method.</param>
        /// <returns>Object describing referenced method.</returns>
        public static MethodInfo GetMethod<TArgument>(this Expression<Func<TArgument>> expression)
        {
            return GetMethod((LambdaExpression)expression);
        }

        public static Type GetDeclaringType<TArgument>(this Expression<Func<TArgument>> expression)
        {
            var body = expression.Body;
            var memberExpression = GetMemberExpression(body);

            if (memberExpression != null)
            {
                return memberExpression.Expression.Type;
            }


            var method = body as MethodCallExpression;

            if (method != null)
            {
                return ((ConstantExpression)method.Object)?.Value.GetType() ?? method.Method.DeclaringType;
            }

            if (body.NodeType == ExpressionType.New)
            {
                var newExpression = (NewExpression)body;
                return newExpression.Constructor.DeclaringType;
            }

            throw new ArgumentException("Provided expression does not refer to member.");
        }

        /// <summary>
        /// Retrieves all referenced by expression properties.
        /// </summary>
        /// <typeparam name="TArgument">Type of entity declaring first property.</typeparam>
        /// <typeparam name="TResult">Type returned by last property.</typeparam>
        /// <param name="pathExpression">Expression referencing properties.</param>
        /// <returns>Collection of objects describing properties referenced by expression.</returns>
        public static IEnumerable<PropertyInfo> GetProperties<TArgument, TResult>(this Expression<Func<TArgument, TResult>> pathExpression)
        {
            Expression expression = pathExpression.Body;

            if (expression.NodeType == ExpressionType.Convert)
            {
                var convert = (UnaryExpression)pathExpression.Body;
                expression = convert.Operand;
            }

            var propertes = new List<PropertyInfo>();

            while (expression.NodeType == ExpressionType.MemberAccess)
            {
                var memberExpression = (MemberExpression)expression;
                var property = (PropertyInfo)memberExpression.Member;
                propertes.Add(property);
                expression = memberExpression.Expression;
            }

            if (expression.NodeType != ExpressionType.Parameter
                && (expression.NodeType != ExpressionType.Convert || ((UnaryExpression)expression).Operand.NodeType != ExpressionType.Parameter))
            {
                throw new InvalidOperationException();
            }

            return propertes.Reverse<PropertyInfo>().ToArray();
        }

        /// <summary>
        /// Retrieves doted path of property referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type of entity declaring first property.</typeparam>
        /// <typeparam name="TResult">Type returned by last property.</typeparam>
        /// <param name="pathExpression">Expression referencing properties.</param>
        /// <returns>Doted string containing names of referenced properties.</returns>
        public static string GetPropertiesPath<TArgument, TResult>(this Expression<Func<TArgument, TResult>> pathExpression)
        {
            return string.Join(".", GetProperties(pathExpression).Select(x => x.Name));
        }

        public static string GetCamelCasePropertyPath<TArgument, TResult>(this Expression<Func<TArgument, TResult>> pathExpression)
        {
            return string.Join(".", GetProperties(pathExpression).Select(x => char.ToLower(x.Name[0]) + x.Name.Substring(1)));
        }

        /// <summary>
        /// Retrieves <see cref="PropertyInfo"/> describing referenced property.
        /// </summary>
        /// <typeparam name="TArgument">Type of entity that declares property.</typeparam>
        /// <typeparam name="TResult">Type returned by property.</typeparam>
        /// <param name="expression">Expression referencing property.</param>
        /// <returns>Object describing property.</returns>
        public static PropertyInfo GetProperty<TArgument, TResult>(this Expression<Func<TArgument, TResult>> expression)
        {
            return GetProperty((LambdaExpression)expression);
        }

        /// <summary>
        /// Retrieves object describing property referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references property.</param>
        /// <exception cref="ArgumentExcetion">Provided expression does not refer to
        /// property.</exception>
        /// <returns>Object describing referenced property.</returns>
        public static PropertyInfo GetProperty(Expression expression)
        {
            var lamblda = expression as LambdaExpression;

            if (lamblda != null)
            {
                expression = lamblda.Body;
            }

            var poprerty = GetMember(expression) as PropertyInfo;

            if (poprerty == null)
            {
                throw new ArgumentException("Provided expression does not refer to property.");
            }

            return poprerty;
        }

        /// <summary>
        /// Retrieves object describing property referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references property.</param>
        /// <exception cref="ArgumentExcetion">Provided expression does not refer to
        /// property.</exception>
        /// <returns>Object describing referenced property.</returns>
        public static PropertyInfo GetProperty(LambdaExpression expression)
        {
            return GetProperty(expression.Body);
        }

        /// <summary>
        /// Retrieves property name of referenced expression.
        /// </summary>
        /// <typeparam name="TArgument">Type returned by property.</typeparam>
        /// <param name="expression">Expression referencing property.</param>
        /// <returns>Name of property.</returns>
        public static string GetPropertyName<TArgument>(this Expression<Func<TArgument>> expression)
        {
            return GetProperty(expression).Name;
        }

        /// <summary>
        /// Retrieves name of property referenced by expression.
        /// </summary>
        /// <typeparam name="TArgument">Type of entity.</typeparam> <typeparam name="TResult">Return
        /// type returned by property.</typeparam>
        /// <param name="expression">Expression referencing property.</param>
        /// <returns>Name of property.</returns>
        public static string GetPropertyName<TArgument, TResult>(this Expression<Func<TArgument, TResult>> expression)
        {
            return GetProperty(expression).Name;
        }

        /// <summary>
        /// Retrieves object describing member referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references member.</param>
        /// <exception cref="ArgumentException">Provided expression does not refer to
        /// member.</exception>
        /// <returns>Object describing referenced member.</returns>
        private static MemberInfo GetMember(LambdaExpression expression)
        {
            return GetMember(expression.Body);
        }

        /// <summary>
        /// Retrieves object describing member referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references member.</param>
        /// <exception cref="ArgumentException">Provided expression does not refer to
        /// member.</exception>
        /// <returns>Object describing referenced member.</returns>
        private static MemberInfo GetMember(Expression expression)
        {
            var memberExpression = GetMemberExpression(expression);

            if (memberExpression != null)
            {
                return memberExpression.Member;
            }

            var method = expression as MethodCallExpression;

            if (method != null)
            {
                return method.Method;
            }

            if (expression.NodeType == ExpressionType.New)
            {
                var newExpression = (NewExpression)expression;
                return newExpression.Constructor;
            }

            throw new ArgumentException("Provided expression does not refer to member.");
        }

        /// <summary>
        /// Retrieves object describing member referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references member.</param>
        /// <returns>Object describing referenced member.</returns>
        private static MemberExpression GetMemberExpression(LambdaExpression expression)
        {
            return GetMemberExpression(expression.Body);
        }

        /// <summary>
        /// Retrieves object describing member referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references member.</param>
        /// <returns>Object describing referenced member.</returns>
        private static MemberExpression GetMemberExpression(Expression expression)
        {
            var lamblda = expression as LambdaExpression;

            if (lamblda != null)
            {
                expression = lamblda.Body;
            }

            UnaryExpression unaryExpression = expression as UnaryExpression;
            MemberExpression result;

            if (unaryExpression != null)
            {
                result = unaryExpression.Operand as MemberExpression;
            }
            else
            {
                result = expression as MemberExpression;
            }

            return result;
        }

        /// <summary>
        /// Retrieves object describing method referenced by expression.
        /// </summary>
        /// <param name="expression">Expression that references method.</param>
        /// <exception cref="ArgumentException">Provided expression does not refer to
        /// method.</exception>
        /// <returns>Object describing referenced method.</returns>
        private static MethodInfo GetMethod(LambdaExpression expression)
        {
            var method = GetMember((LambdaExpression)expression) as MethodInfo;

            if (method == null)
            {
                throw new ArgumentException("Provided expression does not refer to method");
            }

            return method;
        }
    }
}