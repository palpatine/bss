﻿using System;

namespace Qdarc.Modules.Common.Utils
{
    public class Disposer : IDisposable
    {
        private Action _dispose;

        public Disposer(Action dispose)
        {
            _dispose = dispose;
        }

        public void Dispose()
        {
            _dispose();
        }
    }
}