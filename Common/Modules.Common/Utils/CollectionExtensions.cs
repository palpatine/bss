﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qdarc.Modules.Common.Utils
{
    public static class CollectionExtensions
    {
        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable collection)
        {
            return collection != null ? collection.Cast<T>() : Enumerable.Empty<T>();
        }

        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> collection)
        {
            return collection ?? Enumerable.Empty<T>();
        }

        public static async Task<IEnumerable<G>> SelectManyAsync<T, G>(this IEnumerable<T> collection, Func<T, Task<IEnumerable<G>>> retriver)
        {
            List<G> result = new List<G>();

            foreach (var item in collection)
            {
                var inner = await retriver(item);
                result.AddRange(inner);
            }

            return result;
        }

        public static async Task<IEnumerable<G>> SelectAsync<T, G>(this IEnumerable<T> collection, Func<T, Task<G>> retriver)
        {
            List<G> result = new List<G>();

            foreach (var item in collection)
            {
                var inner = await retriver(item);
                result.Add(inner);
            }

            return result;
        }

        public static IEnumerable<Tuple<TFirst, TSecond>> CrossJoin<TFirst, TSecond>(
           this IEnumerable<TFirst> first,
            IEnumerable<TSecond> second)
        {
            return first.CrossJoin(second, Tuple.Create);
        }

        public static IEnumerable<TResult> CrossJoin<TFirst, TSecond, TResult>(
            this IEnumerable<TFirst> first,
            IEnumerable<TSecond> second,
            Func<TFirst, TSecond, TResult> creator)
        {
            return first.SelectMany(x => second.Select(z => creator(x, z)));
        }

        public static IEnumerable<TResult> FullJoin<TFirst, TSecond, TKey, TResult>(
            this IEnumerable<TFirst> first,
            IEnumerable<TSecond> second,
            Func<TFirst, TKey> firstKey,
            Func<TSecond, TKey> secondKey,
            Func<TFirst, TSecond, TKey, TResult> result)
        {
            var firstLookup = first.ToLookup(firstKey);
            var secondLookup = second.ToLookup(secondKey);
            var keys = new HashSet<TKey>(firstLookup.Select(p => p.Key));
            keys.UnionWith(secondLookup.Select(p => p.Key));

            return (from key in keys
                    from firstElement in firstLookup[key].DefaultIfEmpty()
                    from secondElement in secondLookup[key].DefaultIfEmpty()
                    select result(firstElement, secondElement, key)).ToList();
        }

        /// <summary>
        /// Returns collection that joins two other in full join manner.
        /// </summary>
        /// <typeparam name="TFirst">The type of the first.</typeparam>
        /// <typeparam name="TSecond">The type of the second.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <param name="firstKey">The first key.</param>
        /// <param name="secondKey">The second key.</param>
        /// <param name="result">The result.</param>
        /// <returns>Full join of both collections.</returns>
        public static IEnumerable<TResult> FullJoin<TFirst, TSecond, TKey, TResult>(
            this IEnumerable<TFirst> first,
            IEnumerable<TSecond> second,
            Func<TFirst, TKey> firstKey,
            Func<TSecond, TKey> secondKey,
            Func<TFirst, TSecond, TResult> result)
        {
            var firstLookup = first.ToLookup(firstKey);
            var secondLookup = second.ToLookup(secondKey);
            var keys = new HashSet<TKey>(firstLookup.Select(p => p.Key));
            keys.UnionWith(secondLookup.Select(p => p.Key));

            return (from key in keys
                    from firstElement in firstLookup[key].DefaultIfEmpty()
                    from secondElement in secondLookup[key].DefaultIfEmpty()
                    select result(firstElement, secondElement)).ToList();
        }

    }
}
