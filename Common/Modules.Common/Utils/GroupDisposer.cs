﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qdarc.Modules.Common.Utils
{
    public class GroupDisposer : IDisposable
    {
        private IEnumerable<IDisposable> _elements;

        public GroupDisposer(IEnumerable<IDisposable> elements)
        {
            _elements = elements.ToArray();
        }

        public GroupDisposer(params IDisposable[] elements)
        {
            _elements = elements;
        }

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            foreach (var item in _elements)
            {
                item.Dispose();
            }

            IsDisposed = true;
        }
    }
}