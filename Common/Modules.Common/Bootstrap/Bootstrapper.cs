﻿using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.Ioc;
using Qdarc.Modules.Common.QueryGeneration;

namespace Qdarc.Modules.Common.Bootstrap
{
    /// <summary>
    /// Bootstraps types located in Common Bootstrapper assembly.
    /// </summary>
    public class Bootstrapper : IBootstrapper
    {
        /// <summary>
        /// Performs initalization of part of the system.
        /// </summary>
        /// <param name="registrar">Registrar that enables IOC configuration.</param>
        public void Bootstrap(IRegistrar registrar)
        {
            var eventAggregator = registrar.Resolver.Resolve<EventAggregator>();
            registrar.RegisterInstance<IEventAggregator>(eventAggregator);
            registrar.RegisterAsSingleton<IQueryParameterValueConverterFactory, QueryParameterValueConverterFactory>();
        }

        public double Order => 0;
    }
}