﻿using Qdarc.Modules.Common.Ioc;

namespace Qdarc.Modules.Common.Bootstrap
{
    /// <summary>
    /// Abstracts all classes that are a part of system initalization process.
    /// </summary>
    public interface IBootstrapper
    {
         double Order { get; }

        /// <summary>
        /// Performs initalization of part of the system.
        /// </summary>
        /// <param name="registrar">Registrar that enables IOC configuration.</param>
        void Bootstrap(IRegistrar registrar);
    }
}