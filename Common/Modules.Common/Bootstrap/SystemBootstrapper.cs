﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Qdarc.Modules.Common.Ioc;

namespace Qdarc.Modules.Common.Bootstrap
{
    /// <summary>
    /// Botstraps system using given registrar and set of assemblies that are to be scanned for
    /// bootstrappers.
    /// </summary>
    public class SystemBootstrapper
    {
        /// <summary>
        /// The registrar that will be used to register all system parts.
        /// </summary>
        private readonly IRegistrar _registrar;

        /// <summary>
        /// The system assemblies that may contain bootstrappers.
        /// </summary>
        private readonly IEnumerable<Assembly> _systemAssemblies;

        /// <summary>
        ///
        /// </summary>
        /// <param name="registrar"></param>
        /// <param name="systemAssemblies"></param>
        public SystemBootstrapper(
            IRegistrar registrar,
            IEnumerable<Assembly> systemAssemblies)
        {
            _registrar = registrar;
            _systemAssemblies = systemAssemblies;
        }

        /// <summary>
        /// Bootstraps the system.
        /// </summary>
        public void Bootstrap()
        {
            var bootstrappers = _systemAssemblies
                .SelectMany(x => x.ExportedTypes)
                .Where(x => x.IsClass && !x.IsAbstract && typeof(IBootstrapper).IsAssignableFrom(x))
                .Select(x => Activator.CreateInstance(x))
                .Cast<IBootstrapper>();

            _registrar.RegisterInstance(_registrar);

            foreach (var item in bootstrappers.OrderBy(x => x.Order))
            {
                item.Bootstrap(_registrar);
            }
        }
    }
}