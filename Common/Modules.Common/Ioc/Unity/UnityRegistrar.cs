﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Practices.Unity;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.Modules.Common.Ioc.Unity
{
    /// <summary>
    /// Wrapper for unity container that exposes registration functionality.
    /// </summary>
    public sealed class UnityRegistrar : IRegistrar
    {
        /// <summary>
        /// Underlying unity container.
        /// </summary>
        private readonly IUnityContainer _container;

        /// <summary>
        /// The default life time manager factory.
        /// </summary>
        private readonly Func<LifetimeManager> _defaultLifetimeManagerFactory;

        /// <summary>
        /// Initializes a new instance of the UnityRegistrar class.
        /// </summary>
        /// <param name="container">Unity container.</param>
        public UnityRegistrar(IUnityContainer container)
            : this(container, () => new TransientLifetimeManager())
        {
        }

        /// <summary>
        /// Initializes a new instance of the UnityRegistrar class.
        /// </summary>
        /// <param name="container">Unity container.</param>
        /// <param name="defaultLifetimeManagerFactory">The default life time manager
        /// factory.</param>
        public UnityRegistrar(
            IUnityContainer container,
            Func<LifetimeManager> defaultLifetimeManagerFactory)
        {
            _container = container;
            _defaultLifetimeManagerFactory = defaultLifetimeManagerFactory;
            Resolver = new UnityDependencyResolver(_container);
            PerHttpRequestLifetimeManagerFactory = () => new PerHttpRequestLifetimeManager();
        }

        public Func<LifetimeManager> PerHttpRequestLifetimeManagerFactory { get; set; }

        /// <summary>
        /// Gets the resolver.
        /// </summary>
        /// <value>The resolver.</value>
        public IDependencyResolver Resolver { get; set; }

        private LifetimeManager PerHttpRequestLifetimeManager => PerHttpRequestLifetimeManagerFactory();

        /// <summary>
        /// Adds registration to container that allows resolution of IEnumerable{T} where T is any
        /// abstraction and all implementations of this abstraction are registered with specified
        /// name (e.g. via <see cref="RegisterMultiple{TA,TI}"/> helper method of this class.
        /// </summary>
        /// <remarks>
        /// Registration uses transient life time manager to not reuse collection itself but items
        /// of collection will be resolved using theirs lifetime management.
        /// </remarks>
        public void EnableEnumerableResolution()
        {
            _container.RegisterType(
                typeof(IEnumerable<>),
                new InjectionFactory((cntainer, type, name) =>
                {
                    var innerTyp = type.GenericTypeArguments[0];
                    var array = innerTyp.MakeArrayType();
                    return _container.Resolve(array);
                }));
        }

        public void RegisterAsPerHttpRequest<TAbstraction, TImplementation>()
                    where TImplementation : class, TAbstraction
        {
            _container.RegisterType<TAbstraction, TImplementation>(PerHttpRequestLifetimeManager);
        }

        /// <summary>
        /// Register single type as implementation of given abstraction and forces that only one
        /// instance of this class is created singe resoultion tree.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Intension of method is to create those object")]
        public void RegisterAsPerResolve<TAbstraction, TImplementation>() where TImplementation : class, TAbstraction
        {
            _container.RegisterType<TAbstraction, TImplementation>(new PerResolveLifetimeManager());
        }

        /// <summary>
        /// Register single type as implementation of given abstraction and forces that only one
        /// instance of this class is ever created.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Intension of method is to create those object")]
        public void RegisterAsSingleton<TAbstraction, TImplementation>() where TImplementation : class, TAbstraction
        {
            _container.RegisterType<TAbstraction, TImplementation>(new ContainerControlledLifetimeManager());
        }

        /// <summary>
        /// Register single type as implementation of given abstraction and forces that only one
        /// instance of this class is ever created.
        /// </summary>
        /// <param name="abstraction">Abstraction.</param>
        /// <param name="implementation">Implementation.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Intension of method is to create those object")]
        public void RegisterAsSingleton(
            Type abstraction,
            Type implementation)
        {
            _container.RegisterType(abstraction, implementation, new ContainerControlledLifetimeManager());
        }

        /// <summary>
        /// Registers instance as given abstraction.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <param name="instance">Instance to register.</param>
        public void RegisterInstance<TAbstraction>(TAbstraction instance)
        {
            _container.RegisterInstance(instance);
        }

        /// <summary>
        /// Registers the instance per HTTP request.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <param name="instance">Instance to register.</param>
        public void RegisterInstancePerHttpRequest<TAbstraction>(TAbstraction instance)
        {
            _container.RegisterInstance(instance, PerHttpRequestLifetimeManager);
        }

        /// <summary>
        /// Registers implementation as one of many implementing given abstraction. Ti is designed
        /// to be be resolved as item of IEnumerable&lt;TAbstraction&gt;.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        public void RegisterMultiple<TAbstraction, TImplementation>() where TImplementation : class, TAbstraction
        {
            _container.RegisterType<TAbstraction, TImplementation>(Guid.NewGuid().ToString(), CreateDefaultLifeTimeManager());
        }

        /// <summary>
        /// Registers implementation as one of many implementing given abstraction. Ti is designed
        /// to be be resolved as item of IEnumerable&lt;TAbstraction&gt;.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        /// <param name="instance">The instance.</param>
        public void RegisterMultiple<TAbstraction>(TAbstraction instance)
        {
            _container.RegisterInstance<TAbstraction>(Guid.NewGuid().ToString(), instance);
        }

        /// <summary>
        /// Registers implementation as one of many implementing given abstraction. It is designed to
        /// be resolved as factory method that will produce instance marked with given key.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">The type of the implementation.</typeparam>
        /// <param name="key">The key.</param>
        public void RegisterMultiple<TAbstraction, TImplementation>(string key)
            where TImplementation : class, TAbstraction
        {
            _container.RegisterType<TAbstraction, TImplementation>(key);
        }

        /// <summary>
        /// Registers given resolution factory.
        /// </summary>
        /// <param name="abstraction">Type handled by factory.</param>
        /// <param name="factory">The factory.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Intension of method is to create those object")]
        public void RegisterResolutionFactory(
            Type abstraction,
            Func<IDependencyResolver, Type, object> factory,
            string name = null)
        {
            _container.RegisterType(
                abstraction,
                name,
                CreateDefaultLifeTimeManager(),
                new InjectionFactory((x, t, n) => factory(new UnityDependencyResolver(x), t)));
        }

        /// <summary>
        /// Registers single implementation of given abstraction.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        public void RegisterSingle<TAbstraction, TImplementation>() where TImplementation : class, TAbstraction
        {
            _container.RegisterType<TAbstraction, TImplementation>(CreateDefaultLifeTimeManager());
        }

        /// <summary>
        /// Registers single implementation of given abstraction.
        /// </summary>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        public void RegisterSingle<TImplementation>() where TImplementation : class
        {
            _container.RegisterType<TImplementation, TImplementation>(CreateDefaultLifeTimeManager());
        }

        /// <summary>
        /// Registers single implementation of given abstraction.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        /// <param name="constructorExpression">The expression that points to constructor that is to
        /// be used while resolving.</param>
        public void RegisterSingle<TAbstraction, TImplementation>(Expression<Func<TImplementation>> constructorExpression)
            where TImplementation : class, TAbstraction
        {
            _container.RegisterType<TAbstraction, TImplementation>();
            var constructor = constructorExpression.GetConstructor();
            var constructorParametersTypes = constructor.GetParameters().Select(x => x.ParameterType).ToArray();
            _container.RegisterType<TAbstraction, TImplementation>(
                CreateDefaultLifeTimeManager(),
                new InjectionConstructor(constructorParametersTypes));
        }

        /// <summary>
        /// Registers single implementation of given abstraction.
        /// </summary>
        /// <param name="abstraction">Abstraction.</param>
        /// <param name="implementation">Implementation.</param>
        public void RegisterSingle(
            Type abstraction,
            Type implementation)
        {
            _container.RegisterType(abstraction, implementation, CreateDefaultLifeTimeManager());
        }

        /// <summary>
        /// Registers given resolution factory and forces that only one instance is ever created.
        /// </summary>
        /// <param name="abstraction">Type handled by factory.</param>
        /// <param name="factory">The factory.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Intension of method is to create those object")]
        public void RegisterSingletonResolutionFactory(
            Type abstraction,
            Func<IDependencyResolver, Type, object> factory)
        {
            _container.RegisterType(
                abstraction,
                new ContainerControlledLifetimeManager(),
                new InjectionFactory((x, t, n) => factory(new UnityDependencyResolver(x), t)));
        }

        /// <summary>
        /// Creates the default life time manager.
        /// </summary>
        /// <returns>Life time manager.</returns>
        private LifetimeManager CreateDefaultLifeTimeManager()
        {
            var manager = _defaultLifetimeManagerFactory();
            return manager;
        }
    }
}