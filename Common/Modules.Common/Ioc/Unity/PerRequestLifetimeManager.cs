using System;
using System.Web;
using Microsoft.Practices.Unity;

namespace Qdarc.Modules.Common.Ioc.Unity
{
    public class PerHttpRequestLifetimeManager : LifetimeManager
    {
        private readonly string _key
            = string.Format("RequestLifeTimeManager_{0}", Guid.NewGuid());

        public PerHttpRequestLifetimeManager()
        {
        }
            
        public override object GetValue()
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items.Contains(_key))
                {
                    return HttpContext.Current.Items[_key];
                }
            }
            else
            {
                throw new InvalidOperationException("No context");
            }

            return null;
        }

        public override void RemoveValue()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items.Remove(_key);
            }
        }

        public override void SetValue(object newValue)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items[_key] = newValue;
            }
        }
    }
}