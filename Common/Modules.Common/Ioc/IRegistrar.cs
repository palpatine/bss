﻿using System;
using System.Linq.Expressions;

namespace Qdarc.Modules.Common.Ioc
{ /// <summary>
    /// Enables dependency registrations.
    /// </summary>
    public interface IRegistrar
    {
        /// <summary>
        /// Gets the resolver.
        /// </summary>
        /// <value>The resolver.</value>
        IDependencyResolver Resolver { get; }

        /// <summary>
        /// Register single type as implementation of given abstraction and forces that only one
        /// instance of this class is created singe resoultion tree.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        void RegisterAsPerResolve<TAbstraction, TImplementation>()
            where TImplementation : class, TAbstraction;

        /// <summary>
        /// Register single type as implementation of given abstraction and forces that only one
        /// instance of this class is ever created.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        void RegisterAsSingleton<TAbstraction, TImplementation>()
            where TImplementation : class, TAbstraction;

        /// <summary>
        /// Register single type as implementation of given abstraction and forces that only one
        /// instance of this class is ever created.
        /// </summary>
        /// <param name="abstraction">Abstraction.</param>
        /// <param name="implementation">Implementation.</param>
        void RegisterAsSingleton(
            Type abstraction,
            Type implementation);

        /// <summary>
        /// Registers given instance as implementation of given abstraction.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <param name="instance">Instance.</param>
        void RegisterInstance<TAbstraction>(TAbstraction instance);

        /// <summary>
        /// Registers the instance per HTTP request.
        /// </summary>
        /// <typeparam name="TAbstraction">The type of the abstraction.</typeparam>
        /// <param name="instance">The instance.</param>
        void RegisterInstancePerHttpRequest<TAbstraction>(TAbstraction instance);

        /// <summary>
        /// Registers one of many types that implement given abstraction.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        void RegisterMultiple<TAbstraction, TImplementation>()
            where TImplementation : class, TAbstraction;

        /// <summary>
        /// Registers implementation as one of many implementing given abstraction. It is designed
        /// to be be resolved as item of IEnumerable&lt;TAbstraction&gt;.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        void RegisterMultiple<TAbstraction>(TAbstraction instance);

        /// <summary>
        /// Registers implementation as one of many implementing given abstraction. It is designed to
        /// be resolved as factory method that will produce instance marked with given key.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">The type of the implementation.</typeparam>
        /// <param name="key">The key.</param>
        void RegisterMultiple<TAbstraction, TImplementation>(string key)
                    where TImplementation : class, TAbstraction;

        /// <summary>
        /// Registers given resolution factory.
        /// </summary>
        /// <param name="abstraction">Type handled by factory.</param>
        /// <param name="factory">The factory.</param>
        /// <param name="name">The name.</param>
        void RegisterResolutionFactory(
            Type abstraction,
            Func<IDependencyResolver, Type, object> factory,
            string name = null);

        /// <summary>
        /// Register single type as implementation of given abstraction.
        /// </summary>
        /// <param name="abstraction">Abstraction.</param>
        /// <param name="implementation">Implementation.</param>
        void RegisterSingle(
            Type abstraction,
            Type implementation);

        /// <summary>
        /// Register single type as implementation of given abstraction.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        void RegisterSingle<TAbstraction, TImplementation>()
            where TImplementation : class, TAbstraction;

        /// <summary>
        /// Register single type as implementation of given abstraction.
        /// </summary>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        void RegisterSingle<TImplementation>()
            where TImplementation : class;

        /// <summary>
        /// Registers single implementation of given abstraction.
        /// </summary>
        /// <typeparam name="TAbstraction">Abstraction.</typeparam>
        /// <typeparam name="TImplementation">Implementation.</typeparam>
        /// <param name="constructorExpression">The expression that points to constructor that is to
        /// be used while resolving.</param>
        void RegisterSingle<TAbstraction, TImplementation>(Expression<Func<TImplementation>> constructorExpression)
            where TImplementation : class, TAbstraction;

        /// <summary>
        /// Registers given resolution factory. And forces that only one instance is ever created.
        /// </summary>
        /// <param name="abstraction">Type handled by factory.</param>
        /// <param name="factory">The factory.</param>
        void RegisterSingletonResolutionFactory(
            Type abstraction,
            Func<IDependencyResolver, Type, object> factory);

        void RegisterAsPerHttpRequest<TAbstraction, TImplementation>()
            where TImplementation : class, TAbstraction;
    }
}