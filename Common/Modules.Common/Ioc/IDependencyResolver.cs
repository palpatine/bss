﻿using System;
using System.Collections.Generic;

namespace Qdarc.Modules.Common.Ioc
{
    /// <summary>
    /// Interface of dependency resolver.
    /// </summary>
    public interface IDependencyResolver
    {
        /// <summary>
        /// Inject object of T type.
        /// </summary>
        /// <typeparam name="T">Type parameter.</typeparam>
        /// <param name="existing">Instance of existing object.</param>
        void Inject<T>(T existing);

        /// <summary>
        /// Register a new instance of T.
        /// </summary>
        /// <typeparam name="T">Type parameter.</typeparam>
        /// <param name="instance">Instance of T type.</param>
        void Register<T>(T instance);

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <param name="type">Type of dependence.</param>
        /// <returns>Resolved object.</returns>
        T Resolve<T>(Type type);

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <param name="type">Type of dependence.</param>
        /// <param name="name">Type name.</param>
        /// <returns>
        /// Resolved object.
        /// </returns>
        object Resolve(
            Type type,
            string name);

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <returns>Resolved object.</returns>
        T Resolve<T>();

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <param name="name">Type name.</param>
        /// <returns>Resolved object.</returns>
        T Resolve<T>(string name);

        /// <summary>
        /// Resolve a dependences.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <returns>Collection of resolved objects.</returns>
        IEnumerable<T> ResolveAll<T>();

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <returns>Resolved object.</returns>
        object Resolve(Type resultType);
    }
}