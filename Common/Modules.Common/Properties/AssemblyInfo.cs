﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Qdarc.Modules.Common")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Qdarc.Modules.Common")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("58c3dba2-b10e-4f31-8aa0-f9450ebf67b0")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
#if DEBUG
[assembly: InternalsVisibleTo("Modules.Common.Tests, PublicKey=00240000048000009400000006020000002400005253413100040000010001000ffbbc9220859ca0d52eff70299c390772e1b1a30d83b8472869d7598415b9014e77fcdb3fcd089b3671872d8bfc1fe6993ed0583ed344627c9e41b626192fe979f81b0f6353f174fa43594354f86595c78b6a4af46d0b6cfcd77a89b16bffb3cc7057cd664563af76f13582d8ccc2fc341b32554534f70af6c842b40f0d1fbe")]
#endif