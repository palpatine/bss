﻿using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    public sealed class AdminAliasUser : IUser
    {
        public int Id { get; set; }
    }
}