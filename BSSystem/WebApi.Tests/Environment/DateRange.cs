﻿using System;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    public sealed class DateRange
    {
        public DateTime DateStart { get; set; }

        public DateTime? DateEnd { get; set; }
    }
}