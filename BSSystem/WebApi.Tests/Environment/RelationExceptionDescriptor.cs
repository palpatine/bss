﻿using System;
using System.Reflection;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    public class RelationExceptionDescriptor
    {
        public RelationExceptionDescriptor(Type entityType, Type relationType, MethodInfo testMethod)
        {
            EntityType = entityType;
            RelationType = relationType;
            TestMethod = testMethod;
        }

        public Type EntityType { get; }

        public Type RelationType { get; }

        public MethodInfo TestMethod { get; }
    }
}