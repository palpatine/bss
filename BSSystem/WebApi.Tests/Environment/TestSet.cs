using System;
using System.Reflection;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    public class TestSet
    {
        public Type EntityType { get; set; }

        public string Name { get; set; }
        public Type Relation { get; set; }
        public string EntityName { get; set; }
        public string RelationName { get; set; }
    }
}