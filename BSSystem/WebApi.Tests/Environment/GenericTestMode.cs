namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    public enum GenericTestMode
    {
        Undefined,
        OnePerController,
        OnePerControllerAndSlave,
        OnePerControllerAndMaster,
        Inherit,
        OnePerControllerAndAssignmentJunctionRelatedEntity
    }
}