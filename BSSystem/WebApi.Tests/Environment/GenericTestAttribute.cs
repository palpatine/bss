using System;
using System.Collections.Generic;
using System.Linq;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes")]
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class GenericTestAttribute : Attribute
    {
        public GenericTestAttribute(GenericTestMode mode, string nameFormat)
        {
            Mode = mode;
            NameFormat = nameFormat;
            RelationExceptions = Enumerable.Empty<RelationExceptionDescriptor>();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Justification = "Attributes do not accept collections")]
        public Type[] EntityRestrictions { get; set; }

        public GenericTestMode Mode { get; }

        public string NameFormat { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Justification = "Attributes do not accept collections")]
        public Type[] RelationRestrictions { get; set; }

        public IEnumerable<RelationExceptionDescriptor> RelationExceptions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Justification = "Attributes do not accept collections")]
        public Type[] EntityFilters { get; set; }
    }
}