﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class BaseTests
    {
        public static IEnumerable<DateRange> CreateDateRange(Func<int, DateTime> converter)
        {
            var datesMask = new[]
            {
                new { Start = -2, End = (int?)-1 },
                new { Start = -1, End = (int?)null },
                new { Start = -1, End = (int?)0 },
                new { Start = -1, End = (int?)1 },
                new { Start = 0, End = (int?)null },
                new { Start = 1, End = (int?)null },
                new { Start = 1, End = (int?)2 },
            };

            return datesMask.Select(
                     x =>
                         new DateRange
                         {
                             DateStart = converter(x.Start),
                             DateEnd = x.End.HasValue ? converter(x.End.Value) : (DateTime?)null
                         })
                         .ToArray();
        }

        protected static async Task CreateAsync()
        {
            var database = ServiceLocator.Current.GetInstance<Database>();
            await database.InitAsync();

            var sessionRespository = ServiceLocator.Current.GetInstance<IEntityRepository<Session, ISession>>();
            sessionRespository.Write(ServiceLocator.Current.GetInstance<Session>());
        }
    }
}