using System;
using System.Collections.Generic;
using System.Linq;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    internal class TestsCategory
    {
        public GenericTestAttribute Descriptor { get; set; }

        public IEnumerable<TestsCategory> SubCategories { get; set; }

        public IEnumerable<TestSet> TestSets { get; set; }

        public Type Type { get; set; }

        public TestsCategory Parent { get; set; }

        public IEnumerable<TestSet> ReservedTestSets
        {
            get { return SubCategories.SelectMany(x => x.ReservedTestSets).Concat(TestSets ?? Enumerable.Empty<TestSet>()); }
        }

        public GenericTestMode RootMode => Descriptor.Mode == GenericTestMode.Inherit ? Parent.RootMode : Descriptor.Mode;
    }
}