using System;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    internal interface IEntityFilter
    {
        bool CanUse(Type entity, Type relation);
    }
}