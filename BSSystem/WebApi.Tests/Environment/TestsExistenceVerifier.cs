﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    [TestClass]
    public class TestsExistenceVerifier
    {
        private readonly Assembly[] _abstractionAssemblies = { typeof(IUser).Assembly, typeof(ITopic).Assembly };
        private readonly Assembly _assembly = typeof(TestsExistenceVerifier).Assembly;
        private readonly Type[] _baseControllers = { typeof(EditApiController<,,,>), typeof(EditApiController<,,>), typeof(EditScopedApiController<,,>), typeof(EditScopedApiController<,,,>) };
        private readonly IEnumerable<TypeInfo> _controllers;
        private readonly TypeInfo[] _entities;
        private readonly PluralizationService _pluralizer = PluralizationService.CreateService(new CultureInfo("en-US"));

        private string _scopePropertyName = ExpressionExtensions.GetPropertyName((IScopedEntity p) => p.Scope);

        public TestsExistenceVerifier()
        {
            _entities = _abstractionAssemblies.SelectMany(x => x.DefinedTypes.Where(z => z.IsClass && !z.IsAbstract && typeof(Entity).IsAssignableFrom(z))).ToArray();
            _controllers = _baseControllers[0].Assembly.DefinedTypes
                .Where(x => x.IsClass && !x.IsAbstract && x.BaseType != null && x.BaseType.IsGenericType && _baseControllers.Any(y => y == x.BaseType.GetGenericTypeDefinition())).ToArray();
        }

        [TestMethod]
        public void VerifyTestsExistence()
        {
            var testsBasicClass = _assembly.DefinedTypes
                .Where(x => x.IsAbstract && typeof(BaseApiTests).IsAssignableFrom(x))
                .ToArray();

            var categories = CreateCategories(testsBasicClass);

            foreach (var testsCategory in categories)
            {
                Process(null, testsCategory);
            }

            var log = new StringBuilder();
            var hasError = categories.Aggregate(false, (current, testsCategory) => current | Verify(log, testsCategory));

            if (hasError)
            {
                Assert.Fail(log.ToString());
            }
        }

        private static bool AreEntitiesReduced(TestsCategory category, Type[] entityArguments)
        {
            var areEntitiesReduced = false;

            if (category.Descriptor.Mode == GenericTestMode.Inherit)
            {
                if (category.Type.BaseType == null)
                {
                    throw new InvalidOperationException();
                }

                var baseEntityArguments = category.Type.BaseType.GetGenericArguments()
                    .Where(x => x.GetGenericParameterConstraints().Any(z => typeof(Entity).IsAssignableFrom(z)))
                    .ToArray();
                areEntitiesReduced = entityArguments.Count() < baseEntityArguments.Count();
            }

            return areEntitiesReduced;
        }

        private static IEnumerable<TestSet> FilterForRestrictionOnEntity(IEnumerable<TestSet> testSets, TestsCategory category, bool areEntitiesReduced)
        {
            IEnumerable<TestSet> subTestSets;

            if (areEntitiesReduced)
            {
                subTestSets = testSets
                    .Where(x => x.EntityType == x.Relation && category.Descriptor.EntityRestrictions.All(z => z.IsAssignableFrom(x.EntityType)))
                    .ToArray();
            }
            else
            {
                subTestSets = testSets
                    .Where(x => category.Descriptor.EntityRestrictions.All(z => z.IsAssignableFrom(x.EntityType)))
                    .ToArray();
            }

            return subTestSets;
        }

        private static IEnumerable<TestSet> FilterForRestrictionsOnBothTypes(IEnumerable<TestSet> testSets, TestsCategory category)
        {
            IEnumerable<TestSet> subTestSets = testSets.Where(
                x => category.Descriptor.EntityRestrictions.All(z => z.IsAssignableFrom(x.EntityType))
                     && category.Descriptor.RelationRestrictions.All(z => z.IsAssignableFrom(x.Relation)))
                .ToArray();
            return subTestSets;
        }

        private static IEnumerable<TestSet> FilterForSingleGenericType(IEnumerable<TestSet> testSets, bool areEntitiesReduced, Type[] entityArguments)
        {
            IEnumerable<TestSet> subTestSets;

            if (areEntitiesReduced)
            {
                subTestSets = testSets
                    .Where(x => x.EntityType == x.Relation && entityArguments.Single().GetGenericParameterConstraints().All(z => z.IsGenericParameter || z.IsAssignableFrom(x.EntityType)))
                    .ToArray();
            }
            else
            {
                subTestSets = testSets
                    .Where(x => entityArguments.Single().GetGenericParameterConstraints().All(z => z.IsGenericParameter || z.IsAssignableFrom(x.EntityType)))
                    .ToArray();
            }

            return subTestSets;
        }

        private static IEnumerable<TestSet> FilterForTwoGenericTypes(IEnumerable<TestSet> testSets, Type[] entityArguments)
        {
            IEnumerable<TestSet> subTestSets = testSets.Where(x =>
                entityArguments.First().GetGenericParameterConstraints().All(z => z.IsGenericParameter || z.IsAssignableFrom(x.EntityType))
                && entityArguments.Last().GetGenericParameterConstraints().All(z => z.IsGenericParameter || z.IsAssignableFrom(x.Relation)))
                .ToArray();
            return subTestSets;
        }

        private static IEnumerable<TestSet> FilterTestSets(IEnumerable<TestSet> testSets, TestsCategory category)
        {
            var entityArguments = category.Type.GetGenericArguments()
                .Where(x => x.GetGenericParameterConstraints().Any(z => typeof(Entity).IsAssignableFrom(z)))
                .ToArray();

            var areEntitiesReduced = AreEntitiesReduced(category, entityArguments);

            IEnumerable<TestSet> subTestSets;
            if (entityArguments.Length == 1)
            {
                subTestSets = FilterForSingleGenericType(testSets, areEntitiesReduced, entityArguments);
            }
            else if (entityArguments.Length >= 2)
            {
                subTestSets = FilterForTwoGenericTypes(testSets, entityArguments.Take(2).ToArray());
            }
            else if (category.Descriptor.RelationRestrictions != null)
            {
                subTestSets = FilterForRestrictionsOnBothTypes(testSets, category);
            }
            else if (category.Descriptor.EntityRestrictions != null)
            {
                subTestSets = FilterForRestrictionOnEntity(testSets, category, areEntitiesReduced);
            }
            else
            {
                throw new InvalidOperationException(category.Type.ToString());
            }

            if (category.RootMode == GenericTestMode.OnePerControllerAndSlave
                || category.RootMode == GenericTestMode.OnePerControllerAndMaster
                || category.RootMode == GenericTestMode.OnePerControllerAndAssignmentJunctionRelatedEntity)
            {
                subTestSets = FilterRelationExceptions(subTestSets, category).ToArray();
            }

            return subTestSets;
        }

        private static IEnumerable<TestSet> FilterRelationExceptions(IEnumerable<TestSet> testSets, TestsCategory category)
        {
            return testSets
                .Where(x => category.Descriptor.RelationExceptions.All(z => z.EntityType != x.EntityType || z.RelationType != x.Relation));
        }

        private static ICollection<TestSet> FilterTestSets(GenericTestAttribute attribute, ICollection<TestSet> descriptors)
        {
            if (attribute.EntityFilters == null)
            {
                return descriptors;
            }

            return attribute.EntityFilters
                .Select(entityFilter => (IEntityFilter)Activator.CreateInstance(entityFilter))
                .Aggregate(descriptors, (current, filter) => current.Where(x => filter.CanUse(x.EntityType, x.Relation))
                    .ToArray());
        }

        private bool CanUseAsAssignmentJunctionRelatedWithType(GenericTestAttribute attribute, Type entityType, Type entityMarkerType, TypeInfo testedEntityType)
        {
            return typeof(IAssignmentJunctionEntity).IsAssignableFrom(testedEntityType)
                && testedEntityType != typeof(RoleDefinition)
                && !attribute.RelationExceptions.Any(q => q.EntityType == entityType && testedEntityType.IsAssignableFrom(q.RelationType))
                && testedEntityType.DeclaredProperties.Any(z => (!typeof(IScopedEntity).IsAssignableFrom(testedEntityType) || z.Name != _scopePropertyName) && z.PropertyType == entityMarkerType);
        }

        private ICollection<TestsCategory> CreateCategories(ICollection<TypeInfo> testsBasicClass)
        {
            return CreateCategories(typeof(object), testsBasicClass);
        }

        private ICollection<TestsCategory> CreateCategories(
            Type node,
            ICollection<TypeInfo> testsBasicClass)
        {
            var roots = testsBasicClass
                .Where(
                    x => x.BaseType != null
                         && (x.BaseType == node || (node.IsGenericType && x.BaseType.IsGenericType && node.GetGenericTypeDefinition() == x.BaseType.GetGenericTypeDefinition())))
                .ToArray();
            return roots.Select(x => CreateTestCategory(testsBasicClass, x)).ToArray();
        }

        private TestsCategory CreateTestCategory(ICollection<TypeInfo> testsBasicClass, TypeInfo type)
        {
            var subCategories = CreateCategories(type, testsBasicClass);

            var category = new TestsCategory
            {
                Type = type,
                Descriptor = type.GetCustomAttribute<GenericTestAttribute>(),
                SubCategories = subCategories
            };

            foreach (var testsCategory in subCategories)
            {
                testsCategory.Parent = category;
            }

            return category;
        }

        private Type FindOtherAssignmentType(TypeInfo junctionType, Type markerType)
        {
            var otherRelatedEntityProperty = junctionType.DeclaredProperties
                .Where(
                    z => typeof(IMarker).IsAssignableFrom(z.PropertyType)
                         && (!typeof(IScopedEntity).IsAssignableFrom(junctionType) || z.Name != _scopePropertyName)
                         && z.PropertyType != typeof(IRoleDefinition)
                         && z.PropertyType != markerType);
            return StorableHelper.GetEntityType(otherRelatedEntityProperty.Single().PropertyType);
        }

        private ICollection<TestSet> GenerateForOnePerController(GenericTestAttribute attribute)
        {
            ICollection<TestSet> descriptors = _controllers
                .Select(
                    x =>
                    {
                        if (x.BaseType == null)
                        {
                            throw new InvalidOperationException();
                        }

                        var entity = x.BaseType.GetGenericArguments()[0];
                        var entityName = x.Name.Substring(0, x.Name.IndexOf("Controller", StringComparison.Ordinal));
                        return new TestSet
                        {
                            EntityName = entityName,
                            EntityType = entity
                        };
                    })
                .ToArray();

            return FilterTestSets(attribute, descriptors);
        }

        private ICollection<TestSet> GenerateForOnePerControllerAndMaster(GenericTestAttribute attribute)
        {
            ICollection<TestSet> descriptors = _controllers.SelectMany(
                x =>
                {
                    if (x.BaseType == null)
                    {
                        throw new InvalidOperationException();
                    }

                    var entity = x.BaseType.GetGenericArguments()[0];
                    var isScoped = typeof(IScopedEntity).IsAssignableFrom(entity);
                    var relations = entity.GetTypeInfo().DeclaredProperties
                        .Where(z => (!isScoped || z.Name != _scopePropertyName)
                                    && !attribute.RelationExceptions.Any(y => y.EntityType == entity && z.PropertyType.IsAssignableFrom(y.RelationType))
                                    && typeof(IMarker).IsAssignableFrom(z.PropertyType));

                    var entityName = x.Name.Substring(0, x.Name.IndexOf("Controller", StringComparison.Ordinal));
                    return relations.Select(z => new TestSet
                    {
                        EntityName = entityName,
                        EntityType = entity,
                        Relation = StorableHelper.GetEntityType(z.PropertyType),
                        RelationName = z.Name
                    });
                }).ToArray();
            return FilterTestSets(attribute, descriptors);
        }

        private ICollection<TestSet> GenerateForOnePerControllerAndSlave(GenericTestAttribute attribute)
        {
            ICollection<TestSet> descriptors = _controllers.SelectMany(
                x =>
                {
                    if (x.BaseType == null)
                    {
                        throw new InvalidOperationException();
                    }

                    var entity = x.BaseType.GetGenericArguments()[0];
                    var markerType = StorableHelper.GetEntityIndicator(entity);
                    var relations = _entities
                        .Where(y => !attribute.RelationExceptions.Any(q => q.EntityType == entity && y.IsAssignableFrom(q.RelationType)))
                        .SelectMany(y => y.DeclaredProperties.Where(z => (!typeof(IScopedEntity).IsAssignableFrom(y) || z.Name != _scopePropertyName) && z.PropertyType == markerType));

                    var entityName = x.Name.Substring(0, x.Name.IndexOf("Controller", StringComparison.Ordinal));
                    return relations.Select(z => new TestSet
                    {
                        EntityName = entityName,
                        EntityType = entity,
                        Relation = z.DeclaringType,
                        RelationName = z.DeclaringType.Name + (z.Name != entity.Name ? z.Name : string.Empty)
                    });
                }).ToArray();
            return FilterTestSets(attribute, descriptors);
        }

        private ICollection<TestSet> GenerateOnePerControllerAndAssignmentJunctionRelatedEntity(GenericTestAttribute attribute)
        {
            ICollection<TestSet> descriptors = _controllers.SelectMany(
               x =>
               {
                   if (x.BaseType == null)
                   {
                       throw new InvalidOperationException();
                   }

                   var entity = x.BaseType.GetGenericArguments()[0];
                   if (entity == typeof(RoleDefinition))
                   {
                       return Enumerable.Empty<TestSet>();
                   }

                   var markerType = StorableHelper.GetEntityIndicator(entity);
                   var relations = _entities
                       .Where(y => CanUseAsAssignmentJunctionRelatedWithType(attribute, entity, markerType, y))
                       .Select(y => FindOtherAssignmentType(y, markerType));

                   var entityName = x.Name.Substring(0, x.Name.IndexOf("Controller", StringComparison.Ordinal));
                   return relations.Select(z => new TestSet
                   {
                       EntityName = entityName,
                       EntityType = entity,
                       Relation = z,
                       RelationName = _pluralizer.Pluralize(z.Name)
                   });
               }).ToArray();
            return FilterTestSets(attribute, descriptors);
        }

        private void Process(
            ICollection<TestSet> testSets,
            TestsCategory category)
        {
            var attribute = category.Descriptor;

            if (attribute == null && testSets == null)
            {
                foreach (var testsCategory in category.SubCategories)
                {
                    Process(null, testsCategory);
                }

                category.TestSets = Enumerable.Empty<TestSet>();
                return;
            }

            if ((attribute != null && attribute.Mode == GenericTestMode.Inherit) ^ testSets != null)
            {
                throw new InvalidOperationException();
            }

            if (attribute.Mode != GenericTestMode.Inherit)
            {
                if (attribute.Mode == GenericTestMode.OnePerController)
                {
                    testSets = GenerateForOnePerController(attribute);
                }
                else if (attribute.Mode == GenericTestMode.OnePerControllerAndSlave)
                {
                    testSets = GenerateForOnePerControllerAndSlave(attribute);
                }
                else if (attribute.Mode == GenericTestMode.OnePerControllerAndMaster)
                {
                    testSets = GenerateForOnePerControllerAndMaster(attribute);
                }
                else if (attribute.Mode == GenericTestMode.OnePerControllerAndAssignmentJunctionRelatedEntity)
                {
                    testSets = GenerateOnePerControllerAndAssignmentJunctionRelatedEntity(attribute);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }

            testSets = FilterTestSets(testSets, category).ToArray();

            foreach (var testsCategory in category.SubCategories)
            {
                Process(testSets, testsCategory);
            }

            var remainingTestSets = testSets.Except(category.ReservedTestSets).ToArray();
            foreach (var remainingTestSet in remainingTestSets)
            {
                remainingTestSet.Name = string.Format(attribute.NameFormat, remainingTestSet.EntityName, remainingTestSet.RelationName);
            }

            category.TestSets = remainingTestSets;
        }

        private bool Verify(StringBuilder log, TestsCategory category)
        {
            var hasError = false;
            foreach (var set in category.TestSets)
            {
                var entityModuleName = StorableHelper.GetSchemaName(set.EntityType);
                var testClassNamespace = $"SolvesIt.BSSystem.WebApi.Tests.Bss.{entityModuleName}.{_pluralizer.Pluralize(set.EntityType.Name)}";
                var type = _assembly.DefinedTypes.SingleOrDefault(x => x.Namespace == testClassNamespace && x.Name == set.Name);
                if (type?.BaseType == null || (type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() != category.Type) || (!type.BaseType.IsGenericType && type.BaseType != category.Type))
                {
                    if (!hasError)
                    {
                        hasError = true;
                        log.AppendLine("-------");
                        log.AppendFormat("For base class {0}", category.Type.Name);
                        log.AppendLine();
                    }

                    log.Append(type == null ? "\tcreate" : "\tfix base");
                    log.AppendFormat(" {0} in {1}", set.Name, testClassNamespace);
                    log.AppendLine();
                }
            }

            return category.SubCategories.Aggregate(hasError, (current, subCategory) => current | Verify(log, subCategory));
        }
    }
}