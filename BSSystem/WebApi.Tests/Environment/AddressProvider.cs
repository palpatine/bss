﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Web.Extensions.Route;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    public static class AddressProvider
    {
        public static Uri Get<TController, TResult>(Expression<Func<TController, TResult>> action)
            where TController : ApiController
        {
            var actionMethod = action.GetMethod();
            var controller = typeof(TController).Name.Replace("Controller", string.Empty);
            string area;

            if (typeof(TController).Namespace.Contains("Web.Controllers"))
            {
                area = string.Empty;
            }
            else
            {
                var areaMatch = Regex.Match(typeof(TController).Namespace, @"Areas\.(.*?)\.Controllers");
                if (!areaMatch.Success)
                {
                    throw new InvalidOperationException("Controller namespace does not follow required pattern.");
                }

                area = areaMatch.Groups[1].Value;
            }

            var route = actionMethod.GetParameters().Any(x => x.Name == "id") ? string.Empty : "List";

            var call = (MethodCallExpression)action.Body;
            var postCreationReplacements = new Dictionary<string, string>();

            using (var enumerator = call.Arguments.GetEnumerator())
            {
                foreach (var parameter in call.Method.GetParameters())
                {
                    enumerator.MoveNext();

                    if (parameter.ParameterType == typeof(HttpRequest))
                    {
                        continue;
                    }

                    if (enumerator.Current.NodeType == ExpressionType.Constant)
                    {
                        var value = ((ConstantExpression)enumerator.Current).Value;
                        if (value != null)
                        {
                            postCreationReplacements.Add(parameter.Name, value.ToString());
                        }
                    }
                    else
                    {
                        var expression = enumerator.Current as UnaryExpression;
                        if (expression != null && expression.Operand.Type == typeof(IRouteParameter))
                        {
                            var convert = expression;
                            var lambda = Expression.Lambda<Func<IRouteParameter>>(
                                Expression.Convert(convert.Operand, typeof(IRouteParameter)));
                            var function = lambda.Compile();
                            var endValue = function().Value;
                            postCreationReplacements.Add(parameter.Name, endValue);
                        }
                        else
                        {
                            var lambda = Expression.Lambda<Func<object>>(
                                Expression.Convert(enumerator.Current, typeof(object)));
                            var function = lambda.Compile();
                            var endValue = function();
                            postCreationReplacements.Add(parameter.Name, endValue.ToString());
                        }
                    }
                }
            }

            var baseAddres = new Uri(ConfigurationManager.AppSettings["TestInstanceUri"] + "/api/");
            var current = baseAddres;
            if (!string.IsNullOrEmpty(area))
            {
                current = new Uri(current, area + "/");
            }

            if (!string.IsNullOrEmpty(route))
            {
                current = new Uri(current, route + "/");
            }

            current = new Uri(current, controller + "/");

            if (controller == "Assignments")
            {
                current = new Uri(current, actionMethod.Name + "/" + postCreationReplacements["master"] + "/" + postCreationReplacements["id"] + "/" + postCreationReplacements["related"] + "/");
            }
            else
            {
                if (postCreationReplacements.ContainsKey("id"))
                {
                    current = new Uri(current, postCreationReplacements["id"] + "/");
                }

                current = new Uri(current, actionMethod.Name + "/");

                if (postCreationReplacements.ContainsKey("actionId"))
                {
                    current = new Uri(current, postCreationReplacements["actionId"] + "/");
                }
            }

            return current;
        }
    }
}