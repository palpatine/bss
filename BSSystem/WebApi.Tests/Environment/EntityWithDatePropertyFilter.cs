using System;
using System.Data;
using System.Linq;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

namespace SolvesIt.BSSystem.WebApi.Tests.Environment
{
    internal class EntityWithDatePropertyFilter : IEntityFilter
    {
        public bool CanUse(Type entity, Type relation)
        {
            return entity.GetProperties().Any(x => x.GetCustomAttribute<SqlTypeAttribute>()?.Type == SqlDbType.SmallDateTime);
        }
    }
}