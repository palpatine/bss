﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIT.GuiTool.Logic.Builders;
using SolvesIT.GuiTool.Logic.SolutionService;
using SolvesIT.GuiTool.Model.SettingsModel;
using SolvesIT.GuiTool.Model.TemplateModel;

namespace SolvesIt.BSSystem.WebApi.Tests
{
    public sealed class Database
    {
        private const string ConnectionStringName = "BSS_DB_Tests";
        private static string _buildStructureScript;
        private readonly ISqlBuilder _bssSqlBuilder;
        private readonly Settings _settings;
        private readonly ISlnService _slnService;

        public Database(
            ISlnService slnService,
            ISqlBuilder bssSqlBuilder,
            Settings settings)
        {
            _slnService = slnService;
            _bssSqlBuilder = bssSqlBuilder;
            _settings = settings;
        }

        internal static string GetConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings[ConnectionStringName] == null)
                {
                    throw new InvalidOperationException($"Connection string was not found.");
                }

                return ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
            }
        }

        public async Task InitAsync()
        {
            var validationMessages = new Collection<ValidationMessage>();
            await _bssSqlBuilder.InitializeDevDatabase(validationMessages);

            if (string.IsNullOrEmpty(_buildStructureScript))
            {
                var sqlProjects = await _slnService.GetSqlProjects(validationMessages);
                _buildStructureScript = await _bssSqlBuilder.CreateDatabaseScript(sqlProjects, validationMessages);

                if (string.Compare(_settings.Sql.DevDbName, _settings.Sql.DbName, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    await _bssSqlBuilder.InitializeDevDatabase(validationMessages);
                }
            }

            using (var sqlConnection = new SqlConnection(GetConnectionString))
            {
                await sqlConnection.OpenAsync();
                await sqlConnection.ExecuteSrciptAsync(_buildStructureScript);
            }
        }
    }
}