using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class TopicKindGenerator : EntityGenerator<TopicKind, ITopicKind>
    {
        private readonly ConcurrentBag<TopicGenerator> _topicGenerators = new ConcurrentBag<TopicGenerator>();

        public TopicKindGenerator(
            NumbersGenerator numbersGenerator)
        {
            var number = numbersGenerator.GenerateUnique().First();
            Entity = new TopicKind
            {
                DateStart = DateTime.Today,
                Name = "TopicKindName_" + number,
                DisplayName = "DN_" + number,
            };
        }

        public OrganizationGenerator OrganizationGenerator => (OrganizationGenerator)Parent;

        public IEnumerable<TopicGenerator> TopicGenerators => _topicGenerators;

        protected override IEnumerable<IGenerator> Generators => _topicGenerators;

        public TopicGenerator CreateTopic(Action<Topic> configure = null)
        {
            var generator = Create<TopicGenerator>();
            _topicGenerators.Add(generator);
            generator.Entity.Scope = Entity.Scope;
            generator.Entity.TopicKind = Entity;

            configure?.Invoke(generator.Entity);

            return generator;
        }
    }
}