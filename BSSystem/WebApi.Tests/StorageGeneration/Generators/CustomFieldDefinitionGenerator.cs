using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public class CustomFieldDefinitionGenerator : EntityGenerator<CustomFieldDefinition, ICustomFieldDefinition>
    {
        private readonly ConcurrentDictionary<Type, ConcurrentBag<VersionedEntity>> _customFieldValues = new ConcurrentDictionary<Type, ConcurrentBag<VersionedEntity>>();

        public CustomFieldDefinitionGenerator(
            NumbersGenerator numbersGenerator)
        {
            Entity = new CustomFieldDefinition
            {
                DateStart = DateTime.Today,
                DisplayName = numbersGenerator.GenerateUnique().First(),
                Schema = string.Empty
            };
        }

        public IEnumerable<KeyValuePair<Type, ConcurrentBag<VersionedEntity>>> CustomFieldValues => _customFieldValues.ToArray();

        protected override IEnumerable<IGenerator> Generators { get; } = Enumerable.Empty<IGenerator>();

        public TCustomFiled AddValue<TCustomFiled>(IStorable entity, string value)
            where TCustomFiled : VersionedEntity, IScopedEntity, ICustomFieldAssignmentDescriptor, new()
        {
            var tableKey = StorableHelper.GetTableKey(entity.GetType());
            if (tableKey != Entity.TableKey)
            {
                Assert.Inconclusive($"Custom filed definition value must be set to entity of appropriate type {tableKey}");
            }

            var valueEntity = new TCustomFiled
            {
                Value = value,
                CustomFieldTarget = entity,
                Scope = (IOrganization)Parent.Entity,
            };

            typeof(TCustomFiled).GetProperty("CustomFieldDefinition").SetValue(valueEntity, Entity);

            _customFieldValues.AddOrUpdate(
                typeof(TCustomFiled),
                key => new ConcurrentBag<VersionedEntity>(new[] { valueEntity }),
                (key, bag) =>
                {
                    bag.Add(valueEntity);
                    return bag;
                });

            return valueEntity;
        }
    }
}