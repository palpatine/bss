using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class TopicGenerator : EntityGenerator<Topic, ITopic>
    {
        private readonly ConcurrentBag<TopicUser> _userAssignments = new ConcurrentBag<TopicUser>();

        public TopicGenerator(
            NumbersGenerator numbersGenerator)
        {
            var number = numbersGenerator.GenerateUnique().First();

            Entity = new Topic
            {
                DateStart = DateTime.Today,
                Name = "Name",
                DisplayName = "Topic_" + number
            };
        }

        public OrganizationGenerator OrganizationGenerator => (OrganizationGenerator)((TopicKindGenerator)Parent).Parent;

        public IEnumerable<TopicUser> UserAssignments => _userAssignments;

        protected override IEnumerable<IGenerator> Generators => Enumerable.Empty<IGenerator>();

        public TopicGenerator AddActivity(
            IActivity activity,
            Action<ActivityTopic> configure)
        {
            if (OrganizationGenerator.Find<ActivityGenerator>(activity) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var generator = OrganizationGenerator.Find<ActivityGenerator>(activity);
            generator.AddTopic(Entity, configure);
            return this;
        }

        public ActivityTopic AddActivity(
            IActivity activity)
        {
            var generator = OrganizationGenerator.Find<ActivityGenerator>(activity);
            if (generator == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            return generator.AddTopic(Entity);
        }

        public TopicGenerator AddUser(
            IUser user,
            IRoleDefinition role,
            Action<TopicUser> configure)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            if (OrganizationGenerator.Find<RoleDefinitionGenerator>(role) == null
                || OrganizationGenerator.Find<UserGenerator>(user) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var topicUser = AddUser(user, role);
            configure?.Invoke(topicUser);
            return this;
        }

        public TopicUser AddUser(
            IUser user,
            IRoleDefinition role)
        {
            if (OrganizationGenerator.Find<RoleDefinitionGenerator>(role) == null
                || OrganizationGenerator.Find<UserGenerator>(user) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var topicUser = new TopicUser
            {
                DateStart = DateTime.Today,
                Topic = Entity,
                User = user,
                RoleDefinition = role,
                Scope = OrganizationGenerator.Entity
            };

            _userAssignments.Add(topicUser);
            return topicUser;
        }
    }
}