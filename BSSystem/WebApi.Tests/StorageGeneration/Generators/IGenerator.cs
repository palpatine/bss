using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public interface IGenerator
    {
        Entity Entity { get; }

        IMarker Marker { get; }

        IGenerator Parent { get; }

        void SetParent(IGenerator generator);

        TGenerator Find<TGenerator>(IMarker marker)
            where TGenerator : class, IGenerator;
    }
}