using System;
using System.Collections.Generic;
using System.Linq;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class SectionGenerator : EntityGenerator<Section, ISection>
    {
        private readonly List<SectionUser> _sectionUsers = new List<SectionUser>();

        public SectionGenerator(
            NumbersGenerator numbersGenerator)
        {
            Entity = new Section
            {
                DateStart = DateTime.Today,
                DisplayName = "DN" + numbersGenerator.GenerateUnique().First(),
                Name = "Section" + numbersGenerator.GenerateUnique().First(),
                Parent = null
            };
        }

        public OrganizationGenerator OrganizationGenerator => (OrganizationGenerator)Parent;

        public IEnumerable<SectionUser> SectionUsers => _sectionUsers;

        protected override IEnumerable<IGenerator> Generators => Enumerable.Empty<IGenerator>();

        public ActivitySection AddActivity(
            IActivity activity)
        {
            var generator = OrganizationGenerator.Find<ActivityGenerator>(activity);
            return generator.AddSection(Entity);
        }

        public SectionGenerator AddActivity(
            IActivity activity,
            Action<ActivitySection> configure)
        {
            if (OrganizationGenerator.Find<ActivityGenerator>(activity) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var generator = OrganizationGenerator.Find<ActivityGenerator>(activity);
            generator.AddSection(Entity, configure);
            return this;
        }

        public SectionGenerator AddUser(
            IUser user,
            IRoleDefinition roleDefinition,
            Action<SectionUser> configure)
        {
            if (OrganizationGenerator.Find<UserGenerator>(user) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var sectionUser = AddUser(user, roleDefinition);
            configure?.Invoke(sectionUser);
            return this;
        }

        public SectionUser AddUser(
            IUser user,
            IRoleDefinition roleDefinition)
        {
            var sectionUser = new SectionUser
            {
                User = user,
                DateStart = DateTime.Today,
                Section = Entity,
                RoleDefinition = roleDefinition,
                Scope = OrganizationGenerator.Entity
            };

            _sectionUsers.Add(sectionUser);

            return sectionUser;
        }
    }
}