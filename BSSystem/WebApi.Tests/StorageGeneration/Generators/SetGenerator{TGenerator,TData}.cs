﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    internal sealed class SetGenerator<TGenerator, TData> : ISetGenerator<TGenerator, TData>
    {
        private readonly IEnumerable<Tuple<TGenerator, TData>> _generatorsSet;

        public SetGenerator(IEnumerable<Tuple<TGenerator, TData>> generatorsSet)
        {
            _generatorsSet = generatorsSet;
        }

        public async Task<ISetGenerator<TGenerator, TData>> OnEachAsync(Func<TGenerator, TData, Task> configurator)
        {
            var tasks = _generatorsSet
                .Select(generator => Task.Factory.StartNew(
                    () => configurator(generator.Item1, generator.Item2),
                    CancellationToken.None,
                    TaskCreationOptions.LongRunning,
                    LimitedConcurrencyLevelTaskScheduler.Instance))
                .ToArray();

            await Task.WhenAll(await Task.WhenAll(tasks));
            return this;
        }

        public async Task<ISetGenerator<TGenerator, TData>> OnEachAsync(Action<TGenerator, TData> configurator)
        {
            var tasks = _generatorsSet
               .Select(generator => Task.Factory.StartNew(
                   () => configurator(generator.Item1, generator.Item2),
                   CancellationToken.None,
                   TaskCreationOptions.LongRunning,
                   LimitedConcurrencyLevelTaskScheduler.Instance))
               .ToArray();

            await Task.WhenAll(tasks);
            return this;
        }

        public ISetGenerator<TGenerator, TData> OnEach(Action<TGenerator, TData> configurator)
        {
            foreach (var generator in _generatorsSet)
            {
                configurator(generator.Item1, generator.Item2);
            }

            return this;
        }
    }
}