using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class RoleDefinitionGenerator : EntityGenerator<RoleDefinition, IRoleDefinition>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public RoleDefinitionGenerator(
            IDatabaseAccessor databaseAccessor,
            NumbersGenerator numbersGenerator)
        {
            _databaseAccessor = databaseAccessor;
            Entity = new RoleDefinition
            {
                DateStart = DateTime.Today,
                DisplayName = "RoleDefintionDN_" + numbersGenerator.GenerateUnique().First()
            };
        }

        public ConcurrentBag<PermissionRoleDefinition> Permissions { get; } = new ConcurrentBag<PermissionRoleDefinition>();

        protected override IEnumerable<IGenerator> Generators => Enumerable.Empty<IGenerator>();

        private OrganizationGenerator OrganizationGenerator => (OrganizationGenerator)Parent;

        public RoleDefinitionGenerator AddPermission(
            params string[] permissionsPath)
        {
            foreach (var path in permissionsPath.EmptyIfNull())
            {
                var systemPermission = _databaseAccessor.Query<SystemPermission>()
                    .Single(x => x.Path == path);
                var permission = ((OrganizationGenerator)Parent).Permissions.SingleOrDefault(x => x.SystemPermission.Id == systemPermission.Id);

                if (permission == null)
                {
                    throw new InvalidOperationException("Organization has to have permission in order to use it in role.");
                }

                var rolePermission = new PermissionRoleDefinition
                {
                    Permission = permission,
                    RoleDefinition = Entity,
                    Scope = OrganizationGenerator.Entity
                };

                Permissions.Add(rolePermission);
            }

            return this;
        }
    }
}