using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Logic.Security;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class UserGenerator : EntityGenerator<User, IUser>
    {
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ISecurityManager _securityManager;
        private readonly ConcurrentBag<PermissionUser> _userPermissions = new ConcurrentBag<PermissionUser>();

        public UserGenerator(
            NumbersGenerator numbersGenerator,
            IDatabaseAccessor databaseAccessor,
            ISecurityManager securityManager)
        {
            _databaseAccessor = databaseAccessor;
            _securityManager = securityManager;
            Number = numbersGenerator.GenerateUnique().First();

            Entity = new User
            {
                DateStart = DateTime.Today.ToUnspecified(),
                Surname = "Surname",
                FirstName = "FirstName",
                DisplayName = "User_" + Number
            };
        }

        public Login LogOn { get; private set; }

        public string Number { get; }

        public OrganizationGenerator OrganizationGenerator => (OrganizationGenerator)Parent;

        public string Password { get; set; }

        public IEnumerable<PermissionUser> UserPermissions => _userPermissions;

        protected override IEnumerable<IGenerator> Generators => Enumerable.Empty<IGenerator>();

        public UserGenerator AddEntry(
            ITopic topic,
            IActivity activity,
            Action<Entry> configure)
        {
            if (OrganizationGenerator.Find<TopicGenerator>(topic) == null
                || OrganizationGenerator.Find<ActivityGenerator>(activity) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var activityGenerator = OrganizationGenerator.Find<ActivityGenerator>(activity);
            activityGenerator.AddEntry(topic, Entity, configure);
            return this;
        }

        public Entry AddEntry(
            ITopic topic,
            IActivity activity)
        {
            var activityGenerator = OrganizationGenerator.Find<ActivityGenerator>(activity);
            return activityGenerator.AddEntry(topic, Entity);
        }

        public UserGenerator AddLogOn(
            string logOnName,
            string password)
        {
            Password = password;
            var data = _securityManager.EncryptPassword(password);
            LogOn = new Login
            {
                User = Entity,
                Password = data.Password,
                Salt = data.Salt,
                Scope = Entity.Scope,
                LoginName = logOnName
            };

            return this;
        }

        public UserGenerator AddPermission(
            string path,
            Action<PermissionUser> configure = null)
        {
            var systemPermission = _databaseAccessor.Query<SystemPermission>()
                .Where(p => p.Path == path)
                .Single();

            var permission = OrganizationGenerator.Permissions.SingleOrDefault(x => x.SystemPermission.Id == systemPermission.Id);

            if (permission == null)
            {
                throw new InvalidOperationException("Organization has to have permission in order to use it for user.");
            }

            var userPermisssion = new PermissionUser
            {
                Permission = permission,
                User = Entity,
                Scope = OrganizationGenerator.Entity
            };

            configure?.Invoke(userPermisssion);

            _userPermissions.Add(userPermisssion);
            return this;
        }

        public UserGenerator AddPermissions(params string[] permissions)
        {
            foreach (var permission in permissions)
            {
                AddPermission(permission);
            }

            return this;
        }

        public UserGenerator AddPosition(
            IPosition position,
            Action<PositionUser> configure)
        {
            if (OrganizationGenerator.Find<PositionGenerator>(position) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var positionGenerator = OrganizationGenerator.Find<PositionGenerator>(position);
            positionGenerator.AddUser(Entity, configure);
            return this;
        }

        public PositionUser AddPosition(
            IPosition position)
        {
            if (OrganizationGenerator.Find<PositionGenerator>(position) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var positionGenerator = OrganizationGenerator.Find<PositionGenerator>(position);
            return positionGenerator.AddUser(Entity);
        }

        public UserGenerator AddSection(
            ISection section,
            IRoleDefinition role,
            Action<SectionUser> configure)
        {
            if (OrganizationGenerator.Find<SectionGenerator>(section) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var sectionGenerator = OrganizationGenerator.Find<SectionGenerator>(section);
            sectionGenerator.AddUser(Entity, role, configure);
            return this;
        }

        public SectionUser AddSection(
            ISection section,
            IRoleDefinition role)
        {
            if (OrganizationGenerator.Find<SectionGenerator>(section) == null
                || OrganizationGenerator.Find<RoleDefinitionGenerator>(role) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var sectionGenerator = OrganizationGenerator.Find<SectionGenerator>(section);
            return sectionGenerator.AddUser(Entity, role);
        }

        public UserGenerator AddTopic(
            ITopic topic,
            IRoleDefinition role,
            Action<TopicUser> configure)
        {
            if (OrganizationGenerator.Find<TopicGenerator>(topic) == null
                || OrganizationGenerator.Find<RoleDefinitionGenerator>(role) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var topicGenerator = OrganizationGenerator.Find<TopicGenerator>(topic);
            topicGenerator.AddUser(Entity, role, configure);
            return this;
        }

        public TopicUser AddTopic(
            ITopic topic,
            IRoleDefinition role)
        {
            var topicGenerator = OrganizationGenerator.Find<TopicGenerator>(topic);
            return topicGenerator.AddUser(Entity, role);
        }
    }
}