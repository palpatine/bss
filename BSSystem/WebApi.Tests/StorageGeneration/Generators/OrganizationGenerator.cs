using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class OrganizationGenerator : EntityGenerator<Organization, IOrganization>
    {
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ConcurrentBag<ActivityKindGenerator> _activityKindGenerators = new ConcurrentBag<ActivityKindGenerator>();
        private readonly ConcurrentBag<CustomFieldDefinitionGenerator> _customFieldDefinitionGenerators = new ConcurrentBag<CustomFieldDefinitionGenerator>();
        private readonly ConcurrentBag<EntryLimitExclusion> _entryLimitExclusions = new ConcurrentBag<EntryLimitExclusion>();
        private readonly ConcurrentBag<ModuleOrganization> _modules = new ConcurrentBag<ModuleOrganization>();
        private readonly ConcurrentBag<OrganizationUser> _organizationUsers = new ConcurrentBag<OrganizationUser>();
        private readonly ConcurrentBag<Permission> _permissions = new ConcurrentBag<Permission>();
        private readonly ConcurrentBag<PositionGenerator> _positionGenerators = new ConcurrentBag<PositionGenerator>();
        private readonly ConcurrentBag<RoleDefinitionGenerator> _roleDefinitionGenerators = new ConcurrentBag<RoleDefinitionGenerator>();
        private readonly ConcurrentBag<SectionGenerator> _sectionGenerators = new ConcurrentBag<SectionGenerator>();
        private readonly ConcurrentBag<Setting> _settings = new ConcurrentBag<Setting>();
        private readonly ConcurrentBag<TopicKindGenerator> _topicKindGenerators = new ConcurrentBag<TopicKindGenerator>();
        private readonly ConcurrentBag<UserGenerator> _userGenerators = new ConcurrentBag<UserGenerator>();
        private IOrganization _marker;

        public OrganizationGenerator(
            NumbersGenerator numbersGenerator,
            IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
            _marker = Entity = new Organization
            {
                DateStart = DateTime.Today,
                DisplayName = "Org_" + numbersGenerator.GenerateUnique().First()
            };
            AdminAlias = new AdminAliasUser();
        }

        public IEnumerable<ActivityKindGenerator> ActivityKindGenerators => _activityKindGenerators;

        public AdminAliasUser AdminAlias { get; private set; }

        public IEnumerable<CustomFieldDefinitionGenerator> CustomFieldDefinitionGenerators => _customFieldDefinitionGenerators;

        public IEnumerable<EntryLimitExclusion> EntryLimitExclusions => _entryLimitExclusions;

        public override IOrganization Marker => _marker ?? Entity;

        public IEnumerable<ModuleOrganization> Modules => _modules;

        public IEnumerable<OrganizationUser> OrganizationUsers => _organizationUsers;

        public IEnumerable<Permission> Permissions => _permissions;

        public IEnumerable<PositionGenerator> PositionGenerators => _positionGenerators;

        public IEnumerable<RoleDefinitionGenerator> RoleDefinitionGenerators => _roleDefinitionGenerators;

        public IEnumerable<SectionGenerator> SectionGenerators => _sectionGenerators;

        public Action SessionSetter { get; set; }

        public IEnumerable<Setting> Settings => _settings;

        public IEnumerable<TopicKindGenerator> TopicKindGenerators => _topicKindGenerators;

        public IEnumerable<UserGenerator> UserGenerators => _userGenerators;

        protected override IEnumerable<IGenerator> Generators => _roleDefinitionGenerators
            .Concat<IGenerator>(_sectionGenerators)
            .Concat(_positionGenerators)
            .Concat(_topicKindGenerators)
            .Concat(_activityKindGenerators)
            .Concat(_userGenerators)
            .Concat(CustomFieldDefinitionGenerators);

        public OrganizationGenerator AddEntryLimitExclusion(
            IUser user,
            Action<EntryLimitExclusion> configure)
        {
            if (Find<UserGenerator>(user) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var entryLimitExclusion = AddEntryLimitExclusion(user);
            configure?.Invoke(entryLimitExclusion);
            return this;
        }

        public EntryLimitExclusion AddEntryLimitExclusion(
            IUser user)
        {
            if (user == null)
            {
                Assert.Inconclusive("User cannot be null");
            }

            var entryLimitExclusion = new EntryLimitExclusion
            {
                PeriodStart = DateTime.Today,
                PeriodEnd = DateTime.Today.AddDays(3),
                DateStart = DateTime.Today,
                DateEnd = DateTime.Today.AddDays(4),
                Scope = Entity,
                User = user
            };

            _entryLimitExclusions.Add(entryLimitExclusion);

            return entryLimitExclusion;
        }

        public ModuleOrganization AddModule(
            ModuleMarker module)
        {
            var relation = new ModuleOrganization()
            {
                Module = module,
                Organization = _marker,
                DateStart = Entity.DateStart
            };

            _modules.Add(relation);
            return relation;
        }

        public OrganizationGenerator AddModule(
            ModuleMarker module,
            Action<ModuleOrganization> configure)
        {
            var relation = AddModule(module);
            configure?.Invoke(relation);

            return this;
        }

        public OrganizationGenerator AddModules(IEnumerable<ModuleMarker> modules)
        {
            foreach (var moduleMarker in modules)
            {
                AddModule(moduleMarker);
            }

            return this;
        }

        public OrganizationGenerator AddPermissions(
            params string[] paths)
        {
            foreach (var path in paths.EmptyIfNull())
            {
                var systemPermission = _databaseAccessor.Query<SystemPermission>()
                    .Single(x => x.Path == path);
                var permission = new Permission
                {
                    SystemPermission = systemPermission,
                    Scope = _marker
                };

                _permissions.Add(permission);
            }

            return this;
        }

        public OrganizationUser AddUser(
            IUser user,
            IRoleDefinition roleDefinition)
        {
            var organizationUser = new OrganizationUser
            {
                User = user,
                DateStart = DateTime.Today,
                Organization = Entity,
                RoleDefinition = roleDefinition,
                Scope = Entity
            };

            _organizationUsers.Add(organizationUser);

            return organizationUser;
        }

        public ActivityKindGenerator CreateActivityKind(Action<ActivityKind> configure = null)
        {
            var generator = Create<ActivityKindGenerator>();
            _activityKindGenerators.Add(generator);
            generator.Entity.Scope = _marker;
            configure?.Invoke(generator.Entity);

            return generator;
        }

        public CustomFieldDefinitionGenerator CreateCustomFieldDefinition<TEntity>(Action<CustomFieldDefinition> configure = null)
            where TEntity : IVersionedEntity
        {
            var generator = Create<CustomFieldDefinitionGenerator>();
            generator.Entity.TableKey = StorableHelper.GetTableKey(typeof(TEntity));
            _customFieldDefinitionGenerators.Add(generator);
            generator.Entity.Scope = _marker;
            configure?.Invoke(generator.Entity);

            return generator;
        }

        public PositionGenerator CreatePosition(Action<Position> configure = null)
        {
            var generator = Create<PositionGenerator>();
            _positionGenerators.Add(generator);
            generator.Entity.Scope = _marker;
            configure?.Invoke(generator.Entity);

            return generator;
        }

        public RoleDefinitionGenerator CreateRoleDefinition<TTargetEntity>(
                            Action<RoleDefinition> configure = null)
            where TTargetEntity : IStorable
        {
            var generator = Create<RoleDefinitionGenerator>();
            _roleDefinitionGenerators.Add(generator);
            generator.Entity.Scope = _marker;
            generator.Entity.TableKey = StorableHelper.GetTableKey(typeof(TTargetEntity));
            configure?.Invoke(generator.Entity);

            return generator;
        }

        public SectionGenerator CreateSection(Action<Section> configure = null)
        {
            var generator = Create<SectionGenerator>();
            _sectionGenerators.Add(generator);
            generator.Entity.Scope = _marker;
            configure?.Invoke(generator.Entity);

            return generator;
        }

        public TopicKindGenerator CreateTopicKind(Action<TopicKind> configure = null)
        {
            var generator = Create<TopicKindGenerator>();
            _topicKindGenerators.Add(generator);
            generator.Entity.Scope = _marker;
            configure?.Invoke(generator.Entity);

            return generator;
        }

        public UserGenerator CreateUser(Action<User> configure = null)
        {
            var generator = Create<UserGenerator>();
            _userGenerators.Add(generator);
            generator.Entity.Scope = _marker;
            configure?.Invoke(generator.Entity);

            return generator;
        }

        public ISetGenerator<UserGenerator, T> CreateUsers<T>(
            IEnumerable<T> dataSource,
            Action<T, User> configure)
        {
            return
                new SetGenerator<UserGenerator, T>(
                    dataSource.Select(x => Tuple.Create(CreateUser(o => configure(x, o)), x)).ToArray());
        }

        public async Task UseAsync(OrganizationMarker organizationMarker)
        {
            _marker = organizationMarker;
            Entity = await _databaseAccessor.Query<Organization>().SingleAsync(x => x.Id == _marker.Id);
        }

        internal Setting AddSetting(SystemSettingMarker systemSetting, string value)
        {
            var setting = new Setting
            {
                Scope = this.Entity,
                SystemSetting = systemSetting,
                TimeStart = DateTime.Today,
                Value = value
            };

            _settings.Add(setting);

            return setting;
        }
    }
}