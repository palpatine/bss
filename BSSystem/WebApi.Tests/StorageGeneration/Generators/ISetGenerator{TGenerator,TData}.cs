﻿using System;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public interface ISetGenerator<TGenerator, TData>
    {
        Task<ISetGenerator<TGenerator, TData>> OnEachAsync(Func<TGenerator, TData, Task> configurator);

        Task<ISetGenerator<TGenerator, TData>> OnEachAsync(Action<TGenerator, TData> configurator);

        ISetGenerator<TGenerator, TData> OnEach(Action<TGenerator, TData> configurator);
    }
}