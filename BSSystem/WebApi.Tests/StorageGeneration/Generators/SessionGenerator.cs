using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class SessionGenerator : EntityGenerator<Session, ISession>
    {
        private readonly List<Func<JunctionLock>> _junctionLocks = new List<Func<JunctionLock>>();

        private readonly List<Func<Lock>> _locks = new List<Func<Lock>>();

        public SessionGenerator()
        {
            Entity = new Session
            {
                TimeStart = DateTime.Now,
                TimeEnd = DateTime.Now.AddHours(1),
                RemoteIP = new byte[4],
                Guid = Guid.NewGuid()
            };
        }

        public IEnumerable<Func<JunctionLock>> JunctionLocks => _junctionLocks;

        public IEnumerable<Func<Lock>> Locks => _locks;

        protected override IEnumerable<IGenerator> Generators => Enumerable.Empty<IGenerator>();

        public SessionGenerator AddJunctionLock<TEntity, TMarker>(
            Expression<Func<TEntity, TMarker>> relationProperty,
            TMarker scope,
            string token)
            where TEntity : Entity
            where TMarker : IMarker
        {
            return AddJunctionLock(
                typeof(TEntity),
                relationProperty.GetProperty(),
                scope,
                token);
        }

        public SessionGenerator AddJunctionLock(
            Type entityType,
            PropertyInfo relationProperty,
            IStorable scope,
            string token)
        {
            var tableName = StorableHelper.GetQualifiedTableName(entityType);
            var columnName = StorableHelper.GetQualifiedColumnName(relationProperty);
            var junctionLock = new Func<JunctionLock>(() => new JunctionLock
            {
                ColumnName = columnName,
                ColumnValue = scope.Id,
                User = Entity.User,
                OperationToken = token,
                Session = Entity,
                TableName = tableName,
            });

            _junctionLocks.Add(junctionLock);

            return this;
        }

        public SessionGenerator AddLock<TTarget>(
            TTarget marker,
            string token)
            where TTarget : IStorable
        {
            return AddLock(
                typeof(TTarget),
                marker,
                token);
        }

        public SessionGenerator AddLock(
            Type type,
            IStorable marker,
            string token)
        {
            var tableName = StorableHelper.GetQualifiedTableName(type);
            var entityLock = new Func<Lock>(() => new Lock
            {
                User = Entity.User,
                OperationToken = token,
                Session = Entity,
                TableName = tableName,
                RecordId = marker.Id
            });

            _locks.Add(entityLock);

            return this;
        }
    }
}