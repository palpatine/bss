﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public interface IGenerator<TEntity, TEntityIndicator> : IGenerator<TEntity>
        where TEntity : IEntity, TEntityIndicator
        where TEntityIndicator : IStorable
    {
        new TEntityIndicator Marker { get; }
    }
}