using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class ActivityKindGenerator : EntityGenerator<ActivityKind, IActivityKind>
    {
        private readonly ConcurrentBag<ActivityGenerator> _activityGenerators = new ConcurrentBag<ActivityGenerator>();

        public ActivityKindGenerator(
            NumbersGenerator numbersGenerator)
        {
            var number = numbersGenerator.GenerateUnique().First();
            Entity = new ActivityKind
            {
                DateStart = DateTime.Today,
                Name = "ActivityKindName_" + number,
                DisplayName = "DN_" + number
            };
        }

        public IEnumerable<ActivityGenerator> ActivityGenerators => _activityGenerators;

        protected override IEnumerable<IGenerator> Generators => _activityGenerators;

        public ActivityGenerator CreateActivity(Action<Activity> configure = null)
        {
            var generator = Create<ActivityGenerator>();
            _activityGenerators.Add(generator);
            generator.Entity.Scope = Entity.Scope;
            generator.Entity.ActivityKind = Entity;

            configure?.Invoke(generator.Entity);

            return generator;
        }
    }
}