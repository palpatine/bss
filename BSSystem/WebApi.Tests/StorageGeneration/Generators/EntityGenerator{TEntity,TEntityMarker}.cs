﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public abstract class EntityGenerator<TEntity, TEntityMarker> : IGenerator<TEntity, TEntityMarker>
        where TEntity : Entity, TEntityMarker
        where TEntityMarker : IMarker
    {
        public TEntity Entity { get; protected set; }

        Entity IGenerator.Entity => Entity;

        IMarker IGenerator.Marker => Marker;

        public virtual TEntityMarker Marker => Entity;

        public IGenerator Parent { get; private set; }

        protected abstract IEnumerable<IGenerator> Generators { get; }

        public TGenerator Find<TGenerator>(IMarker marker)
            where TGenerator : class, IGenerator
        {
            foreach (var generator in Generators)
            {
                if (ReferenceEquals(generator.Entity, marker))
                {
                    return (TGenerator)generator;
                }
            }

            return Generators.Select(x => x.Find<TGenerator>(marker)).FirstOrDefault(x => x != null);
        }

        public void SetParent(IGenerator generator)
        {
            Parent = generator;
            var property = typeof(IScopedEntity).IsAssignableFrom(typeof(TEntity)) ? typeof(TEntity).GetProperty(ExpressionExtensions.GetPropertyName((IScopedEntity e) => e.Scope)) : null;
            if (property == null)
            {
                return;
            }

            var currentParent = Parent;
            OrganizationGenerator organizationGenerator;
            while (true)
            {
                organizationGenerator = currentParent as OrganizationGenerator;
                if (organizationGenerator != null)
                {
                    break;
                }

                currentParent = currentParent.Parent;
                if (currentParent == null)
                {
                    throw new InvalidOperationException();
                }
            }

            property.SetValue(Entity, organizationGenerator.Entity);
        }

        protected TGenerator Create<TGenerator>()
            where TGenerator : IGenerator
        {
            var generator = ServiceLocator.Current.GetInstance<TGenerator>();
            generator.SetParent(this);
            return generator;
        }
    }
}