using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Repositories;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class Generator : IGenerator
    {
        private readonly IVersionedEntityRepository<Company, ICompany> _companyRepository;
        private readonly IDatabaseAccessor _databaseAccessor;

        private readonly ConcurrentBag<OrganizationGenerator> _organizationGenerators =
            new ConcurrentBag<OrganizationGenerator>();

        private readonly IOrganizationRepository _organizationRepository;
        private readonly OrganizationsCreator _organizationsCreator;
        private readonly SessionDataProvider _sessionDataProvider;
        private readonly IEntityRepository<Session, ISession> _sessionRepository;
        private readonly SessionsCreator _sessionsCreator;

        public Generator(
            IVersionedEntityRepository<Company, ICompany> companyRepository,
            IOrganizationRepository organizationRepository,
            IEntityRepository<Session, ISession> sessionRepository,
            OrganizationsCreator organizationsCreator,
            SessionsCreator sessionsCreator,
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider)
        {
            _companyRepository = companyRepository;
            _organizationRepository = organizationRepository;
            _sessionRepository = sessionRepository;
            _organizationsCreator = organizationsCreator;
            _organizationsCreator.Root = this;
            _sessionsCreator = sessionsCreator;
            _sessionsCreator.Root = this;
            _databaseAccessor = databaseAccessor;
            _sessionDataProvider = (SessionDataProvider)sessionDataProvider;
            CompanyGenerators = new ConcurrentBag<CompanyGenerator>();
            SessionGenerators = new List<SessionGenerator>();
        }

        public ConcurrentBag<CompanyGenerator> CompanyGenerators { get; }

        public Entity Entity => null;

        public IMarker Marker => null;

        public IGenerator Parent
        {
            get { throw new InvalidOperationException(); }
        }

        public ICollection<SessionGenerator> SessionGenerators { get; }

        public OrganizationGenerator CreateOrganization(Action<Organization, Company> configure = null)
        {
            var organizationGenerator = Create<OrganizationGenerator>();
            _organizationGenerators.Add(organizationGenerator);
            var companyGenerator = Create<CompanyGenerator>();
            CompanyGenerators.Add(companyGenerator);
            organizationGenerator.Entity.Company = companyGenerator.Entity;

            configure?.Invoke(organizationGenerator.Entity, companyGenerator.Entity);

            return organizationGenerator;
        }

        public ISetGenerator<OrganizationGenerator, T> CreateOrganizations<T>(
            IEnumerable<T> dataSource,
            Action<T, Organization, Company> configure)
        {
            return new SetGenerator<OrganizationGenerator, T>(
                dataSource.Select(x => Tuple.Create(CreateOrganization((o, c) => configure(x, o, c)), x)).ToArray());
        }

        public SessionGenerator CreateSession(
            IUser owner,
            Action<Session> configure = null)
        {
            var sessionGenerator = Create<SessionGenerator>();
            SessionGenerators.Add(sessionGenerator);
            sessionGenerator.Entity.User = owner;

            configure?.Invoke(sessionGenerator.Entity);

            return sessionGenerator;
        }

        public TGenerator Find<TGenerator>(IMarker marker)
            where TGenerator : class, IGenerator
        {
            foreach (IGenerator generator in _organizationGenerators)
            {
                if (ReferenceEquals(generator.Entity, marker))
                {
                    return (TGenerator)generator;
                }
            }

            return _organizationGenerators.Select(x => x.Find<TGenerator>(marker)).FirstOrDefault(x => x != null);
        }

        public async Task FlushAsync()
        {
            foreach (var companyGenerator in CompanyGenerators)
            {
                await _companyRepository.WriteAsync(companyGenerator.Entity);
            }

            foreach (var organizationGenerator in _organizationGenerators)
            {
                var entity = organizationGenerator.Entity;
                _sessionDataProvider.ResetForAdmin();

                if (entity.Id == 0)
                {
                    entity.NormalizeDates(organizationGenerator);
                    await _organizationRepository.WriteAsync(entity);
                }

                await _organizationRepository.CreateAdminAliasAsync(entity);
                var adminAlias = await _databaseAccessor.Query<User>()
                    .Where(x => x.Scope.IsEquivalent(entity) && x.Root.Id == 1)
                    .SingleAsync();
                organizationGenerator.AdminAlias.Id = adminAlias.Id;

                var adminAliasSession = new Session
                {
                    Guid = Guid.NewGuid(),
                    TimeStart = DateTime.UtcNow,
                    TimeEnd = DateTime.UtcNow.AddHours(1),
                    RemoteIP = new byte[4],
                    User = adminAlias
                };
                await _sessionRepository.WriteAsync(adminAliasSession);

                organizationGenerator.SessionSetter = () =>
                {
                    _sessionDataProvider.Scope = entity;
                    _sessionDataProvider.CurrentUser = adminAlias;
                    _sessionDataProvider.Session = adminAliasSession;
                };
            }

            foreach (var companyGenerator in CompanyGenerators)
            {
                await companyGenerator.FlushAsync();
            }

            await _organizationsCreator.FlushAsync(_organizationGenerators);

            _sessionDataProvider.ResetForAdmin();

            foreach (var sessionGenerator in SessionGenerators)
            {
                sessionGenerator.Entity.NormalizeDates();
                await _sessionRepository.WriteAsync(sessionGenerator.Entity);
            }

            await _sessionsCreator.FlushAsync(SessionGenerators);
        }

        public async Task<OrganizationGenerator> ForOrganizationAsync(OrganizationMarker organizationMarker)
        {
            var organizationGenerator = Create<OrganizationGenerator>();
            await organizationGenerator.UseAsync(organizationMarker);
            _organizationGenerators.Add(organizationGenerator);

            return organizationGenerator;
        }

        public void SetParent(IGenerator generator)
        {
            throw new NotImplementedException();
        }

        private TGenerator Create<TGenerator>()
            where TGenerator : IGenerator
        {
            var generator = ServiceLocator.Current.GetInstance<TGenerator>();
            generator.SetParent(this);
            return generator;
        }
    }
}