using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class ActivityGenerator : EntityGenerator<Activity, IActivity>
    {
        private readonly ConcurrentBag<ActivityPosition> _positionAssignments = new ConcurrentBag<ActivityPosition>();
        private readonly ConcurrentBag<ActivitySection> _sectionAssignments = new ConcurrentBag<ActivitySection>();
        private readonly ConcurrentBag<ActivityTopic> _topicAssignments = new ConcurrentBag<ActivityTopic>();

        public ActivityGenerator(
            NumbersGenerator numbersGenerator)
        {
            var number = numbersGenerator.GenerateUnique().First();

            Entity = new Activity
            {
                DateStart = DateTime.Today,
                Name = "Name",
                DisplayName = "Activity_" + number
            };
        }

        public ConcurrentBag<Entry> Entries { get; } = new ConcurrentBag<Entry>();

        public OrganizationGenerator OrganizationGenerator => (OrganizationGenerator)((ActivityKindGenerator)Parent).Parent;

        public IEnumerable<ActivityPosition> PositionAssignments => _positionAssignments;

        public IEnumerable<ActivitySection> SectionAssignments => _sectionAssignments;

        public IEnumerable<ActivityTopic> TopicAssignments => _topicAssignments;

        protected override IEnumerable<IGenerator> Generators => Enumerable.Empty<IGenerator>();

        public ActivityGenerator AddEntry(
            ITopic topic,
            IUser user,
            Action<Entry> configure)
        {
            if (OrganizationGenerator.Find<TopicGenerator>(topic) == null
                || OrganizationGenerator.Find<UserGenerator>(user) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var entry = AddEntry(topic, user);
            configure?.Invoke(entry);

            return this;
        }

        public Entry AddEntry(
            ITopic topic,
            IUser user)
        {
            if (OrganizationGenerator.Find<TopicGenerator>(topic) == null
                || OrganizationGenerator.Find<UserGenerator>(user) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var entry = new Entry
            {
                Topic = topic,
                Activity = Entity,
                User = user,
                Date = DateTime.Today,
                Minutes = 60,
                Scope = OrganizationGenerator.Entity
            };

            Entries.Add(entry);

            return entry;
        }

        public ActivityGenerator AddPosition(
                            IPosition position,
            Action<ActivityPosition> configure)
        {
            if (OrganizationGenerator.Find<PositionGenerator>(position) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var activityPosition = AddPosition(position);
            configure?.Invoke(activityPosition);
            return this;
        }

        public ActivityPosition AddPosition(
            IPosition position)
        {
            var activityPosition = new ActivityPosition
            {
                DateStart = DateTime.Today,
                Activity = Entity,
                Position = position,
                Scope = OrganizationGenerator.Entity
            };

            _positionAssignments.Add(activityPosition);
            return activityPosition;
        }

        public ActivitySection AddSection(
            ISection section)
        {
            var activitySection = new ActivitySection
            {
                DateStart = DateTime.Today,
                Activity = Entity,
                Section = section,
                Scope = OrganizationGenerator.Entity
            };

            _sectionAssignments.Add(activitySection);
            return activitySection;
        }

        public ActivityGenerator AddSection(
            ISection section,
            Action<ActivitySection> configure)
        {
            if (OrganizationGenerator.Find<SectionGenerator>(section) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var activitySection = AddSection(section);
            configure?.Invoke(activitySection);
            return this;
        }

        public ActivityGenerator AddTopic(
            ITopic topic,
            Action<ActivityTopic> configure)
        {
            if (OrganizationGenerator.Find<TopicGenerator>(topic) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var activityTopic = AddTopic(topic);
            configure?.Invoke(activityTopic);
            return this;
        }

        public ActivityTopic AddTopic(
            ITopic topic)
        {
            var activityTopic = new ActivityTopic
            {
                DateStart = DateTime.Today,
                Activity = Entity,
                Topic = topic,
                Scope = OrganizationGenerator.Entity
            };

            _topicAssignments.Add(activityTopic);
            return activityTopic;
        }
    }
}