using System;
using System.Collections.Generic;
using System.Threading;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class NumbersGenerator
    {
        [ThreadStatic]
        private static int _number;

        public IEnumerable<string> GenerateUnique()
        {
            yield return $"{Thread.CurrentThread.ManagedThreadId}_{++_number}";
        }
    }
}