using System;
using System.Collections.Generic;
using System.Linq;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class PositionGenerator : EntityGenerator<Position, IPosition>
    {
        private readonly List<ActivityPosition> _positionActivities = new List<ActivityPosition>();
        private readonly List<PositionUser> _positionUsers = new List<PositionUser>();

        public PositionGenerator(
            NumbersGenerator numbersGenerator)
        {
            Entity = new Position
            {
                DisplayName = "DN" + numbersGenerator.GenerateUnique().First(),
                Name = "Position" + numbersGenerator.GenerateUnique().First(),
                DateStart = DateTime.Today
            };
        }

        public OrganizationGenerator OrganizationGenerator => (OrganizationGenerator)Parent;

        public ICollection<ActivityPosition> PositionActivities => _positionActivities;

        public ICollection<PositionUser> PositionUsers => _positionUsers;

        protected override IEnumerable<IGenerator> Generators => Enumerable.Empty<IGenerator>();

        public ActivityPosition AddActivity(
            IActivity activity)
        {
            var generator = OrganizationGenerator.Find<ActivityGenerator>(activity);
            return generator.AddPosition(Entity);
        }

        public PositionGenerator AddActivity(
            IActivity activity,
            Action<ActivityPosition> configure)
        {
            if (OrganizationGenerator.Find<ActivityGenerator>(activity) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var generator = OrganizationGenerator.Find<ActivityGenerator>(activity);
            generator.AddPosition(Entity, configure);
            return this;
        }

        public PositionGenerator AddUser(
            IUser user,
            Action<PositionUser> configure)
        {
            if (OrganizationGenerator.Find<UserGenerator>(user) == null)
            {
                throw new InvalidOperationException("Cross organization junction is not allowed");
            }

            var positionUser = AddUser(user);
            configure?.Invoke(positionUser);
            return this;
        }

        public PositionUser AddUser(
                    IUser user)
        {
            var positionUser = new PositionUser
            {
                User = user,
                Position = Entity,
                DateStart = DateTime.Today,
                Scope = OrganizationGenerator.Entity
            };

            _positionUsers.Add(positionUser);
            return positionUser;
        }
    }
}