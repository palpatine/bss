using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public sealed class CompanyGenerator : EntityGenerator<Company, ICompany>
    {
        public CompanyGenerator(
            NumbersGenerator numbersGenerator)
        {
            Entity = new Company
            {
                Name = "CompanyName" + numbersGenerator.GenerateUnique().First(),
                FullName = "CompanyFullName" + numbersGenerator.GenerateUnique().First(),
            };
        }

        protected override IEnumerable<IGenerator> Generators => Enumerable.Empty<IGenerator>();

        public async Task FlushAsync()
        {
            await Task.Delay(0);
        }
    }
}