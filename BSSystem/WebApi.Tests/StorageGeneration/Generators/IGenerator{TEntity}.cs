﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators
{
    public interface IGenerator<TEntity> : IGenerator
        where TEntity : IEntity
    {
        new TEntity Entity { get; }
    }
}