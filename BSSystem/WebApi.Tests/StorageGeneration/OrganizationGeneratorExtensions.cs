﻿using System.Linq;
using System.Net;
using Microsoft.Practices.ServiceLocation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration
{
    internal static class OrganizationGeneratorExtensions
    {
        public static NetworkCredential CreateUserWithLogOnAndGlobalPermissions(
            this OrganizationGenerator generator,
            params string[] permissions)
        {
            UserGenerator userGenerator;
            return CreateUserWithLogOnAndGlobalPermissions(generator, out userGenerator, permissions);
        }

        public static NetworkCredential CreateUserWithLogOnAndGlobalPermissions(
            this OrganizationGenerator generator,
            out UserGenerator userGenerator,
            params string[] permissions)
        {
            var numbersGenerator = ServiceLocator.Current.GetInstance<NumbersGenerator>();
            userGenerator = generator.CreateUser();
            string logOn = $"user{numbersGenerator.GenerateUnique().First()}";
            string password = "password";
            userGenerator.AddLogOn(logOn, password);

            foreach (var permission in permissions)
            {
                userGenerator.AddPermission(permission);
            }

            return userGenerator.CreateCredentials();
        }
    }
}