using System;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration
{
    public static class DateExtensions
    {
        public static DateTime ToUnspecified(this DateTime date)
        {
            return new DateTime(date.Ticks, DateTimeKind.Unspecified);
        }
    }
}