﻿using System.Linq;
using System.Net;
using Microsoft.Practices.ServiceLocation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration
{
    internal static class UserGeneratorExtensions
    {
        public static NetworkCredential CreateLogOn(this UserGenerator userGenerator)
        {
            var numbersGenerator = ServiceLocator.Current.GetInstance<NumbersGenerator>();
            string logOn = $"user{numbersGenerator.GenerateUnique().First()}";
            string password = "password";
            userGenerator.AddLogOn(logOn, password);

            return userGenerator.CreateCredentials();
        }

        public static NetworkCredential CreateCredentials(this UserGenerator userGenerator)
        {
            return new NetworkCredential($"{userGenerator.OrganizationGenerator.Entity.DisplayName}\\{userGenerator.LogOn.LoginName}", userGenerator.Password);
        }
    }
}