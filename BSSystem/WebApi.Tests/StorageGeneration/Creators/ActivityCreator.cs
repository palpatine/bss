using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class ActivityCreator
    {
        private readonly IVersionedEntityRepository<ActivityPosition, IActivityPosition> _activityPositionRepository;
        private readonly IVersionedEntityRepository<ActivitySection, IActivitySection> _activitySectionRepository;
        private readonly IVersionedEntityRepository<ActivityTopic, IActivityTopic> _activityTopicRepository;
        private readonly IVersionedEntityRepository<Entry, IEntry> _entryRepository;
        private readonly ILockRepository _lockRepository;

        public ActivityCreator(
            ILockRepository lockRepository,
            IVersionedEntityRepository<ActivityPosition, IActivityPosition> activityPositionRepository,
            IVersionedEntityRepository<ActivitySection, IActivitySection> activitySectionRepository,
            IVersionedEntityRepository<ActivityTopic, IActivityTopic> activityTopicRepository,
            IVersionedEntityRepository<Entry, IEntry> entryRepository)
        {
            _lockRepository = lockRepository;
            _activityPositionRepository = activityPositionRepository;
            _activitySectionRepository = activitySectionRepository;
            _activityTopicRepository = activityTopicRepository;
            _entryRepository = entryRepository;
        }

        public Generator Root { get; set; }

        public async Task FlushAsync(IEnumerable<ActivityGenerator> activityGenerators)
        {
            await FlushActivityPositionsAsync(activityGenerators);
            await FlushActivitySectionsAsync(activityGenerators);
            await FlushActivityTopicsAsync(activityGenerators);
            await FlushEntriesAsync(activityGenerators);
        }

        private async Task FlushActivityPositionsAsync(IEnumerable<ActivityGenerator> activityGenerators)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var generator in activityGenerators)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(ActivityPosition),
                    ExpressionExtensions.GetProperty((ActivityPosition mo) => mo.Activity),
                    generator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var activityGenerator in activityGenerators)
            {
                foreach (var entity in activityGenerator.PositionAssignments)
                {
                    entity.NormalizeDates(activityGenerator.OrganizationGenerator);
                    entity.VerifyDatesConsistency(entity.Activity, entity.Position);
                    await _activityPositionRepository.WriteAsync(entity);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }

        private async Task FlushActivitySectionsAsync(IEnumerable<ActivityGenerator> activityGenerators)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var generator in activityGenerators)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(ActivitySection),
                    ExpressionExtensions.GetProperty((ActivitySection mo) => mo.Activity),
                    generator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var activityGenerator in activityGenerators)
            {
                foreach (var entity in activityGenerator.SectionAssignments)
                {
                    entity.NormalizeDates(activityGenerator.OrganizationGenerator);
                    entity.VerifyDatesConsistency(entity.Activity, entity.Section);
                    await _activitySectionRepository.WriteAsync(entity);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }

        private async Task FlushActivityTopicsAsync(IEnumerable<ActivityGenerator> activityGenerators)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var generator in activityGenerators)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(ActivityTopic),
                    ExpressionExtensions.GetProperty((ActivityTopic mo) => mo.Activity),
                    generator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var activityGenerator in activityGenerators)
            {
                foreach (var entity in activityGenerator.TopicAssignments)
                {
                    entity.NormalizeDates(activityGenerator.OrganizationGenerator);
                    entity.VerifyDatesConsistency(entity.Activity, entity.Topic);
                    await _activityTopicRepository.WriteAsync(entity);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }

        private async Task FlushEntriesAsync(IEnumerable<ActivityGenerator> activityGenerators)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var generator in activityGenerators)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(Entry),
                    ExpressionExtensions.GetProperty((Entry mo) => mo.Activity),
                    generator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var generator in activityGenerators)
            {
                foreach (var entity in generator.Entries)
                {
                    entity.NormalizeDates(generator.OrganizationGenerator);
                    DatedEntityExtensions.VerifyDateConsistency(entity.Date, entity.Activity, entity.User, entity.Topic);
                    var organizationGenerator = Root.Find<OrganizationGenerator>(entity.Scope);

                    var isUserAssignedToTopic = organizationGenerator.Find<TopicGenerator>(entity.Topic)
                        .UserAssignments
                        .Any(y => y.User == entity.User && y.DateStart <= entity.Date && (y.DateEnd ?? DateTime.MaxValue) >= entity.Date);
                    var sectionsAssignedToUser = organizationGenerator.SectionGenerators
                        .Where(q => q.SectionUsers.Any(x => x.User == entity.User && x.DateStart <= entity.Date && (x.DateEnd ?? DateTime.MaxValue) >= entity.Date))
                        .Select(x => x.Entity.Id);
                    var positionsAssignedToUser = organizationGenerator.PositionGenerators
                        .Where(q => q.PositionUsers.Any(x => x.User == entity.User && x.DateStart <= entity.Date && (x.DateEnd ?? DateTime.MaxValue) >= entity.Date))
                        .Select(x => x.Entity.Id);
                    var isActivityAssignedToTopic = generator.TopicAssignments
                        .Any(x => x.Topic.IsEquivalent(entity.Topic) && x.Activity.IsEquivalent(entity.Activity) && x.DateStart <= entity.Date && (x.DateEnd ?? DateTime.MaxValue) >= entity.Date);
                    var sectionsAssignedToActivity = generator.SectionAssignments
                        .Where(x => x.Activity.IsEquivalent(entity.Activity) && x.DateStart <= entity.Date && (x.DateEnd ?? DateTime.MaxValue) >= entity.Date).Select(x => x.Section.Id);
                    var positionsAssignedToActivity = generator.PositionAssignments
                        .Where(x => x.Activity.IsEquivalent(entity.Activity) && x.DateStart <= entity.Date && (x.DateEnd ?? DateTime.MaxValue) >= entity.Date).Select(x => x.Activity.Id);

                    if (!isUserAssignedToTopic
                        || (!isActivityAssignedToTopic && !sectionsAssignedToActivity.Intersect(sectionsAssignedToUser).Any() && !positionsAssignedToUser.Intersect(positionsAssignedToActivity).Any()))
                    {
                        Assert.Inconclusive("Entry cannot be added");
                    }

                    await _entryRepository.WriteAsync(entity);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }
    }
}