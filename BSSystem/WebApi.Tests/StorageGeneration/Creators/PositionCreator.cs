using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class PositionCreator
    {
        private readonly IEntityRepository<PositionUser, IPositionUser> _positionUserRepository;
        private readonly ILockRepository _lockRepository;

        public PositionCreator(
            IEntityRepository<PositionUser, IPositionUser> positionUserRepository,
            ILockRepository lockRepository)
        {
            _positionUserRepository = positionUserRepository;
            _lockRepository = lockRepository;
        }

        public Generator Root { get; set; }

        public async Task FlushAsync(IEnumerable<PositionGenerator> positions)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var generator in positions)
            {
               var lockResult = await _lockRepository.TryLockAsync(
                    typeof(PositionUser),
                    ExpressionExtensions.GetProperty((PositionUser mo) => mo.Position),
                    generator.Entity,
                    token);
               if (lockResult.Any())
               {
                   throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
               }
            }

            foreach (var sectionGenerator in positions)
            {
                foreach (var positionUser in sectionGenerator.PositionUsers)
                {
                    positionUser.NormalizeDates(sectionGenerator.OrganizationGenerator);
                    positionUser.VerifyDatesConsistency(positionUser.Position, positionUser.User);
                    await _positionUserRepository.WriteAsync(positionUser);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }
    }
}