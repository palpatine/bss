using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class UsersCreator
    {
        private readonly ILockRepository _lockRepository;
        private readonly IVersionedEntityRepository<Login, ILogin> _logOnRepository;
        private readonly IVersionedEntityRepository<PermissionUser, IPermissionUser> _permissionUserRepository;

        public UsersCreator(
            ILockRepository lockRepository,
            IVersionedEntityRepository<PermissionUser, IPermissionUser> permissionUserRepository,
            IVersionedEntityRepository<Login, ILogin> logOnRepository)
        {
            _lockRepository = lockRepository;
            _permissionUserRepository = permissionUserRepository;
            _logOnRepository = logOnRepository;
        }

        public Generator Root { get; set; }

        public async Task FlushAsync(IEnumerable<UserGenerator> userGenerators)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var userGenerator in userGenerators)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(PermissionUser),
                    ExpressionExtensions.GetProperty((PermissionUser pu) => pu.User),
                    userGenerator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var permission in userGenerators.SelectMany(x => x.UserPermissions))
            {
                await _permissionUserRepository.WriteAsync(permission);
            }

            await _lockRepository.RemoveLockAsync(token);

            foreach (var userGenerator in userGenerators)
            {
                if (userGenerator.LogOn != null)
                {
                    await _logOnRepository.WriteAsync(userGenerator.LogOn);
                }
            }
        }
    }
}