using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class RoleDefinitionCreator
    {
        private readonly IVersionedEntityRepository<PermissionRoleDefinition, IPermissionRoleDefinition>
            _permissionRoleDefinitionRepository;

        private readonly ILockRepository _lockRepository;

        public RoleDefinitionCreator(
            IVersionedEntityRepository<PermissionRoleDefinition, IPermissionRoleDefinition> permissionRoleDefinitionRepository,
            ILockRepository lockRepository)
        {
            _permissionRoleDefinitionRepository = permissionRoleDefinitionRepository;
            _lockRepository = lockRepository;
        }

        public Generator Root { get; set; }

        public async Task FlushAsync(
            IEnumerable<RoleDefinitionGenerator> allRoleDefinitions)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var generator in allRoleDefinitions)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(PermissionRoleDefinition),
                    ExpressionExtensions.GetProperty((PermissionRoleDefinition mo) => mo.RoleDefinition),
                    generator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            var permissions = allRoleDefinitions.SelectMany(x => x.Permissions).ToArray();
            foreach (var permissionRoleDefinition in permissions)
            {
                await _permissionRoleDefinitionRepository.WriteAsync(permissionRoleDefinition);
            }

            await _lockRepository.RemoveLockAsync(token);
        }
    }
}