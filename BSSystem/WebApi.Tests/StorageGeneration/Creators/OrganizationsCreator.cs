using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class OrganizationsCreator
    {
        private readonly ActivityKindCreator _activityKindCreator;
        private readonly IVersionedEntityRepository<ActivityKind, IActivityKind> _activityKindRepository;
        private readonly CustomFieldDefinitionCreator _customFieldDefinitionCreator;
        private readonly IVersionedEntityRepository<CustomFieldDefinition, ICustomFieldDefinition> _customFieldDefinitionRepository;
        private readonly IVersionedEntityRepository<EntryLimitExclusion, IEntryLimitExclusion> _entryLimitExclusionRepository;
        private readonly ILockRepository _lockRepository;

        private readonly IVersionedEntityRepository<ModuleOrganization, IModuleOrganization> _moduleOrganizationRepository;

        private readonly IVersionedEntityRepository<Permission, IPermission> _organizationPermissionsRepository;
        private readonly IVersionedEntityRepository<OrganizationUser, IOrganizationUser> _organizationUserRepository;
        private readonly PositionCreator _positionCreator;
        private readonly IVersionedEntityRepository<Position, IPosition> _positionRepository;
        private readonly RoleDefinitionCreator _roleDefinitionCreator;
        private readonly IVersionedEntityRepository<RoleDefinition, IRoleDefinition> _roleDefinitionRepository;
        private readonly SectionCreator _sectionCreator;
        private readonly IVersionedEntityRepository<Section, ISection> _sectionRepository;
        private readonly IVersionedEntityRepository<Setting, ISetting> _settingRepository;
        private readonly TopicKindCreator _topicKindCreator;
        private readonly IVersionedEntityRepository<TopicKind, ITopicKind> _topicKindRepository;
        private readonly IVersionedEntityRepository<User, IUser> _userRepository;
        private readonly UsersCreator _usersCreator;
        private Generator _root;

        public OrganizationsCreator(
            UsersCreator usersCreator,
            RoleDefinitionCreator roleDefinitionCreator,
            TopicKindCreator topicKindCreator,
            ActivityKindCreator activityKindCreator,
            SectionCreator sectionCreator,
            PositionCreator positionCreator,
            CustomFieldDefinitionCreator customFieldDefinitionCreator,
            ILockRepository lockRepository,
            IVersionedEntityRepository<User, IUser> userRepository,
            IVersionedEntityRepository<ModuleOrganization, IModuleOrganization> moduleOrganizationRepository,
            IVersionedEntityRepository<Permission, IPermission> organizationPermissionsRepository,
            IVersionedEntityRepository<TopicKind, ITopicKind> topicKindRepository,
            IVersionedEntityRepository<ActivityKind, IActivityKind> activityKindRepository,
            IVersionedEntityRepository<RoleDefinition, IRoleDefinition> roleDefinitionRepository,
            IVersionedEntityRepository<Section, ISection> sectionRepository,
            IVersionedEntityRepository<Position, IPosition> positionRepository,
            IVersionedEntityRepository<EntryLimitExclusion, IEntryLimitExclusion> entryLimitExclusionRepository,
            IVersionedEntityRepository<CustomFieldDefinition, ICustomFieldDefinition> customFieldDefinitionRepository,
            IVersionedEntityRepository<OrganizationUser, IOrganizationUser> organizationUserRepository,
            IVersionedEntityRepository<Setting, ISetting> settingRepository)
        {
            _usersCreator = usersCreator;
            _roleDefinitionCreator = roleDefinitionCreator;
            _topicKindCreator = topicKindCreator;
            _activityKindCreator = activityKindCreator;
            _sectionCreator = sectionCreator;
            _positionCreator = positionCreator;
            _userRepository = userRepository;
            _lockRepository = lockRepository;
            _moduleOrganizationRepository = moduleOrganizationRepository;
            _organizationPermissionsRepository = organizationPermissionsRepository;
            _topicKindRepository = topicKindRepository;
            _activityKindRepository = activityKindRepository;
            _roleDefinitionRepository = roleDefinitionRepository;
            _sectionRepository = sectionRepository;
            _positionRepository = positionRepository;
            _entryLimitExclusionRepository = entryLimitExclusionRepository;
            _customFieldDefinitionRepository = customFieldDefinitionRepository;
            _organizationUserRepository = organizationUserRepository;
            _settingRepository = settingRepository;
            _customFieldDefinitionCreator = customFieldDefinitionCreator;
        }

        public Generator Root
        {
            get
            {
                return _root;
            }
            set
            {
                _root = value;
                _sectionCreator.Root = Root;
                _usersCreator.Root = Root;
                _roleDefinitionCreator.Root = Root;
                _topicKindCreator.Root = Root;
                _activityKindCreator.Root = Root;
                _positionCreator.Root = Root;
            }
        }

        public async Task FlushAsync(
            IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            var generators = organizationGenerators as OrganizationGenerator[] ?? organizationGenerators.ToArray();
            await FlushSettingsAsync(generators);
            await FlushPermissionsAsync(generators);
            await FlushUsersAsync(generators);
            await FlushRoleDefinitionsAsync(generators);
            await FlushSectionsAsync(generators);
            await FlushPositionsAsync(generators);
            await FlushTopicKindsAsync(generators);
            await FlushActivityKindsAsync(generators);
            await FlushEntryLimitExclusionAsync(generators);
            await FlushCustomFieldDefinitionAsync(generators);
            await FlushOrganizationUserAsync(generators);
        }

        private async Task FlushActivityKindsAsync(IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            foreach (var organizationGenerator in organizationGenerators)
            {
                organizationGenerator.SessionSetter();
                foreach (var generator in organizationGenerator.ActivityKindGenerators)
                {
                    generator.Entity.NormalizeDates(organizationGenerator);
                    await _activityKindRepository.WriteAsync(generator.Entity);
                }

                await _activityKindCreator.FlushAsync(organizationGenerator.ActivityKindGenerators);
            }
        }

        private async Task FlushCustomFieldDefinitionAsync(OrganizationGenerator[] organizationGenerators)
        {
            foreach (var organization in organizationGenerators)
            {
                organization.SessionSetter();
                foreach (var entityGenerator in organization.CustomFieldDefinitionGenerators)
                {
                    entityGenerator.Entity.NormalizeDates(organization);
                    await _customFieldDefinitionRepository.WriteAsync(entityGenerator.Entity);
                }

                await _customFieldDefinitionCreator.FlushAsync(organization.CustomFieldDefinitionGenerators);
            }
        }

        private async Task FlushEntryLimitExclusionAsync(OrganizationGenerator[] organizationGenerators)
        {
            foreach (var organization in organizationGenerators)
            {
                organization.SessionSetter();
                foreach (var entity in organization.EntryLimitExclusions)
                {
                    entity.NormalizeDates(organization);
                    await _entryLimitExclusionRepository.WriteAsync(entity);
                }
            }
        }

        private async Task FlushOrganizationUserAsync(IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var organizationGenerator in organizationGenerators)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(OrganizationUser),
                    ExpressionExtensions.GetProperty((OrganizationUser ou) => ou.Organization),
                    organizationGenerator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var organization in organizationGenerators)
            {
                foreach (var entity in organization.OrganizationUsers)
                {
                    entity.NormalizeDates(organization);
                    await _organizationUserRepository.WriteAsync(entity);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }

        private async Task FlushPermissionsAsync(
            IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var organizationGenerator in organizationGenerators)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(ModuleOrganization),
                    ExpressionExtensions.GetProperty((ModuleOrganization mo) => mo.Organization),
                    organizationGenerator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var organization in organizationGenerators)
            {
                foreach (var entity in organization.Modules)
                {
                    entity.NormalizeDates(organization);
                    await _moduleOrganizationRepository.WriteAsync(entity);
                }
            }

            foreach (var organization in organizationGenerators)
            {
                organization.SessionSetter();
                foreach (var entity in organization.Permissions)
                {
                    await _organizationPermissionsRepository.WriteAsync(entity);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }

        private async Task FlushPositionsAsync(IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            foreach (var organizationGenerator in organizationGenerators)
            {
                organizationGenerator.SessionSetter();

                foreach (var generator in organizationGenerator.PositionGenerators)
                {
                    generator.Entity.NormalizeDates(organizationGenerator);
                    await _positionRepository.WriteAsync(generator.Entity);
                }

                await _positionCreator.FlushAsync(organizationGenerator.PositionGenerators);
            }
        }

        private async Task FlushRoleDefinitionsAsync(IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            foreach (var organizationGenerator in organizationGenerators)
            {
                organizationGenerator.SessionSetter();
                foreach (var generator in organizationGenerator.RoleDefinitionGenerators)
                {
                    generator.Entity.NormalizeDates(organizationGenerator);
                    await _roleDefinitionRepository.WriteAsync(generator.Entity);
                }

                await _roleDefinitionCreator.FlushAsync(organizationGenerator.RoleDefinitionGenerators);
            }
        }

        private async Task FlushSectionsAsync(IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            foreach (var organizationGenerator in organizationGenerators)
            {
                organizationGenerator.SessionSetter();
                var allSections = organizationGenerator.SectionGenerators;
                SectionGenerator[] sectionsToFlush;
                do
                {
                    sectionsToFlush = allSections
                        .Where(x => x.Entity.Id == 0 && (x.Entity.Parent == null || x.Entity.Parent.Id != 0))
                        .ToArray();

                    foreach (var generator in sectionsToFlush)
                    {
                        generator.Entity.NormalizeDates(organizationGenerator);
                        if (generator.Entity.Parent != null)
                        {
                            generator.Entity.VerifyDatesConsistency(generator.Entity.Parent);
                        }

                        await _sectionRepository.WriteAsync(generator.Entity);
                    }
                }
                while (sectionsToFlush.Any());

                await _sectionCreator.FlushAsync(allSections);
            }
        }

        private async Task FlushSettingsAsync(IEnumerable<OrganizationGenerator> generators)
        {
            foreach (var organizationGenerator in generators)
            {
                organizationGenerator.SessionSetter();
                foreach (var setting in organizationGenerator.Settings)
                {
                    setting.NormalizeDates(organizationGenerator);
                    await _settingRepository.WriteAsync(setting);
                }
            }
        }

        private async Task FlushTopicKindsAsync(IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            foreach (var organizationGenerator in organizationGenerators)
            {
                organizationGenerator.SessionSetter();
                foreach (var generator in organizationGenerator.TopicKindGenerators)
                {
                    generator.Entity.NormalizeDates(organizationGenerator);
                    await _topicKindRepository.WriteAsync(generator.Entity);
                }

                await _topicKindCreator.FlushAsync(organizationGenerator.TopicKindGenerators);
            }
        }

        private async Task FlushUsersAsync(IEnumerable<OrganizationGenerator> organizationGenerators)
        {
            var allUsers = organizationGenerators.SelectMany(x => x.UserGenerators).ToArray();
            UserGenerator[] usersToFlush;
            do
            {
                usersToFlush = allUsers
                    .Where(x => x.Entity.Id == 0 && (x.Entity.Parent == null || x.Entity.Parent.Id != 0))
                    .ToArray();

                foreach (var userGenerator in usersToFlush)
                {
                    userGenerator.Entity.NormalizeDates(userGenerator.OrganizationGenerator);
                    userGenerator.OrganizationGenerator.SessionSetter();
                    await _userRepository.WriteAsync(userGenerator.Entity);
                }
            }
            while (usersToFlush.Any());

            foreach (var organizationGenerator in organizationGenerators)
            {
                organizationGenerator.SessionSetter();
                await _usersCreator.FlushAsync(organizationGenerator.UserGenerators);
            }
        }
    }
}