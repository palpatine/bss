﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public class CustomFieldDefinitionCreator
    {
        private readonly IServiceLocator _serviceLocator;

        public CustomFieldDefinitionCreator(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
        }

        public async Task FlushAsync(IEnumerable<CustomFieldDefinitionGenerator> customFieldDefinitionGenerators)
        {
            foreach (var customFieldDefinitionGenerator in customFieldDefinitionGenerators)
            {
                foreach (var customFieldValue in customFieldDefinitionGenerator.CustomFieldValues)
                {
                    var repositoryType = typeof(IVersionedEntityRepository<,>).MakeGenericType(customFieldValue.Key, typeof(ICustomFieldAssignmentDescriptor));
                    var repository = _serviceLocator.GetInstance(repositoryType);
                    var writeMethodSample = ExpressionExtensions.GetMethod((IVersionedEntityRepository<User, IUser> x) => x.WriteAsync(null));
                    var writeMethod = repository.GetType().GetMethod(writeMethodSample.Name, BindingFlags.Instance | BindingFlags.Public);
                    foreach (var item in customFieldValue.Value)
                    {
                        await (Task)writeMethod.Invoke(repository, new object[] { item });
                    }
                }
            }
        }
    }
}
