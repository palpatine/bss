using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class SessionsCreator
    {
        private readonly IEntityRepository<JunctionLock, IJunctionLock> _junctionLockRepository;

        private readonly IEntityRepository<Lock, ILock> _lockRepository;

        public SessionsCreator(
            IEntityRepository<JunctionLock, IJunctionLock> junctionLockRepository,
            IEntityRepository<Lock, ILock> lockRepository)
        {
            _junctionLockRepository = junctionLockRepository;
            _lockRepository = lockRepository;
        }

        public Generator Root { get; set; }

        public async Task FlushAsync(ICollection<SessionGenerator> sessionGenerators)
        {
            foreach (var junctionLock in sessionGenerators.SelectMany(x => x.JunctionLocks))
            {
                await _junctionLockRepository.WriteAsync(junctionLock());
            }

            foreach (var entityLock in sessionGenerators.SelectMany(x => x.Locks))
            {
                await _lockRepository.WriteAsync(entityLock());
            }
        }
    }
}