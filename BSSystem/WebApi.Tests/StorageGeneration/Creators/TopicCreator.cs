using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class TopicCreator
    {
        private readonly IVersionedEntityRepository<TopicUser, ITopicUser> _topicUserRepository;
        private readonly ILockRepository _lockRepository;

        public TopicCreator(
            IVersionedEntityRepository<TopicUser, ITopicUser> topicUserRepository,
            ILockRepository lockRepository)
        {
            _topicUserRepository = topicUserRepository;
            _lockRepository = lockRepository;
        }

        public Generator Root { get; set; }

        public async Task FlushAsync(IEnumerable<TopicGenerator> topicGenerators)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var generator in topicGenerators)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(TopicUser),
                    ExpressionExtensions.GetProperty((TopicUser mo) => mo.Topic),
                    generator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var topicGenerator in topicGenerators)
            {
                foreach (var entity in topicGenerator.UserAssignments)
                {
                    entity.NormalizeDates(topicGenerator.OrganizationGenerator);
                    entity.VerifyDatesConsistency(entity.Topic, entity.User, entity.RoleDefinition);
                    await _topicUserRepository.WriteAsync(entity);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }
    }
}