using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class TopicKindCreator
    {
        private readonly TopicCreator _topicCreator;

        private readonly IVersionedEntityRepository<Topic, ITopic> _topicRepository;
        private Generator _root;

        public TopicKindCreator(
            IVersionedEntityRepository<Topic, ITopic> topicRepository,
            TopicCreator topicCreator)
        {
            _topicRepository = topicRepository;
            _topicCreator = topicCreator;
        }

        public Generator Root
        {
            get
            {
                return _root;
            }
            set
            {
                _root = value;
                _topicCreator.Root = Root;
            }
        }

        public async Task FlushAsync(IEnumerable<TopicKindGenerator> topicKinds)
        {
            foreach (var topicKindGenerator in topicKinds)
            {
                foreach (var generator in topicKindGenerator.TopicGenerators)
                {
                    generator.Entity.NormalizeDates(topicKindGenerator.OrganizationGenerator);
                    generator.Entity.VerifyDatesConsistency(generator.Entity.TopicKind);
                    await _topicRepository.WriteAsync(generator.Entity);
                }

                await _topicCreator.FlushAsync(topicKindGenerator.TopicGenerators);
            }
        }
    }
}