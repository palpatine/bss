using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class ActivityKindCreator
    {
        private readonly ActivityCreator _activityCreator;

        private readonly IVersionedEntityRepository<Activity, IActivity> _activityRepository;
        private Generator _root;

        public ActivityKindCreator(
            IVersionedEntityRepository<Activity, IActivity> activityRepository,
            ActivityCreator activityCreator)
        {
            _activityRepository = activityRepository;
            _activityCreator = activityCreator;
        }

        public Generator Root
        {
            get
            {
                return _root;
            }
            set
            {
                _root = value;
                _activityCreator.Root = Root;
            }
        }

        public async Task FlushAsync(IEnumerable<ActivityKindGenerator> activityKinds)
        {
            foreach (var activityKindGenerator in activityKinds)
            {
                foreach (var generator in activityKindGenerator.ActivityGenerators)
                {
                    var entity = generator.Entity;
                    generator.Entity.NormalizeDates(generator.OrganizationGenerator);
                    entity.VerifyDatesConsistency(entity.ActivityKind);
                    await _activityRepository.WriteAsync(generator.Entity);
                }

                await _activityCreator.FlushAsync(activityKindGenerator.ActivityGenerators);
            }
        }
    }
}