using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Creators
{
    public sealed class SectionCreator
    {
        private readonly ILockRepository _lockRepository;
        private readonly IEntityRepository<SectionUser, ISectionUser> _sectionUserRepository;

        public SectionCreator(
            IEntityRepository<SectionUser, ISectionUser> sectionUserRepository,
            ILockRepository lockRepository)
        {
            _sectionUserRepository = sectionUserRepository;
            _lockRepository = lockRepository;
        }

        public Generator Root { get; set; }

        public async Task FlushAsync(IEnumerable<SectionGenerator> sections)
        {
            var token = Guid.NewGuid().ToString("N");
            foreach (var generator in sections)
            {
                var lockResult = await _lockRepository.TryLockAsync(
                    typeof(SectionUser),
                    ExpressionExtensions.GetProperty((SectionUser mo) => mo.Section),
                    generator.Entity,
                    token);
                if (lockResult.Any())
                {
                    throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
                }
            }

            foreach (var sectionGenerator in sections)
            {
                foreach (var sectionUser in sectionGenerator.SectionUsers)
                {
                    sectionUser.NormalizeDates(sectionGenerator.OrganizationGenerator);
                    sectionUser.VerifyDatesConsistency(sectionUser.Section, sectionUser.User);
                    await _sectionUserRepository.WriteAsync(sectionUser);
                }
            }

            await _lockRepository.RemoveLockAsync(token);
        }
    }
}