using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration
{
    internal static class ModulesProvider
    {
        public static ModuleMarker Common => new ModuleMarker(1);

        public static ModuleMarker Wtt => new ModuleMarker(2);
    }
}