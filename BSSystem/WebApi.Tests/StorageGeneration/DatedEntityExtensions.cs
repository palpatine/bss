﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration
{
    public static class DatedEntityExtensions
    {
        public static void NormalizeDates<TStorable>(this TStorable entity, OrganizationGenerator generator = null)
            where TStorable : IStorable
        {
            var organizationTimeZoneId = generator?.Settings.SingleOrDefault(x => x.SystemSetting.IsEquivalent(SystemSettingsProvider.OrganizationTimeZone))?.Value ?? "UTC";
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(organizationTimeZoneId);

            var datedProperties = entity.GetType().GetProperties().Where(x => x.GetCustomAttribute<SqlTypeAttribute>()?.Type == SqlDbType.SmallDateTime);

            foreach (var datedProperty in datedProperties)
            {
                var value = datedProperty.GetValue(entity);
                if (value == null)
                {
                    continue;
                }

                var date = value as DateTime? ?? (DateTime)value;
                var newValue = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
                newValue = newValue.Add(-timeZone.BaseUtcOffset);
                datedProperty.SetValue(entity, default(DateTime));
                datedProperty.SetValue(entity, newValue);
            }

            var dateTimeProperties = entity.GetType().GetProperties().Where(x => x.GetCustomAttribute<SqlTypeAttribute>()?.Type == SqlDbType.DateTime);

            foreach (var datedProperty in dateTimeProperties)
            {
                var value = datedProperty.GetValue(entity);
                if (value == null)
                {
                    continue;
                }

                var date = value as DateTime? ?? (DateTime)value;
                var newValue = date.ToUniversalTime();
                datedProperty.SetValue(entity, default(DateTime));
                datedProperty.SetValue(entity, newValue);
            }
        }

        public static void VerifyDateConsistency(
            DateTime date,
            params IStorable[] masters)
        {
            foreach (var storable in masters)
            {
                var master = (IDatedEntity)storable;
                if (date < master.DateStart
                    || (master.DateEnd.HasValue && date > master.DateEnd.Value)
                    || date < master.DateStart
                    || (master.DateEnd.HasValue && date > master.DateEnd.Value))
                {
                    Assert.Inconclusive("Test data inconsistency detected");
                }
            }
        }

        public static void VerifyDatesConsistency(
            this IDatedEntity entity,
            params IStorable[] masters)
        {
            foreach (var storable in masters)
            {
                var master = (IDatedEntity)storable;
                if (entity.DateStart < master.DateStart
                    || (master.DateEnd.HasValue && entity.DateStart > master.DateEnd.Value)
                    || (entity.DateEnd.HasValue && entity.DateEnd < master.DateStart)
                    || (!entity.DateEnd.HasValue && master.DateEnd.HasValue)
                    || (entity.DateEnd.HasValue && master.DateEnd.HasValue && entity.DateEnd.Value > master.DateEnd.Value))
                {
                    Assert.Inconclusive("Test data inconsistency detected");
                }
            }
        }
    }
}
