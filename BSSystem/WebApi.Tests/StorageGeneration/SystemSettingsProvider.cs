using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;

namespace SolvesIt.BSSystem.WebApi.Tests.StorageGeneration
{
    internal static class SystemSettingsProvider
    {
        public static SystemSettingMarker OrganizationTimeZone => new SystemSettingMarker(1);
    }
}