﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.Utils;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;

namespace SolvesIt.BSSystem.WebApi.Tests.Core.LogOn
{
    public sealed class LogonTests : BaseTests
    {
        private static List<LogOnTestMetadata> _logOnTestMetadata;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            InitializeAsync().Wait();
        }

        [TestMethod]
        public async Task AuthenticateByOrganizationLogOnAndPasswordAsync()
        {
            var target = AddressProvider
                  .Get((LogOnController c) => c.Post(null));

            var client = ServiceLocator.Current.GetInstance<IServiceClient>();

            foreach (var testMetadata in _logOnTestMetadata)
            {
                var result = await client.CallServiceAsync<AuthenticationViewModel, ValidationResult>(
                    target,
                    new AuthenticationViewModel
                    {
                        Login = testMetadata.Organization + "/" + testMetadata.Username,
                        Password = testMetadata.Password,
                    });

                result.Code.ShouldBeEqual(HttpStatusCode.OK);
                result.Response.IsValid.ShouldBeEqual(testMetadata.ShouldSucceed);
            }
        }

        [TestMethod]
        public async Task AuthenticationWithExistingUserInvalidPasswordAsync()
        {
            var target = AddressProvider
                 .Get((LogOnController c) => c.Post(null));

            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var user = _logOnTestMetadata.First(x => x.ShouldSucceed);
            var result = await client.CallServiceAsync<AuthenticationViewModel, ValidationResult>(
                    target,
                    new AuthenticationViewModel
                    {
                        Login = user.Organization + "/" + user.Username,
                        Password = "InvalidPassword",
                    });

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
        }

        [TestMethod]
        public async Task AuthenticationWithNotExistingOrganizationAsync()
        {
            var target = AddressProvider
                 .Get((LogOnController c) => c.Post(null));

            var client = ServiceLocator.Current.GetInstance<IServiceClient>();

            var result = await client.CallServiceAsync<AuthenticationViewModel, ValidationResult>(
                    target,
                    new AuthenticationViewModel
                    {
                        Login = "NotExistingOrganization/UserDisplayName1",
                        Password = "Password",
                    });

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
        }

        [TestMethod]
        public async Task AuthenticationWithNotExistingUserAsync()
        {
            var target = AddressProvider
                 .Get((LogOnController c) => c.Post(null));

            var client = ServiceLocator.Current.GetInstance<IServiceClient>();

            var result = await client.CallServiceAsync<AuthenticationViewModel, ValidationResult>(
                    target,
                    new AuthenticationViewModel
                    {
                        Login = "OrganizationDisplayName1/NotExistingUser",
                        Password = "Password",
                    });

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
        }

        private static IEnumerable<DateRange> GetUserDateRanges(
            DateTime organizationDateStart,
            DateTime? organizationDateEnd)
        {
            var userDateRanges =
                CreateDateRange(x => DateTime.Today.AddMonths(x))
                    .Concat(CreateDateRange(organizationDateStart.AddMonths))
                    .ToArray();

            if (organizationDateEnd.HasValue)
            {
                userDateRanges =
                    userDateRanges.Concat(CreateDateRange(organizationDateEnd.Value.AddMonths))
                        .ToArray();
            }

            userDateRanges = userDateRanges.AsEnumerable().Distinct().ToArray();
            return userDateRanges;
        }

        private static async Task InitializeAsync()
        {
            await CreateAsync();

            var organizationDateRanges =
                CreateDateRange(x => DateTime.Today.AddYears(x));

            _logOnTestMetadata = new List<LogOnTestMetadata>();

            var generator = ServiceLocator.Current.GetInstance<Generator>();

            var organizations = generator
                   .CreateOrganizations(
                       organizationDateRanges.CrossJoin(new[] { true, false }, (d, p) => new { d.DateStart, d.DateEnd, HasLogOnPermission = p }),
                       (r, o, c) =>
                       {
                           o.DateStart = r.DateStart;
                           o.DateEnd = r.DateEnd;
                       });

            await organizations.OnEachAsync(async (organization, organizationData) =>
                       {
                           organization.AddModule(ModulesProvider.Common);

                           if (organizationData.HasLogOnPermission)
                           {
                               organization.AddPermissions(PermissionsCommon.User.CanLogin.Path);
                           }

                           var userDateRanges = GetUserDateRanges(organizationData.DateStart, organizationData.DateEnd)
                               .ToArray();

                           await organization.CreateUsers(
                                  userDateRanges
                                    .CrossJoin(new[] { true, false }, (d, p) => new { d.DateStart, d.DateEnd, HasLogOnPermission = p })
                                    .CrossJoin(new[] { true, false }, (d, p) => new { d.DateStart, d.DateEnd, d.HasLogOnPermission, HasLogOn = p }),
                                  (r, u) =>
                                  {
                                      u.DateStart = r.DateStart;
                                      u.DateEnd = r.DateEnd;
                                  })
                                  .OnEachAsync(async (user, userData) =>
                                  {
                                      if (userData.HasLogOnPermission)
                                      {
                                          user.AddPermission(PermissionsCommon.User.CanLogin.Path);
                                      }

                                      if (userData.HasLogOn)
                                      {
                                          user.AddLogOn("LogOn" + user.Number, "password");
                                      }

                                      var metadata = new LogOnTestMetadata
                                      {
                                          OrganizationMetadata = organizationData,
                                          UserMetadata = userData,
                                          Organization = organization.Entity.DisplayName,
                                          Username = "LogOn" + user.Number,
                                          Password = "password",
                                          ShouldSucceed = organizationData.DateStart <= DateTime.Today
                                                           && (organizationData.DateEnd == null || organizationData.DateEnd >= DateTime.Today)
                                                           && organizationData.HasLogOnPermission
                                                           && userData.DateStart <= DateTime.Today
                                                           && (userData.DateEnd == null || userData.DateEnd >= DateTime.Today)
                                                           && userData.HasLogOn
                                                           && userData.HasLogOnPermission
                                      };

                                      _logOnTestMetadata.Add(metadata);

                                      await organizations.OnEachAsync(
                                          async (innerOrganization, innerOrganizationData) =>
                                          {
                                              if (innerOrganization == organization)
                                              {
                                                  return;
                                              }

                                              innerOrganization.CreateUsers(
                                                  userDateRanges
                                                      .CrossJoin(
                                                          new[]
                                                          {
                                                              true,
                                                              false
                                                          },
                                                          (d, p) => new { d.DateStart, d.DateEnd, HasLogOnPermission = p }),
                                                  (r, a) =>
                                                  {
                                                      a.Parent = user.Entity;
                                                      a.DateStart = r.DateStart;
                                                      a.DateEnd = r.DateEnd;
                                                  })
                                                  .OnEach((alias, aliasData) =>
                                                  {
                                                      if (aliasData.HasLogOnPermission)
                                                      {
                                                          alias.AddPermission(PermissionsCommon.User.CanLogin.Path);
                                                      }

                                                      var aliasMetadata = new LogOnTestMetadata
                                                      {
                                                          SourceOrganizatoinMetadata =
                                                              new List<object> { organizationData },
                                                          AliassesMetadata = new List<object> { userData },
                                                          OrganizationMetadata = innerOrganization,
                                                          UserMetadata = aliasData,
                                                          Organization = innerOrganization.Entity.DisplayName,
                                                          Username = "LogOn" + user.Number,
                                                          Password = "password",
                                                          ShouldSucceed =
                                                              organizationData.DateStart <= DateTime.Today
                                                              &&
                                                              (organizationData.DateEnd == null ||
                                                               organizationData.DateEnd >= DateTime.Today)
                                                              && organizationData.HasLogOnPermission

                                                              &&
                                                              innerOrganizationData.DateStart <=
                                                              DateTime.Today
                                                              &&
                                                              (innerOrganizationData.DateEnd == null ||
                                                               innerOrganizationData.DateEnd >=
                                                               DateTime.Today)
                                                              && innerOrganizationData.HasLogOnPermission

                                                              && userData.DateStart <= DateTime.Today
                                                              &&
                                                              (userData.DateEnd == null ||
                                                               userData.DateEnd >= DateTime.Today)
                                                              && userData.HasLogOn
                                                              && userData.HasLogOnPermission
                                                      };

                                                      _logOnTestMetadata.Add(aliasMetadata);
                                                  });
                                              await Task.Delay(0);
                                          });
                                  });
                       });
            Debugger.Launch();
            await generator.FlushAsync();
        }
    }
}