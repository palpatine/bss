using System.Collections.Generic;

namespace SolvesIt.BSSystem.WebApi.Tests.Core.LogOn
{
    internal sealed class LogOnTestMetadata
    {
        public List<object> SourceOrganizatoinMetadata { get; set; }

        public bool ShouldSucceed { get; set; }

        public string Organization { get; set; }

        public string Password { get; set; }

        public string Username { get; set; }

        public object OrganizationMetadata { get; set; }

        public object UserMetadata { get; set; }

        public List<object> AliassesMetadata { get; set; }
    }
}