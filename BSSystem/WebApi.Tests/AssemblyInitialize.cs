﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SolvesIt.BSSystem.WebApi.Tests
{
    [TestClass]
    public static class AssemblyInitialize
    {
        public static Task Initialize { get; set; }

        /////   public static Semaphore TestToken { get; private set; }

        ////static AssemblyInitialize()
        ////{
        ////    TestToken = new Semaphore(0, 1);
        ////    TestToken.Release();
        ////}

        [AssemblyInitialize]
        public static void InitializeTests(TestContext context)
        {
            Initialize = CreateAsync();
        }

        private static async Task CreateAsync()
        {
            AppBootstrapper.Configure();
            await Task.Delay(0);
        }
    }
}
