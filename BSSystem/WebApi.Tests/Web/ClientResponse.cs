﻿using System.Diagnostics.CodeAnalysis;
using System.Net;

namespace SolvesIt.BSSystem.WebApi.Tests.Web
{
    public sealed class ClientResponse
    {
        public HttpStatusCode Code { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public CookieCollection Cookies { get; set; }

        public string FailureDescription { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public WebHeaderCollection Headers { get; set; }

        public string Response { get; set; }

        public WebExceptionStatus Status { get; set; }
    }
}