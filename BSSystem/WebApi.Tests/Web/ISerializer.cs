﻿namespace SolvesIt.BSSystem.WebApi.Tests.Web
{
    public interface ISerializer
    {
        string ContentType { get; }

        T Deserialize<T>(
            string value,
            string validationSchema);

        string Serialize<T>(
            T instance,
            string validationSchema);

        bool TryDeserialize<T>(
            string value,
            string validationSchema,
            out T result);
    }
}