using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.WebApi.Tests.Web
{
    public interface IServiceClient
    {
        Task<ClientResponse<TResponse>> CallServiceAsync<TRequest, TResponse>(
            Uri target,
            TRequest message,
            string method = "POST",
            string requestValidationSchema = null,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null);

        Task<ClientResponse<TResponse>> CallServiceAsync<TRequest, TResponse>(
            Uri target,
            TRequest message,
            HttpMethod method,
            string requestValidationSchema = null,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null);

        Task<ClientResponse> CallServiceAsync(
            Uri target,
            string method = "GET",
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null);

        Task<ClientResponse> CallServiceAsync(
            Uri target,
            string messageBody,
            string contentType,
            HttpMethod method,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null);

        Task<ClientResponse<TResponse>> CallServiceAsync<TResponse>(
            Uri target,
            string messageBody,
            string contentType,
            HttpMethod method,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null);

        Task<ClientResponse> CallServiceAsync<TRequest>(
           Uri target,
           TRequest message,
           string validationSchema = null,
           CookieCollection cookies = null,
           NetworkCredential credentials = null,
           Dictionary<string, string> customHeaders = null);

        Task<ClientResponse<TResponse>> CallServiceAsync<TResponse>(
            Uri target,
            string method = "GET",
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null);

        Task<ClientResponse<TResponse>> CallServiceAsync<TResponse>(
            Uri target,
            HttpMethod method,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null);

        Task<ClientResponse> CallServiceAsync(
            Uri target,
            HttpMethod method,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null);

        Task<ClientResponse> DownloadFileAsync(
            Uri target,
            Func<Stream, Task> handler,
            string method = "GET");

        Task<ClientResponse> DownloadFileAsync(
            Uri target,
            Func<Stream, Task> handler,
            CancellationToken token,
            string method = "GET");

        Task<ClientResponse> DownloadFileAsync(
            Uri target,
            Func<Stream, Task> handler,
            CancellationToken token,
            HttpMethod method);

        void SetDefaultCredential(NetworkCredential networkCredential);

        Task UploadFilesToServerAsync(
            Uri uri,
            Dictionary<string, string> data,
            string fileName,
            string fileContentType,
            byte[] fileData,
            CookieCollection cookies,
            NetworkCredential credentials,
            Dictionary<string, string> customHeaders);

        Task<ClientResponse> CallServiceAsync<TRequest>(
           Uri target,
           TRequest message,
           string method = "POST",
           string requestValidationSchema = null,
           CookieCollection cookies = null,
           NetworkCredential credentials = null,
           Dictionary<string, string> customHeaders = null);
    }
}