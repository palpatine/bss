﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.WebApi.Tests.Web
{
    internal sealed class ServiceClient : IServiceClient
    {
        private readonly ISerializer _serializer;
        private NetworkCredential _defaultNetworkCredential;

        public ServiceClient(ISerializer serializer)
        {
            _serializer = serializer;
        }

        public async Task<ClientResponse<TResponse>> CallServiceAsync<TRequest, TResponse>(
            Uri target,
            TRequest message,
            HttpMethod method,
            string requestValidationSchema = null,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            NormalizeDates(message);
            var data = _serializer.Serialize(message, requestValidationSchema);
            var rawResult = await CallServiceAsync(target, data, _serializer.ContentType, method, cookies, credentials, customHeaders);
            var result = DeserializeResponse<TResponse>(rawResult, validationSchema);
            return result;
        }

        public async Task<ClientResponse<TResponse>> CallServiceAsync<TRequest, TResponse>(
            Uri target,
            TRequest message,
            string method = "POST",
            string requestValidationSchema = null,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            return await CallServiceAsync<TRequest, TResponse>(target, message, new HttpMethod(method), requestValidationSchema, validationSchema, cookies, credentials, customHeaders);
        }

        public async Task<ClientResponse> CallServiceAsync<TRequest>(
            Uri target,
            TRequest message,
            string method = "POST",
            string requestValidationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            NormalizeDates(message);
            var data = _serializer.Serialize(message, requestValidationSchema);
            var result = await CallServiceAsync(target, data, _serializer.ContentType, method, cookies, credentials, customHeaders);
            return result;
        }

        public Task<ClientResponse> CallServiceAsync(
            Uri target,
            string method = "GET",
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            return CallServiceAsync(target, null, null, method, cookies, credentials, customHeaders);
        }

        public Task<ClientResponse> CallServiceAsync(
            Uri target,
            HttpMethod method,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            return CallServiceAsync(target, null, null, method, cookies, credentials, customHeaders);
        }

        public async Task<ClientResponse<TResponse>> CallServiceAsync<TResponse>(
            Uri target,
            string messageBody,
            string contentType,
            HttpMethod method,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            var rawResult = await CallServiceAsync(target, messageBody, contentType, method, cookies, credentials, customHeaders);
            var result = DeserializeResponse<TResponse>(rawResult, validationSchema);
            return result;
        }

        public async Task<ClientResponse> CallServiceAsync(
            Uri target,
            string messageBody,
            string contentType,
            HttpMethod method,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            if (messageBody == null ^ contentType == null)
            {
                throw new ArgumentNullException("messageBody", "Message body and content type must both be null or both set.");
            }

            try
            {
                var request = await CreateRequestAsync(target, messageBody, contentType, method, cookies, credentials, customHeaders);
                var result = await HandleResponseAsync(request);
                return result;
            }
            catch (WebException ex)
            {
                return HandleWebException(ex);
            }
        }

        public async Task<ClientResponse<TResponse>> CallServiceAsync<TResponse>(
            Uri target,
            string method = "GET",
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            return await CallServiceAsync<TResponse>(target, new HttpMethod(method), validationSchema, cookies, credentials, customHeaders);
        }

        public async Task<ClientResponse<TResponse>> CallServiceAsync<TResponse>(
            Uri target,
            HttpMethod method,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            var rawResult = await CallServiceAsync(target, null, null, method, cookies, credentials, customHeaders);
            var result = DeserializeResponse<TResponse>(rawResult, validationSchema);
            return result;
        }

        public async Task<ClientResponse> CallServiceAsync<TRequest>(
            Uri target,
            TRequest message,
            string validationSchema = null,
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            NormalizeDates(message);
            var data = _serializer.Serialize(message, validationSchema);
            return await CallServiceAsync(target, data, _serializer.ContentType, HttpMethod.Post, cookies, credentials, customHeaders);
        }

        public async Task<ClientResponse> DownloadFileAsync(
            Uri target,
            Func<Stream, Task> handler,
            string method = "GET")
        {
            return await DownloadFileAsync(target, handler, new HttpMethod(method));
        }

        public async Task<ClientResponse> DownloadFileAsync(
            Uri target,
            Func<Stream, Task> handler,
            CancellationToken token,
            string method = "GET")
        {
            return await DownloadFileAsync(target, handler, token, new HttpMethod(method));
        }

        public async Task<ClientResponse> DownloadFileAsync(
            Uri target,
            Func<Stream, Task> handler,
            CancellationToken token,
            HttpMethod method)
        {
            return await DownloadFileAsync(target, handler, token, method, null, null);
        }

        public void SetDefaultCredential(NetworkCredential networkCredential)
        {
            _defaultNetworkCredential = networkCredential;
        }

        public async Task UploadFilesToServerAsync(
            Uri uri,
            Dictionary<string, string> data,
            string fileName,
            string fileContentType,
            byte[] fileData,
            CookieCollection cookies,
            NetworkCredential credentials,
            Dictionary<string, string> customHeaders)
        {
            var boundary = "----------" + DateTime.Now.Ticks.ToString("x");
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            if (cookies != null)
            {
                foreach (Cookie cookie in cookies)
                {
                    httpWebRequest.CookieContainer.Add(cookie);
                }
            }

            if (credentials == null)
            {
                credentials = _defaultNetworkCredential;
            }

            if (credentials != null)
            {
                var authInfo = credentials.UserName + ":" + credentials.Password;
                authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                httpWebRequest.Headers["Authorization"] = "Basic " + authInfo;
            }

            if (customHeaders != null)
            {
                foreach (var customHeader in customHeaders)
                {
                    httpWebRequest.Headers[customHeader.Key] = customHeader.Value;
                }
            }

            httpWebRequest.ContentType = "multipart/form-data; boundary=" + boundary;
            httpWebRequest.Method = "POST";
            using (var requestStream = await httpWebRequest.GetRequestStreamAsync())
            {
                WriteMultipartForm(requestStream, boundary, data, fileName, fileContentType, fileData);
            }

            using (var response = await httpWebRequest.GetResponseAsync())
            {
                using (var responseStream = response.GetResponseStream())
                {
                }
            }
        }

        public async Task<ClientResponse> CallServiceAsync(
            Uri target,
            string messageBody,
            string contentType,
            string method = "POST",
            CookieCollection cookies = null,
            NetworkCredential credentials = null,
            Dictionary<string, string> customHeaders = null)
        {
            return await CallServiceAsync(target, messageBody, contentType, new HttpMethod(method), cookies, credentials);
        }

        public async Task<ClientResponse> DownloadFileAsync(
            Uri target,
            Func<Stream, Task> handler,
            HttpMethod method)
        {
            return await DownloadFileAsync(target, handler, CancellationToken.None, method);
        }

        public async Task<ClientResponse> DownloadFileAsync(
            Uri target,
            Func<Stream, Task> handler,
            CancellationToken token,
            HttpMethod method,
            NetworkCredential credentials,
            Dictionary<string, string> customHeaders)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            try
            {
                var client = new HttpClient
                {
                    Timeout = new TimeSpan(0, 0, 5, 0)
                };
                var message = new HttpRequestMessage(method, target);

                if (credentials == null)
                {
                    credentials = _defaultNetworkCredential;
                }

                if (credentials != null)
                {
                    var authInfo = credentials.UserName + ":" + credentials.Password;
                    authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                    message.Headers.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
                }

                if (customHeaders != null)
                {
                    foreach (var customHeader in customHeaders)
                    {
                        message.Headers.Add(customHeader.Key, customHeader.Value);
                    }
                }

                token.ThrowIfCancellationRequested();
                var result = new ClientResponse();

                using (var response = await client.SendAsync(message, token))
                {
                    token.ThrowIfCancellationRequested();

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result.Status = WebExceptionStatus.UnknownError;
                        result.Code = response.StatusCode;
                        using (var stream = await response.Content.ReadAsStreamAsync())
                        {
                            var reader = new StreamReader(stream);
                            var failureDescription = reader.ReadToEnd();
                            result.FailureDescription = failureDescription;
                        }
                    }
                    else
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync())
                        {
                            await handler(responseStream);
                        }

                        result.Code = response.StatusCode;
                    }
                }

                return result;
            }
            catch (WebException ex)
            {
                var exceptionResult = HandleWebException(ex);
                return exceptionResult;
            }
        }

        private static async Task<ClientResponse> HandleResponseAsync(HttpWebRequest request)
        {
            var result = new ClientResponse();
            using (var response = (HttpWebResponse)await request.GetResponseAsync())
            {
                using (var responseStream = response.GetResponseStream())
                {
                    var responseData = await new StreamReader(responseStream).ReadToEndAsync();
                    result.Response = responseData;
                }

                result.Code = response.StatusCode;
                result.Cookies = response.Cookies;
                result.Headers = response.Headers;
            }

            return result;
        }

        private static ClientResponse HandleWebException(WebException exception)
        {
            var result = new ClientResponse { Status = exception.Status };

            if (exception.Response == null)
            {
                return result;
            }

            var response = (HttpWebResponse)exception.Response;
            result.Code = response.StatusCode;
            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                var failureDescription = reader.ReadToEnd();
                result.FailureDescription = failureDescription;
            }

            return result;
        }

        private static void NormalizeDates(object model)
        {
            var datedProperties = model.GetType().GetProperties().Where(x => x.PropertyType == typeof(DateTimeOffset));

            foreach (var datedProperty in datedProperties)
            {
                var value = datedProperty.GetValue(model);
                if (value == null)
                {
                    continue;
                }

                var date = value as DateTimeOffset? ?? (DateTimeOffset)value;
                var newValue = new DateTimeOffset(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Millisecond, new TimeSpan(0));
                datedProperty.SetValue(model, default(DateTimeOffset));
                datedProperty.SetValue(model, newValue);
            }
        }

        private static void WriteMultipartForm(
            Stream s,
            string boundary,
            Dictionary<string, string> data,
            string fileName,
            string fileContentType,
            byte[] fileData)
        {
            var boundarybytes = Encoding.UTF8.GetBytes("--" + boundary + "\r\n");
            var trailer = Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            var formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            var fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";

            var isClrfNeeded = false;

            if (data != null)
            {
                foreach (var key in data.Keys)
                {
                    if (isClrfNeeded)
                    {
                        WriteToStream(s, "\r\n");
                    }

                    WriteToStream(s, boundarybytes);

                    WriteToStream(s, string.Format(formdataTemplate, key, data[key]));
                    isClrfNeeded = true;
                }
            }

            if (isClrfNeeded)
            {
                WriteToStream(s, "\r\n");
            }

            WriteToStream(s, boundarybytes);
            WriteToStream(s, string.Format(fileheaderTemplate, fileName, fileName, fileContentType));
            WriteToStream(s, fileData);
            WriteToStream(s, trailer);
        }

        private static void WriteToStream(
            Stream s,
            string txt)
        {
            var bytes = Encoding.UTF8.GetBytes(txt);
            s.Write(bytes, 0, bytes.Length);
        }

        private static void WriteToStream(
            Stream s,
            byte[] bytes)
        {
            s.Write(bytes, 0, bytes.Length);
        }

        private async Task<HttpWebRequest> CreateRequestAsync(
            Uri target,
            string messageBody,
            string contentType,
            HttpMethod method,
            CookieCollection cookies,
            NetworkCredential credentials,
            Dictionary<string, string> customHeaders)
        {
            var request = WebRequest.CreateHttp(target);
            request.Method = method.Method;
            request.ContentType = contentType;

            if (credentials == null)
            {
                credentials = _defaultNetworkCredential;
            }

            if (credentials != null)
            {
                var authInfo = credentials.UserName + ":" + credentials.Password;
                authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                request.Headers["Authorization"] = "Basic " + authInfo;
            }

            if (customHeaders != null)
            {
                foreach (var customHeader in customHeaders)
                {
                    request.Headers[customHeader.Key] = customHeader.Value;
                }
            }

            request.Accept = "application/json";

            if (cookies != null)
            {
                request.CookieContainer = new CookieContainer();
                request.CookieContainer.Add(target, cookies);
            }

            if (messageBody != null)
            {
                using (var stream = await request.GetRequestStreamAsync())
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        await writer.WriteAsync(messageBody);
                        writer.Flush();
                    }
                }
            }
            else if (method != HttpMethod.Get)
            {
                using (await request.GetRequestStreamAsync())
                {
                }
            }

            return request;
        }

        private ClientResponse<TResponse> DeserializeResponse<TResponse>(
            ClientResponse rawResult,
            string validationSchema)
        {
            var result = new ClientResponse<TResponse>
            {
                Code = rawResult.Code,
                Status = rawResult.Status,
                Cookes = rawResult.Cookies,
                Headers = rawResult.Headers
            };

            if (rawResult.Status == WebExceptionStatus.Success)
            {
                result.Response = _serializer.Deserialize<TResponse>(rawResult.Response, validationSchema);
            }
            else
            {
                TResponse response;
                if (rawResult.FailureDescription != null
                    && _serializer.TryDeserialize(rawResult.FailureDescription, validationSchema, out response))
                {
                    result.Response = response;
                }
                else
                {
                    result.FailureDescription = rawResult.FailureDescription;
                }
            }

            return result;
        }
    }
}