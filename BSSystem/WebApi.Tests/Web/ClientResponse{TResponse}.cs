﻿using System.Diagnostics.CodeAnalysis;
using System.Net;

namespace SolvesIt.BSSystem.WebApi.Tests.Web
{
    public sealed class ClientResponse<TResponse>
    {
        public HttpStatusCode Code { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Cookes")]
        public CookieCollection Cookes { get; set; }

        public string FailureDescription { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public WebHeaderCollection Headers { get; set; }

        public TResponse Response { get; set; }

        public WebExceptionStatus Status { get; set; }
    }
}