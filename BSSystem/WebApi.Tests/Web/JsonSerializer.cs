﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;

namespace SolvesIt.BSSystem.WebApi.Tests.Web
{
    internal sealed class JsonSerializer : ISerializer
    {
        public string ContentType => "application/json";

        public T Deserialize<T>(
            string value,
            string validationSchema)
        {
            if (validationSchema == null)
            {
                var instance = JsonConvert.DeserializeObject<T>(
                    value,
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver
                        {
                            IgnoreSerializableAttribute = false,
                            IgnoreSerializableInterface = false,
                        }
                    });

                return instance;
            }

            using (var stringReader = new StringReader(value))
            {
                var reader = new JsonTextReader(stringReader);
                using (var validatingReader = new JSchemaValidatingReader(reader))
                {
                    validatingReader.Schema = JSchema.Parse(validationSchema);
                    IList<string> messages = new List<string>();
                    validatingReader.ValidationEventHandler += (o, a) => messages.Add(a.Message);

                    var serializer = new Newtonsoft.Json.JsonSerializer();
                    var instance = serializer.Deserialize<T>(validatingReader);

                    if (!messages.Any())
                    {
                        return instance;
                    }

                    var message = new[]
                    {
                        "Errors while deserializing responce occured: "
                    }
                        .Concat(messages);
                    throw new JsonException(string.Join(System.Environment.NewLine, message));
                }
            }
        }

        public string Serialize<T>(
            T instance,
            string validationSchema)
        {
            var json = JsonConvert.SerializeObject(
                instance,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver
                    {
                        IgnoreSerializableAttribute = false,
                        IgnoreSerializableInterface = false,
                    },
                    NullValueHandling = NullValueHandling.Ignore
                });

            if (validationSchema == null)
            {
                return json;
            }

            var schema = JSchema.Parse(validationSchema);

            var parsedResult = JObject.Parse(json);

            IList<string> errors;
            if (!parsedResult.IsValid(schema, out errors))
            {
                throw new InvalidOperationException(string.Join(System.Environment.NewLine, new[] { "Errors while serializing request occured: " }.Concat(errors)));
            }

            return json;
        }

        public bool TryDeserialize<T>(
            string value,
            string validationSchema,
            out T result)
        {
            try
            {
                result = Deserialize<T>(value, validationSchema);
                return true;
            }
            catch (JsonException)
            {
                result = default(T);
                return false;
            }
        }
    }
}