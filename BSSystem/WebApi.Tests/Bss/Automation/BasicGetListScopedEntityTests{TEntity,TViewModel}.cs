﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "GetList{0}BasicTests")]
    public abstract class BasicGetListScopedEntityTests<TEntity, TViewModel> : BasicGetListTests<TEntity, TViewModel>
        where TEntity : Entity, IScopedEntity
        where TViewModel : IViewModel
    {
        protected virtual HttpStatusCode CrossDomainListStatusCode => HttpStatusCode.NoContent;

        [TestMethod]
        public async Task UserWithGlobalReadPermissionCannotGetEntitiesFromOtherOrganizationsAsync()
        {
            await TestInitialize;

            var secondaryOrganization = Generator.CreateOrganization();
            secondaryOrganization.AddModules(Modules)
                .AddPermissions(
                    PermissionsCommon.User.CanLogin.Path,
                    Permission);
            var userGenerator = secondaryOrganization.CreateUser();
            userGenerator.AddPermission(Permission);
            var credentials = userGenerator.CreateLogOn();
            userGenerator.AddPermission(PermissionsCommon.User.CanLogin.Path);
            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(
                Target,
                credentials: credentials);

            result.Code.ShouldBeEqual(CrossDomainListStatusCode);
            VerifyCrossOrganizationAccessTest(result.Response);
        }

        protected virtual void VerifyCrossOrganizationAccessTest(IEnumerable<TViewModel> result)
        {
            result.ShouldBeNull();
        }
    }
}