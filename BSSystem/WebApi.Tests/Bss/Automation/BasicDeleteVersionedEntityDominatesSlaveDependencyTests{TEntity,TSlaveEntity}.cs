﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.Web;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [BasicDeleteVersionedEntityDominatesDependency(
        GenericTestMode.OnePerControllerAndSlave,
        "Delete{0}Dominates{1}BasicTests")]
    public abstract class BasicDeleteVersionedEntityDominatesSlaveDependencyTests<TEntity, TSlaveEntity>
        : BaseDeleteVersionedEntityTests<TEntity>
        where TEntity : VersionedEntity, IDatedEntity, new()
        where TSlaveEntity : Entity, new()
    {
        [TestMethod]
        public async Task UserCanDeleteEntityWhenOnlyDeletedSlavesExistsAsync()
        {
            await TestInitialize;
            CurrentUserGenerator.AddPermission(Permission);
            Entity = CreateEntityToModify();
            var slave = CreateSlaveEntity();
            LockEntity();
            await Generator.FlushAsync();
            await DeleteSlaveEntity(slave);

            var result = await CallWebService();
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.ShouldNotBeNull();
            entity.IsDeleted.ShouldBeTrue();
        }

        [TestMethod]
        public async Task UserCannotDeleteEntityWhenSlavesExistsAsync()
        {
            await TestInitialize;
            CurrentUserGenerator.AddPermission(Permission);
            Entity = CreateEntityToModify();
            CreateSlaveEntity();
            LockEntity();
            await Generator.FlushAsync();

            var result = await CallWebService();
            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            result.Response.IsValid.ShouldBeFalse("Deletion should have been rejected");
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == "SlaveExists").ShouldNotBeNull();

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.ShouldNotBeNull();
            entity.IsDeleted.ShouldBeFalse();
        }

        protected virtual async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<ValidationResult>(
                Target,
                method: HttpMethod.Delete,
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected abstract TSlaveEntity CreateSlaveEntity();

        protected virtual async Task DeleteSlaveEntity(TSlaveEntity slave)
        {
            await DeleteEntityAsync(slave);
        }
    }
}
