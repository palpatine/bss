using System;
using System.Collections.Generic;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Common.CustomFieldDefinitions;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Common.RoleDefinitions;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public sealed class BasicDeleteVersionedEntityDominatesDependencyAttribute
    : GenericTestAttribute
    {
        public BasicDeleteVersionedEntityDominatesDependencyAttribute(GenericTestMode mode, string nameFormat)
            : base(mode, nameFormat)
        {
            RelationExceptions = new[]
            {
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(AddressUser),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserWithAddressAsync())),
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(Lock),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserWithLockAsync())),
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(JunctionLock),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserWithJunctionLockAsync())),
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(UserCustomField),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserWithCustomFieldAsync())),
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(UserSetting),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserWithSettingAsync())),
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(Session),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserWithSessionAsync())),
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(Login),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserWithLoginAsync())),
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(PermissionUser),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserAsync())),
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(User),
                    ExpressionExtensions.GetMethod((DeleteUserTests x) => x.UserCanDeleteUserWithWithAliasAndAliasBecomesIndependentUserAsync())),
                new RelationExceptionDescriptor(
                    typeof(RoleDefinition),
                    typeof(PermissionRoleDefinition),
                    ExpressionExtensions.GetMethod((DeleteRoleDefinitionTests x) => x.UserCanDeleteRoleDefinitionWithPermissionAsync())),
                new RelationExceptionDescriptor(
                    typeof(CustomFieldDefinition),
                    typeof(CustomFieldLastValue),
                    ExpressionExtensions.GetMethod((DeleteCustomFieldDefinitionTests x) => x.UserCanDeleteCustomFieldDefinitionWithCustomFieldLastValueAsync()))
            };
        }
    }
}
