﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "Post{0}BasicTests")]
    public abstract class BasicPostScopedVersionedEntityTests<TEntity, TViewModel>
        : BasicPostVersionedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, IScopedEntity, new()
        where TViewModel : IViewModel
    {
        protected abstract TEntity CreateEntityToModify(OrganizationGenerator organizationGenerator);

        protected override sealed TEntity CreateEntityToModify()
        {
            return CreateEntityToModify(CurrentOrganizationGenerator);
        }

        protected virtual void PrepareForCreate(OrganizationGenerator organizationGenerator)
        {
        }

        protected override sealed void PrepareForCreate()
        {
            PrepareForCreate(CurrentOrganizationGenerator);
        }

        protected sealed override void VerifyPersistence(TEntity actual)
        {
            VerifyPersistence(actual, CurrentOrganizationGenerator);
        }

        protected sealed override Task VerifyPersistenceAsync(TEntity entity)
        {
            return VerifyPersistenceAsync(entity, CurrentOrganizationGenerator);
        }

        protected virtual void VerifyPersistence(TEntity actual, OrganizationGenerator organizationGenerator)
        {
            Assert.Fail("One verification method must be overriden");
        }

        protected virtual async Task VerifyPersistenceAsync(TEntity entity, OrganizationGenerator organizationGenerator)
        {
            VerifyPersistence(entity, organizationGenerator);
            await Task.Delay(0);
        }
    }
}