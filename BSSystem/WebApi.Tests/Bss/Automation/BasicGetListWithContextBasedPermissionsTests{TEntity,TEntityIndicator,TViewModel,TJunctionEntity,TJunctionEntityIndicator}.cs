﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    /*

            1. rol with read
                ut   -> topic
                -------------
                arch -> acti, arch                    2x 403        ArchivalUserAssignmentDeniesAccessToActiveElementByBlockingApi                                v
                                                                    ArchivalUserAssignmentDeniesAccessToArchivalElementByBlockingApi                              v
                acti -> acti                          topic         ActiveUserAssignmentGrantsAccessToActiveElement                                               v
                dorm -> acti, dorm                    2x 403        DormantUserAssignmentDeniesAccessTo<Active,Dormant>ElementByBlockingApi                       v
                dele -> arch, acti, dorm, dele        4x 403        DeletedUserAssignmentDeniesAccessTo<Archival,Active,Dormant,Deleted>ElementByBlockingApi      v

            2. role without read
                acit -> acti                           403          ActiveUserAssignmentWithoutPermissionDeniesAccessToActiveElementByBlockingApi                               v

            3. rol with read
                acti -> acti:T1
                and
              rol with read
                arch -> acti, arch                    2x T1        WithOtherwiseAccessibleApiArchivalUserAssignmentDeniesAccessTo<Active,Archival>Element               v
                acti -> acti                          T1+topic     WithOtherwiseAccessibleApiActiveUserAssignmentGrantsAccessToActiveElement                            v
                acti -> acti:T1                      T1            MultipleActiveUserAssignmentsForTheSameElementDonotProduceDuplicatesInResults
                dorm -> acti, dorm                    2x T1        WithOtherwiseAccessibleApiDormantUserAssignmentDeniesAccessTo<Active,Dormant>Element          v
                dele -> arch, acti, dorm, dele        4x T1        WithOtherwiseAccessibleApiDeletedUserAssignmentDeniesAccessTo<Archival,Active,Dormant,Deleted>Element  v
              role without read
                acit -> acti                          T1           WithOtherwiseAccessibleApiActiveUserAssignmentWithoutPermissionDeniesAccessToActiveElement                   v

    */

    public abstract class BasicGetListWithContextBasedPermissionsTests<TEntity, TEntityIndicator, TViewModel, TJunctionEntity,
        TJunctionEntityIndicator>
        : BaseApiTests
        where TEntity : VersionedEntity, IDatedEntity, IStorable, TEntityIndicator, new()
        where TViewModel : IViewModel
        where TJunctionEntity : VersionedEntity, IRoleAssignmentJunctionEntity, TJunctionEntityIndicator, new()
        where TJunctionEntityIndicator : IMarker
        where TEntityIndicator : IMarker
    {
        public abstract string Permission { get; }

        protected abstract Uri Target { get; }

        [TestMethod]
        public async Task ActiveUserAssignmentWithoutPermissionDeniesAccessToActiveElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            var entityGenerator = CreateEntity();
            AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task ActiveUserAssignmentGrantsAccessToActiveElementAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var accessibleEntities = Enumerable.Range(0, 5).Select(x =>
            {
                var entityGenerator = CreateEntity();
                AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
                return entityGenerator;
            }).ToArray();

            for (var i = 0; i < 5; i++)
            {
                CreateEntity();
            }

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, accessibleEntities.Select(x => x.Entity).OrderBy(x => x.Id));
        }

        [TestMethod]
        public async Task ArchivalUserAssignmentDeniesAccessToActiveElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.Entity.DateStart = DateTime.Today.AddDays(-2);
            var role =
                CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>(
                    x => { x.DateStart = DateTime.Today.AddDays(-2); });
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity(x => x.DateStart = DateTime.Today.AddDays(-2));
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            junction.DateStart = DateTime.Today.AddDays(-2);
            junction.DateEnd = DateTime.Today.AddDays(-1);
            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task ArchivalUserAssignmentDeniesAccessToArchivalElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.Entity.DateStart = DateTime.Today.AddDays(-2);
            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role =
                CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>(
                    x => { x.DateStart = DateTime.Today.AddDays(-2); });
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity(x =>
            {
                x.DateStart = DateTime.Today.AddDays(-2);
                x.DateEnd = DateTime.Today.AddDays(-1);
            });
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            junction.DateStart = DateTime.Today.AddDays(-2);
            junction.DateEnd = DateTime.Today.AddDays(-1);
            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task DeletedUserAssignmentDeniesAccessToActiveElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity();
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            CreateEntity();

            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(junction);

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task DeletedUserAssignmentDeniesAccessToArchivalElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity();
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            CreateEntity();

            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(junction);

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task DeletedUserAssignmentDeniesAccessToDeletedElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity();
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            CreateEntity();

            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(junction);
            await DeleteVersionedEntityAsync(entityGenerator.Entity);

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task DeletedUserAssignmentDeniesAccessToDormantElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity(x => x.DateStart = DateTime.Today.AddDays(1));
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            junction.DateStart = DateTime.Today.AddDays(1);
            CreateEntity();

            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(junction);

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task DormantUserAssignmentDeniesAccessToActiveElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity(x => x.DateStart = DateTime.Today.AddDays(-1));
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            junction.DateStart = DateTime.Today.AddDays(1);
            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task DormantUserAssignmentDeniesAccessToDormantElementByBlockingApiAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity(x => x.DateStart = DateTime.Today.AddDays(1));
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            junction.DateStart = DateTime.Today.AddDays(1);
            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task
            MultipleActiveUserAssignmentsForTheSameElementDoNotProduceDuplicatesInResultsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var entityGenerator = CreateEntity();
            var firstRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            firstRole.AddPermission(Permission);
            var secondRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            secondRole.AddPermission(Permission);
            AssociateUser(CurrentUserGenerator, entityGenerator, firstRole.Entity);
            AssociateUser(CurrentUserGenerator, entityGenerator, secondRole.Entity);
            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, new[] { entityGenerator.Entity });
        }

        [TestMethod]
        public async Task
            WithOtherwiseAccessibleApiActiveUserAssignmentWithoutPermissionDeniesAccessToActiveElementAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var accessibleEntities = Enumerable.Range(0, 5).Select(x =>
            {
                var entityGenerator = CreateEntity();
                AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
                return entityGenerator;
            }).ToArray();

            var roleWithoutPermission = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            for (var i = 0; i < 5; i++)
            {
                var entityGenerator = CreateEntity();
                AssociateUser(CurrentUserGenerator, entityGenerator, roleWithoutPermission.Entity);
            }

            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, accessibleEntities.Select(x => x.Entity).OrderBy(x => x.Id));
        }

        [TestMethod]
        public async Task WithOtherwiseAccessibleApiActiveUserAssignmentGrantsAccessToActiveElementAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var firstEntityGenerator = CreateEntity();
            var secondEntityGenerator = CreateEntity();
            var firstRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            firstRole.AddPermission(Permission);
            var secondRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            secondRole.AddPermission(Permission);
            AssociateUser(CurrentUserGenerator, firstEntityGenerator, firstRole.Entity);
            AssociateUser(CurrentUserGenerator, secondEntityGenerator, secondRole.Entity);
            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(
                result.Response,
                new[] { firstEntityGenerator.Entity, secondEntityGenerator.Entity }.OrderBy(x => x.Id));
        }

        [TestMethod]
        public async Task WithOtherwiseAccessibleApiArchivalUserAssignmentDeniesAccessToActiveElementAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.Entity.DateStart = DateTime.Today.AddDays(-2);
            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var accessibleEntities = Enumerable.Range(0, 5).Select(x =>
            {
                var entityGenerator = CreateEntity();
                AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
                return entityGenerator;
            }).ToArray();

            var archivalRoleWitPermission = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>(
                x => { x.DateStart = DateTime.Today.AddDays(-2); });
            archivalRoleWitPermission.AddPermission(Permission);
            for (var i = 0; i < 5; i++)
            {
                var entityGenerator = CreateEntity(x => x.DateStart = DateTime.Today.AddDays(-2));
                var relation = AssociateUser(CurrentUserGenerator, entityGenerator, archivalRoleWitPermission.Entity);
                relation.DateStart = DateTime.Today.AddDays(-2);
                relation.DateEnd = DateTime.Today.AddDays(-1);
            }

            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, accessibleEntities.Select(x => x.Entity).OrderBy(x => x.Id));
        }

        [TestMethod]
        public async Task
            WithOtherwiseAccessibleApiArchivalUserAssignmentDeniesAccessToArchivalElementAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.Entity.DateStart = DateTime.Today.AddDays(-2);
            CurrentOrganizationGenerator.AddPermissions(Permission);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var accessibleEntities = Enumerable.Range(0, 5).Select(x =>
            {
                var entityGenerator = CreateEntity();
                AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
                return entityGenerator;
            }).ToArray();

            var activeRoleWitPermission = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>(
                x => { x.DateStart = DateTime.Today.AddDays(-2); });
            activeRoleWitPermission.AddPermission(Permission);
            for (var i = 0; i < 5; i++)
            {
                var entityGenerator = CreateEntity(
                    x =>
                    {
                        x.DateStart = DateTime.Today.AddDays(-2);
                        x.DateEnd = DateTime.Today.AddDays(-1);
                    });
                var relation = AssociateUser(CurrentUserGenerator, entityGenerator, activeRoleWitPermission.Entity);
                relation.DateStart = DateTime.Today.AddDays(-2);
                relation.DateEnd = DateTime.Today.AddDays(-1);
            }

            CreateEntity();

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, accessibleEntities.Select(x => x.Entity).OrderBy(x => x.Id));
        }

        [TestMethod]
        public async Task WithOtherwiseAccessibleApiDeletedUserAssignmentDeniesAccessToActiveElementAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var firstEntityGenerator = CreateEntity();
            var secondEntityGenerator = CreateEntity();
            var firstRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            firstRole.AddPermission(Permission);
            var secondRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            secondRole.AddPermission(Permission);
            AssociateUser(CurrentUserGenerator, firstEntityGenerator, firstRole.Entity);
            var junction = AssociateUser(CurrentUserGenerator, secondEntityGenerator, secondRole.Entity);
            CreateEntity();

            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(junction);
            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, new[] { firstEntityGenerator.Entity });
        }

        [TestMethod]
        public async Task
            WithOtherwiseAccessibleApiDeletedUserAssignmentDeniesAccessToArchivalElementAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.Entity.DateStart = DateTime.Today.AddDays(-2);
            CurrentOrganizationGenerator.AddPermissions(Permission);
            var firstEntityGenerator = CreateEntity();
            var secondEntityGenerator = CreateEntity(
                x =>
                {
                    x.DateStart = DateTime.Today.AddDays(-2);
                });
            var firstRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            firstRole.AddPermission(Permission);
            var secondRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>(x =>
            {
                x.DateStart = DateTime.Today.AddDays(-2);
            });
            secondRole.AddPermission(Permission);
            AssociateUser(CurrentUserGenerator, firstEntityGenerator, firstRole.Entity);
            var junction = AssociateUser(CurrentUserGenerator, secondEntityGenerator, secondRole.Entity);
            junction.DateStart = DateTime.Today.AddDays(-2);
            CreateEntity();

            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(junction);
            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            ValidateResponse(result.Response, new[] { firstEntityGenerator.Entity });
        }

        [TestMethod]
        public async Task WithOtherwiseAccessibleApiDeletedUserAssignmentDeniesAccessToDeletedElementAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var firstEntityGenerator = CreateEntity();
            var firstRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            firstRole.AddPermission(Permission);
            AssociateUser(CurrentUserGenerator, firstEntityGenerator, firstRole.Entity);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            role.AddPermission(Permission);
            var entityGenerator = CreateEntity();
            var junction = AssociateUser(CurrentUserGenerator, entityGenerator, role.Entity);
            CreateEntity();

            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(junction);
            await DeleteVersionedEntityAsync(entityGenerator.Entity);

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, new[] { firstEntityGenerator.Entity });
        }

        [TestMethod]
        public async Task WithOtherwiseAccessibleApiDeletedUserAssignmentDeniesAccessToDormantElementAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var firstEntityGenerator = CreateEntity();
            var secondEntityGenerator = CreateEntity(
                x =>
                {
                    x.DateStart = DateTime.Today.AddDays(1);
                    x.DateEnd = DateTime.Today.AddDays(2);
                });
            var firstRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            firstRole.AddPermission(Permission);
            var secondRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            secondRole.AddPermission(Permission);
            AssociateUser(CurrentUserGenerator, firstEntityGenerator, firstRole.Entity);
            var junction = AssociateUser(CurrentUserGenerator, secondEntityGenerator, secondRole.Entity);
            junction.DateStart = DateTime.Today.AddDays(1);
            junction.DateEnd = DateTime.Today.AddDays(2);

            CreateEntity();
            await FinalizeInitializationAsync();
            await DeleteVersionedEntityAsync(junction);
            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, new[] { firstEntityGenerator.Entity });
        }

        [TestMethod]
        public async Task WithOtherwiseAccessibleApiDormantUserAssignmentDeniesAccessToActiveElementAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var firstEntityGenerator = CreateEntity();
            var secondEntityGenerator = CreateEntity();
            var firstRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            firstRole.AddPermission(Permission);
            var secondRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            secondRole.AddPermission(Permission);
            AssociateUser(CurrentUserGenerator, firstEntityGenerator, firstRole.Entity);
            var junction = AssociateUser(CurrentUserGenerator, secondEntityGenerator, secondRole.Entity);
            junction.DateStart = DateTime.Today.AddDays(1);
            junction.DateEnd = DateTime.Today.AddDays(2);

            CreateEntity();
            await FinalizeInitializationAsync();
            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, new[] { firstEntityGenerator.Entity });
        }

        [TestMethod]
        public async Task WithOtherwiseAccessibleApiDormantUserAssignmentDeniesAccessToDormantElementAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            var firstEntityGenerator = CreateEntity();
            var secondEntityGenerator = CreateEntity(x =>
            {
                x.DateStart = DateTime.Today.AddDays(1);
                x.DateEnd = DateTime.Today.AddDays(2);
            });
            var firstRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            firstRole.AddPermission(Permission);
            var secondRole = CurrentOrganizationGenerator.CreateRoleDefinition<TEntity>();
            secondRole.AddPermission(Permission);
            AssociateUser(CurrentUserGenerator, firstEntityGenerator, firstRole.Entity);
            var junction = AssociateUser(CurrentUserGenerator, secondEntityGenerator, secondRole.Entity);
            junction.DateStart = DateTime.Today.AddDays(1);
            junction.DateEnd = DateTime.Today.AddDays(2);

            CreateEntity();
            await FinalizeInitializationAsync();
            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(Target, credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, new[] { firstEntityGenerator.Entity });
        }

        protected abstract TJunctionEntity AssociateUser(
            UserGenerator currentUserGenerator,
            IGenerator<TEntity> generator,
            IRoleDefinition roleDefinition);

        protected IGenerator<TEntity> CreateEntity()
        {
            return CreateEntity(x => { });
        }

        protected abstract IGenerator<TEntity> CreateEntity(Action<IDatedEntity> configure);

        protected abstract void ShouldMatch(
            TViewModel viewModel,
            TEntity current);

        protected virtual void ValidateResponse(
            IEnumerable<TViewModel> response,
            IEnumerable<TEntity> accessibleEntities)
        {
            response.Count().ShouldBeEqual(accessibleEntities.Count());
            using (var enumerator = accessibleEntities.GetEnumerator())
            {
                foreach (var viewModel in response)
                {
                    enumerator.MoveNext();

                    ShouldMatch(viewModel, enumerator.Current);
                }
            }
        }
    }
}