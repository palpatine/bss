using System;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public sealed class BasicPostVersionedEntityDependsOnSelfAttribute
    : GenericTestAttribute
    {
        public BasicPostVersionedEntityDependsOnSelfAttribute(GenericTestMode mode, string nameFormat)
            : base(mode, nameFormat)
        {
            RelationExceptions = new[]
            {
                new RelationExceptionDescriptor(
                    typeof(User),
                    typeof(User),
                    ExpressionExtensions.GetMethod((CreateAdminAliasTests x) => x.UserWithAddAliasPermissionCanCreateAliasIfHeDoesNotHaveOneInTargetedOrganizationAsync()))
            };
        }
    }
}