﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "Get{0}BasicTests")]
    public abstract class BasicGetScopedEntityTests<TEntity, TViewModel> : BasicGetEntityTests<TViewModel, TEntity>
        where TViewModel : IViewModel
        where TEntity : Entity, IScopedEntity, new()
    {
        [TestMethod]
        public async Task UserWithGlobalReadPermissionCannotGetEntityFromDifferentOrganizationByIdAsync()
        {
            await TestInitialize;

            var secondaryOrganization = Generator.CreateOrganization();
            secondaryOrganization.AddModules(Modules)
                .AddPermissions(
                    PermissionsCommon.User.CanLogin.Path,
                    Permission);
            var userGenerator = secondaryOrganization.CreateUser();
            userGenerator.AddPermission(Permission);
            userGenerator.AddPermission(PermissionsCommon.User.CanLogin.Path);
            var credentials = userGenerator.CreateLogOn();
            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<TViewModel>(
                Target,
                credentials: credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.NotFound);
        }
    }
}