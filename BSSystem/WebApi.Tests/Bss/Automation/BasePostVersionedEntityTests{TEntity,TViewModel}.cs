﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    public abstract class BasePostVersionedEntityTests<TEntity, TViewModel> : BaseApiTests
        where TEntity : VersionedEntity, new()
        where TViewModel : IViewModel
    {
        public string OperationToken => Guid.NewGuid().ToString("N");

        protected TEntity Entity { get; set; }

        protected TViewModel Model { get; set; }

        protected abstract string Permission { get; }

        protected abstract Uri Target { get; }

        protected async Task<ValidationResult> ApiCallTestAsync(
            Func<Task> prepare,
            Func<TViewModel> createModel,
            HttpStatusCode expectedResult)
        {
            await TestInitialize;
            await prepare();
            await FinalizeInitializationAsync();

            Model = createModel();
            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            result.Code.ShouldBeEqual(expectedResult);

            return result.Response;
        }

        protected async Task<ValidationResult> ApiCallTestAsync(
           Func<Task> prepare,
           Func<Task<TViewModel>> createModelAsync,
           HttpStatusCode expectedResult)
        {
            await TestInitialize;
            await prepare();
            await FinalizeInitializationAsync();

            Model = await createModelAsync();
            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            result.Code.ShouldBeEqual(expectedResult);

            return result.Response;
        }

        protected async Task CreateTestsAsync(
            Action prepare,
            Func<TViewModel> createModel,
            Func<TEntity, Task> verifyPersistence)
        {
            await UpdateTestsAsync(
                () =>
                {
                    prepare();
                    return null;
                },
                () => { },
                createModel,
                verifyPersistence);
        }

        protected async Task CreateTestsAsync(
            Action prepare,
            Func<TViewModel> createModel,
            Func<IDatabaseAccessor, ValidationResult, Task> verifyPersistence)
        {
            await UpdateTestsAsync(
                () =>
                {
                    prepare();
                    return null;
                },
                () => { },
                createModel,
                verifyPersistence);
        }

        protected async Task CreateValidationErrorTestAsync(
            Func<Task> prepare,
            Func<TViewModel> createModel,
            Action<ValidationResult> verifyPersistence)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                () => Task.FromResult(createModel()),
                verifyPersistence);
        }

        protected async Task CreateValidationErrorTestAsync(
            Func<Task> prepare,
            Func<Task<TViewModel>> createModelAsync,
            Action<ValidationResult> verifyPersistence)
        {
            var result = await ApiCallTestAsync(
               prepare,
               createModelAsync,
               HttpStatusCode.OK);

            result.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            verifyPersistence(result);
        }

        protected async Task CreateValidationErrorTestAsync(
            Action prepare,
            Func<TViewModel> createModel,
            Action<ValidationResult> verifyPersistence)
        {
            await CreateValidationErrorTestAsync(
                () =>
                {
                    prepare();
                    return Task.Delay(0);
                },
                () => Task.FromResult(createModel()),
                verifyPersistence);
        }

        protected override void InitializeState()
        {
            Entity = null;
            Model = default(TViewModel);

            CurrentOrganizationGenerator.AddPermissions(
                    Permission);
        }

        protected virtual void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddLock(Entity, OperationToken);
        }

        protected async Task UpdateTestsAsync(
                    Func<TEntity> createEntityToModify,
            Action lockEntity,
            Func<TViewModel> createModel,
            Func<TEntity, Task> verifyPersistence)
        {
            await UpdateTestsAsync(
                createEntityToModify,
                lockEntity,
                createModel,
                async (databaseAccessor, result) =>
                {
                    int entityId;
                    if (Entity != null)
                    {
                        entityId = Entity.Id;
                    }
                    else
                    {
                        entityId = result.Id.Value;
                        var intModelProperty = typeof(TViewModel).GetProperty("Id", typeof(int));
                        intModelProperty?.SetValue(Model, entityId);
                    }

                    var entity = await databaseAccessor.Query<TEntity>()
                        .SingleOrDefaultAsync(x => !x.IsDeleted && x.Id == entityId);
                    entity.ShouldNotBeNull();
                    await verifyPersistence(entity);
                });
        }

        protected async Task UpdateTestsAsync(
            Func<TEntity> createEntityToModify,
            Action lockEntity,
            Func<TViewModel> createModel,
            Func<IDatabaseAccessor, ValidationResult, Task> verifyPersistence)
        {
            Func<Task> prepare = async () =>
            {
                CurrentUserGenerator.AddPermission(Permission);
                Entity = createEntityToModify();
                lockEntity();
                await Task.Delay(0);
            };
            var result = await ApiCallTestAsync(
                prepare,
                createModel,
                HttpStatusCode.OK);

            result.IsValid.ShouldBeTrue(string.Join(", ", result.Errors.Select(x => x.Message)));

            await verifyPersistence(DatabaseAccessor, result);
        }

        protected async Task UpdateValidationErrorTestAsync(
            Func<TEntity> createEntityToModify,
            Action lockEntity,
            Func<TViewModel> createModel,
            Action<ValidationResult> verifyPersistence)
        {
            Func<Task> prepare = async () =>
            {
                CurrentUserGenerator.AddPermission(Permission);
                Entity = createEntityToModify();
                lockEntity();
                await Task.Delay(0);
            };
            var result = await ApiCallTestAsync(
                prepare,
                createModel,
                HttpStatusCode.OK);

            result.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            verifyPersistence(result);
        }
    }
}