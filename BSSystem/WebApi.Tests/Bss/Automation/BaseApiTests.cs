﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    public abstract class BaseApiTests
    {
        protected BaseApiTests()
        {
            Client = ServiceLocator.Current.GetInstance<IServiceClient>();
            Random = ServiceLocator.Current.GetInstance<NumbersGenerator>();
            DatabaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
        }

        protected static Task ClassInitialize { get; set; }

        protected IServiceClient Client { get; set; }

        protected NetworkCredential Credentials { get; set; }

        protected OrganizationGenerator CurrentOrganizationGenerator { get; private set; }

        protected UserGenerator CurrentUserGenerator { get; private set; }

        protected IDatabaseAccessor DatabaseAccessor { get; set; }

        protected Generator Generator { get; private set; }

        protected abstract IEnumerable<ModuleMarker> Modules { get; }

        protected NumbersGenerator Random { get; set; }

        protected Task TestInitialize { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            TestInitialize = TestInitializeAsync();
        }

        public string NextRandomValue()
        {
            return Random.GenerateUnique().First();
        }

        ////[TestCleanup]
        ////public void TestFinalize()
        ////{
        ////    AssemblyInitialize.TestToken.Release();
        ////}
        public async Task TestInitializeAsync()
        {
            await ClassInitialize;

            ////   AssemblyInitialize.TestToken.WaitOne();

            var database = ServiceLocator.Current.GetInstance<Database>();
            await database.InitAsync();

            var sessionRespository = ServiceLocator.Current.GetInstance<IEntityRepository<Session, ISession>>();
            var sessionDataProvider = (SessionDataProvider)ServiceLocator.Current.GetInstance<ISessionDataProvider>();

            var session = new Session()
            {
                Guid = Guid.NewGuid(),
                TimeStart = DateTime.UtcNow,
                TimeEnd = DateTime.UtcNow.AddHours(1),
                RemoteIP = new byte[4],
                User = new UserMarker(1)
            };
            sessionDataProvider.SetAdminSession(session);
            sessionRespository.Write(session);

            Generator = ServiceLocator.Current.GetInstance<Generator>();
            CurrentOrganizationGenerator = Generator.CreateOrganization();
            CurrentUserGenerator = CurrentOrganizationGenerator.CreateUser();
            CurrentOrganizationGenerator
                .AddModules(Modules)
                .AddPermissions(
                    PermissionsCommon.User.CanLogin.Path);
            Credentials = CurrentUserGenerator.CreateLogOn();
            CurrentUserGenerator.AddPermission(PermissionsCommon.User.CanLogin.Path);
            await InitializeStateAsync();
        }

        protected static async Task ClassInitializeAsync()
        {
            await AssemblyInitialize.Initialize;
        }

        protected async Task DeleteEntityAsync<TEntity>(
            TEntity entity)
            where TEntity : IEntity, new()
        {
            var sessionDataProvider = (SessionDataProvider)ServiceLocator.Current.GetInstance<ISessionDataProvider>();
            var lockRepository = ServiceLocator.Current.GetInstance<ILockRepository>();
            var entityIndicatortype = StorableHelper.GetEntityIndicator(typeof(TEntity));
            var entityRepositoryType = typeof(IEntityRepository<,>).MakeGenericType(typeof(TEntity), entityIndicatortype);
            var entityRepository = ServiceLocator.Current.GetInstance(entityRepositoryType);

            var scope = (entity as IScopedEntity)?.Scope;

            if (scope != null)
            {
                var organizationGenerator = Generator.Find<OrganizationGenerator>(scope);
                organizationGenerator.SessionSetter();
            }
            else
            {
                sessionDataProvider.ResetForAdmin();
            }

            var lockToken = Guid.NewGuid().ToString("N");
            var lockResult = await lockRepository.TryLockAsync(entity, lockToken);
            if (lockResult.Any())
            {
                throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
            }

            var deleteMethod = entityRepository.GetType().GetMethods()
                .Single(x => x.Name == "DeleteAsync" && x.GetParameters().First().ParameterType == entityIndicatortype);

            await (Task)deleteMethod.Invoke(entityRepository, new object[] { entity });

            await lockRepository.RemoveLockAsync(lockToken);
        }

        protected async Task DeleteVersionedEntityAsync<TEntity>(
            TEntity entity)
            where TEntity : IVersionedEntity, new()
        {
            var sessionDataProvider = (SessionDataProvider)ServiceLocator.Current.GetInstance<ISessionDataProvider>();
            var lockRepository = ServiceLocator.Current.GetInstance<ILockRepository>();
            var entityIndicatortype = StorableHelper.GetEntityIndicator(typeof(TEntity));
            var entityRepositoryType = typeof(IVersionedEntityRepository<,>).MakeGenericType(typeof(TEntity), entityIndicatortype);
            var entityRepository = ServiceLocator.Current.GetInstance(entityRepositoryType);

            var scope = (entity as IScopedEntity)?.Scope;

            if (scope != null)
            {
                var organizationGenerator = Generator.Find<OrganizationGenerator>(scope);
                organizationGenerator.SessionSetter();
            }
            else
            {
                sessionDataProvider.ResetForAdmin();
            }

            var lockToken = Guid.NewGuid().ToString("N");
            var lockResult = await lockRepository.TryLockAsync(entity, lockToken);
            if (lockResult.Any())
            {
                throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
            }

            var deleteMethod = entityRepository.GetType().GetMethods()
                .Single(x => x.Name == "DeleteAsync" && x.GetParameters().First().ParameterType == entityIndicatortype);

            await (Task)deleteMethod.Invoke(entityRepository, new object[] { entity });

            await lockRepository.RemoveLockAsync(lockToken);
        }

        protected async Task FinalizeInitializationAsync()
        {
            await Generator.FlushAsync();
        }

        protected virtual void InitializeState()
        {
        }

        protected virtual Task InitializeStateAsync()
        {
            InitializeState();
            return Task.Delay(0);
        }
    }
}