﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerControllerAndAssignmentJunctionRelatedEntity, "Get{0}{1}BasicTests")]
    public abstract class BasicAssignmentJunctionRelationTests<TMaster, TRelated, TRelation, TViewModel>
        : GetAssignmentJunctionRelationTests<TMaster, TRelated, TViewModel>
        where TMaster : VersionedEntity, new()
        where TRelated : VersionedEntity, new()
        where TRelation : VersionedEntity, IAssignmentJunctionEntity, new()
        where TViewModel : AssignmentViewModel, new()
    {
        private const int NonexistentEntityId = 13597;

        [TestMethod]
        public async Task UnauthorizedUserCannotGetRelationsAsync()
        {
            await TestInitialize;
            Entity = new TMaster { Id = NonexistentEntityId };
            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
                Target);
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithFakeCredentialsCannotGetRelationsAsync()
        {
            await TestInitialize;
            Entity = new TMaster { Id = NonexistentEntityId };
            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
                Target,
                credentials: new NetworkCredential("Organization\\NonExistLogin", "password"));
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithoutPermissionCannotGetRelationsAsync()
        {
            await ApiCallTestAsync(
                () =>
                {
                    var master = CreateMasterGenerator();
                    var related = CreateRelatedGenerator();
                    RelateEntities(master, related);
                    return Tuple.Create(master, new[] { related }.AsEnumerable());
                },
                HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task UserCanGetRelationsAsync()
        {
            await ResultTestsAsync(
                () =>
                {
                    var master = CreateMasterGenerator();
                    var related1 = CreateRelatedGenerator();
                    var related2 = CreateRelatedGenerator();

                    RelateEntities(master, related1);
                    RelateEntities(master, related2);
                    return Tuple.Create(master, new[] { related1, related2 }.OrderBy(z => z.Id).AsEnumerable());
                },
                async (databaseAccessor, data) =>
                {
                    var expected = await GetRelations(databaseAccessor);
                    Verify(data, expected);
                });
        }

        [TestMethod]
        public async Task UserCannotGetRelationsForNotExistingMasterAsync()
        {
            await TestInitialize;
            Entity = new TMaster { Id = NonexistentEntityId };
            CurrentUserGenerator.AddPermission(Permission);

            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
                Target,
                credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();
        }

        [TestMethod]
        public async Task UserCannotGetRelationsForDeletedMasterAsync()
        {
            await TestInitialize;
            Entity = CreateMasterGenerator();
            CurrentUserGenerator.AddPermission(Permission);

            await FinalizeInitializationAsync();

            await DeleteEntityAsync(Entity);
            var result = await Client.CallServiceAsync<ValidationResult>(
                Target,
                credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();
        }

        protected abstract TMaster CreateMasterGenerator();

        protected abstract TRelated CreateRelatedGenerator();

        protected async Task<IEnumerable<TRelation>> GetRelations(IDatabaseAccessor databaseAccessor)
        {
            IEnumerable<TRelation> expected;
            if (typeof(TRelation).Name.IndexOf(typeof(TMaster).Name, StringComparison.InvariantCulture) == 0)
            {
                expected = await databaseAccessor.Query<TRelation>()
                    .Where(x => !x.IsDeleted && x.First.IsEquivalent(Entity))
                    .ToListAsync();
            }
            else
            {
                expected = await databaseAccessor.Query<TRelation>()
                    .Where(x => !x.IsDeleted && x.Second.IsEquivalent(Entity))
                    .ToListAsync();
            }

            return expected;
        }

        protected abstract void RelateEntities(
            TMaster master,
            TRelated related);

        protected virtual void Verify(IEnumerable<TViewModel> actual, IEnumerable<TRelation> expected)
        {
            var property = typeof(TViewModel).GetProperty("Id");
            var actualSorded = actual.OrderBy(x => property.GetValue(x));
            var expectedSorted = expected.OrderBy(x => x.Id);

            using (var item = expectedSorted.GetEnumerator())
            {
                foreach (var viewModel in actualSorded)
                {
                    item.MoveNext();
                    Verify(viewModel, item.Current);
                }
            }
        }

        protected abstract void Verify(TViewModel actual, TRelation expected);
    }
}