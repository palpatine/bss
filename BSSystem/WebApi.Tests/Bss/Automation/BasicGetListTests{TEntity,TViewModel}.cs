﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "GetList{0}BasicTests")]
    public abstract class BasicGetListTests<TEntity, TViewModel>
        : BaseApiTests
        where TEntity : Entity
        where TViewModel : IViewModel
    {
        protected abstract string Permission { get; }

        protected abstract Uri Target { get; }

        [TestMethod]
        public async Task UserWithGlobalReadPermissionCanGetEntitiesListAsync()
        {
            await TestInitialize;
            CurrentUserGenerator.AddPermission(Permission);
            var entities = CreateEntities();
            await FinalizeInitializationAsync();
            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(
                Target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            ValidateResponse(result.Response, entities);
        }

        [TestMethod]
        public async Task UserWithoutGlobalReadPermissionCannotGetEntitiesListAsync()
        {
            await TestInitialize;
            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(
                Target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        protected abstract IEnumerable<TEntity> CreateEntities();

        protected override void InitializeState()
        {
            CurrentOrganizationGenerator.AddPermissions(Permission);
        }

        protected virtual IEnumerable<TViewModel> OrderActualById(IEnumerable<TViewModel> actual)
        {
            var idProperty = typeof(TViewModel).GetProperty("Id");
            if (idProperty == null)
            {
                Assert.Inconclusive("Cannot order view models by id");
            }

            return actual.OrderBy(x => idProperty.GetValue(x));
        }

        protected virtual IOrderedEnumerable<TEntity> OrderExpectedById(IEnumerable<TEntity> expected)
        {
            return expected.OrderBy(x => x.Id);
        }

        protected abstract void ShouldMatch(
                                           TViewModel actual,
           TEntity expected);

        protected virtual void ValidateResponse(
            IEnumerable<TViewModel> actual,
            IEnumerable<TEntity> expected)
        {
            actual.Count().ShouldBeEqual(expected.Count());
            using (var enumerator = OrderExpectedById(expected).GetEnumerator())
            {
                foreach (var viewModel in OrderActualById(actual))
                {
                    enumerator.MoveNext();

                    ShouldMatch(viewModel, enumerator.Current);
                }
            }
        }
    }
}