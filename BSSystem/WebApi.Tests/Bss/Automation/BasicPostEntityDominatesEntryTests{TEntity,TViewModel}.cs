﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerControllerAndSlave, "Post{0}DominatesEntryBasicTests")]
    public abstract class BasicPostEntityDominatesEntryTests<TEntity, TViewModel>
        : BasePostVersionedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, IScopedEntity, IDatedEntity, new()
        where TViewModel : IViewModel
    {
        protected ActivityGenerator ActivityGenerator { get; private set; }

        protected TopicGenerator TopicGenerator { get; private set; }

        protected UserGenerator UserGenerator { get; private set; }

        [TestMethod]
        public async Task UserCannotSetEntityDateEndThatIsLessThanMaximumDateOfEntriesAsync()
        {
            await TestInitialize;
            var targetDateEnd = DateTime.Today.AddDays(1);

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");

            var expectedErrorCode = $"{StorableHelper.GetEntityName(typeof(TEntity))}_DateEnd_BeAfter";
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == expectedErrorCode).ShouldNotBeNull("expected DateEndError");

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.ShouldNotBeNull();
            entity.DateEnd.ShouldBeEqual(Entity.DateEnd);
        }

        [TestMethod]
        public async Task UserCannotSetEntityDateStartThatIsGreaterThanMinimumDateOfEntriesAsync()
        {
            await TestInitialize;
            var targetDateStart = DateTime.Today.AddDays(-1);

            var result = await PrepareAndRun(
                targetDateStart,
                Entity.DateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            var expectedErrorCode = $"{StorableHelper.GetEntityName(typeof(TEntity))}_DateStart_BeBefore";
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == expectedErrorCode).ShouldNotBeNull("expected DateStartError");

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.ShouldNotBeNull();
            entity.DateStart.ShouldBeEqual(Entity.DateStart);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateEndThatIsEqualToMaximumDateOfEntriesAsync()
        {
            await TestInitialize;
            var targetDateEnd = DateTime.Today.AddDays(2);

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateEnd.ShouldBeEqual(targetDateEnd);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateEndThatIsGreaterThanMaximumDateOfEntriesAsync()
        {
            await TestInitialize;
            var targetDateEnd = DateTime.Today.AddDays(3);

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateEnd.ShouldBeEqual(targetDateEnd);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateEndThatIsLessThanMaximumDateOfDeletedEntriesAsync()
        {
            await TestInitialize;
            var targetDateEnd = DateTime.Today.AddDays(1);

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd,
                false);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateEnd.ShouldBeEqual(targetDateEnd);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateEndToNullWhenEntriesExistsAsync()
        {
            await TestInitialize;

            var result = await PrepareAndRun(
                Entity.DateStart,
                null);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateEnd.ShouldBeEqual(null);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateStartThatIsEqualToMinimumDateOfEntriesAsync()
        {
            await TestInitialize;
            var targetDateStart = DateTime.Today.AddDays(-2);

            var result = await PrepareAndRun(
                targetDateStart,
                Entity.DateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateStart.ShouldBeEqual(targetDateStart);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateStartThatIsGreaterThanMinimumDateOfDeletedEntriesAsync()
        {
            await TestInitialize;
            var targetDateStart = DateTime.Today.AddDays(-1);

            var result = await PrepareAndRun(
                targetDateStart,
                Entity.DateEnd,
                false);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateStart.ShouldBeEqual(targetDateStart);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateStartThatIsLessThanMinimumDateOfEntriesAsync()
        {
            await TestInitialize;
            var targetDateStart = DateTime.Today.AddDays(-3);

            var result = await PrepareAndRun(
                targetDateStart,
                Entity.DateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.ErrorCode + ":" + x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateStart.ShouldBeEqual(targetDateStart);
        }

        protected virtual async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
        }

        protected abstract TEntity CreateEntityToModifyWithRequiredJunctions(
            Action<IDatedEntity> masterDateRange);

        protected abstract TViewModel CreateModelToModify(
            DateTime dateStart,
            DateTime? dateEnd);

        private void CreateDependencies(Action<IDatedEntity> dateRange)
        {
            ActivityGenerator = CurrentOrganizationGenerator.CreateActivityKind(dateRange).CreateActivity(dateRange);
            TopicGenerator = CurrentOrganizationGenerator.CreateTopicKind(dateRange).CreateTopic(dateRange);
            UserGenerator = CurrentOrganizationGenerator.CreateUser(dateRange);
        }

        private IEnumerable<Entry> CreateEntries(
            DateTime periodStart,
            DateTime periodEnd)
        {
            var result = new List<Entry>();
            var date = periodStart;
            while (date > periodEnd)
            {
                var date1 = date;
                UserGenerator.AddEntry(TopicGenerator.Entity, ActivityGenerator.Entity, x =>
                {
                    x.Date = date1;
                    result.Add(x);
                });
                date = date.AddDays(1);
            }

            return result;
        }

        private async Task<ClientResponse<ValidationResult>> PrepareAndRun(
            DateTime targetDateStart,
            DateTime? targetDateEnd,
            bool createEntries = true)
        {
            Action<IDatedEntity> masterDateRange = x =>
            {
                x.DateStart = DateTime.Today.AddDays(-4);
                x.DateEnd = DateTime.Today.AddDays(4);
            };
            var entriesPeriodDateStart = DateTime.Today.AddDays(-2);
            var entriesPeriodDateEnd = DateTime.Today.AddDays(2);

            CurrentUserGenerator.AddPermission(Permission);
            CreateDependencies(masterDateRange);
            Entity = CreateEntityToModifyWithRequiredJunctions(masterDateRange);
            if (createEntries)
            {
                CreateEntries(entriesPeriodDateStart, entriesPeriodDateEnd);
            }

            var deletedEntries = CreateEntries(entriesPeriodDateStart, entriesPeriodDateEnd);
            LockEntity();
            await Generator.FlushAsync();
            foreach (var deletedEntry in deletedEntries)
            {
                await DeleteVersionedEntityAsync(deletedEntry);
            }

            Model = CreateModelToModify(targetDateStart, targetDateEnd);
            var result = await CallWebService();
            return result;
        }
    }
}