﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "Post{0}BasicTests")]
    public abstract class BasicPostVersionedEntityTests<TEntity, TViewModel>
        : BasePostVersionedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, new()
        where TViewModel : IViewModel
    {
        private const int NonexistentEntityId = 13597;

        [TestMethod]
        public async Task UnauthorizedUserCannotChangeEntityAsync()
        {
            await TestInitialize;
            PrepareForCreate();
            await Generator.FlushAsync();
            var model = CreateModel();

            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
               Target,
               model);
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithFakeCredentialsCannotChangeEntityAsync()
        {
            await TestInitialize;
            PrepareForCreate();
            Entity = new TEntity { Id = NonexistentEntityId };
            await Generator.FlushAsync();

            var fakeModel = CreateModel();

            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
               Target,
               fakeModel,
               credentials: new NetworkCredential("Organization\\NonExistLogin", "password"));
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithoutPermissionCannotChangeEntityAsync()
        {
            await ApiCallTestAsync(
                () =>
                {
                    Entity = CreateEntityToModify();
                    return Task.Delay(0);
                },
                () => CreateModel(),
                HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task UserWithoutPermissionCannotCreateEntityAsync()
        {
            await ApiCallTestAsync(
                () =>
                {
                    PrepareForCreate();
                    return Task.Delay(0);
                },
                () => CreateModel(),
                HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task UserCanChangeEntityAsync()
        {
            await UpdateTestsAsync(CreateEntityToModify, LockEntity, CreateModel, VerifyPersistenceAsync);
        }

        [TestMethod]
        public async Task UserCanCreateEntityAsync()
        {
            await CreateTestsAsync(PrepareForCreate, CreateModel, VerifyPersistenceAsync);
        }

        [TestMethod]
        public async Task UserCannotChangeNonExistingEntityAsync()
        {
            await TestInitialize;
            CurrentUserGenerator.AddPermission(Permission);
            PrepareForCreate();
            Entity = new TEntity { Id = NonexistentEntityId };
            await Generator.FlushAsync();

            var fakeModel = CreateModel();

            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
               Target,
               fakeModel,
               credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();
        }

        [TestMethod]
        public async Task UserMustHaveLockInOrderToChangeEntityAsync()
        {
            await UpdateValidationErrorTestAsync(
                CreateEntityToModify,
                () => { },
                CreateModel,
                result => result.Errors.Single().Message.ShouldBeEqual("NoLockError"));
        }

        protected abstract TEntity CreateEntityToModify();

        protected abstract TViewModel CreateModel();

        protected virtual void PrepareForCreate()
        {
        }

        protected virtual void VerifyPersistence(TEntity actual)
        {
            Assert.Fail("One verification method must be overriden");
        }

        protected virtual async Task VerifyPersistenceAsync(TEntity entity)
        {
            VerifyPersistence(entity);
            await Task.Delay(0);
        }
    }
}