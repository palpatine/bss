﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "JunctionLock{0}{1}BasicTests")]
    public abstract class BasicScopedJunctionLockTests<TMasterEntity, TMasterEntityIndicator, TRelatedEntity>
        : BasicJunctionLockTests<TMasterEntity, TMasterEntityIndicator, TRelatedEntity>
        where TMasterEntity : VersionedEntity, TMasterEntityIndicator, new()
        where TMasterEntityIndicator : IStorable
        where TRelatedEntity : VersionedEntity, new()
    {
        [TestMethod]
        public async Task UserCannotLockJunctionEntitiesIfItsBelongsToOtherOrganizationAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var otherOrganizationGenerator = Generator.CreateOrganization();
            var masterEntity = MasterEntityGenerator(otherOrganizationGenerator).Entity;
            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        protected abstract IGenerator<TMasterEntity> MasterEntityGenerator(OrganizationGenerator organizationGenerator);

        protected sealed override IGenerator<TMasterEntity> MasterEntityGenerator()
        {
            return MasterEntityGenerator(CurrentOrganizationGenerator);
        }
    }
}