﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "{0}LockBasicTests")]
    public abstract class BasicScopedLockTests<TVersionedEntity, TVersionedEntityIndicator>
        : BasicLockTests<TVersionedEntity, TVersionedEntityIndicator>
        where TVersionedEntity : VersionedEntity, IScopedEntity, TVersionedEntityIndicator, new()
        where TVersionedEntityIndicator : IStorable
    {
        [TestMethod]
        public async Task UserCannotLockEntityIfItsBelongsToOtherOrganizationAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var targetEntity = CreateEntityToLockInOtherOrganization();
            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                            Target(targetEntity.Id),
                            HttpMethod.Post,
                            credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<Lock>()
                .SingleOrDefaultAsync(x => x.TableName == TargetTableName
                                        && x.RecordId == targetEntity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        protected abstract TVersionedEntity CreateEntityToLockInOtherOrganization();
    }
}