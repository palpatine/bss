﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerControllerAndAssignmentJunctionRelatedEntity, "Post{0}{1}BasicTests")]
    public abstract class BasicPostAssignmentJunctionRelationTests<TMaster, TRelated, TRelation, TViewModel>
        : PostAssignmentJunctionRelationTests<TMaster, TRelated, TRelation, TViewModel>
        where TMaster : VersionedEntity, new()
        where TRelated : VersionedEntity, new()
        where TRelation : VersionedEntity, IAssignmentJunctionEntity, new()
        where TViewModel : AssignmentPersistenceViewModel, new()
    {
        private const int NonexistentEntityId = 13597;

        private readonly Type _relatedMarkerType = StorableHelper.GetEntityIndicator(typeof(TRelated));
        private readonly Type _masterMarkerType = StorableHelper.GetEntityIndicator(typeof(TMaster));

        private readonly PropertyInfo _relationRelatedProperty;
        private readonly PropertyInfo _relationMasterProperty;

        protected BasicPostAssignmentJunctionRelationTests()
        {
            _relationRelatedProperty = typeof(TRelation).GetProperties().Single(x => x.PropertyType == _relatedMarkerType);
            _relationMasterProperty = typeof(TRelation).GetProperties().Single(x => x.PropertyType == _masterMarkerType);
        }

        [TestMethod]
        public async Task UnauthorizedUserCannotPostRelationsAsync()
        {
            await TestInitialize;
            Entity = new TMaster { Id = NonexistentEntityId };
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>, ValidationResult>(
                Target,
                Enumerable.Empty<TViewModel>());
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithFakeCredentialsCannotPostRelationsAsync()
        {
            await TestInitialize;
            Entity = new TMaster { Id = NonexistentEntityId };
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>, ValidationResult>(
                Target,
                Enumerable.Empty<TViewModel>(),
            credentials: new NetworkCredential("Organization\\NonExistLogin", "password"));
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserCannotPostRelationsForNonExistingMasterAsync()
        {
            await TestInitialize;
            Entity = new TMaster { Id = NonexistentEntityId };
            CurrentUserGenerator.AddPermission(Permission);
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>, ValidationResult>(
                Target,
                Enumerable.Empty<TViewModel>(),
                credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull("expected TargetNotFound actual: " + string.Join(", ", result.Response.Errors.Select(x => x.ErrorCode + ":" + x.Message)));
        }

        [TestMethod]
        public async Task UserCanPostRelationsAsync()
        {
            IEnumerable<TRelation> relations = null;

            await SuccessResultTestsAsync(
                () =>
                {
                    var master = CreateMasterGenerator();
                    var related1 = CreateRelatedGenerator();
                    var related2 = CreateRelatedGenerator();
                    PrepareForCreate();

                    relations = new[]
                    {
                        RelateEntities(master, related1),
                        RelateEntities(master, related2)
                    };
                    return Tuple.Create(master, new[] { related1, related2 }.OrderBy(z => z.Id).AsEnumerable());
                },
                CreateLock,
                () => relations.Select(CreateModel).ToArray());
        }

        protected virtual void PrepareForCreate()
        {
        }

        protected virtual void CreateLock()
        {
            var session = Generator.CreateSession(CurrentUserGenerator.Entity);
            session.AddJunctionLock(typeof(TRelation), _relationMasterProperty, Entity, Guid.NewGuid().ToString("N"));
        }

        protected abstract TMaster CreateMasterGenerator();

        protected virtual TViewModel CreateModel(TRelation relation)
        {
            var model = CreateModel();
            model.Id = relation.Id;
            model.ElementId = ((IMarker)_relationRelatedProperty.GetValue(relation)).Id;
            model.DateStart = relation.DateStart.Date;
            model.DateEnd = relation.DateEnd?.Date;
            return model;
        }

        protected virtual TViewModel CreateModel() => new TViewModel();

        protected abstract TRelated CreateRelatedGenerator();

        protected abstract TRelation RelateEntities(
            TMaster master,
            TRelated related);

        protected TViewModel CreateModel(TRelated related, int id)
        {
            var model = CreateModel();
            model.Id = id;
            model.ElementId = related.Id;
            model.DateStart = DateTime.Today;

            return model;
        }
    }
}