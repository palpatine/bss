﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.Web;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerControllerAndSlave, "Post{0}Dominates{1}BasicTests")]
    public abstract class BasicPostEntityDominatesSlaveDependencyTests<TEntity, TViewModel, TSlaveEntity>
        : BasePostVersionedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, IScopedEntity, IDatedEntity, new()
        where TViewModel : IViewModel
        where TSlaveEntity : VersionedEntity, IDatedEntity, new()
    {
        [TestMethod]
        public async Task UserCannotSetEntityDateEndThatIsLessThanMaximumSlavesDateEndAsync()
        {
            await TestInitialize;
            var targetDateEnd = DateTime.Today.AddDays(1);

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");

            var expectedErrorCode = $"{StorableHelper.GetEntityName(typeof(TEntity))}_DateEnd_BeAfter";
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == expectedErrorCode).ShouldNotBeNull("expected DateEndError");

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.ShouldNotBeNull();
            entity.DateEnd.ShouldBeEqual(Entity.DateEnd);
        }

        [TestMethod]
        public async Task UserCannotSetEntityDateEndWhenAnySlavesDateEndIsNullAsync()
        {
            await TestInitialize;
            Action<IDatedEntity> slaveDateRange = x =>
            {
                x.DateStart = DateTime.Today.AddDays(-2);
                x.DateEnd = null;
            };
            var targetDateEnd = DateTime.Today;

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd,
                slaveDateRange);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            var expectedErrorCode = $"{StorableHelper.GetEntityName(typeof(TEntity))}_DateEnd_BeEmpty";
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == expectedErrorCode).ShouldNotBeNull("expected DateEndError");

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.ShouldNotBeNull();
            entity.DateEnd.ShouldBeEqual(Entity.DateEnd);
        }

        [TestMethod]
        public async Task UserCannotSetEntityDateStartThatIsGreaterThanMinimumSlavesDateStartAsync()
        {
            await TestInitialize;
            var targetDateStart = DateTime.Today.AddDays(-1);

            var result = await PrepareAndRun(
                targetDateStart,
                Entity.DateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            var expectedErrorCode = $"{StorableHelper.GetEntityName(typeof(TEntity))}_DateStart_BeBefore";
            result.Response.Errors.SingleOrDefault(x => x.ErrorCode == expectedErrorCode).ShouldNotBeNull("expected DateStartError");

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.ShouldNotBeNull();
            entity.DateStart.ShouldBeEqual(Entity.DateStart);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateEndThatIsEqualToMaximumSlavesDateEndAsync()
        {
            await TestInitialize;
            var targetDateEnd = DateTime.Today.AddDays(2);

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateEnd.ShouldBeEqual(targetDateEnd);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateEndThatIsGreaterThanMaximumSlavesDateEndAsync()
        {
            await TestInitialize;
            var targetDateEnd = DateTime.Today.AddDays(3);

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateEnd.ShouldBeEqual(targetDateEnd);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateEndToNullWhenAllSlavesDateEndAreNotNullAsync()
        {
            await TestInitialize;

            var result = await PrepareAndRun(
                Entity.DateStart,
                null);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateEnd.ShouldBeEqual(null);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateEndWhenOnlyDeletedSlavesExistsAsync()
        {
            await TestInitialize;
            var targetDateEnd = DateTime.Today.AddDays(3);

            var result = await PrepareAndRun(
                Entity.DateStart,
                targetDateEnd,
                null,
                false);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateEnd.ShouldBeEqual(targetDateEnd);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateStartThatIsEqualToMinimumSlavesDateStartAsync()
        {
            await TestInitialize;
            var targetDateStart = DateTime.Today.AddDays(-2);

            var result = await PrepareAndRun(
                targetDateStart,
                Entity.DateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateStart.ShouldBeEqual(targetDateStart);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateStartThatIsLessThanMinimumSlavesDateStartAsync()
        {
            await TestInitialize;
            var targetDateStart = DateTime.Today.AddDays(-3);

            var result = await PrepareAndRun(
                targetDateStart,
                Entity.DateEnd);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.ErrorCode + ":" + x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateStart.ShouldBeEqual(targetDateStart);
        }

        [TestMethod]
        public async Task UserCanSetEntityDateStartWhenOnlyDeletedSlavesExistsAsync()
        {
            await TestInitialize;
            var targetDateStart = DateTime.Today.AddDays(-3);

            var result = await PrepareAndRun(
                targetDateStart,
                Entity.DateEnd,
                null,
                false);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(", ", result.Response.Errors.Select(x => x.Message)));

            var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
            entity.DateStart.ShouldBeEqual(targetDateStart);
        }

        protected abstract TEntity CreateEntityToModify(
            Action<IDatedEntity> masterDateRange);

        protected abstract TSlaveEntity CreateSlaveEntity(
            TEntity master,
            Action<IDatedEntity> masterDateRange,
            Action<IDatedEntity> slaveDateRange);

        protected abstract TViewModel GetViewModel(
            DateTime dateStart,
            DateTime? dateEnd);

        private async Task<ClientResponse<ValidationResult>> PrepareAndRun(
                                                                                                                    DateTime targetDateSatart,
            DateTime? targetDateEnd,
            Action<IDatedEntity> slaveDateRange = null,
            bool createSlave = true)
        {
            Action<IDatedEntity> masterDateRange = x =>
            {
                x.DateStart = DateTime.Today.AddDays(-4);
                x.DateEnd = DateTime.Today.AddDays(4);
            };
            if (slaveDateRange == null)
            {
                slaveDateRange = x =>
                {
                    x.DateStart = DateTime.Today.AddDays(-2);
                    x.DateEnd = DateTime.Today.AddDays(2);
                };
            }

            CurrentUserGenerator.AddPermission(Permission);
            Entity = CreateEntityToModify(masterDateRange);
            var deletedSlave = CreateSlaveEntity(Entity, masterDateRange, masterDateRange);
            if (createSlave)
            {
                CreateSlaveEntity(Entity, masterDateRange, slaveDateRange);
            }

            LockEntity();
            await Generator.FlushAsync();
            await DeleteVersionedEntityAsync(deletedSlave);

            Model = GetViewModel(targetDateSatart, targetDateEnd);
            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            return result;
        }
    }
}