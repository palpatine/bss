﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    public abstract class GetAssignmentJunctionRelationTests<TMaster, TRelated, TViewModel>
        : BaseApiTests
        where TMaster : VersionedEntity, new()
        where TRelated : VersionedEntity, new()
        where TViewModel : AssignmentViewModel, new()
    {
        public IEnumerable<TRelated> Related { get; private set; }

        protected TMaster Entity { get; set; }

        protected abstract string Permission { get; }

        protected Uri Target
        {
            get
            {
                return AddressProvider.Get(
                    (AssignmentsController x) =>
                        x.Get(StorableHelper.GetTableKey(typeof(TMaster)), Entity.Id, StorableHelper.GetTableKey(typeof(TRelated))));
            }
        }

        protected async Task<IEnumerable<TViewModel>> ApiCallTestAsync(
            Func<Tuple<TMaster, IEnumerable<TRelated>>> prepare,
            HttpStatusCode expectedResult)
        {
            return await ApiCallTestAsync(
                async () =>
                {
                    await Task.Delay(0);
                    return prepare();
                },
                expectedResult);
        }

        protected async Task<IEnumerable<TViewModel>> ApiCallTestAsync(
            Func<Task<Tuple<TMaster, IEnumerable<TRelated>>>> prepare,
            HttpStatusCode expectedResult)
        {
            await TestInitialize;
            var state = await prepare();
            Entity = state.Item1;
            Related = state.Item2;
            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(
                Target,
                credentials: Credentials);
            result.Code.ShouldBeEqual(expectedResult);

            return result.Response;
        }

        protected override void InitializeState()
        {
            Related = null;
            Entity = null;
            CurrentOrganizationGenerator.AddPermissions(
                Permission);
        }

        protected async Task ResultTestsAsync(
            Func<Tuple<TMaster, IEnumerable<TRelated>>> createState,
            Func<IDatabaseAccessor, IEnumerable<TViewModel>, Task> verifyResultAsync)
        {
            Func<Task<Tuple<TMaster, IEnumerable<TRelated>>>> prepare = async () =>
            {
                await Task.Delay(0);
                CurrentUserGenerator.AddPermission(Permission);
                var state = createState();
                return state;
            };

            var result = await ApiCallTestAsync(
                prepare,
                HttpStatusCode.OK);

            await verifyResultAsync(DatabaseAccessor, result);
        }
    }
}