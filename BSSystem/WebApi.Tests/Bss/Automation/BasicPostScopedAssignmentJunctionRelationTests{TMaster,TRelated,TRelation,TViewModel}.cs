﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "Post{0}{1}BasicTests")]
    public abstract class BasicPostScopedAssignmentJunctionRelationTests<TMaster, TRelated, TRelation, TViewModel>
        : BasicPostAssignmentJunctionRelationTests<TMaster, TRelated, TRelation, TViewModel>
        where TMaster : VersionedEntity, IScopedEntity, new()
        where TRelated : VersionedEntity, IScopedEntity, new()
        where TViewModel : AssignmentPersistenceViewModel, new()
        where TRelation : VersionedEntity, IAssignmentJunctionEntity, new()
    {
        [TestMethod]
        public async Task EntitiesHaveDatesAdjustedAccordingToOrganizationTimeZoneAsync()
        {
            await SuccessResultTestsAsync(
                () =>
                {
                    var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Tonga Standard Time");
                    CurrentOrganizationGenerator.AddSetting(SystemSettingsProvider.OrganizationTimeZone, timeZone.Id);
                    var master = CreateMasterGenerator();
                    var related1 = CreateRelatedGenerator();
                    var related2 = CreateRelatedGenerator();
                    PrepareForCreate();
                    return Tuple.Create(master, new[] { related1, related2 }.OrderBy(z => z.Id).AsEnumerable());
                },
                CreateLock,
                () => Related
                .Select((x, id) => CreateModel(x, -id - 1)).ToArray());
        }

        protected abstract TMaster CreateMaster(
            Action<IDatedEntity> configure);

        protected override TMaster CreateMasterGenerator()
        {
            return CreateMaster(
                x => { x.DateStart = DateTime.Today; });
        }

        protected abstract TRelated CreateRelated(
            Action<IDatedEntity> configure);

        protected override TRelated CreateRelatedGenerator()
        {
            return CreateRelated(
                x => { x.DateStart = DateTime.Today; });
        }

        protected override TRelation RelateEntities(
            TMaster master,
            TRelated related)
        {
            return RelateEntities(
                master,
                related,
                x => { x.DateStart = DateTime.Today; });
        }

        protected abstract TRelation RelateEntities(
            TMaster master,
            TRelated related,
            Action<IDatedEntity> configure);
    }
}