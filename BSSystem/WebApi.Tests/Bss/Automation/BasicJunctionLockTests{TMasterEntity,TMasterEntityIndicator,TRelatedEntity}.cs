﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerControllerAndAssignmentJunctionRelatedEntity, "JunctionLock{0}{1}BasicTests")]
    public abstract class BasicJunctionLockTests<TMasterEntity, TMasterEntityIndicator, TRelatedEntity>
        : BaseApiTests
        where TMasterEntity : VersionedEntity, TMasterEntityIndicator, new()
        where TMasterEntityIndicator : IStorable
        where TRelatedEntity : VersionedEntity, new()
    {
        private const int NonexistentEntityId = 13597;

        protected string JunctionTableName
        {
            get
            {
                Type junctionTableType;
                StorableHelper.TryGetJunctionTable(
                    typeof(TMasterEntity),
                    typeof(TRelatedEntity),
                    out junctionTableType);
                var junctionTableName = StorableHelper.GetQualifiedTableName(junctionTableType);
                return junctionTableName;
            }
        }

        protected string MasterColumnName
        {
            get
            {
                var columnName = string.Format(
                    CultureInfo.InvariantCulture,
                    "[{0}Id]",
                    StorableHelper.GetEntityName(typeof(TMasterEntity)));
                return columnName;
            }
        }

        protected string MasterTableName { get; } = StorableHelper.GetQualifiedTableName(typeof(TMasterEntity));

        protected abstract string Permission { get; }

        protected string RelatedTableKey { get; } = StorableHelper.GetTableKey<TRelatedEntity>();

        [TestMethod]
        public async Task UnauthorizedUserCannotLockJunctionEntitiesAsync()
        {
            await TestInitialize;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
               Target(NonexistentEntityId),
                HttpMethod.Post);
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithFakeCannotLockJunctionEntitiesAsync()
        {
            await TestInitialize;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
               Target(NonexistentEntityId),
                HttpMethod.Post,
               credentials: new NetworkCredential("Organization\\NonExistLogin", "password"));
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithoutPermissionCannotLockJunctionEntitiesAsync()
        {
            await TestInitialize;

            var masterEntity = MasterEntityGenerator().Entity;
            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCanLockJunctionEntitiesAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var masterEntity = MasterEntityGenerator().Entity;
            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeTrue();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldNotBeNull();
        }

        [TestMethod]
        public async Task UserCanLockJunctionEntitiesIfItsAlreadyLockedByHimselfAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var masterEntity = MasterEntityGenerator().Entity;
            var currentUser = CurrentUserGenerator.Entity;

            CreateJunctionLock(currentUser, masterEntity);

            await Generator.FlushAsync();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var currentLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntity.Id
                                        && x.User.IsEquivalent(currentUser));

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeTrue();

            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldNotBeNull();
            targetLock.Id.ShouldBeEqual(currentLock.Id);
            (targetLock.OperationToken == currentLock.OperationToken).ShouldBeFalse();
            targetLock.Session.IsEquivalent(currentLock.Session).ShouldBeFalse();
            result.Response.OperationToken.ShouldBeEqual(targetLock.OperationToken);
        }

        [TestMethod]
        public async Task UserCannotLockJunctionEntitiesIfCrossJunctionLockExistsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var masterEntityGenerator = MasterEntityGenerator();
            var currentUser = CurrentUserGenerator.Entity;

            var otherUser = CurrentOrganizationGenerator.CreateUser();
            var relatedEntity = CreateRelatedEntity();
            CreateRelationEntityToLock(
                 masterEntityGenerator,
                 relatedEntity);
            CreateJunctionLock(otherUser.Entity, relatedEntity);

            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntityGenerator.Entity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "LockError").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntityGenerator.Entity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCannotLockJunctionEntitiesIfCrossJunctionLockExistsOwnedByTheSameUserAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var masterEntityGenerator = MasterEntityGenerator();
            var currentUser = CurrentUserGenerator.Entity;

            var relatedEntity = CreateRelatedEntity();
            CreateRelationEntityToLock(
                 masterEntityGenerator,
                 relatedEntity);
            CreateJunctionLock(currentUser, relatedEntity);

            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntityGenerator.Entity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "LockError").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntityGenerator.Entity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCannotLockJunctionEntitiesIfItsAlreadyLockedAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var masterEntity = MasterEntityGenerator().Entity;
            var currentUser = CurrentUserGenerator.Entity;

            var otherUser = CurrentOrganizationGenerator.CreateUser();
            CreateJunctionLock(otherUser.Entity, masterEntity);

            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "LockError").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCannotLockJunctionEntitiesIfSingleLockExistsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var masterEntityGenerator = MasterEntityGenerator();
            var currentUser = CurrentUserGenerator.Entity;

            var otherUser = CurrentOrganizationGenerator.CreateUser();
            var session = Generator.CreateSession(otherUser.Entity);
            var relatedEntity = CreateRelatedEntity();
            var relationEntityToLock = CreateRelationEntityToLock(
                masterEntityGenerator,
                relatedEntity);
            session.AddLock(
                relationEntityToLock.GetType(),
                relationEntityToLock,
                "token");

            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntityGenerator.Entity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "LockError").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntityGenerator.Entity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCannotLockJunctionEntitiesWhenMasterIsDeletedAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var masterEntityGenerator = MasterEntityGenerator();
            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            await DeleteVersionedEntityAsync(masterEntityGenerator.Entity);

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(masterEntityGenerator.Entity.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == masterEntityGenerator.Entity.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCannotLockJunctionEntitiesWhenMasterNotExistAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(NonexistentEntityId),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<JunctionLock>()
                .SingleOrDefaultAsync(x => x.TableName == JunctionTableName
                                        && x.ColumnName == MasterColumnName
                                        && x.ColumnValue == NonexistentEntityId
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        protected abstract TRelatedEntity CreateRelatedEntity();

        protected abstract IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<TMasterEntity> activityGenerator,
            TRelatedEntity relatedEntity);

        protected abstract IGenerator<TMasterEntity> MasterEntityGenerator();

        protected abstract Uri Target(int id);

        private void CreateJunctionLock(
            IUser changer,
            IStorable masterEntity)
        {
            Type junctionTableType;
            PropertyInfo masterProperty;
            if (masterEntity.GetType() == typeof(TMasterEntity))
            {
                StorableHelper.TryGetJunctionTable(
                    typeof(TMasterEntity),
                    typeof(TRelatedEntity),
                    out junctionTableType);
                masterProperty = junctionTableType.GetProperty(StorableHelper.GetEntityName(typeof(TMasterEntity)));
            }
            else if (masterEntity.GetType() == typeof(TRelatedEntity))
            {
                StorableHelper.TryGetJunctionTable(
                    typeof(TRelatedEntity),
                    typeof(TMasterEntity),
                    out junctionTableType);
                masterProperty = junctionTableType.GetProperty(StorableHelper.GetEntityName(typeof(TRelatedEntity)));
            }
            else
            {
                throw new InvalidOperationException();
            }

            var session = Generator.CreateSession(changer);
            session.AddJunctionLock(junctionTableType, masterProperty, masterEntity, "token");
        }
    }
}