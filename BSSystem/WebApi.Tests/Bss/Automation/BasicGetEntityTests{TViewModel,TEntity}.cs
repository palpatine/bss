﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "Get{0}BasicTests")]
    public abstract class BasicGetEntityTests<TViewModel, TEntity>
        : BaseApiTests
        where TViewModel : IViewModel
        where TEntity : Entity, new()
    {
        private const int NonexistentEntityId = 13597;

        protected TEntity Entity { get; private set; }

        protected abstract string Permission { get; }

        protected abstract Uri Target { get; }

        [TestMethod]
        public async Task UnauthorizedUserCannotGetEntityAsync()
        {
            await TestInitialize;
            Entity = new TEntity { Id = NonexistentEntityId };
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
                Target);
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithFakeCredentialsCannotGetEntityAsync()
        {
            await TestInitialize;
            Entity = new TEntity { Id = NonexistentEntityId };
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
                Target,
                credentials: new NetworkCredential("Organization\\NonExistLogin", "password"));
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithGlobalPermissionCanGetEntityByIdAsync()
        {
            await TestInitialize;
            CurrentUserGenerator.AddPermission(Permission);
            await FinalizeInitializationAsync();
            var result = await Client.CallServiceAsync<TViewModel>(
                Target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            VerifySuccessResult(result.Response, Entity);
        }

        [TestMethod]
        public async Task UserWithGlobalPermissionCannotGetDeletedEntityByIdAsync()
        {
            await TestInitialize;
            await Generator.FlushAsync();
            await DeleteEntityAsync(Entity);

            var result = await Client.CallServiceAsync<ValidationResult>(
                Target,
                credentials: new NetworkCredential("Organization\\NonExistLogin", "password"));
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithGlobalPermissionCannotGetNotExistingEntityAsync()
        {
            await TestInitialize;
            Entity = new TEntity { Id = NonexistentEntityId };
            CurrentUserGenerator.AddPermission(Permission);
            await FinalizeInitializationAsync();
            var result = await Client.CallServiceAsync<TViewModel>(
                Target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task UserWithoutGlobalPermissionCannotGetEntityByIdAsync()
        {
            await TestInitialize;
            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<TViewModel>(
                Target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        protected abstract TEntity CreateEntity();

        protected override void InitializeState()
        {
            Entity = CreateEntity();
            CurrentOrganizationGenerator.AddPermissions(Permission);
        }

        protected abstract void VerifySuccessResult(
            TViewModel actual,
            TEntity expected);
    }
}