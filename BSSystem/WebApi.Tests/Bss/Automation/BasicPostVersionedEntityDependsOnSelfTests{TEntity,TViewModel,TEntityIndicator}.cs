﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [BasicPostVersionedEntityDependsOnSelfAttribute(GenericTestMode.Inherit, "Post{0}DependsOnSelfBasicTests")]
    public abstract class BasicPostVersionedEntityDependsOnSelfTests<TEntity, TViewModel, TEntityIndicator>
        : BasicPostScopedEntityDependsOnVersionedEntityTests<TEntity, TViewModel, TEntity, TEntityIndicator>
        where TEntity : VersionedEntity, IScopedEntity, TEntityIndicator, new()
        where TViewModel : IViewModel
        where TEntityIndicator : IStorable
    {
        [TestMethod]
        public async Task UserCannotUpdateEntityToBeItsOwnMasterAsync()
        {
            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    Entity = CreateEntityToModify(null, x => { });
                },
                () =>
                {
                    var model = CreateModel(Entity);
                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_CyclicReference"));
        }

        [TestMethod]
        public async Task UserCannotUpdateEntityToCreateLoopOnSecondLevelAsync()
        {
            TEntity dependency = null;
            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    Entity = CreateEntityToModify(null, x => { });
                    var first = CreateEntityToModify(Entity, x => { });
                    dependency = CreateEntityToModify(first, x => { });
                },
                () =>
                {
                    var model = CreateModel(dependency);
                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_CyclicReference"));
        }
    }
}