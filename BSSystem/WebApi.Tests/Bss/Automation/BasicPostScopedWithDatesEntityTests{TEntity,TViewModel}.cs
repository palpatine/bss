﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "Post{0}BasicScopedWithDatesTests", EntityFilters = new[] { typeof(EntityWithDatePropertyFilter) })]
    public abstract class BasicPostScopedWithDatesEntityTests<TEntity, TViewModel>
        : BasePostVersionedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, IScopedEntity, new()
        where TViewModel : IViewModel
    {
        [TestMethod]
        public async Task CreatedEntityHasDatesAdjustedAccordingToOrganizationTimeZoneAsync()
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Tonga Standard Time");
            var dateStart = DateTime.Today.AddDays(-3);
            var dateEnd = DateTime.Today.AddDays(3);

            await CreateTestsAsync(
                () =>
                {
                    CurrentOrganizationGenerator.AddSetting(SystemSettingsProvider.OrganizationTimeZone, timeZone.Id);
                    PrepareForCreate(
                        CurrentOrganizationGenerator,
                        x =>
                        {
                            x.DateStart = DateTime.Today.AddDays(-5);
                            x.DateEnd = DateTime.Today.AddDays(5);
                        });
                },
                () =>
                {
                    var model = CreateModel(dateStart.ToUnspecified(), dateEnd.ToUnspecified(), DateTime.Today.ToUnspecified());
                    return model;
                },
                entity =>
                {
                    Verify(CurrentOrganizationGenerator, entity, Model);
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task UpdatedEntityHasDatesAdjustedAccordingToOrganizationTimeZoneAsync()
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Tonga Standard Time");
            var dateStart = DateTime.Today.AddDays(-3);
            var dateEnd = DateTime.Today.AddDays(3);

            await CreateTestsAsync(
                () =>
                {
                    CurrentOrganizationGenerator.AddSetting(SystemSettingsProvider.OrganizationTimeZone, timeZone.Id);
                    Entity = CreateEntityToModify(
                        CurrentOrganizationGenerator,
                        x =>
                        {
                            x.DateStart = dateStart.AddDays(-3);
                            x.DateEnd = dateEnd.AddDays(3);
                        },
                        x =>
                        {
                            x.DateStart = dateStart.AddDays(-1);
                            x.DateEnd = dateEnd.AddDays(1);
                        },
                        DateTime.Today);
                },
                () =>
                {
                    var model = CreateModel(dateStart.ToUnspecified(), dateEnd.ToUnspecified(), DateTime.Today.ToUnspecified());
                    return model;
                },
                entity =>
                {
                    Verify(CurrentOrganizationGenerator, entity, Model);
                    return Task.Delay(0);
                });
        }

        protected abstract TEntity CreateEntityToModify(
            OrganizationGenerator generator,
            Action<IDatedEntity> configureMaster,
            Action<IDatedEntity> configureSlave,
            DateTime otherDate);

        protected abstract TViewModel CreateModel(
            DateTime dateStart,
            DateTime? dateEnd,
            DateTime otherDate);

        protected virtual void PrepareForCreate(
            OrganizationGenerator generator,
            Action<IDatedEntity> configure)
        {
        }

        protected abstract void Verify(
            OrganizationGenerator generator,
            TEntity actual,
            TViewModel expected);
    }
}