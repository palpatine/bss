﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "Get{0}{1}BasicTests")]
    public abstract class BasicScopedAssignmentJunctionRelationTests<TMaster, TRelated, TRelation, TViewModel>
        : BasicAssignmentJunctionRelationTests<TMaster, TRelated, TRelation, TViewModel>
        where TMaster : VersionedEntity, IScopedEntity, new()
        where TRelated : VersionedEntity, IScopedEntity, new()
        where TViewModel : AssignmentViewModel, new()
        where TRelation : VersionedEntity, IAssignmentJunctionEntity, new()
    {
        [TestMethod]
        public async Task DatesOfJunctionEntitiesShouldBeAdjustedAccordingToOrganizationTimeZoneSetting()
        {
            await ResultTestsAsync(
                () =>
                {
                    var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Tonga Standard Time");
                    CurrentOrganizationGenerator.AddSetting(SystemSettingsProvider.OrganizationTimeZone, timeZone.Id);
                    var master = CreateMasterGenerator();
                    var related1 = CreateRelatedGenerator();
                    var related2 = CreateRelatedGenerator();

                    RelateEntities(master, related1);
                    RelateEntities(master, related2);
                    return Tuple.Create(master, new[] { related1, related2 }.OrderBy(z => z.Id).AsEnumerable());
                },
                async (databaseAccessor, data) =>
                {
                    var expected = await GetRelations(databaseAccessor);
                    Verify(data, expected);
                });
        }

        protected override TMaster CreateMasterGenerator()
        {
            return CreateMaster(
                x =>
                {
                    x.DateStart = DateTime.Today;
                });
        }

        protected abstract TMaster CreateMaster(
            Action<IDatedEntity> configure);

        protected override TRelated CreateRelatedGenerator()
        {
            return CreateRelated(
                x =>
                {
                    x.DateStart = DateTime.Today;
                });
        }

        protected abstract TRelated CreateRelated(
            Action<IDatedEntity> configure);

        protected override void RelateEntities(
            TMaster master,
            TRelated related)
        {
            RelateEntities(
                master,
                related,
                x =>
                {
                    x.DateStart = DateTime.Today;
                });
        }

        protected abstract void RelateEntities(
            TMaster master,
            TRelated related,
            Action<IDatedEntity> configure);
    }
}