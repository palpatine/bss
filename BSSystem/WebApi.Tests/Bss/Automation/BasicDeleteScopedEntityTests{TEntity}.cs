﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "Delete{0}BasicTests")]
    public abstract class BasicDeleteScopedEntityTests<TEntity>
        : BasicDeleteVersionedEntityTests<TEntity>
        where TEntity : VersionedEntity, IScopedEntity, new()
    {
        [TestMethod]
        public virtual async Task UserCannotDeleteEntityFromOtherOrganizationAsync()
        {
            Func<Task> prepare = async () =>
            {
                CurrentUserGenerator.AddPermission(Permission);
                var organization = Generator.CreateOrganization();
                Entity = CreateEntityToModify(organization);
                await Task.Delay(0);
            };

            await DeleteValidationErrorTestAsync(
                prepare,
                (result) =>
                {
                    result.Errors.SingleOrDefault(x => x.ErrorCode == "NoLockError").ShouldNotBeNull();
                });
        }

        protected abstract TEntity CreateEntityToModify(OrganizationGenerator organizationGenerator);

        protected sealed override TEntity CreateEntityToModify()
        {
            return CreateEntityToModify(CurrentOrganizationGenerator);
        }
    }
}