﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "Get{0}BasicScopedWithDatesTests", EntityFilters = new[] { typeof(EntityWithDatePropertyFilter) })]
    public abstract class BasicGetScopedWithDatesTests<TEntity, TViewModel>
        : BaseApiTests
        where TEntity : Entity, IScopedEntity
        where TViewModel : IViewModel
    {
        public TEntity Entity { get; private set; }

        protected abstract string Permission { get; }

        protected abstract Uri Target { get; }

        [TestMethod]
        public async Task EntitiesHaveDatesAdjustedAccordingToOrganizationTimeZoneAsync()
        {
            await TestInitialize;
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Tonga Standard Time");
            CurrentOrganizationGenerator.AddSetting(SystemSettingsProvider.OrganizationTimeZone, timeZone.Id);

            var dateStart = DateTime.Today.AddDays(3);
            var dateEnd = DateTime.Today.AddDays(9);
            var otherDate = DateTime.Today.AddDays(6);
            Entity = CreateEntity(
                x =>
                {
                    x.DateStart = dateStart;
                    x.DateEnd = dateEnd;
                },
                otherDate);

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);
            await FinalizeInitializationAsync();
            var result = await Client.CallServiceAsync<TViewModel>(
                Target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            Verify(result.Response, Entity);
        }

        protected abstract TEntity CreateEntity(Action<IDatedEntity> configure, DateTime otherDate);

        protected abstract void Verify(TViewModel actual, TEntity expected);
    }
}