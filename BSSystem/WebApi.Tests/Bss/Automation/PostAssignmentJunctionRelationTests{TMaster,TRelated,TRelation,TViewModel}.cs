﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    public abstract class PostAssignmentJunctionRelationTests<TMaster, TRelated, TRelation, TViewModel>
        : BaseApiTests
        where TMaster : VersionedEntity, new()
        where TRelated : VersionedEntity, new()
        where TRelation : VersionedEntity, IAssignmentJunctionEntity, new()
        where TViewModel : AssignmentPersistenceViewModel, new()
    {
        public IEnumerable<TViewModel> Model { get; set; }

        public IEnumerable<TRelated> Related { get; private set; }

        protected TMaster Entity { get; set; }

        protected abstract string Permission { get; }

        protected Uri Target
        {
            get
            {
                return AddressProvider.Get(
                  (AssignmentsController x) =>
                      x.Set(StorableHelper.GetTableKey(typeof(TMaster)), Entity.Id, StorableHelper.GetTableKey(typeof(TRelated)), null));
            }
        }

        protected async Task<ValidationResult> ApiCallTestAsync(
            Func<Tuple<TMaster, IEnumerable<TRelated>>> prepare,
            Action createLock,
            Func<IEnumerable<TViewModel>> createPayload,
            HttpStatusCode expectedResult)
        {
            return await ApiCallTestAsync(
                async () =>
                {
                    await Task.Delay(0);
                    return prepare();
                },
                async () =>
                {
                    await Task.Delay(0);
                    createLock();
                },
                async () =>
                {
                    await Task.Delay(0);
                    return createPayload();
                },
                expectedResult);
        }

        protected async Task<ValidationResult> ApiCallTestAsync(
            Func<Task<Tuple<TMaster, IEnumerable<TRelated>>>> prepare,
            Func<Task> createLock,
            Func<Task<IEnumerable<TViewModel>>> createPayload,
            HttpStatusCode expectedResult)
        {
            await TestInitialize;
            var state = await prepare();
            Entity = state.Item1;
            Related = state.Item2;
            await createLock();
            await FinalizeInitializationAsync();
            Model = await createPayload();
            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            result.Code.ShouldBeEqual(expectedResult);

            return result.Response;
        }

        protected async Task<IEnumerable<TRelation>> GetRelations(IDatabaseAccessor databaseAccessor)
        {
            IEnumerable<TRelation> expected;
            if (typeof(TRelation).Name.IndexOf(typeof(TMaster).Name, StringComparison.InvariantCulture) == 0)
            {
                expected = await databaseAccessor.Query<TRelation>()
                    .Where(x => !x.IsDeleted && x.First.IsEquivalent(Entity))
                    .ToListAsync();
            }
            else
            {
                expected = await databaseAccessor.Query<TRelation>()
                    .Where(x => !x.IsDeleted && x.Second.IsEquivalent(Entity))
                    .ToListAsync();
            }

            return expected;
        }

        protected override void InitializeState()
        {
            Related = null;
            Entity = null;
            CurrentOrganizationGenerator.AddPermissions(
                Permission);
        }

        protected async Task ResultTestsAsync(
            Func<Tuple<TMaster, IEnumerable<TRelated>>> createState,
            Action createLock,
            Func<IEnumerable<TViewModel>> createPayload,
            Func<IDatabaseAccessor, ValidationResult, Task> verifyResultAsync)
        {
            Func<Task<Tuple<TMaster, IEnumerable<TRelated>>>> prepare = async () =>
            {
                await Task.Delay(0);
                CurrentUserGenerator.AddPermission(Permission);
                var state = createState();
                return state;
            };

            var result = await ApiCallTestAsync(
                prepare,
                async () =>
                {
                    await Task.Delay(0);
                    createLock();
                },
                async () =>
                {
                    await Task.Delay(0);
                    return createPayload();
                },
                HttpStatusCode.OK);

            await verifyResultAsync(DatabaseAccessor, result);
        }

        protected async Task SuccessResultTestsAsync(
            Func<Tuple<TMaster, IEnumerable<TRelated>>> createState,
            Action createLock,
             Func<IEnumerable<TViewModel>> createPayload)
        {
            Func<IDatabaseAccessor, ValidationResult, Task> validate = async (x, r) =>
            {
                r.IsValid.ShouldBeTrue(string.Join(", ", r.Errors.Select(y => y.Message)));
                var actual = await GetRelations(x);
                Verify(actual, Model);
            };
            await ResultTestsAsync(createState, createLock, createPayload, validate);
        }

        protected async Task ValidationResultTestsAsync(
           Func<Tuple<TMaster, IEnumerable<TRelated>>> createState,
            Action createLock,
           Func<IEnumerable<TViewModel>> createPayload,
           Action<ValidationResult> verify)
        {
            Func<IDatabaseAccessor, ValidationResult, Task> validate = async (x, r) =>
            {
                r.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
                verify(r);
                await Task.Delay(0);
            };
            await ResultTestsAsync(createState, createLock, createPayload, validate);
        }

        protected virtual void Verify(IEnumerable<TRelation> actual, IEnumerable<TViewModel> expected)
        {
            var actualSorded = actual.OrderBy(x => x.Id);
            var newCount = expected.Count(x => x.Id < 0);
            var maxId = expected.Select(x => x.Id).Where(x => x > 0).Concat(new[] { 0 }).Max();

            foreach (var item in expected)
            {
                if (item.Id == 0)
                {
                    Assert.Inconclusive("model id cannot be 0");
                }

                if (item.Id < 0)
                {
                    //// this is not perfect
                    item.Id = maxId + newCount + 1 + item.Id;
                }
            }

            var expectedSorted = expected.OrderBy(x => x.Id > 0 ? x.Id : 1000 - x.Id);
            using (var item = expectedSorted.GetEnumerator())
            {
                foreach (var relation in actualSorded)
                {
                    item.MoveNext();
                    Verify(relation, item.Current);
                }
            }
        }

        protected abstract void Verify(TRelation actual, TViewModel expected);
    }
}