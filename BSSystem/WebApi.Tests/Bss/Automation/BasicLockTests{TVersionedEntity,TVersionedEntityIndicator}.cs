﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using Qdarc.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "{0}LockBasicTests")]
    public abstract class BasicLockTests<TVersionedEntity, TVersionedEntityIndicator>
        : BaseApiTests
        where TVersionedEntity : VersionedEntity, TVersionedEntityIndicator, new()
        where TVersionedEntityIndicator : IStorable
    {
        private const int NonexistentEntityId = 13597;

        protected abstract string Permission { get; }

        protected string TargetTableName { get; } = StorableHelper.GetQualifiedTableName(typeof(TVersionedEntity));

        protected virtual IUser UserForJunctionLock => CurrentOrganizationGenerator.AdminAlias;

        [TestMethod]
        public async Task UnauthorizedUserCannotLockEntityAsync()
        {
            await TestInitialize;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
               Target(NonexistentEntityId),
                HttpMethod.Post);
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserCanLockEntityAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var target = CreateEntityToLock();
            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(target.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeTrue();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<Lock>()
                .SingleOrDefaultAsync(x => x.TableName == TargetTableName
                                        && x.RecordId == target.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldNotBeNull();
        }

        [TestMethod]
        public async Task UserCanLockEntityIfItsAlreadyLockedByHimselfAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var target = CreateEntityToLock();
            var currentUser = CurrentUserGenerator.Entity;

            var session = Generator.CreateSession(currentUser);
            session.AddLock(target, "token");

            await Generator.FlushAsync();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var currentLock = await databaseAccessor.Query<Lock>()
                 .SingleAsync(x => x.TableName == TargetTableName
                                && x.RecordId == target.Id
                                && x.User.IsEquivalent(currentUser));

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(target.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeTrue();

            var targetLock = await databaseAccessor.Query<Lock>()
                .SingleOrDefaultAsync(x => x.TableName == TargetTableName
                                        && x.RecordId == target.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldNotBeNull();
            targetLock.Id.ShouldBeEqual(currentLock.Id);
            (targetLock.OperationToken == currentLock.OperationToken).ShouldBeFalse();
            targetLock.Session.IsEquivalent(currentLock.Session).ShouldBeFalse();
            result.Response.OperationToken.ShouldBeEqual(targetLock.OperationToken);
        }

        [TestMethod]
        public async Task UserCannotLockDeletedEntityAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var target = CreateEntityToLock();
            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            await DeleteVersionedEntityAsync(target);

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                            Target(target.Id),
                            HttpMethod.Post,
                            credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<Lock>()
                .SingleOrDefaultAsync(x => x.TableName == TargetTableName
                                        && x.RecordId == target.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCannotLockEntityIfItsAlreadyLockedAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var target = CreateEntityToLock();
            var currentUser = CurrentUserGenerator.Entity;

            var otherUser = CurrentOrganizationGenerator.CreateUser();
            var session = Generator.CreateSession(otherUser.Entity);
            session.AddLock(target, "token");

            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                            Target(target.Id),
                            HttpMethod.Post,
                            credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "LockError").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<Lock>()
                .SingleOrDefaultAsync(x => x.TableName == TargetTableName
                                        && x.RecordId == target.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCannotLockEntityIfJunctionLockExistsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var target = CreateEntityToLock();
            var currentUser = CurrentUserGenerator.Entity;

            var session = Generator.CreateSession(UserForJunctionLock);
            session.AddJunctionLock(
                target.GetType(),
                target.GetProperty(x => x.ChangerId),
                UserForJunctionLock,
                "token");

            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                            Target(target.Id),
                            HttpMethod.Post,
                            credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "LockError").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<Lock>()
                .SingleOrDefaultAsync(x => x.TableName == TargetTableName
                                        && x.RecordId == target.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserCannotLockNonexistentEntityAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);

            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                            Target(NonexistentEntityId),
                            HttpMethod.Post,
                            credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.ValidationResult.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.ValidationResult.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<Lock>()
                .SingleOrDefaultAsync(x => x.TableName == TargetTableName
                                        && x.RecordId == NonexistentEntityId
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        [TestMethod]
        public async Task UserWithFakeCredentialsCannotLockEntityAsync()
        {
            await TestInitialize;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
               Target(NonexistentEntityId),
               HttpMethod.Post,
               credentials: new NetworkCredential("Organization\\NonExistLogin", "password"));
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized, result.FailureDescription);
        }

        [TestMethod]
        public async Task UserWithoutWritePermissionCannotLockEntityAsync()
        {
            await TestInitialize;

            var target = CreateEntityToLock();
            var currentUser = CurrentUserGenerator.Entity;
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<LockResultViewModel>(
                Target(target.Id),
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var targetLock = await databaseAccessor.Query<Lock>()
                .SingleOrDefaultAsync(x => x.TableName == TargetTableName
                                        && x.RecordId == target.Id
                                        && x.User.IsEquivalent(currentUser));
            targetLock.ShouldBeNull();
        }

        protected abstract TVersionedEntity CreateEntityToLock();

        protected abstract Uri Target(int id);
    }
}