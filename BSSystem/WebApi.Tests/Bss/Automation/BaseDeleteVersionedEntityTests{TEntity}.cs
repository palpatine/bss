﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Web.ViewModels;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    public abstract class BaseDeleteVersionedEntityTests<TEntity>
        : BaseApiTests
        where TEntity : VersionedEntity, new()
    {
        public string OperationToken => Guid.NewGuid().ToString("N");

        protected TEntity Entity { get; set; }

        protected abstract string Permission { get; }

        protected abstract Uri Target { get; }

        protected async Task<ValidationResult> ApiCallTestAsync(
            Func<Task> prepare,
            HttpStatusCode expectedResult)
        {
            await TestInitialize;
            await prepare();
            await FinalizeInitializationAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
                Target,
                method: HttpMethod.Delete,
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
            result.Code.ShouldBeEqual(expectedResult);

            return result.Response;
        }

        protected abstract TEntity CreateEntityToModify();

        protected async Task DeleteTestsAsync(
            Func<TEntity> createEntityToModify,
            Action lockEntity,
            Func<IDatabaseAccessor, ValidationResult, Task> verifyPersistenceAsync)
        {
            Func<Task> prepare = async () =>
            {
                CurrentUserGenerator.AddPermission(Permission);
                Entity = createEntityToModify();
                lockEntity();
                await Task.Delay(0);
            };
            var result = await ApiCallTestAsync(
                prepare,
                HttpStatusCode.OK);

            result.IsValid.ShouldBeTrue(string.Join(", ", result.Errors.EmptyIfNull().Select(x => x.Message)));

            await verifyPersistenceAsync(DatabaseAccessor, result);
        }

        protected async Task DeleteValidationErrorTestAsync(
            Func<Task> prepare,
            Action<ValidationResult> verifyPersistenceAsync)
        {
            var result = await ApiCallTestAsync(
                prepare,
                HttpStatusCode.OK);

            result.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            verifyPersistenceAsync(result);
        }

        protected override void InitializeState()
        {
            Entity = null;
            CurrentOrganizationGenerator.AddPermissions(
                    Permission);
        }

        protected virtual void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddLock(Entity, OperationToken);
        }
    }
}