﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "{0}DependsOn{1}BasicTests")]
    public abstract class BasicPostScopedEntityDependsOnVersionedEntityTests<TEntity, TViewModel, TMasterEntity, TMasterEntityIndicator>
        : BasicPostVersionedEntityDependsOnVersionedEntityTests<TEntity, TViewModel, TMasterEntity, TMasterEntityIndicator>
            where TEntity : VersionedEntity, IScopedEntity, new()
            where TMasterEntity : VersionedEntity, TMasterEntityIndicator, new()
            where TViewModel : IViewModel
            where TMasterEntityIndicator : IStorable
    {
        [TestMethod]
        public async Task UserCannotUpdateEntityToBecomeSlaveOfEntityFromOtherOrganizationAsync()
        {
            TMasterEntity other = null;
            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    Entity = CreateEntityToModify(CreateDependencyEntity(), x => { });
                    var organization = Generator.CreateOrganization();
                    other = CreateDependencyEntity(organization);
                },
                () =>
                {
                    var model = CreateModel(other);
                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        [TestMethod]
        public async Task UserCannotCreateEntityToBecomeSlaveOfEntityFromOtherOrganizationAsync()
        {
            TMasterEntity other = null;
            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    var organization = Generator.CreateOrganization();
                    other = CreateDependencyEntity(organization);
                },
                () =>
                {
                    var model = CreateModel(other);
                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        protected abstract TMasterEntity CreateDependencyEntity(OrganizationGenerator organizationGenerator);

        protected sealed override TMasterEntity CreateDependencyEntity()
        {
            return CreateDependencyEntity(CurrentOrganizationGenerator);
        }

        protected sealed override TEntity CreateEntityToModify(
            TMasterEntity master,
            Action<TEntity> configure)
        {
            return CreateEntityToModify(CurrentOrganizationGenerator, master, configure);
        }

        protected abstract TEntity CreateEntityToModify(
            OrganizationGenerator organizationGenerator,
            TMasterEntity master,
            Action<TEntity> configure);
    }
}