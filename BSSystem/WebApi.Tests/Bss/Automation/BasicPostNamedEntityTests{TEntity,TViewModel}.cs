﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "Post{0}BasicNamedTests")]
    public abstract class BasicPostNamedEntityTests<TEntity, TViewModel>
        : BasePostVersionedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, INamedEntity, new()
        where TViewModel : IViewModel
    {
        [TestMethod]
        public async Task UserCanCreateEntityWithDuplicateDisplayNameFromDeletedEntityAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.AddPermission(Permission);
            var entity = CreateEntityToModify();

            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(entity);

            Model = CreateModel();
            Model.GetType().GetProperty("DisplayName").SetValue(Model, entity.DisplayName);

            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue();
        }

        [TestMethod]
        public async Task UserCanModifyEntityToHaveDuplicateDisplayNameWithDeletedEntityAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.AddPermission(Permission);
            var entityToDelete = CreateEntityToModify();
            Entity = CreateEntityToModify();
            Entity.DisplayName = NextRandomValue();

            LockEntity();
            await FinalizeInitializationAsync();

            await DeleteVersionedEntityAsync(entityToDelete);

            Model = CreateModel();
            Model.GetType().GetProperty("DisplayName").SetValue(Model, entityToDelete.DisplayName);

            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue();
        }

        [TestMethod]
        public async Task UserCannotCreateEntityWithDuplicateDisplayNameAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.AddPermission(Permission);
            var entityWithDisplayName = CreateEntityToModify();
            PrepareForCreate();

            await FinalizeInitializationAsync();

            var sessionDataProvider = (SessionDataProvider)ServiceLocator.Current.GetInstance<ISessionDataProvider>();
            sessionDataProvider.Scope = CurrentOrganizationGenerator.Entity;
            sessionDataProvider.CurrentUser = CurrentUserGenerator.Entity;
            Model = CreateModel();
            Model.GetType().GetProperty("DisplayName").SetValue(Model, entityWithDisplayName.DisplayName);

            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.Single().Reference.ShouldBeEqual("DisplayName");
        }

        [TestMethod]
        public async Task UserCannotModifyEntityToHaveDuplicateDisplayNameAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.AddPermission(Permission);
            var entityWithDisplayName = CreateEntityToModify();
            Entity = CreateEntityToModify();
            Entity.DisplayName = NextRandomValue();
            LockEntity();
            await FinalizeInitializationAsync();

            var sessionDataProvider = (SessionDataProvider)ServiceLocator.Current.GetInstance<ISessionDataProvider>();
            sessionDataProvider.Scope = CurrentOrganizationGenerator.Entity;
            sessionDataProvider.CurrentUser = CurrentUserGenerator.Entity;

            Model = CreateModel();
            Model.GetType().GetProperty("DisplayName").SetValue(Model, entityWithDisplayName.DisplayName);

            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.Single().Reference.ShouldBeEqual("DisplayName");
        }

        protected abstract TEntity CreateEntityToModify();

        protected abstract TViewModel CreateModel();

        protected virtual void PrepareForCreate()
        {
        }
    }
}