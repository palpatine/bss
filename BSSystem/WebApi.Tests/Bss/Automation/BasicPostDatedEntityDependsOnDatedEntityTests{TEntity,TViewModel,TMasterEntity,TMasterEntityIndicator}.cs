﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerControllerAndMaster, "Post{0}DependsOn{1}BasicTests")]
    public abstract class BasicPostDatedEntityDependsOnDatedEntityTests<TEntity, TViewModel, TMasterEntity, TMasterEntityIndicator>
         : BasePostVersionedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, IDatedEntity, new()
        where TMasterEntity : VersionedEntity, IDatedEntity, TMasterEntityIndicator, new()
        where TViewModel : IViewModel
        where TMasterEntityIndicator : IStorable
    {
        protected static string EntityName { get; } = StorableHelper.GetEntityName(typeof(TEntity));

        protected virtual string RelationName => StorableHelper.GetEntityName(typeof(TMasterEntity));

        [TestMethod]
        public async Task GivenEntityWithMasterUserCannotSetDateStartBeforeMastersDateStartAsync()
        {
            TMasterEntity master = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity(
                        x => { });
                    Entity = CreateEntityToModify(master, x => x.DateStart = DateTime.Today.AddDays(2));
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(-1), null, master);

                    return model;
                },
                x => x.Errors
                    .Select(y => y.ErrorCode).OrderBy(y => y)
                    .AllCorrespondingElementsShouldBeEqual(
                        new[]
                        {
                            EntityName + "_DateStart_BeAfter"
                        }));
        }

        [TestMethod]
        public async Task GivenFiniteEntityWithFiniteMasterUserCanExpandDateRangeWithinMastersSpanAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { x.DateEnd = DateTime.Today.AddDays(4); });
                    Entity = CreateEntityToModify(
                        master,
                        x =>
                    {
                        x.DateStart = DateTime.Today.AddDays(1);
                        x.DateEnd = DateTime.Today.AddDays(2);
                    });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today, DateTime.Today.AddDays(4), master);

                    return model;
                },
                x =>
                {
                    x.DateStart.ShouldBeEqual(DateTime.Today);
                    x.DateEnd.ShouldBeEqual(DateTime.Today.AddDays(4));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task GivenFiniteEntityWithFiniteMasterUserCannotSetDateEndAfterMastersDateEndAsync()
        {
            TMasterEntity master = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity(
                        x => { x.DateEnd = DateTime.Today.AddDays(3); });
                    Entity = CreateEntityToModify(
                        master,
                        x =>
                        {
                            x.DateStart = DateTime.Today.AddDays(1);
                            x.DateEnd = DateTime.Today.AddDays(2);
                        });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), DateTime.Today.AddDays(4), master);
                    return model;
                },
                x => x.Errors
                    .Select(y => y.ErrorCode).OrderBy(y => y)
                    .AllCorrespondingElementsShouldBeEqual(
                        new[]
                        {
                            EntityName + "_DateEnd_BeBefore"
                        }));
        }

        [TestMethod]
        public async Task GivenFiniteEntityWithFiniteMasterUserCannotSetDateEndToNullAsync()
        {
            TMasterEntity master = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity(
                        x => { x.DateEnd = DateTime.Today.AddDays(3); });
                    Entity = CreateEntityToModify(
                        master,
                        x =>
                    {
                        x.DateStart = DateTime.Today.AddDays(1);
                        x.DateEnd = DateTime.Today.AddDays(2);
                    });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), null, master);
                    return model;
                },
                x => x.Errors
                    .Select(y => y.ErrorCode).OrderBy(y => y)
                    .AllCorrespondingElementsShouldBeEqual(
                        new[]
                        {
                            EntityName + "_DateEnd_BeBefore"
                        }));
        }

        [TestMethod]
        public async Task GivenFiniteEntityWithInfiniteMasterUserCanExpandDateRangeWithinMastersSpanAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    Entity = CreateEntityToModify(
                        master,
                        x =>
                    {
                        x.DateStart = DateTime.Today.AddDays(1);
                        x.DateEnd = DateTime.Today.AddDays(2);
                    });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today, DateTime.Today.AddDays(5), master);

                    return model;
                },
                x =>
                {
                    x.DateStart.ShouldBeEqual(DateTime.Today);
                    x.DateEnd.ShouldBeEqual(DateTime.Today.AddDays(5));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task GivenFiniteEntityWithInfiniteMasterUserCanSetDateEndToNullAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    Entity = CreateEntityToModify(
                        master,
                        x =>
                    {
                        x.DateStart = DateTime.Today.AddDays(1);
                        x.DateEnd = DateTime.Today.AddDays(2);
                    });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), null, master);

                    return model;
                },
                x =>
                {
                    x.DateStart.ShouldBeEqual(DateTime.Today.AddDays(1));
                    x.DateEnd.ShouldBeNull();
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task GivenInfiniteEntityWithInfiniteMasterUserCanSetDateEndToAnyValueAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    Entity = CreateEntityToModify(
                        master,
                        x =>
                    {
                        x.DateStart = DateTime.Today.AddDays(1);
                    });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today, DateTime.Today.AddDays(4), master);

                    return model;
                },
                x =>
                {
                    x.DateStart.ShouldBeEqual(DateTime.Today);
                    x.DateEnd.ShouldBeEqual(DateTime.Today.AddDays(4));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task UserCanCreateEntityIfProvidesDatesInRangeOfMastersDatesAndMasterIsFiniteAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    PrepareForCreate();
                    master = CreateDependencyEntity(
                        x => { x.DateEnd = DateTime.Today.AddDays(3); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), DateTime.Today.AddDays(2), master);

                    return model;
                },
                x =>
                {
                    x.DateStart.ShouldBeEqual(DateTime.Today.AddDays(1));
                    x.DateEnd.ShouldBeEqual(DateTime.Today.AddDays(2));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task UserCanCreateEntityIfProvidesDatesInRangeOfMastersDatesAndMasterIsInfiniteAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    PrepareForCreate();
                    master = CreateDependencyEntity(
                        x => { });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), DateTime.Today.AddDays(2), master);

                    return model;
                },
                x =>
                {
                    x.DateStart.ShouldBeEqual(DateTime.Today.AddDays(1));
                    x.DateEnd.ShouldBeEqual(DateTime.Today.AddDays(2));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task UserCanCreateInfiniteEntityIfProvidesDateStartInRangeOfMastersDatesAndMasterIsInfiniteAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    PrepareForCreate();
                    master = CreateDependencyEntity(
                        x => { });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), null, master);

                    return model;
                },
                x =>
                {
                    x.DateStart.ShouldBeEqual(DateTime.Today.AddDays(1));
                    x.DateEnd.ShouldBeNull();
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task UserCannotCreateEntityIfProvidesDateEndAfterMastersDateEndAndMasterAsync()
        {
            TMasterEntity master = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    PrepareForCreate();
                    master = CreateDependencyEntity(
                        x => { x.DateEnd = DateTime.Today.AddDays(2); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), DateTime.Today.AddDays(3), master);

                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_DateEnd_BeBefore"));
        }

        [TestMethod]
        public async Task UserCannotCreateEntityIfProvidesDateStartAfterMastersDateEndAsync()
        {
            TMasterEntity master = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    PrepareForCreate();
                    master = CreateDependencyEntity(
                        x => { x.DateEnd = DateTime.Today.AddDays(2); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(3), DateTime.Today.AddDays(4), master);

                    return model;
                },
                x => x.Errors.Select(y => y.ErrorCode).OrderBy(y => y)
                    .AllCorrespondingElementsShouldBeEqual(
                        new[]
                        {
                            EntityName + "_DateEnd_BeBefore",
                            EntityName + "_DateStart_BeBefore"
                        }));
        }

        [TestMethod]
        public async Task UserCannotCreateEntityIfProvidesDateStartBeforeMastersDateStartAsync()
        {
            TMasterEntity master = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    PrepareForCreate();
                    master = CreateDependencyEntity(x => { });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(-1), null, master);

                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_DateStart_BeAfter"));
        }

        [TestMethod]
        public async Task UserCannotCreateInfiniteEntityIfProvidesDateStartAfterMastersDateStartAndMasterIsFiniteAsync()
        {
            TMasterEntity master = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    PrepareForCreate();
                    master = CreateDependencyEntity(
                        x => { x.DateEnd = DateTime.Today.AddDays(2); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), null, master);

                    return model;
                },
                x => x.Errors
                    .Select(y => y.ErrorCode).OrderBy(y => y)
                    .AllCorrespondingElementsShouldBeEqual(
                        new[]
                        {
                            EntityName + "_DateEnd_BeBefore"
                        }));
        }

        protected abstract TMasterEntity CreateDependencyEntity(Action<IDatedEntity> configure);

        protected abstract TEntity CreateEntityToModify(
            TMasterEntity master,
            Action<TEntity> configure);

        protected abstract TViewModel CreateModel(
                          DateTime dateStart,
          DateTime? dateEnd,
          TMasterEntity master);

        protected virtual void PrepareForCreate()
        {
        }
    }
}