﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "GetList{0}BasicScopedWithDatesTests", EntityFilters = new[] { typeof(EntityWithDatePropertyFilter) })]
    public abstract class BasicGetListScopedWithDatesTests<TEntity, TViewModel>
        : BaseApiTests
        where TEntity : Entity, IScopedEntity
        where TViewModel : IViewModel
    {
        private List<TEntity> _entities;
        protected abstract string Permission { get; }

        protected abstract Uri Target { get; }

        [TestMethod]
        public async Task EntitiesHaveDatesAdjustedAccordingToOrganizationTimeZoneAsync()
        {
            await TestInitialize;
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Tonga Standard Time");
            CurrentOrganizationGenerator.AddSetting(SystemSettingsProvider.OrganizationTimeZone, timeZone.Id);

            _entities = new List<TEntity>();

            for (var i = -5; i < 6; i++)
            {
                var dateStart = DateTime.Today.AddDays(3 * i);
                var dateEnd = DateTime.Today.AddDays(3 * (i + 2));
                var otherDate = DateTime.Today.AddDays(3 * (i + 1));
                var entity = CreateEntity(
                    x =>
                    {
                        x.DateStart = dateStart;
                        x.DateEnd = dateEnd;
                    },
                    otherDate);
                _entities.Add(entity);
            }

            CurrentOrganizationGenerator.AddPermissions(Permission);
            CurrentUserGenerator.AddPermission(Permission);
            await FinalizeInitializationAsync();
            var result = await Client.CallServiceAsync<IEnumerable<TViewModel>>(
                Target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            CompareElements(result.Response, _entities);
        }

        protected virtual void CompareElements(IEnumerable<TViewModel> actual, IEnumerable<TEntity> expected)
        {
            var property = typeof(TViewModel).GetProperty("Id");
            var actualSorded = actual.OrderBy(x => property.GetValue(x));
            var expectedSorted = expected.OrderBy(x => x.Id);

            using (var item = expectedSorted.GetEnumerator())
            {
                foreach (var viewModel in actualSorded)
                {
                    item.MoveNext();
                    Verify(viewModel, item.Current);
                }
            }
        }

        protected abstract TEntity CreateEntity(Action<IDatedEntity> configure, DateTime otherDate);

        protected abstract void Verify(TViewModel actual, TEntity expected);
    }
}