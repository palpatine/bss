﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.Inherit, "Post{0}BasicScopedNamedTests")]
    public abstract class BasicPostScopedNamedEntityTests<TEntity, TViewModel>
        : BasicPostNamedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, IScopedEntity, INamedEntity, new()
        where TViewModel : IViewModel
    {
        [TestMethod]
        public async Task UserCanCreateEntityWithDuplicateDisplayNameCrossOrganizationAsync()
        {
            await TestInitialize;

            CurrentUserGenerator.AddPermission(Permission);
            var organization = Generator.CreateOrganization();
            var entity = CreateEntityToModify(organization);
            PrepareForCreate();
            var session = Generator.CreateSession(CurrentUserGenerator.Entity).Entity;

            await FinalizeInitializationAsync();

            var sessionDataProvider = (SessionDataProvider)ServiceLocator.Current.GetInstance<ISessionDataProvider>();
            sessionDataProvider.Scope = CurrentOrganizationGenerator.Entity;
            sessionDataProvider.CurrentUser = CurrentUserGenerator.Entity;
            sessionDataProvider.Session = session;

            Model = CreateModel();
            Model.GetType().GetProperty("DisplayName").SetValue(Model, entity.DisplayName);

            var result = await Client.CallServiceAsync<TViewModel, ValidationResult>(
                Target,
                Model,
                credentials: Credentials);
            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue("Expected server to accept payload: " + string.Join(",", result.Response.Errors.Select(x => x.Message)));
        }

        [TestMethod]
        public async Task UserCannotChangeEntityFromOtherOrganizationAsync()
        {
            await UpdateValidationErrorTestAsync(
                 () =>
                 {
                     var secondaryOrganization = Generator.CreateOrganization();
                     secondaryOrganization.AddModules(Modules);
                     secondaryOrganization.AddPermissions(
                         PermissionsCommon.User.CanLogin.Path,
                         Permission);
                     var userGenerator = secondaryOrganization.CreateUser();
                     userGenerator.AddPermissions(
                         PermissionsCommon.User.CanLogin.Path,
                         Permission);
                     Credentials = userGenerator.CreateLogOn();
                     return CreateEntityToModify();
                 },
                 LockEntity,
                 CreateModel,
                response =>
                {
                    response.Errors.SingleOrDefault(x => x.ErrorCode == "TargetNotFound").ShouldNotBeNull();
                });
        }

        protected override sealed TEntity CreateEntityToModify()
        {
            return CreateEntityToModify(CurrentOrganizationGenerator);
        }

        protected abstract TEntity CreateEntityToModify(OrganizationGenerator generator);
    }
}