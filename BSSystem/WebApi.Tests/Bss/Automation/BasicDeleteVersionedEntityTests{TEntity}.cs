﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerController, "Delete{0}BasicTests")]
    public abstract class BasicDeleteVersionedEntityTests<TEntity>
        : BaseDeleteVersionedEntityTests<TEntity>
        where TEntity : VersionedEntity, new()
    {
        protected const int NonexistentEntityId = 13597;

        [TestMethod]
        public async Task UserWithoutPermissionCannotDeleteEntityAsync()
        {
            await ApiCallTestAsync(
                async () =>
                {
                    Entity = CreateEntityToModify();
                    await Task.Delay(0);
                },
                HttpStatusCode.Forbidden);
        }

        [TestMethod]
        public async Task UserCanDeleteEntityAsync()
        {
            await DeleteTestsAsync(
                CreateEntityToModify,
                LockEntity,
                async (databaseAccessor, result) =>
                {
                    var entity = await databaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
                    entity.ShouldNotBeNull();
                    entity.IsDeleted.ShouldBeTrue();
                });
        }

        [TestMethod]
        public virtual async Task UserCanDeleteNotExistingEntityAsync()
        {
            Func<TEntity> prepare = () => new TEntity { Id = NonexistentEntityId };

            await DeleteTestsAsync(
                prepare,
                () => { },
                (accessor, result) => Task.Delay(0));
        }

        [TestMethod]
        public async Task UserCannotDeleteNotLockedEntityAsync()
        {
            Func<Task> prepare = async () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    Entity = CreateEntityToModify();
                    await Task.Delay(0);
                };

            await DeleteValidationErrorTestAsync(
                prepare,
                async (result) =>
                {
                    result.Errors.SingleOrDefault(x => x.ErrorCode == "NoLockError").ShouldNotBeNull();

                    var entity = await DatabaseAccessor.Query<TEntity>()
                       .SingleOrDefaultAsync(x => x.IsEquivalent(Entity));
                    entity.ShouldNotBeNull();
                    entity.IsDeleted.ShouldBeFalse();
                });
        }

        [TestMethod]
        public async Task UnauthorizedUserCannotDeleteEntityAsync()
        {
            await TestInitialize;
            Entity = new TEntity { Id = NonexistentEntityId };
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
               Target,
                method: HttpMethod.Delete,
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } });
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task UserWithFakeCredentialsCannotDeleteEntityAsync()
        {
            await TestInitialize;
            Entity = new TEntity { Id = NonexistentEntityId };
            await Generator.FlushAsync();

            var result = await Client.CallServiceAsync<ValidationResult>(
               Target,
                method: HttpMethod.Delete,
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: new NetworkCredential("Organization\\NonExistLogin", "password"));
            result.Code.ShouldBeEqual(HttpStatusCode.Unauthorized);
        }
    }
}