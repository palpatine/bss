﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Automation
{
    [GenericTest(GenericTestMode.OnePerControllerAndMaster, "Post{0}DependsOn{1}BasicTests")]
    public abstract class BasicPostVersionedEntityDependsOnVersionedEntityTests<TEntity, TViewModel, TMasterEntity, TMasterEntityIndicator>
        : BasePostVersionedEntityTests<TEntity, TViewModel>
        where TEntity : VersionedEntity, new()
        where TMasterEntity : VersionedEntity, TMasterEntityIndicator, new()
        where TViewModel : IViewModel
        where TMasterEntityIndicator : IStorable
    {
        protected static string EntityName { get; } = StorableHelper.GetEntityName(typeof(TEntity));

        protected virtual string RelationName => StorableHelper.GetEntityName(typeof(TMasterEntity));

        [TestMethod]
        public async Task UserCannotCreateEntityIfMasterDoesNotExistAsync()
        {
            Session session = null;
            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    PrepareForCreate();
                    session = Generator.CreateSession(CurrentUserGenerator.Entity).Entity;
                    return Task.Delay(0);
                },
                () =>
                {
                    var model = CreateModel(new TMasterEntity { Id = 543321 });
                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        [TestMethod]
        public async Task UserCannotCreateEntityIfMasterIsDeletedAsync()
        {
            TMasterEntity master = null;
            Session session = null;
            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    PrepareForCreate();
                    master = CreateDependencyEntity();
                    session = Generator.CreateSession(CurrentUserGenerator.Entity).Entity;
                    return Task.Delay(0);
                },
                async () =>
                {
                    await DeleteMasterAsync(master, session);
                    var model = CreateModel(master);
                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        [TestMethod]
        public async Task UserCannotUpdateEntityIfMasterDoesNotExistAsync()
        {
            Session session = null;
            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    Entity = CreateEntityToModify(CreateDependencyEntity(), x => { });
                    session = Generator.CreateSession(CurrentUserGenerator.Entity).Entity;
                    return Task.Delay(0);
                },
                () =>
                {
                    var model = CreateModel(new TMasterEntity { Id = 543321 });
                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        [TestMethod]
        public async Task UserCannotUpdateEntityIfMasterIsDeletedAsync()
        {
            TMasterEntity master = null;
            Session session = null;
            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity();
                    Entity = CreateEntityToModify(CreateDependencyEntity(), x => { });
                    session = Generator.CreateSession(CurrentUserGenerator.Entity).Entity;
                    return Task.Delay(0);
                },
                async () =>
                {
                    await DeleteMasterAsync(master, session);
                    var model = CreateModel(master);
                    return model;
                },
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        protected abstract TMasterEntity CreateDependencyEntity();

        protected abstract TEntity CreateEntityToModify(
            TMasterEntity master,
            Action<TEntity> configure);

        protected abstract TViewModel CreateModel(
            TMasterEntity master);

        protected virtual void PrepareForCreate()
        {
        }

        private async Task DeleteMasterAsync(
            TMasterEntity entity,
            Session session)
        {
            var sessionDataProvider = (SessionDataProvider)ServiceLocator.Current.GetInstance<ISessionDataProvider>();
            var lockRepository = ServiceLocator.Current.GetInstance<ILockRepository>();
            var entityRepository =
                ServiceLocator.Current.GetInstance<IVersionedEntityRepository<TMasterEntity, TMasterEntityIndicator>>();
            sessionDataProvider.Scope = CurrentOrganizationGenerator.Entity;
            sessionDataProvider.CurrentUser = CurrentUserGenerator.Entity;
            sessionDataProvider.Session = session;
            var lockToken = Guid.NewGuid().ToString("N");
            var lockResult = await lockRepository.TryLockAsync(entity, lockToken);
            if (lockResult.Any())
            {
                throw new InvalidOperationException(string.Join(", ", lockResult.Select(x => x.ValidationMessage)));
            }

            await entityRepository.DeleteAsync(entity);
            await lockRepository.RemoveLockAsync(lockToken);
        }
    }
}