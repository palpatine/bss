﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Permissions
{
    [TestClass]
    public sealed class FindAccessibleEntitiesTests
        : BaseApiTests
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = InitializeAsync();
        }

        [TestMethod]
        public async Task ContextPermissionGivesResultWithRelevantEntitiesIdsArrayAsync()
        {
            await TestInitialize;

            var generator = ServiceLocator.Current.GetInstance<Generator>();
            var organization = generator.CreateOrganization();
            organization.AddModule(ModulesProvider.Common);
            organization.AddModule(ModulesProvider.Wtt);
            organization.AddPermissions(
                PermissionsCommon.User.CanLogin.Path,
                PermissionsWtt.Topic.Details.Path);
            UserGenerator userGenerator;
            var credentials = organization.CreateUserWithLogOnAndGlobalPermissions(
                out userGenerator,
                PermissionsCommon.User.CanLogin.Path);
            var roleGenerator = organization.CreateRoleDefinition<Topic>()
                .AddPermission(PermissionsWtt.Topic.Details.Path);

            var topicGenerator = organization
                .CreateTopicKind()
                .CreateTopic();

            topicGenerator.AddUser(userGenerator.Entity, roleGenerator.Entity);
            await generator.FlushAsync();

            var target = AddressProvider
                  .Get((PermissionController c) => c.FindAccessibleEntities(null, null));
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result = await client.CallServiceAsync<UserPermissionsVerificationRequestViewModel, IEnumerable<UserPermissionsVerificationResponseViewModel>>(
                target,
                new UserPermissionsVerificationRequestViewModel { PermissionPaths = new[] { PermissionsWtt.Topic.Details.Path } },
                credentials: credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.Count().ShouldBeEqual(1);
            result.Response.Single().Entities.Count().ShouldBeEqual(1);
            result.Response.Single().Entities.Single().ShouldBeEqual(topicGenerator.Entity.Id);
            result.Response.Single().PermissionPath.ShouldBeEqual(PermissionsWtt.Topic.Details.Path);
        }

        [TestMethod]
        public async Task GloballyAssignedPermissionGivesResultWithEmptyEntitiesArrayAsync()
        {
            await TestInitialize;

            var generator = ServiceLocator.Current.GetInstance<Generator>();
            var organization = generator.CreateOrganization();
            organization.AddModule(ModulesProvider.Common);
            organization.AddPermissions(
                PermissionsCommon.User.CanLogin.Path);
            var credentials = organization.CreateUserWithLogOnAndGlobalPermissions(PermissionsCommon.User.CanLogin.Path);

            await generator.FlushAsync();

            var target = AddressProvider
                  .Get((PermissionController c) => c.FindAccessibleEntities(null, null));
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result = await client.CallServiceAsync<UserPermissionsVerificationRequestViewModel, IEnumerable<UserPermissionsVerificationResponseViewModel>>(
                target,
                new UserPermissionsVerificationRequestViewModel { PermissionPaths = new[] { PermissionsCommon.User.CanLogin.Path } },
                credentials: credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.Count().ShouldBeEqual(1);
            result.Response.Single().Entities.Count().ShouldBeEqual(0);
            result.Response.Single().PermissionPath.ShouldBeEqual(PermissionsCommon.User.CanLogin.Path);
        }

        [TestMethod]
        public async Task NotAssignedPermissionProducesNoResultAsync()
        {
            await TestInitialize;

            var generator = ServiceLocator.Current.GetInstance<Generator>();
            var organization = generator.CreateOrganization();
            organization.AddModule(ModulesProvider.Common);
            organization.AddPermissions(
                  PermissionsCommon.User.CanLogin.Path);
            var credentials = organization.CreateUserWithLogOnAndGlobalPermissions(PermissionsCommon.User.CanLogin.Path);

            await generator.FlushAsync();

            var target = AddressProvider
                  .Get((PermissionController c) => c.FindAccessibleEntities(null, null));
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result = await client.CallServiceAsync<UserPermissionsVerificationRequestViewModel, IEnumerable<UserPermissionsVerificationResponseViewModel>>(
                target,
                new UserPermissionsVerificationRequestViewModel { PermissionPaths = new[] { PermissionsCommon.User.Details.Path } },
                credentials: credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.Count().ShouldBeEqual(0);
        }

        [TestMethod]
        public async Task NotExistingPermissionIsIndistinguishableFromUnassignedPermissionResultAsync()
        {
            await TestInitialize;

            var generator = ServiceLocator.Current.GetInstance<Generator>();
            var organization = generator.CreateOrganization();
            organization.AddModule(ModulesProvider.Common);
            organization.AddPermissions(
                    PermissionsCommon.User.CanLogin.Path);
            var credentials = organization.CreateUserWithLogOnAndGlobalPermissions(PermissionsCommon.User.CanLogin.Path);

            await generator.FlushAsync();

            var target = AddressProvider
                  .Get((PermissionController c) => c.FindAccessibleEntities(null, null));
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result = await client.CallServiceAsync<UserPermissionsVerificationRequestViewModel, IEnumerable<UserPermissionsVerificationResponseViewModel>>(
                target,
                new UserPermissionsVerificationRequestViewModel { PermissionPaths = new[] { "AAAAAA" } },
                credentials: credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.Count().ShouldBeEqual(0);
        }

        private static async Task InitializeAsync()
        {
            await AssemblyInitialize.Initialize;
        }
    }
}