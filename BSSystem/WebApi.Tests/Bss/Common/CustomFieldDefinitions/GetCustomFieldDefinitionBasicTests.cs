﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.CustomFieldDefinitions
{
    [TestClass]
    public class GetCustomFieldDefinitionBasicTests : BasicGetScopedEntityTests<CustomFieldDefinition, CustomFieldDefinitionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
        };

        protected override string Permission => PermissionsCommon.CustomFieldDefinition.Details.Path;

        protected override Uri Target => AddressProvider.Get((CustomFieldDefinitionController c) => c.Get(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override CustomFieldDefinition CreateEntity()
        {
            var entity = CurrentOrganizationGenerator.CreateCustomFieldDefinition<Section>().Entity;
            return entity;
        }

        protected override void VerifySuccessResult(
            CustomFieldDefinitionViewModel actual,
            CustomFieldDefinition expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}