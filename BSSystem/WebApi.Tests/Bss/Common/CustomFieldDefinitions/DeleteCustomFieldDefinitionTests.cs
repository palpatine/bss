﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.CustomFieldDefinitions
{
    [TestClass]
    public class DeleteCustomFieldDefinitionTests
        : BaseDeleteVersionedEntityTests<CustomFieldDefinition>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.CustomFieldDefinition.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                  .Get((CustomFieldDefinitionController c) => c.Delete(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        [TestMethod]
        public async Task UserCanDeleteCustomFieldDefinitionWithCustomFieldLastValueAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        protected override CustomFieldDefinition CreateEntityToModify()
        {
            return CurrentOrganizationGenerator.CreateCustomFieldDefinition<User>().Entity;
        }
    }
}