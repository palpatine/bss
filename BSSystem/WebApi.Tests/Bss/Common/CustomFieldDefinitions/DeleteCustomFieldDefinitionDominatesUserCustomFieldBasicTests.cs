﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.CustomFieldDefinitions
{
    [TestClass]
    public class DeleteCustomFieldDefinitionDominatesUserCustomFieldBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<CustomFieldDefinition, UserCustomField>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.CustomFieldDefinition.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((CustomFieldDefinitionController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override CustomFieldDefinition CreateEntityToModify()
        {
            return CurrentOrganizationGenerator.CreateCustomFieldDefinition<User>().Entity;
        }

        protected override UserCustomField CreateSlaveEntity()
        {
            return CurrentOrganizationGenerator.Find<CustomFieldDefinitionGenerator>(Entity).AddValue<UserCustomField>(CurrentUserGenerator.Entity, "value");
        }
    }
}
