﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.CustomFieldDefinitions
{
    [TestClass]
    public sealed class CustomFieldDefinitionLockBasicTests
        : BasicScopedLockTests<CustomFieldDefinition, ICustomFieldDefinition>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.CustomFieldDefinition.Details.Write.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override CustomFieldDefinition CreateEntityToLock()
        {
            var target = CurrentOrganizationGenerator.CreateCustomFieldDefinition<Section>().Entity;
            return target;
        }

        protected override CustomFieldDefinition CreateEntityToLockInOtherOrganization()
        {
            var otherOrganization = Generator.CreateOrganization();
            var target = otherOrganization.CreateCustomFieldDefinition<Section>().Entity;
            return target;
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((CustomFieldDefinitionController c) => c.Lock(id));
        }
    }
}