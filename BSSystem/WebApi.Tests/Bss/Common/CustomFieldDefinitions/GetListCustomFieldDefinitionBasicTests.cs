using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.CustomFieldDefinitions
{
    [TestClass]
    public class GetListCustomFieldDefinitionBasicTests
        : BasicGetListScopedEntityTests<CustomFieldDefinition, CustomFieldDefinitionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.CustomFieldDefinition.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((CustomFieldDefinitionController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<CustomFieldDefinition> CreateEntities()
        {
            var entities = new List<CustomFieldDefinition>();
            var first = CurrentOrganizationGenerator.CreateCustomFieldDefinition<User>(
                 (u) =>
                 {
                     u.DateEnd = DateTime.Today.AddDays(3);
                 });
            entities.Add(first.Entity);

            var second = CurrentOrganizationGenerator.CreateCustomFieldDefinition<Section>();
            entities.Add(second.Entity);
            return entities;
        }

        protected override void ShouldMatch(CustomFieldDefinitionViewModel actual, CustomFieldDefinition expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}