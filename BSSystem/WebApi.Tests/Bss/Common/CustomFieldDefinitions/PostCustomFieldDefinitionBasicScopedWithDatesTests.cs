using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.CustomFieldDefinitions
{
    [TestClass]
    public sealed class PostCustomFieldDefinitionBasicScopedWithDatesTests
        : BasicPostScopedWithDatesEntityTests<CustomFieldDefinition, CustomFieldDefinitionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.CustomFieldDefinition.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((CustomFieldDefinitionController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override CustomFieldDefinition CreateEntityToModify(
            OrganizationGenerator generator,
            Action<IDatedEntity> configureMaster,
            Action<IDatedEntity> configureSlave,
            DateTime otherDate)
        {
            var targetGenerator = generator.CreateCustomFieldDefinition<User>(
                u =>
                {
                    configureSlave(u);
                    u.DisplayName = "DisplayName";
                    u.Comment = "Comment";
                    u.IsEditable = true;
                    u.IsInViews = true;
                    u.IsNullable = true;
                    u.IsRequired = true;
                    u.IsUnique = true;
                });

            return targetGenerator.Entity;
        }

        protected override CustomFieldDefinitionViewModel CreateModel(
            DateTime dateStart,
            DateTime? dateEnd,
            DateTime otherDate)
        {
            var model = new CustomFieldDefinitionViewModel
            {
                Id = Entity?.Id ?? 0,
                TableKey = Entity?.TableKey ?? StorableHelper.GetTableKey(typeof(Section)),
                DateEnd = dateEnd,
                DateStart = dateStart,
                DisplayName = "DN",
                Comment = "C",
                IsEditable = false,
                IsInViews = false,
                IsNullable = false,
                IsRequired = false,
                IsUnique = false
            };
            return model;
        }

        protected override void Verify(
            OrganizationGenerator generator,
            CustomFieldDefinition actual,
            CustomFieldDefinitionViewModel expected)
        {
            actual.ShouldBeEqual(expected, generator);
        }
    }
}