﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class JunctionLockUserSectionsBasicTests
        : BasicScopedJunctionLockTests<User, IUser, Section>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Section.Details.Assignment.Common_User.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateRelatedEntity()
        {
            return CurrentOrganizationGenerator.CreateSection().Entity;
        }

        protected override IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<User> generator,
            Section relatedEntity)
        {
            var masterEntityGenerator = (UserGenerator)generator;
           return masterEntityGenerator.AddSection(relatedEntity, CurrentOrganizationGenerator.CreateRoleDefinition<Section>().Entity);
        }

        protected override IGenerator<User> MasterEntityGenerator(OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreateUser();
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((SectionController c) => c.JunctionLock(id, RelatedTableKey));
        }
    }
}