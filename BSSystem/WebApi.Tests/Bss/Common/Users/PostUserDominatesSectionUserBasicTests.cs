﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public class PostUserDominatesSectionUserBasicTests
        : BasicPostEntityDominatesSlaveDependencyTests<User, UserViewModel, SectionUser>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common
        };

        protected override string Permission => PermissionsCommon.User.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((UserController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateUser(masterDateRange).Entity;
        }

        protected override SectionUser CreateSlaveEntity(
            User master,
            Action<IDatedEntity> masterDateRange,
            Action<IDatedEntity> slaveDateRange)
        {
            SectionUser slaveEntity = null;
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Section>(masterDateRange).Entity;
            CurrentOrganizationGenerator
                .CreateSection(masterDateRange)
                .AddUser(master, role, x => slaveEntity = x);
            slaveDateRange(slaveEntity);
            return slaveEntity;
        }

        protected override UserViewModel GetViewModel(
            DateTime dateStart,
            DateTime? dateEnd)
        {
            var model = new UserViewModel
            {
                DateEnd = dateEnd,
                DateStart = dateStart,
                DisplayName = Entity.DisplayName,
                FirstName = Entity.FirstName,
                Id = Entity.Id,
                Surname = Entity.Surname
            };
            return model;
        }
    }
}