﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public class DeleteUserTests
        : BaseDeleteVersionedEntityTests<User>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.User.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((UserController c) => c.Delete(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        [TestMethod]
        public async Task UserCanDeleteUserWithWithAliasAndAliasBecomesIndependentUserAsync()
        {
            UserGenerator generator = null;
            User otherUser = null;

            await DeleteTestsAsync(
                () =>
                {
                    generator = CurrentOrganizationGenerator.CreateUser();
                    var otherOrganization = Generator.CreateOrganization();
                    otherUser = otherOrganization.CreateUser(x => x.Parent = generator.Entity).Entity;
                    return generator.Entity;
                },
                () => Generator
                    .CreateSession(CurrentUserGenerator.Entity)
                    .AddLock(Entity, OperationToken),
                async (databaseAccessor, result) =>
                {
                    var entity = await databaseAccessor.Query<User>()
                        .SingleOrDefaultAsync(x => x.Id == Entity.Id);
                    entity.ShouldNotBeNull();
                    entity.IsDeleted.ShouldBeTrue();

                    var otherEntity = await databaseAccessor.Query<User>()
                        .SingleOrDefaultAsync(x => x.Id == otherUser.Id);
                    otherEntity.ShouldNotBeNull();
                    otherEntity.IsDeleted.ShouldBeFalse();
                    otherEntity.IsAlias.ShouldBeFalse();
                });
        }

        [TestMethod]
        public async Task UserCanDeleteNewlyCreatedUserWithLogOnAsync()
        {
            UserGenerator generator = null;

            await DeleteTestsAsync(
                () =>
                {
                    generator = CurrentOrganizationGenerator.CreateUser();
                    generator.CreateLogOn();

                    return generator.Entity;
                },
                () => Generator
                    .CreateSession(CurrentUserGenerator.Entity)
                    .AddLock(Entity, OperationToken)
                    .AddLock(generator.LogOn, OperationToken),
                async (databaseAccessor, result) =>
                {
                    var entity = await databaseAccessor.Query<User>()
                        .SingleOrDefaultAsync(x => x.Id == Entity.Id);
                    entity.ShouldNotBeNull();
                    entity.IsDeleted.ShouldBeTrue();

                    var login = await databaseAccessor.Query<Login>()
                        .SingleOrDefaultAsync(x => x.Id == generator.LogOn.Id);
                    login.ShouldNotBeNull();
                    login.IsDeleted.ShouldBeTrue();
                });
        }

        [TestMethod]
        public async Task UserCanDeleteUserWithAddressAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        [TestMethod]
        public async Task UserCanDeleteUserWithCustomFieldAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        [TestMethod]
        public async Task UserCanDeleteUserWithJunctionLockAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        [TestMethod]
        public async Task UserCanDeleteUserWithLockAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        [SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login")]
        [TestMethod]
        public async Task UserCanDeleteUserWithLoginAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        [TestMethod]
        public async Task UserCanDeleteUserAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        [TestMethod]
        public async Task UserCanDeleteUserWithSessionAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        [TestMethod]
        public async Task UserCanDeleteUserWithSettingAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        protected override User CreateEntityToModify()
        {
            return CurrentOrganizationGenerator.CreateUser().Entity;
        }
    }
}