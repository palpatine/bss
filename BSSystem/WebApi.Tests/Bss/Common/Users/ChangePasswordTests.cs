﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.Web;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class ChangePasswordTests
        : BaseApiTests
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        [TestMethod]
        public async Task UserCanChangeOwnPasswordAsync()
        {
            await TestInitialize;
            await FinalizeInitializationAsync();

            var model = new ChangePasswordViewModel
            {
                NewPassword = "PAsswor2",
                NewPasswordRepeated = "PAsswor2",
                Password = CurrentUserGenerator.Password
            };
            CurrentUserGenerator.Password = "PAsswor2";

            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var target = AddressProvider.Get((UserController x) => x.ChangePassword(null));
            var result = await client.CallServiceAsync<ChangePasswordViewModel, ValidationResult>(
                target,
                model,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            var credentials = CurrentUserGenerator.CreateCredentials();
            var authenticationResult = await client.CallServiceAsync<AuthenticationViewModel, ValidationResult>(
                AddressProvider.Get((LogOnController c) => c.Post(null)),
                new AuthenticationViewModel { Login = credentials.UserName, Password = credentials.Password },
                credentials: Credentials);

            authenticationResult.Code.ShouldBeEqual(HttpStatusCode.OK);
            authenticationResult.Response.IsValid.ShouldBeTrue();
        }

        [TestMethod]
        public async Task UserMustProvideCorrectCurrentPasswordInOrderToChangeOwnPasswordAsync()
        {
            await TestInitialize;
            await FinalizeInitializationAsync();

            var model = new ChangePasswordViewModel
            {
                NewPassword = "PAsswor2",
                NewPasswordRepeated = "PAsswor2",
                Password = "IncorrectPassword"
            };

            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var target = AddressProvider.Get((UserController x) => x.ChangePassword(null));
            var result = await client.CallServiceAsync<ChangePasswordViewModel, ValidationResult>(
                target,
                model,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.Single().Reference.ShouldBeEqual("Password");
        }
    }
}