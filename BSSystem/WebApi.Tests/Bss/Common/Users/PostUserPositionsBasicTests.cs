using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class PostUserPositionsBasicTests
        : BasicPostScopedAssignmentJunctionRelationTests<User, Position, PositionUser, AssignmentPersistenceViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.User.Details.Assignment.Common_Position.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override Position CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreatePosition(configure).Entity;
        }

        protected override PositionUser RelateEntities(
            User master,
            Position related,
            Action<IDatedEntity> configure)
        {
            var relation = Generator.Find<UserGenerator>(master).AddPosition(related);
            configure(relation);
            return relation;
        }

        protected override void Verify(PositionUser actual, AssignmentPersistenceViewModel expected)
        {
            actual.ShouldBeEqual(expected, x => x.First, CurrentOrganizationGenerator);
        }
    }
}