﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public class DeleteUserDominatesEntryBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<User, Entry>
    {
        private TopicUser _topicUser;
        protected UserGenerator EntityGenerator => CurrentOrganizationGenerator.CreateUser();

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.User.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((UserController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateEntityToModify()
        {
            return EntityGenerator.Entity;
        }

        protected override Entry CreateSlaveEntity()
        {
            var topicKindGenerator = CurrentOrganizationGenerator.CreateTopicKind();
            var topicGenerator = topicKindGenerator.CreateTopic();
            var roleGenerator = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>();
            _topicUser = topicGenerator.AddUser(Entity, roleGenerator.Entity);
            var activityKindGenerator = CurrentOrganizationGenerator.CreateActivityKind();
            var activityGenerator = activityKindGenerator.CreateActivity();
            topicGenerator.AddActivity(activityGenerator.Entity);

            var entry = activityGenerator.AddEntry(topicGenerator.Entity, Entity);
            return entry;
        }

        protected override async Task DeleteSlaveEntity(Entry slave)
        {
            await DeleteEntityAsync(_topicUser);
            await DeleteEntityAsync(slave);
        }
    }
}