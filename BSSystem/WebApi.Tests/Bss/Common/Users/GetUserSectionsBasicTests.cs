﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class GetUserSectionsBasicTests
        : BasicScopedAssignmentJunctionRelationTests<User, Section, SectionUser, JobRoleAssignmentViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.User.Details.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override Section CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateSection(configure).Entity;
        }

        protected override void RelateEntities(
            User master,
            Section related,
            Action<IDatedEntity> configure)
        {
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Section>();
            Generator.Find<UserGenerator>(master).AddSection(related, role.Entity, configure);
        }

        protected override void Verify(JobRoleAssignmentViewModel actual, SectionUser expected)
        {
            actual.ShouldBeEqual(expected, x => x.First, CurrentOrganizationGenerator);
        }
    }
}