using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class PostUserTests
        : BasePostVersionedEntityTests<User, UserViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.User.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((UserController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        [TestMethod]
        public async Task UserCanCreateLogOnForExistingUserAsync()
        {
            await UpdateTestsAsync(
                () => CurrentOrganizationGenerator.CreateUser().Entity,
                () => Generator.CreateSession(CurrentUserGenerator.Entity).AddLock(Entity, "token"),
                () => new UserViewModel
                {
                    Id = Entity.Id,
                    DateStart = DateTime.Today,
                    FirstName = "FN",
                    Surname = "Sn",
                    DisplayName = "DN" + NextRandomValue(),
                    LoginName = "LN" + NextRandomValue(),
                    Mail = "mail@mail.com"
                },
                async (databaseAccessor, result) =>
                {
                    var logOn = await databaseAccessor.Query<Login>()
                        .SingleOrDefaultAsync(x => !x.IsDeleted && x.User.IsEquivalent(Entity));
                    logOn.ShouldNotBeNull();

                    logOn.LoginName.ShouldBeEqual(Model.LoginName);
                });
        }

        [TestMethod]
        public async Task UserCanCreateUserWithLogOnAsync()
        {
            await CreateTestsAsync(
                () => { },
                () => new UserViewModel
                {
                    DateStart = DateTime.Today,
                    FirstName = "FN",
                    Surname = "Sn",
                    DisplayName = "DN" + NextRandomValue(),
                    LoginName = "LN" + NextRandomValue(),
                    Mail = "mail@mail.com"
                },
                async (databaseAccessor, result) =>
                {
                    var entityId = result.Id;
                    var entity = await databaseAccessor.Query<User>()
                        .SingleOrDefaultAsync(x => !x.IsDeleted && x.Id == entityId);
                    entity.ShouldNotBeNull();
                    var logOn = await databaseAccessor.Query<Login>()
                        .SingleOrDefaultAsync(x => !x.IsDeleted && x.User.IsEquivalent(entity));
                    logOn.ShouldNotBeNull();

                    logOn.LoginName.ShouldBeEqual(Model.LoginName);
                });
        }

        [TestMethod]
        public async Task UserCanDeleteLogOnForExistingUserAsync()
        {
            Login logOn = null;
            await UpdateTestsAsync(
                () =>
                {
                    var generator = CurrentOrganizationGenerator
                         .CreateUser()
                         .AddLogOn("LN" + NextRandomValue(), "password");
                    logOn = generator.LogOn;
                    return generator.Entity;
                },
                () => Generator.CreateSession(CurrentUserGenerator.Entity)
                   .AddLock(Entity, "token")
                   .AddLock(logOn, "token"),
                () => new UserViewModel
                {
                    Id = Entity.Id,
                    DateStart = DateTime.Today,
                    FirstName = "FN",
                    Surname = "Sn",
                    DisplayName = "DN" + NextRandomValue(),
                    LoginName = null,
                },
                async (databaseAccessor, result) =>
                {
                    var currentLogOn = await databaseAccessor.Query<Login>()
                        .SingleOrDefaultAsync(x => !x.IsDeleted && x.User.IsEquivalent(Entity));
                    currentLogOn.ShouldBeNull();
                });
        }
    }
}