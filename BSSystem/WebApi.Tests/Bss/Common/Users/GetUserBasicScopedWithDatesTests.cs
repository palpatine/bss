﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public class GetUserBasicScopedWithDatesTests
        : BasicGetScopedWithDatesTests<User, UserViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.User.Details.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider.Get((UserController x) => x.Get(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateEntity(Action<IDatedEntity> configure, DateTime otherDate)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override void Verify(UserViewModel actual, User expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}