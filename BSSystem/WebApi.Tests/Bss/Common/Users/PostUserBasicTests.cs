using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class PostUserBasicTests
        : BasicPostScopedVersionedEntityTests<User, UserViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.User.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((UserController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateEntityToModify(OrganizationGenerator generator)
        {
            var targetGenerator = generator.CreateUser(
                u =>
                {
                    u.DateEnd = DateTime.Today;
                    u.DateStart = DateTime.Today.AddDays(-3);
                    u.DisplayName = "DisplayName";
                    u.FirstName = "FirstName";
                    u.Surname = "Surname";
                });

            return targetGenerator.Entity;
        }

        protected override UserViewModel CreateModel()
        {
            var model = new UserViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = DateTime.Today.AddDays(4),
                DateStart = DateTime.Today.AddDays(-3),
                DisplayName = "DN",
                FirstName = "FN",
                Surname = "SN"
            };
            return model;
        }

        protected override void VerifyPersistence(User entity, OrganizationGenerator organizationGenerator)
        {
            entity.ShouldBeEqual(Model, organizationGenerator);
        }
    }
}