﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public class PostUserDominatesEntryLimitExclusionBasicTests
        : BasicPostEntityDominatesSlaveDependencyTests<User, UserViewModel, EntryLimitExclusion>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common, ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsCommon.User.Details.Write.Path;

        protected override Uri Target =>AddressProvider.Get((UserController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateUser(masterDateRange).Entity;
        }

        protected override EntryLimitExclusion CreateSlaveEntity(
            User master,
            Action<IDatedEntity> masterDateRange,
            Action<IDatedEntity> slaveDateRange)
        {
            var slave = CurrentOrganizationGenerator.AddEntryLimitExclusion(master);
            slaveDateRange(slave);
            return slave;
        }

        protected override UserViewModel GetViewModel(
            DateTime dateStart,
            DateTime? dateEnd)
        {
            var model = new UserViewModel
            {
                DateEnd = dateEnd,
                DateStart = dateStart,
                DisplayName = Entity.DisplayName,
                FirstName = Entity.FirstName,
                Id = Entity.Id,
                Surname = Entity.Surname
            };
            return model;
        }
    }
}