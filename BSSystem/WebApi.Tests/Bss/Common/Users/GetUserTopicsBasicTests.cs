﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class GetUserTopicsBasicTests
        : BasicScopedAssignmentJunctionRelationTests<User, Topic, TopicUser, RoleAssignmentViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsCommon.User.Details.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override Topic CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure).Entity;
        }

        protected override void RelateEntities(
            User master,
            Topic related,
            Action<IDatedEntity> configure)
        {
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>();
            Generator.Find<UserGenerator>(master).AddTopic(related, role.Entity, configure);
        }

        protected override void Verify(RoleAssignmentViewModel actual, TopicUser expected)
        {
            actual.ShouldBeEqual(expected, x => x.First, CurrentOrganizationGenerator);
        }
    }
}