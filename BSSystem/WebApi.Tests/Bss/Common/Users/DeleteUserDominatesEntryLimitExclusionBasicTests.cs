﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public class DeleteUserDominatesEntryLimitExclusionBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<User, EntryLimitExclusion>
    {
        protected UserGenerator EntityGenerator => CurrentOrganizationGenerator.CreateUser();

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.User.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((UserController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateEntityToModify()
        {
            return EntityGenerator.Entity;
        }

        protected override EntryLimitExclusion CreateSlaveEntity()
        {
            var entryLimitExclusion = CurrentOrganizationGenerator.AddEntryLimitExclusion(Entity);
            return entryLimitExclusion;
        }
    }
}