﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public class PostUserDominatesUserBasicTests
        : BasicPostEntityDominatesSlaveDependencyTests<User, UserViewModel, User>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common };

        protected override string Permission { get; } = PermissionsCommon.User.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((UserController a) => a.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateUser(masterDateRange).Entity;
        }

        protected override User CreateSlaveEntity(User master, Action<IDatedEntity> masterDateRange, Action<IDatedEntity> slaveDateRange)
        {
            var slave = CurrentOrganizationGenerator
                .CreateUser(
                x =>
                {
                    x.Parent = master;
                    x.Root = master;
                }).Entity;
            slaveDateRange(slave);
            return slave;
        }

        protected override UserViewModel GetViewModel(DateTime dateStart, DateTime? dateEnd)
        {
            return new UserViewModel
            {
                Id = Entity.Id,
                DateStart = dateStart,
                DateEnd = dateEnd,
                DisplayName = NextRandomValue(),
                FirstName = NextRandomValue(),
                Surname = NextRandomValue()
            };
        }
    }
}