using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class PostUserTopicsBasicTests
        : BasicPostScopedAssignmentJunctionRelationTests<User, Topic, TopicUser, RoleAssignmentPersistenceViewModel>
    {
        private RoleDefinition _role;
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsCommon.User.Details.Assignment.Wtt_Topic.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override RoleAssignmentPersistenceViewModel CreateModel(TopicUser relation)
        {
            var model = base.CreateModel(relation);
            model.RoleId = relation.RoleDefinition.Id;
            return model;
        }

        protected override RoleAssignmentPersistenceViewModel CreateModel()
        {
            var model = base.CreateModel();
            model.RoleId = _role.Id;
            return model;
        }

        protected override Topic CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure).Entity;
        }

        protected override void PrepareForCreate()
        {
            _role = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity;
        }

        protected override TopicUser RelateEntities(
            User master,
            Topic related,
            Action<IDatedEntity> configure)
        {
            var relation = Generator.Find<UserGenerator>(master).AddTopic(related, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity);
            configure(relation);
            return relation;
        }

        protected override void Verify(TopicUser actual, RoleAssignmentPersistenceViewModel expected)
        {
            actual.ShouldBeEqual(expected, x => x.First, CurrentOrganizationGenerator);
        }
    }
}