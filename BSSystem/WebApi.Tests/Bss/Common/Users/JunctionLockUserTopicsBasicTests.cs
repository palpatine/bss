﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class JunctionLockUserTopicsBasicTests : BasicScopedJunctionLockTests<User, IUser, Topic>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
         {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsCommon.User.Details.Assignment.Wtt_Topic.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateRelatedEntity()
        {
            return CurrentOrganizationGenerator.CreateTopicKind().CreateTopic().Entity;
        }

        protected override IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<User> generator,
            Topic relatedEntity)
        {
            var masterEntityGenerator = (UserGenerator)generator;
           return masterEntityGenerator.AddTopic(relatedEntity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
        }

        protected override IGenerator<User> MasterEntityGenerator(OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreateUser();
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((TopicController c) => c.JunctionLock(id, RelatedTableKey));
        }
    }
}