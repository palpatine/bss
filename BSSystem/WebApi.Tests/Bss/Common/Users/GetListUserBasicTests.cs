using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public class GetListUserBasicTests
        : BasicGetListScopedEntityTests<User, UserViewModel>
    {
        protected override HttpStatusCode CrossDomainListStatusCode => HttpStatusCode.OK;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.User.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((UserController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<User> CreateEntities()
        {
            var newUser = CurrentOrganizationGenerator.CreateUser(
                u =>
                {
                    u.FirstName = "All";
                    u.Surname = "Bundy";
                    u.Mail = "email";
                    u.DateEnd = DateTime.Today.AddDays(3);
                    u.Phone = "13412";
                });

            return new[] { CurrentUserGenerator.Entity, newUser.Entity };
        }

        protected override void ShouldMatch(UserViewModel actual, User expected)
        {
            actual.FirstName.ShouldBeEqual(expected.FirstName);
            actual.Surname.ShouldBeEqual(expected.Surname);
            actual.Mail.ShouldBeEqual(expected.Mail);
            actual.Phone.ShouldBeEqual(expected.Phone);
            actual.DateStart.ShouldBeEqual(expected.DateStart);
            actual.DateEnd.ShouldBeEqual(expected.DateEnd);
        }

        protected override void ValidateResponse(IEnumerable<UserViewModel> actual, IEnumerable<User> expected)
        {
            base.ValidateResponse(actual.Where(x => !x.IsAlias), expected);
        }

        protected override void VerifyCrossOrganizationAccessTest(IEnumerable<UserViewModel> result)
        {
            result.Count(x => !x.IsAlias).ShouldBeEqual(1);
        }
    }
}