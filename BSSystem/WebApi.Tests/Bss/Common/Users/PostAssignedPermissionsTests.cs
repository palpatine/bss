using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class PostAssignedPermissionsTests
        : BaseApiTests
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = InitializeAsync();
        }

        [TestMethod]
        public async Task UserCanChangeUserPermissionsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.User.Read.Path,
                    PermissionsCommon.User.Details.Path,
                    PermissionsCommon.Section.Details.Path,
                    PermissionsCommon.User.Details.Assignment.Common_Permission.Path);
            CurrentUserGenerator.AddPermissions(
                PermissionsCommon.User.Details.Assignment.Common_Permission.Path);
            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.User.Details.Path
            };

            var targetGenerator = CurrentOrganizationGenerator.CreateUser()
                .AddPermissions(targetPermissions);

            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((PermissionUser x) => x.User, targetGenerator.Entity, "token");

            await FinalizeInitializationAsync();

            var currentPermissions = await DatabaseAccessor.Query<Permission>()
                .Join(DatabaseAccessor.Query<PermissionUser>().Where(x => !x.IsDeleted && x.User.IsEquivalent(targetGenerator.Entity)), x => x, x => x.Permission, (x, y) => new { Permission = x, PermissionUser = y })
                 .Join(DatabaseAccessor.Query<SystemPermission>(), x => x.Permission.SystemPermission, (x, y) => new { x.Permission.Id, y.Path, JunctionId = x.PermissionUser.Id })
                 .ToListAsync();
            var permissionToAdd = await DatabaseAccessor.Query<SystemPermission>()
                .Where(x => x.Path == PermissionsCommon.Section.Details.Path)
                .Join(DatabaseAccessor.Query<Permission>().Where(x => x.Scope.IsEquivalent(CurrentOrganizationGenerator.Entity)), x => x, x => x.SystemPermission, (x, y) => y.Id)
                .SingleAsync();
            var permissionToNotRemove = currentPermissions.Single(x => x.Path == PermissionsCommon.User.Read.Path);

            var target = AddressProvider
                .Get((UserController c) => c.PostAssignedPermissions(null, targetGenerator.Entity.Id, null));

            var permissionsToSave = new[]
            {
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToNotRemove.Id,
                    JunctionId = permissionToNotRemove.JunctionId
                },
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToAdd,
                    JunctionId = null
                }
            };

            var result = await Client.CallServiceAsync<IEnumerable<PermissionAssignmentPersistenceViewModel>, ValidationResult>(
                target,
                permissionsToSave,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue();

            var permissionsAfterCall = await DatabaseAccessor.Query<Permission>()
                .Where(x => !x.IsDeleted)
                .Join(DatabaseAccessor.Query<PermissionUser>().Where(x => !x.IsDeleted && x.User.IsEquivalent(targetGenerator.Entity)), x => x, x => x.Permission, (x, y) => x)
                .Join(DatabaseAccessor.Query<SystemPermission>(), x => x.SystemPermission, (x, y) => y.Path)
                .ToListAsync();

            CollectionAssert.AreEquivalent(
                new[]
                {
                    PermissionsCommon.Section.Details.Path,
                    PermissionsCommon.User.Read.Path
                },
                permissionsAfterCall);
        }

        [TestMethod]
        public async Task UserMustHaveLockInOrderToChangeUserPermissionsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.User.Read.Path,
                    PermissionsCommon.User.Details.Path,
                    PermissionsCommon.Section.Details.Path,
                    PermissionsCommon.User.Details.Assignment.Common_Permission.Path);
            CurrentUserGenerator.AddPermissions(
                PermissionsCommon.User.Details.Assignment.Common_Permission.Path);
            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.User.Details.Path
            };

            var targetGenerator = CurrentOrganizationGenerator.CreateUser()
                .AddPermissions(targetPermissions);

            await FinalizeInitializationAsync();

            var currentPermissions = await DatabaseAccessor.Query<Permission>()
                .Join(DatabaseAccessor.Query<PermissionUser>().Where(x => !x.IsDeleted && x.User.IsEquivalent(targetGenerator.Entity)), x => x, x => x.Permission, (x, y) => new { Permission = x, PermissionUser = y })
                 .Join(DatabaseAccessor.Query<SystemPermission>(), x => x.Permission.SystemPermission, (x, y) => new { x.Permission.Id, y.Path, JunctionId = x.PermissionUser.Id })
                 .ToListAsync();
            var permissionToAdd = await DatabaseAccessor.Query<SystemPermission>()
                .Where(x => x.Path == PermissionsCommon.Section.Details.Path)
                .Join(DatabaseAccessor.Query<Permission>().Where(x => x.Scope.IsEquivalent(CurrentOrganizationGenerator.Entity)), x => x, x => x.SystemPermission, (x, y) => y.Id)
                .SingleAsync();
            var permissionToNotRemove = currentPermissions.Single(x => x.Path == PermissionsCommon.User.Read.Path);

            var target = AddressProvider
                .Get((UserController c) => c.PostAssignedPermissions(null, targetGenerator.Entity.Id, null));

            var permissionsToSave = new[]
            {
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToNotRemove.Id,
                    JunctionId = permissionToNotRemove.JunctionId
                },
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToAdd,
                    JunctionId = null
                }
            };

            var result = await Client.CallServiceAsync<IEnumerable<PermissionAssignmentPersistenceViewModel>, ValidationResult>(
                target,
                permissionsToSave,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.Single().ErrorCode.ShouldBeEqual("NoLockError");
        }

        [TestMethod]
        public async Task UserWithoutAppropriatePermissionCannotChangeUserPermissionsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.User.Read.Path,
                    PermissionsCommon.User.Details.Path,
                    PermissionsCommon.Section.Details.Path,
                    PermissionsCommon.User.Details.Assignment.Common_Permission.Path);
            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.User.Details.Path
            };

            var targetGenerator = CurrentOrganizationGenerator.CreateUser()
                .AddPermissions(targetPermissions);

            await FinalizeInitializationAsync();

            var currentPermissions = await DatabaseAccessor.Query<Permission>()
                .Join(DatabaseAccessor.Query<PermissionUser>().Where(x => !x.IsDeleted && x.User.IsEquivalent(targetGenerator.Entity)), x => x, x => x.Permission, (x, y) => new { Permission = x, PermissionUser = y })
                 .Join(DatabaseAccessor.Query<SystemPermission>(), x => x.Permission.SystemPermission, (x, y) => new { x.Permission.Id, y.Path, JunctionId = x.PermissionUser.Id })
                 .ToListAsync();
            var permissionToAdd = await DatabaseAccessor.Query<SystemPermission>()
                .Where(x => x.Path == PermissionsCommon.Section.Details.Path)
                .Join(DatabaseAccessor.Query<Permission>().Where(x => x.Scope.IsEquivalent(CurrentOrganizationGenerator.Entity)), x => x, x => x.SystemPermission, (x, y) => y.Id)
                .SingleAsync();
            var permissionToNotRemove = currentPermissions.Single(x => x.Path == PermissionsCommon.User.Read.Path);

            var target = AddressProvider
                .Get((UserController c) => c.PostAssignedPermissions(null, targetGenerator.Entity.Id, null));

            var permissionsToSave = new[]
            {
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToNotRemove.Id,
                    JunctionId = permissionToNotRemove.JunctionId
                },
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToAdd,
                    JunctionId = null
                }
            };

            var result = await Client.CallServiceAsync<IEnumerable<PermissionAssignmentPersistenceViewModel>, ValidationResult>(
                target,
                permissionsToSave,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        private static async Task InitializeAsync()
        {
            await AssemblyInitialize.Initialize;
        }
    }
}