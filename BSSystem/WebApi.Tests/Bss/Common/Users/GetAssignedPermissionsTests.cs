using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.Web;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Users
{
    [TestClass]
    public sealed class GetAssignedPermissionsTests
        : BaseApiTests
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = InitializeAsync();
        }

        [TestMethod]
        public async Task UserCanListUserPermissionsAsync()
        {
            await TestInitialize;
            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.Section.Read.Path
            };

            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.User.Read.Path,
                    PermissionsCommon.Section.Read.Path,
                    PermissionsCommon.User.Details.Path);
            CurrentUserGenerator.AddPermission(
                PermissionsCommon.User.Details.Path);

            var targetGenerator = CurrentOrganizationGenerator.CreateUser()
                .AddPermissions(targetPermissions);

            await FinalizeInitializationAsync();

            var target = AddressProvider
                .Get((UserController c) => c.GetAssignedPermissions(targetGenerator.Entity.Id));
            var result = await Client.CallServiceAsync<IEnumerable<PermissionAssignmentViewModel>>(
                target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);

            var assignedPermissions = result.Response.Where(x => x.IsAssigned).Select(x => x.Id).ToArray();
            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var assignedPermissionsPaths = await databaseAccessor.Query<Permission>()
                     .Where(x => assignedPermissions.Contains(x.Id))
                     .Join(databaseAccessor.Query<SystemPermission>(), x => x.SystemPermission, (x, y) => y.Path)
                     .ToListAsync();
            CollectionAssert.AreEquivalent(targetPermissions, assignedPermissionsPaths);
        }

        [TestMethod]
        public async Task UserCannotListUserPermissionsIfUserIsFromOtherOrganizationAsync()
        {
            await TestInitialize;
            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.Section.Details.Path
            };
            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.User.Details.Path,
                    PermissionsCommon.User.Read.Path,
                    PermissionsCommon.Section.Details.Path);
            CurrentUserGenerator.AddPermission(
                PermissionsCommon.User.Details.Path);

            var organizationGenerator = Generator.CreateOrganization()
                .AddPermissions(targetPermissions);
            var targetGenerator = organizationGenerator.CreateUser()
                .AddPermissions(targetPermissions);

            await FinalizeInitializationAsync();

            var target = AddressProvider
                .Get((UserController c) => c.GetAssignedPermissions(targetGenerator.Entity.Id));
            var result = await Client.CallServiceAsync<IEnumerable<PermissionAssignmentViewModel>>(
                target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task UserWithoutAppropriatePermissionCannotListUserPermissionsAsync()
        {
            await TestInitialize;
            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.Section.Details.Path
            };

            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.Organization.Details.Path,
                    PermissionsCommon.User.Read.Path,
                    PermissionsCommon.Section.Details.Path);

            var targetGenerator = CurrentOrganizationGenerator.CreateUser()
                .AddPermissions(targetPermissions);

            await FinalizeInitializationAsync();

            var target = AddressProvider
                .Get((OrganizationController c) => c.GetAssignedPermissions(targetGenerator.Entity.Id));
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result = await client.CallServiceAsync<IEnumerable<PermissionAssignmentViewModel>>(
                target,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        private static async Task InitializeAsync()
        {
            await AssemblyInitialize.Initialize;
        }
    }
}