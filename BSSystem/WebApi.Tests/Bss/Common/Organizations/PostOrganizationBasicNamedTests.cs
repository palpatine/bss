﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public sealed class PostOrganizationBasicNamedTests
        : BasicPostNamedEntityTests<Organization, OrganizationViewModel>
    {
        private Company _company;

        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Organization.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((OrganizationController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Organization CreateEntityToModify()
        {
            var targetGenerator = Generator.CreateOrganization(
                (o, c) =>
                {
                    _company = c;
                });

            return targetGenerator.Entity;
        }

        protected override OrganizationViewModel CreateModel()
        {
            var model = new OrganizationViewModel
            {
                Id = Entity?.Id ?? 0,
                DateStart = DateTime.Today,
                FullName = "newFullName",
                Name = "newName",
            };
            return model;
        }

        protected override void LockEntity()
        {
            var token = OperationToken;
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddLock(_company, token)
                .AddLock(Entity, token);
        }
    }
}