﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public class DeleteOrganizationBasicTests
        : BasicDeleteVersionedEntityTests<Organization>
    {
        private Company _company;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Organization.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                  .Get((OrganizationController c) => c.Delete(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Organization CreateEntityToModify()
        {
            return Generator.CreateOrganization((o, c) => _company = c).Entity;
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddLock(Entity, OperationToken)
                .AddLock(_company, OperationToken);
        }
    }
}