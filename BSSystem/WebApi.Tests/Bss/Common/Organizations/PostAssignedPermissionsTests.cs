using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.Web;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public sealed class PostAssignedPermissionsTests
        : BaseApiTests
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = InitializeAsync();
        }

        [TestMethod]
        public async Task UserCanChangeOrganizationPermissionsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.Organization.Details.Assignment.Common_Permission.Path);
            CurrentUserGenerator.AddPermissions(
                PermissionsCommon.Organization.Details.Assignment.Common_Permission.Path);
            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.User.Details.Path
            };

            var targetGenerator = Generator.CreateOrganization();
            targetGenerator.AddModule(ModulesProvider.Common);
               targetGenerator.AddPermissions(targetPermissions);

            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((Permission x) => x.Scope, targetGenerator.Entity, "token");
            await FinalizeInitializationAsync();

            var currentPermissions = await DatabaseAccessor.Query<Permission>()
                 .Where(x => x.Scope.IsEquivalent(targetGenerator.Entity))
                 .Join(DatabaseAccessor.Query<SystemPermission>(), x => x.SystemPermission, (x, y) => new { y.Id, y.Path, JunctionId = x.Id })
                 .ToListAsync();
            var systemPermissionToAdd = await DatabaseAccessor.Query<SystemPermission>()
                .Where(x => x.Path == PermissionsCommon.Section.Details.Path)
                .Select(x => x.Id)
                .SingleAsync();
            var target = AddressProvider
                .Get((OrganizationController c) => c.PostAssignedPermissions(targetGenerator.Entity.Id, null));
            var permissionToNotRemove = currentPermissions.Single(x => x.Path == PermissionsCommon.User.Read.Path);
            var permissionsToSave = new[]
            {
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToNotRemove.Id,
                    JunctionId = permissionToNotRemove.JunctionId
                },
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = systemPermissionToAdd,
                    JunctionId = null
                }
            };
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result = await client.CallServiceAsync<IEnumerable<PermissionAssignmentPersistenceViewModel>, ValidationResult>(
                target,
                permissionsToSave,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(",", result.Response.Errors.Select(x => x.ErrorCode + ":" + x.Message)));

            var permissionsAfterCall = await DatabaseAccessor.Query<Permission>()
                .Where(x => !x.IsDeleted && x.Scope.IsEquivalent(targetGenerator.Entity))
                .Join(DatabaseAccessor.Query<SystemPermission>(), x => x.SystemPermission, (x, y) => y.Path)
                .ToListAsync();
            CollectionAssert.AreEquivalent(
                new[]
                {
                    PermissionsCommon.Section.Details.Path,
                    PermissionsCommon.User.Read.Path
                },
                permissionsAfterCall);
        }

        [TestMethod]
        public async Task UserMustHaveLockInOrderToChangeOrganizationPermissionsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator
                .AddPermissions(
                PermissionsCommon.Organization.Details.Assignment.Common_Permission.Path);

            CurrentUserGenerator
                .AddPermission(
                    PermissionsCommon.Organization.Details.Assignment.Common_Permission.Path);

            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.User.Details.Path
            };

            var targetGenerator = Generator.CreateOrganization();
            targetGenerator.AddModule(ModulesProvider.Common);
                targetGenerator.AddPermissions(targetPermissions);

            await FinalizeInitializationAsync();

            var currentPermissions = await DatabaseAccessor.Query<Permission>()
                .Where(x => x.Scope.IsEquivalent(targetGenerator.Entity))
                .Join(
                    DatabaseAccessor.Query<SystemPermission>(),
                    x => x.SystemPermission,
                    (x, y) => new { y.Id, y.Path, JunctionId = x.Id })
                .ToListAsync();
            var systemPermissionToAdd = await DatabaseAccessor.Query<SystemPermission>()
                .Where(x => x.Path == PermissionsCommon.Section.Details.Path)
                .Select(x => x.Id)
                .SingleAsync();
            var target = AddressProvider
                .Get((OrganizationController c) => c.PostAssignedPermissions(targetGenerator.Entity.Id, null));
            var permissionToNotRemove = currentPermissions.Single(x => x.Path == PermissionsCommon.User.Read.Path);
            var permissionsToSave = new[]
            {
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToNotRemove.Id,
                    JunctionId = permissionToNotRemove.JunctionId
                },
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = systemPermissionToAdd,
                    JunctionId = null
                }
            };
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result =
                await client.CallServiceAsync<IEnumerable<PermissionAssignmentPersistenceViewModel>, ValidationResult>(
                    target,
                    permissionsToSave,
                    credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
            result.Response.Errors.Single().Message.ShouldBeEqual("NoLockError");
        }

        [TestMethod]
        public async Task UserWithoutAppropriatePermissionCannotChangeOrganizationPermissionsAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.Organization.Details.Assignment.Common_Permission.Path);

            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.User.Details.Path
            };

            var targetGenerator = Generator.CreateOrganization();
            targetGenerator.AddModule(ModulesProvider.Common);
            targetGenerator.AddPermissions(targetPermissions);

            await FinalizeInitializationAsync();

            var currentPermissions = await DatabaseAccessor.Query<Permission>()
                .Where(x => x.Scope.IsEquivalent(targetGenerator.Entity))
                .Join(
                    DatabaseAccessor.Query<SystemPermission>(),
                    x => x.SystemPermission,
                    (x, y) => new { y.Id, y.Path, JunctionId = x.Id })
                .ToListAsync();
            var systemPermissionToAdd = await DatabaseAccessor.Query<SystemPermission>()
                .Where(x => x.Path == PermissionsCommon.Section.Details.Path)
                .Select(x => x.Id)
                .SingleAsync();
            var target = AddressProvider
                .Get((OrganizationController c) => c.PostAssignedPermissions(targetGenerator.Entity.Id, null));
            var permissionToNotRemove = currentPermissions.Single(x => x.Path == PermissionsCommon.User.Read.Path);
            var permissionsToSave = new[]
            {
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = permissionToNotRemove.Id,
                    JunctionId = permissionToNotRemove.JunctionId
                },
                new PermissionAssignmentPersistenceViewModel
                {
                    Id = systemPermissionToAdd,
                    JunctionId = null
                }
            };
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result =
                await client.CallServiceAsync<IEnumerable<PermissionAssignmentPersistenceViewModel>, ValidationResult>(
                    target,
                    permissionsToSave,
                    credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        private static async Task InitializeAsync()
        {
            await AssemblyInitialize.Initialize;
        }
    }
}