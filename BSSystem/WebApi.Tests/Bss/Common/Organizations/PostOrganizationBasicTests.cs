using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public sealed class PostOrganizationBasicTests
        : BasicPostVersionedEntityTests<Organization, OrganizationViewModel>
    {
        private Company _company;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Organization.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((OrganizationController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        [TestMethod]
        public async Task UserCannotCreateEntityWithDateEndBeingEarlierThanDateStartAsync()
        {
            Func<Task> prepare = async () =>
            {
                await Task.Delay(0);
                CurrentUserGenerator.AddPermission(Permission);
                PrepareForCreate();
            };

            Func<OrganizationViewModel> createModel = () =>
            {
                var model = CreateModel();
                var value = new DateTimeOffset?(DateTime.Today.AddDays(-1));
                model.DateEnd = value;
                model.DateStart = new DateTimeOffset(DateTime.Today);
                return model;
            };

            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                result => result.Errors.Single().Reference.ShouldBeEqual("DateEnd"));
        }

        protected override Organization CreateEntityToModify()
        {
            var targetGenerator = Generator.CreateOrganization(
                (o, c) =>
                {
                    _company = c;
                    _company.Fax = 12345;
                    _company.CountryCode = "CC";
                    _company.Email = "email";
                    _company.OfficialNumber = "officialNumber";
                    _company.Phone = 23456;
                    _company.StatisticNumber = "statisitcNumber";
                    _company.VatNumber = "vatNumber";
                });

            return targetGenerator.Entity;
        }

        protected override OrganizationViewModel CreateModel()
        {
            var model = new OrganizationViewModel
            {
                Id = Entity != null ? Entity.Id : 0,
                CountryCode = "CC2",
                DateStart = DateTime.Today.AddDays(1),
                DateEnd = DateTime.Today.AddDays(3),
                DisplayName = "DN" + ServiceLocator.Current.GetInstance<NumbersGenerator>().GenerateUnique().First(),
                Email = "newEmail",
                Fax = 0987,
                FullName = "newFullName",
                Name = "newName",
                OfficialNumber = "newOfficalNumber",
                Phone = 8765432,
                StatisticNumber = "newStatisticNumber",
                VatNumber = "newVatNumber"
            };
            return model;
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddLock(_company, "token")
                .AddLock(Entity, "token");
        }

        protected override async Task VerifyPersistenceAsync(Organization entity)
        {
            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var companyAfterUpdate = await databaseAccessor.Query<Company>()
                .Join(databaseAccessor.Query<Organization>().Where(x => x.IsEquivalent(entity)), x => x, x => x.Company, (x, y) => x)
                .SingleAsync();
            entity.DisplayName.ShouldBeEqual(Model.DisplayName);
            entity.DateStart.ShouldBeEqual(Model.DateStart.Date);
            entity.DateEnd.ShouldBeEqual(Model.DateEnd.Value.Date);
            companyAfterUpdate.Name.ShouldBeEqual(Model.Name);
            companyAfterUpdate.FullName.ShouldBeEqual(Model.FullName);
            companyAfterUpdate.Fax.ShouldBeEqual(Model.Fax);
            companyAfterUpdate.CountryCode.ShouldBeEqual(Model.CountryCode);
            companyAfterUpdate.Email.ShouldBeEqual(Model.Email);
            companyAfterUpdate.OfficialNumber.ShouldBeEqual(Model.OfficialNumber);
            companyAfterUpdate.Phone.ShouldBeEqual(Model.Phone);
            companyAfterUpdate.StatisticNumber.ShouldBeEqual(Model.StatisticNumber);
            companyAfterUpdate.VatNumber.ShouldBeEqual(Model.VatNumber);
        }
    }
}