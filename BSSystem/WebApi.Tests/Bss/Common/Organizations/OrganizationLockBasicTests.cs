﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public sealed class OrganizationLockBasicTests
        : BasicLockTests<Organization, IOrganization>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Organization.Details.Write.Path;

        protected override IUser UserForJunctionLock => new UserMarker(1);

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Organization CreateEntityToLock()
        {
            var target = Generator.CreateOrganization().Entity;
            return target;
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((OrganizationController c) => c.Lock(id));
        }
    }
}