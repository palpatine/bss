﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public class GetOrganizationBasicTests
        : BasicGetEntityTests<OrganizationViewModel, Organization>
    {
        private Company _company;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Organization.Details.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider.Get((OrganizationController x) => x.Get(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Organization CreateEntity()
        {
            var targetEntityGenerator = Generator.CreateOrganization(
                (o, c) =>
                {
                    _company = c;
                    _company.Fax = 12345;
                    _company.CountryCode = "CC";
                    _company.Email = "email";
                    _company.OfficialNumber = "officialNumber";
                    _company.Phone = 23456;
                    _company.StatisticNumber = "statisitcNumber";
                    _company.VatNumber = "vatNumber";
                });

            return targetEntityGenerator.Entity;
        }

        protected override void VerifySuccessResult(
            OrganizationViewModel actual,
            Organization expected)
        {
            actual.ShouldBeEqual(expected, _company);
        }
    }
}