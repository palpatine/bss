﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public class GetListOrganizationBasicTests
        : BasicGetListTests<Organization, OrganizationViewModel>
    {
        private Company _company;

        private Organization _organization;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Organization.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((OrganizationController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<Organization> CreateEntities()
        {
            _company = null;
            var targetEntityGenerator = Generator.CreateOrganization(
                (o, c) =>
                {
                    _company = c;
                    _company.Fax = 44232;
                    _company.CountryCode = "AA";
                    _company.Email = "email";
                    _company.OfficialNumber = "officialNumber";
                    _company.Phone = 13412;
                    _company.StatisticNumber = "statisitcNumber";
                    _company.VatNumber = "vatNumber";
                });
            _organization = targetEntityGenerator.Entity;

            return new[] { _organization };
        }

        protected override void ShouldMatch(OrganizationViewModel actual, Organization expected)
        {
        }

        protected override void ValidateResponse(
            IEnumerable<OrganizationViewModel> actual,
            IEnumerable<Organization> expected)
        {
            //// This tests are a bit wrong as this entity is not organization scoped and other tests influence
            //// results that are recived here. So for instance there is no way to check if count of entities recived is correct.
            var model = actual.Single(x => x.Id == _organization.Id);
            model.DisplayName.ShouldBeEqual(_organization.DisplayName);
            model.DateStart.ShouldBeEqual(_organization.DateStart);
            model.DateEnd.ShouldBeEqual(_organization.DateEnd);
            model.Name.ShouldBeEqual(_company.Name);
            model.FullName.ShouldBeEqual(_company.FullName);
            model.Fax.ShouldBeEqual(_company.Fax);
            model.CountryCode.ShouldBeEqual(_company.CountryCode);
            model.Email.ShouldBeEqual(_company.Email);
            model.OfficialNumber.ShouldBeEqual(_company.OfficialNumber);
            model.Phone.ShouldBeEqual(_company.Phone);
            model.StatisticNumber.ShouldBeEqual(_company.StatisticNumber);
            model.VatNumber.ShouldBeEqual(_company.VatNumber);
        }
    }
}