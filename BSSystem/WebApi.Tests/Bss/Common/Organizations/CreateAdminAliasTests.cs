using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.Web;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public sealed class CreateAdminAliasTests
        : BaseApiTests
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = InitializeAsync();
        }

        [TestMethod]
        public async Task UserWithAddAliasPermissionCanCreateAliasIfHeDoesNotHaveOneInTargetedOrganizationAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator
                .AddPermissions(
                    PermissionsCommon.Organization.Details.AddAlias.Path);
            CurrentUserGenerator.AddPermission(
                PermissionsCommon.Organization.Details.AddAlias.Path);

            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.User.Details.Path
            };

            var targetGenerator = Generator.CreateOrganization();
            targetGenerator.AddModule(ModulesProvider.Common);
            targetGenerator.AddPermissions(targetPermissions);

            await FinalizeInitializationAsync();

            var target = AddressProvider
                .Get((OrganizationController c) => c.CreateAdminAlias(targetGenerator.Entity.Id));
            var result = await Client.CallServiceAsync<ValidationResult>(
                target,
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(",", result.Response.Errors.Select(x => x.ErrorCode + ":" + x.Message)));
            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var alias = await databaseAccessor.Query<User>()
                .SingleOrDefaultAsync(x => !x.IsDeleted && x.Parent.IsEquivalent(CurrentUserGenerator.Entity));
            alias.ShouldNotBeNull("alias must be created");
            alias.IsAlias.ShouldBeTrue("alias must be marked as alias");
            alias.Root.Id.ShouldBeEqual(CurrentUserGenerator.Entity.Id, "aliases root must be user who generated it");
            alias.Scope.Id.ShouldBeEqual(targetGenerator.Entity.Id, "alias must belong to targeted organization");
            alias.TimeEnd.ShouldBeNull("administarator alias must not be deleted");
            alias.DateEnd.ShouldBeNull("administarator alias must not be limited in time");

            var permissions = await databaseAccessor.Query<PermissionUser>()
                .Where(x => x.User.IsEquivalent(alias))
                .Join(
                    databaseAccessor.Query<Permission>(),
                    x => x.Permission,
                    (pu, p) => new { pu, p })
                .Join(
                    databaseAccessor.Query<SystemPermission>(),
                    x => x.p.SystemPermission,
                    (ppu, sp) => sp.Path)
                .ToListAsync();

            CollectionAssert.AreEquivalent(targetPermissions, permissions, "alias should get full permissions that are available for organization");
        }

        [TestMethod]
        public async Task UserWithAddAliasPermissionCanRenewHisAliasIfHeDoesHaveOneInTargetedOrganizationAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator
                .AddPermissions(
                PermissionsCommon.Organization.Details.AddAlias.Path);
            CurrentUserGenerator.AddPermission(
                PermissionsCommon.Organization.Details.AddAlias.Path);

            var targetPermissions = new[]
            {
                PermissionsCommon.User.CanLogin.Path,
                PermissionsCommon.User.Read.Path,
                PermissionsCommon.User.Details.Path
            };

            var targetGenerator = Generator.CreateOrganization();
            targetGenerator.AddModule(ModulesProvider.Common);
            targetGenerator.AddPermissions(targetPermissions);

            var existingAlias = targetGenerator
                .CreateUser(x =>
                     {
                         x.Parent = CurrentUserGenerator.Entity;
                         x.Root = CurrentUserGenerator.Entity;
                         x.DateEnd = DateTime.Today.AddDays(4);
                     })
                .AddPermission(PermissionsCommon.User.CanLogin.Path);

            await FinalizeInitializationAsync();

            var target = AddressProvider
                .Get((OrganizationController c) => c.CreateAdminAlias(targetGenerator.Entity.Id));
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result = await client.CallServiceAsync<ValidationResult>(
                target,
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.OK);
            result.Response.IsValid.ShouldBeTrue(string.Join(",", result.Response.Errors.Select(x => x.ErrorCode + ":" + x.Message)));

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var alias = await databaseAccessor.Query<User>()
                .SingleOrDefaultAsync(x => !x.IsDeleted && x.Parent.IsEquivalent(CurrentUserGenerator.Entity));
            alias.ShouldNotBeNull("alias must be created");
            alias.Id.ShouldBeEqual(existingAlias.Entity.Id, "existion alias should be upadate");
            alias.IsAlias.ShouldBeTrue("alias must be marked as alias");
            alias.Root.Id.ShouldBeEqual(CurrentUserGenerator.Entity.Id, "aliases root must be user who generated it");
            alias.Scope.Id.ShouldBeEqual(targetGenerator.Entity.Id, "alias must belong to targeted organization");
            alias.TimeEnd.ShouldBeNull("administarator alias must not be deleted");
            alias.DateEnd.ShouldBeNull("administarator alias must not be limited in time");

            var permissions = await databaseAccessor.Query<PermissionUser>()
               .Where(x => x.User.IsEquivalent(alias))
               .Join(
                   databaseAccessor.Query<Permission>(),
                   x => x.Permission,
                   (pu, p) => new { pu, p })
               .Join(
                   databaseAccessor.Query<SystemPermission>(),
                   x => x.p.SystemPermission,
                   (ppu, sp) => sp.Path)
               .ToListAsync();

            targetGenerator.AddModule(ModulesProvider.Common);
            targetGenerator.AddPermissions(targetPermissions);
            CollectionAssert.AreEquivalent(targetPermissions, permissions, "alias should get full permissions that are available for organization without duplicates");
        }

        [TestMethod]
        public async Task UserWithoutAddAliasPermissionCannotCreateAliasAsync()
        {
            await TestInitialize;

            CurrentOrganizationGenerator.AddPermissions(
                PermissionsCommon.Organization.Details.AddAlias.Path);

            var targetGenerator = Generator.CreateOrganization();

            await FinalizeInitializationAsync();

            var target = AddressProvider
                .Get((OrganizationController c) => c.CreateAdminAlias(targetGenerator.Entity.Id));
            var client = ServiceLocator.Current.GetInstance<IServiceClient>();
            var result = await client.CallServiceAsync<ValidationResult>(
                target,
                HttpMethod.Post,
                credentials: Credentials);

            result.Code.ShouldBeEqual(HttpStatusCode.Forbidden);
        }

        private static async Task InitializeAsync()
        {
            await AssemblyInitialize.Initialize;
        }
    }
}