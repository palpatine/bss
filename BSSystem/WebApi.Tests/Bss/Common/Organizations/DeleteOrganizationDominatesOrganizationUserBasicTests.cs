﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Organizations
{
    [TestClass]
    public class DeleteOrganizationDominatesOrganizationUserBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<Organization, OrganizationUser>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission { get; } = PermissionsCommon.Organization.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((OrganizationController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Organization CreateEntityToModify()
        {
            return CurrentOrganizationGenerator.Entity;
        }

        protected override OrganizationUser CreateSlaveEntity()
        {
            return CurrentOrganizationGenerator.AddUser(CurrentOrganizationGenerator.CreateUser().Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Organization>().Entity);
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddLock(Entity, OperationToken)
                .AddLock(Entity.Company, OperationToken);
        }
    }
}
