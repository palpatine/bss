using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Positions
{
    [TestClass]
    public sealed class PostPositionUsersBasicTests
        : BasicPostScopedAssignmentJunctionRelationTests<Position, User, PositionUser, AssignmentPersistenceViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Position.Details.Assignment.Common_User.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Position CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreatePosition(configure).Entity;
        }

        protected override User CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override PositionUser RelateEntities(
            Position master,
            User related,
            Action<IDatedEntity> configure)
        {
            var relation = Generator.Find<PositionGenerator>(master).AddUser(related);
            configure(relation);
            return relation;
        }

        protected override void Verify(PositionUser actual, AssignmentPersistenceViewModel expected)
        {
            actual.ShouldBeEqual(expected, x => x.Second, CurrentOrganizationGenerator);
        }
    }
}