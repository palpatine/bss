﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Positions
{
    [TestClass]
    public class PostPositionDominatesActivityPositionBasicTests
        : BasicPostEntityDominatesSlaveDependencyTests<Position, PositionViewModel, ActivityPosition>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.Position.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((PositionController a) => a.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Position CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreatePosition(masterDateRange).Entity;
        }

        protected override ActivityPosition CreateSlaveEntity(Position master, Action<IDatedEntity> masterDateRange, Action<IDatedEntity> slaveDateRange)
        {
            var activity = CurrentOrganizationGenerator.CreateActivityKind(masterDateRange).CreateActivity(masterDateRange);
            var slave = activity.AddPosition(master);
            slaveDateRange(slave);
            return slave;
        }

        protected override PositionViewModel GetViewModel(DateTime dateStart, DateTime? dateEnd)
        {
            return new PositionViewModel
            {
                Id = Entity.Id,
                DateStart = dateStart,
                DateEnd = dateEnd,
                Name = NextRandomValue(),
                DisplayName = NextRandomValue()
            };
        }
    }
}