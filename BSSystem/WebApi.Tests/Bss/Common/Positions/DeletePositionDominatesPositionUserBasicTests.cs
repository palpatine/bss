﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Positions
{
    [TestClass]
    public class DeletePositionDominatesPositionUserBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<Position, PositionUser>
    {
        protected PositionGenerator EntityGenerator => CurrentOrganizationGenerator.CreatePosition();

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission { get; } = PermissionsCommon.Position.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((PositionController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Position CreateEntityToModify()
        {
            return EntityGenerator.Entity;
        }

        protected override PositionUser CreateSlaveEntity()
        {
            var user = CurrentOrganizationGenerator.CreateUser();
            return user.AddPosition(Entity);
        }
    }
}