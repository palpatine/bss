using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Positions
{
    [TestClass]
    public sealed class GetPositionActivitiesBasicTests
        : BasicScopedAssignmentJunctionRelationTests<Position, Activity, ActivityPosition, AssignmentViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsCommon.Position.Details.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Position CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreatePosition(configure).Entity;
        }

        protected override Activity CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure).Entity;
        }

        protected override void RelateEntities(
            Position master,
            Activity related,
            Action<IDatedEntity> configure)
        {
            Generator.Find<PositionGenerator>(master).AddActivity(related, configure);
        }

        protected override void Verify(AssignmentViewModel actual, ActivityPosition expected)
        {
            actual.ShouldBeEqual(expected, x => x.First, CurrentOrganizationGenerator);
        }
    }
}