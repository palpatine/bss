﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Positions
{
    [TestClass]
    public sealed class PositionLockBasicTests
        : BasicScopedLockTests<Position, IPosition>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Position.Details.Write.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Position CreateEntityToLock()
        {
            var target = CurrentOrganizationGenerator.CreatePosition().Entity;
            return target;
        }

        protected override Position CreateEntityToLockInOtherOrganization()
        {
            var otherOrganization = Generator.CreateOrganization();
            var target = otherOrganization.CreatePosition().Entity;
            return target;
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((PositionController c) => c.Lock(id));
        }
    }
}