using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Positions
{
    [TestClass]
    public class GetListPositionBasicTests
        : BasicGetListScopedEntityTests<Position, PositionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Position.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((PositionController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<Position> CreateEntities()
        {
            var entities = new List<Position>();
            var first = CurrentOrganizationGenerator.CreatePosition(
                 (u) =>
                 {
                     u.DateEnd = DateTime.Today.AddDays(3);
                 });
            entities.Add(first.Entity);

            var second = CurrentOrganizationGenerator.CreatePosition();
            entities.Add(second.Entity);
            return entities;
        }

        protected override void ShouldMatch(PositionViewModel actual, Position expected)
        {
            actual.DateEnd.ShouldBeEqual(expected.DateEnd);
            actual.DateStart.ShouldBeEqual(expected.DateStart);
            actual.Name.ShouldBeEqual(expected.Name);
        }
    }
}