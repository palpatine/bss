﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Positions
{
    [TestClass]
    public sealed class JunctionLockPositionActivitiesBasicTests
        : BasicScopedJunctionLockTests<Position, IPosition, Activity>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsCommon.Position.Details.Assignment.Wtt_Activity.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateRelatedEntity()
        {
            var activityKindGenerator = CurrentOrganizationGenerator.CreateActivityKind();
            return activityKindGenerator.CreateActivity().Entity;
        }

        protected override IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<Position> activityGenerator,
            Activity relatedEntity)
        {
            var masterEntityGenerator = (PositionGenerator)activityGenerator;
            ActivityPosition junctionEntity = null;
            masterEntityGenerator.AddActivity(relatedEntity, x => junctionEntity = x);
            return junctionEntity;
        }

        protected override IGenerator<Position> MasterEntityGenerator(OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreatePosition();
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((PositionController c) => c.JunctionLock(id, RelatedTableKey));
        }
    }
}