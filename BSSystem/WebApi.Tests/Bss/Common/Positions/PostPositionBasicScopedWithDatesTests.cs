using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Positions
{
    [TestClass]
    public sealed class PostPositionBasicScopedWithDatesTests
        : BasicPostScopedWithDatesEntityTests<Position, PositionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Position.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((PositionController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Position CreateEntityToModify(
            OrganizationGenerator generator,
            Action<IDatedEntity> configureMaster,
            Action<IDatedEntity> configureSlave,
            DateTime otherDate)
        {
            var targetGenerator = generator.CreatePosition(
                u =>
                {
                    configureSlave(u);
                    u.DisplayName = "DisplayName";
                    u.Name = "Name";
                });

            return targetGenerator.Entity;
        }

        protected override PositionViewModel CreateModel(
            DateTime dateStart,
            DateTime? dateEnd,
            DateTime otherDate)
        {
            var model = new PositionViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = dateEnd,
                DateStart = dateStart,
                DisplayName = "DN",
                Name = "N"
            };
            return model;
        }

        protected override void Verify(
            OrganizationGenerator generator,
            Position actual,
            PositionViewModel expected)
        {
            actual.ShouldBeEqual(expected, generator);
        }
    }
}