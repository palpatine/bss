﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.RoleDefinitions
{
    [TestClass]
    public class PostRoleDefinitionDominatesOrganizationUserBasicTests
        : BasicPostEntityDominatesSlaveDependencyTests<RoleDefinition, RoleDefinitionViewModel, OrganizationUser>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.RoleDefinition.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((RoleDefinitionController a) => a.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override RoleDefinition CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateRoleDefinition<Organization>(masterDateRange).Entity;
        }

        protected override OrganizationUser CreateSlaveEntity(RoleDefinition master, Action<IDatedEntity> masterDateRange, Action<IDatedEntity> slaveDateRange)
        {
            var user = CurrentOrganizationGenerator.CreateUser(masterDateRange).Entity;
            var slave = CurrentOrganizationGenerator.AddUser(user, master);
            slaveDateRange(slave);
            return slave;
        }

        protected override RoleDefinitionViewModel GetViewModel(DateTime dateStart, DateTime? dateEnd)
        {
            return new RoleDefinitionViewModel
            {
                Id = Entity.Id,
                DateStart = dateStart,
                DateEnd = dateEnd,
                Target = new TargetViewModel
                {
                    Id = Entity.TableKey,
                },
                DisplayName = NextRandomValue()
            };
        }
    }
}