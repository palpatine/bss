using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.RoleDefinitions
{
    [TestClass]
    public class GetListRoleDefinitionBasicTests
        : BasicGetListScopedEntityTests<RoleDefinition, RoleDefinitionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.RoleDefinition.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((RoleDefinitionController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<RoleDefinition> CreateEntities()
        {
            var entities = new List<RoleDefinition>();
            var first = CurrentOrganizationGenerator.CreateRoleDefinition<Section>(
                 (u) =>
                 {
                     u.DateEnd = DateTime.Today.AddDays(3);
                 });
            entities.Add(first.Entity);

            var second = CurrentOrganizationGenerator.CreateRoleDefinition<Section>();
            entities.Add(second.Entity);
            return entities;
        }

        protected override void ShouldMatch(RoleDefinitionViewModel actual, RoleDefinition expected)
        {
            actual.DateEnd.ShouldBeEqual(expected.DateEnd);
            actual.DateStart.ShouldBeEqual(expected.DateStart);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
        }
    }
}