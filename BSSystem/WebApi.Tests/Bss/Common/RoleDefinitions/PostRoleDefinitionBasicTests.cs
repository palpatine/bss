using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.RoleDefinitions
{
    [TestClass]
    public sealed class PostRoleDefinitionBasicTests
        : BasicPostScopedVersionedEntityTests<RoleDefinition, RoleDefinitionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.RoleDefinition.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((RoleDefinitionController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override RoleDefinition CreateEntityToModify(OrganizationGenerator generator)
        {
            var targetGenerator = generator.CreateRoleDefinition<Section>(
                u =>
                {
                    u.DateEnd = DateTime.Today;
                    u.DateStart = DateTime.Today.AddDays(-3);
                    u.DisplayName = "DisplayName";
                });

            return targetGenerator.Entity;
        }

        protected override RoleDefinitionViewModel CreateModel()
        {
            var model = new RoleDefinitionViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = DateTime.Today.AddDays(4),
                DateStart = DateTime.Today.AddDays(-3),
                DisplayName = "DN",
                Target = new TargetViewModel
                {
                    Id = "Common_Section"
                }
            };
            return model;
        }

        protected override void VerifyPersistence(RoleDefinition entity, OrganizationGenerator organizationGenerator)
        {
            entity.ShouldBeEqual(Model, organizationGenerator);
        }
    }
}