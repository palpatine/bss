﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.RoleDefinitions
{
    [TestClass]
    public class DeleteRoleDefinitionTests
        : BaseDeleteVersionedEntityTests<RoleDefinition>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.RoleDefinition.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                  .Get((RoleDefinitionController c) => c.Delete(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        [TestMethod]
        public async Task UserCanDeleteRoleDefinitionWithPermissionAsync()
        {
            await TestInitialize;
            Assert.Inconclusive();

            // check user is deleted check slave is deleted
        }

        protected override RoleDefinition CreateEntityToModify()
        {
            return CurrentOrganizationGenerator.CreateRoleDefinition<Organization>().Entity;
        }
    }
}