﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.RoleDefinitions
{
    [TestClass]
    public sealed class PostRoleDefinitionBasicScopedNamedTests
        : BasicPostScopedNamedEntityTests<RoleDefinition, RoleDefinitionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsCommon.RoleDefinition.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((RoleDefinitionController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override RoleDefinition CreateEntityToModify(OrganizationGenerator generator)
        {
            var targetGenerator = generator.CreateRoleDefinition<User>(
                u =>
                {
                    u.DisplayName = "DisplayName";
                });

            return targetGenerator.Entity;
        }

        protected override RoleDefinitionViewModel CreateModel()
        {
            var model = new RoleDefinitionViewModel
            {
                Id = Entity?.Id ?? 0,
                Target = new TargetViewModel() { Id = Entity?.TableKey ?? StorableHelper.GetTableKey(typeof(Section)) },
                DateStart = DateTime.Today,
                DisplayName = "DN"
            };
            return model;
        }
    }
}