﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.RoleDefinitions
{
    [TestClass]
    public class DeleteRoleDefinitionDominatesOrganizationUserBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<RoleDefinition, OrganizationUser>
    {
        protected RoleDefinitionGenerator EntityGenerator => CurrentOrganizationGenerator.CreateRoleDefinition<Organization>();

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission { get; } = PermissionsCommon.RoleDefinition.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((RoleDefinitionController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override RoleDefinition CreateEntityToModify()
        {
            return EntityGenerator.Entity;
        }

        protected override OrganizationUser CreateSlaveEntity()
        {
            var user = CurrentOrganizationGenerator.CreateUser();
            var organizationUser = CurrentOrganizationGenerator.AddUser(user.Entity, Entity);
            return organizationUser;
        }
    }
}