﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public sealed class JunctionLockSectionUsersBasicTests
        : BasicScopedJunctionLockTests<Section, ISection, User>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Section.Details.Assignment.Common_User.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateRelatedEntity()
        {
            return CurrentOrganizationGenerator.CreateUser().Entity;
        }

        protected override IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<Section> activityGenerator,
            User relatedEntity)
        {
            var masterEntityGenerator = (SectionGenerator)activityGenerator;
            var roleDefinition = CurrentOrganizationGenerator.CreateRoleDefinition<Section>().Entity;
            SectionUser junctionEntity = null;
            masterEntityGenerator.AddUser(relatedEntity, roleDefinition, x => junctionEntity = x);
            return junctionEntity;
        }

        protected override IGenerator<Section> MasterEntityGenerator(OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreateSection();
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((SectionController c) => c.JunctionLock(id, RelatedTableKey));
        }
    }
}