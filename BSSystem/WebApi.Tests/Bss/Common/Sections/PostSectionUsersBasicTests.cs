using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public sealed class PostSectionUsersBasicTests
        : BasicPostScopedAssignmentJunctionRelationTests<Section, User, SectionUser, JobRoleAssignmentPersistenceViewModel>
    {
        private RoleDefinition _role;
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Section.Details.Assignment.Common_User.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateSection(configure).Entity;
        }

        protected override JobRoleAssignmentPersistenceViewModel CreateModel(SectionUser relation)
        {
            var model = base.CreateModel(relation);
            model.RoleId = relation.RoleDefinition.Id;
            model.JobKind = JobKind.None;
            return model;
        }

        protected override JobRoleAssignmentPersistenceViewModel CreateModel()
        {
            var model = base.CreateModel();
            model.RoleId = _role.Id;
            model.JobKind = JobKind.None;
            return model;
        }

        protected override User CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override void PrepareForCreate()
        {
            _role = CurrentOrganizationGenerator.CreateRoleDefinition<Section>().Entity;
        }

        protected override SectionUser RelateEntities(
            Section master,
            User related,
            Action<IDatedEntity> configure)
        {
            var relation = Generator.Find<SectionGenerator>(master).AddUser(related, CurrentOrganizationGenerator.CreateRoleDefinition<Section>(configure).Entity);
            configure(relation);
            return relation;
        }

        protected override void Verify(SectionUser actual, JobRoleAssignmentPersistenceViewModel expected)
        {
            actual.ShouldBeEqual(expected, x => x.Second, CurrentOrganizationGenerator);
        }
    }
}