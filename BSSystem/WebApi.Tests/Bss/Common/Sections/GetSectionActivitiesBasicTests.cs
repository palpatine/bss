using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public sealed class GetSectionActivitiesBasicTests
        : BasicScopedAssignmentJunctionRelationTests<Section, Activity, ActivitySection, JobRoleAssignmentViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Section.Details.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateSection(configure).Entity;
        }

        protected override Activity CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure).Entity;
        }

        protected override void RelateEntities(
            Section master,
            Activity related,
            Action<IDatedEntity> configure)
        {
            Generator.Find<SectionGenerator>(master).AddActivity(related, configure);
        }

        protected override void Verify(
            JobRoleAssignmentViewModel actual,
            ActivitySection expected)
        {
            actual.ShouldBeEqual(expected, x => x.First, CurrentOrganizationGenerator);
        }
    }
}