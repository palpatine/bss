using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public sealed class GetSectionUsersBasicTests
        : BasicScopedAssignmentJunctionRelationTests<Section, User, SectionUser, JobRoleAssignmentViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Section.Details.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateSection(configure).Entity;
        }

        protected override User CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override void RelateEntities(
            Section master,
            User related,
            Action<IDatedEntity> configure)
        {
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Section>(configure);
            Generator.Find<SectionGenerator>(master).AddUser(related, role.Entity, configure);
        }

        protected override void Verify(
            JobRoleAssignmentViewModel actual,
            SectionUser expected)
        {
            actual.ShouldBeEqual(expected, x => x.Second, CurrentOrganizationGenerator);
        }
    }
}