using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public class GetListSectionBasicTests
        : BasicGetListScopedEntityTests<Section, SectionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Section.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((SectionController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<Section> CreateEntities()
        {
            var entities = new List<Section>();
            var first = CurrentOrganizationGenerator.CreateSection(
                 (u) =>
                 {
                     u.DateStart = DateTime.Today.AddDays(-3);
                 });
            entities.Add(first.Entity);

            var second = CurrentOrganizationGenerator.CreateSection();
            entities.Add(second.Entity);
            return entities;
        }

        protected override void ShouldMatch(SectionViewModel actual, Section expected)
        {
            actual.DateEnd.ShouldBeEqual(expected.DateEnd);
            actual.DateStart.ShouldBeEqual(expected.DateStart);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.Parent.ShouldBeNull();
        }
    }
}