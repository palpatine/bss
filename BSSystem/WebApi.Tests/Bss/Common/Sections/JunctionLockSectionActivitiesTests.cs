﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public sealed class JunctionLockSectionActivitiesTests : BasicScopedJunctionLockTests<Section, ISection, Activity>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsCommon.Section.Details.Assignment.Wtt_Activity.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateRelatedEntity()
        {
            return CurrentOrganizationGenerator.CreateActivityKind().CreateActivity().Entity;
        }

        protected override IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<Section> activityGenerator,
            Activity relatedEntity)
        {
            var masterEntityGenerator = (SectionGenerator)activityGenerator;
            return masterEntityGenerator.AddActivity(relatedEntity);
        }

        protected override IGenerator<Section> MasterEntityGenerator(OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreateSection();
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((SectionController c) => c.JunctionLock(id, RelatedTableKey));
        }
    }
}