﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public sealed class PostSectionBasicScopedNamedTests
        : BasicPostScopedNamedEntityTests<Section, SectionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsCommon.Section.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((SectionController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateEntityToModify(OrganizationGenerator generator)
        {
            var targetGenerator = generator.CreateSection();

            return targetGenerator.Entity;
        }

        protected override SectionViewModel CreateModel()
        {
            var model = new SectionViewModel
            {
                Id = Entity?.Id ?? 0,
                DateStart = DateTime.Today,
                DisplayName = "DN",
                Name = "Name"
            };
            return model;
        }
    }
}