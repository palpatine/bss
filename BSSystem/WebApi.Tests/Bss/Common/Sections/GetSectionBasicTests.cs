using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public class GetSectionBasicTests
        : BasicGetScopedEntityTests<Section, SectionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Section.Details.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider.Get((SectionController x) => x.Get(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateEntity()
        {
            var generator = CurrentOrganizationGenerator.CreateSection(
              (s) =>
              {
                  s.DateEnd = DateTime.Today.AddDays(3);
              });

            return generator.Entity;
        }

        protected override void VerifySuccessResult(
            SectionViewModel actual,
            Section expected)
        {
            actual.DateStart.ShouldBeEqual(expected.DateStart);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.Parent.ShouldBeNull();
            actual.DateEnd.ShouldBeEqual(expected.DateEnd);
        }
    }
}