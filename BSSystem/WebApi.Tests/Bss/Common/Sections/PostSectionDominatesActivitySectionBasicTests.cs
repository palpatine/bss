﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public class PostSectionDominatesActivitySectionBasicTests : BasicPostEntityDominatesSlaveDependencyTests<Section, SectionViewModel, ActivitySection>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.Section.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((SectionController a) => a.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateSection(masterDateRange).Entity;
        }

        protected override ActivitySection CreateSlaveEntity(Section master, Action<IDatedEntity> masterDateRange, Action<IDatedEntity> slaveDateRange)
        {
            var activity = CurrentOrganizationGenerator.CreateActivityKind(masterDateRange).CreateActivity(masterDateRange);
            var slave = activity.AddSection(master);
            slaveDateRange(slave);
            return slave;
        }

        protected override SectionViewModel GetViewModel(DateTime dateStart, DateTime? dateEnd)
        {
            return new SectionViewModel
            {
                Id = Entity.Id,
                DateStart = dateStart,
                DateEnd = dateEnd,
                DisplayName = NextRandomValue(),
                Name = NextRandomValue(),
            };
        }
    }
}