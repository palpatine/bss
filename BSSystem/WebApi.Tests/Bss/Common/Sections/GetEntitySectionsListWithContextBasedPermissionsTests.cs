﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public class GetEntitySectionsListWithContextBasedPermissionsTests
        : BasicGetListWithContextBasedPermissionsTests<Section, ISection, SectionViewModel, SectionUser, ISectionUser>
    {
        public override string Permission => PermissionsCommon.Section.Read.Path;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override Uri Target
        {
            get { return AddressProvider.Get((SectionController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override SectionUser AssociateUser(
            UserGenerator currentUserGenerator,
            IGenerator<Section> generator,
            IRoleDefinition roleDefinition)
        {
            var entityGenerator = (SectionGenerator)generator;
            SectionUser sectionUser = null;
            entityGenerator.AddUser(currentUserGenerator.Entity, roleDefinition, x => sectionUser = x);
            return sectionUser;
        }

        protected override IGenerator<Section> CreateEntity(Action<IDatedEntity> configure)
        {
            var section = CurrentOrganizationGenerator.CreateSection(configure);
            return section;
        }

        protected override void ShouldMatch(
            SectionViewModel viewModel,
            Section current)
        {
            viewModel.Id.ShouldBeEqual(current.Id);
            viewModel.Name.ShouldBeEqual(current.Name);
            viewModel.DateStart.ShouldBeEqual(current.DateStart);
            viewModel.DateEnd.ShouldBeEqual(current.DateEnd);
            viewModel.DisplayName.ShouldBeEqual(current.DisplayName);
        }
    }
}