﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public class BasicPostSectionSelfDependencyTests
        : BasicPostVersionedEntityDependsOnVersionedEntityTests<Section, SectionViewModel, Section, ISection>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission => PermissionsCommon.Section.Details.Write.Path;

        protected override string RelationName
        {
            get { return ExpressionExtensions.GetPropertyName((Section x) => x.Parent); }
        }

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((SectionController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateDependencyEntity()
        {
            return CurrentOrganizationGenerator.CreateSection().Entity;
        }

        protected override Section CreateEntityToModify(
            Section master,
            Action<Section> configure)
        {
            return CurrentOrganizationGenerator.CreateSection(
                x =>
                {
                    x.Parent = master;
                    configure(x);
                })
            .Entity;
        }

        protected override SectionViewModel CreateModel(
            Section master)
        {
            return new SectionViewModel
            {
                Id = Entity?.Id ?? 0,
                DisplayName = NextRandomValue(),
                Name = NextRandomValue(),
                DateStart = Entity?.DateStart ?? DateTime.Now.Date,
                DateEnd = Entity?.DateStart,
                Parent = new NamedViewModel
                {
                    Id = master.Id
                }
            };
        }
    }
}