﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public class PostSectionDominatesSectionUserBasicTests
        : BasicPostEntityDominatesSlaveDependencyTests<Section, SectionViewModel, SectionUser>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.Section.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((SectionController a) => a.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateSection(masterDateRange).Entity;
        }

        protected override SectionUser CreateSlaveEntity(Section master, Action<IDatedEntity> masterDateRange, Action<IDatedEntity> slaveDateRange)
        {
            var user = CurrentOrganizationGenerator.CreateUser(masterDateRange);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Section>(masterDateRange).Entity;
            var slave = user.AddSection(master, role);
            slaveDateRange(slave);
            return slave;
        }

        protected override SectionViewModel GetViewModel(DateTime dateStart, DateTime? dateEnd)
        {
            return new SectionViewModel
            {
                Id = Entity.Id,
                DateStart = dateStart,
                DateEnd = dateEnd,
                DisplayName = NextRandomValue(),
                Name = NextRandomValue(),
            };
        }
    }
}