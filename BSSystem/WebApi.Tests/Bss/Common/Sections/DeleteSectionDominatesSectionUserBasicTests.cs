﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.Sections
{
    [TestClass]
    public class DeleteSectionDominatesSectionUserBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<Section, SectionUser>
    {
        protected SectionGenerator EntityGenerator => CurrentOrganizationGenerator.CreateSection();

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common };

        protected override string Permission { get; } = PermissionsCommon.Section.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((SectionController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Section CreateEntityToModify()
        {
            return EntityGenerator.Entity;
        }

        protected override SectionUser CreateSlaveEntity()
        {
            var user = CurrentOrganizationGenerator.CreateUser();
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Section>();
            return user.AddSection(Entity, role.Entity);
        }
    }
}