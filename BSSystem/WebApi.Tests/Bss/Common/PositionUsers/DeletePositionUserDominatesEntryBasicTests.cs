﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Common.PositionUsers
{
    [TestClass]
    public class DeletePositionUserDominatesEntryBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<PositionUser, Entry>
    {
        private UserGenerator _userGenerator;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.User.Details.Assignment.Common_Position.Path;

        protected override Uri Target => AddressProvider.Get((AssignmentsController c) => c.Set(
            StorableHelper.GetTableKey<User>(),
            _userGenerator.Entity.Id,
            StorableHelper.GetTableKey<Position>(),
            null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<IEnumerable<RoleAssignmentPersistenceViewModel>, ValidationResult>(
                Target,
                Enumerable.Empty<RoleAssignmentPersistenceViewModel>(),
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected override PositionUser CreateEntityToModify()
        {
            _userGenerator = CurrentOrganizationGenerator.CreateUser();
            var positionGenerator = CurrentOrganizationGenerator.CreatePosition();
            return _userGenerator.AddPosition(positionGenerator.Entity);
        }

        protected override Entry CreateSlaveEntity()
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
            var roleGenerator = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>();
            topicGenerator.AddUser(_userGenerator.Entity, roleGenerator.Entity);
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();

            activityGenerator.AddPosition(Entity.Position);

            var entry = activityGenerator.AddEntry(topicGenerator.Entity, _userGenerator.Entity);
            return entry;
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((PositionUser x) => x.User, Entity.User, OperationToken);
        }
    }
}