using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.ActivityKinds
{
    [TestClass]
    public class GetListActivityKindBasicTests : BasicGetListScopedEntityTests<ActivityKind, ActivityKindViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.ActivityKind.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((ActivityKindController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<ActivityKind> CreateEntities()
        {
            var entities = new List<ActivityKind>();

            var first = CurrentOrganizationGenerator.CreateActivityKind().Entity;
            entities.Add(first);

            var second = CurrentOrganizationGenerator.CreateActivityKind().Entity;
            entities.Add(second);

            return entities;
        }

        protected override void ShouldMatch(ActivityKindViewModel actual, ActivityKind expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}