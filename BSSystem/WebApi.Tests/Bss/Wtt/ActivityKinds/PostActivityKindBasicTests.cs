using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.ActivityKinds
{
    [TestClass]
    public sealed class PostActivityKindBasicTests
        : BasicPostScopedVersionedEntityTests<ActivityKind, ActivityKindViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.ActivityKind.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((ActivityKindController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override ActivityKind CreateEntityToModify(OrganizationGenerator generator)
        {
            var targetGenerator = generator.CreateActivityKind(
                (u) =>
                {
                    u.DateEnd = DateTime.Today;
                    u.DateStart = DateTime.Today.AddDays(-3);
                    u.DisplayName = "DisplayName";
                    u.Name = "Name";
                });

            return targetGenerator.Entity;
        }

        protected override ActivityKindViewModel CreateModel()
        {
            var model = new ActivityKindViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = DateTime.Today.AddDays(4),
                DateStart = DateTime.Today.AddDays(-3),
                DisplayName = "DN",
                Name = "N"
            };
            return model;
        }

        protected override void VerifyPersistence(ActivityKind entity, OrganizationGenerator organizationGenerator)
        {
            entity.ShouldBeEqual(Model, organizationGenerator);
        }
    }
}