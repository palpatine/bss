﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public class DeleteActivityDominatesActivitySectionBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<Activity, ActivitySection>
    {
        protected ActivityGenerator EntityGenerator => CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Activity.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((ActivityController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateEntityToModify()
        {
            return EntityGenerator.Entity;
        }

        protected override ActivitySection CreateSlaveEntity()
        {
            var section = CurrentOrganizationGenerator.CreateSection();
            return section.AddActivity(Entity);
        }
    }
}