﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public sealed class JunctionLockActivityPositionsBasicTests : BasicScopedJunctionLockTests<Activity, IActivity, Position>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Activity.Details.Assignment.Common_Position.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Position CreateRelatedEntity()
        {
            return CurrentOrganizationGenerator.CreatePosition().Entity;
        }

        protected override IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<Activity> activityGenerator,
            Position relatedEntity)
        {
            var masterGenerator = (ActivityGenerator)activityGenerator;
            return masterGenerator.AddPosition(relatedEntity);
        }

        protected override IGenerator<Activity> MasterEntityGenerator(OrganizationGenerator organizationGenerator)
        {
            var activityKind = organizationGenerator.CreateActivityKind();
            return activityKind.CreateActivity();
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((ActivityController c) => c.JunctionLock(id, RelatedTableKey));
        }
    }
}