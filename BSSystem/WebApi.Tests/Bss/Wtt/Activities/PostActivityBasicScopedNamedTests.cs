﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public sealed class PostActivityBasicScopedNamedTests
        : BasicPostScopedNamedEntityTests<Activity, ActivityViewModel>
    {
        private IActivityKind _activityKind;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Activity.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((ActivityController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateEntityToModify(OrganizationGenerator generator)
        {
            var targetGenerator = generator.CreateActivityKind().CreateActivity();
            _activityKind = targetGenerator.Entity.ActivityKind;
            return targetGenerator.Entity;
        }

        protected override ActivityViewModel CreateModel()
        {
            var model = new ActivityViewModel
            {
                Id = Entity?.Id ?? 0,
                DateStart = DateTime.Today,
                DisplayName = "DN",
                Name = "N",
                ActivityKind = new NamedViewModel
                {
                    Id = _activityKind.Id
                }
            };
            return model;
        }
    }
}