using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public sealed class PostActivityTopicsBasicTests
        : BasicPostScopedAssignmentJunctionRelationTests<Activity, Topic, ActivityTopic, AssignmentPersistenceViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Activity.Details.Assignment.Wtt_Topic.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure).Entity;
        }

        protected override Topic CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure).Entity;
        }

        protected override ActivityTopic RelateEntities(
            Activity master,
            Topic related,
            Action<IDatedEntity> configure)
        {
            var relation = Generator.Find<ActivityGenerator>(master).AddTopic(related);
            configure(relation);
            return relation;
        }

        protected override void Verify(ActivityTopic actual, AssignmentPersistenceViewModel expected)
        {
            actual.ShouldBeEqual(expected, x => x.Second, CurrentOrganizationGenerator);
        }
    }
}