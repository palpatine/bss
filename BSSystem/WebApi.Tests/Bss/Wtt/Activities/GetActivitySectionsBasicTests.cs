using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public sealed class GetActivitySectionsBasicTests : BasicScopedAssignmentJunctionRelationTests<Activity, Section, ActivitySection, JobRoleAssignmentViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Activity.Details.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure).Entity;
        }

        protected override Section CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateSection(configure).Entity;
        }

        protected override void RelateEntities(
            Activity master,
            Section related,
            Action<IDatedEntity> configure)
        {
            Generator.Find<ActivityGenerator>(master).AddSection(related, configure);
        }

        protected override void Verify(JobRoleAssignmentViewModel actual, ActivitySection expected)
        {
            actual.ShouldBeEqual(expected, x => x.Second, CurrentOrganizationGenerator);
        }
    }
}