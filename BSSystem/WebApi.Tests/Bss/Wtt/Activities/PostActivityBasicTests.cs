using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public sealed class PostActivityBasicTests
        : BasicPostScopedVersionedEntityTests<Activity, ActivityViewModel>
    {
        private ActivityKindGenerator _activityKind;

        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Activity.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((ActivityController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateEntityToModify(OrganizationGenerator generator)
        {
            _activityKind = generator.CreateActivityKind(x => x.DateStart = DateTime.Today.AddDays(-3));
            var targetGenerator = _activityKind.CreateActivity(
                (u) =>
                {
                    u.DateEnd = DateTime.Today;
                    u.DateStart = DateTime.Today.AddDays(-3);
                    u.DisplayName = "DisplayName";
                    u.Name = "Name";
                });

            return targetGenerator.Entity;
        }

        protected override ActivityViewModel CreateModel()
        {
            var model = new ActivityViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = DateTime.Today.AddDays(4),
                DateStart = DateTime.Today.AddDays(-3),
                DisplayName = "DN",
                Name = "N",
                ActivityKind = new NamedViewModel { Id = _activityKind.Entity.Id }
            };
            return model;
        }

        protected override void PrepareForCreate(OrganizationGenerator organizationGenerator)
        {
            _activityKind = organizationGenerator.CreateActivityKind(
                x =>
                {
                    x.DateStart = DateTime.Today.AddDays(-3);
                });
        }

        protected override void VerifyPersistence(Activity entity, OrganizationGenerator organizationGenerator)
        {
            entity.ShouldBeEqual(Model, organizationGenerator);
        }
    }
}