﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public class PostActivityDominatesActivitySectionBasicTests
        : BasicPostEntityDominatesSlaveDependencyTests<Activity, ActivityViewModel, ActivitySection>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Activity.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((ActivityController a) => a.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateActivityKind(masterDateRange).CreateActivity(masterDateRange).Entity;
        }

        protected override ActivitySection CreateSlaveEntity(Activity master, Action<IDatedEntity> masterDateRange, Action<IDatedEntity> slaveDateRange)
        {
            var section = CurrentOrganizationGenerator.CreateSection(masterDateRange).Entity;
            var slave = Generator.Find<ActivityGenerator>(master).AddSection(section);
            slaveDateRange(slave);
            return slave;
        }

        protected override ActivityViewModel GetViewModel(DateTime dateStart, DateTime? dateEnd)
        {
            return new ActivityViewModel
            {
                Id = Entity.Id,
                DateStart = dateStart,
                DateEnd = dateEnd,
                Name = NextRandomValue(),
                DisplayName = NextRandomValue(),
                ActivityKind = new NamedViewModel
                {
                    Id = Entity.ActivityKind.Id
                }
            };
        }
    }
}