﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public sealed class ActivityLockBasicTests
        : BasicScopedLockTests<Activity, IActivity>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Activity.Details.Write.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateEntityToLock()
        {
            var activityKind = CurrentOrganizationGenerator.CreateActivityKind();
            var target = activityKind.CreateActivity().Entity;
            return target;
        }

        protected override Activity CreateEntityToLockInOtherOrganization()
        {
            var otherOrganization = Generator.CreateOrganization();
            var activityKind = otherOrganization.CreateActivityKind();
            var target = activityKind.CreateActivity().Entity;
            return target;
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((ActivityController c) => c.Lock(id));
        }
    }
}