﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Activities
{
    [TestClass]
    public class PostActivityDependsOnActivityKindBasicTests
        : BasicPostScopedEntityDependsOnVersionedEntityTests<Activity, ActivityViewModel, ActivityKind, IActivityKind>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Activity.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((ActivityController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override ActivityKind CreateDependencyEntity(OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreateActivityKind().Entity;
        }

        protected override Activity CreateEntityToModify(
            OrganizationGenerator organizationGenerator,
            ActivityKind master,
            Action<Activity> configure)
        {
            return organizationGenerator.Find<ActivityKindGenerator>(master).CreateActivity(configure).Entity;
        }

        protected override ActivityViewModel CreateModel(
            ActivityKind master)
        {
            return new ActivityViewModel
            {
                Id = Entity?.Id ?? 0,
                DisplayName = NextRandomValue(),
                Name = NextRandomValue(),
                DateStart = DateTime.Today,
                ActivityKind = new ActivityViewModel
                {
                    Id = master.Id
                }
            };
        }
    }
}