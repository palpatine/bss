using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public sealed class PostTopicBasicTests
        : BasicPostScopedVersionedEntityTests<Topic, TopicViewModel>
    {
        private TopicKindGenerator _topicKind;

        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((TopicController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateEntityToModify(OrganizationGenerator generator)
        {
            _topicKind = generator.CreateTopicKind(x => x.DateStart = DateTime.Today.AddDays(-3));
            var targetGenerator = _topicKind.CreateTopic(
                (u) =>
                {
                    u.DateEnd = DateTime.Today;
                    u.DateStart = DateTime.Today.AddDays(-3);
                    u.DisplayName = "DisplayName";
                    u.Name = "Name";
                });

            return targetGenerator.Entity;
        }

        protected override TopicViewModel CreateModel()
        {
            var model = new TopicViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = DateTime.Today.AddDays(4),
                DateStart = DateTime.Today.AddDays(-3),
                DisplayName = "DN",
                Name = "N",
                TopicKind = new NamedViewModel { Id = _topicKind.Entity.Id }
            };
            return model;
        }

        protected override void PrepareForCreate(OrganizationGenerator organizationGenerator)
        {
            _topicKind = organizationGenerator.CreateTopicKind(
                x =>
                {
                    x.DateStart = DateTime.Today.AddDays(-3);
                });
        }

        protected override void VerifyPersistence(Topic entity, OrganizationGenerator organizationGenerator)
        {
            entity.ShouldBeEqual(Model, organizationGenerator);
        }
    }
}