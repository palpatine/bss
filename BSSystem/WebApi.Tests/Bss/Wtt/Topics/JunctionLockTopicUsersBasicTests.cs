﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public sealed class JunctionLockTopicUsersBasicTests : BasicScopedJunctionLockTests<Topic, ITopic, User>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Details.Assignment.Common_User.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateRelatedEntity()
        {
            return CurrentOrganizationGenerator.CreateUser().Entity;
        }

        protected override IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<Topic> activityGenerator,
            User relatedEntity)
        {
            var masterGenerator = (TopicGenerator)activityGenerator;
            var roleDefinition = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity;
            TopicUser topicUser = null;
            masterGenerator.AddUser(relatedEntity, roleDefinition, x => topicUser = x);
            return topicUser;
        }

        protected override IGenerator<Topic> MasterEntityGenerator(OrganizationGenerator organizationGenerator)
        {
            var topicKind = organizationGenerator.CreateTopicKind();
            return topicKind.CreateTopic();
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((TopicController c) => c.JunctionLock(id, RelatedTableKey));
        }
    }
}