using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public sealed class GetTopicUsersBasicTests : BasicScopedAssignmentJunctionRelationTests<Topic, User, TopicUser, JobRoleAssignmentViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Details.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure).Entity;
        }

        protected override User CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override void RelateEntities(
            Topic master,
            User related,
            Action<IDatedEntity> configure)
        {
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure);
            Generator.Find<TopicGenerator>(master).AddUser(related, role.Entity, configure);
        }

        protected override void Verify(JobRoleAssignmentViewModel actual, TopicUser expected)
        {
            actual.ShouldBeEqual(expected, x => x.Second, CurrentOrganizationGenerator);
        }
    }
}