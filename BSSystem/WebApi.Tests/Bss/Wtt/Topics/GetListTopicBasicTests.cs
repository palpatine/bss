using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public class GetListTopicBasicTests : BasicGetListScopedEntityTests<Topic, TopicViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((TopicController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<Topic> CreateEntities()
        {
            var entities = new List<Topic>();
            var first = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic(
                 (u) =>
                 {
                     u.DateEnd = DateTime.Today.AddDays(3);
                 });
            entities.Add(first.Entity);

            var second = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
            entities.Add(second.Entity);

            return entities;
        }

        protected override void ShouldMatch(TopicViewModel actual, Topic expected)
        {
            actual.DateEnd.ShouldBeEqual(expected.DateEnd);
            actual.DateStart.ShouldBeEqual(expected.DateStart);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.TopicKind.Id.ShouldBeEqual(expected.TopicKind.Id);
        }
    }
}