using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public sealed class PostTopicActivitiesBasicTests
        : BasicPostScopedAssignmentJunctionRelationTests<Topic, Activity, ActivityTopic, AssignmentPersistenceViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Details.Assignment.Wtt_Activity.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure).Entity;
        }

        protected override Activity CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure).Entity;
        }

        protected override ActivityTopic RelateEntities(
            Topic master,
            Activity related,
            Action<IDatedEntity> configure)
        {
            var relation = Generator.Find<TopicGenerator>(master).AddActivity(related);
            configure(relation);
            return relation;
        }

        protected override void Verify(ActivityTopic actual, AssignmentPersistenceViewModel expected)
        {
            actual.ShouldBeEqual(expected, x => x.First, CurrentOrganizationGenerator);
        }
    }
}