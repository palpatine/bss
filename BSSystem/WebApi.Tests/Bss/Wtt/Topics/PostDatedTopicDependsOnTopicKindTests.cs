﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public class PostDatedTopicDependsOnTopicKindTests : BasicPostScopedEntityDependsOnVersionedEntityTests<Topic, TopicViewModel, TopicKind, ITopicKind>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Topic.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((TopicController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override TopicKind CreateDependencyEntity(OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreateTopicKind().Entity;
        }

        protected override Topic CreateEntityToModify(
            OrganizationGenerator organizationGenerator,
            TopicKind master,
            Action<Topic> configure)
        {
            return organizationGenerator.Find<TopicKindGenerator>(master).CreateTopic(configure).Entity;
        }

        protected override TopicViewModel CreateModel(TopicKind master)
        {
            return new TopicViewModel
            {
                Id = Entity?.Id ?? 0,
                DisplayName = NextRandomValue(),
                Name = NextRandomValue(),
                DateStart = Entity?.DateStart ?? DateTime.Today,
                DateEnd = Entity?.DateEnd,
                TopicKind = new TopicViewModel
                {
                    Id = master.Id
                }
            };
        }
    }
}