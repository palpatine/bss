﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public sealed class PostTopicUsersBasicTests
        : BasicPostScopedAssignmentJunctionRelationTests<Topic, User, TopicUser, RoleAssignmentPersistenceViewModel>
    {
        private RoleDefinition _role;

        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Details.Assignment.Common_User.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateMaster(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure).Entity;
        }

        protected override RoleAssignmentPersistenceViewModel CreateModel(TopicUser relation)
        {
            var model = base.CreateModel(relation);
            model.RoleId = relation.RoleDefinition.Id;
            return model;
        }

        protected override RoleAssignmentPersistenceViewModel CreateModel()
        {
            var model = base.CreateModel();
            model.RoleId = _role.Id;
            return model;
        }

        protected override User CreateRelated(Action<IDatedEntity> configure)
        {
            return CurrentOrganizationGenerator.CreateUser(configure).Entity;
        }

        protected override void PrepareForCreate()
        {
            _role = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity;
        }

        protected override TopicUser RelateEntities(
            Topic master,
            User related,
            Action<IDatedEntity> configure)
        {
            var relation = Generator.Find<TopicGenerator>(master).AddUser(related, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity);
            configure(relation);
            return relation;
        }

        protected override void Verify(TopicUser actual, RoleAssignmentPersistenceViewModel expected)
        {
            actual.ShouldBeEqual(expected, x => x.Second, CurrentOrganizationGenerator);
        }
    }
}