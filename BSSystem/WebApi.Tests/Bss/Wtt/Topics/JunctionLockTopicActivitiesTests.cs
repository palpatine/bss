﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public sealed class JunctionLockTopicActivitiesTests : BasicScopedJunctionLockTests<Topic, ITopic, Activity>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Details.Assignment.Wtt_Activity.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateRelatedEntity()
        {
            return CurrentOrganizationGenerator.CreateActivityKind().CreateActivity().Entity;
        }

        protected override IAssignmentJunctionEntity CreateRelationEntityToLock(
            IGenerator<Topic> activityGenerator,
            Activity relatedEntity)
        {
            var masterGenerator = (TopicGenerator)activityGenerator;
            return masterGenerator.AddActivity(relatedEntity);
        }

        protected override IGenerator<Topic> MasterEntityGenerator(OrganizationGenerator organizationGenerator)
        {
            var topicKind = organizationGenerator.CreateTopicKind();
            return topicKind.CreateTopic();
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((TopicController c) => c.JunctionLock(id, RelatedTableKey));
        }
    }
}