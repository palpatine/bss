﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public class DeleteTopicDominatesTopicUserBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<Topic, TopicUser>
    {
        protected TopicGenerator EntityGenerator => CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Topic.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((TopicController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateEntityToModify()
        {
            return EntityGenerator.Entity;
        }

        protected override TopicUser CreateSlaveEntity()
        {
            var user = CurrentOrganizationGenerator.CreateUser();
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>();
            return user.AddTopic(Entity, role.Entity);
        }
    }
}