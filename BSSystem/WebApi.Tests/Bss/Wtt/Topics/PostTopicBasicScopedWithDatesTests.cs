using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public sealed class PostTopicBasicScopedWithDatesTests : BasicPostScopedWithDatesEntityTests<Topic, TopicViewModel>
    {
        private ITopicKind _topicKind;
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((TopicController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateEntityToModify(
            OrganizationGenerator generator,
            Action<IDatedEntity> configureMaster,
            Action<IDatedEntity> configureSlave,
            DateTime otherDate)
        {
            var targetGenerator = generator.CreateTopicKind(configureMaster).CreateTopic(configureSlave);
            _topicKind = targetGenerator.Entity.TopicKind;
            return targetGenerator.Entity;
        }

        protected override TopicViewModel CreateModel(
            DateTime dateStart,
            DateTime? dateEnd,
            DateTime otherDate)
        {
            var model = new TopicViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = dateEnd,
                DateStart = dateStart,
                DisplayName = "DN",
                Name = "N",
                Comment = "C",
                TopicKind = new NamedViewModel
                {
                    Id = _topicKind.Id
                }
            };
            return model;
        }

        protected override void PrepareForCreate(
            OrganizationGenerator generator,
            Action<IDatedEntity> configure)
        {
            _topicKind = generator.CreateTopicKind(configure).Entity;
        }

        protected override void Verify(
            OrganizationGenerator generator,
            Topic actual,
            TopicViewModel expected)
        {
            actual.ShouldBeEqual(expected, generator);
        }
    }
}