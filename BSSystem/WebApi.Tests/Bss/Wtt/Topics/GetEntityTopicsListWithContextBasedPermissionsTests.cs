﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public class GetEntityTopicsListWithContextBasedPermissionsTests : BasicGetListWithContextBasedPermissionsTests<Topic, ITopic, TopicViewModel, TopicUser, ITopicUser>
    {
        public override string Permission => PermissionsWtt.Topic.Read.Path;

        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override Uri Target
        {
            get { return AddressProvider.Get((TopicController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override TopicUser AssociateUser(
            UserGenerator currentUserGenerator,
            IGenerator<Topic> generator,
            IRoleDefinition roleDefinition)
        {
            var entityGenerator = (TopicGenerator)generator;
            TopicUser topicUser = null;
            entityGenerator.AddUser(currentUserGenerator.Entity, roleDefinition, x => topicUser = x);
            return topicUser;
        }

        protected override IGenerator<Topic> CreateEntity(Action<IDatedEntity> configure)
        {
            var kind = CurrentOrganizationGenerator.CreateTopicKind(configure);
            var topic = kind.CreateTopic(configure);
            return topic;
        }

        protected override void ShouldMatch(
            TopicViewModel viewModel,
            Topic current)
        {
            viewModel.ShouldBeEqual(current, CurrentOrganizationGenerator);
        }
    }
}