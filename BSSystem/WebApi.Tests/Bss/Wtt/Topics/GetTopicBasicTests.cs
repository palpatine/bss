using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public class GetTopicBasicTests : BasicGetScopedEntityTests<Topic, TopicViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Topic.Details.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider.Get((TopicController x) => x.Get(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateEntity()
        {
            var topicKind = CurrentOrganizationGenerator.CreateTopicKind();
            var generator = topicKind.CreateTopic(
                          (s) =>
                          {
                              s.DateEnd = DateTime.Today.AddDays(3);
                          });

            return generator.Entity;
        }

        protected override void VerifySuccessResult(
            TopicViewModel actual,
            Topic expected)
        {
            actual.DateStart.ShouldBeEqual(expected.DateStart);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.DateEnd.ShouldBeEqual(expected.DateEnd);
        }
    }
}