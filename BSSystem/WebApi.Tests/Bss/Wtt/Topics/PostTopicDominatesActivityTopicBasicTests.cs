﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Topics
{
    [TestClass]
    public class PostTopicDominatesActivityTopicBasicTests
        : BasicPostEntityDominatesSlaveDependencyTests<Topic, TopicViewModel, ActivityTopic>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Topic.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((TopicController a) => a.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateTopicKind(masterDateRange).CreateTopic(masterDateRange).Entity;
        }

        protected override ActivityTopic CreateSlaveEntity(Topic master, Action<IDatedEntity> masterDateRange, Action<IDatedEntity> slaveDateRange)
        {
            var activity = CurrentOrganizationGenerator.CreateActivityKind(masterDateRange).CreateActivity(masterDateRange).Entity;
            var slave = Generator.Find<TopicGenerator>(master).AddActivity(activity);
            slaveDateRange(slave);
            return slave;
        }

        protected override TopicViewModel GetViewModel(DateTime dateStart, DateTime? dateEnd)
        {
            return new TopicViewModel
            {
                Id = Entity.Id,
                DateStart = dateStart,
                DateEnd = dateEnd,
                Name = NextRandomValue(),
                DisplayName = NextRandomValue(),
                TopicKind = new NamedViewModel
                {
                    Id = Entity.TopicKind.Id
                }
            };
        }
    }
}