﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.TopicUsers
{
    [TestClass]
    public class PostTopicUserDominatesEntryBasicTests
        : BasicPostEntityDominatesEntryTests<TopicUser, AssignmentPersistenceViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsCommon.User.Details.Assignment.Wtt_Topic.Path;

        protected override Uri Target => AddressProvider.Get((AssignmentsController c) => c.Set(
            StorableHelper.GetTableKey<User>(),
            UserGenerator.Entity.Id,
            StorableHelper.GetTableKey<Topic>(),
            null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<IEnumerable<AssignmentPersistenceViewModel>, ValidationResult>(
                Target,
                new[] { Model },
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected override TopicUser CreateEntityToModifyWithRequiredJunctions(Action<IDatedEntity> masterDateRange)
        {
            ActivityGenerator.AddTopic(TopicGenerator.Entity, masterDateRange);

            var topicRole = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(masterDateRange).Entity;
            var entity = UserGenerator.AddTopic(TopicGenerator.Entity, topicRole);

            masterDateRange(entity);
            return entity;
        }

        protected override AssignmentPersistenceViewModel CreateModelToModify(
            DateTime dateStart,
            DateTime? dateEnd)
        {
            return new AssignmentPersistenceViewModel
            {
                DateEnd = dateEnd,
                DateStart = dateStart,
                Id = Entity.Id,
                ElementId = TopicGenerator.Entity.Id
            };
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((TopicUser x) => x.User, Entity.User, OperationToken);
        }
    }
}