﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.TopicUsers
{
    [TestClass]
    public class DeleteTopicUserDominatesEntryBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<TopicUser, Entry>
    {
        private TopicGenerator _topicGenerator;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Topic.Details.Assignment.Common_User.Path;

        protected override Uri Target => AddressProvider.Get((AssignmentsController c) => c.Set(
            StorableHelper.GetTableKey<Topic>(),
            _topicGenerator.Entity.Id,
            StorableHelper.GetTableKey<User>(),
            null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<IEnumerable<RoleAssignmentPersistenceViewModel>, ValidationResult>(
                Target,
                Enumerable.Empty<RoleAssignmentPersistenceViewModel>(),
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected override TopicUser CreateEntityToModify()
        {
            _topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
            var userGenerator = CurrentOrganizationGenerator.CreateUser();
            var roleGenerator = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>();
            return _topicGenerator.AddUser(userGenerator.Entity, roleGenerator.Entity);
        }

        protected override Entry CreateSlaveEntity()
        {
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
            activityGenerator.AddTopic(Entity.Topic);

            var entry = activityGenerator.AddEntry(Entity.Topic, Entity.User);
            return entry;
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((TopicUser x) => x.Topic, Entity.Topic, OperationToken);
        }
    }
}