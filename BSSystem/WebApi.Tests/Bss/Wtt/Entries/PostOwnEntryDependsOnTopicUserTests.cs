﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOwnEntryDependsOnTopicUserTests : BasicPostEntryDependsOnVersionedEntityTests<TopicUser, ITopicUser>
    {
        private Activity _activity;
        private Topic _topic;
        private User _user;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Own.Write.Path;

        protected override string RelationName => StorableHelper.GetEntityName(typeof(Topic));

        protected override Uri Target => AddressProvider.Get((OwnEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override TopicUser CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            _user = organization == CurrentOrganizationGenerator ? CurrentUserGenerator.Entity : organization.CreateUser().Entity;
            configure(_user);
            _user.DateEnd = null;
            var topicGenerator = organization.CreateTopicKind(configure).CreateTopic(configure);
            _topic = topicGenerator.Entity;
            var topicUser = topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
            return topicUser;
        }

        protected override Entry CreateEntity(
            TopicUser master,
            Action<IDatedEntity> configure)
        {
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            _activity = activityGenerator.Entity;
            var sectionGenerator = CurrentOrganizationGenerator.CreateSection(configure);
            sectionGenerator.AddUser(
                _user,
                CurrentOrganizationGenerator.CreateRoleDefinition<Section>(configure).Entity,
               configure);
            sectionGenerator.AddActivity(_activity, configure);
            return activityGenerator.AddEntry(_topic, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            TopicUser master)
        {
            return new EntryViewModel
            {
                Date = date,
                Activity = new NamedViewModel
                {
                    Id = _activity.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic?.Id ?? master.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
        }

        protected override void PrepareForCreate(
            Action<IDatedEntity> configure,
            TopicUser master)
        {
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            _activity = activityGenerator.Entity;

            UserGenerator userGenerator;
            if (master != null)
            {
                userGenerator = Generator.Find<UserGenerator>(_user);
            }
            else
            {
                userGenerator = CurrentUserGenerator;
                _user = userGenerator.Entity;
                configure(_user);
            }

            var sectionGenerator = CurrentOrganizationGenerator.CreateSection(configure);
            sectionGenerator.AddUser(userGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Section>().Entity, configure);
            sectionGenerator.AddActivity(_activity, configure);
        }
    }
}