﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOwnEntryTests : BasePostVersionedEntityTests<Entry, EntryViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Entry.Details.Own.Write.Path;

        protected override Uri Target => AddressProvider.Get((OwnEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        [TestMethod]
        public async Task UserCanCreateEntryIfHeHasAccessToActivityFromActivityPositionRelationAsync()
        {
            Topic topic = null;
            Activity activity = null;

            await CreateTestsAsync(
                () =>
                {
                    var positionGenerator = CurrentOrganizationGenerator.CreatePosition();
                    positionGenerator.AddUser(CurrentUserGenerator.Entity);
                    var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
                    topic = topicGenerator.Entity;
                    topicGenerator.AddUser(CurrentUserGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
                    var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
                    activity = activityGenerator.Entity;
                    positionGenerator.AddActivity(activityGenerator.Entity);
                },
                () =>
                {
                    var model = new EntryViewModel
                    {
                        Date = DateTime.Today,
                        User = new NamedViewModel
                        {
                            Id = CurrentUserGenerator.Entity.Id
                        },
                        Topic = new NamedViewModel
                        {
                            Id = topic.Id
                        },
                        Activity = new NamedViewModel
                        {
                            Id = activity.Id
                        }
                    };

                    return model;
                },
                x => Task.Delay(0));
        }

        [TestMethod]
        public async Task UserCanCreateEntryIfHeHasAccessToActivityFromActivitySectionRelationAsync()
        {
            Topic topic = null;
            Activity activity = null;

            await CreateTestsAsync(
                () =>
                {
                    var sectionGenerator = CurrentOrganizationGenerator.CreateSection();
                    sectionGenerator.AddUser(CurrentUserGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Section>().Entity);
                    var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
                    topic = topicGenerator.Entity;
                    topicGenerator.AddUser(CurrentUserGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
                    var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
                    activity = activityGenerator.Entity;
                    sectionGenerator.AddActivity(activityGenerator.Entity);
                },
                () =>
                {
                    var model = new EntryViewModel
                    {
                        Date = DateTime.Today,
                        User = new NamedViewModel
                        {
                            Id = CurrentUserGenerator.Entity.Id
                        },
                        Topic = new NamedViewModel
                        {
                            Id = topic.Id
                        },
                        Activity = new NamedViewModel
                        {
                            Id = activity.Id
                        }
                    };

                    return model;
                },
                x => Task.Delay(0));
        }

        [TestMethod]
        public async Task UserCanCreateEntryIfHeHasAccessToActivityFromActivityTopicRelationAsync()
        {
            Topic topic = null;
            Activity activity = null;

            await CreateTestsAsync(
                () =>
                {
                    var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
                    topic = topicGenerator.Entity;
                    topicGenerator.AddUser(CurrentUserGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
                    var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
                    activity = activityGenerator.Entity;
                    activityGenerator.AddTopic(topicGenerator.Entity);
                },
                () =>
                {
                    var model = new EntryViewModel
                    {
                        Date = DateTime.Today,
                        User = new NamedViewModel
                        {
                            Id = CurrentUserGenerator.Entity.Id
                        },
                        Topic = new NamedViewModel
                        {
                            Id = topic.Id
                        },
                        Activity = new NamedViewModel
                        {
                            Id = activity.Id
                        }
                    };

                    return model;
                },
                x => Task.Delay(0));
        }

        [TestMethod]
        public async Task UserCannotCreateEntryIfHeDoesNotHaveAccessToActivityAsync()
        {
            Topic topic = null;
            Activity activity = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
                    topic = topicGenerator.Entity;
                    topicGenerator.AddUser(CurrentUserGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
                    var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
                    activity = activityGenerator.Entity;
                    return Task.Delay(0);
                },
                () =>
                {
                    var model = new EntryViewModel
                    {
                        Date = DateTime.Today,
                        User = new NamedViewModel
                        {
                            Id = CurrentUserGenerator.Entity.Id
                        },
                        Topic = new NamedViewModel
                        {
                            Id = topic.Id
                        },
                        Activity = new NamedViewModel
                        {
                            Id = activity.Id
                        }
                    };

                    return model;
                },
                x => { x.Errors.Single().ErrorCode.ShouldBeEqual("Entry_Activity_NotFound"); });
        }

        [TestMethod]
        public async Task UserCannotCreateEntryIfHeIsNotRelatedWithTopicAsync()
        {
            Topic topic = null;
            Activity activity = null;

            await CreateValidationErrorTestAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
                    topic = topicGenerator.Entity;
                    var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
                    activity = activityGenerator.Entity;
                    var sectionGenerator = CurrentOrganizationGenerator.CreateSection();
                    sectionGenerator.AddUser(CurrentUserGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Section>().Entity);
                    sectionGenerator.AddActivity(activity);
                    return Task.Delay(0);
                },
                () =>
                {
                    var model = new EntryViewModel
                    {
                        Date = DateTime.Today,
                        User = new NamedViewModel
                        {
                            Id = CurrentUserGenerator.Entity.Id
                        },
                        Topic = new NamedViewModel
                        {
                            Id = topic.Id
                        },
                        Activity = new NamedViewModel
                        {
                            Id = activity.Id
                        }
                    };

                    return model;
                },
                x => { x.Errors.Single().ErrorCode.ShouldBeEqual("Entry_Topic_NotFound"); });
        }
    }
}