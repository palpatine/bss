﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class GetOtherEntryBasicTests : BasicGetScopedEntityTests<Entry, EntryViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Other.Path;

        protected override Uri Target => AddressProvider.Get((OtherEntryController c) => c.Get(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Entry CreateEntity()
        {
            var topic = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
            var activity = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
            var user = CurrentOrganizationGenerator.CreateUser();
            topic.AddUser(user.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
            topic.AddActivity(activity.Entity);
            var entry = activity.AddEntry(topic.Entity, user.Entity);
            entry.Date = DateTime.Today;
            return entry;
        }

        protected override void VerifySuccessResult(
            EntryViewModel actual,
            Entry expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}