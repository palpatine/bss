﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class GetOtherEntryBasicScopedWithDatesTests : BasicGetScopedWithDatesTests<Entry, EntryViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Entry.Details.Other.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider.Get((OtherEntryController x) => x.Get(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Entry CreateEntity(Action<IDatedEntity> configure, DateTime otherDate)
        {
            var topic = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activity = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            var user = CurrentOrganizationGenerator.CreateUser(configure);
            topic.AddUser(user.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            topic.AddActivity(activity.Entity, configure);

            var entry = activity.AddEntry(topic.Entity, user.Entity);
            entry.Date = otherDate;
            return entry;
        }

        protected override void Verify(EntryViewModel actual, Entry expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}