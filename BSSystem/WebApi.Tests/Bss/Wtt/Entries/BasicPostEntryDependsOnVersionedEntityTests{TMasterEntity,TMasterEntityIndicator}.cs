﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    public abstract class BasicPostEntryDependsOnVersionedEntityTests<TMasterEntity, TMasterEntityIndicator>
        : BasePostVersionedEntityTests<Entry, EntryViewModel>
        where TMasterEntity : VersionedEntity, IDatedEntity, TMasterEntityIndicator, new()
        where TMasterEntityIndicator : IStorable
    {
        protected string EntityName { get; } = StorableHelper.GetEntityName(typeof(Entry));

        protected virtual string RelationName => StorableHelper.GetEntityName(typeof(TMasterEntity));

        [TestMethod]
        public async Task GivenEntityWithInfiniteMasterUserCanUpdateDateToBeEqualThenMasterStartMastersSpanAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    Entity = CreateEntityToModify(
                        master,
                        x => { x.Date = DateTime.Today.AddDays(1); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today, master);

                    return model;
                },
                x =>
                {
                    x.Date.ShouldBeEqual(DateTime.Today);
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task GivenEntityWithInfiniteMasterUserCanUpdateDateToBeGreaterThenMasterStartMastersSpanAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    Entity = CreateEntityToModify(
                        master,
                        x => { x.Date = DateTime.Today.AddDays(1); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(5), master);

                    return model;
                },
                x =>
                {
                    x.Date.ShouldBeEqual(DateTime.Today.AddDays(5));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task GivenEntityWithLimitedMasterUserCannotUpdateDateAfterMastersDateEndAsync()
        {
            TMasterEntity master = null;

            await GivenEntityWithLimitedMasterUserCannotUpdateDateAfterMastersDateEndAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity(
                        x => { });
                    master.DateEnd = DateTime.Today.AddDays(3);
                    Entity = CreateEntityToModify(
                        master,
                        x => { x.Date = DateTime.Today.AddDays(1); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(4), master);
                    return model;
                });
        }

        [TestMethod]
        public async Task GivenEntityWithLimitedMasterUserCanUpdateDateToBeEqualDateEndSpanAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    master.DateEnd = DateTime.Today.AddDays(4);
                    Entity = CreateEntityToModify(
                        master,
                        x => { x.Date = DateTime.Today.AddDays(1); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(4), master);

                    return model;
                },
                x =>
                {
                    x.Date.ShouldBeEqual(DateTime.Today.AddDays(4));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task GivenEntityWithLimitedMasterUserCanUpdateDateWithinMastersSpanAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    master.DateEnd = DateTime.Today.AddDays(4);
                    Entity = CreateEntityToModify(
                        master,
                        x => { x.Date = DateTime.Today.AddDays(1); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(4), master);

                    return model;
                },
                x =>
                {
                    x.Date.ShouldBeEqual(DateTime.Today.AddDays(4));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task GivenEntityWithMasterUserCannotUpdateDateBeforeMastersDateStartAsync()
        {
            TMasterEntity master = null;

            await GivenEntityWithMasterUserCannotUpdateDateBeforeMastersDateStartAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity(
                        x => { x.DateStart = DateTime.Today.AddDays(-1); });
                    master.DateStart = DateTime.Today;
                    Entity = CreateEntityToModify(master, x => x.Date = DateTime.Today.AddDays(2), x => { x.DateStart = DateTime.Today.AddDays(-1); });
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(-1), master);

                    return model;
                });
        }

        [TestMethod]
        public async Task UserCanCreateEntityIfProvidesDateInRangeOfMastersDatesAndMasterIsInfiniteAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    PrepareForCreate(x => { }, master);
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), master);

                    return model;
                },
                x =>
                {
                    x.Date.ShouldBeEqual(DateTime.Today.AddDays(1));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task UserCanCreateEntityIfProvidesDateInRangeOfMastersDatesAndMasterIsLimitedAsync()
        {
            TMasterEntity master = null;

            await CreateTestsAsync(
                () =>
                {
                    master = CreateDependencyEntity(
                        x => { });
                    master.DateEnd = DateTime.Today.AddDays(3);
                    PrepareForCreate(x => { }, master);
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(1), master);

                    return model;
                },
                x =>
                {
                    x.Date.ShouldBeEqual(DateTime.Today.AddDays(1));
                    return Task.Delay(0);
                });
        }

        [TestMethod]
        public async Task UserCannotCreateEntityIfMasterDoesNotExistAsync()
        {
            await UserCannotCreateEntityIfMasterDoesNotExistAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    PrepareForCreate(x => { }, null);
                },
                () =>
                {
                    var model = CreateModel(
                        DateTime.Today.AddDays(3),
                        new TMasterEntity
                        {
                            Id = 493028
                        });

                    return model;
                });
        }

        [TestMethod]
        public async Task UserCannotCreateEntityIfMasterIsDeletedAsync()
        {
            TMasterEntity master = null;
            await UserCannotCreateEntityIfMasterIsDeletedAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity(
                        x => { });
                    master.DateEnd = DateTime.Today.AddDays(3);
                    PrepareForCreate(x => { }, master);
                    return Task.Delay(0);
                },
                async () =>
                {
                    await DeleteMasterAsync(master);
                    var model = CreateModel(DateTime.Today.AddDays(1), master);
                    return model;
                });
        }

        [TestMethod]
        public async Task UserCannotCreateEntityIfProvidesDateAfterMastersDateEndAsync()
        {
            TMasterEntity master = null;

            await UserCannotCreateEntityIfProvidesDateAfterMastersDateEndAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity(
                        x => { });
                    master.DateEnd = DateTime.Today.AddDays(2);
                    PrepareForCreate(x => { }, master);
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(3), master);

                    return model;
                });
        }

        [TestMethod]
        public async Task UserCannotCreateEntityIfProvidesDateBeforeMastersDateStartAsync()
        {
            TMasterEntity master = null;

            await UserCannotCreateEntityIfProvidesDateBeforeMastersDateStartAsync(
                () =>
                {
                    CurrentUserGenerator.AddPermission(Permission);
                    master = CreateDependencyEntity(x => { x.DateStart = DateTime.Today.AddDays(-1); });
                    master.DateStart = DateTime.Today;
                    PrepareForCreate(x => { x.DateStart = DateTime.Today.AddDays(-1); }, master);
                },
                () =>
                {
                    var model = CreateModel(DateTime.Today.AddDays(-1), master);

                    return model;
                });
        }

        protected abstract TMasterEntity CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure);

        protected abstract Entry CreateEntity(TMasterEntity master, Action<IDatedEntity> configure);

        protected abstract EntryViewModel CreateModel(
            DateTime date,
            TMasterEntity master);

        protected virtual async Task DeleteMasterAsync(
            TMasterEntity entity)
        {
            await DeleteEntityAsync(entity);
        }

        protected virtual async Task GivenEntityWithLimitedMasterUserCannotUpdateDateAfterMastersDateEndAsync(
                    Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        protected virtual async Task GivenEntityWithMasterUserCannotUpdateDateBeforeMastersDateStartAsync(
            Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        protected virtual void PrepareForCreate(Action<IDatedEntity> configure, TMasterEntity master)
        {
        }

        protected virtual async Task UserCannotCreateEntityIfMasterDoesNotExistAsync(
            Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        protected virtual async Task UserCannotCreateEntityIfMasterIsDeletedAsync(
            Func<Task> prepare,
            Func<Task<EntryViewModel>> createModelAsync)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModelAsync,
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        protected virtual async Task UserCannotCreateEntityIfProvidesDateAfterMastersDateEndAsync(
                                     Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        protected virtual async Task UserCannotCreateEntityIfProvidesDateBeforeMastersDateStartAsync(
            Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Single().ErrorCode.ShouldBeEqual(EntityName + "_" + RelationName + "_NotFound"));
        }

        private TMasterEntity CreateDependencyEntity(Action<IDatedEntity> configure)
        {
            return CreateDependencyEntity(CurrentOrganizationGenerator, configure);
        }

        private Entry CreateEntityToModify(TMasterEntity master, Action<Entry> configureEntry, Action<IDatedEntity> configure = null)
        {
            Entry entry = CreateEntity(master, configure ?? (x => { }));
            configureEntry(entry);
            return entry;
        }
    }
}