﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOwnEntryDependsOnTopicTests : BasicPostEntryDependsOnVersionedEntityTests<Topic, ITopic>
    {
        private Activity _activity;
        private User _user;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Own.Write.Path;

        protected override Uri Target => AddressProvider.Get((OwnEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Topic CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            return organization.CreateTopicKind(configure).CreateTopic(configure).Entity;
        }

        protected override Entry CreateEntity(
            Topic master,
            Action<IDatedEntity> configure)
        {
            var userGenerator = CurrentUserGenerator;
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            var topicGenerator = CurrentOrganizationGenerator.Find<TopicGenerator>(master);
            var sectionGenerator = CurrentOrganizationGenerator.CreateSection(configure);
            _user = userGenerator.Entity;
            sectionGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Section>(configure).Entity, configure);
            configure(_user);
            topicGenerator.AddUser(
                _user,
                CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity,
                x =>
                {
                    x.DateStart = master.DateStart;
                    x.DateEnd = master.DateEnd;
                });
            _activity = activityGenerator.Entity;
            sectionGenerator.AddActivity(_activity, configure);
            return activityGenerator.AddEntry(master, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            Topic master)
        {
            return new EntryViewModel
            {
                Date = date,
                User = new NamedViewModel
                {
                    Id = _user.Id
                },
                Topic = new NamedViewModel
                {
                    Id = master.Id
                },
                Activity = new NamedViewModel
                {
                    Id = _activity.Id
                }
            };
        }

        protected override async Task DeleteMasterAsync(
          Topic entity)
        {
            var topicUser = Generator.Find<TopicGenerator>(entity).UserAssignments.Single();
            await DeleteEntityAsync(topicUser);
            await DeleteEntityAsync(entity);
        }

        protected override void PrepareForCreate(
                    Action<IDatedEntity> configure,
            Topic master)
        {
            var userGenerator = CurrentUserGenerator;
            _user = userGenerator.Entity;
            configure(_user);
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            _activity = activityGenerator.Entity;
            var sectionGenerator = CurrentOrganizationGenerator.CreateSection(configure);
            sectionGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Section>().Entity, configure);
            sectionGenerator.AddActivity(_activity, configure);

            if (master != null)
            {
                var topicGenerator = Generator.Find<TopicGenerator>(master);
                topicGenerator.AddUser(
                    _user,
                    CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity,
                    x =>
                    {
                        x.DateStart = master.DateStart;
                        x.DateEnd = master.DateEnd;
                    });
            }
        }
    }
}