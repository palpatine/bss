﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOwnEntryDependsOnPositionUserTests : BasicPostEntryDependsOnVersionedEntityTests<PositionUser, IPositionUser>
    {
        private Activity _activity;
        private Topic _topic;
        private User _user;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Own.Write.Path;

        protected override string RelationName => StorableHelper.GetEntityName(typeof(Activity));

        protected override Uri Target => AddressProvider.Get((OwnEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override PositionUser CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            var position = organization.CreatePosition(configure);
            _user = organization == CurrentOrganizationGenerator ? CurrentUserGenerator.Entity : organization.CreateUser().Entity;
            configure(_user);
            var positionUser = position.AddUser(_user);
            configure(positionUser);
            return positionUser;
        }

        protected override Entry CreateEntity(
            PositionUser master,
            Action<IDatedEntity> configure)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            _activity = activityGenerator.Entity;
            var positionGenerator = Generator.Find<PositionGenerator>(master.Position);
            _topic = topicGenerator.Entity;
            activityGenerator.AddPosition(positionGenerator.Entity);
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            positionGenerator.AddUser(
                _user,
                x =>
                {
                    x.DateStart = master.DateStart;
                    x.DateEnd = master.DateEnd;
                });
            return activityGenerator.AddEntry(_topic, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            PositionUser master)
        {
            return new EntryViewModel
            {
                Date = date,
                Activity = new NamedViewModel
                {
                    Id = _activity?.Id ?? master.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
        }

        protected override void PrepareForCreate(
            Action<IDatedEntity> configure,
            PositionUser master)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            if (master != null)
            {
                var positionGenerator = Generator.Find<PositionGenerator>(master.Position);
                positionGenerator.AddActivity(activityGenerator.Entity, configure);
            }
            else
            {
                var userGenerator = CurrentOrganizationGenerator.CreateUser(configure);
                configure(userGenerator.Entity);
                _user = userGenerator.Entity;
            }

            _topic = topicGenerator.Entity;
            configure(_user);
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
        }

        protected override async Task UserCannotCreateEntityIfMasterDoesNotExistAsync(Action prepare, Func<EntryViewModel> createModel)
        {
            await ApiCallTestAsync(
                () =>
                {
                    prepare();
                    return Task.Delay(0);
                },
                createModel,
                HttpStatusCode.BadRequest);
        }
    }
}