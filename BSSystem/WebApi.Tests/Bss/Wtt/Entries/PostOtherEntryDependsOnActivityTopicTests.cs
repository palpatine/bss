﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOtherEntryDependsOnActivityTopicTests : BasicPostEntryDependsOnVersionedEntityTests<ActivityTopic, IActivityTopic>
    {
        private Activity _activity;
        private Topic _topic;
        private User _user;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Other.Write.Path;

        protected override string RelationName => StorableHelper.GetEntityName(typeof(Activity));

        protected override Uri Target => AddressProvider.Get((OtherEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override ActivityTopic CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            _topic = organization.CreateTopicKind(configure).CreateTopic(configure).Entity;
            var activityGenerator = organization.CreateActivityKind(configure).CreateActivity(configure);
            _activity = activityGenerator.Entity;
            var activityTopic = activityGenerator.AddTopic(_topic);
            return activityTopic;
        }

        protected override Entry CreateEntity(
            ActivityTopic master,
            Action<IDatedEntity> configure)
        {
            var topicGenerator = Generator.Find<TopicGenerator>(master.Topic);
            var activityGenerator = Generator.Find<ActivityGenerator>(master.Activity);
            _topic = topicGenerator.Entity;
            _user = CurrentOrganizationGenerator.CreateUser(configure).Entity;
            topicGenerator.AddUser(
                _user,
                CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity,
                configure);
            return activityGenerator.AddEntry(_topic, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            ActivityTopic master)
        {
            return new EntryViewModel
            {
                Date = date,
                Activity = new NamedViewModel
                {
                    Id = _activity?.Id ?? master.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic?.Id ?? master.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
        }

        protected override void PrepareForCreate(
            Action<IDatedEntity> configure,
            ActivityTopic master)
        {
            var userGenerator = CurrentOrganizationGenerator.CreateUser(configure);
            configure(userGenerator.Entity);
            if (master != null)
            {
                var topicGenerator = Generator.Find<TopicGenerator>(_topic);
                topicGenerator.AddUser(userGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            }
            else
            {
                var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
                _topic = topicGenerator.Entity;
                topicGenerator.AddUser(userGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            }

            _user = userGenerator.Entity;
            configure(_user);
        }
    }
}