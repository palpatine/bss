﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOtherEntryDependsOnActivitySectionTests : BasicPostEntryDependsOnVersionedEntityTests<ActivitySection, IActivitySection>
    {
        private Activity _activity;
        private Topic _topic;
        private User _user;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Other.Write.Path;

        protected override string RelationName => StorableHelper.GetEntityName(typeof(Activity));

        protected override Uri Target => AddressProvider.Get((OtherEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override ActivitySection CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            var section = organization.CreateSection(configure);
            var activityGenerator = organization.CreateActivityKind(configure).CreateActivity(configure);
            _activity = activityGenerator.Entity;
            var activitySection = activityGenerator.AddSection(section.Entity);
            configure(activitySection);
            return activitySection;
        }

        protected override Entry CreateEntity(
            ActivitySection master,
            Action<IDatedEntity> configure)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activityGenerator = Generator.Find<ActivityGenerator>(master.Activity);
            var sectionGenerator = Generator.Find<SectionGenerator>(master.Section);
            _topic = topicGenerator.Entity;
            _user = CurrentOrganizationGenerator.CreateUser(configure).Entity;
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            var roleGenerator = CurrentOrganizationGenerator.CreateRoleDefinition<Section>(
                x =>
                {
                    x.DateStart = master.DateStart;
                    x.DateEnd = master.DateEnd;
                });
            sectionGenerator.AddUser(
                _user,
               roleGenerator.Entity,
                x =>
            {
                x.DateStart = master.DateStart;
                x.DateEnd = master.DateEnd;
            });
            return activityGenerator.AddEntry(_topic, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            ActivitySection master)
        {
            return new EntryViewModel
            {
                Date = date,
                Activity = new NamedViewModel
                {
                    Id = _activity?.Id ?? master.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
        }

        protected override void PrepareForCreate(
            Action<IDatedEntity> configure,
            ActivitySection master)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var userGenerator = CurrentOrganizationGenerator.CreateUser(configure);
            configure(userGenerator.Entity);
            if (master != null)
            {
                var sectionGenerator = Generator.Find<SectionGenerator>(master.Section);
                sectionGenerator.AddUser(userGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Section>().Entity, configure);
            }

            _topic = topicGenerator.Entity;
            _user = userGenerator.Entity;
            configure(_user);
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
        }
    }
}