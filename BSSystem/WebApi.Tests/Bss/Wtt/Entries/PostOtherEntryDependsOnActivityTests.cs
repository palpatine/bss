﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOtherEntryDependsOnActivityTests : BasicPostEntryDependsOnVersionedEntityTests<Activity, IActivity>
    {
        private Topic _topic;
        private User _user;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Other.Write.Path;

        protected override Uri Target => AddressProvider.Get((OtherEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Activity CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            return organization.CreateActivityKind(configure).CreateActivity(configure).Entity;
        }

        protected override Entry CreateEntity(
            Activity master,
            Action<IDatedEntity> configure)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activityGenerator = Generator.Find<ActivityGenerator>(master);
            activityGenerator.AddTopic(
                topicGenerator.Entity,
                x =>
                {
                    x.DateStart = master.DateStart;
                    x.DateEnd = master.DateEnd;
                });
            _topic = topicGenerator.Entity;
            _user = CurrentOrganizationGenerator.CreateUser(configure).Entity;
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            return activityGenerator.AddEntry(_topic, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            Activity master)
        {
            return new EntryViewModel
            {
                Date = date,
                Activity = new NamedViewModel
                {
                    Id = master.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
        }

        protected override async Task DeleteMasterAsync(
          Activity entity)
        {
            var activityTopic = Generator.Find<ActivityGenerator>(entity).TopicAssignments.Single();
            await DeleteEntityAsync(activityTopic);
            await DeleteEntityAsync(entity);
        }

        protected override void PrepareForCreate(
                    Action<IDatedEntity> configure,
            Activity master)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure);

            if (master != null)
            {
                topicGenerator.AddActivity(
                    master,
                    x =>
                    {
                        x.DateStart = master.DateStart;
                        x.DateEnd = master.DateEnd;
                    });
            }

            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            activityGenerator.AddTopic(topicGenerator.Entity, configure);
            _topic = topicGenerator.Entity;
            var userGenerator = CurrentOrganizationGenerator.CreateUser(configure);
            topicGenerator.AddUser(userGenerator.Entity, role.Entity, configure);
            _user = userGenerator.Entity;
        }
    }
}