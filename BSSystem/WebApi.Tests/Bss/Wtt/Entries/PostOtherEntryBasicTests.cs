using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public sealed class PostOtherEntryBasicTests : BasicPostScopedVersionedEntityTests<Entry, EntryViewModel>
    {
        private Activity _activity;
        private Topic _topic;
        private User _user;
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Entry.Details.Other.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((OtherEntryController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Entry CreateEntityToModify(OrganizationGenerator organizationGenerator)
        {
            var topicGenerator = organizationGenerator.CreateTopicKind().CreateTopic();
            _topic = topicGenerator.Entity;
            var activityGenerator = organizationGenerator.CreateActivityKind().CreateActivity();
            _activity = activityGenerator.Entity;
            var positionGenerator = organizationGenerator.CreatePosition();
            var userGenerator = CurrentOrganizationGenerator.CreateUser();
            _user = userGenerator.Entity;
            topicGenerator.AddUser(userGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
            positionGenerator.AddUser(userGenerator.Entity);
            activityGenerator.AddPosition(positionGenerator.Entity);
            return activityGenerator.AddEntry(topicGenerator.Entity, userGenerator.Entity);
        }

        protected override EntryViewModel CreateModel()
        {
            var model = new EntryViewModel
            {
                Id = Entity?.Id ?? 0,
                Date = DateTime.Today,
                Comment = "C",
                Minutes = 50,
                Activity = new NamedViewModel
                {
                    Id = _activity.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };

            return model;
        }

        protected override void PrepareForCreate(OrganizationGenerator organizationGenerator)
        {
            var topicGenerator = organizationGenerator.CreateTopicKind().CreateTopic();
            _topic = topicGenerator.Entity;
            var activityGenerator = organizationGenerator.CreateActivityKind().CreateActivity();
            _activity = activityGenerator.Entity;
            var positionGenerator = organizationGenerator.CreatePosition();
            var userGenerator = CurrentOrganizationGenerator.CreateUser();
            _user = userGenerator.Entity;
            topicGenerator.AddUser(userGenerator.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
            positionGenerator.AddActivity(activityGenerator.Entity);
            positionGenerator.AddUser(userGenerator.Entity);
        }

        protected override void VerifyPersistence(Entry actual, OrganizationGenerator organizationGenerator)
        {
            actual.ShouldBeEqual(Model, organizationGenerator);
        }
    }
}