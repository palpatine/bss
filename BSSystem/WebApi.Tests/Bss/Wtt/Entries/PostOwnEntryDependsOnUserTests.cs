﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOwnEntryDependsOnUserTests : BasicPostEntryDependsOnVersionedEntityTests<User, IUser>
    {
        private Activity _activity;
        private Topic _topic;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Own.Write.Path;

        protected override Uri Target => AddressProvider.Get((OwnEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            var user = CurrentOrganizationGenerator == organization ? CurrentUserGenerator.Entity : organization.CreateUser().Entity;
            return user;
        }

        protected override Entry CreateEntity(
            User master,
            Action<IDatedEntity> configure)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            activityGenerator.AddTopic(topicGenerator.Entity, configure);
            topicGenerator.AddUser(master, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, x =>
                {
                    x.DateStart = master.DateStart;
                    x.DateEnd = master.DateEnd;
                });
            _topic = topicGenerator.Entity;
            _activity = activityGenerator.Entity;
            return activityGenerator.AddEntry(_topic, master);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            User master)
        {
            return new EntryViewModel
            {
                Date = date,
                User = new NamedViewModel
                {
                    Id = master.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic.Id
                },
                Activity = new NamedViewModel
                {
                    Id = _activity.Id
                }
            };
        }

        protected override async Task DeleteMasterAsync(
           User entity)
        {
            var topicUser = Generator.Find<TopicGenerator>(_topic).UserAssignments.Single();
            await DeleteEntityAsync(topicUser);
            await DeleteEntityAsync(entity);
        }

        protected override async Task GivenEntityWithLimitedMasterUserCannotUpdateDateAfterMastersDateEndAsync(
                    Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Select(z => z.ErrorCode)
                    .OrderBy(z => z)
                    .AllCorrespondingElementsShouldBeEqual(new[] { "Entry_Activity_NotFound", "Entry_Topic_NotFound", "Entry_User_NotFound" }));
        }

        protected override async Task GivenEntityWithMasterUserCannotUpdateDateBeforeMastersDateStartAsync(
            Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Select(z => z.ErrorCode)
                    .OrderBy(z => z)
                    .AllCorrespondingElementsShouldBeEqual(new[] { "Entry_Activity_NotFound", "Entry_Topic_NotFound", "Entry_User_NotFound" }));
        }

        protected override void PrepareForCreate(
            Action<IDatedEntity> configure,
            User master)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var role = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure);

            if (master != null)
            {
                topicGenerator.AddUser(
                    master,
                    role.Entity,
                    x =>
                    {
                        x.DateStart = master.DateStart;
                        x.DateEnd = master.DateEnd;
                    });
            }

            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            activityGenerator.AddTopic(topicGenerator.Entity, configure);
            _topic = topicGenerator.Entity;
            _activity = activityGenerator.Entity;
        }

        protected override async Task UserCannotCreateEntityIfMasterDoesNotExistAsync(
            Action prepare,
            Func<EntryViewModel> createModel)
        {
            await ApiCallTestAsync(
                () =>
                {
                    prepare();
                    return Task.Delay(0);
                },
                createModel,
                HttpStatusCode.BadRequest);
        }

        protected override async Task UserCannotCreateEntityIfMasterIsDeletedAsync(
           Func<Task> prepare,
           Func<Task<EntryViewModel>> createModelAsync)
        {
            await ApiCallTestAsync(
                prepare,
                createModelAsync,
                HttpStatusCode.Unauthorized);
        }

        protected override async Task UserCannotCreateEntityIfProvidesDateAfterMastersDateEndAsync(
                    Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Select(z => z.ErrorCode)
                    .OrderBy(z => z)
                    .AllCorrespondingElementsShouldBeEqual(new[] { "Entry_Activity_NotFound", "Entry_Topic_NotFound", "Entry_User_NotFound" }));
        }

        protected override async Task UserCannotCreateEntityIfProvidesDateBeforeMastersDateStartAsync(
            Action prepare,
            Func<EntryViewModel> createModel)
        {
            await CreateValidationErrorTestAsync(
                prepare,
                createModel,
                x => x.Errors.Select(z => z.ErrorCode)
                    .OrderBy(z => z)
                    .AllCorrespondingElementsShouldBeEqual(new[] { "Entry_Activity_NotFound", "Entry_Topic_NotFound", "Entry_User_NotFound" }));
        }
    }
}