using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public sealed class PostOtherEntryBasicScopedWithDatesTests : BasicPostScopedWithDatesEntityTests<Entry, EntryViewModel>
    {
        private Activity _activity;
        private Topic _topic;
        private User _user;
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Entry.Details.Other.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((OtherEntryController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Entry CreateEntityToModify(
            OrganizationGenerator generator,
            Action<IDatedEntity> configureMaster,
            Action<IDatedEntity> configureSlave,
            DateTime otherDate)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configureMaster).CreateTopic(configureMaster);
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configureMaster).CreateActivity(configureMaster);
            topicGenerator.AddActivity(activityGenerator.Entity, configureMaster);
            var positionGenerator = CurrentOrganizationGenerator.CreatePosition(configureMaster);
            activityGenerator.AddPosition(positionGenerator.Entity, configureMaster);
            _topic = topicGenerator.Entity;
            _user = CurrentOrganizationGenerator.CreateUser(configureMaster).Entity;
            _activity = activityGenerator.Entity;
            configureMaster(_user);
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configureMaster).Entity, configureMaster);
            positionGenerator.AddUser(_user, configureMaster);
            return activityGenerator.AddEntry(_topic, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime dateStart,
            DateTime? dateEnd,
            DateTime otherDate)
        {
            return new EntryViewModel
            {
                Date = otherDate,
                Activity = new NamedViewModel
                {
                    Id = _activity.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
        }

        protected override void PrepareForCreate(
            OrganizationGenerator generator,
            Action<IDatedEntity> configure)
        {
            var topicGenerator = generator.CreateTopicKind(configure).CreateTopic(configure);
            var userGenerator = generator.CreateUser(configure);
            var positionGenerator = generator.CreatePosition(configure);
            positionGenerator.AddUser(userGenerator.Entity, configure);
            var activityGenerator = generator.CreateActivityKind(configure).CreateActivity(configure);
            activityGenerator.AddPosition(positionGenerator.Entity, configure);
            topicGenerator.AddActivity(activityGenerator.Entity, configure);
            _topic = topicGenerator.Entity;
            _user = userGenerator.Entity;
            _activity = activityGenerator.Entity;
            configure(_user);
            topicGenerator.AddUser(_user, generator.CreateRoleDefinition<Topic>(configure).Entity, configure);
        }

        protected override void Verify(
            OrganizationGenerator generator,
            Entry actual,
            EntryViewModel expected)
        {
            actual.ShouldBeEqual(expected, generator);
        }
    }
}