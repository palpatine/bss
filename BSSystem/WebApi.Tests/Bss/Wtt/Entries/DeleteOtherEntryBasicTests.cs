﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public sealed class DeleteOtherEntryBasicTests : BasicDeleteScopedEntityTests<Entry>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
       {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Other.Write.Path;

        protected override Uri Target => AddressProvider.Get((OtherEntryController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Entry CreateEntityToModify(OrganizationGenerator organizationGenerator)
        {
            var topic = organizationGenerator.CreateTopicKind().CreateTopic();
            var activity = organizationGenerator.CreateActivityKind().CreateActivity();
            var user = organizationGenerator.CreateUser();
            topic.AddUser(user.Entity, organizationGenerator.CreateRoleDefinition<Topic>().Entity);
            topic.AddActivity(activity.Entity);
            return activity.AddEntry(topic.Entity, user.Entity);
        }
    }
}