﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public sealed class OwnEntryLockBasicTests
        : BasicScopedLockTests<Entry, IEntry>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.Entry.Details.Own.Write.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Entry CreateEntityToLock()
        {
            var topic = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
            var activity = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
            var user = CurrentUserGenerator;
            topic.AddUser(user.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>().Entity);
            topic.AddActivity(activity.Entity);

            var entry = activity.AddEntry(topic.Entity, user.Entity);
            entry.Date = DateTime.Today;
            return entry;
        }

        protected override Entry CreateEntityToLockInOtherOrganization()
        {
            var otherOrganization = Generator.CreateOrganization();
            var topic = otherOrganization.CreateTopicKind().CreateTopic();
            var activity = otherOrganization.CreateActivityKind().CreateActivity();
            var user = otherOrganization.CreateUser();
            topic.AddUser(user.Entity, otherOrganization.CreateRoleDefinition<Topic>().Entity);
            topic.AddActivity(activity.Entity);

            var entry = activity.AddEntry(topic.Entity, user.Entity);
            entry.Date = DateTime.Today;
            return entry;
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((OwnEntryController c) => c.Lock(id));
        }
    }
}