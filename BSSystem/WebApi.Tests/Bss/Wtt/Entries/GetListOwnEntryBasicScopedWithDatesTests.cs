﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class GetListOwnEntryBasicScopedWithDatesTests : BasicGetListScopedWithDatesTests<Entry, EntryViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Entry.Read.Own.Path;

        protected override Uri Target { get; } = AddressProvider.Get((OwnEntryController x) => x.Get());

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override Entry CreateEntity(
            Action<IDatedEntity> configure,
            DateTime otherDate)
        {
            var topic = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activity = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            var user = CurrentUserGenerator;
            topic.AddUser(user.Entity, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            topic.AddActivity(activity.Entity, configure);
            var entry = activity.AddEntry(topic.Entity, user.Entity);
            entry.Date = otherDate;
            return entry;
        }

        protected override void InitializeState()
        {
            base.InitializeState();
            CurrentUserGenerator.Entity.DateStart = DateTime.Today.AddMonths(-3);
        }

        protected override void Verify(
            EntryViewModel actual,
            Entry expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}