﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOwnEntryDependsOnSectionUserTests : BasicPostEntryDependsOnVersionedEntityTests<SectionUser, ISectionUser>
    {
        private Activity _activity;
        private Topic _topic;
        private User _user;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Own.Write.Path;

        protected override string RelationName => StorableHelper.GetEntityName(typeof(Activity));

        protected override Uri Target => AddressProvider.Get((OwnEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override SectionUser CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            var section = organization.CreateSection(configure);
            _user = organization == CurrentOrganizationGenerator ? CurrentUserGenerator.Entity : organization.CreateUser().Entity;
            configure(_user);
            var sectionUser = section.AddUser(_user, organization.CreateRoleDefinition<Section>(configure).Entity);
            configure(sectionUser);
            return sectionUser;
        }

        protected override Entry CreateEntity(
            SectionUser master,
            Action<IDatedEntity> configure)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            _activity = activityGenerator.Entity;
            var sectionGenerator = Generator.Find<SectionGenerator>(master.Section);
            _topic = topicGenerator.Entity;
            activityGenerator.AddSection(sectionGenerator.Entity);
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            var roleGenerator = CurrentOrganizationGenerator.CreateRoleDefinition<Section>(
                x =>
                {
                    x.DateStart = master.DateStart;
                    x.DateEnd = master.DateEnd;
                });
            sectionGenerator.AddUser(
                _user,
               roleGenerator.Entity,
                x =>
            {
                x.DateStart = master.DateStart;
                x.DateEnd = master.DateEnd;
            });
            return activityGenerator.AddEntry(_topic, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            SectionUser master)
        {
            return new EntryViewModel
            {
                Date = date,
                Activity = new NamedViewModel
                {
                    Id = _activity?.Id ?? master.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
        }

        protected override void PrepareForCreate(
            Action<IDatedEntity> configure,
            SectionUser master)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activityGenerator = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure);
            if (master != null)
            {
                var sectionGenerator = Generator.Find<SectionGenerator>(master.Section);
                sectionGenerator.AddActivity(activityGenerator.Entity, configure);
            }
            else
            {
                var userGenerator = CurrentUserGenerator;
                configure(userGenerator.Entity);
                _user = userGenerator.Entity;
            }

            _topic = topicGenerator.Entity;
            configure(_user);
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
        }
    }
}