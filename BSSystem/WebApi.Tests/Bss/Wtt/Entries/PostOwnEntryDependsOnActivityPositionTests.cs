﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.Entries
{
    [TestClass]
    public class PostOwnEntryDependsOnActivityPositionTests : BasicPostEntryDependsOnVersionedEntityTests<ActivityPosition, IActivityPosition>
    {
        private Activity _activity;
        private Topic _topic;
        private User _user;

        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.Entry.Details.Own.Write.Path;

        protected override string RelationName => StorableHelper.GetEntityName(typeof(Activity));

        protected override Uri Target => AddressProvider.Get((OwnEntryController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override ActivityPosition CreateDependencyEntity(
            OrganizationGenerator organization,
            Action<IDatedEntity> configure)
        {
            var position = organization.CreatePosition(configure).Entity;
            var activityGenerator = organization.CreateActivityKind(configure).CreateActivity(configure);
            _activity = activityGenerator.Entity;
            var activityPosition = activityGenerator.AddPosition(position);
            configure(activityPosition);
            return activityPosition;
        }

        protected override Entry CreateEntity(
            ActivityPosition master,
            Action<IDatedEntity> configure)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var activityGenerator = Generator.Find<ActivityGenerator>(master.Activity);
            var positionGenerator = Generator.Find<PositionGenerator>(master.Position);
            _topic = topicGenerator.Entity;
            _user = CurrentUserGenerator.Entity;
            configure(_user);
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
            positionGenerator.AddUser(_user, configure);
            return activityGenerator.AddEntry(_topic, _user);
        }

        protected override EntryViewModel CreateModel(
            DateTime date,
            ActivityPosition master)
        {
            return new EntryViewModel
            {
                Date = date,
                Activity = new NamedViewModel
                {
                    Id = _activity.Id
                },
                Topic = new NamedViewModel
                {
                    Id = _topic.Id
                },
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
        }

        protected override void PrepareForCreate(
            Action<IDatedEntity> configure,
            ActivityPosition master)
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind(configure).CreateTopic(configure);
            var userGenerator = CurrentUserGenerator;
            if (master != null)
            {
                var positionGenerator = Generator.Find<PositionGenerator>(master.Position);
                positionGenerator.AddUser(userGenerator.Entity, configure);
            }
            else
            {
                _activity = CurrentOrganizationGenerator.CreateActivityKind(configure).CreateActivity(configure).Entity;
            }

            _topic = topicGenerator.Entity;
            _user = userGenerator.Entity;
            configure(_user);
            topicGenerator.AddUser(_user, CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(configure).Entity, configure);
        }
    }
}