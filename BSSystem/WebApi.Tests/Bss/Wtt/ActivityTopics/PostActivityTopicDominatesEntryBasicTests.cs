﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.ActivityTopics
{
    [TestClass]
    public class PostActivityTopicDominatesEntryBasicTests
        : BasicPostEntityDominatesEntryTests<ActivityTopic, AssignmentPersistenceViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Activity.Details.Assignment.Wtt_Topic.Path;

        protected override Uri Target => AddressProvider.Get((AssignmentsController c) => c.Set(
            StorableHelper.GetTableKey<Activity>(),
            ActivityGenerator.Entity.Id,
            StorableHelper.GetTableKey<Topic>(),
            null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<IEnumerable<AssignmentPersistenceViewModel>, ValidationResult>(
                Target,
                new[] { Model },
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected override ActivityTopic CreateEntityToModifyWithRequiredJunctions(Action<IDatedEntity> masterDateRange)
        {
            var topicRole = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(masterDateRange).Entity;
            UserGenerator.AddTopic(TopicGenerator.Entity, topicRole, masterDateRange);

            var entity = ActivityGenerator.AddTopic(TopicGenerator.Entity);
            masterDateRange(entity);
            return entity;
        }

        protected override AssignmentPersistenceViewModel CreateModelToModify(
            DateTime dateStart,
            DateTime? dateEnd)
        {
            return new AssignmentPersistenceViewModel
            {
                DateEnd = dateEnd,
                DateStart = dateStart,
                Id = Entity.Id,
                ElementId = TopicGenerator.Entity.Id
            };
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((ActivityTopic x) => x.Activity, Entity.Activity, OperationToken);
        }
    }
}