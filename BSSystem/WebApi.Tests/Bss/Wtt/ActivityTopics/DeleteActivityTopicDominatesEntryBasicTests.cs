﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.ActivityTopics
{
    [TestClass]
    public class DeleteActivityTopicDominatesEntryBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<ActivityTopic, Entry>
    {
        private ActivityGenerator _activityGenerator;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Activity.Details.Assignment.Wtt_Topic.Path;

        protected override Uri Target => AddressProvider.Get((AssignmentsController c) => c.Set(
            StorableHelper.GetTableKey<Activity>(),
            _activityGenerator.Entity.Id,
            StorableHelper.GetTableKey<Topic>(),
            null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<IEnumerable<RoleAssignmentPersistenceViewModel>, ValidationResult>(
                Target,
                Enumerable.Empty<RoleAssignmentPersistenceViewModel>(),
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected override ActivityTopic CreateEntityToModify()
        {
            _activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
            return _activityGenerator.AddTopic(topicGenerator.Entity);
        }

        protected override Entry CreateSlaveEntity()
        {
            var topicGenerator = Generator.Find<TopicGenerator>(Entity.Topic);
            var roleGenerator = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>();
            var userGenerator = CurrentOrganizationGenerator.CreateUser();
            userGenerator.AddTopic(topicGenerator.Entity, roleGenerator.Entity);

            var entry = _activityGenerator.AddEntry(topicGenerator.Entity, userGenerator.Entity);
            return entry;
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((ActivityTopic x) => x.Activity, Entity.Activity, OperationToken);
        }
    }
}