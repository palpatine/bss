﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.ActivitySections
{
    [TestClass]
    public class PostActivitySectionDominatesEntryBasicTests
        : BasicPostEntityDominatesEntryTests<ActivitySection, AssignmentPersistenceViewModel>
    {
        private SectionGenerator _sectionGenerator;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Activity.Details.Assignment.Common_Section.Path;

        protected override Uri Target => AddressProvider.Get((AssignmentsController c) => c.Set(
            StorableHelper.GetTableKey<Activity>(),
            ActivityGenerator.Entity.Id,
            StorableHelper.GetTableKey<Section>(),
            null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<IEnumerable<AssignmentPersistenceViewModel>, ValidationResult>(
                Target,
                new[] { Model },
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected override ActivitySection CreateEntityToModifyWithRequiredJunctions(Action<IDatedEntity> masterDateRange)
        {
            var topicRole = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(masterDateRange).Entity;
            UserGenerator.AddTopic(TopicGenerator.Entity, topicRole, masterDateRange);

            var sectionRole = CurrentOrganizationGenerator.CreateRoleDefinition<Section>(masterDateRange).Entity;
            _sectionGenerator = CurrentOrganizationGenerator.CreateSection(masterDateRange);

            UserGenerator.AddSection(_sectionGenerator.Entity, sectionRole, masterDateRange);
            var entity = ActivityGenerator.AddSection(_sectionGenerator.Entity);
            masterDateRange(entity);
            return entity;
        }

        protected override AssignmentPersistenceViewModel CreateModelToModify(
            DateTime dateStart,
            DateTime? dateEnd)
        {
            return new AssignmentPersistenceViewModel
            {
                DateEnd = dateEnd,
                DateStart = dateStart,
                Id = Entity.Id,
                ElementId = _sectionGenerator.Entity.Id
            };
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((ActivitySection x) => x.Activity, Entity.Activity, OperationToken);
        }
    }
}