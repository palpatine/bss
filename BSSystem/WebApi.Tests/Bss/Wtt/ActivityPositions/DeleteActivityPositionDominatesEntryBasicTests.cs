﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.ActivityPositions
{
    [TestClass]
    public class DeleteActivityPositionDominatesEntryBasicTests
        : BasicDeleteVersionedEntityDominatesSlaveDependencyTests<ActivityPosition, Entry>
    {
        private ActivityGenerator _activityGenerator;
        private PositionGenerator _positionGenerator;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Activity.Details.Assignment.Common_Position.Path;

        protected override Uri Target => AddressProvider.Get((AssignmentsController c) => c.Set(
            StorableHelper.GetTableKey<Activity>(),
            _activityGenerator.Entity.Id,
            StorableHelper.GetTableKey<Position>(),
            null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<IEnumerable<RoleAssignmentPersistenceViewModel>, ValidationResult>(
                Target,
                Enumerable.Empty<RoleAssignmentPersistenceViewModel>(),
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected override ActivityPosition CreateEntityToModify()
        {
            _activityGenerator = CurrentOrganizationGenerator.CreateActivityKind().CreateActivity();
            _positionGenerator = CurrentOrganizationGenerator.CreatePosition();
            return _activityGenerator.AddPosition(_positionGenerator.Entity);
        }

        protected override Entry CreateSlaveEntity()
        {
            var topicGenerator = CurrentOrganizationGenerator.CreateTopicKind().CreateTopic();
            var roleGenerator = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>();
            var userGenerator = CurrentOrganizationGenerator.CreateUser();
            userGenerator.AddTopic(topicGenerator.Entity, roleGenerator.Entity);

            userGenerator.AddPosition(_positionGenerator.Entity);

            var entry = _activityGenerator.AddEntry(topicGenerator.Entity, userGenerator.Entity);
            return entry;
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((ActivityPosition x) => x.Activity, Entity.Activity, OperationToken);
        }
    }
}