﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.ActivityPositions
{
    [TestClass]
    public class PostActivityPositionDominatesEntryBasicTests
        : BasicPostEntityDominatesEntryTests<ActivityPosition, AssignmentPersistenceViewModel>
    {
        private PositionGenerator _positionGenerator;

        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.Activity.Details.Assignment.Common_Position.Path;

        protected override Uri Target => AddressProvider.Get((AssignmentsController c) => c.Set(
            StorableHelper.GetTableKey<Activity>(),
            ActivityGenerator.Entity.Id,
            StorableHelper.GetTableKey<Position>(),
            null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override async Task<ClientResponse<ValidationResult>> CallWebService()
        {
            return await Client.CallServiceAsync<IEnumerable<AssignmentPersistenceViewModel>, ValidationResult>(
                Target,
                new[] { Model },
                customHeaders: new Dictionary<string, string> { { "OperationToken", OperationToken } },
                credentials: Credentials);
        }

        protected override ActivityPosition CreateEntityToModifyWithRequiredJunctions(Action<IDatedEntity> masterDateRange)
        {
            var topicRole = CurrentOrganizationGenerator.CreateRoleDefinition<Topic>(masterDateRange).Entity;
            UserGenerator.AddTopic(TopicGenerator.Entity, topicRole, masterDateRange);

            _positionGenerator = CurrentOrganizationGenerator.CreatePosition(masterDateRange);
            UserGenerator.AddPosition(_positionGenerator.Entity, masterDateRange);
            var entity = ActivityGenerator.AddPosition(_positionGenerator.Entity);
            masterDateRange(entity);
            return entity;
        }

        protected override AssignmentPersistenceViewModel CreateModelToModify(
            DateTime dateStart,
            DateTime? dateEnd)
        {
            return new AssignmentPersistenceViewModel
            {
                DateEnd = dateEnd,
                DateStart = dateStart,
                Id = Entity.Id,
                ElementId = _positionGenerator.Entity.Id
            };
        }

        protected override void LockEntity()
        {
            Generator
                .CreateSession(CurrentUserGenerator.Entity)
                .AddJunctionLock((ActivityPosition x) => x.Activity, Entity.Activity, OperationToken);
        }
    }
}