using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.EntryLimitExclusions
{
    [TestClass]
    public class PostEntryLimitExclusionDependsOnUserTests
        : BasicPostScopedEntityDependsOnVersionedEntityTests<EntryLimitExclusion, EntryLimitExclusionViewModel, User, IUser>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
        {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission { get; } = PermissionsWtt.EntryLimitExclusion.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((EntryLimitExclusionController c) => c.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override User CreateDependencyEntity(
                    OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreateUser().Entity;
        }

        protected override EntryLimitExclusion CreateEntityToModify(
            OrganizationGenerator organizationGenerator,
            User master,
            Action<EntryLimitExclusion> configure)
        {
            EntryLimitExclusion entryLimitExclusion = null;
            organizationGenerator.AddEntryLimitExclusion(
                master,
                x =>
                {
                    configure(x);
                    entryLimitExclusion = x;
                });
            return entryLimitExclusion;
        }

        protected override EntryLimitExclusionViewModel CreateModel(User master)
        {
            return new EntryLimitExclusionViewModel
            {
                Id = Entity?.Id ?? 0,
                User = new NamedViewModel { Id = master.Id },
                PeriodStart = DateTime.Today,
                PeriodEnd = DateTime.Today.AddDays(3),
                DateStart = DateTime.Today
            };
        }
    }
}