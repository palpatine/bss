﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.EntryLimitExclusions
{
    [TestClass]
    public sealed class DeleteEntryLimitExclusionBasicTests : BasicDeleteScopedEntityTests<EntryLimitExclusion>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
       {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.EntryLimitExclusion.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((EntryLimitExclusionController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override EntryLimitExclusion CreateEntityToModify(OrganizationGenerator organizationGenerator)
        {
            var user = organizationGenerator.CreateUser();
            return organizationGenerator.AddEntryLimitExclusion(user.Entity);
        }
    }
}