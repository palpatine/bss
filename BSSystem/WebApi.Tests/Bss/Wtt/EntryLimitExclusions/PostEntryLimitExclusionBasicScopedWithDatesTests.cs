using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.EntryLimitExclusions
{
    [TestClass]
    public sealed class PostEntryLimitExclusionBasicScopedWithDatesTests
        : BasicPostScopedWithDatesEntityTests<EntryLimitExclusion, EntryLimitExclusionViewModel>
    {
        private User _user;
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.EntryLimitExclusion.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((EntryLimitExclusionController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override EntryLimitExclusion CreateEntityToModify(
            OrganizationGenerator generator,
            Action<IDatedEntity> configureMaster,
            Action<IDatedEntity> configureSlave,
            DateTime otherDate)
        {
            _user = CurrentOrganizationGenerator.CreateUser(configureMaster).Entity;
            var target = generator.AddEntryLimitExclusion(_user);
            configureSlave(target);
            target.PeriodStart = otherDate;
            target.PeriodEnd = otherDate;
            return target;
        }

        protected override EntryLimitExclusionViewModel CreateModel(
            DateTime dateStart,
            DateTime? dateEnd,
            DateTime otherDate)
        {
            var model = new EntryLimitExclusionViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = dateEnd,
                DateStart = dateStart,
                PeriodStart = otherDate,
                PeriodEnd = otherDate,
                User = new NamedViewModel
                {
                    Id = _user.Id
                }
            };
            return model;
        }

        protected override void PrepareForCreate(
            OrganizationGenerator organizationGenerator,
            Action<IDatedEntity> configure)
        {
            _user = organizationGenerator.CreateUser(configure).Entity;
        }

        protected override void Verify(
            OrganizationGenerator organizationGenerator,
            EntryLimitExclusion actual,
            EntryLimitExclusionViewModel expected)
        {
            actual.ShouldBeEqual(expected, organizationGenerator);
        }
    }
}