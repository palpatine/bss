using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.EntryLimitExclusions
{
    [TestClass]
    public class GetListEntryLimitExclusionBasicTests : BasicGetListScopedEntityTests<EntryLimitExclusion, EntryLimitExclusionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.EntryLimitExclusion.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((EntryLimitExclusionController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<EntryLimitExclusion> CreateEntities()
        {
            var entities = new List<EntryLimitExclusion>();
            var user = CurrentOrganizationGenerator.CreateUser();

            var first = CurrentOrganizationGenerator.AddEntryLimitExclusion(user.Entity);
            entities.Add(first);

            var second = CurrentOrganizationGenerator.AddEntryLimitExclusion(user.Entity);
            entities.Add(second);

            return entities;
        }

        protected override void ShouldMatch(EntryLimitExclusionViewModel actual, EntryLimitExclusion expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}