﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.EntryLimitExclusions
{
    [TestClass]
    public sealed class EntryLimitExclusionLockBasicTests
        : BasicScopedLockTests<EntryLimitExclusion, IEntryLimitExclusion>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.EntryLimitExclusion.Details.Write.Path;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override EntryLimitExclusion CreateEntityToLock()
        {
            var user = CurrentOrganizationGenerator.CreateUser();
            var target = CurrentOrganizationGenerator.AddEntryLimitExclusion(user.Entity);
            return target;
        }

        protected override EntryLimitExclusion CreateEntityToLockInOtherOrganization()
        {
            var otherOrganization = Generator.CreateOrganization();
            var user = otherOrganization.CreateUser();
            var target = otherOrganization.AddEntryLimitExclusion(user.Entity);
            return target;
        }

        protected override Uri Target(int id)
        {
            return AddressProvider
                .Get((EntryLimitExclusionController c) => c.Lock(id));
        }
    }
}