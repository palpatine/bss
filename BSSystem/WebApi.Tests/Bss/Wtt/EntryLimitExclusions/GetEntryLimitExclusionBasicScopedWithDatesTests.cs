﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.EntryLimitExclusions
{
    [TestClass]
    public class GetEntryLimitExclusionBasicScopedWithDatesTests : BasicGetScopedWithDatesTests<EntryLimitExclusion, EntryLimitExclusionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.EntryLimitExclusion.Details.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider.Get((EntryLimitExclusionController x) => x.Get(Entity.Id));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override EntryLimitExclusion CreateEntity(Action<IDatedEntity> configure, DateTime otherDate)
        {
            var target = CurrentOrganizationGenerator.AddEntryLimitExclusion(CurrentOrganizationGenerator.CreateUser(configure).Entity);
            target.PeriodStart = otherDate;
            target.PeriodEnd = otherDate;
            return target;
        }

        protected override void Verify(EntryLimitExclusionViewModel actual, EntryLimitExclusion expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}