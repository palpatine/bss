using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.EntryLimitExclusions
{
    [TestClass]
    public sealed class PostEntryLimitExclusionBasicTests
        : BasicPostScopedVersionedEntityTests<EntryLimitExclusion, EntryLimitExclusionViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.EntryLimitExclusion.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((EntryLimitExclusionController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override EntryLimitExclusion CreateEntityToModify(OrganizationGenerator organizationGenerator)
        {
            var target = CurrentOrganizationGenerator.AddEntryLimitExclusion(CurrentUserGenerator.Entity);
            return target;
        }

        protected override EntryLimitExclusionViewModel CreateModel()
        {
            return new EntryLimitExclusionViewModel
            {
                Id = Entity?.Id ?? 0,
                User = new NamedViewModel { Id = CurrentUserGenerator.Entity.Id },
                PeriodStart = DateTime.Today,
                PeriodEnd = DateTime.Today.AddDays(3),
                DateStart = DateTime.Today,
                DateEnd = DateTime.Today.AddDays(4)
            };
        }

        protected override void VerifyPersistence(EntryLimitExclusion actual, OrganizationGenerator organizationGenerator)
        {
            actual.ShouldBeEqual(Model, organizationGenerator);
        }
    }
}