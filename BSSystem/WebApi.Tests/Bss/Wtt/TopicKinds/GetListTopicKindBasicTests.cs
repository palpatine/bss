using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.TopicKinds
{
    [TestClass]
    public class GetListTopicKindBasicTests : BasicGetListScopedEntityTests<TopicKind, TopicKindViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.TopicKind.Read.Path;

        protected override Uri Target
        {
            get { return AddressProvider.Get((TopicKindController x) => x.Get()); }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override IEnumerable<TopicKind> CreateEntities()
        {
            var entities = new List<TopicKind>();

            var first = CurrentOrganizationGenerator.CreateTopicKind().Entity;
            entities.Add(first);

            var second = CurrentOrganizationGenerator.CreateTopicKind().Entity;
            entities.Add(second);

            return entities;
        }

        protected override void ShouldMatch(
            TopicKindViewModel actual,
            TopicKind expected)
        {
            actual.ShouldBeEqual(expected, CurrentOrganizationGenerator);
        }
    }
}