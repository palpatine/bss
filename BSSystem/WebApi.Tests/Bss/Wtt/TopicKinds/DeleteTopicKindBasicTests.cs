﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.TopicKinds
{
    [TestClass]
    public sealed class DeleteTopicKindBasicTests : BasicDeleteScopedEntityTests<TopicKind>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[]
       {
            ModulesProvider.Common,
            ModulesProvider.Wtt
        };

        protected override string Permission => PermissionsWtt.TopicKind.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((TopicKindController c) => c.Delete(Entity.Id));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override TopicKind CreateEntityToModify(OrganizationGenerator organizationGenerator)
        {
            return organizationGenerator.CreateTopicKind().Entity;
        }
    }
}