using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.TopicKinds
{
    [TestClass]
    public sealed class PostTopicKindBasicScopedWithDatesTests : BasicPostScopedWithDatesEntityTests<TopicKind, TopicKindViewModel>
    {
        protected override IEnumerable<ModuleMarker> Modules { get; } = new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission => PermissionsWtt.TopicKind.Details.Write.Path;

        protected override Uri Target
        {
            get
            {
                return AddressProvider
                    .Get((TopicKindController c) => c.Post(null));
            }
        }

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override TopicKind CreateEntityToModify(
            OrganizationGenerator generator,
            Action<IDatedEntity> configureMaster,
            Action<IDatedEntity> configureSlave,
            DateTime otherDate)
        {
            var targetGenerator = generator.CreateTopicKind(configureSlave);
            return targetGenerator.Entity;
        }

        protected override TopicKindViewModel CreateModel(
            DateTime dateStart,
            DateTime? dateEnd,
            DateTime otherDate)
        {
            var model = new TopicKindViewModel
            {
                Id = Entity?.Id ?? 0,
                DateEnd = dateEnd,
                DateStart = dateStart,
                DisplayName = "DN",
                Name = "N"
            };

            return model;
        }

        protected override void Verify(
            OrganizationGenerator generator,
            TopicKind actual,
            TopicKindViewModel expected)
        {
            actual.ShouldBeEqual(expected, generator);
        }
    }
}