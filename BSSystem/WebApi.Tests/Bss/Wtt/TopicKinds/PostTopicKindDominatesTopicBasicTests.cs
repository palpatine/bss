﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.WebApi.Tests.Bss.Automation;
using SolvesIt.BSSystem.WebApi.Tests.Environment;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests.Bss.Wtt.TopicKinds
{
    [TestClass]
    public class PostTopicKindDominatesTopicBasicTests : BasicPostEntityDominatesSlaveDependencyTests<TopicKind, TopicKindViewModel, Topic>
    {
        protected override IEnumerable<ModuleMarker> Modules => new[] { ModulesProvider.Common, ModulesProvider.Wtt };

        protected override string Permission { get; } = PermissionsWtt.TopicKind.Details.Write.Path;

        protected override Uri Target => AddressProvider.Get((TopicKindController a) => a.Post(null));

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            ClassInitialize = ClassInitializeAsync();
        }

        protected override TopicKind CreateEntityToModify(
            Action<IDatedEntity> masterDateRange)
        {
            return CurrentOrganizationGenerator.CreateTopicKind(masterDateRange).Entity;
        }

        protected override Topic CreateSlaveEntity(TopicKind master, Action<IDatedEntity> masterDateRange, Action<IDatedEntity> slaveDateRange)
        {
            return Generator.Find<TopicKindGenerator>(master).CreateTopic(slaveDateRange).Entity;
        }

        protected override TopicKindViewModel GetViewModel(DateTime dateStart, DateTime? dateEnd)
        {
            return new TopicKindViewModel
            {
                Id = Entity?.Id ?? 0,
                DateStart = dateStart,
                DateEnd = dateEnd,
                Name = NextRandomValue(),
                DisplayName = NextRandomValue()
            };
        }
    }
}