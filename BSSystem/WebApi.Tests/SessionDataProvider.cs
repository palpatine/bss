using System.Security.Principal;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests
{
    internal sealed class SessionDataProvider : ISessionDataProvider
    {
        private ISession _session;
        public IUser CurrentUser { get; set; }
        public IOrganization Scope { get; set; }
        public ISession Session { get; set; }

        public void Resolve(IPrincipal principal)
        {
        }

        public void ResetForAdmin()
        {
            Session = _session;
            CurrentUser = new UserMarker(1);
            Scope = new OrganizationMarker(1);
        }

        public void SetAdminSession(ISession session)
        {
            _session = session;
            ResetForAdmin();
        }
    }
}