﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration;
using SolvesIt.BSSystem.WebApi.Tests.StorageGeneration.Generators;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WebApi.Tests
{
    internal static class EntityAsserts
    {
        public static void ShouldBeEqual(
            this OrganizationViewModel actual,
            Organization expected,
            Company company)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.Name.ShouldBeEqual(company.Name);
            actual.FullName.ShouldBeEqual(company.FullName);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Fax.ShouldBeEqual(company.Fax);
            actual.CountryCode.ShouldBeEqual(company.CountryCode);
            actual.Email.ShouldBeEqual(company.Email);
            actual.OfficialNumber.ShouldBeEqual(company.OfficialNumber);
            actual.Phone.ShouldBeEqual(company.Phone);
            actual.StatisticNumber.ShouldBeEqual(company.StatisticNumber);
            actual.VatNumber.ShouldBeEqual(company.VatNumber);
            CompareModelDates(actual, expected);
        }

        public static void ShouldBeEqual(
            this User actual,
            UserViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.FirstName.ShouldBeEqual(expected.FirstName);
            actual.Surname.ShouldBeEqual(expected.Surname);
            actual.Location.ShouldBeEqual(expected.Location);
            actual.Mail.ShouldBeEqual(expected.Mail);
            actual.Phone.ShouldBeEqual(expected.Phone);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this UserViewModel actual,
            User expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.FirstName.ShouldBeEqual(expected.FirstName);
            actual.Surname.ShouldBeEqual(expected.Surname);
            actual.Location.ShouldBeEqual(expected.Location);
            actual.Mail.ShouldBeEqual(expected.Mail);
            actual.Phone.ShouldBeEqual(expected.Phone);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this PositionViewModel actual,
            Position expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this Position actual,
            PositionViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this SectionViewModel actual,
            Section expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this Section actual,
            SectionViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this EntryLimitExclusionViewModel actual,
            EntryLimitExclusion expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.User.Id.ShouldBeEqual(expected.User.Id);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this EntryLimitExclusion actual,
            EntryLimitExclusionViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.User.Id.ShouldBeEqual(expected.User.Id);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this ActivityKindViewModel actual,
            ActivityKind expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this ActivityKind actual,
            ActivityKindViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this CustomFieldDefinitionViewModel actual,
            CustomFieldDefinition expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.IsEditable.ShouldBeEqual(expected.IsEditable);
            actual.IsInViews.ShouldBeEqual(expected.IsInViews);
            actual.IsNullable.ShouldBeEqual(expected.IsNullable);
            actual.IsRequired.ShouldBeEqual(expected.IsRequired);
            actual.IsUnique.ShouldBeEqual(expected.IsUnique);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this CustomFieldDefinition actual,
            CustomFieldDefinitionViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.IsEditable.ShouldBeEqual(expected.IsEditable);
            actual.IsInViews.ShouldBeEqual(expected.IsInViews);
            actual.IsNullable.ShouldBeEqual(expected.IsNullable);
            actual.IsRequired.ShouldBeEqual(expected.IsRequired);
            actual.IsUnique.ShouldBeEqual(expected.IsUnique);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this ActivityViewModel actual,
            Activity expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.Color.ShouldBeEqual(expected.Color);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this Activity actual,
            ActivityViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.Color.ShouldBeEqual(expected.Color);
            actual.Comment.ShouldBeEqual(expected.Comment);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this TopicViewModel actual,
            Topic expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this Topic actual,
            TopicViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            actual.Comment.ShouldBeEqual(expected.Comment);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this RoleDefinitionViewModel actual,
            RoleDefinition expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Target.Id.ShouldBeEqual(expected.TableKey);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this RoleDefinition actual,
            RoleDefinitionViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.TableKey.ShouldBeEqual(expected.Target.Id);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this TopicKindViewModel actual,
            TopicKind expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this TopicKind actual,
            TopicKindViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.DisplayName.ShouldBeEqual(expected.DisplayName);
            actual.Name.ShouldBeEqual(expected.Name);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this EntryViewModel actual,
            Entry expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.Minutes.ShouldBeEqual(expected.Minutes);
            actual.Activity.Id.ShouldBeEqual(expected.Activity.Id);
            actual.Topic.Id.ShouldBeEqual(expected.Topic.Id);
            actual.User.Id.ShouldBeEqual(expected.User.Id);
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this Entry actual,
            EntryViewModel expected,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id);
            actual.Minutes.ShouldBeEqual(expected.Minutes);
            actual.Activity.Id.ShouldBeEqual(expected.Activity.Id);
            actual.Topic.Id.ShouldBeEqual(expected.Topic.Id);
            actual.User.Id.ShouldBeEqual(expected.User.Id);
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this AssignmentViewModel actual,
            IAssignmentJunctionEntity expected,
            Func<IAssignmentJunctionEntity, IStorable> elementRetriver,
            OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id, "Id");
            actual.Element.Id.ShouldBeEqual(elementRetriver(expected).Id, "ElementId");
            CompareModelDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this RoleAssignmentViewModel actual,
            IRoleAssignmentJunctionEntity expected,
            Func<IAssignmentJunctionEntity, IStorable> elementRetriver,
            OrganizationGenerator organizationGenerator)
        {
            actual.Role.Id.ShouldBeEqual(expected.RoleDefinition.Id, "RoleId");
            actual.ShouldBeEqual((IAssignmentJunctionEntity)expected, elementRetriver, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this JobRoleAssignmentViewModel actual,
            IJobRoleAssignmentJunctionEntity expected,
            Func<IAssignmentJunctionEntity, IStorable> elementRetriver,
            OrganizationGenerator organizationGenerator)
        {
            actual.JobKind.ShouldBeEqual(expected.JobHours == null ? (expected.JobTime == null ? JobKind.None : JobKind.Time) : JobKind.Hours, "JobKind");
            actual.JobValue.ShouldBeEqual(expected.JobHours ?? expected.JobTime, "JobValue");
            actual.ShouldBeEqual((IRoleAssignmentJunctionEntity)expected, elementRetriver, organizationGenerator);
        }

        public static void ShouldBeEqual(
           this IAssignmentJunctionEntity actual,
           AssignmentPersistenceViewModel expected,
           Func<IAssignmentJunctionEntity, IStorable> elementRetriver,
           OrganizationGenerator organizationGenerator)
        {
            actual.Id.ShouldBeEqual(expected.Id, "id");
            elementRetriver(actual).Id.ShouldBeEqual(expected.ElementId, "ElementId");
            CompareEntityDates(actual, expected, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this IRoleAssignmentJunctionEntity actual,
           RoleAssignmentPersistenceViewModel expected,
            Func<IAssignmentJunctionEntity, IStorable> elementRetriver,
            OrganizationGenerator organizationGenerator)
        {
            actual.RoleDefinition.Id.ShouldBeEqual(expected.RoleId, "RoleId");
            actual.ShouldBeEqual((AssignmentPersistenceViewModel)expected, elementRetriver, organizationGenerator);
        }

        public static void ShouldBeEqual(
            this IJobRoleAssignmentJunctionEntity actual,
            JobRoleAssignmentPersistenceViewModel expected,
            Func<IAssignmentJunctionEntity, IStorable> elementRetriver,
            OrganizationGenerator organizationGenerator)
        {
            (actual.JobHours == null ? (actual.JobTime == null ? JobKind.None : JobKind.Time) : JobKind.Hours).ShouldBeEqual(expected.JobKind, "JobKind");
            (actual.JobHours ?? actual.JobTime).ShouldBeEqual(expected.JobValue, "JobValue");
            actual.ShouldBeEqual((RoleAssignmentPersistenceViewModel)expected, elementRetriver, organizationGenerator);
        }

        private static void CompareEntityDates<TEntity>(TEntity actual, object expected, OrganizationGenerator organizationGenerator = null)
            where TEntity : IEntity
        {
            var organizationTimeZoneId = organizationGenerator?.Settings.SingleOrDefault(x => x.SystemSetting.IsEquivalent(SystemSettingsProvider.OrganizationTimeZone))?.Value ?? "UTC";
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(organizationTimeZoneId);
            var type = actual.GetType();
            var datedProperties = type.GetProperties().Where(x => x.GetCustomAttribute<SqlTypeAttribute>()?.Type == SqlDbType.SmallDateTime);
            foreach (var actualProperty in datedProperties)
            {
                var expectedProperty = expected.GetType().GetProperty(actualProperty.Name);
                var actualValue = actualProperty.GetValue(actual);

                if (actualValue != null && typeof(IScopedEntity).IsAssignableFrom(type))
                {
                    var expectedValueAsDateTime = actualValue as DateTime? ?? (DateTime)actualValue;
                    actualValue = expectedValueAsDateTime.Add(timeZone.BaseUtcOffset);
                }

                var expectedValue = expectedProperty.GetValue(expected);
                if (expectedValue != null)
                {
                    expectedValue = ((DateTimeOffset)expectedValue).Date;
                }

                actualValue.ShouldBeEqual(expectedValue, actualProperty.Name);
            }
        }

        private static void CompareModelDates<TEntity>(object actual, TEntity expected, OrganizationGenerator organizationGenerator = null)
            where TEntity : IEntity
        {
            var organizationTimeZoneId = organizationGenerator?.Settings.SingleOrDefault(x => x.SystemSetting.IsEquivalent(SystemSettingsProvider.OrganizationTimeZone))?.Value ?? "UTC";
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(organizationTimeZoneId);
            var type = expected.GetType();
            var datedProperties = type.GetProperties().Where(x => x.GetCustomAttribute<SqlTypeAttribute>()?.Type == SqlDbType.SmallDateTime);
            foreach (var expectedProperty in datedProperties)
            {
                var actualProperty = actual.GetType().GetProperty(expectedProperty.Name);
                var actualValue = actualProperty.GetValue(actual);
                if (actualValue != null)
                {
                    actualValue = ((DateTimeOffset)actualValue).Date;
                }

                var expectedValue = expectedProperty.GetValue(expected);

                if (expectedValue != null && typeof(IScopedEntity).IsAssignableFrom(type))
                {
                    var expectedValueAsDateTime = expectedValue as DateTime? ?? (DateTime)expectedValue;
                    expectedValue = expectedValueAsDateTime.Add(timeZone.BaseUtcOffset);
                }

                actualValue.ShouldBeEqual(expectedValue, actualProperty.Name);
            }
        }
    }
}