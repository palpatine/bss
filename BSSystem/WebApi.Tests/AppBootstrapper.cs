﻿using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Qdarc.Modules.Common.Ioc.Unity;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.WebApi.Tests.Web;
using SolvesIT.GuiTool.Model.SettingsModel;
using Bootstrapper = SolvesIT.GuiTool.Bootstrapper;

namespace SolvesIt.BSSystem.WebApi.Tests
{
    internal static class AppBootstrapper
    {
        private static UnityContainer _container;

        public static void Configure()
        {
            _container = new UnityContainer();
            var locator = _container.Resolve<UnityServiceLocator>();
            ServiceLocator.SetLocatorProvider(() => locator);

            var registrar = new UnityRegistrar(_container);
            registrar.EnableEnumerableResolution();
            registrar.PerHttpRequestLifetimeManagerFactory = () => new TransientLifetimeManager();
            registrar.RegisterAsPerResolve<IServiceClient, ServiceClient>();
            registrar.RegisterAsPerResolve<ISerializer, JsonSerializer>();
            RegisterReferenceBootstrap(registrar);
            PepareBssEnvironment(registrar);
        }

        private static void PepareBssEnvironment(UnityRegistrar registrar)
        {
            var configurationProvider = new ConfigurationProvider();
            registrar.RegisterInstance<IConfigurationProvider>(configurationProvider);

            var provider = new SessionDataProvider();
            registrar.RegisterInstance<ISessionDataProvider>(provider);
        }

        private static void RegisterReferenceBootstrap(UnityRegistrar registrar)
        {
            var registrar2 = new Qdarc.Utils.Ioc.Unity.UnityRegistrar(_container);
            var devTool = new Bootstrapper();
            devTool.Bootstrap(registrar2);

            var settings = new Settings
            {
                Sql = new SqlSettings
                {
                    DbName = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["BSS_DB_Tests"].ConnectionString).InitialCatalog,
                    DevDbName = ConfigurationManager.AppSettings["DevDbName"],
                    InstanceName = ConfigurationManager.AppSettings["InstanceName"],
                    IsIntegratedSecurity = bool.Parse(ConfigurationManager.AppSettings["IsIntegratedSecurity"]),
                    Password = ConfigurationManager.AppSettings["Password"],
                    User = ConfigurationManager.AppSettings["User"]
                },
                SolutionFile = ConfigurationManager.AppSettings["SolutionFile"],
                Template = new TemplatesSettings
                {
                    DirectoryPath = ConfigurationManager.AppSettings["TemplateDirectoryPath"]
                }
            };

            registrar.RegisterInstance(settings);

            var qdarcCommon = new Qdarc.Modules.Common.Bootstrap.Bootstrapper();
            qdarcCommon.Bootstrap(registrar);

            var coreLogic = new BSSystem.Core.Logic.Environment.Bootstrapper();
            coreLogic.Bootstrap(registrar);

            var commonLogic = new CommonModule.Logic.Environment.Bootstrapper();
            commonLogic.Bootstrap(registrar);
        }
    }
}