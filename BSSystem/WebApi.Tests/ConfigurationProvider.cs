﻿using System;
using Microsoft.Practices.ServiceLocation;
using SolvesIt.BSSystem.Core.Abstraction.Environment;

namespace SolvesIt.BSSystem.WebApi.Tests
{
    internal sealed class ConfigurationProvider : IConfigurationProvider
    {
        public string ConnectionString => Database.GetConnectionString;

        public TimeSpan SessionTimeout => new TimeSpan(1, 0, 0, 0);

        public bool IsCacheEnabled => false;
    }
}