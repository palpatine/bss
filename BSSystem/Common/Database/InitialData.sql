﻿DECLARE @MinDate SMALLDATETIME = '1900-01-01';
INSERT [Common].[Module](
    [Key]
  , [MajorVersion]
  , [DevVersion]
  , [Required])
VALUES('Common', 1, 0, NULL);
DECLARE @ModuleId INT = SCOPE_IDENTITY();

INSERT [Common].[Module_Organization](
    [OrganizationId]
  , [ModuleId]
  , [DateStart]
  , [ChangerId])
VALUES(1, @ModuleId, @MinDate, 1);

BEGIN --InitialUser
SET IDENTITY_INSERT [Common].[Company] ON;
INSERT [Common].[Company](
    [Id]
  , [Name]
  , [FullName]
  , [ChangerId])
VALUES(1, N'BSS', N'BSS', 1);
SET IDENTITY_INSERT [Common].[Company] OFF;

SET IDENTITY_INSERT [Common].[Organization] ON;
INSERT [Common].[Organization](
    [Id]
  , [DisplayName]
  , [CompanyId]
  , [DateStart]
  , [ChangerId])
VALUES(1, N'BSS', 1, @minDate, 1);
SET IDENTITY_INSERT [Common].[Organization] OFF;

SET IDENTITY_INSERT [Common].[User] ON;
INSERT [Common].[User](
    [Id]
  , [ScopeId]
  , [DisplayName]
  , [Surname]
  , [FirstName]
  , [DateStart]
  , [IsBillable]
  , [ChangerId])
VALUES(1, 1, N'Admin', N'Bss', N'Bss', @MinDate, 0, 1);
SET IDENTITY_INSERT [Common].[User] OFF;

INSERT [Common].[Login](
    [ScopeId]
  , [UserId]
  , [LoginName]
  , [Password]
  , [Salt]
  , [ChangerId])
VALUES(1, 1, N'admin', 0x1000000035F0964BFA7AE7738ADA810E5296DD46D00A628ECCD9FAE6D9095D293A7592BE, 0x9A366B362AB86C6236D05578E5659BDE5121, 1);
END

BEGIN --Dictionaries
    INSERT INTO [Common].[EntityStatus]([Value])
    VALUES
        ('Dormant')
      , ('Active')
      , ('Archival')
      , ('Deleted' );
END

BEGIN--PERMISSIONS
    DECLARE @IdOffset INT

    SET IDENTITY_INSERT [Common].[SystemPermission] ON
    
    BEGIN --Organization
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Common_Organization', NULL,        'Common_Organization',                                      0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',                @IdOffset+1, 'Common_Organization|Read',                                 1, 0, 0)
          , (@IdOffset+3, @ModuleId, 'CompanyModify',       @IdOffset+1, 'Common_Organization|CompanyModify',                        1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Details',             @IdOffset+1, 'Common_Organization|Details',                              1, 0, 0)
          , (@IdOffset+5, @ModuleId, 'Write',               @IdOffset+4, 'Common_Organization|Details|Write',                        1, 0, 0)
          , (@IdOffset+6, @ModuleId, 'AddAlias',            @IdOffset+4, 'Common_Organization|Details|AddAlias',                     1, 0, 0)
          , (@IdOffset+7, @ModuleId, 'Assignment',          @IdOffset+4, 'Common_Organization|Details|Assignment',                   0, 1, 0)
          , (@IdOffset+8, @ModuleId, 'Common_Permission',   @IdOffset+7, 'Common_Organization|Details|Assignment|Common_Permission', 1, 0, 0)
          , (@IdOffset+9, @ModuleId, 'Common_Module',		@IdOffset+7, 'Common_Organization|Details|Assignment|Common_Module',	 1, 0, 0)
    END
    BEGIN --Section
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Common_Section', NULL,        'Common_Section',                                0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',           @IdOffset+1, 'Common_Section|Read',                           1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',        @IdOffset+1, 'Common_Section|Details',                        1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',          @IdOffset+3, 'Common_Section|Details|Write',                  1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Assignment',     @IdOffset+3, 'Common_Section|Details|Assignment',             0, 1, 1)
          , (@IdOffset+6, @ModuleId, 'Common_User',    @IdOffset+5, 'Common_Section|Details|Assignment|Common_User', 1, 0, 1)
        INSERT [Common].[SystemPermissionContext]
            ([SystemPermissionId], [TableKey])
        VALUES
            (@IdOffset+1, 'Common_Section')
          , (@IdOffset+2, 'Common_Section')
          , (@IdOffset+3, 'Common_Section')
          , (@IdOffset+4, 'Common_Section')
          , (@IdOffset+5, 'Common_Section')
          , (@IdOffset+6, 'Common_Section')
    END
    BEGIN --User
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Common_User',       NULL,        'Common_User',                                      0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',              @IdOffset+1, 'Common_User|Read',                                 1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',           @IdOffset+1, 'Common_User|Details',                              1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',             @IdOffset+3, 'Common_User|Details|Write',                        1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Assignment',        @IdOffset+3, 'Common_User|Details|Assignment',                   0, 1, 1)
          , (@IdOffset+6, @ModuleId, 'Common_Section',    @IdOffset+5, 'Common_User|Details|Assignment|Common_Section',    1, 0, 1)
          , (@IdOffset+7, @ModuleId, 'Common_Position',   @IdOffset+5, 'Common_User|Details|Assignment|Common_Position',   1, 0, 1)
          , (@IdOffset+8, @ModuleId, 'Common_Permission', @IdOffset+5, 'Common_User|Details|Assignment|Common_Permission', 1, 0, 1)
          , (@IdOffset+9, @ModuleId, 'CanLogin',          @IdOffset+1, 'Common_User|CanLogin',                             1, 0, 1)
    END
    BEGIN --Position
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Common_Position', NULL,        'Common_Position',                                0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',            @IdOffset+1, 'Common_Position|Read',                           1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',         @IdOffset+1, 'Common_Position|Details',                        1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',           @IdOffset+3, 'Common_Position|Details|Write',                  1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Assignment',      @IdOffset+3, 'Common_Position|Details|Assignment',             0, 1, 1)
          , (@IdOffset+6, @ModuleId, 'Common_User',     @IdOffset+5, 'Common_Position|Details|Assignment|Common_User', 1, 0, 1)
    END
    BEGIN --RoleDefinition
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Common_RoleDefinition', NULL,        'Common_RoleDefinition',                                      0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',                  @IdOffset+1, 'Common_RoleDefinition|Read',                                 1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',               @IdOffset+1, 'Common_RoleDefinition|Details',                              1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',                 @IdOffset+3, 'Common_RoleDefinition|Details|Write',                        1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Assignment',            @IdOffset+3, 'Common_RoleDefinition|Details|Assignment',                   1, 0, 1)
          , (@IdOffset+6, @ModuleId, 'Common_Permission',     @IdOffset+5, 'Common_RoleDefinition|Details|Assignment|Common_Permission', 1, 0, 1)
    END
    BEGIN --CustomFieldDefinition
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Common_CustomFieldDefinition', NULL,        'Common_CustomFieldDefinition',                   0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',                         @IdOffset+1, 'Common_CustomFieldDefinition|Read',              1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',                      @IdOffset+1, 'Common_CustomFieldDefinition|Details',           1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',                        @IdOffset+3, 'Common_CustomFieldDefinition|Details|Write',     1, 0, 1)
    END
    BEGIN --Permissions
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Common_Permission', NULL,        'Common_Permission',                             0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',              @IdOffset+1, 'Common_Permission|Read',                        1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',           @IdOffset+1, 'Common_Permission|Details',                     1, 0, 1)
    END
    BEGIN --Settings
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Settings', NULL,        'Settings',                             0, 1, 1)
    END

    SET IDENTITY_INSERT [Common].[SystemPermission] OFF

    INSERT [Common].[Permission](
        [ScopeId]
      , [SystemPermissionId]
      , [CustomPermissionId]
      , [ChangerId])
    SELECT
        1 AS [ScopeId]
      , [csp].[Id] AS [SystemPermissionId]
      , NULL AS [CustomPermissionId]
      , 1 AS [ChangerId]
    FROM [Common].[SystemPermission] AS [csp]
    WHERE [csp].[ModuleId] = @ModuleId

    INSERT [Common].[Permission_User](
        [ScopeId]
      , [UserId]
      , [PermissionId]
      , [ChangerId])
    SELECT
        1 AS [ScopeId]
      , 1 AS [UserId]
      , [cp].[Id] AS [PermissionId]
      , 1 AS [ChangerId]
    FROM [Common].[Permission] AS [cp]
    JOIN [Common].[SystemPermission] AS [csp] ON [csp].[Id] = [cp].[SystemPermissionId]
    WHERE [csp].[ModuleId] = @ModuleId
END --PERMISSIONS

BEGIN --Menu
    DECLARE @MenuSectionId INT

    BEGIN --Profile
        INSERT [Common].[MenuSection]
            ([Key], [Path], [ParentId], [NavigationPosition], [Order], [IconName])
        VALUES
            ('Profile', 'Profile', NULL, 2, 3, 'profile')
        SET @MenuSectionId = SCOPE_IDENTITY();

        INSERT [Common].[MenuItem]
            ([Key], [MenuSectionId], [Order], [IconName], [PermissionPath])
        VALUES
            ('Profile',        @MenuSectionId, 0, 'profile',        NULL)
          , ('ChangePassword', @MenuSectionId, 0, 'changepassword', NULL)
    END
    BEGIN --Administration
        INSERT [Common].[MenuSection]
            ([Key], [Path], [ParentId], [NavigationPosition], [Order], [IconName])
        VALUES
            ('Administration', 'Administration', NULL, 1, 4, 'administration')
        SET @MenuSectionId = SCOPE_IDENTITY();

        INSERT [Common].[MenuItem]
            ([Key], [MenuSectionId], [Order], [IconName], [PermissionPath])
        VALUES
            ('Organization',          @MenuSectionId, 3, 'organization',   'Common_Organization|Read')
          , ('Section',               @MenuSectionId, 4, 'section',        'Common_Section|Read')
          , ('Position',              @MenuSectionId, 5, 'position',       'Common_Position|Read')
          , ('User',                  @MenuSectionId, 6, 'users',          'Common_User|Read')
          , ('Permission',            @MenuSectionId, 7, 'permission',     'Common_Permission|Read')
          , ('RoleDefinition',        @MenuSectionId, 8, 'roledefinition', 'Common_RoleDefinition|Read')
          , ('CustomFieldDefinition', @MenuSectionId, 9, 'signdefinition', 'Common_CustomFieldDefinition|Read')
    END

    BEGIN --Settings
        INSERT [Common].[MenuSection]
            ([Key], [Path], [ParentId], [NavigationPosition], [Order], [IconName])
        VALUES
            ('Settings', 'Settings', NULL, 1, 5, 'settings')
        SET @MenuSectionId = SCOPE_IDENTITY();

        INSERT [Common].[MenuItem]
            ([Key], [MenuSectionId], [Order], [IconName], [PermissionPath])
        VALUES
            ('Basic', @MenuSectionId, 1, NULL, 'Settings|Basic')

        DECLARE @MenuItemId INT = SCOPE_IDENTITY();

        INSERT [Common].[SettingPanel]
            ([Key], [MenuItemId], [Order], [PermissionPath])
        VALUES
            ('BasicSettings', @MenuItemId, 0, 'Settings|Basic')
        DECLARE @SettingPanelId INT = SCOPE_IDENTITY();

         INSERT INTO [Common].[SystemSetting]
        ([ModuleId], [SettingPanelId], [Key], [EditorType], [EditorParameter], [DefaultValue], [CanBeOverridenByOrganization], [CanBeOverridenByUser])
    VALUES 
          (@ModuleId, @SettingPanelId, 'TimeZone', 'timeZone', '{}', 'UTC', 1, 0)
  

        --INSERT [Common].[MenuItem]
        --    ([Key], [MenuSectionId], [Order], [IconName], [PermissionPath])
        --VALUES
        --    ('Common', @MenuSectionId, 1, NULL, NULL)
    END
END
