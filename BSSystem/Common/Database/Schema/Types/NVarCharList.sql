﻿CREATE TYPE [Common].[NVarCharList] AS TABLE (
    [Value] NVARCHAR (MAX) NOT NULL);
