﻿CREATE TYPE [Common].[ValidationResultList] AS TABLE (
    [Id]              INT            NULL,
    [Field]           NVARCHAR (MAX) NULL,
    [MessageCode]     NVARCHAR (MAX) NOT NULL,
    [AdditionalValue] NVARCHAR (MAX) NULL);
