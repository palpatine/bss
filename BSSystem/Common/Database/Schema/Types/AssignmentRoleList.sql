﻿CREATE TYPE [Common].[AssignmentRoleList] AS TABLE (
    [Id]        INT            NOT NULL,
    [ElementId] INT            NOT NULL,
    [RoleId]    INT            NOT NULL,
    [DateStart] SMALLDATETIME  NOT NULL,
    [DateEnd]   SMALLDATETIME  NULL,
    UNIQUE NONCLUSTERED ([Id] ASC));
