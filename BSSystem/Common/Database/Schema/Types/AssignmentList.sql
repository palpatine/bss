﻿CREATE TYPE [Common].[AssignmentList] AS TABLE (
    [Id]        INT				NOT NULL,
    [ElementId] INT				NOT NULL,
    [DateStart] SMALLDATETIME	NOT NULL,
    [DateEnd]   SMALLDATETIME	 NULL,
    UNIQUE NONCLUSTERED ([Id] ASC));
