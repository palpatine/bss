﻿CREATE TYPE [Common].[JunctionList] AS TABLE (
    [Id]        INT  NOT NULL,
    [ElementId] INT  NOT NULL,
    UNIQUE NONCLUSTERED ([Id] ASC));
