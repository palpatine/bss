﻿CREATE TYPE [Common].[AssignmentJobRoleList] AS TABLE (
    [Id]        INT				NOT NULL,
    [ElementId] INT				NOT NULL,
    [RoleId]    INT				NOT NULL,
    [JobTime]   DECIMAL (28, 3) NULL,
    [JobHours]  INT   NULL,
    [DateStart] SMALLDATETIME	NOT NULL,
    [DateEnd]   SMALLDATETIME	NULL,
    UNIQUE NONCLUSTERED ([Id] ASC));
