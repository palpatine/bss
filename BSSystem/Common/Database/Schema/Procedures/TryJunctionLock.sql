﻿CREATE PROCEDURE [Common].[TryJunctionLock](
    @OperationToken VARCHAR(40),
    @UserId INT, 
    @SessionId INT, 
    @TableName VARCHAR(100),
    @ColumnName VARCHAR(50),
    @ColumnValue INT)
AS
BEGIN
    SET NOCOUNT ON;

    IF @OperationToken IS NULL
    OR @UserId IS NULL
    OR @SessionId IS NULL
    OR @TableName IS NULL
    OR @ColumnName IS NULL
    OR @ColumnValue IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 1
   
    DECLARE @ValidationResult [Common].[ValidationResultList]
    DECLARE @MessageCode NVARCHAR(MAX)

    SET @MessageCode = 'LockError'
    INSERT @ValidationResult
    SELECT 
        NULL AS [Id]
      , NULL AS [Field]
      , @MessageCode AS [MessageCode]
      , [u].[DisplayName] AS [AdditionalValue]
    FROM [Common].[JunctionLock] AS [jl]
    JOIN [Common].[User] AS [u] ON [u].[Id] = [jl].[UserId]
    JOIN [Common].[Session] AS [s] ON [s].[Id] = [jl].[SessionId] AND [s].[TimeEnd] >= GETUTCDATE()
    WHERE [jl].[TableName] = @TableName
      AND [jl].[ColumnName] = @ColumnName
      AND [jl].[ColumnValue] = @ColumnValue
      AND [jl].[UserId] <> @UserId;
    
    DECLARE @Sql NVARCHAR(MAX);  
    -- exists single lock
    SET @Sql = '
        SELECT 
            NULL AS [Id]
          , NULL AS [Field]
          , @MessageCode AS [MessageCode]
          , [u].[DisplayName] AS [AdditionalValue]
        FROM '+@TableName+' [t]
        JOIN [Common].[Lock] AS [l] ON [l].[RecordId] = [t].[Id]
                                   AND [l].[TableName] = '''+@TableName+'''
        JOIN [Common].[User] AS [u] ON [u].[Id] = [l].[UserId]
        WHERE [t].'+@ColumnName+' = @ColumnValue'
    INSERT @ValidationResult
    EXECUTE sp_executesql @Sql,
    N'@MessageCode NVARCHAR(MAX), @ColumnValue INT',
    @MessageCode,
    @ColumnValue

    -- exists junction lock cross
    DECLARE @UnpivotColumns NVARCHAR(MAX) = NULL;
    SELECT @UnpivotColumns = COALESCE(@UnpivotColumns + ',','') + '(''['+[c].[name]+']'','+[c].[name]+')'
    FROM sys.columns AS [c]
    JOIN sys.foreign_key_columns [fkc] ON [fkc].[parent_object_id] = [c].[object_id]
                                      AND [fkc].[parent_column_id] = [c].[column_id]
    WHERE [c].[object_id] = OBJECT_ID(@TableName)
        AND [c].[is_computed] = 0
    SET @Sql = '
        SELECT 
            NULL AS [Id]
          , NULL AS [Field]
          , @MessageCode AS [MessageCode]
          , [u].[DisplayName] AS [AdditionalValue]
        FROM '+@TableName+' [t]
        CROSS APPLY (VALUES '+@UnpivotColumns+') [ca]([Name], [Value])
        JOIN (
            SELECT
                [TableName]
              , [ColumnName]
              , [ColumnValue]
              , [UserId]
            FROM [Common].[JunctionLock]
            WHERE [TableName] = @TableName
                AND ([ColumnName] <> @ColumnName
                    OR [ColumnValue] <> @ColumnValue
                    OR [UserId] <> @UserId)
            ) [jl] ON [jl].[TableName] = @TableName
                  AND [jl].[ColumnName] = [ca].[Name]
                  AND [jl].[ColumnValue] = [ca].[Value]
        JOIN [Common].[User] AS [u] ON [u].[Id] = [jl].[UserId]
        WHERE [t].'+@ColumnName+' = @ColumnValue'
    INSERT @ValidationResult
    EXECUTE sp_executesql @Sql,
    N'@MessageCode NVARCHAR(MAX), @TableName VARCHAR(100), @ColumnName VARCHAR(50), @ColumnValue INT, @UserId INT',
    @MessageCode,
    @TableName,
    @ColumnName,
    @ColumnValue,
    @UserId

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT DISTINCT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END  

    MERGE [Common].[JunctionLock] AS [target]
    USING (SELECT @TableName, @ColumnName, @ColumnValue) AS [source] (TableName, ColumnName, ColumnValue)
    ON [target].[TableName] = [source].[TableName]
    AND [target].[ColumnName] = [source].[ColumnName]
    AND [target].[ColumnValue] = [source].[ColumnValue]
    WHEN MATCHED THEN 
        UPDATE SET 
            [SessionId] = @SessionId
          , [TimeStart] = GETUTCDATE()
          , [UserId] = @UserId
          , [OperationToken] = @OperationToken
    WHEN NOT MATCHED THEN
        INSERT ([OperationToken], [TableName], [ColumnName], [ColumnValue], [SessionId], [UserId], [TimeStart])
        VALUES (@OperationToken, @TableName, @ColumnName, @ColumnValue, @SessionId, @UserId, GETUTCDATE());
    
    SET NOCOUNT OFF;
END;
