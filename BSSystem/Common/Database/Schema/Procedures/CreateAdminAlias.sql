﻿CREATE PROCEDURE [Common].[CreateAdminAlias](
    @UserId INT,
    @TargetOrganizationId INT,
    @SessionId INT)
AS
BEGIN
    SET NOCOUNT ON;
    IF @UserId IS NULL
    OR @TargetOrganizationId IS NULL
    OR @SessionId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 1

    DECLARE @AliasId INT
    DECLARE @OperationToken VARCHAR(40) = NEWID()
    DECLARE @ValidationResult [Common].[ValidationResultList]

    IF EXISTS (
        SELECT u.* FROM [Common].[User] AS u
        WHERE u.[ScopeId] = @TargetOrganizationId
        AND u.[Id] = @UserId)
        BEGIN
            SELECT
                CAST(NULL AS INT) AS [Id]
              , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
              , 'CyclicReference' AS [MessageCode]
              , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
            RETURN
        END

    SELECT @AliasId = [ut].[Id]
    FROM [Common].[User] AS [ut]
    JOIN [Common].[User] AS [ur] on [ur].[Id] = @UserId
    WHERE ([ut].[ParentId] = [ur].[Id] OR [ut].[RootId] = [ur].[RootId])
      AND [ut].[ScopeId] = @TargetOrganizationId
      AND [ut].[IsDeleted] = 0
 
    IF @AliasId IS NOT NULL
    BEGIN
        IF @@ROWCOUNT > 1
            BEGIN
            SELECT
                CAST(NULL AS INT) AS [Id]
              , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
              , 'MultipleAliases' AS [MessageCode]
              , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
            RETURN
        END
    
        EXEC [Common].[TryLock]
            @OperationToken,
            @UserId,
            @SessionId,
            @TableName = '[Common].[User]',
            @RecordId = @AliasId
            
        DECLARE @NotAllovedDateValue SMALLDATETIME = '2079-06-06'
        UPDATE [ua]
        SET
            [DateStart] = [u].[DateStart]
          , [DateEnd] = [u].[DateEnd]
          , [ChangerId] = @UserId
        FROM [Common].[User] AS [ua]
        JOIN [Common].[User] AS [u] ON [u].[Id] = [ua].[ParentId]
        WHERE [ua].[Id]=@AliasId
          AND ([ua].[DateStart] <> [u].[DateStart]
            OR ISNULL([ua].[DateEnd], @NotAllovedDateValue) <> ISNULL([u].[DateEnd], @NotAllovedDateValue))
    END
    ELSE
    BEGIN
        INSERT [Common].[User](
            [ScopeId]
          , [DisplayName]
          , [Surname]
          , [FirstName]
          , [DateStart]
          , [DateEnd]
          , [ParentId]
          , [RootId]
          , [IsBillable]
          , [ChangerId])
        SELECT
            @TargetOrganizationId AS [ScopeId]
          , [u].[DisplayName]
          , [u].[Surname]
          , [u].[FirstName]
          , [u].[DateStart]
          , [u].[DateEnd]
          , @UserId AS [ParentId]
          , ISNULL([u].[RootId], @UserId) AS [RootId]
          , CAST(0 AS BIT) AS [IsBillable]
          , @UserId AS [ChangerId]
        FROM [Common].[User] AS [u]
        WHERE [u].[Id] = @UserId;

        SET @AliasId = @@IDENTITY
    END

    EXEC [Common].[TryJunctionLock]
        @OperationToken,
        @UserId,
        @SessionId,
        @TableName = '[Common].[Permission_User]',
        @ColumnName = '[UserId]',
        @ColumnValue = @AliasId

    DECLARE @NewList [Common].[JunctionList]
    INSERT @NewList ([Id], [ElementId])
    SELECT
        ISNULL([gup].[JunctionId], -[gup].[Id]) AS [Id]
        , [gup].[Id] AS [ElementId]
    FROM [Common].[GetUserPermissions](@AliasId) AS [gup]
    WHERE [gup].[IsContainer] = 0

    EXEC [Common].[Update_Permission_User]
        @TargetOrganizationId,
        @ChangerId = @UserId,
        @MasterColumnName = '[UserId]',
        @MasterId = @AliasId,
        @NewList = @NewList


    EXEC [Common].[RemoveLock]
        @OperationToken
        , @UserId
        , @SessionId

    SET NOCOUNT OFF;
END;
