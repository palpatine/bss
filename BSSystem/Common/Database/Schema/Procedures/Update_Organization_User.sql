﻿SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Common].[Update_Organization_User]
(
    @ScopeId INT,
    @ChangerId INT,
    @MasterColumnName VARCHAR(50),
    @MasterId INT,
    @NewList [Common].[AssignmentRoleList] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

    IF @ScopeId IS NULL
    OR @ChangerId IS NULL
    OR @MasterColumnName IS NULL
    OR @MasterId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 1

    DECLARE @ValidationResult [Common].[ValidationResultList]
    DECLARE @MainTableName NVARCHAR(MAX) = '[Common].[Organization_User]'
          , @MessageCode NVARCHAR(MAX)
    
    SET @MessageCode = 'NoLockError'
    IF NOT EXISTS (
        SELECT *
        FROM [Common].[JunctionLock] [jl]
        WHERE [jl].[TableName] = @MainTableName
          AND [jl].[ColumnName] = @MasterColumnName
          AND [jl].[ColumnValue] = @MasterId
          AND [jl].[UserId] = @ChangerId
    )
    BEGIN
        SELECT
            CAST(NULL AS INT) AS [Id]
          , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
          , @MessageCode AS [MessageCode]
          , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
        RETURN
    END
    
    SET @MessageCode = 'ReferenceError'
    IF @MasterColumnName = '[OrganizationId]' AND NOT EXISTS (
            SELECT *
            FROM [Common].[Organization]
            WHERE [Id]=@MasterId
                AND [IsDeleted] = 0)
    OR @MasterColumnName = '[UserId]' AND NOT EXISTS (
            SELECT *
            FROM [Common].[User]
            WHERE [Id]=@MasterId
                AND [IsDeleted] = 0
                AND [ScopeId] = @ScopeId)
    BEGIN
        SELECT
            CAST(NULL AS INT) AS [Id]
          , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
          , @MessageCode AS [MessageCode]
          , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
        RETURN
    END
    INSERT @ValidationResult
    SELECT
        [Elements].[Id]
      , 'Element' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM (
        SELECT [nl].[Id]
        FROM @NewList AS [nl]
        LEFT JOIN [Common].[Organization] [t] ON [t].[Id] = [nl].[ElementId]
            AND [t].[IsDeleted] = 0
        WHERE @MasterColumnName <> '[OrganizationId]' AND [t].[Id] IS NULL
        UNION ALL
        SELECT [nl].[Id]
        FROM @NewList AS [nl]
        LEFT JOIN [Common].[User] [t] ON [t].[Id] = [nl].[ElementId]
             AND [t].[IsDeleted] = 0 
             AND [t].[ScopeId] = @ScopeId 
        WHERE @MasterColumnName <> '[UserId]' AND [t].[Id] IS NULL
    ) AS [Elements](Id)
    
    UNION ALL
    SELECT
        [nl].[Id]
      , 'Role' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM @NewList AS [nl]
    LEFT JOIN [Common].[RoleDefinition] AS [rd] ON [rd].[Id] = [nl].[RoleId]
                                                AND [rd].[IsDeleted] = 0
                                                AND [rd].[ScopeId] = @ScopeId
                                                AND [rd].[TableKey] = 'Common_Organization' 
    WHERE [rd].[Id] IS NULL

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END

    SET @MessageCode = 'DateRangeError'
    SELECT
        [nl].[Id]
      , 'DateEnd' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM @NewList [nl]
    WHERE [nl].[DateEnd] IS NOT NULL AND [nl].[DateStart] > [nl].[DateEnd]

    DECLARE @MinDateValue date = '1753-01-01'

    -- 'DateStartError' / 'DateEndError'
    INSERT @ValidationResult
    SELECT
        [Q].[Id] AS [Id]
      , [ca].[Field] AS [Field]
      , [ca].[Field]+'Error' AS [MessageCode]
      , CONVERT(NVARCHAR(MAX), [ca].[Date], 126) AS [AdditionalValue]
    FROM (
        SELECT
            [nl].[Id]
          , [nl].[DateStart]
          , [nl].[DateEnd]

          , (SELECT MAX([DateSatrt])
            FROM ( VALUES
              (NULL)
              , ([mst].[DateStart]) 
              , ([est].[DateStart]) 
              , ([rd].[DateStart])
            ) MaxDate([DateSatrt])) AS [MinimumPossibleDate]

          , (SELECT MIN([DateEnd])
            FROM ( VALUES
              (NULL)
              , ([mst].[DateEnd]) 
              , ([est].[DateEnd]) 
              , ([rd].[DateEnd])
            ) MinDate([DateEnd])) AS [MaximumPossibleDate]
        FROM @NewList [nl]
        LEFT JOIN [Common].[User] [mst] ON @MasterColumnName = '[UserId]' AND [mst].[Id] = @MasterId
        LEFT JOIN [Common].[User] [est] ON @MasterColumnName <> '[UserId]' AND [est].[Id] = [nl].[ElementId]
        JOIN [Common].[RoleDefinition] [rd] ON [rd].[Id] = [nl].[RoleId]
    ) AS Q
    CROSS APPLY (
        VALUES ('DateStart', [MinimumPossibleDate])
             , ('DateEnd',   [MaximumPossibleDate])
    ) AS [ca]([Field], [Date])
    WHERE [ca].[Field] = 'DateStart' AND [Q].[DateStart] < [ca].[Date]
        OR [ca].[Field] = 'DateEnd' AND (
            [Q].[DateEnd] IS NULL AND [ca].[Date] IS NOT NULL
            OR [Q].[DateEnd] > COALESCE([ca].[Date], [Q].[DateEnd])
        )

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END  
    
    DECLARE @MaxAllovedDateValue SMALLDATETIME = '2079-06-06 23:59:00'

    SET @MessageCode = 'DateOverlapError';
    INSERT @ValidationResult
    SELECT
        [nl1].[Id] AS [Id]
      , 'Element' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST([nl2].[Id] AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM @NewList AS [nl1]
    LEFT JOIN @NewList AS [nl2] ON @MasterColumnName = '[UserId]'
                            AND [nl1].[Id] <> [nl2].[Id]
                            AND [nl1].[ElementId] = [nl2].[ElementId]
                            AND [nl1].[RoleId] = [nl2].[RoleId]

    WHERE [nl1].[DateStart] <= ISNULL([nl2].[DateEnd], @MaxAllovedDateValue)
      AND ISNULL([nl1].[DateEnd], @MaxAllovedDateValue) >= [nl2].[DateStart];

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END
   
    DECLARE @NotAllovedNumberValue int = -1

    ;WITH
    TargetCTE AS (
        SELECT
            [T].[Id]
          , [T].[ScopeId]
          , [T].[OrganizationId]
          , [T].[UserId]
          , [T].[RoleDefinitionId]
          , [T].[DateStart]
          , [T].[DateEnd]
          , [T].[ChangerId]
          , [T].[TimeEnd]
        FROM [Common].[Organization_User] [T]
        WHERE [T].[IsDeleted] = 0
        AND (  @MasterColumnName = '[OrganizationId]' AND [T].[OrganizationId] = @MasterId
            OR @MasterColumnName = '[UserId]' AND [T].[UserId] = @MasterId)
    ),
    SourceCTE AS (
        SELECT
            [nl].[Id] AS [Id]
          , IIF(@MasterColumnName='[OrganizationId]', @MasterId, [nl].[ElementId]) AS [OrganizationId]
          , IIF(@MasterColumnName='[UserId]', @MasterId, [nl].[ElementId]) AS [UserId]
          , [nl].[RoleId] AS [RoleDefinitionId]
          , [nl].[DateStart] AS [DateStart]
          , [nl].[DateEnd] AS [DateEnd]
        FROM @NewList [nl]
    )
    MERGE TargetCTE AS [T]
    USING SourceCTE AS [S]
    ON [T].[Id] = [S].[Id]
    WHEN NOT MATCHED BY TARGET THEN
        INSERT ([OrganizationId], [UserId], [RoleDefinitionId], [DateStart], [DateEnd], [ChangerId], [ScopeId])
        VALUES ([S].[OrganizationId], [S].[UserId], [S].[RoleDefinitionId], [S].[DateStart], [S].[DateEnd], @ChangerId, @ScopeId)
    WHEN MATCHED AND 
          (  [T].[OrganizationId] <> [S].[OrganizationId]
          OR [T].[UserId] <> [S].[UserId]
          OR [T].[RoleDefinitionId] <> [S].[RoleDefinitionId]
          OR [T].[DateStart] <> [S].[DateStart]
          OR ISNULL([T].[DateEnd], @MaxAllovedDateValue) <> ISNULL([S].[DateEnd], @MaxAllovedDateValue) ) THEN
        UPDATE SET
            [T].[OrganizationId] = [S].[OrganizationId]
          , [T].[UserId] = [S].[UserId]
          , [T].[RoleDefinitionId] = [S].[RoleDefinitionId]
          , [T].[DateStart] = [S].[DateStart]
          , [T].[DateEnd] = [S].[DateEnd]
          , [T].[ChangerId] = @ChangerId
    WHEN NOT MATCHED BY SOURCE THEN
        UPDATE SET
            [T].[ChangerId] = @ChangerId
          , [T].[TimeEnd] = GETUTCDATE();

    SET NOCOUNT OFF;
END