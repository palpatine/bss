﻿CREATE PROCEDURE [Common].[Update_Permissions]
(
    @ChangerId INT,
    @OrganizationId INT,
    @TargetPermissions [Common].[JunctionList] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

    IF @ChangerId IS NULL
    OR @OrganizationId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 1

    DECLARE @ValidationResult [Common].[ValidationResultList]
    DECLARE @MainTableName NVARCHAR(MAX) = '[Common].[Permission]'
          , @MessageCode NVARCHAR(MAX)

    SET @MessageCode = 'NoLockError'
    IF NOT EXISTS (
        SELECT *
        FROM [Common].[JunctionLock] [jl]
        WHERE [jl].[TableName] = @MainTableName
          AND [jl].[ColumnName] = '[ScopeId]'
          AND [jl].[ColumnValue] = @OrganizationId
          AND [jl].[UserId] = @ChangerId
    )
    BEGIN
        SELECT
            CAST(NULL AS INT) AS [Id]
          , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
          , @MessageCode AS [MessageCode]
          , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
        RETURN
    END

    SET @MessageCode = 'ReferenceError'
    IF NOT EXISTS (
        SELECT *
        FROM [Common].[Organization]
        WHERE [Id]=@OrganizationId
            AND [IsDeleted] = 0
    )
    BEGIN
        SELECT
            CAST(NULL AS INT) AS [Id]
          , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
          , @MessageCode AS [MessageCode]
          , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
        RETURN
    END

    INSERT @ValidationResult
    SELECT
        [Elements].[Id]
      , 'ElementId' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM (
        SELECT [tp].[Id]
        FROM @TargetPermissions AS [tp]
        LEFT JOIN [Common].[SystemPermission] [sp] ON [sp].[Id] = [tp].[ElementId]
        WHERE [sp].[Id] IS NULL
    ) AS [Elements]
    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END 

    ;WITH
    TargetCTE AS (
        SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0
    ),
    SourceCTE AS (
        SELECT
            [Id]
          , [ElementId] AS [SystemPermissionId]
        FROM @TargetPermissions
    )
    MERGE TargetCTE AS [T]
    USING SourceCTE AS [S]
    ON [T].[Id] = [S].[Id]
    WHEN NOT MATCHED BY TARGET THEN
        INSERT ([ScopeId], [SystemPermissionId], [ChangerId])
        VALUES (@OrganizationId, [S].[SystemPermissionId], @ChangerId)
    WHEN MATCHED AND 
          ([T].[SystemPermissionId] <> [S].[SystemPermissionId]) THEN
        UPDATE SET
            [T].[SystemPermissionId] = [S].[SystemPermissionId]
          , [T].[ChangerId] = @ChangerId
    WHEN NOT MATCHED BY SOURCE THEN
        UPDATE SET
            [T].[ChangerId] = @ChangerId
          , [T].[TimeEnd] = GETUTCDATE();

    SET NOCOUNT OFF;
END