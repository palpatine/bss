﻿CREATE PROCEDURE [Common].[RemoveLock](
    @OperationToken VARCHAR(40)
  , @UserId INT
  , @SessionId INT)
AS
BEGIN
    SET NOCOUNT ON;
    IF @OperationToken IS NULL
    OR @UserId IS NULL
    OR @SessionId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 1

    DELETE [Common].[Lock]
    WHERE
        [Lock].[OperationToken] = @OperationToken
    AND [Lock].[UserId] = @UserId
    AND [Lock].[SessionId] = @SessionId;

    DELETE [Common].[JunctionLock]
    WHERE
        [JunctionLock].[OperationToken] = @OperationToken
    AND [JunctionLock].[UserId] = @UserId
    AND [JunctionLock].[SessionId] = @SessionId;
    SET NOCOUNT OFF;
END;
