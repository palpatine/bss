﻿CREATE PROCEDURE [Common].[TryLock](
    @OperationToken VARCHAR(40),
    @UserId INT, 
    @SessionId INT, 
    @TableName VARCHAR(100), 
    @RecordId INT)
AS
BEGIN
    SET NOCOUNT ON;

    IF @OperationToken IS NULL
    OR @UserId IS NULL
    OR @SessionId IS NULL
    OR @TableName IS NULL
    OR @RecordId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 1

    DECLARE @ValidationResult [Common].[ValidationResultList]
    DECLARE @MessageCode NVARCHAR(MAX)

    SET @MessageCode = 'LockError'
    INSERT @ValidationResult
    SELECT 
        NULL AS [Id]
      , NULL AS [Field]
      , @MessageCode AS [MessageCode]
      , [u].[DisplayName] AS [AdditionalValue]
    FROM [Common].[Lock] AS [l]
    JOIN [Common].[User] AS [u] ON [u].[Id] = [l].[UserId]
    JOIN [Common].[Session] AS [s] ON [s].[Id] = [l].[SessionId] AND [s].[TimeEnd] >= GETUTCDATE()
    WHERE [l].[TableName] = @TableName
      AND [l].[RecordId] = @RecordId
      AND [l].[UserId] <> @UserId;
    
    DECLARE @UnpivotColumns NVARCHAR(MAX) = NULL;
    SELECT @UnpivotColumns = COALESCE(@UnpivotColumns + ',','') + '(''['+[c].[name]+']'','+[c].[name]+')'
    FROM sys.columns AS [c]
    JOIN sys.foreign_key_columns [fkc] ON [fkc].[parent_object_id] = [c].[object_id]
                                      AND [fkc].[parent_column_id] = [c].[column_id]
    WHERE [c].[object_id] = OBJECT_ID(@TableName)
        AND [c].[is_computed] = 0
    DECLARE @Sql NVARCHAR(MAX) = '
        SELECT 
            NULL AS [Id]
          , NULL AS [Field]
          , @MessageCode AS [MessageCode]
          , [u].[DisplayName] AS [AdditionalValue]
        FROM '+@TableName+' [t]
        CROSS APPLY (VALUES '+@UnpivotColumns+') [ca](Name, Value)
        JOIN [Common].[JunctionLock] [jl] ON [jl].[TableName] = @TableName
                    AND [jl].[ColumnName] = [ca].[Name]
                    AND [jl].[ColumnValue] = [ca].[Value]
        JOIN [Common].[User] AS [u] ON [u].[Id] = [jl].[UserId]
        WHERE [t].[Id] = @RecordId'
    INSERT @ValidationResult
    EXECUTE sp_executesql @Sql,
    N'@MessageCode NVARCHAR(MAX), @TableName VARCHAR(100), @RecordId INT',
    @MessageCode,
    @TableName,
    @RecordId

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT DISTINCT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END  

    MERGE [Common].[Lock] AS [target]
    USING (SELECT @TableName, @RecordId) AS [source] (TableName, RecordId)
    ON [target].[TableName] = [source].[TableName]
    AND [target].[RecordId] = [source].[RecordId]
    WHEN MATCHED THEN 
        UPDATE SET 
            [SessionId] = @SessionId
          , [TimeStart] = GETUTCDATE()
          , [UserId] = @UserId
          , [OperationToken] = @OperationToken
    WHEN NOT MATCHED THEN
        INSERT ([OperationToken], [TableName], [RecordId], [SessionId], [UserId], [TimeStart])
        VALUES (@OperationToken, @TableName, @RecordId, @SessionId, @UserId, GETUTCDATE());
    
    SET NOCOUNT OFF;
END;
