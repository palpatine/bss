﻿CREATE PROCEDURE [Common].[Update_Module_Organization]
(
    @ChangerId INT,
    @MasterColumnName VARCHAR(50),
    @MasterId INT,
    @NewList [Common].[AssignmentList] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

    IF @ChangerId IS NULL
    OR @MasterColumnName IS NULL
    OR @MasterId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 1

    DECLARE @ValidationResult [Common].[ValidationResultList]
    DECLARE @MainTableName NVARCHAR(MAX) = '[Common].[Module_Organization]'
          , @MessageCode NVARCHAR(MAX)
    
    SET @MessageCode = 'NoLockError'
    IF NOT EXISTS (
        SELECT *
        FROM [Common].[JunctionLock] [jl]
        WHERE [jl].[TableName] = @MainTableName
          AND [jl].[ColumnName] = @MasterColumnName
          AND [jl].[ColumnValue] = @MasterId
          AND [jl].[UserId] = @ChangerId
    )
    BEGIN
        SELECT
            CAST(NULL AS INT) AS [Id]
          , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
          , @MessageCode AS [MessageCode]
          , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
        RETURN
    END
    
    SET @MessageCode = 'ReferenceError'
    IF @MasterColumnName = '[ModuleId]' AND NOT EXISTS (
            SELECT *
            FROM [Common].[Module]
            WHERE [Id]=@MasterId)
    OR @MasterColumnName = '[OrganizationId]' AND NOT EXISTS (
            SELECT *
            FROM [Common].[Organization]
            WHERE [Id]=@MasterId
                AND [IsDeleted] = 0)
    BEGIN
        SELECT
            CAST(NULL AS INT) AS [Id]
          , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
          , @MessageCode AS [MessageCode]
          , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
        RETURN
    END
    INSERT @ValidationResult
    SELECT
        [Elements].[Id]
      , 'Element' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM (
        SELECT [nl].[Id]
        FROM @NewList AS [nl]
        LEFT JOIN [Common].[Module] [t] ON [t].[Id] = [nl].[ElementId]  
        WHERE @MasterColumnName <> '[ModuleId]' AND [t].[Id] IS NULL
        UNION ALL
        SELECT [nl].[Id]
        FROM @NewList AS [nl]
        LEFT JOIN [Common].[Organization] [t] ON [t].[Id] = [nl].[ElementId]
             AND [t].[IsDeleted] = 0
        WHERE @MasterColumnName <> '[OrganizationId]' AND [t].[Id] IS NULL
    ) AS [Elements](Id)

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END 

SET @MessageCode = 'DateRangeError'
    SELECT
        [nl].[Id]
      , 'DateEnd' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM @NewList [nl]
    WHERE [nl].[DateEnd] IS NOT NULL AND [nl].[DateStart] > [nl].[DateEnd]

    -- 'DateStartError' / 'DateEndError'
    INSERT @ValidationResult
    SELECT
        [Q].[Id] AS [Id]
      , [ca].[Field] AS [Field]
      , [ca].[Field]+'Error' AS [MessageCode]
      , CONVERT(NVARCHAR(MAX), [ca].[Date], 126) AS [AdditionalValue]
    FROM (
        SELECT
            [nl].[Id]
          , [nl].[DateStart]
          , [nl].[DateEnd]
          , (SELECT MAX([DateSatrt])
            FROM ( VALUES
              (NULL)
              , ([mst].[DateStart])
              , ([est].[DateStart])
            ) MaxDate([DateSatrt])) AS [MinimumPossibleDate]
          , (SELECT MIN([DateEnd])
            FROM ( VALUES
              (NULL)
              , ([mst].[DateEnd])
              , ([est].[DateEnd])
            ) MinDate([DateEnd])) AS [MaximumPossibleDate]
        FROM @NewList [nl]
        LEFT JOIN [Common].[Organization] [mst] ON @MasterColumnName = '[OrganizationId]' AND [mst].[Id] = @MasterId
        LEFT JOIN [Common].[Organization] [est] ON @MasterColumnName <> '[OrganizationId]' AND [est].[Id] = [nl].[ElementId]
    ) AS Q
    CROSS APPLY (
        VALUES ('DateStart', [MinimumPossibleDate])
             , ('DateEnd',   [MaximumPossibleDate])
    ) AS [ca]([Field], [Date])
    WHERE [ca].[Field] = 'DateStart' AND [Q].[DateStart] < [ca].[Date]
        OR [ca].[Field] = 'DateEnd' AND (
            [Q].[DateEnd] IS NULL AND [ca].[Date] IS NOT NULL
            OR [Q].[DateEnd] > COALESCE([ca].[Date], [Q].[DateEnd])
        )
    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END  
    
    DECLARE @MaxAllovedDateValue SMALLDATETIME = '2079-06-06 23:59:00'

    SET @MessageCode = 'DateOverlapError';
    INSERT @ValidationResult
    SELECT
        [nl1].[Id] AS [Id]
      , 'Element' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST([nl2].[Id] AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM @NewList AS [nl1]
    LEFT JOIN @NewList AS [nl2] ON @MasterColumnName = '[UserId]'
                            AND [nl1].[Id] <> [nl2].[Id]
                            AND [nl1].[ElementId] = [nl2].[ElementId]

    WHERE [nl1].[DateStart] <= ISNULL([nl2].[DateEnd], @MaxAllovedDateValue)
      AND ISNULL([nl1].[DateEnd], @MaxAllovedDateValue) >= [nl2].[DateStart];

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END

    ;WITH
    TargetCTE AS (
        SELECT
            [T].[Id]
          , [T].[ModuleId]
          , [T].[OrganizationId]
          , [T].[DateStart]
          , [T].[DateEnd]
          , [T].[ChangerId]
          , [T].[TimeEnd]
        FROM [Common].[Module_Organization] [T]
        WHERE (  @MasterColumnName = '[ModuleId]' AND [T].[ModuleId] = @MasterId
            OR @MasterColumnName = '[OrganizationId]' AND [T].[OrganizationId] = @MasterId)
            AND [T].[IsDeleted] = 0
    ),
    SourceCTE AS (
        SELECT
            [nl].[Id] AS [Id]
          , IIF(@MasterColumnName='[ModuleId]', @MasterId, [nl].[ElementId]) AS [ModuleId]
          , IIF(@MasterColumnName='[OrganizationId]', @MasterId, [nl].[ElementId]) AS [OrganizationId]
          , [nl].[DateStart] AS [DateStart]
          , [nl].[DateEnd] AS [DateEnd]
        FROM @NewList [nl]
    )
    MERGE TargetCTE AS [T]
    USING SourceCTE AS [S]
    ON [T].[Id] = [S].[Id]
    WHEN NOT MATCHED BY TARGET THEN
        INSERT ([ModuleId], [OrganizationId], [DateStart], [DateEnd], [ChangerId])
        VALUES ([S].[ModuleId], [S].[OrganizationId], [S].[DateStart], [S].[DateEnd], @ChangerId)
    WHEN MATCHED AND 
          (  [T].[ModuleId] <> [S].[ModuleId]
          OR [T].[OrganizationId] <> [S].[OrganizationId]
          OR [T].[DateStart] <> [S].[DateStart]
          OR COALESCE([T].[DateEnd], @MaxAllovedDateValue) <> COALESCE([S].[DateEnd], @MaxAllovedDateValue) ) THEN
        UPDATE SET
            [T].[ModuleId] = [S].[ModuleId]
          , [T].[OrganizationId] = [S].[OrganizationId]
          , [T].[DateStart] = [S].[DateStart]
          , [T].[DateEnd] = [S].[DateEnd]
          , [T].[ChangerId] = @ChangerId
    WHEN NOT MATCHED BY SOURCE THEN
        UPDATE SET
            [T].[ChangerId] = @ChangerId
          , [T].[TimeEnd] = GETUTCDATE();

    SET NOCOUNT OFF;
END