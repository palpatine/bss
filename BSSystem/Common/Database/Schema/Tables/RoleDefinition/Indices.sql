﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[RoleDefinition]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
