﻿CREATE TABLE [Common].[RoleDefinition] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]        INT              NOT NULL,
    [TableKey]       VARCHAR (100)    NOT NULL,
    [DisplayName]    NVARCHAR (25)    NOT NULL,
    [DateStart]      SMALLDATETIME    NOT NULL,
    [DateEnd]        SMALLDATETIME    NULL,
    [Comment]        VARCHAR (MAX)    NULL,
    [ChangerId]      INT              NOT NULL,
    [Number]         UNIQUEIDENTIFIER CONSTRAINT [DF_RoleDefinition_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_RoleDefinition_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]        DATETIME         NULL,
    CONSTRAINT [PK_RoleDefinition] PRIMARY KEY ([Id])
);
