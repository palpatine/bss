﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[CustomFieldDefinition]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
