﻿CREATE TABLE [Common].[EntityStatus] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [Value]       VARCHAR (25)     NOT NULL,
    CONSTRAINT [PK_EntityStatus] PRIMARY KEY ([Id])
);
