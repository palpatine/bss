﻿ALTER TABLE [Common].[Organization_User]
ADD CONSTRAINT [FK_Organization_User_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Organization_User]
ADD CONSTRAINT [FK_Organization_User_OrganizationId] FOREIGN KEY ([OrganizationId]) 
  REFERENCES [Common].[Organization] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Organization_User]
ADD CONSTRAINT [FK_Organization_User_RoleDefinitionId] FOREIGN KEY ([RoleDefinitionId]) 
  REFERENCES [Common].[RoleDefinition] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
