﻿CREATE INDEX [IX_Permission_RoleDefinition_1]
    ON [Common].[Permission_RoleDefinition]([RoleDefinitionId], [PermissionId])
    INCLUDE ([IsDeleted])