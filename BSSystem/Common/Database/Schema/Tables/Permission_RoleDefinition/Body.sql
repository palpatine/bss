﻿CREATE TABLE [Common].[Permission_RoleDefinition] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]          INT              NOT NULL,
    [RoleDefinitionId] INT              NOT NULL,
    [PermissionId]     INT              NOT NULL,
    [ChangerId]        INT              NOT NULL,
    [Number]           UNIQUEIDENTIFIER CONSTRAINT [DF_Permission_RoleDefinition_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]        DATETIME         CONSTRAINT [DF_Permission_RoleDefinition_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]          DATETIME         NULL,
    CONSTRAINT [PK_Permission_RoleDefinition] PRIMARY KEY ([Id])
);
