﻿ALTER TABLE [Common].[Permission_RoleDefinition]
ADD CONSTRAINT [FK_Permission_RoleDefinition_PermissionId] FOREIGN KEY ([PermissionId]) 
  REFERENCES [Common].[Permission] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Permission_RoleDefinition]
ADD CONSTRAINT [FK_Permission_RoleDefinition_RoleDefinitionId] FOREIGN KEY ([RoleDefinitionId]) 
  REFERENCES [Common].[RoleDefinition] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
