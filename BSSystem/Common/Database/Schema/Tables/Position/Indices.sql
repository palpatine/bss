﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[Position]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
