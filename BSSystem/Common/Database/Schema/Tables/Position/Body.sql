﻿CREATE TABLE [Common].[Position] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]        INT              NOT NULL,
    [DisplayName]    NVARCHAR (25)    NOT NULL,
    [Name]           NVARCHAR (MAX)   NOT NULL,
    [DateStart]      SMALLDATETIME    NOT NULL,
    [DateEnd]        SMALLDATETIME    NULL,
    [ChangerId]      INT              NOT NULL,
    [Number]         UNIQUEIDENTIFIER CONSTRAINT [DF_Position_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_Position_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]        DATETIME         NULL,
    CONSTRAINT [PK_Position] PRIMARY KEY ([Id])
);
