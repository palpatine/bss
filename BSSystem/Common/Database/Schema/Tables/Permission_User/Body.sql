﻿CREATE TABLE [Common].[Permission_User] (
    [Id]           INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]      INT              NOT NULL,
    [UserId]       INT              NOT NULL,
    [PermissionId] INT              NOT NULL,
    [ChangerId]    INT              NOT NULL,
    [Number]       UNIQUEIDENTIFIER CONSTRAINT [DF_Permission_User_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]    DATETIME         CONSTRAINT [DF_Permission_User_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]      DATETIME         NULL,
    CONSTRAINT [PK_Permission_User] PRIMARY KEY ([Id])
);
