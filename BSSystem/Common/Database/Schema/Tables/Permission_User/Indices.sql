﻿CREATE UNIQUE NONCLUSTERED INDEX [IX_Permission_User_UserId_PermissionId] ON [Common].[Permission_User]
(	
	[UserId] ASC,
	[PermissionId] ASC
)
WHERE ([TimeEnd] IS NULL)