﻿ALTER TABLE [Common].[Permission_User]
ADD CONSTRAINT [FK_Permission_User_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Permission_User]
ADD CONSTRAINT [FK_Permission_User_PermissionId] FOREIGN KEY ([PermissionId]) 
  REFERENCES [Common].[Permission] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION

