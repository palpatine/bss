﻿CREATE TABLE [Common].[Module_Organization] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [OrganizationId] INT              NOT NULL,
    [ModuleId]       INT              NOT NULL,
    [DateStart]      SMALLDATETIME    NOT NULL,
    [DateEnd]        SMALLDATETIME    NULL,
    [ChangerId]      INT              NOT NULL,
    [Number]         UNIQUEIDENTIFIER CONSTRAINT [DF_Module_Organization_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_Module_Organization_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]        DATETIME         NULL,
    CONSTRAINT [PK_Module_Organization] PRIMARY KEY ([Id])
);
