﻿ALTER TABLE [Common].[Module_Organization]
ADD CONSTRAINT [FK_Module_Organization_OrganizationId] FOREIGN KEY ([OrganizationId]) 
  REFERENCES [Common].[Organization] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Module_Organization]
ADD CONSTRAINT [FK_Module_Organization_ModuleId] FOREIGN KEY ([ModuleId]) 
  REFERENCES [Common].[Module] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
