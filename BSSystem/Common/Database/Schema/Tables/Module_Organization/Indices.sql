﻿CREATE INDEX [IX_Module_Organization_1]
    ON [Common].[Module_Organization] ([OrganizationId], [ModuleId])
    INCLUDE ([DateStart], [DateEnd], [IsDeleted])