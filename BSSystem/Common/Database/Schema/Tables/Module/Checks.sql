﻿ALTER TABLE [Common].[Module]
    ADD CONSTRAINT [CK_Module_Required]
    CHECK ([Common].[IsRequiredModulesExists]([Required]) = 1)