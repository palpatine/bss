﻿CREATE TABLE [Common].[Module] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [Key]            VARCHAR (50)   NOT NULL,
    [Required]       VARCHAR(900)   NULL,
    [MajorVersion]   INT            NOT NULL,
    [DevVersion]     INT            NOT NULL, 
    [RevisionNumber] VARCHAR(30)    NULL,
    [Comment]        VARCHAR (MAX)  NULL
    CONSTRAINT [PK_Module] PRIMARY KEY ([Id])
);
