﻿ALTER TABLE [Common].[Setting]
ADD CONSTRAINT [FK_Setting_SystemSettingId] FOREIGN KEY ([SystemSettingId]) 
  REFERENCES [Common].[SystemSetting] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION