﻿CREATE TABLE [Common].[Setting] (
    [Id]                   INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]              INT              NOT NULL,
    [SystemSettingId]      INT              NOT NULL,
    [Value]                NVARCHAR (MAX)   NULL,
    [CanBeOverridenByUser] BIT              NOT NULL,
    [ChangerId]            INT              NOT NULL,
    [Number]               UNIQUEIDENTIFIER CONSTRAINT [DF_Setting_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]            DATETIME         CONSTRAINT [DF_Setting_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]              DATETIME         NULL,
    CONSTRAINT [PK_Setting] PRIMARY KEY ([Id])
);
