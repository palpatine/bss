﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_JunctionLock]
    ON [Common].[JunctionLock]([TableName], [ColumnName], [ColumnValue]);
