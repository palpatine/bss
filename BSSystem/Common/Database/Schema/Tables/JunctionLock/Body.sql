﻿CREATE TABLE [Common].[JunctionLock] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [OperationToken] VARCHAR (40)  NOT NULL,
    [TableName]      VARCHAR (100) NOT NULL,
    [ColumnName]     VARCHAR (50)  NOT NULL,
    [ColumnValue]    INT           NOT NULL,
    [SessionId]      INT           NOT NULL,
    [UserId]         INT           NOT NULL,
    [TimeStart]      DATETIME      CONSTRAINT [DF_JunctionLock_TimeStart] DEFAULT (getutcdate()) NOT NULL, 
    CONSTRAINT [PK_JunctionLock] PRIMARY KEY ([Id])
);
