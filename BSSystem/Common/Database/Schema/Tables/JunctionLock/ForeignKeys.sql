﻿ALTER TABLE [Common].[JunctionLock]
ADD CONSTRAINT [FK_JunctionLock_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[JunctionLock]
ADD CONSTRAINT [FK_JunctionLock_SessionId] FOREIGN KEY ([SessionId]) 
  REFERENCES [Common].[Session] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
