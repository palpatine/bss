﻿CREATE TABLE [Common].[UserSetting] (
    [Id]                INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]           INT              NOT NULL,
    [UserId]            INT              NOT NULL,
    [SystemSettingId]   INT              NOT NULL,
    [Value]             NVARCHAR (MAX)   NULL,
    [ChangerId]         INT              NOT NULL,
    [Number]            UNIQUEIDENTIFIER CONSTRAINT [DF_UserSetting_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]         DATETIME         CONSTRAINT [DF_UserSetting_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]           DATETIME         NULL,
    CONSTRAINT [PK_UserSetting] PRIMARY KEY ([Id])
);
