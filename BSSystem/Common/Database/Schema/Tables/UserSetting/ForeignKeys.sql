﻿ALTER TABLE [Common].[UserSetting]
ADD CONSTRAINT [FK_UserSetting_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[UserSetting]
ADD CONSTRAINT [FK_UserSetting_SystemSettingId] FOREIGN KEY ([SystemSettingId]) 
  REFERENCES [Common].[SystemSetting] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
