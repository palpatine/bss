﻿CREATE TABLE [Common].[Permission] (
    [Id]                 INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]            INT              NOT NULL,
    [SystemPermissionId] INT              NULL,
    [CustomPermissionId] INT              NULL,
    [ChangerId]          INT              NOT NULL,
    [Number]             UNIQUEIDENTIFIER CONSTRAINT [DF_Permission_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]          DATETIME         CONSTRAINT [DF_Permission_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]            DATETIME         NULL,
    CONSTRAINT [PK_Permission] PRIMARY KEY ([Id])
);
