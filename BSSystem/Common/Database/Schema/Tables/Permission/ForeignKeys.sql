﻿ALTER TABLE [Common].[Permission]
ADD CONSTRAINT [FK_Permission_SystemPermissionId] FOREIGN KEY ([SystemPermissionId]) 
  REFERENCES [Common].[SystemPermission] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Permission]
ADD CONSTRAINT [FK_Permission_CustomPermissionId] FOREIGN KEY ([CustomPermissionId]) 
  REFERENCES [Common].[CustomPermission] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
