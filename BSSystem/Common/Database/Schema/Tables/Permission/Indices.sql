﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_ReferencePermission]
    ON [Common].[Permission]([ScopeId], [SystemPermissionId], [CustomPermissionId]) WHERE ([TimeEnd] IS NULL);