﻿ALTER TABLE [Common].[Permission]
    ADD CONSTRAINT [CK_Permission_SystemPermissionOrCustomPermissionReference]
    CHECK ([SystemPermissionId] IS NULL AND [CustomPermissionId] IS NOT NULL
        OR [SystemPermissionId] IS NOT NULL AND [CustomPermissionId] IS NULL);