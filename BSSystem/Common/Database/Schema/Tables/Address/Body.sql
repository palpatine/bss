﻿CREATE TABLE [Common].[Address] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [Category]    INT              NULL,
    [Street]      NVARCHAR (15)    NOT NULL,
    [HouseNumber] NVARCHAR (5)     NOT NULL,
    [FlatNumber]  INT              NULL,
    [City]        NVARCHAR (MAX)   NOT NULL,
    [Postcode]    DECIMAL (5)      NOT NULL,
    [Country]     NVARCHAR (MAX)   NOT NULL,
    [Comment]     NVARCHAR (MAX)   NULL,
    [ChangerId]   INT              NOT NULL,
    [Number]      UNIQUEIDENTIFIER CONSTRAINT [DF_Address_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]   DATETIME         CONSTRAINT [DF_Address_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]     DATETIME         NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY ([Id])
);
