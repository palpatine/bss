﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_Login]
    ON [Common].[Login]([ScopeId], [LoginName]) WHERE ([TimeEnd] IS NULL);
