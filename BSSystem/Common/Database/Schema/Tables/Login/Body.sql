﻿CREATE TABLE [Common].[Login] (
    [Id]                 INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]            INT              NOT NULL,
    [UserId]             INT              NOT NULL,
    [LoginName]          NVARCHAR (20)    NOT NULL,
    [Password]           VARBINARY (64)   NULL,
    [Salt]               VARBINARY (32)   NULL,
    [ChangerId]          INT              NOT NULL,
    [Number]             UNIQUEIDENTIFIER CONSTRAINT [DF_Login_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]          DATETIME      CONSTRAINT [DF_Login_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]            DATETIME         NULL,
    CONSTRAINT [PK_Login] PRIMARY KEY ([Id])
);
