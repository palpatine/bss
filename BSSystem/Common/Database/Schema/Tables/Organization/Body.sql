﻿CREATE TABLE [Common].[Organization] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [DisplayName] NVARCHAR (25)    NOT NULL,
    [CompanyId]   INT              NOT NULL,
    [DateStart]   SMALLDATETIME    NOT NULL,
    [DateEnd]     SMALLDATETIME    NULL,
    [ChangerId]   INT              NOT NULL,
    [Number]      UNIQUEIDENTIFIER CONSTRAINT [DF_Organization_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]   DATETIME         CONSTRAINT [DF_Organization_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]     DATETIME         NULL,
    CONSTRAINT [PK_Organization] PRIMARY KEY ([Id])
);
