﻿ALTER TABLE [Common].[Organization]
ADD CONSTRAINT [FK_Organization_CompanyId] FOREIGN KEY ([CompanyId]) 
  REFERENCES [Common].[Company] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO