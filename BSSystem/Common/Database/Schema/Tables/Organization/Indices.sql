﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[Organization]([DisplayName])
    WHERE ([TimeEnd] IS NULL);
