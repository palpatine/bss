﻿CREATE TABLE [Common].[AddressCompany] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [AddressId]   INT           NOT NULL,
    [CompanyId]   INT           NOT NULL,
    [DisplayName] NVARCHAR (25) NOT NULL, 
    CONSTRAINT [PK_AddressCompany] PRIMARY KEY ([Id])
);
