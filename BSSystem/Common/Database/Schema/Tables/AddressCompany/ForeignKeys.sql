﻿ALTER TABLE [Common].[AddressCompany]
ADD CONSTRAINT [FK_AddressCompany_AddressId] FOREIGN KEY ([AddressId]) 
  REFERENCES [Common].[Address] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[AddressCompany]
ADD CONSTRAINT [FK_AddressCompany_CompanyId] FOREIGN KEY ([CompanyId]) 
  REFERENCES [Common].[Company] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
