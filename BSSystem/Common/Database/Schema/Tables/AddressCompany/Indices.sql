﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[AddressCompany]([CompanyId], [DisplayName]);
