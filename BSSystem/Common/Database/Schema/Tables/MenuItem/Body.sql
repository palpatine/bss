﻿CREATE TABLE [Common].[MenuItem] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [Key]		     NVARCHAR (50) NOT NULL,
    [MenuSectionId]	 INT           NOT NULL,
    [Order]          FLOAT         NOT NULL,
    [IconName]       NVARCHAR(MAX) NULL,
    [PermissionPath] VARCHAR (900) NULL
    CONSTRAINT [PK_MenuItem] PRIMARY KEY ([Id])
);
