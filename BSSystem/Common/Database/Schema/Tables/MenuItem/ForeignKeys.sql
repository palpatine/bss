﻿ALTER TABLE [Common].[MenuItem]
ADD CONSTRAINT [FK_MenuItem_MenuSectionId] FOREIGN KEY ([MenuSectionId]) 
  REFERENCES [Common].[MenuSection] (Id) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION