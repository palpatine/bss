﻿CREATE TABLE [Common].[Position_User] (
    [Id]         INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]    INT              NOT NULL,
    [UserId]     INT              NOT NULL,
    [PositionId] INT              NOT NULL,
    [DateStart]  SMALLDATETIME    NOT NULL,
    [DateEnd]    SMALLDATETIME    NULL,
    [ChangerId]  INT              NOT NULL,
    [Number]     UNIQUEIDENTIFIER CONSTRAINT [DF_Position_User_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_Position_User_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]    DATETIME         NULL,
    CONSTRAINT [PK_Position_User] PRIMARY KEY ([Id])
);
