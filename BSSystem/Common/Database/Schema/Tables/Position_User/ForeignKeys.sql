﻿ALTER TABLE [Common].[Position_User]
ADD CONSTRAINT [FK_Position_User_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Position_User]
ADD CONSTRAINT [FK_Position_User_PositionId] FOREIGN KEY ([PositionId]) 
  REFERENCES [Common].[Position] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
