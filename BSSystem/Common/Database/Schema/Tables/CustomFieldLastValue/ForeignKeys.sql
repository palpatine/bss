﻿ALTER TABLE [Common].[CustomFieldLastValue]
ADD CONSTRAINT [FK_CustomFieldLastValue_CustomFieldDefinitionId] FOREIGN KEY ([CustomFieldDefinitionId]) 
  REFERENCES [Common].[CustomFieldDefinition] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
