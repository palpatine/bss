﻿CREATE TABLE [Common].[CustomFieldLastValue] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [CustomFieldDefinitionId] INT            NOT NULL,
    [ConstPart]        NVARCHAR (100) NULL,
    [IdentityPart]     INT            NOT NULL, 
    CONSTRAINT [PK_CustomFieldLastValue] PRIMARY KEY ([Id])
);
