﻿ALTER TABLE [Common].[SettingPanel]
ADD CONSTRAINT [FK_SettingPanel_MenuItemId] FOREIGN KEY ([MenuItemId]) 
  REFERENCES [Common].[MenuItem] (Id) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION