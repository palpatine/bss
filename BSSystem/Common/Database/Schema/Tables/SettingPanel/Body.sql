﻿CREATE TABLE [Common].[SettingPanel] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [Key]		     NVARCHAR (50) NOT NULL,
    [MenuItemId]	 INT           NOT NULL,
    [Order]          FLOAT         NOT NULL,
    [PermissionPath] VARCHAR (900) NULL
    CONSTRAINT [PK_SettingPanel] PRIMARY KEY ([Id])
);
