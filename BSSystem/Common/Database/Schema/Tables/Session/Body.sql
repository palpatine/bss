﻿CREATE TABLE [Common].[Session] (
    [Id]        INT              IDENTITY (1, 1) NOT NULL,
    [Guid]      UNIQUEIDENTIFIER NOT NULL,
    [UserId]    INT              NOT NULL,
    [RemoteIP]  BINARY (4)       NULL,
    [TimeStart] DATETIME         NOT NULL,
    [TimeEnd]   DATETIME         NOT NULL, 
    CONSTRAINT [PK_Session] PRIMARY KEY ([Id])
);
