﻿CREATE TRIGGER [Common].[Sessions_DeleteObsoleteLocks] ON [Common].[Session]
WITH EXECUTE AS CALLER
FOR INSERT, UPDATE
AS
BEGIN
  DELETE [l]
  FROM [Common].[Lock] AS [l]
  INNER JOIN
  (
  	SELECT [Id]
    FROM [Common].[Session]
    WHERE [TimeEnd] <= GETUTCDATE()
  ) AS [s] ON [s].[Id] = [l].[SessionId]

  DELETE [jl]
  FROM [Common].[JunctionLock] AS [jl]
  INNER JOIN
  (
  	SELECT [Id]
    FROM [Common].[Session]
    WHERE [TimeEnd] <= GETUTCDATE()
  ) AS [s] ON [s].[Id] = [jl].[SessionId]
END
