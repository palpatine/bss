﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[Section]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
