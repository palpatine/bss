﻿ALTER TABLE [Common].[Section]
ADD CONSTRAINT [FK_Section_ParentId] FOREIGN KEY ([ParentId]) 
  REFERENCES [Common].[Section] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION