﻿CREATE TABLE [Common].[Company] (
    [Id]              INT              IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (MAX)   NOT NULL,
    [FullName]        NVARCHAR (MAX)   NOT NULL,
    [CountryCode]     VARCHAR (3)      NULL,
    [VatNumber]       NVARCHAR (MAX)   NULL,
    [OfficialNumber]  NVARCHAR (MAX)   NULL,
    [StatisticNumber] NVARCHAR (MAX)   NULL,
    [Phone]           DECIMAL (11)     NULL,
    [Fax]             DECIMAL (11)     NULL,
    [Email]           NVARCHAR (MAX)   NULL,
    [ChangerId]       INT              NOT NULL,
    [Number]          UNIQUEIDENTIFIER CONSTRAINT [DF_Company_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]       DATETIME         CONSTRAINT [DF_Company_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]         DATETIME         NULL,
    CONSTRAINT [PK_Company] PRIMARY KEY ([Id])
);
