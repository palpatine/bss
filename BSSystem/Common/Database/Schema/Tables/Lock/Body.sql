﻿CREATE TABLE [Common].[Lock] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [OperationToken] VARCHAR (40)  NOT NULL,
    [TableName]      VARCHAR (100) NOT NULL,
    [RecordId]       INT           NOT NULL,
    [SessionId]      INT           NOT NULL,
    [UserId]         INT           NOT NULL,
    [TimeStart]      DATETIME      CONSTRAINT [DF_Lock_TimeStart] DEFAULT (getutcdate()) NOT NULL, 
    CONSTRAINT [PK_Lock] PRIMARY KEY ([Id])
);
