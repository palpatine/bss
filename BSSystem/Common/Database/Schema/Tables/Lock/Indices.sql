﻿ALTER TABLE [Common].[Lock] ADD CONSTRAINT [UQ_Lock] 
UNIQUE NONCLUSTERED ([TableName], [RecordId]);
