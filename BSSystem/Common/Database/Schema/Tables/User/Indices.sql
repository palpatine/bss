﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[User]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
GO

CREATE INDEX [IX_User_1]
    ON [Common].[User] ([Id], [ScopeId], [DateStart], [DateEnd], [IsDeleted])