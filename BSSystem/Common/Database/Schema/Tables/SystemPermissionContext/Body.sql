﻿CREATE TABLE [Common].[SystemPermissionContext] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [SystemPermissionId] INT           NOT NULL,
    [TableKey]    VARCHAR (100) NOT NULL, 
    CONSTRAINT [PK_SystemPermissionContext] PRIMARY KEY ([Id])
);
