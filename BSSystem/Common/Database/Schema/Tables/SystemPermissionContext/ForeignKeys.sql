﻿ALTER TABLE [Common].[SystemPermissionContext]
ADD CONSTRAINT [FK_SystemPermissionContext_SystemPermissionId] FOREIGN KEY ([SystemPermissionId]) 
  REFERENCES [Common].[SystemPermission] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
