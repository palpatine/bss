﻿ALTER TABLE [Common].[AddressUser]
ADD CONSTRAINT [FK_AddressUser_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[AddressUser]
ADD CONSTRAINT [FK_AddressUser_AddressId] FOREIGN KEY ([AddressId]) 
  REFERENCES [Common].[Address] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
