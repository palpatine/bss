﻿CREATE TABLE [Common].[AddressUser] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [AddressId]   INT           NOT NULL,
    [UserId]      INT           NOT NULL,
    [DisplayName] NVARCHAR (25) NOT NULL, 
    CONSTRAINT [PK_AddressUser] PRIMARY KEY ([Id])
);
