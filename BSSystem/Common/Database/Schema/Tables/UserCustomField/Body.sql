﻿CREATE TABLE [Common].[UserCustomField] (
    [Id]                      INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]                 INT              NOT NULL,
    [UserId]                  INT              NOT NULL,
    [CustomFieldDefinitionId] INT              NOT NULL,
    [Value]                   NVARCHAR (MAX)   NULL,
    [ChangerId]               INT              NOT NULL,
    [Number]                  UNIQUEIDENTIFIER CONSTRAINT [DF_UserCustomField_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]               DATETIME         CONSTRAINT [DF_UserCustomField_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]                 DATETIME         NULL,
    CONSTRAINT [PK_UserCustomField] PRIMARY KEY ([Id])
);
