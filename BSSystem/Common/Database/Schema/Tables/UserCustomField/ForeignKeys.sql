﻿ALTER TABLE [Common].[UserCustomField]
ADD CONSTRAINT [FK_UserCustomField_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[UserCustomField]
ADD CONSTRAINT [FK_UserCustomField_CustomFieldDefinitionId] FOREIGN KEY ([CustomFieldDefinitionId]) 
  REFERENCES [Common].[CustomFieldDefinition] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
