﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[CustomPermission]([ScopeId], [ParentId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
GO

