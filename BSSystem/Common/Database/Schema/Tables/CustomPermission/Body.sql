﻿CREATE TABLE [Common].[CustomPermission] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]        INT              NOT NULL,
    [DisplayName]    NVARCHAR(25)     NOT NULL,
    [Description]    NVARCHAR(MAX)    NULL,
    [ParentId]       INT              NULL,
    [IsContainer]    BIT              NOT NULL,
    [ChangerId]      INT              NOT NULL,
    [Number]         UNIQUEIDENTIFIER CONSTRAINT [DF_CustomPermission_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_CustomPermission_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]        DATETIME         NULL,
    CONSTRAINT [PK_CustomPermission] PRIMARY KEY ([Id])
);
