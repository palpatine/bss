﻿ALTER TABLE [Common].[CustomPermission]
ADD CONSTRAINT [FK_CustomPermission_ParentId] FOREIGN KEY ([ParentId]) 
  REFERENCES [Common].[CustomPermission] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION