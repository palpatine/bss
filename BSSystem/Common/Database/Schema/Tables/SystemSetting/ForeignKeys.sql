﻿ALTER TABLE [Common].[SystemSetting]
ADD CONSTRAINT [FK_SystemSetting_SettingPanelId] FOREIGN KEY ([SettingPanelId]) 
  REFERENCES [Common].[SettingPanel] (Id) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION

  ALTER TABLE [Common].[SystemSetting]
ADD CONSTRAINT [FK_SystemSetting_ModuleId] FOREIGN KEY ([ModuleId]) 
  REFERENCES [Common].[Module] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION