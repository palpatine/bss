﻿CREATE TABLE [Common].[SystemSetting] (
    [Id]                           INT            IDENTITY (1, 1) NOT NULL,
    [ModuleId]				       INT            NOT NULL,
    [SettingPanelId]               INT            NOT NULL,
    [Key]                          NVARCHAR (50)  NOT NULL,
    [EditorType]                   NVARCHAR (MAX) NOT NULL,
    [EditorParameter]              NVARCHAR (MAX) NOT NULL,
    [DefaultValue]                 NVARCHAR (MAX) NULL,
    [CanBeOverridenByOrganization] BIT            NOT NULL,
    [CanBeOverridenByUser]         BIT            NOT NULL,
    CONSTRAINT [PK_SystemSetting] PRIMARY KEY ([Id])
);
