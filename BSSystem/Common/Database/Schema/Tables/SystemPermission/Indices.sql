﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_Path]
    ON [Common].[SystemPermission]([Path]);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_RootContainer]
    ON [Common].[SystemPermission]([ParentId], [Key]);
GO

CREATE NONCLUSTERED INDEX [IX_SystemPermission_Path]
    ON [Common].[SystemPermission]([Path])
    INCLUDE ([Id])