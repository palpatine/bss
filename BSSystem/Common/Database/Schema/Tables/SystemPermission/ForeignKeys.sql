﻿ALTER TABLE [Common].[SystemPermission]
ADD CONSTRAINT [FK_SystemPermission_ParentId] FOREIGN KEY ([ParentId]) 
  REFERENCES [Common].[SystemPermission] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[SystemPermission]
ADD CONSTRAINT [FK_SystemPermission_ModuleId] FOREIGN KEY ([ModuleId]) 
  REFERENCES [Common].[Module] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
