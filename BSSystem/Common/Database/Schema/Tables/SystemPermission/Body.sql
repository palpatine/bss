﻿CREATE TABLE [Common].[SystemPermission] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [ModuleId]              INT           NOT NULL,
    [Key]                   VARCHAR(50)   NOT NULL,
    [ParentId]              INT           NULL,
    [Path]                  VARCHAR (900) NOT NULL,
    [IsUserPermission]      BIT           NOT NULL,
    [IsContainer]           BIT           NOT NULL,
    [IsOrganizationDefault] BIT           NOT NULL,
    CONSTRAINT [PK_SystemPermission] PRIMARY KEY ([Id])
);
