﻿CREATE TABLE [Common].[Section_User] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]        INT              NOT NULL,
    [UserId]           INT              NOT NULL,
    [SectionId]        INT              NOT NULL,
    [RoleDefinitionId] INT              NOT NULL,
    [JobTime]          DECIMAL (28, 3)  NULL,
    [JobHours]         INT              NULL,
    [DateStart]        SMALLDATETIME    NOT NULL,
    [DateEnd]          SMALLDATETIME    NULL,
    [ChangerId]        INT              NOT NULL,
    [Number]           UNIQUEIDENTIFIER CONSTRAINT [DF_Section_User_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]        DATETIME         CONSTRAINT [DF_Section_User_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]          DATETIME         NULL,
    CONSTRAINT [PK_Section_User] PRIMARY KEY ([Id])
);
