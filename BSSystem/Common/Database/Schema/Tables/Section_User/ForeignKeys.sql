﻿ALTER TABLE [Common].[Section_User]
ADD CONSTRAINT [FK_Section_User_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Section_User]
ADD CONSTRAINT [FK_Section_User_SectionId] FOREIGN KEY ([SectionId]) 
  REFERENCES [Common].[Section] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Common].[Section_User]
ADD CONSTRAINT [FK_Section_User_RoleDefinitionId] FOREIGN KEY ([RoleDefinitionId]) 
  REFERENCES [Common].[RoleDefinition] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
