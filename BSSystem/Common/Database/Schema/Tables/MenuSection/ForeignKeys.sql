﻿ALTER TABLE [Common].[MenuSection]
ADD CONSTRAINT [FK_MenuSection_ParentId] FOREIGN KEY ([ParentId]) 
  REFERENCES [Common].[MenuSection] (Id) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION