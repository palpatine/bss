﻿CREATE TABLE [Common].[MenuSection] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [Key]		         NVARCHAR (50) NOT NULL,
    [Path]               VARCHAR (900) NULL,
    [ParentId]           INT           NULL,
    [NavigationPosition] TINYINT       NOT NULL,
    [Order]              FLOAT         NOT NULL,
    [IconName]           NVARCHAR(MAX) NULL,
    CONSTRAINT [PK_MenuSection] PRIMARY KEY ([Id])
);
