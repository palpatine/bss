﻿CREATE FUNCTION [Common].[GetRoleDefinitionAssociatedTableKey](
    @RoleDefinitionId INT)
RETURNS NVARCHAR(100)
    WITH SCHEMABINDING
AS
BEGIN
    RETURN(SELECT [rd].[TableKey]
           FROM [Common].[RoleDefinition] AS [rd]
           WHERE [Id] = @RoleDefinitionId);
END;
