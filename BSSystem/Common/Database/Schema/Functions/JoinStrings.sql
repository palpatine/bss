﻿CREATE FUNCTION [Common].[JoinStrings]
(
   @Delimiter NVARCHAR(255),
   @List [Common].[NVarCharList] READONLY
)
RETURNS NVARCHAR(MAX)
    WITH SCHEMABINDING
AS
BEGIN
    DECLARE @result NVARCHAR(MAX)
    SELECT @result = COALESCE(@result, '') + [Value] + @Delimiter
    FROM @List
    RETURN @result
END;
