﻿CREATE FUNCTION [Common].[IsRequiredModulesExists]
(
   @RequiredList VARCHAR(900)
)
RETURNS BIT
    WITH SCHEMABINDING
AS
BEGIN
    DECLARE @result BIT
    SELECT @result = CAST(IIF(
        @RequiredList IS NOT NULL AND
        EXISTS (
            SELECT [m].[Id]
            FROM [Common].[SplitStrings]('|', @RequiredList) AS [x]
            LEFT JOIN [Common].[Module] AS [m] ON [m].[Key] = [x].[Item]
            WHERE [m].[Id] IS NULL
        )
    , 0, 1) AS BIT)
    RETURN @result
END;