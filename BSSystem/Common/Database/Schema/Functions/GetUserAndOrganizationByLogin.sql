﻿CREATE FUNCTION [Common].[GetUserAndOrganizationByLogin](
    @OrganizationName NVARCHAR(25)
  , @LoginName NVARCHAR(20))
RETURNS TABLE
    WITH SCHEMABINDING
AS
RETURN
    WITH
    [UserOrganizationCTE] AS (
        SELECT
            [o].[Id] AS [OrganizationId]
          , [u].[Id] AS [UserId]
          , [u].[ParentId] AS [UserParentId]
          , [u].[RootId] AS [UserRootId]
        FROM [Common].[Organization] AS [o]
        JOIN [Common].[User] AS [u] ON [u].[ScopeId] = [o].[Id]
                                   AND [u].[IsDeleted] = 0
                                   AND [u].[EntityStatusId] = 2
        WHERE [o].[IsDeleted] = 0
          AND [o].[EntityStatusId] = 2
    ),
    [UserCTE] AS (
        SELECT
            [uoc].[UserId]
          , [uoc].[UserRootId]
          , [uoc].[OrganizationId]
        FROM [Common].[Login] AS [l]
        JOIN [UserOrganizationCTE] AS [uoc] ON [uoc].[UserId] = [l].[UserId]
        WHERE [l].[LoginName] = @LoginName
          AND [l].[IsDeleted] = 0
        UNION ALL
        SELECT
            [uoc].[UserId]
          , [uoc].[UserRootId]
          , [uoc].[OrganizationId]
        FROM [UserCTE] AS [uc]
        JOIN [UserOrganizationCTE] AS [uoc] ON [uoc].[UserParentId] = [uc].[UserId]
    )
    SELECT
        [uc].[OrganizationId]
      , [uc].[UserId]
      , [l].[Password]
      , [l].[Salt]
    FROM [UserCTE] AS [uc]
    JOIN [Common].[Organization] AS [o] ON [o].[Id] = [uc].[OrganizationId]
                                       AND [o].[DisplayName] = @OrganizationName
    JOIN [Common].[Permission] AS [cp] ON [cp].[ScopeId] = [uc].[OrganizationId]
                                        AND [cp].[IsDeleted] = 0
    JOIN [Common].[SystemPermission] AS [csp] ON [csp].[Id] = [cp].[SystemPermissionId]
                                        AND [csp].[Path] = 'Common_User|CanLogin'
    JOIN [Common].[Permission_User] AS [pu] ON [pu].[UserId] = [uc].[UserId]
                                           AND [pu].[PermissionId] = [cp].[Id]
                                           AND [pu].[IsDeleted] = 0
    JOIN [Common].[Login] AS [l] ON [l].[UserId] = ISNULL([uc].[UserRootId], [uc].[UserId])
                                AND [l].[IsDeleted] = 0

