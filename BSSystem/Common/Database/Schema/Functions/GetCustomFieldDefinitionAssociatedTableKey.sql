﻿CREATE FUNCTION [Common].[GetCustomFieldDefinitionAssociatedTableKey](
    @CustomFieldDefinitionId INT)
RETURNS NVARCHAR(100)
    WITH SCHEMABINDING
AS
BEGIN
    RETURN(SELECT [sd].[TableKey]
           FROM [Common].[CustomFieldDefinition] AS [sd]
           WHERE [Id] = @CustomFieldDefinitionId);
END;
