﻿CREATE FUNCTION [Common].[GetUserEffectiveSettings](
    @UserId INT
)
RETURNS  TABLE
    WITH SCHEMABINDING
AS
RETURN
WITH
[ActualModulesCTE] AS (
    SELECT
        [o].[Id] AS [OrganizationId]
      , [om].[ModuleId]
    FROM [Common].[User] AS [u]
    JOIN [Common].[Organization] AS [o] ON [o].[Id] = [u].[ScopeId]
                                       AND [o].[IsDeleted] = 0
                                       AND [o].[EntityStatusId] = 2
    JOIN [Common].[Module_Organization] AS [om] ON [om].[OrganizationId] = [o].[Id]
                                               AND [om].[IsDeleted] = 0
                                               AND [om].[EntityStatusId] = 2
    WHERE [u].[IsDeleted] = 0
      AND [u].[EntityStatusId] = 2
      AND [u].[Id] = @UserId )
SELECT
    [ss].[Id]
  , [ss].[Key]
  , COALESCE([us].[Value], [s].[Value], [ss].[DefaultValue]) AS [Value]
FROM [Common].[SystemSetting] AS [ss]
JOIN [ActualModulesCTE] AS [am] ON [am].[ModuleId] = [ss].[ModuleId]
LEFT JOIN [Common].[Setting] AS [s] ON [s].[SystemSettingId] = [ss].[Id]
                                   AND [s].[IsDeleted] = 0
                                   AND [s].[ScopeId] = [am].[OrganizationId]
LEFT JOIN [Common].[UserSetting] AS [us] ON [us].[SystemSettingId] = [ss].[Id]
                                        AND [us].[IsDeleted] = 0
                                        AND [us].[UserId] = @UserId