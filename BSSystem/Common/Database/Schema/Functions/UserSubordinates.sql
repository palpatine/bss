﻿CREATE FUNCTION [Common].[UserSubordinates] (
  @ScopeId INT,
  @ManagerId INT
)
RETURNS TABLE
    WITH SCHEMABINDING
AS
RETURN
WITH
[rootSectionCTE] AS
(
    SELECT [su].[SectionId] AS [Id]
    FROM [Common].[User] AS [u]
    JOIN [Common].[Section_User] AS [su] ON [su].[UserId] = [u].[Id]
                            AND [su].[IsDeleted] = 0
                            AND [su].[EntityStatusId] = 2
    WHERE [u].[ScopeId] = @ScopeId
      AND [u].[IsDeleted] = 0
      AND [u].[Id] = @ManagerId
      AND [u].[EntityStatusId] = 2
),
[RecurenceCTE] (Id) AS (
    SELECT [rs].[Id]
    FROM [rootSectionCTE] AS [rs]
    UNION ALL
    SELECT [s].[Id]
    FROM [RecurenceCTE] AS [rCTE] 
    JOIN [Common].[Section] AS [s] ON [s].[ParentId] = [rCTE].[Id]
                                 AND [s].[ScopeId] = @ScopeId
                                 AND [s].[IsDeleted] = 0
)
SELECT
    [u].[Id]
  , [u].[DisplayName]
  , [su].[DateStart]
  , [su].[DateEnd]
FROM [RecurenceCTE] AS [rc]
JOIN [Common].[Section_User] AS [su] ON [su].[SectionId] = [rc].[Id]
                                    AND [su].[IsDeleted] = 0
JOIN [Common].[User] AS [u] ON [u].[Id] = [su].[UserId]
UNION
SELECT
    [u].[Id]
  , [u].[DisplayName]
  , [su].[DateStart]
  , [su].[DateEnd]
FROM [Common].[Section_User] AS [su]
JOIN [Common].[User] AS [u] ON [u].[Id] = [su].[UserId]
WHERE [su].[UserId] = @ManagerId
    AND [u].[ScopeId] = @ScopeId
    AND [su].[IsDeleted] = 0;
