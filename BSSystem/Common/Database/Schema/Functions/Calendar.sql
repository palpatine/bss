﻿CREATE FUNCTION [Common].[Calendar](
    @Year INT
  , @Month INT)
RETURNS TABLE
    WITH SCHEMABINDING
AS
RETURN
    WITH
    [CalendarCTE] AS (
        SELECT
            DATEFROMPARTS(@Year, @Month, 1) AS [Date]
        UNION ALL
        SELECT
            DATEADD([dd], 1, [Date])
        FROM [CalendarCTE]
        WHERE [Date] < DATEADD([dd], -1, DATEADD([mm], 1, DATEFROMPARTS(@Year, @Month, 1)))
    )
    SELECT
        [c].[Date]
        , CAST(IIF(DATEPART([dw], [c].[Date]) IN(1, 7), 1, 0) AS BIT) AS [IsFreeDay]
    FROM [CalendarCTE] AS [c];
