﻿CREATE FUNCTION [Common].[FreeDaysPl]
(
   @Year int
)
RETURNS @Result TABLE ([Date] SMALLDATETIME)
    WITH SCHEMABINDING
AS
BEGIN
  DECLARE @a INT = ((19 * (@Year % 19) + 24) % 30);
  DECLARE @b INT = (32 + 2 * ((@Year % 100) / 4) - @a - (@year % 100) % 4) % 7;
  DECLARE @c INT = (@Year % 19 + 11 * @a + 22 * @b) / 451;
  DECLARE @d INT = @a + @b - 7 * @c + 114;
  DECLARE @EasterDay SMALLDATETIME = DATEFROMPARTS(@Year, @d / 31, @d % 31 + 1);
  
  INSERT @Result ([Date])
  VALUES
  (DATEFROMPARTS(@Year, 1, 1)),
  (DATEFROMPARTS(@Year, 1, 6)),
  (DATEFROMPARTS(@Year, 5, 1)),
  (DATEFROMPARTS(@Year, 5, 3)),
  (DATEFROMPARTS(@Year, 8, 15)),
  (DATEFROMPARTS(@Year, 11, 1)),
  (DATEFROMPARTS(@Year, 11, 11)),
  (DATEFROMPARTS(@Year, 12, 25)),
  (DATEFROMPARTS(@Year, 12, 26)),
  (@EasterDay),
  (DATEADD(dd,1,@EasterDay)),
  (DATEADD(dd,49,@EasterDay)),
  (DATEADD(dd,60,@EasterDay))
  RETURN
END;
