﻿CREATE FUNCTION [Common].[GetUserPermissions](
    @UserId INT)
RETURNS  TABLE
    WITH SCHEMABINDING
AS
RETURN
    WITH
    [ActualModulesCTE] AS
    (
        SELECT
            [o].[Id] AS [OrganizationId]
          , [om].[ModuleId]
        FROM [Common].[User] AS [u]
        JOIN [Common].[Organization] AS [o] ON [o].[Id] = [u].[ScopeId]
                                        AND [o].[IsDeleted] = 0
                                        AND [o].[EntityStatusId] = 2
        JOIN [Common].[Module_Organization] AS [om] ON [om].[OrganizationId] = [o].[Id]
                                        AND [om].[IsDeleted] = 0
                                        AND [om].[EntityStatusId] = 2
        WHERE [u].[IsDeleted] = 0
          AND [u].[Id] = @UserId
    ),
    [ModulesCTE] AS (
        SELECT
            0 - [m].Id AS [Id]
          , [m].[Key]
          , CAST(NULL AS NVARCHAR(25)) AS [DisplayName]
          , CAST(NULL AS INT) AS [ParentId]
          , CAST(1 AS BIT) AS [IsContainer]
          , CAST(NULL AS INT) AS [JunctionId]
        FROM [ActualModulesCTE] AS [amc]
        JOIN [Common].[Module] AS [m] ON [m].[Id] = [amc].[ModuleId]
    ),
    [SystemPermissionCTE] As (
        SELECT
            [p].[Id]
          , [sp].[Key]
          , CAST(NULL AS NVARCHAR(25)) AS [DisplayName]
          , IIF([pParent].[Id] IS NULL, 0 - [amc].[ModuleId], [pParent].[Id]) AS [ParentId]
          , [sp].[IsContainer]
          , [pu].[Id] AS [JunctionId]
        FROM [ActualModulesCTE] AS [amc]
        JOIN [Common].[SystemPermission] AS [sp] ON [sp].[ModuleId] = [amc].[ModuleId]
        JOIN [Common].[Permission] AS [p] ON [p].[SystemPermissionId] = [sp].[Id]
                                        AND [p].[ScopeId] = [amc].[OrganizationId]
                                        AND [p].[IsDeleted] = 0
        LEFT JOIN [Common].[Permission] AS [pParent] ON [pParent].[SystemPermissionId] = [sp].[ParentId]
                                        AND [pParent].[ScopeId] = [amc].[OrganizationId]
                                        AND [pParent].[IsDeleted] = 0
        LEFT JOIN [Common].[Permission_User] AS [pu] ON [pu].[PermissionId] = [p].[Id]
                                        AND [pu].[IsDeleted] = 0
                                        AND [pu].[UserId] = @UserId
    ),
    [CustomPermissionsCTE] AS (
        SELECT
            [cp].[Id]
          , CAST(NULL AS VARCHAR(50)) AS [Key]
          , [cp].[DisplayName] AS [DisplayName]
          , IIF([pParent].[Id] IS NULL, 2147483647, [pParent].[Id]) AS [ParentId]
          , [cp].[IsContainer]
          , [pu].[Id] AS [JunctionId]
        FROM [ActualModulesCTE] AS [amc]
        JOIN [Common].[CustomPermission] AS [cp] ON [cp].[ScopeId] = [amc].[OrganizationId]
                                        AND [cp].[IsDeleted] = 0
        JOIN [Common].[Permission] AS [p] ON [p].[CustomPermissionId] = [cp].[Id]
                                        AND [p].[ScopeId] = [amc].[OrganizationId]
                                        AND [p].[IsDeleted] = 0
       LEFT JOIN [Common].[Permission] AS [pParent] ON [pParent].[CustomPermissionId] = [cp].[ParentId]
                                        AND [pParent].[ScopeId] = [amc].[OrganizationId]
                                        AND [pParent].[IsDeleted] = 0
        LEFT JOIN [Common].[Permission_User] AS [pu] ON [pu].[PermissionId] = [p].[Id]
                                        AND [pu].[IsDeleted] = 0
                                        AND [pu].[UserId] = @UserId
    ),
    [CustomPermissionContainerCTE] AS (
        SELECT
            CAST(2147483647 AS INT) AS [Id]
          , CAST('Custom' AS VARCHAR(50)) AS [Key]
          , CAST(NULL AS NVARCHAR(25)) AS [DisplayName]
          , CAST(NULL AS INT) AS [ParentId]
          , CAST(1 AS BIT) AS [IsContainer]
          , CAST(NULL AS INT) AS [JunctionId]
        FROM (SELECT TOP 1 [Id] FROM CustomPermissionsCTE) AS [Any]
    )
    
    SELECT [Id], [Key], [DisplayName], [ParentId], [IsContainer], [JunctionId] FROM [ModulesCTE]
    UNION ALL
    SELECT [Id], [Key], [DisplayName], [ParentId], [IsContainer], [JunctionId] FROM [CustomPermissionContainerCTE]
    UNION ALL
    SELECT [Id], [Key], [DisplayName], [ParentId], [IsContainer], [JunctionId] FROM [SystemPermissionCTE]
    UNION ALL
    SELECT [Id], [Key], [DisplayName], [ParentId], [IsContainer], [JunctionId] FROM [CustomPermissionsCTE]