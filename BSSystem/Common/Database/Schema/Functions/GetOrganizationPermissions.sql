﻿Create FUNCTION [Common].[GetOrganizationPermissions](
    @ScopeId INT)
RETURNS  TABLE
    WITH SCHEMABINDING
AS
RETURN
    WITH [NonEndedModulesCTE] AS
    (
        SELECT
            [o].[Id] AS [OrganizationId]
          , [om].[ModuleId]
        FROM [Common].[Organization] AS [o]
        JOIN [Common].[Module_Organization] AS [om] ON [om].[OrganizationId] = [o].[Id]
                                            AND [om].[IsDeleted] = 0
                                            AND [om].[EntityStatusId] IN (1,2)
        WHERE [o].[IsDeleted] = 0
          AND [o].[Id] = @ScopeId
    ),
    [ModulesCTE] AS (
        SELECT
            0 - [m].Id AS [Id]
          , [m].[Key]
          , CAST(NULL AS NVARCHAR(25)) AS [DisplayName]
          , CAST(NULL AS INT) AS [ParentId]
          , CAST(1 AS BIT) AS [IsContainer]
          , CAST(NULL AS INT) AS [JunctionId]
        FROM [NonEndedModulesCTE] AS [nemc]
        JOIN [Common].[Module] AS [m] ON [m].[Id] = [nemc].[ModuleId]
    ),
    [SystemPermissionCTE] AS (
        SELECT
            [sp].[Id]
          , [sp].[Key]
          , CAST(NULL AS NVARCHAR(25)) AS [DisplayName]
          , IIF([sp].[ParentId] IS NULL, 0 - [nemc].[ModuleId], [sp].[ParentId]) AS [ParentId]
          , [sp].[IsContainer]
          , [p].[Id] AS [JunctionId]
        FROM [NonEndedModulesCTE] AS [nemc]
        JOIN [Common].[SystemPermission] AS [sp] ON [sp].[ModuleId] = [nemc].[ModuleId]
        LEFT JOIN [Common].[Permission] AS [p] ON [p].[SystemPermissionId] = [sp].[Id]
                                            AND [p].[ScopeId] = [nemc].[OrganizationId]
                                            AND [p].[IsDeleted] = 0
    )
    SELECT [Id], [Key], [DisplayName], [ParentId], [IsContainer], [JunctionId] FROM [ModulesCTE]
    UNION ALL
    SELECT [Id], [Key], [DisplayName], [ParentId], [IsContainer], [JunctionId] FROM [SystemPermissionCTE]