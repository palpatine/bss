﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Repositories
{
    public interface IOrganizationRepository : IVersionedEntityRepository<Organization, IOrganization>
    {
        Task<IEnumerable<ValidationResultUdt>> CreateAdminAliasAsync(
            IOrganization organization);

        Task<IEnumerable<ValidationResultUdt>> UpdatePermissionsAsync(
            IOrganization organization,
            IEnumerable<JunctionUdt<ISystemPermission>> targetPermissions);
    }
}
