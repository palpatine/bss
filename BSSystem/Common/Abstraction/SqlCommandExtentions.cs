﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;

namespace SolvesIt.BSSystem.CommonModule.Abstraction
{
    public static class SqlCommandExtentions
    {
        public static async Task<IEnumerable<ValidationResultUdt>> ExecuteWithValidationAsync(
            this ISqlCommand sqlCommand)
        {
            var validationResults = new List<ValidationResultUdt>();

            using (var reader = await sqlCommand.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    var field = reader[ExpressionExtensions.GetPropertyName((ValidationResultUdt a) => a.Field)];
                    var messageCode =
                        reader[ExpressionExtensions.GetPropertyName((ValidationResultUdt a) => a.MessageCode)];
                    var id = reader[ExpressionExtensions.GetPropertyName((ValidationResultUdt a) => a.Id)];
                    var additionalValue =
                        reader[ExpressionExtensions.GetPropertyName((ValidationResultUdt a) => a.AdditionalValue)];
                    var result = new ValidationResultUdt
                    {
                        Field = field != DBNull.Value ? (string)field : null,
                        MessageCode = messageCode != DBNull.Value ? (string)messageCode : null,
                        Id = id != DBNull.Value ? (int?)id : null,
                        AdditionalValue = additionalValue != DBNull.Value ? (string)additionalValue : null
                    };

                    validationResults.Add(result);
                }
            }

            return validationResults;
        }
    }
}