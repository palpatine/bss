﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Validation
{
    public interface IEntityDeletionValidator<TEntityIndicator>
        where TEntityIndicator : IStorable
    {
        Task<IEnumerable<ValidationError>> ValidateDeletionAsync(TEntityIndicator entity);
    }
}