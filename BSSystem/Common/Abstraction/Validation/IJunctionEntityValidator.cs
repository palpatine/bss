﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Validation
{
    public interface IJunctionEntityValidator
    {
        Task<IEnumerable<ValidationError>> ValidateModelAsync(Type modelType, IStorable entity, IEnumerable<object> model);
    }
}