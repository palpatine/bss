﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Validation
{
    public interface IEntityValidator<TModel>
        : IEntityValidator
    {
        Task<IEnumerable<ValidationError>> ValidateModelAsync(TModel model);
    }
}