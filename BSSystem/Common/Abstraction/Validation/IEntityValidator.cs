﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Validation
{
    public interface IEntityValidator
    {
        Task<IEnumerable<ValidationError>> ValidateModelAsync(object model);
    }
}