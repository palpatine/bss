﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction
{
    public interface IVersionedEntityService<TVersionedEntity, TPesistanceModel, TEntityIndicator>
        where TVersionedEntity : VersionedEntity, TEntityIndicator
        where TPesistanceModel : TEntityIndicator
        where TEntityIndicator : IStorable
    {
        Task<IEnumerable<ValidationError>> TryLockAsync(
            TEntityIndicator entity,
            string operationToken);

        Task<IEnumerable<ValidationError>> TryLockAsync(
            Type junctionTable,
            PropertyInfo relationProperty,
            IMarker referenceValue,
            string operationToken);

        Task<IEnumerable<ValidationError>> DeleteAsync(
            TEntityIndicator entity,
            string operationToken);

        Task<IEnumerable<ValidationError>> DeleteAsync(
            Expression<Func<TVersionedEntity, bool>> where,
            string operationToken);

        Task<IEnumerable<ValidationError>> SaveAsync(TPesistanceModel model);
    }
}
