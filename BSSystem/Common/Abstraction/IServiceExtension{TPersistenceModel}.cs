﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction
{
    public interface IServiceExtension<TPersistenceModel>
    {
        Task<IEnumerable<ValidationError>> ValidateAsync(TPersistenceModel model);

        Task OnReadyToSaveAsync(TPersistenceModel model);

        Task FillModelAsync(TPersistenceModel model);
    }
}