using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos
{
    public sealed class MenuPositionDto
    {
        [SqlType(SqlDbType.NVarChar, true, -1)]
        public string IconName { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; set; }

        [SqlType(SqlDbType.NVarChar, false, 50)]
        public string Key { get; set; }

        [SqlType(SqlDbType.TinyInt, true, 3)]
        public byte? NavigationPosition { get; set; }

        [SqlType(SqlDbType.Float, false, 53)]
        public double Order { get; set; }

        [SqlType(SqlDbType.Int, true, 10)]
        public int? ParentId { get; set; }

        [SqlType(SqlDbType.VarChar, true, 900)]
        public string PermissionPath { get; set; }
    }
}