using System;
using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos
{
    public sealed class UserEffectivePermissionDto
      : IPermission
    {
        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; set; }

        [SqlType(SqlDbType.VarChar, false, 900)]
        public string Path { get; set; }

        [SqlType(SqlDbType.Bit, false, 1)]
        public bool IsGlobal { get; set; }
    }
}
