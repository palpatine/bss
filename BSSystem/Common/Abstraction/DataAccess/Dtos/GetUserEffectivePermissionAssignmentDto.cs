using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos
{
    public sealed class GetUserEffectivePermissionAssignmentDto
    {
        [SqlType(SqlDbType.Int, false, 10)]
        public int PermissionId { get; set; }

        [SqlType(SqlDbType.VarChar, false, 100)]
        public string AssignmentTableKey { get; set; }

        [SqlType(SqlDbType.VarChar, false, 100)]
        public string RelatedTableKey { get; set; }
    }
}