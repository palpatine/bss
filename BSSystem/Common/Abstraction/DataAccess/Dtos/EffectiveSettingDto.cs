﻿using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos
{
    public sealed class EffectiveSettingDto : Dto, ISystemSetting
    {
        [SqlType(SqlDbType.VarChar, false, 50)]
        public string Key { get; set; }

        [SqlType(SqlDbType.NVarChar, true, -1)]
        public string Value { get; set; }
    }
}