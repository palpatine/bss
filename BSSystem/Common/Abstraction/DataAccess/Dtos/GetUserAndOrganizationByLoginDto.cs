using System;
using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos
{
    public sealed class GetUserAndOrganizationByLoginDto
    {
        [SqlType(SqlDbType.Int, true, 10)]
        public int OrganizationId { get; set; }

        [SqlType(SqlDbType.VarBinary, true, 64)]
        public byte[] Password { get; set; }

        [SqlType(SqlDbType.VarBinary, true, 32)]
        public byte[] Salt { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        public int UserId { get; set; }
    }
}