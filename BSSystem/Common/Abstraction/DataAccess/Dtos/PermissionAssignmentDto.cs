using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos
{
    public sealed class PermissionAssignmentDto : IPermission, IDto
    {
        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; set; }

        [SqlType(SqlDbType.Bit, false)]
        public bool IsContainer { get; set; }

        [SqlType(SqlDbType.Int, true, 10)]
        public int? JunctionId { get; set; }

        [SqlType(SqlDbType.VarChar, false, 50)]
        public string Key { get; set; }

        [SqlType(SqlDbType.VarChar, false, 25)]
        public string DisplayName { get; set; }

        [SqlType(SqlDbType.Int, true, 10)]
        public int? ParentId { get; set; }
    }
}