﻿using System;
using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos
{
    public sealed class UserSubordinates
        : IUser
    {
        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; set; }

        [SqlType(SqlDbType.NVarChar, false, 25)]
        public string DisplayName { get; set; }

        [SqlType(SqlDbType.SmallDateTime, false, 16)]
        public DateTime DateStart { get; set; }

        [SqlType(SqlDbType.SmallDateTime, true, 16)]
        public DateTime DateEnd { get; set; }
    }
}