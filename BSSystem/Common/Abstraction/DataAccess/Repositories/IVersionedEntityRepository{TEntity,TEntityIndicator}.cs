﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories
{
    public interface IVersionedEntityRepository<TEntity, TEntityIndicator>
        : IEntityRepository<TEntity, TEntityIndicator>
        where TEntity : IVersionedEntity, TEntityIndicator, new()
        where TEntityIndicator : IStorable
    {
    }
}