﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories
{
    public interface IAssignmentsRepository
    {
        Task<IEnumerable<ValidationResultUdt>> UpdateEntityAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator master,
            IEnumerable<IDataRecord> newListValue,
            string newListType)
            where TEntityIndicator : IStorable
            where TSlave : IMarker;
    }
}