//------------------------------------------------------------------------------
// <auto-generated>
//     This DataAccess code was generated by a SolvesIT.DevTools.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("SolvesIT.DevTool", "1.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    
    public sealed class Company
    :  VersionedEntity 
    
    
    
    
    
    , ICompany
    
    
    
    
    
    {
        
    [SqlType(SqlDbType.NVarChar, false, -1)]
    
    
        public System.String Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name == value)
                {
                    return;
                }
                _Name = value;
                Modified = true;
            }
        }
        private System.String _Name;
    


    [SqlType(SqlDbType.NVarChar, false, -1)]
    
    
        public System.String FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                if (_FullName == value)
                {
                    return;
                }
                _FullName = value;
                Modified = true;
            }
        }
        private System.String _FullName;
    


    [SqlType(SqlDbType.VarChar, true, 3)]
    
    
        public System.String CountryCode
        {
            get
            {
                return _CountryCode;
            }
            set
            {
                if (_CountryCode == value)
                {
                    return;
                }
                _CountryCode = value;
                Modified = true;
            }
        }
        private System.String _CountryCode;
    


    [SqlType(SqlDbType.NVarChar, true, -1)]
    
    
        public System.String VatNumber
        {
            get
            {
                return _VatNumber;
            }
            set
            {
                if (_VatNumber == value)
                {
                    return;
                }
                _VatNumber = value;
                Modified = true;
            }
        }
        private System.String _VatNumber;
    


    [SqlType(SqlDbType.NVarChar, true, -1)]
    
    
        public System.String OfficialNumber
        {
            get
            {
                return _OfficialNumber;
            }
            set
            {
                if (_OfficialNumber == value)
                {
                    return;
                }
                _OfficialNumber = value;
                Modified = true;
            }
        }
        private System.String _OfficialNumber;
    


    [SqlType(SqlDbType.NVarChar, true, -1)]
    
    
        public System.String StatisticNumber
        {
            get
            {
                return _StatisticNumber;
            }
            set
            {
                if (_StatisticNumber == value)
                {
                    return;
                }
                _StatisticNumber = value;
                Modified = true;
            }
        }
        private System.String _StatisticNumber;
    


    [SqlType(SqlDbType.Decimal, true, 11)]
    
    
        public System.Decimal? Phone
        {
            get
            {
                return _Phone;
            }
            set
            {
                if (_Phone == value)
                {
                    return;
                }
                _Phone = value;
                Modified = true;
            }
        }
        private System.Decimal? _Phone;
    


    [SqlType(SqlDbType.Decimal, true, 11)]
    
    
        public System.Decimal? Fax
        {
            get
            {
                return _Fax;
            }
            set
            {
                if (_Fax == value)
                {
                    return;
                }
                _Fax = value;
                Modified = true;
            }
        }
        private System.Decimal? _Fax;
    


    [SqlType(SqlDbType.NVarChar, true, -1)]
    
    
        public System.String Email
        {
            get
            {
                return _Email;
            }
            set
            {
                if (_Email == value)
                {
                    return;
                }
                _Email = value;
                Modified = true;
            }
        }
        private System.String _Email;
    



        
        

        
        
        
        [SqlType(SqlDbType.Bit, true, 10)]
        [SqlReadOnly]
        public SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Enumerations.EntityStatus EntityStatus
        {
            get
            {
                return _entityStatus;
            }
            set
            {
                if (_entityStatus == value)
                {
                    return;
                }
                _entityStatus = value;
                Modified = true;
            }
        }
        private SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Enumerations.EntityStatus _entityStatus;
        
    }
}
      