//------------------------------------------------------------------------------
// <auto-generated>
//     This DataAccess code was generated by a SolvesIT.DevTools.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("SolvesIT.DevTool", "1.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    
    public sealed class SystemPermissionContext
    :  Entity 
    
    
    
    
    
    , ISystemPermissionContext
    
    
    
    
    
    {
        
    [SqlType(SqlDbType.Int, false, 10)]
    
    
        public CommonModule.Abstraction.DataAccess.Interfaces.ISystemPermission SystemPermission
        {
            get
            {
                return _SystemPermission;
            }
            set
            {
                if (_SystemPermission.IsEquivalent(value))
                {
                    return;
                }
                _SystemPermission = value;
                Modified = true;
            }
        }
        private CommonModule.Abstraction.DataAccess.Interfaces.ISystemPermission _SystemPermission;
    


    [SqlType(SqlDbType.VarChar, false, 100)]
    
    
        public System.String TableKey
        {
            get
            {
                return _TableKey;
            }
            set
            {
                if (_TableKey == value)
                {
                    return;
                }
                _TableKey = value;
                Modified = true;
            }
        }
        private System.String _TableKey;
    



        
        

        
        
        
    }
}
      