//------------------------------------------------------------------------------
// <auto-generated>
//     This DataAccess code was generated by a SolvesIT.DevTools.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("SolvesIT.DevTool", "1.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    
    public sealed class Module
    :  Entity 
    
    
    
    
    
    , IModule
    
    
    
    
    
    {
        
    [SqlType(SqlDbType.VarChar, false, 50)]
    
    
        public System.String Key
        {
            get
            {
                return _Key;
            }
            set
            {
                if (_Key == value)
                {
                    return;
                }
                _Key = value;
                Modified = true;
            }
        }
        private System.String _Key;
    


    [SqlType(SqlDbType.VarChar, true, 900)]
    
    
        public System.String Required
        {
            get
            {
                return _Required;
            }
            set
            {
                if (_Required == value)
                {
                    return;
                }
                _Required = value;
                Modified = true;
            }
        }
        private System.String _Required;
    


    [SqlType(SqlDbType.Int, false, 10)]
    
    
        public System.Int32 MajorVersion
        {
            get
            {
                return _MajorVersion;
            }
            set
            {
                if (_MajorVersion == value)
                {
                    return;
                }
                _MajorVersion = value;
                Modified = true;
            }
        }
        private System.Int32 _MajorVersion;
    


    [SqlType(SqlDbType.Int, false, 10)]
    
    
        public System.Int32 DevVersion
        {
            get
            {
                return _DevVersion;
            }
            set
            {
                if (_DevVersion == value)
                {
                    return;
                }
                _DevVersion = value;
                Modified = true;
            }
        }
        private System.Int32 _DevVersion;
    


    [SqlType(SqlDbType.VarChar, true, 30)]
    
    
        public System.String RevisionNumber
        {
            get
            {
                return _RevisionNumber;
            }
            set
            {
                if (_RevisionNumber == value)
                {
                    return;
                }
                _RevisionNumber = value;
                Modified = true;
            }
        }
        private System.String _RevisionNumber;
    


    [SqlType(SqlDbType.VarChar, true, -1)]
    
    
        public System.String Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                if (_Comment == value)
                {
                    return;
                }
                _Comment = value;
                Modified = true;
            }
        }
        private System.String _Comment;
    



        
        

        
        
        
    }
}
      