﻿using System.Linq.Expressions;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions
{
    public static class OrganizationFunctions
    {
        public static ITableQuery<PermissionAssignmentDto> GetOrganizationPermissions(
            this IDatabaseAccessor query,
            IOrganization organization)
        {
            return query.Function<PermissionAssignmentDto>("[Common].[GetOrganizationPermissions]", Expression.Constant(organization.Id));
        }
    }
}