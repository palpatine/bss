﻿using System.Linq.Expressions;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions
{
    public static class RoleFunctions
    {
        public static ITableQuery<PermissionAssignmentDto> GetRolePermissions(
            this IDatabaseAccessor query,
            IRoleDefinition roleDefinition)
        {
            return query.Function<PermissionAssignmentDto>("[Common].[GetRolePermissions]", Expression.Constant(roleDefinition.Id));
        }
    }
}