﻿using System.Linq.Expressions;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions
{
    public static class UserFunctions
    {
        public static ITableQuery<GetUserAndOrganizationByLoginDto> GetUserAndOrganizationByLogin(
           this IDatabaseAccessor query,
           string organizationName,
           string loginName)
        {
            return query.Function<GetUserAndOrganizationByLoginDto>(
                "[Common].[GetUserAndOrganizationByLogin]",
                Expression.Constant(organizationName),
                Expression.Constant(loginName));
        }

        public static ITableQuery<GetUserEffectivePermissionAssignmentDto> GetUserEffectivePermissionAssignments(
            this IDatabaseAccessor query,
            IUser user)
        {
            return query.Function<GetUserEffectivePermissionAssignmentDto>(
                "[Common].[GetUserEffectivePermissionAssignments]",
                Expression.Constant(user.Id));
        }

        public static ITableQuery<UserEffectivePermissionDto> GetUserEffectivePermissions(
            this IDatabaseAccessor query,
            IUser user)
        {
            return query.Function<UserEffectivePermissionDto>(
                "[Common].[GetUserEffectivePermissions]",
                Expression.Constant(user.Id));
        }

        public static ITableQuery<EffectiveSettingDto> GetUserEffectiveSettings(
            this IDatabaseAccessor query,
            IUser user)
        {
            return query.Function<EffectiveSettingDto>(
                "[Common].[GetUserEffectiveSettings]",
                Expression.Constant(user.Id));
        }

        public static ITableQuery<MenuPositionDto> GetUserMenu(
            this IDatabaseAccessor query,
            IUser user)
        {
            return query.Function<MenuPositionDto>(
                "[Common].[GetUserMenu]",
                Expression.Constant(user.Id));
        }

        public static ITableQuery<PermissionAssignmentDto> GetUserPermissions(
           this IDatabaseAccessor query,
           IUser user)
        {
            return query.Function<PermissionAssignmentDto>(
                "[Common].[GetUserPermissions]",
                Expression.Constant(user.Id));
        }

        public static ITableQuery<UserSubordinates> GetUserSubordinates(
            this IDatabaseAccessor query,
            IOrganization organization,
            IUser manager)
        {
            return query.Function<UserSubordinates>(
                "[Common].[UserSubordinates]",
                Expression.Constant(organization.Id),
                Expression.Constant(manager.Id));
        }
    }
}