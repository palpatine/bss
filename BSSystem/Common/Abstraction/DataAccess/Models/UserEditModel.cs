﻿using System;
using System.Diagnostics.CodeAnalysis;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models
{
    public sealed class UserEditModel : IUser
    {
        public string DisplayName { get; set; }

        public DateTime? DateEnd { get; set; }

        public DateTime DateStart { get; set; }

        public string FirstName { get; set; }

        public string Location { get; set; }

        [SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login")]
        [PropertySource(Ignore = true)]
        public string LoginName { get; set; }

        public string Mail { get; set; }

        public string Phone { get; set; }

        public string Surname { get; set; }

        public int Id { get; set; }
    }
}