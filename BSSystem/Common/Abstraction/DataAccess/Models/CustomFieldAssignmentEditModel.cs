﻿using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models
{
    public sealed class CustomFieldAssignmentEditModel
    {
        public ICustomFieldAssignment CustomFieldAssignment { get; set; }

        public CustomFieldDefinition CustomFieldDefinition { get; set; }

        public string Value { get; set; }

        public bool ValueEnabled { get; set; }

        public ICustomFieldDefinition CustomFieldDefinitionMarker { get; set; }
    }
}