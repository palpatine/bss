﻿namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models
{
    public sealed class ChangePasswordModel
    {
        public string NewPassword { get; set; }

        public string NewPasswordRepeated { get; set; }

        public string Password { get; set; }
    }
}