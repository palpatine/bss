﻿using System;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models
{
    public sealed class OrganizationEditModel : IOrganization
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }

        public DateTime? DateEnd { get; set; }

        public DateTime DateStart { get; set; }

        [PropertySource(typeof(Company))]
        public string Name { get; set; }

        [PropertySource(typeof(Company))]
        public string FullName { get; set; }

        [PropertySource(typeof(Company))]
        public string CountryCode { get; set; }

        [PropertySource(typeof(Company))]
        public string VatNumber { get; set; }

        [PropertySource(typeof(Company))]
        public string OfficialNumber { get; set; }

        [PropertySource(typeof(Company))]
        public string StatisticNumber { get; set; }

        [PropertySource(typeof(Company))]
        public decimal? Phone { get; set; }

        [PropertySource(typeof(Company))]
        public decimal? Fax { get; set; }

        [PropertySource(typeof(Company))]
        public string Email { get; set; }
    }
}