﻿using System;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models
{
    public sealed class CustomFieldDefinitionEditModel : ICustomFieldDefinition
    {
        public bool IsEditable { get; set; }

        public bool IsInViews { get; set; }

        public bool IsNullable { get; set; }

        public bool IsRequired { get; set; }

        public bool IsUnique { get; set; }

        public string DisplayName { get; set; }

        public string Schema { get; set; }

        public int StartingIndex { get; set; }

        public ICustomFieldTarget Target { get; set; }

        public DateTime? DateEnd { get; set; }

        public DateTime DateStart { get; set; }

        public int Id { get; set; }

        public bool CanEditSchema { get; set; }
    }
}