﻿using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers
{
    public sealed class RoleTargetMarker : IRoleTarget
    {
        public string Key { get; set; }
    }
}