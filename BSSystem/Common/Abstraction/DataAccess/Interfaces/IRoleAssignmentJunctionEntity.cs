using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface IRoleAssignmentJunctionEntity : IAssignmentJunctionEntity, IUserJunctionEntity
    {
        IRoleDefinition RoleDefinition { get; }
    }
}