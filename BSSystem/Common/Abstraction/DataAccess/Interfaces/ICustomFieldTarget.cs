﻿namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces
{
    public interface ICustomFieldTarget
    {
        string Key { get; }
    }
}