﻿namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces
{
    public interface IRoleTarget
    {
        string Key { get; }
    }
}