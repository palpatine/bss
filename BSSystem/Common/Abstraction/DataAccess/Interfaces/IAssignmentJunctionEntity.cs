using System;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface IAssignmentJunctionEntity : IJunctionEntity, IDatedEntity
    {
    }
}