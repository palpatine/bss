using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface IJobRoleAssignmentJunctionEntity : IRoleAssignmentJunctionEntity
    {
        decimal? JobTime { get; }

        int? JobHours { get; }
    }
}