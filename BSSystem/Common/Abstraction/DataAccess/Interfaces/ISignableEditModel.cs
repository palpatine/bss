﻿using System.Collections.Generic;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface ISignableEditModel : IStorable
    {
        IEnumerable<CustomFieldAssignmentEditModel> CustomFields { get; set; }

        ICustomFieldTarget CustomFieldTarget { get; }
    }
}