﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface ICustomFieldAssignmentDescriptor : ICustomFieldAssignment
    {
        ICustomFieldDefinition CustomFieldDefinition { get; set; }

        IStorable CustomFieldTarget { get; set; }

        string Value { get; set; }
    }
}