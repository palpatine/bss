﻿using System;
using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts
{
    [SqlName("[Common].[AssignmentJobRoleList]")]
    public sealed class AssignmentJobRoleUdt<TElement> : IUdt
        where TElement : IStorable
    {
        [SqlType(SqlDbType.Int, false, 10)]
        [SqlColumnOrder(3)]
        public IRoleDefinition RoleDefinition { get; set; }

        [SqlType(SqlDbType.Decimal, true, 4, 3)]
        [SqlColumnOrder(4)]
        public decimal? JobTime { get; set; }

        [SqlType(SqlDbType.Decimal, false, 10, 0)]
        [SqlColumnOrder(5)]
        public decimal? JobHours { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        [SqlColumnOrder(1)]
        public int Id { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        [SqlColumnOrder(2)]
        public TElement Element { get; set; }

        [SqlType(SqlDbType.SmallDateTime, false, 16)]
        [SqlColumnOrder(6)]
        public DateTime DateStart { get; set; }

        [SqlType(SqlDbType.SmallDateTime, true, 16)]
        [SqlColumnOrder(7)]
        public DateTime? DateEnd { get; set; }
    }
}