﻿using System;
using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts
{
    [SqlName("[Common].[AssignmentList]")]
    public sealed class AssignmentUdt<TElement>
        : IUdt
        where TElement : IStorable
    {
        [SqlType(SqlDbType.Int, false, 10)]
        [SqlColumnOrder(1)]
        public int Id { get; set; }

        [SqlType(SqlDbType.Int, false, 10)]
        [SqlColumnOrder(2)]
        public TElement Element { get; set; }

        [SqlType(SqlDbType.SmallDateTime, false, 16)]
        [SqlColumnOrder(3)]
        public DateTime DateStart { get; set; }

        [SqlType(SqlDbType.SmallDateTime, true, 16)]
        [SqlColumnOrder(4)]
        public DateTime? DateEnd { get; set; }
    }
}
