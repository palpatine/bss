using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts
{
    [SqlName("[Common].[ValidationResultList]")]
    public sealed class ValidationResultUdt
    {
        [SqlType(SqlDbType.Int, true, 10)]
        [SqlColumnOrder(1)]
        public int? Id { get; set; }

        [SqlType(SqlDbType.NVarChar, true)]
        [SqlColumnOrder(2)]
        public string Field { get; set; }

        [SqlType(SqlDbType.NVarChar, false)]
        [SqlColumnOrder(3)]
        public string MessageCode { get; set; }

        [SqlType(SqlDbType.NVarChar, true)]
        [SqlColumnOrder(4)]
        public string AdditionalValue { get; set; }
    }
}