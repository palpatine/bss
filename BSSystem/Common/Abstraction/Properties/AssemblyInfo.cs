﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

[assembly: AssemblyTitle("BestSolutions.BSSystem.Common.Abstraction")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("BestSolutions.BSSystem.Common.Abstraction")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("9badbdc5-3811-4970-b1cd-7b495e03921d")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: InternalsVisibleTo("SolvesIt.BSSystem.WebApi.Tests")]
[assembly: InternalsVisibleTo("InternalsVisible.ToDynamicProxyGenAssembly2," +
                              "PublicKey=0024000004800000940000000602000000240000525341310004000001000100c547cac37abd99c8db225ef2f6c8a3602f3b3606cc9891605d02baa56104f4cfc0734aa39b93bf7852f7d9266654753cc297e7d2edfe0bac1cdcf9f717241550e0a7b191195b7667bb4f64bcb8e2121380fd1d9d46ad2d92d2d15605093924cceaf74c4861eff62abf69b9291ed0a340e113be11e6a7d3113e92484cf7045cc7")]
[assembly: DefaultSchemaName("Common")]
[assembly: NeutralResourcesLanguage("en-US")]
