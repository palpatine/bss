﻿using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction
{
    public interface IVersionedEntityService<TVersionedEntity, TEntityIndicator>
        : IVersionedEntityService<TVersionedEntity, TVersionedEntity, TEntityIndicator>
        where TVersionedEntity : VersionedEntity, TEntityIndicator
        where TEntityIndicator : IStorable
    {
    }
}