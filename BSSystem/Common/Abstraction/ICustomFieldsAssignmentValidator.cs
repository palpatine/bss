using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction
{
    public interface ICustomFieldsAssignmentValidator : IEntityValidator<ISignableEditModel>
    {
    }
}