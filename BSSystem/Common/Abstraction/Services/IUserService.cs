﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Services
{
    public interface IUserService :
        IVersionedEntityService<User, UserEditModel, IUser>
    {
        Task<IEnumerable<ValidationError>> ChangePasswordAsync(ChangePasswordModel changePasswordModel);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "login")]
        Task<Tuple<IUser, IOrganization>> GetUserAndOrganizationByLoginAsync(
            string login,
            string password);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "login")]
        Task<Tuple<IUser, IOrganization>> GetUserAndOrganizationByLoginAsync(
            string organization,
            string login,
            string password);

        Task ResetPasswordAsync(IUser user);
    }
}