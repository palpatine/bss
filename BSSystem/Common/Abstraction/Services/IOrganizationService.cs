﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Services
{
    public interface IOrganizationService :
        IVersionedEntityService<Organization, OrganizationEditModel, IOrganization>
    {
        Task<IEnumerable<ValidationError>> CreateAdminAliasAsync(
            IOrganization organization);

        Task<IEnumerable<ValidationError>> UpdatePermissionsAsync(
            IOrganization organization,
            IEnumerable<JunctionUdt<ISystemPermission>> targetPermissions);
    }
}