﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Services
{
    public sealed class RoleTargetDescriptor
    {
        public IRoleTarget RoleTarget { get; set; }

        public Type TargetType { get; set; }
    }
}
