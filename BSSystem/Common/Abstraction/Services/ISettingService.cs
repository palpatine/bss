﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Services
{
    public interface ISettingService
    {
        Task<IEnumerable<EffectiveSettingDto>> GetCurrentUserEffectiveSettings();
    }
}