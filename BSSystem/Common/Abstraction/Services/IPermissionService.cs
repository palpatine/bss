﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Services
{
    public interface IPermissionService
    {
        Task<IEnumerable<UserEffectivePermissionDto>> GetCurrentUserEffectivePermissions();

        Task<IEnumerable<int>> GetCurrentUserEffectivePermissionsContextValues(
            GetUserEffectivePermissionAssignmentDto permissionAssignment);

        Task<IEnumerable<GetUserEffectivePermissionAssignmentDto>> GetUserEffectivePermissionAssignments();
    }
}