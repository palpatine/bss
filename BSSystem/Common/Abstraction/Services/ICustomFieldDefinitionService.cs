using System.Collections.Generic;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Services
{
    public interface ICustomFieldDefinitionService : IVersionedEntityService<CustomFieldDefinition, CustomFieldDefinitionEditModel, ICustomFieldDefinition>
    {
        IEnumerable<CustomFieldTargetDescriptor> SignableTargets { get; }

        IDictionary<ICustomFieldTarget, IEnumerable<TerminalDescriptor>> ApplicableTerminals { get; }
    }
}