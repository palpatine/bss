﻿using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Services
{
    public interface ISessionService
        : IEntityService<Session, ISession>
    {
    }
}