using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.Services
{
    public interface IRoleDefinitionService : IVersionedEntityService<RoleDefinition, IRoleDefinition>
    {
        IEnumerable<RoleTargetDescriptor> RoleTargets { get; }
    }

    public interface IRoleTargetsProvider
    {
        IEnumerable<RoleTargetDescriptor> Targets { get; }
    }
}