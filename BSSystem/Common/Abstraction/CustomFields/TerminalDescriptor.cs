﻿using System;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields
{
    public sealed class TerminalDescriptor
    {
        public string DefaultValue { get; set; }

        public Func<string> Description { get; set; }

        public Func<string> Name { get; set; }

        public string Template { get; set; }
    }
}