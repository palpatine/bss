﻿using System;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields
{
    public sealed class CustomFieldTargetDescriptor
    {
        public ICustomFieldTarget CustomFieldTarget { get; set; }

        public Type TargetType { get; set; }
    }
}