namespace SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields
{
    public sealed class Token
    {
        public bool IsLiteral { get; set; }

        public int IndexInSchema { get; set; }

        public int IndexInValue { get; set; }

        public int Length { get; set; }

        public int LengthOfValue { get; set; }

        public string Value { get; set; }

        public string ExpectedValueDescription { get; set; }
    }
}