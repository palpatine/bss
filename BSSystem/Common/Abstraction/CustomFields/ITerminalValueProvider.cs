using System.Collections.Generic;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields
{
    public interface ITerminalValueProvider
    {
        double ExecutionOrder { get; }

        string TargetName { get; }

        IEnumerable<TerminalDescriptor> HandledTokens { get; }

        bool CanHandle(string terminal);

        Task<string> GetValue(CustomFieldGenerationDescriptor descriptor);

        string GetMask(CustomFieldGenerationDescriptor descriptor);

        string GetEditionValue(CustomFieldGenerationDescriptor descriptor);

        bool IsEditionValueValid(CustomFieldGenerationDescriptor descriptor);

        bool IsValueFixed(CustomFieldGenerationDescriptor descriptor);
    }
}