﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields
{
    public interface ICustomFieldTargetsProvider
    {
        IEnumerable<CustomFieldTargetDescriptor> Targets { get; }
    }
}