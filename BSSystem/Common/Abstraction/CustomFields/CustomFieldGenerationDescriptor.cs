using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields
{
    public sealed class CustomFieldGenerationDescriptor
    {
        public string PartialResult { get; set; }

        public IStorable Instance { get; set; }

        public CustomFieldDefinition CustomFieldDefinition { get; set; }

        public string Terminal { get; set; }

        public string CustomFieldValue { get; set; }

        public Token Token { get; set; }

        public string CustomFieldEditableValue { get; set; }
    }
}