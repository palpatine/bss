using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields
{
    public abstract class TerminalValueProvider : ITerminalValueProvider
    {
        private readonly IEnumerable<TerminalDescriptor> _handledTokens;

        protected TerminalValueProvider(
            IEnumerable<TerminalDescriptor> handledTokens,
            string targetName = null)
        {
            _handledTokens = handledTokens;
            TargetName = targetName;
        }

        public virtual double ExecutionOrder => 0;

        public IEnumerable<TerminalDescriptor> HandledTokens => _handledTokens;

        public string TargetName { get; }

        public virtual bool CanHandle(string terminal)
        {
            return _handledTokens.Any(
                x => Regex.IsMatch(terminal, $"^{x.Template}$"));
        }

        public string GetEditionValue(CustomFieldGenerationDescriptor descriptor)
        {
            var signPart = descriptor.CustomFieldValue.Substring(descriptor.Token.IndexInValue, descriptor.Token.LengthOfValue);
            var result = "{" + signPart + "}";
            return result;
        }

        public abstract string GetMask(CustomFieldGenerationDescriptor descriptor);

        public abstract Task<string> GetValue(CustomFieldGenerationDescriptor descriptor);

        public bool IsEditionValueValid(CustomFieldGenerationDescriptor descriptor)
        {
            var signPart = GetSignValueOfTerminal(descriptor);
            string errorDescription;
            var result = ValidateValue(signPart, descriptor.Terminal, out errorDescription);
            descriptor.Token.ExpectedValueDescription = errorDescription;
            return result;
        }

        public bool IsValueFixed(CustomFieldGenerationDescriptor descriptor)
        {
            var signPart = GetSignValueOfTerminal(descriptor);
            return IsValueFixed(signPart, descriptor.Terminal);
        }

        protected abstract bool IsValueFixed(
            string value,
            string terminal);

        protected abstract bool ValidateValue(
            string value,
            string terminal,
            out string errorDescription);

        private static string GetSignValueOfTerminal(CustomFieldGenerationDescriptor descriptor)
        {
            return descriptor.CustomFieldValue.Substring(descriptor.Token.IndexInSchema + 1, descriptor.Token.LengthOfValue);
        }
    }
}