﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Sql;

namespace SolvesIt.BSSystem.Common.Tests.DataAccess
{
    [TestClass]
    public class WhereClauseGeneratorTests
    {
        [TestMethod]
        public void BooleanMemberEquivalence()
        {
            var whereQuery = new WhereClauseGenerator().Generate<SectionUser>(x => x.IsDeleted == true);

            Assert.AreEqual("([IsDeleted] = @Parameter1)", whereQuery.Clause);
            Assert.AreEqual(true, whereQuery.Parameters["@Parameter1"]);
        }

        [TestMethod]
        public void BooleanSimplifiedMemberEquivalenceAndAlsoLeft()
        {
            var whereQuery = new WhereClauseGenerator().Generate<SectionUser>(x => x.IsDeleted && x.Id == 5);

            Assert.AreEqual("(([IsDeleted] = @Parameter2) AND ([Id] = @Parameter1))", whereQuery.Clause);
            Assert.AreEqual(true, whereQuery.Parameters["@Parameter2"]);
            Assert.AreEqual(5, whereQuery.Parameters["@Parameter1"]);
        }

        [TestMethod]
        public void BooleanSimplifiedMemberEquivalenceAndAlsoRight()
        {
            var whereQuery = new WhereClauseGenerator().Generate<SectionUser>(x => x.Id == 5 && x.IsDeleted);

            Assert.AreEqual("(([Id] = @Parameter1) AND ([IsDeleted] = @Parameter2))", whereQuery.Clause);
            Assert.AreEqual(true, whereQuery.Parameters["@Parameter2"]);
            Assert.AreEqual(5, whereQuery.Parameters["@Parameter1"]);
        }

        [TestMethod]
        public void BooleanSimplifiedMemberEquivalenceOrElseLeft()
        {
            var whereQuery = new WhereClauseGenerator().Generate<SectionUser>(x => x.IsDeleted || x.Id == 5);

            Assert.AreEqual("(([IsDeleted] = @Parameter2) OR ([Id] = @Parameter1))", whereQuery.Clause);
            Assert.AreEqual(true, whereQuery.Parameters["@Parameter2"]);
            Assert.AreEqual(5, whereQuery.Parameters["@Parameter1"]);
        }

        [TestMethod]
        public void BooleanSimplifiedMemberEquivalenceOrElseRight()
        {
            var whereQuery = new WhereClauseGenerator().Generate<SectionUser>(x => x.Id == 5 || x.IsDeleted);

            Assert.AreEqual("(([Id] = @Parameter1) OR ([IsDeleted] = @Parameter2))", whereQuery.Clause);
            Assert.AreEqual(true, whereQuery.Parameters["@Parameter2"]);
            Assert.AreEqual(5, whereQuery.Parameters["@Parameter1"]);
        }

        [TestMethod]
        public void BooleanSimplifiedMemberEquivalenceTopLevel()
        {
            var whereQuery = new WhereClauseGenerator().Generate<SectionUser>(x => x.IsDeleted);

            Assert.AreEqual("([IsDeleted] = @Parameter1)", whereQuery.Clause);
            Assert.AreEqual(true, whereQuery.Parameters["@Parameter1"]);
        }

        [TestMethod]
        public void StorableEquivalence()
        {
            var section = new SectionMarker(3);
            var whereQuery = new WhereClauseGenerator().Generate<SectionUser>(x => x.Section.IsEquivalent(section));

            Assert.AreEqual("([SectionId] = @Parameter1)", whereQuery.Clause);
            Assert.AreEqual(3, whereQuery.Parameters["@Parameter1"]);
        }
    }
}