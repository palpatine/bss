﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.Common.Tests.DataAccess
{
    [TestClass]
    public class EntityDescriptorTests
    {
        [TestMethod]
        public void ColumsListShouldBeFormatted()
        {
            var testSubject = new EntityDescriptor<TestRabbit>();

            var formattedColumns = testSubject.FormatWriteColumnsList();

            CollectionAssert.AreEquivalent(
                "[Number], [Text]".Split(','),
                formattedColumns.Split(','));
        }

        [TestMethod]
        public void Dd()
        {
            var user = new TestRabbit { Number = 444 };
            Expression<Func<TestRabbit, bool>> expression = x => x.Number > user.Number || x.Number < 34 && x.Number != 4;

            var converter = new Converter();
            var res = converter.Generate(expression);
        }

        [TestMethod]
        public void InsertCommandParametersShouldBeBindedToCommand()
        {
            var command = new SqlCommandWrapper(new SqlCommand());
            var testSubject = new EntityDescriptor<TestRabbit>();
            var rabbit = new TestRabbit
            {
                Number = 59,
                Text = "asdfasd"
            };
            testSubject.MapWriteValues(command, rabbit);

            command.Parameters["@Number"].Value.ShouldBeEqual(rabbit.Number);
            command.Parameters["@Text"].Value.ShouldBeEqual(rabbit.Text);
        }

        [TestMethod]
        public void InsertValuesMustMatchOrderWithColumnsListShouldBeFormatted()
        {
            var testSubject = new EntityDescriptor<TestRabbit>();

            var formattedColumns = testSubject.FormatWriteColumnsList()
                .Split(',')
                .Select(x => x.Trim().TrimEnd(']').TrimStart('['))
                .ToArray();
            var formattedInsertSection = testSubject.FormatInsertValuesSection()
                .Split(',')
                .Select(x => x.Trim().TrimStart('@'))
                .ToArray();

            CollectionAssert.AreEqual(
                formattedColumns,
                formattedInsertSection);
        }

        [TestMethod]
        public void InsertValuesSectionShouldBeFormatted()
        {
            var testSubject = new EntityDescriptor<TestRabbit>();

            var formattedInsertSection = testSubject.FormatInsertValuesSection();

            CollectionAssert.AreEquivalent(
                "@Number, @Text".Split(','),
                formattedInsertSection.Split(','));
        }

        [TestMethod]
        public void TableNameShouldBeGenearted()
        {
            var testSubject = new EntityDescriptor<TestRabbit>();

            testSubject.SqlName.ShouldBeEqual("[RabbitWhole].[TestsRabbits]");
        }

        [TestMethod]
        public void UpdateValuesSectionShouldBeFormatted()
        {
            var testSubject = new EntityDescriptor<TestRabbit>();

            var formattedUpdateSection = testSubject.FormatUpdateSetSection();

            CollectionAssert.AreEquivalent(
                "[Number] = @Number, [Text] = @Text".Split(','),
                formattedUpdateSection.Split(','));
        }

        [TestMethod]
        public void ViewNameShouldBeGenearted()
        {
            var testSubject = new DtoDescriptor<UserDetailDto>();

            testSubject.SqlName.ShouldBeEqual("[RabbitWhole].[v_UsersDetails]");
        }

        private class Converter : ExpressionVisitor
        {
            private Stack<string> _stack = new Stack<string>();

            public string Generate(Expression<Func<TestRabbit, bool>> expre)
            {
                Visit(expre);

                return _stack.Pop();
            }

            protected override Expression VisitBinary(BinaryExpression node)
            {
                var value = base.VisitBinary(node);
                string comparisionOperator = null;

                switch (node.NodeType)
                {
                    case ExpressionType.GreaterThan:
                        comparisionOperator = ">";
                        break;

                    case ExpressionType.Equal:
                        comparisionOperator = "=";
                        break;

                    case ExpressionType.GreaterThanOrEqual:
                        comparisionOperator = ">=";
                        break;

                    case ExpressionType.LessThan:
                        comparisionOperator = "<";
                        break;

                    case ExpressionType.LessThanOrEqual:
                        comparisionOperator = "<=";
                        break;

                    case ExpressionType.NotEqual:
                        comparisionOperator = "<>";
                        break;

                    case ExpressionType.AndAlso:
                        comparisionOperator = "and";
                        break;

                    case ExpressionType.OrElse:
                        comparisionOperator = "or";
                        break;

                    default:
                        throw new InvalidOperationException();
                }

                var right = _stack.Pop();
                var left = _stack.Pop();
                _stack.Push(string.Format("({0} {1} {2})", left, comparisionOperator, right));

                return value;
            }

            protected override Expression VisitConstant(ConstantExpression node)
            {
                _stack.Push(node.Value.ToString());
                return base.VisitConstant(node);
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                if (node.Expression.NodeType == ExpressionType.Parameter)
                {
                    _stack.Push("[" + node.Member.Name + "]");
                }
                else
                {
                    var method = Expression.Lambda<Func<object>>(Expression.Convert(node, typeof(object))).Compile();
                    var data = method();
                    _stack.Push(data.ToString());
                }

                return node;
            }

            protected override Expression VisitUnary(UnaryExpression node)
            {
                if (node.NodeType != ExpressionType.Not)
                {
                    throw new InvalidOperationException();
                }

                var value = base.VisitUnary(node);

                var argument = _stack.Pop();

                _stack.Push(string.Format("(NOT {0})", argument));

                return value;
            }
        }

        private class TestRabbit : Entity
        {
            public int Number { get; set; }

            public string Text { get; set; }
        }

        private abstract class UserDetailDto : Dto
        {
            public int Number { get; set; }

            public string Text { get; set; }
        }
    }
}