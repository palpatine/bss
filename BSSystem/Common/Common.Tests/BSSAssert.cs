﻿using Qdarc.Tests.Common;
using System.Linq;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Common.Tests
{
    public static class BSSAssert
    {
        public static void StorableShouldBeEqual<TStorable, TModel>(this TStorable actual, TModel expected, string additonalMessage = "")
            where TStorable : IStorable
            where TModel : IStorable
        {
            if (expected == null && actual == null)
                return;
            var objectType = ((object)actual ?? expected).GetType();
            if (objectType.IsClass && objectType != typeof(string))
            {
                if (actual == null || expected == null)
                {
                    string message = string.Format(
                                       "Should be equal assert failed. One of the objects is null. {0}",
                                       additonalMessage);
                    throw new AssertException(message);
                }
                var versionedProperties = typeof(VersionedEntity).GetProperties().Select(vp => vp.Name).ToArray();
                foreach (var property in typeof(TStorable).GetProperties().Where(p => !versionedProperties.Contains(p.Name)))
                {
                    string message = string.Format(
                                       "{0}. {1}",
                                       property.Name,
                                       additonalMessage);
                    var p = typeof(TModel).GetProperty(property.Name);
                    if (p == null)
                    {
                        throw new AssertException(string.Format(
                                       "Property {0} not exist in {1}",
                                       property.Name,
                                       typeof(TModel).Name));
                    }
                    property.GetValue(actual).ShouldBeEqual(p.GetValue(expected), message);
                }
            }
            else if ((actual != null && !actual.Equals(expected)) || !expected.Equals(actual))
            {
                string message = string.Format(
                                       "Should be equal assert failed. Expected: {0}, actual {1}. {2}",
                                       expected,
                                       actual,
                                       additonalMessage);
                throw new AssertException(message);
            }
        }
    }
}