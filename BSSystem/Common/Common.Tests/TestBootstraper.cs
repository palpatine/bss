﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Moq;
using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.Ioc.Unity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Notification;

namespace SolvesIt.BSSystem.Common.Tests
{
    public class TestBootstraper
    {
        public static readonly UserMarker TesterChanger = new UserMarker(4);
        private const string connectiontemplate = @"Data Source=.;Initial Catalog={0};User ID=lawgiver;Password=lawgiver;MultipleActiveResultSets=True";

        internal static string BSSTestConnectionString
        {
            get
            {
                return string.Format(connectiontemplate, "BSS_tests");
            }
        }

        public static IUnityContainer InitializeSystem(params Assembly[] assemblies)
        {
            var container = new UnityContainer();
            var registrar = new UnityRegistrar(container);

            var moduleDLLs = assemblies.Concat(new[] { typeof(IStorable).Assembly, typeof(IEventAggregator).Assembly });

            var serviceLocator = new UnityServiceLocator(container);
            container.RegisterInstance<IServiceLocator>(serviceLocator);
            registrar.EnableEnumerableResolution();

            container.RegisterInstance<ICache>(new Mock<ICache>().Object);
            container.RegisterInstance<IGlobalMessageParametersProvider>(new Mock<IGlobalMessageParametersProvider>().Object);
            var provider = new Mock<ISessionDataProvider>();
            provider.Setup(x => x.CurrentUser).Returns(TesterChanger);

            container.RegisterInstance<ISessionDataProvider>(provider.Object);

            var configurationProvider = new Mock<IConfigurationProvider>();
            configurationProvider.Setup(cp => cp.ConnectionString).Returns(TestBootstraper.BSSTestConnectionString);
            container.RegisterInstance<IConfigurationProvider>(configurationProvider.Object);

            var bootstrapper = new SystemBootstrapper(registrar, moduleDLLs);
            bootstrapper.Bootstrap();

            return container;
        }

        public static void RunTestSqlScriptsInDirectory()
        {
            string scriptDirectory = @"C:\Users\Rafał\Source\Repos\bss\SQLDataBaseDDL";
            var scriptFiles = (new DirectoryInfo(scriptDirectory))
                .GetFiles("BSS_test*.sql")
                .OrderBy(f => f.Name).ToArray();
            foreach (FileInfo file in scriptFiles)
            {
                var fileInfo = new FileInfo(file.FullName);
                string script = fileInfo.OpenText().ReadToEnd();
                var connection = new SqlConnection(string.Format(connectiontemplate, "master"));
                var server = new Server(new ServerConnection(connection));
                server.ConnectionContext.ExecuteNonQuery(script);
            }
        }
    }
}