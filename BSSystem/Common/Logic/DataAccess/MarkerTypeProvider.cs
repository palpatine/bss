﻿using System;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.CommonModule.Logic.DataAccess
{
    internal class MarkerTypeProvider : IMarkerTypeProvider
    {
        public Type GetMarkerFor(Type type)
        {
            if (type == typeof(IUser))
            {
                return typeof(UserMarker);
            }

            if (type == typeof(IOrganization))
            {
                return typeof(OrganizationMarker);
            }

            if (type == typeof(ISession))
            {
                return typeof(SessionMarker);
            }

            return null;
        }
    }
}
