﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    internal class SectionValidator : EntityValidator<Section>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public SectionValidator(
            IServiceLocator serviceLocator,
            IDatabaseAccessor databaseAccessor)
            : base(serviceLocator)
        {
            _databaseAccessor = databaseAccessor;
        }

        public async override Task ValidateAsync(IValidationResult<Section> validator)
        {
            await ValidateSlavesDatesAsync(validator);
            await ValidateParentAsync(validator);
        }

        private async Task ValidateParentAsync(IValidationResult<Section> validator)
        {
            if (validator.Entity.Parent != null)
            {
                var newParent = validator.Entity.Parent;

                var sessionDataProvider = ServiceLocator.Current.GetInstance<ISessionDataProvider>();
                var parent = await _databaseAccessor.Query<Section>()
                    .Where(x => !x.IsDeleted && x.Scope.IsEquivalent(sessionDataProvider.Scope) && x.IsEquivalent(validator.Entity.Parent))
                    .Select(x => new DatedEntityModel { Id = x.Id, DateStart = x.DateStart, DateEnd = x.DateEnd })
                    .SingleOrDefaultAsync();

                if (parent == null)
                {
                    validator.AddError(x => x.Parent, "NotFound");
                }
                else
                {
                    validator.ValidateDatesConsistencyWithMasterEntity(parent);
                }

                var isCyclic = await _databaseAccessor.Query<Section>()
                    .Where(x => x.IsEquivalent(newParent))
                    .Select(x => new { x.Id, x.Parent })
                    .UnionAllRecursive(_databaseAccessor.Query<Section>(), x => x.Parent.Id, x => x.Id, x => new { x.Id, x.Parent })
                    .AnyAsync(x => x.Id == validator.Entity.Id);

                if (isCyclic)
                {
                    validator.AddError(x => x.Parent, "CyclicReference");
                }
            }
        }

        private async Task ValidateSlavesDatesAsync(IValidationResult<Section> validator)
        {
            var slavesDates = await
                    _databaseAccessor.Query<SectionUser>()
                    .Where(x => !x.IsDeleted
                              && x.Section.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd })
                .UnionAll(
                    _databaseAccessor.Query<Section>()
                    .Where(x => !x.IsDeleted
                            && x.Parent.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd }))
                .ToListAsync();

            if (!slavesDates.Any())
            {
                return;
            }

            var extremeSlavesDate = new
            {
                DateStart = slavesDates.Min(x => x.DateStart),
                DateEnd = slavesDates.Max(x => x.DateEnd)
            };

            validator
                .MustBeBeforeOrEqual(x => x.DateStart, extremeSlavesDate.DateStart)
                .MustBeAfterOrEqual(x => x.DateEnd, extremeSlavesDate.DateEnd);
        }
    }
}