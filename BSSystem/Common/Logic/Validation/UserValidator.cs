﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    internal class UserValidator : EntityValidator<UserEditModel>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public UserValidator(
            IServiceLocator serviceLocator,
            IDatabaseAccessor databaseAccessor)
            : base(serviceLocator)
        {
            _databaseAccessor = databaseAccessor;
        }

        public override async Task ValidateAsync(IValidationResult<UserEditModel> validator)
        {
            validator
                .NotLongerThen(x => x.LoginName, 20)
                .MustBeUniqueOrNull(x => x.LoginName, (Login x) => x.User)
                .ValidEmailOrNull(x => x.Mail);
            if (!string.IsNullOrWhiteSpace(validator.Entity.LoginName))
            {
                validator
                    .NotEmpty(x => x.Mail);
            }

            await ValidateSlavesDatesAsync(validator);
        }

        private async Task ValidateSlavesDatesAsync(IValidationResult<UserEditModel> validator)
        {
            var slavesDates = await
                    _databaseAccessor.Query<SectionUser>()
                    .Where(x => !x.IsDeleted
                              && x.User.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd })
                .UnionAll(
                    _databaseAccessor.Query<PositionUser>()
                    .Where(x => !x.IsDeleted
                            && x.User.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd }))
                .UnionAll(
                    _databaseAccessor.Query<User>()
                    .Where(x => !x.IsDeleted
                            && x.Parent.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd }))
                .UnionAll(
                    _databaseAccessor.Query<OrganizationUser>()
                    .Where(x => !x.IsDeleted
                            && x.User.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd }))
                .ToListAsync();

            if (!slavesDates.Any())
            {
                return;
            }

            var extremeSlavesDate = new
            {
                DateStart = slavesDates.Min(x => x.DateStart),
                DateEnd = slavesDates.Max(x => x.DateEnd)
            };

            validator
                .MustBeBeforeOrEqual(x => x.DateStart, extremeSlavesDate.DateStart)
                .MustBeAfterOrEqual(x => x.DateEnd, extremeSlavesDate.DateEnd);
        }
    }
}