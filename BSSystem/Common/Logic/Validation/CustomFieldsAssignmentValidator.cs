using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Logic.CustomFields;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    internal sealed class CustomFieldsAssignmentValidator : EntityValidator<ISignableEditModel>, ICustomFieldsAssignmentValidator
    {
        private readonly IEnumerable<ICustomFieldTargetsProvider> _signableTargetsProviders;
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ICustomFieldsGenerator _customFieldsGenerator;
        private readonly ICustomFieldsParser _customFieldsParser;

        public CustomFieldsAssignmentValidator(
            IServiceLocator serviceLocator,
            ICustomFieldsParser customFieldsParser,
            ICustomFieldsGenerator customFieldsGenerator,
            IEnumerable<ICustomFieldTargetsProvider> signableTargetsProviders,
            IDatabaseAccessor databaseAccessor)
            : base(serviceLocator)
        {
            _customFieldsParser = customFieldsParser;
            _customFieldsGenerator = customFieldsGenerator;
            _signableTargetsProviders = signableTargetsProviders;
            _databaseAccessor = databaseAccessor;
        }

        public override async Task ValidateAsync(IValidationResult<ISignableEditModel> validator)
        {
            foreach (var customField in validator.Entity.CustomFields ?? Enumerable.Empty<CustomFieldAssignmentEditModel>())
            {
                var definition =
                    await _databaseAccessor.Query<CustomFieldDefinition>().SingleAsync(x => x.IsEquivalent(customField.CustomFieldDefinitionMarker));
                if ((validator.Entity.Id == 0 || definition.IsEditable) && (!definition.IsNullable || customField.ValueEnabled))
                {
                    var metadata = new CustomFieldValidationErrorMetadata { CustomFieldDefinition = definition };
                    validator.NotEmpty(x => x.CustomFields, customField.Value, metadata);
                    if (!string.IsNullOrEmpty(customField.Value))
                    {
                        var tokens = _customFieldsParser.Parse(definition.Schema).ToArray();
                        var invalidTokens =
                            _customFieldsGenerator.ValidateSignValue(definition, tokens, customField.Value).ToArray();

                        foreach (var invalidToken in invalidTokens)
                        {
                            validator.AddErrorWithMetadata(
                                x => x.CustomFields,
                                "InvalidValue",
                                metadata,
                                invalidToken.IndexInSchema + 1,
                                invalidToken.Value,
                                invalidToken.ExpectedValueDescription);
                        }

                        if (!invalidTokens.Any())
                        {
                            var isCustomFieldValueFixed = _customFieldsGenerator.IsSignValueFixed(definition, tokens, customField.Value);
                            if (isCustomFieldValueFixed)
                            {
                                var value =
                                    await
                                        _customFieldsGenerator.BuildCustomFieldValueAsync(
                                            definition,
                                            validator.Entity,
                                            tokens,
                                            customField.Value);
                                if (await IsSignValueUsedAsync(validator.Entity, definition, value))
                                {
                                    validator.AddErrorWithMetadata(
                                        x => x.CustomFields,
                                        "NotUniqueValue",
                                        metadata);
                                }
                            }
                        }
                    }
                }
            }
        }

        private async Task<bool> IsSignValueUsedAsync(
            ISignableEditModel entity,
            CustomFieldDefinition customField,
            string value)
        {
            var targetDescriptor = _signableTargetsProviders
                .SelectMany(x => x.Targets)
                .Single(x => x.CustomFieldTarget.Key == entity.CustomFieldTarget.Key);
            var signsAssignmentType = targetDescriptor.TargetType;
            var result = await _databaseAccessor.Query<ICustomFieldAssignmentDescriptor>(signsAssignmentType)
                .Where(x => x.CustomFieldDefinition.IsEquivalent(customField)
                            && x.Value == value
                            && !x.CustomFieldTarget.IsEquivalent(entity))
                .AnyAsync();

            return result;
        }
    }
}