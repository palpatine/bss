﻿using System;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    public sealed class RoleDefinitionDeletionValidator : DominantEntityDeletionValidator<IRoleDefinition>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public RoleDefinitionDeletionValidator(IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<bool> CheckSlaveExistanceAsync(IRoleDefinition entity)
        {
            var slaveExists = await
                    _databaseAccessor.Query<SectionUser>()
                    .Where(x => !x.IsDeleted
                              && x.RoleDefinition.IsEquivalent(entity))
                    .Select(x => x.Id)
                .UnionAll(
                    _databaseAccessor.Query<OrganizationUser>()
                    .Where(x => !x.IsDeleted
                            && x.RoleDefinition.IsEquivalent(entity))
                    .Select(x => x.Id))
                .AnyAsync();

            return slaveExists;
        }
    }
}
