﻿using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    internal class CustomFieldValidationErrorMetadata : ICustomFieldValidationErrorMetadata
    {
        public ICustomFieldDefinition CustomFieldDefinition { get; set; }
    }
}