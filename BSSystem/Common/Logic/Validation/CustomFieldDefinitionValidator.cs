﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Logic.CustomFields;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    internal class CustomFieldDefinitionValidator : EntityValidator<CustomFieldDefinitionEditModel>
    {
        private readonly ICustomFieldsGenerator _customFieldsGenerator;
        private readonly IEnumerable<ICustomFieldTargetsProvider> _signableTargetsProviders;
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ICustomFieldsParser _customFieldsParser;

        public CustomFieldDefinitionValidator(
            IServiceLocator serviceLocator,
            ICustomFieldsParser customFieldsParser,
            ICustomFieldsGenerator customFieldsGenerator,
            IEnumerable<ICustomFieldTargetsProvider> signableTargetsProviders,
            IDatabaseAccessor databaseAccessor)
            : base(serviceLocator)
        {
            _customFieldsParser = customFieldsParser;
            _customFieldsGenerator = customFieldsGenerator;
            _signableTargetsProviders = signableTargetsProviders;
            _databaseAccessor = databaseAccessor;
        }

        public override async Task ValidateAsync(IValidationResult<CustomFieldDefinitionEditModel> validator)
        {
            var signsAssignmentType = _signableTargetsProviders
                    .SelectMany(x => x.Targets)
                    .Single(x => x.CustomFieldTarget.Key == validator.Entity.Target.Key)
                    .TargetType;

            validator.NotEmpty(x => x.DisplayName);

            if (validator.Entity.Id == 0 || await CanEditSchema(validator.Entity, signsAssignmentType))
            {
                validator.NotEmpty(x => x.Schema);
                if (!string.IsNullOrEmpty(validator.Entity.Schema))
                {
                    ValidateSchema(validator);
                }
            }

            if (validator.Entity.Id != 0)
            {
                if (!validator.Entity.IsNullable && await IsNullValueUsed(validator.Entity, signsAssignmentType))
                {
                    validator.AddError(x => x.IsNullable, "NullValueIsUsed");
                }

                if (validator.Entity.IsUnique && await AreValuesUnique(validator.Entity, signsAssignmentType))
                {
                    validator.AddError(x => x.IsUnique, "CurrentValuesAreNotUnique");
                }
            }
        }

        private void ValidateSchema(IValidationResult<CustomFieldDefinitionEditModel> validator)
        {
            IEnumerable<Token> tokens;
            var errors = _customFieldsParser.Validate(validator.Entity.Schema, out tokens);

            if (errors != null && errors.Any())
            {
                foreach (var validationError in errors)
                {
                    validator.AddError(x => x.Schema, "Lexical", validationError);
                }
            }
            else
            {
                IEnumerable<Token> unknownTokens;
                _customFieldsGenerator.CanGenerateValue(validator.Entity.Target.Key, tokens, out unknownTokens);

                if (unknownTokens != null && unknownTokens.Any())
                {
                    var message = string.Join(", ", unknownTokens.Select(x => x.Value));
                    validator.AddError(x => x.Schema, "Terminal", message);
                }
            }
        }

        private async Task<bool> CanEditSchema(CustomFieldDefinitionEditModel customField, Type concreteType)
        {
            return !await _databaseAccessor.Query<ICustomFieldAssignmentDescriptor>(concreteType)
                .Where(x => x.CustomFieldDefinition.IsEquivalent(customField))
                .AnyAsync();
        }

        private async Task<bool> IsNullValueUsed(CustomFieldDefinitionEditModel customField, Type concreteType)
        {
            return await _databaseAccessor.Query<ICustomFieldAssignmentDescriptor>(concreteType)
                .Where(x => x.CustomFieldDefinition.IsEquivalent(customField) && x.Value == null)
                .AnyAsync();
        }

        private async Task<bool> AreValuesUnique(CustomFieldDefinitionEditModel customField, Type concreteType)
        {
            var result = await _databaseAccessor.Query<ICustomFieldAssignmentDescriptor>(concreteType)
                .Where(x => x.CustomFieldDefinition.IsEquivalent(customField) && x.Value != null)
                .GroupBy(x => x.Value, x => new { Count = x.Count() })
                .AnyAsync(x => x.Count() > 1);

            return result;
        }
    }
}