﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Logic.Security;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    internal class ChangePasswordValidator
        : EntityValidator<ChangePasswordModel>
    {
        private readonly ISecurityManager _securityManager;
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ISessionDataProvider _sessionDataProvider;

        public ChangePasswordValidator(
            ISessionDataProvider sessionDataProvider,
            ISecurityManager securityManager,
            IServiceLocator serviceLocator,
            IDatabaseAccessor databaseAccessor)
            : base(serviceLocator)
        {
            _sessionDataProvider = sessionDataProvider;
            _securityManager = securityManager;
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<IEnumerable<ValidationError>> ValidateAsync(ChangePasswordModel model)
        {
            var login = await _databaseAccessor.Query<Login>()
                .SingleAsync(x => !x.IsDeleted && x.User.IsEquivalent(_sessionDataProvider.CurrentUser));
            var isPasswordCorrect = _securityManager.ComparePasswords(
                model.Password,
                login.Password,
                login.Salt);

            return Validator.GetResultSet(model)
                .NotEmpty(x => x.Password)
                .NotEmpty(x => x.NewPassword)
                .NotEmpty(x => x.NewPasswordRepeated)
                .MustMatch(x => x.NewPasswordRepeated, x => x.NewPassword)
                .MustNotMatch(x => x.NewPassword, x => x.Password)
                .ValidateValue(() => isPasswordCorrect, x => x.Password)
                .Errors;
        }
    }
}