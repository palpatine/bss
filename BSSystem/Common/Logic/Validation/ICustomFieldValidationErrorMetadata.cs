﻿using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    public interface ICustomFieldValidationErrorMetadata
    {
        ICustomFieldDefinition CustomFieldDefinition { get; }
    }
}
