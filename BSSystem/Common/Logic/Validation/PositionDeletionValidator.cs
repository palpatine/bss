﻿using System.Data;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    public sealed class PositionDeletionValidator : DominantEntityDeletionValidator<IPosition>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public PositionDeletionValidator(IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<bool> CheckSlaveExistanceAsync(IPosition entity)
        {
            var slaveExists = await
                    _databaseAccessor.Query<PositionUser>()
                    .Where(x => !x.IsDeleted
                              && x.Position.IsEquivalent(entity))
                .AnyAsync();

            return slaveExists;
        }
    }
}