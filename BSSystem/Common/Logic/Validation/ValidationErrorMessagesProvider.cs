﻿using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Validation
{
    internal class ValidationErrorMessagesProvider : IValidationErrorMessagesProvider
    {
        public bool TryGetErrorMessage(string errorCode, out string message)
        {
            message = ValidationErrors.ResourceManager.GetString(errorCode);
            return message != null;
        }
    }
}