﻿using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc;
using SolvesIt.BSSystem.CommonModule.Logic.DataIntegrity;

namespace SolvesIt.BSSystem.CommonModule.Logic.Environment
{
    public class HandlersBootstrapper : IBootstrapper
    {
        public double Order => double.MaxValue;

        public void Bootstrap(IRegistrar registrar)
        {
            var loginHandler = registrar.Resolver.Resolve<LoginHandler>();
            registrar.RegisterInstance(loginHandler);

            var companyHandler = registrar.Resolver.Resolve<CompanyHandler>();
            registrar.RegisterInstance(companyHandler);
        }
    }
}