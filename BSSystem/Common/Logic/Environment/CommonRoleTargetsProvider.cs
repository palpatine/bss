﻿using System.Collections.Generic;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.CommonModule.Logic.Environment
{
    internal sealed class CommonRoleTargetsProvider : IRoleTargetsProvider
    {
        public IEnumerable<RoleTargetDescriptor> Targets => new[]
        {
            new RoleTargetDescriptor
            {
                TargetType = typeof(Section),
                RoleTarget = new RoleTargetMarker
                {
                    Key = StorableHelper.GetTableKey<Section>()
                }
            }
        };
    }
}
