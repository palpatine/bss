﻿using System.Collections.Generic;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;

namespace SolvesIt.BSSystem.CommonModule.Logic.Environment
{
    internal sealed class CommonCustomFieldTargetsProvider : ICustomFieldTargetsProvider
    {
        public IEnumerable<CustomFieldTargetDescriptor> Targets => new[]
        {
            new CustomFieldTargetDescriptor
            {
                TargetType = typeof(UserCustomField),
                CustomFieldTarget = new CustomFieldTargetMarker
                {
                    Key = "[Common].[User]"
                }
            }
        };
    }
}
