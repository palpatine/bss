﻿using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.CommonModule.Logic.CustomFields;
using SolvesIt.BSSystem.CommonModule.Logic.DataAccess;
using SolvesIt.BSSystem.CommonModule.Logic.Repositories;
using SolvesIt.BSSystem.CommonModule.Logic.Security;
using SolvesIt.BSSystem.CommonModule.Logic.Services;
using SolvesIt.BSSystem.CommonModule.Logic.Validation;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.Environment
{
    public class Bootstrapper : IBootstrapper
    {
        public double Order => 1;

        public void Bootstrap(IRegistrar registrar)
        {
            RegisterDataAccess(registrar);
            RegisterRepositories(registrar);
            RegisterServices(registrar);
            RegisterValidators(registrar);
            RegisterSecurity(registrar);
            RegisterSignsManagement(registrar);

            registrar.RegisterMultiple<ICustomFieldTargetsProvider, CommonCustomFieldTargetsProvider>();
            registrar.RegisterMultiple<IRoleTargetsProvider, CommonRoleTargetsProvider>();
        }

        private static void RegisterSecurity(IRegistrar registrar)
        {
            registrar.RegisterSingle<ISecurityManager, SecurityManager>();
        }

        private static void RegisterServices(IRegistrar registrar)
        {
            registrar.RegisterResolutionFactory(typeof(IVersionedEntityService<,>), (resolver, type) =>
            {
                var resultType = typeof(VersionedEntityService<,>).MakeGenericType(type.GetGenericArguments());
                return resolver.Resolve(resultType);
            });

            registrar.RegisterSingle<ISessionService, SessionService>();
            registrar.RegisterSingle<IUserService, UserService>();
            registrar.RegisterSingle<IOrganizationService, OrganizationService>();
            registrar.RegisterSingle<ICustomFieldDefinitionService, CustomFieldDefinitionService>();
            registrar.RegisterSingle<IRoleDefinitionService, RoleDefinitionService>();
            registrar.RegisterSingle<IPermissionService, PermissionService>();
            registrar.RegisterSingle<IMenuService, MenuService>();
            registrar.RegisterSingle<ISettingService, SettingService>();
        }

        private static void RegisterValidators(IRegistrar registrar)
        {
            registrar.RegisterMultiple<IEntityValidator<ChangePasswordModel>, ChangePasswordValidator>();
            registrar.RegisterMultiple<IEntityValidator<CustomFieldDefinitionEditModel>, CustomFieldDefinitionValidator>();
            registrar.RegisterMultiple<ICustomFieldsAssignmentValidator, CustomFieldsAssignmentValidator>();
            registrar.RegisterMultiple<IEntityValidator<Position>, PositionValidator>();
            registrar.RegisterMultiple<IEntityValidator<RoleDefinition>, RoleDefinitionValidator>();
            registrar.RegisterMultiple<IEntityValidator<Section>, SectionValidator>();
            registrar.RegisterMultiple<IEntityValidator<UserEditModel>, UserValidator>();

            registrar.RegisterMultiple<IEntityValidator<OrganizationEditModel>, EntityValidator<OrganizationEditModel>>();
            registrar.RegisterMultiple<IEntityValidator<Address>, EntityValidator<Address>>();

            registrar.RegisterMultiple<IEntityDeletionValidator<IPosition>, PositionDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<IRoleDefinition>, RoleDefinitionDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<ISection>, SectionDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<IUser>, UserDeletionValidator>();

            registrar.RegisterMultiple<IValidationErrorMessagesProvider, ValidationErrorMessagesProvider>();
        }

        private void RegisterDataAccess(IRegistrar registrar)
        {
            registrar.RegisterSingle<IBssQueryTranslationHander, BssQueryTranslationHander>();
            registrar.RegisterMultiple<IMarkerTypeProvider, MarkerTypeProvider>();
        }

        private void RegisterRepositories(IRegistrar registrar)
        {
            registrar.RegisterSingle<IOrganizationRepository, OrganizationRepository>();
            registrar.RegisterSingle<IVersionedEntityRepository<Organization, IOrganization>, OrganizationRepository>();
        }

        private void RegisterSignsManagement(IRegistrar registrar)
        {
            registrar.RegisterSingle<ICustomFieldsParser, CustomFieldsParser>();
            registrar.RegisterSingle<ICustomFieldsGenerator, CustomFieldsGenerator>();
            registrar.RegisterMultiple<ITerminalValueProvider, DateTerminalValueProvider>();
            registrar.RegisterMultiple<ITerminalValueProvider, LocallyUniqueTerminalValueProvider>();
            registrar.RegisterMultiple<ITerminalValueProvider, GloballyUniqueTerminalValueProvider>();
        }
    }
}