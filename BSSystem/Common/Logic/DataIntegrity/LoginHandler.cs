﻿using System;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Communication;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.Logic;

namespace SolvesIt.BSSystem.CommonModule.Logic.DataIntegrity
{
    internal class LoginHandler
    {
        private readonly Func<IEntityRepository<Login, ILogin>> _loginRepositoryFactory;

        public LoginHandler(
            IEventAggregator eventAggregator,
            Func<IEntityRepository<Login, ILogin>> loginRepositoryFactory)
        {
            _loginRepositoryFactory = loginRepositoryFactory;

            eventAggregator.On<EntityDeletedEvent<IUser>>(OnUserDelete);
        }

        private async Task OnUserDelete(EntityDeletedEvent<IUser> entityDeletedEvent)
        {
            var loginRepository = _loginRepositoryFactory();
            foreach (var entity in entityDeletedEvent.EntityList)
            {
                var user = entity;
                await loginRepository.DeleteAsync(x => x.User.IsEquivalent(user) && !x.IsDeleted);
            }
        }
    }
}