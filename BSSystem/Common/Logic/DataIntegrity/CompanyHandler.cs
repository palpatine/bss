﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Logic;

namespace SolvesIt.BSSystem.CommonModule.Logic.DataIntegrity
{
    internal class CompanyHandler
    {
        private readonly Func<IEntityRepository<Company, ICompany>> _companyRepositoryFactory;
        private readonly Func<IDatabaseAccessor> _cbdDataFactory;

        public CompanyHandler(
            IEventAggregator eventAggregator,
            Func<IEntityRepository<Company, ICompany>> companyRepositoryFactory,
            Func<IDatabaseAccessor> cbdDataFactory)
        {
            _companyRepositoryFactory = companyRepositoryFactory;
            _cbdDataFactory = cbdDataFactory;

            eventAggregator.On<EntityDeletedEvent<IOrganization>>(OnOrganizationDelete);
        }

        private async Task OnOrganizationDelete(EntityDeletedEvent<IOrganization> entityDeletedEvent)
        {
            var entitiesToDeleteIds = entityDeletedEvent.EntityList.Select(x => x.Id).ToArray();

            var companyRepository = _companyRepositoryFactory();
            var dbData = _cbdDataFactory();
            var companyToDelete = dbData.Query<Organization>().Where(x => entitiesToDeleteIds.Contains(x.Id)).Select(x => x.Company.Id);

            await companyRepository.DeleteAsync(x => companyToDelete.Contains(x.Id));
        }
    }
}
