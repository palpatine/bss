﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Logic
{
    internal sealed class BssQueryTranslationHander : IBssQueryTranslationHander
    {
        public void HandleExpression(
            Expression node,
            QueryTranslationContext context)
        {
            var methodCallExpression = node as MethodCallExpression;
            if (methodCallExpression != null)
            {
                HandeMethodCall(methodCallExpression, context);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        private static PropertyInfo GetProperty(Type type, string name)
        {
            if (type.IsInterface)
            {
                var stack = new Stack<Type>();
                stack.Push(type);

                while (stack.Any())
                {
                    var current = stack.Pop();
                    var property = current.GetProperty(name);

                    if (property != null)
                    {
                        return property;
                    }

                    var types = current.GetInterfaces();

                    foreach (var t in types)
                    {
                        stack.Push(t);
                    }
                }
            }
            else
            {
                return type.GetProperty(name);
            }

            throw new InvalidOperationException();
        }

        private static void HandleIsEquivalent(MethodCallExpression node, QueryTranslationContext context)
        {
            QueryPartDescriptor left;
            if (node.Arguments[0].NodeType == ExpressionType.Parameter)
            {
                // todo: handle tablereferencegroup
                var table = (TableReference)context.FindTableReference(node.Arguments[0].Type);
                left = new ColumnReference
                {
                    RelatedEntityProperty = GetProperty(node.Arguments[0].Type, "Id"),
                    QueryText = table.Alias == null ?
                    string.Format(QueryTranslationContext.BraceTemplate, "Id") :
                    string.Format(QueryTranslationContext.PrefixedTemplate, table.Alias, "Id"),
                    TableReference = table,
                };
            }
            else
            {
                context.Visitor.Visit(node.Arguments[0]);
                left = context.Stack.Pop();
            }

            context.Visitor.Visit(node.Arguments[1]);
            var right = context.Stack.Pop();

            var value = left.Kind == QueryPartKind.ColumnReference ? (ColumnReference)left : (ColumnReference)right;
            var parameter = left.Kind == QueryPartKind.Parameter ? (QueryParameter)left : (QueryParameter)right;

            var identifierProperty = ExpressionExtensions.GetProperty((IStorable x) => x.Id);
            parameter.Expression = Expression.Property(parameter.Expression, identifierProperty);
            parameter.RelatedEntityProperty = value.RelatedEntityProperty;

            var part = new Comparission
            {
                QueryText = string.Format(
                    CultureInfo.InvariantCulture,
                    "({0} = {1})",
                    value.QueryText,
                    parameter.Alias),
                ParameterValues = parameter.ParameterValues
            };

            context.Stack.Push(part);
        }

        private void HandeMethodCall(
                            MethodCallExpression node,
            QueryTranslationContext context)
        {
            var isEquivalentMethod = ExpressionExtensions.GetMethod(() => StorableExtensions.IsEquivalent<IUser>(null, null));
            if (node.Method.DeclaringType == typeof(StorableExtensions) && node.Method.Name == isEquivalentMethod.Name)
            {
                HandleIsEquivalent(node, context);
                return;
            }

            var expandMethod =
                ExpressionExtensions.GetMethod(
                    () => BssTableQueryExtensions.Expand<IPermissionUser, PermissionUser>(null));
            if (node.Method.DeclaringType == typeof(BssTableQueryExtensions) && node.Method.Name == expandMethod.Name)
            {
                HandleExpand(node, context);
                return;
            }

            throw new InvalidOperationException();
        }

        private void HandleExpand(MethodCallExpression node, QueryTranslationContext context)
        {
            context.Visitor.Visit(node.Arguments[0]);
            var selectionList = context.TryFindSelectionList();
            var joiner = selectionList.Columns.Single();
            var firstType = joiner.RelatedEntityProperty.DeclaringType;
            var joinedType = node.Method.GetGenericArguments()[1];
            var tableName = context.TableNameResolver.GetTableName(joinedType);

            // todo: handle tablereferencegroup
            var tableReference = (TableReference)context.FindTableReference(firstType);
            var alias = context.AliasManager.GetAliasForTable(tableName);
            var joinedTableReference = new TableReference
            {
                Alias = alias == null ? null : string.Format(QueryTranslationContext.BraceTemplate, alias),
                QueryText = tableName,
                Type = joinedType
            };

            var primaryKey = firstType.GetProperty("Id");
            var primaryKeyColumnReference = new ColumnReference
            {
                RelatedEntityProperty = primaryKey,
                QueryText = joinedTableReference.Alias == null ?
                    string.Format(QueryTranslationContext.BraceTemplate, "Id") :
                    string.Format(QueryTranslationContext.PrefixedTemplate, joinedTableReference.Alias, "Id"),
                TableReference = tableReference,
                Type = primaryKey.PropertyType
            };

            var handler = new FromClauseHandler(context);
            handler.AddToFrom(tableReference.ToReferenceGroup(), joinedTableReference, primaryKeyColumnReference, joiner);

            selectionList.Type = joinedType;
            if (joinedTableReference.Alias == null)
            {
                selectionList.QueryText = "*";
            }
            else
            {
                selectionList.QueryText = $"{joinedTableReference.Alias}.*";
            }

            selectionList.Columns = null;
        }
    }
}