﻿using System;
using System.Linq.Expressions;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

// ReSharper disable CheckNamespace generic class visible througout application
namespace SolvesIt.BSSystem

// ReSharper restore CheckNamespace
{
    public static class BssTableQueryExtensions
    {
        public static ITableQuery<TProjection> Join<TStorable, TOther, TOtherIndicator, TProjection>(
            this ITableQuery<TStorable> query,
            ITableQuery<TOther> other,
            Expression<Func<TStorable, TOtherIndicator>> joiner,
            Expression<Func<TStorable, TOther, TProjection>> selector)
            where TOther : TOtherIndicator
            where TOtherIndicator : IStorable
        {
            var idProperty = ExpressionExtensions.GetDeclaredProperty((IStorable x) => x.Id);
            var firstKey = Expression.Lambda<Func<TStorable, int>>(Expression.Property(joiner.Body, idProperty), joiner.Parameters);
            var parameter = Expression.Parameter(typeof(TOther));
            var property = Expression.Property(parameter, idProperty);
            var secondKey = Expression.Lambda<Func<TOther, int>>(property, parameter);
            return TableQuery.Join(query, other, firstKey, secondKey, selector);
        }

        [QueryTranslationHander(typeof(IBssQueryTranslationHander))]
        public static ITableQuery<TOther> Expand<TOtherIndicator, TOther>(
            this ITableQuery<TOtherIndicator> query)
            where TOther : TOtherIndicator, new()
            where TOtherIndicator : IStorable
        {
            var method = ExpressionExtensions.GetMethod(() => Expand<TOtherIndicator, TOther>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TOther>(expression);
        }
    }
}
