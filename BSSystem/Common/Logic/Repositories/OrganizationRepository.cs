﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.CommonModule.Abstraction.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.CommonModule.Logic.Repositories
{
    internal class OrganizationRepository : VersionedEntityRepository<Organization, IOrganization>, IOrganizationRepository
    {
        public OrganizationRepository(
            IStorableObjectsMetadataProvider enitityMetadataProvider,
            ISessionDataProvider sessionDataProvider,
            IConfigurationProvider configurationProvider,
            IWhereClauseGenerator whereClauseGenerator,
            IDatabaseManager databaseManager)
            : base(
                enitityMetadataProvider,
                sessionDataProvider,
                configurationProvider,
                whereClauseGenerator,
                databaseManager)
        {
        }

        public async Task<IEnumerable<ValidationResultUdt>> CreateAdminAliasAsync(IOrganization organization)
        {
            using (DatabaseManager.OpenTransaction())
            {
                var sqlCommand = DatabaseManager.Connection.CreateSqlCommand("[Common].[CreateAdminAlias]", CommandType.StoredProcedure);
                sqlCommand.Parameters.AddWithValue("@UserId", SessionDataProvider.CurrentUser.Id);
                sqlCommand.Parameters.AddWithValue("@TargetOrganizationId", organization.Id);
                sqlCommand.Parameters.AddWithValue("@SessionId", SessionDataProvider.Session.Id);

                var validationResults = await sqlCommand.ExecuteWithValidationAsync();

                DatabaseManager.ComitTransaction();
                return validationResults;
            }
        }

        public async Task<IEnumerable<ValidationResultUdt>> UpdatePermissionsAsync(
            IOrganization organization,
            IEnumerable<JunctionUdt<ISystemPermission>> targetPermissions)
        {
            using (DatabaseManager.OpenTransaction())
            {
                var targetPermissionsSqlParameterValue = StorableHelper.ConvertToDataRecords(targetPermissions);

                var sqlCommand = DatabaseManager.Connection.CreateSqlCommand("[Common].[Update_Permissions]", CommandType.StoredProcedure);
                sqlCommand.Parameters.AddWithValue("@ChangerId", SessionDataProvider.CurrentUser.Id);
                sqlCommand.Parameters.AddWithValue("@OrganizationId", organization.Id);
                var tableParameter = sqlCommand.Parameters.Add("@TargetPermissions", SqlDbType.Structured);
                tableParameter.Value = targetPermissionsSqlParameterValue;
                var newListType = typeof(JunctionUdt<>).GetCustomAttribute<SqlNameAttribute>().Name;
                tableParameter.TypeName = newListType;
                var validationResults = await sqlCommand.ExecuteWithValidationAsync();

                DatabaseManager.ComitTransaction();
                return validationResults;
            }
        }
    }
}
