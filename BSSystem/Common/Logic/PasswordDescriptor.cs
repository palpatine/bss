﻿namespace SolvesIt.BSSystem.CommonModule.Logic
{
    public class PasswordDescriptor
    {
        public byte[] Salt { get; set; }

        public byte[] Password { get; set; }

        public string DecriptedPassword { get; set; }
    }
}