﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.CommonModule.Logic.Services
{
    public class SettingService : ISettingService
    {
        private const string CurrentUserEffectiveSettingsCacheKey = "CurrentUserEffectiveSettings";
        private readonly ICache _cache;
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ISessionDataProvider _sessionDataProvider;

        public SettingService(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            ICache cache)
        {
            _databaseAccessor = databaseAccessor;
            _sessionDataProvider = sessionDataProvider;
            _cache = cache;
        }

        public async Task<IEnumerable<EffectiveSettingDto>> GetCurrentUserEffectiveSettings()
        {
            var user = _sessionDataProvider.CurrentUser;
            return await _cache.GetUsersCacheValueAsync(
                CurrentUserEffectiveSettingsCacheKey,
                () => _databaseAccessor.GetUserEffectiveSettings(user).ToListAsync(),
                new TimeSpan(0, 10, 0));
        }
    }
}