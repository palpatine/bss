﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.CommonModule.Logic.Security;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Notification;

namespace SolvesIt.BSSystem.CommonModule.Logic.Services
{
    internal class UserService
        : VersionedEntityService<User, UserEditModel, IUser>
            , IUserService
    {
        private readonly IEntityRepository<Login, ILogin> _loginRepository;
        private readonly IMailSender _mailSender;
        private readonly IEnumerable<IEntityValidator<ChangePasswordModel>> _passwordValidators;
        private readonly ISecurityManager _securityManager;
        private readonly ISessionDataProvider _sessionDataProvider;

        public UserService(
            IEventAggregator eventAggregator,
            IEnumerable<IEntityValidator<UserEditModel>> validators,
            IEnumerable<IEntityValidator<ChangePasswordModel>> passwordValidators,
            IDatabaseManager databaseManager,
            IEnumerable<IServiceExtension<UserEditModel>> extensions,
            IVersionedEntityRepository<User, IUser> userRepository,
            ILockRepository lockRepository,
            IEntityRepository<Login, ILogin> loginRepository,
            ISessionDataProvider sessionDataProvider,
            ISecurityManager securityManager,
            IMailSender mailSender,
            IDatabaseAccessor databaseAccessor,
            IEnumerable<IEntityDeletionValidator<IUser>> deletionValidators)
            : base(
                  eventAggregator,
                  lockRepository,
                  extensions,
                  userRepository,
                  validators,
                  databaseManager,
                  databaseAccessor,
                  deletionValidators)
        {
            _passwordValidators = passwordValidators;
            _loginRepository = loginRepository;
            _securityManager = securityManager;
            _sessionDataProvider = sessionDataProvider;
            _mailSender = mailSender;
        }

        public async Task<IEnumerable<ValidationError>> ChangePasswordAsync(
            ChangePasswordModel changePasswordModel)
        {
            var errors = (await _passwordValidators
                .SelectManyAsync(x => x.ValidateModelAsync(changePasswordModel))).ToArray();
            if (!errors.Any())
            {
                var login = await DatabaseAccessor.Query<Login>()
                    .SingleAsync(x => !x.IsDeleted && x.User.IsEquivalent(_sessionDataProvider.CurrentUser));

                var operationId = Guid.NewGuid().ToString();
                var result = await LockRepository.TryLockAsync(login, operationId);
                if (result.Any())
                {
                    return result;
                }

                var password = _securityManager.EncryptPassword(changePasswordModel.NewPassword);
                login.Password = password.Password;
                login.Salt = password.Salt;
                await _loginRepository.WriteAsync(login);

                await LockRepository.RemoveLockAsync(operationId);
            }

            return errors;
        }

        public async Task<Tuple<IUser, IOrganization>> GetUserAndOrganizationByLoginAsync(
            string loginName,
            string password)
        {
            var splits = loginName.Replace('/', '\\').Split('\\');

            if (splits.Count() != 2)
            {
                return null;
            }

            return await GetUserAndOrganizationByLoginAsync(splits[0], splits[1], password);
        }

        public async Task<Tuple<IUser, IOrganization>> GetUserAndOrganizationByLoginAsync(
            string organization,
            string loginName,
            string password)
        {
            var data = await DatabaseAccessor.GetUserAndOrganizationByLogin(organization, loginName)
                .SingleOrDefaultAsync();

            if (data != null && _securityManager.ComparePasswords(password, data.Password, data.Salt))
            {
                return Tuple.Create<IUser, IOrganization>(new UserMarker(data.UserId), new OrganizationMarker(data.OrganizationId));
            }

            return null;
        }

        public async Task ResetPasswordAsync(IUser user)
        {
            var model = await DatabaseAccessor.Query<User>()
                .Where(x => x.IsEquivalent(user))
                .Select(x => new { x.FirstName, x.Surname, x.Mail })
                .SingleAsync();
            var login = await DatabaseAccessor.Query<Login>()
                .SingleAsync(x => !x.IsDeleted && x.User.IsEquivalent(user));
            if (login != null)
            {
                var password = _securityManager.GenerateRandomPassword();
                login.Password = password.Password;
                login.Salt = password.Salt;
                await _loginRepository.WriteAsync(login);

                var address = new EmailAddress
                {
                    Address = model.Mail,
                    Name = model.FirstName + " " + model.Surname
                };
                var passwordReset = new SystemNotificationTemplates.PasswordReset(
                    model.FirstName,
                    model.Surname,
                    login.LoginName,
                    password.DecriptedPassword);
                _mailSender.Send(
                    SystemNotificationTemplates.PasswordReset.TempalateName,
                    new EmailAddressList(address),
                    passwordReset.Attributes);
            }
        }

        public override async Task<IEnumerable<ValidationError>> TryLockAsync(
                            IUser marker,
            string operationToken)
        {
            var userLockErrors = await LockRepository.TryLockAsync(marker, operationToken);
            if (userLockErrors.Any())
            {
                return userLockErrors;
            }

            var loginLockErrors = await LockRepository.TryLockAsync(typeof(Login), ExpressionExtensions.GetProperty((Login x) => x.User), marker, operationToken);
            if (loginLockErrors.Any())
            {
                await LockRepository.RemoveLockAsync(operationToken);
                return loginLockErrors;
            }

            return Enumerable.Empty<ValidationError>();
        }

        protected override async Task OnReadyToSaveAsync(UserEditModel model)
        {
            using (DatabaseManager.OpenTransaction())
            {
                User user;
                if (model.Id > 0)
                {
                    user = await DatabaseAccessor.Query<User>()
                    .SingleAsync(x => x.IsEquivalent<IUser>(model));
                }
                else
                {
                    user = new User();
                }

                user.Scope = _sessionDataProvider.Scope;
                user.DisplayName = model.DisplayName;
                user.Surname = model.Surname;
                user.FirstName = model.FirstName;
                user.DateStart = model.DateStart;
                user.DateEnd = model.DateEnd;
                user.Mail = model.Mail;
                user.Phone = model.Phone;
                user.Location = model.Location;
                user.IsBillable = true;

                await Repository.WriteAsync(user);
                model.Id = user.Id;
                var login = await DatabaseAccessor.Query<Login>()
                    .SingleOrDefaultAsync(x => !x.IsDeleted && x.User.IsEquivalent(user));
                await UpdateUserLogin(model, login);

                DatabaseManager.ComitTransaction();
            }
        }

        private void SedNewUserNotification(UserEditModel model, string password)
        {
            var address = new EmailAddress
            {
                Address = model.Mail,
                Name = model.FirstName + " " + model.Surname
            };
            var newUserRegistration = new SystemNotificationTemplates.NewUserRegistration(
                model.FirstName,
                model.Surname,
                model.LoginName,
                password);
            _mailSender.Send(
                SystemNotificationTemplates.NewUserRegistration.TempalateName,
                new EmailAddressList(address),
                newUserRegistration.Attributes);
        }

        private async Task UpdateUserLogin(UserEditModel model, Login login)
        {
            if (login != null && string.IsNullOrWhiteSpace(model.LoginName))
            {
                await _loginRepository.DeleteAsync(login);
            }
            else if (!string.IsNullOrWhiteSpace(model.LoginName))
            {
                if (login == null)
                {
                    login = new Login();
                    var passwordDescriptor = _securityManager.GenerateRandomPassword();
                    login.Salt = passwordDescriptor.Salt;
                    login.Password = passwordDescriptor.Password;
                    login.User = model;
                    login.Scope = _sessionDataProvider.Scope;
                    SedNewUserNotification(model, passwordDescriptor.DecriptedPassword);
                }

                if (login.LoginName != model.LoginName)
                {
                    login.LoginName = model.LoginName;
                    await _loginRepository.WriteAsync(login);
                }
            }
        }
    }
}