﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.CommonModule.Logic.Services
{
    public class PermissionService : IPermissionService
    {
        private const string CurrentUserEffectivePermissionsCacheKey = "CurrentUserEffectivePermissions";
        private const string CurrentUserEffectivePermissionsContextCacheKey = "CurrentUserEffectivePermissionsContext";
        private const string CurrentUserEffectivePermissionsContextValuesCacheKey = "CurrentUserEffectivePermissionsContextValues";
        private readonly ICache _cache;
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ISessionDataProvider _sessionDataProvider;

        public PermissionService(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            ICache cache)
        {
            _sessionDataProvider = sessionDataProvider;
            _cache = cache;
            _databaseAccessor = databaseAccessor;
        }

        public async Task<IEnumerable<UserEffectivePermissionDto>> GetCurrentUserEffectivePermissions()
        {
            var user = _sessionDataProvider.CurrentUser;
            return await _cache.GetUsersCacheValueAsync(
                CurrentUserEffectivePermissionsCacheKey,
                () => _databaseAccessor.GetUserEffectivePermissions(user).ToListAsync(),
                new TimeSpan(0, 10, 0));
        }

        public async Task<IEnumerable<int>> GetCurrentUserEffectivePermissionsContextValues(
            GetUserEffectivePermissionAssignmentDto permissionAssignment)
        {
            var user = _sessionDataProvider.CurrentUser;
            Type assignmentTableType;
            if (!StorableHelper.TryGetEntityType(permissionAssignment.AssignmentTableKey, out assignmentTableType))
            {
                throw new InvalidOperationException();
            }

            var permissionMarker = new PermissionMarker(permissionAssignment.PermissionId);
            var query = _databaseAccessor.Query<IRoleAssignmentJunctionEntity>(assignmentTableType)
                .Where(x => !x.IsDeleted
                            && x.User.IsEquivalent(user)
                            && x.DateStart <= DateTime.Today
                            && (x.DateEnd ?? DateTime.Today) >= DateTime.Today)
                .Join(
                    _databaseAccessor.Query<PermissionRoleDefinition>().Where(x => !x.IsDeleted),
                    x => x.RoleDefinition,
                    x => x.RoleDefinition,
                    (j, r) => new
                    {
                        EntityId = j.Related.Id,
                        r.Permission
                    });

            var value = await _cache.GetUsersCacheValueAsync(
                $"{CurrentUserEffectivePermissionsContextValuesCacheKey}_{permissionAssignment.AssignmentTableKey}",
               async () => await query.ToListAsync(),
                            new TimeSpan(0, 10, 0));

            return value.Where(x => x.Permission.IsEquivalent(permissionMarker))
                .Select(x => x.EntityId)
                .ToList();
        }

        public async Task<IEnumerable<GetUserEffectivePermissionAssignmentDto>> GetUserEffectivePermissionAssignments()
        {
            var user = _sessionDataProvider.CurrentUser;
            var value = await _cache.GetUsersCacheValueAsync(
                CurrentUserEffectivePermissionsContextCacheKey,
                () => _databaseAccessor.GetUserEffectivePermissionAssignments(user).ToListAsync(),
                new TimeSpan(0, 10, 0));
            return value;
        }
    }
}