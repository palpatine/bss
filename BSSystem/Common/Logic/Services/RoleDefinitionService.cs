using System.Collections.Generic;
using System.Linq;
using Qdarc.Modules.Common.Communication;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.CommonModule.Logic.Services
{
    internal class RoleDefinitionService : VersionedEntityService<RoleDefinition, IRoleDefinition>,
        IRoleDefinitionService
    {
        private readonly IEnumerable<RoleTargetDescriptor> _roleTargets;

        public RoleDefinitionService(
            IEventAggregator eventAggregator,
            ILockRepository lockRepository,
            IEnumerable<IServiceExtension<RoleDefinition>> extensions,
            IVersionedEntityRepository<RoleDefinition, IRoleDefinition> repository,
            IEnumerable<IEntityValidator<RoleDefinition>> validators,
            IDatabaseManager databaseManager,
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IEnumerable<IRoleTargetsProvider> roleTargetsProviders,
            IEnumerable<IEntityDeletionValidator<IRoleDefinition>> deletionValidators)
            : base(
                  eventAggregator,
                  lockRepository,
                  extensions,
                  repository,
                  validators,
                  databaseManager,
                  databaseAccessor,
                  sessionDataProvider,
                  deletionValidators)
        {
            _roleTargets = roleTargetsProviders.SelectMany(x => x.Targets).ToArray();
        }

        public IEnumerable<RoleTargetDescriptor> RoleTargets => _roleTargets;
    }
}