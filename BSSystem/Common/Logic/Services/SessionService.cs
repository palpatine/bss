﻿using System.Collections.Generic;
using Qdarc.Modules.Common.Communication;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic;

namespace SolvesIt.BSSystem.CommonModule.Logic.Services
{
    internal class SessionService :
        EntityService<Session, ISession>,
        ISessionService
    {
        public SessionService(
            IEventAggregator eventAggregator,
            IEntityRepository<Session, ISession> sessionRepository,
            IEnumerable<IEntityValidator<Session>> validators,
            IDatabaseManager databaseManager,
            IDatabaseAccessor databaseAccessor)
            : base(eventAggregator, sessionRepository, validators, databaseManager, databaseAccessor)
        {
        }

        public override IEnumerable<ValidationError> Write(Session entity)
        {
            EntityRepository.Write(entity);
            return null;
        }
    }
}