﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.CommonModule.Abstraction.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.CommonModule.Logic.Validation;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic;

namespace SolvesIt.BSSystem.CommonModule.Logic.Services
{
    internal class OrganizationService
        : VersionedEntityService<Organization, OrganizationEditModel, IOrganization>
            , IOrganizationService
    {
        private readonly IVersionedEntityRepository<Company, ICompany> _companyRepository;
        private readonly IVersionedEntityRepository<ModuleOrganization, IModuleOrganization> _moduleOrganizationRepository;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IVersionedEntityRepository<Permission, IPermission> _permissionRepository;

        public OrganizationService(
            IEventAggregator eventAggregator,
            IEnumerable<IEntityValidator<OrganizationEditModel>> validators,
            IDatabaseManager databaseManager,
            IEnumerable<IServiceExtension<OrganizationEditModel>> extensions,
            IOrganizationRepository organizationRepository,
            ILockRepository lockRepository,
            IDatabaseAccessor databaseAccessor,
            IVersionedEntityRepository<Company, ICompany> companyRepository,
            IVersionedEntityRepository<ModuleOrganization, IModuleOrganization> moduleOrganizationRepository,
            IVersionedEntityRepository<Permission, IPermission> permissionRepository,
            IEnumerable<IEntityDeletionValidator<IOrganization>> deletionValidators)
            : base(
                eventAggregator,
                lockRepository,
                extensions,
                organizationRepository,
                validators,
                databaseManager,
                databaseAccessor,
                deletionValidators)
        {
            _organizationRepository = organizationRepository;
            _companyRepository = companyRepository;
            _moduleOrganizationRepository = moduleOrganizationRepository;
            _permissionRepository = permissionRepository;
        }

        public async Task<IEnumerable<ValidationError>> CreateAdminAliasAsync(
            IOrganization organization)
        {
            var results = await _organizationRepository.CreateAdminAliasAsync(organization);
            return results.Select(x => new ValidationError(
                ValidationErrors.ResourceManager.GetString(x.MessageCode) ?? x.MessageCode,
                x.MessageCode,
                null,
                x.AdditionalValue)).ToArray();
        }

        public override async Task<IEnumerable<ValidationError>> TryLockAsync(
            IOrganization organization,
            string operationToken)
        {
            var errors = await LockRepository.TryLockAsync(organization, operationToken);
            if (!errors.Any())
            {
                var company = await DatabaseAccessor.Query<Organization>()
                    .Where(o => o.IsEquivalent(organization) && !o.IsDeleted)
                    .Select(o => o.Company)
                    .SingleAsync();
                errors = await LockRepository.TryLockAsync(company, operationToken);

                if (errors.Any())
                {
                    await LockRepository.RemoveLockAsync(operationToken);
                }
            }

            return errors;
        }

        public async Task<IEnumerable<ValidationError>> UpdatePermissionsAsync(
            IOrganization organization,
            IEnumerable<JunctionUdt<ISystemPermission>> targetPermissions)
        {
            var results = await _organizationRepository.UpdatePermissionsAsync(organization, targetPermissions);
            return results.Select(x =>
            {
                string reference = null;
                if (x.Id != null)
                {
                    reference = $"assignments[{x.Id}].{x.Field ?? "Element"}";
                }

                return new ValidationError(
                    ValidationErrors.ResourceManager.GetString(x.MessageCode) ?? x.MessageCode,
                    x.MessageCode,
                    reference,
                    x.AdditionalValue);
            }).ToArray();
        }

        protected override async Task OnReadyToSaveAsync(OrganizationEditModel model)
        {
            using (DatabaseManager.OpenTransaction())
            {
                Company company;
                Organization organization;
                if (model.Id > 0)
                {
                    organization = await DatabaseAccessor.Query<Organization>().SingleAsync(o => o.IsEquivalent<IOrganization>(model));
                    company = await DatabaseAccessor.Query<Company>().SingleAsync(o => o.IsEquivalent(organization.Company));
                }
                else
                {
                    organization = new Organization();
                    company = new Company();
                }

                company.CountryCode = model.CountryCode;
                company.Phone = model.Phone;
                company.Email = model.Email;
                company.Fax = model.Fax;
                company.FullName = model.FullName;
                company.Name = model.Name;
                company.OfficialNumber = model.OfficialNumber;
                company.StatisticNumber = model.StatisticNumber;
                company.VatNumber = model.VatNumber;

                await _companyRepository.WriteAsync(company);

                organization.Company = company;
                organization.DisplayName = model.DisplayName;
                organization.DateEnd = model.DateEnd;
                organization.DateStart = model.DateStart;

                await Repository.WriteAsync(organization);

                if (model.Id == 0)
                {
                    await SetOrganizationDefaults(organization);
                }

                model.Id = organization.Id;
                DatabaseManager.ComitTransaction();
            }
        }

        private async Task SetOrganizationDefaults(Organization organization)
        {
            var operationToken = Guid.NewGuid().ToString("N");
            var validationError = await LockRepository.TryLockAsync(
                typeof(ModuleOrganization),
                ExpressionExtensions.GetProperty((ModuleOrganization m) => m.Organization),
                organization,
                operationToken);

            if (validationError.Any())
            {
                throw new InvalidOperationException();
            }

            var module = await DatabaseAccessor.Query<Module>()
                .Where(x => x.Key == "Common")
                .Select(x => new ModuleMarker(x.Id))
                .SingleAsync();

            var moduleOrganization = new ModuleOrganization
            {
                DateStart = organization.DateStart,
                Organization = organization,
                Module = module
            };

            await _moduleOrganizationRepository.WriteAsync(moduleOrganization);

            var systemPermissions = await DatabaseAccessor.Query<SystemPermission>()
                .Where(x => x.IsOrganizationDefault && x.Module.IsEquivalent(module))
                .Select(x => new SystemPermissionMarker(x.Id))
                .ToListAsync();

            var permissions = systemPermissions.Select(x => new Permission
            {
                Scope = organization,
                SystemPermission = x
            })
                .ToArray();

            foreach (var permission in permissions)
            {
                await _permissionRepository.WriteAsync(permission);
            }

            await LockRepository.RemoveLockAsync(operationToken);
        }
    }
}