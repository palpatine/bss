﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Dtos;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.CommonModule.Logic.Services
{
    public class MenuService : IMenuService
    {
        private const string CurrentUserMenuCacheKey = "CurrentUserMenu";
        private readonly ICache _cache;
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ISessionDataProvider _sessionDataProvider;

        public MenuService(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            ICache cache)
        {
            _sessionDataProvider = sessionDataProvider;
            _cache = cache;
            _databaseAccessor = databaseAccessor;
        }

        public async Task<IEnumerable<MenuPositionDto>> GetCurrentUserMenu()
        {
            var user = _sessionDataProvider.CurrentUser;
            return await _cache.GetUsersCacheValueAsync(
                CurrentUserMenuCacheKey,
                () => _databaseAccessor.GetUserMenu(user).ToListAsync(),
                new TimeSpan(0, 10, 0));
        }
    }
}