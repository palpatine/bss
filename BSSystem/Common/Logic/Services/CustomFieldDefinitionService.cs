﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.CommonModule.Logic.CustomFields;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.CommonModule.Logic.Services
{
    internal class CustomFieldDefinitionService :
        VersionedEntityService<CustomFieldDefinition, CustomFieldDefinitionEditModel, ICustomFieldDefinition>,
        ICustomFieldDefinitionService
    {
        private readonly ICustomFieldsGenerator _customFieldsGenerator;
        private readonly ICustomFieldsParser _customFieldsParser;
        private readonly ISessionDataProvider _sessionDataProvider;
        private readonly IEnumerable<CustomFieldTargetDescriptor> _signableTargets;
        private readonly IEnumerable<ITerminalValueProvider> _terminalValueProviders;

        public CustomFieldDefinitionService(
            IEventAggregator eventAggregator,
            ILockRepository lockRepository,
            IEnumerable<IServiceExtension<CustomFieldDefinitionEditModel>> extensions,
            IVersionedEntityRepository<CustomFieldDefinition, ICustomFieldDefinition> customFieldsEntityRepository,
            IEnumerable<IEntityValidator<CustomFieldDefinitionEditModel>> validators,
            IDatabaseManager databaseManager,
            ICustomFieldsGenerator customFieldsGenerator,
            ICustomFieldsParser customFieldsParser,
            IEnumerable<ICustomFieldTargetsProvider> signableTargetsProviders,
            IEnumerable<ITerminalValueProvider> terminalValueProviders,
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IEnumerable<IEntityDeletionValidator<ICustomFieldDefinition>> deletionValidators)
            : base(
                eventAggregator,
                lockRepository,
                extensions,
                customFieldsEntityRepository,
                validators,
                databaseManager,
                databaseAccessor,
                deletionValidators)
        {
            _customFieldsGenerator = customFieldsGenerator;
            _customFieldsParser = customFieldsParser;
            _terminalValueProviders = terminalValueProviders;
            _sessionDataProvider = sessionDataProvider;
            _signableTargets = signableTargetsProviders.SelectMany(x => x.Targets).ToArray();
        }

        public IDictionary<ICustomFieldTarget, IEnumerable<TerminalDescriptor>> ApplicableTerminals
        {
            get
            {
                var targets = SignableTargets;
                var terminals =
                    targets.Select(target =>
                                       new
                                       {
                                           Key = target.CustomFieldTarget,
                                           Value = _terminalValueProviders
                                       .Where(x => string.IsNullOrWhiteSpace(x.TargetName) || x.TargetName == target.CustomFieldTarget.Key)
                                       .SelectMany(x => x.HandledTokens)
                                       .ToArray()
                                       .AsEnumerable()
                                       })
                           .ToDictionary(x => x.Key, x => x.Value);

                return terminals;
            }
        }

        public IEnumerable<CustomFieldTargetDescriptor> SignableTargets => _signableTargets;

        protected override async Task OnReadyToSaveAsync(CustomFieldDefinitionEditModel model)
        {
            CustomFieldDefinition customField;
            if (model.Id != 0)
            {
                customField = await DatabaseAccessor.Query<CustomFieldDefinition>()
                                   .SingleAsync(x => x.Id == model.Id);
            }
            else
            {
                customField = new CustomFieldDefinition()
                {
                    Scope = _sessionDataProvider.Scope
                };
            }

            customField.DateEnd = model.DateEnd;
            customField.DateStart = model.DateStart;
            customField.DisplayName = model.DisplayName;
            customField.IsEditable = model.IsEditable;
            customField.IsInViews = model.IsInViews;
            customField.IsNullable = model.IsNullable;
            customField.IsRequired = model.IsRequired;
            customField.IsUnique = model.IsUnique;
            customField.Schema = model.Schema;

            if (model.Id == 0 || await CanEditSchema(customField))
            {
                customField.Schema = model.Schema;
                var tokens = _customFieldsParser.Parse(model.Schema);
                customField.Mask = _customFieldsGenerator.BuildSignMask(customField, tokens);
            }

            await Repository.WriteAsync(customField);
        }

        private async Task<bool> CanEditSchema(CustomFieldDefinition customField)
        {
            var signsAssignmentType = SignableTargets.Single(x => x.CustomFieldTarget.Key == customField.TableKey).TargetType;
            return await IsSchemaUsed(customField, signsAssignmentType);
        }

        private async Task<bool> IsSchemaUsed(CustomFieldDefinition customField, Type signAssignmentType)
        {
            return await DatabaseAccessor.Query<ICustomFieldAssignmentDescriptor>(signAssignmentType)
                .Where(x => x.CustomFieldDefinition.IsEquivalent(customField))
                .AnyAsync();
        }
    }
}