﻿namespace SolvesIt.BSSystem.CommonModule.Logic.Security
{
    public interface ISecurityManager
    {
        bool ComparePasswords(string first, byte[] second, byte[] salt);

        PasswordDescriptor EncryptPassword(string password);

        PasswordDescriptor GenerateRandomPassword();
    }
}