﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SolvesIt.BSSystem.CommonModule.Logic.Security
{
    internal class SecurityManager : ISecurityManager
    {
        private const string DefaultSecret = "efdac326fs)(bdvwey6%{0}$fsudwe";

        private const string PasswordCharacters =
            "qazxswedcvfrtgbnhyujmkiolpZAQWSXCDERFVBGTYHNMJUIKLOP1234567890!@#$%^&*()-=_+[]\\{}|';\":/.,?><";

        private static readonly Random Random = new Random();
        private static readonly byte[] Salt = Encoding.ASCII.GetBytes(@"63fuW)^=vm-[135s");

        public static string Decrypt(byte[] bytes, string sharedSecret, byte[] salt)
        {
            if (bytes == null)
            {
                throw new ArgumentNullException("cipherText");
            }

            if (string.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException("sharedSecret");
            }

            string result = null;

            using (var key = new Rfc2898DeriveBytes(sharedSecret, Salt.Concat(salt).ToArray()))

            // Create the streams used for decryption.
            using (var msDecrypt = new MemoryStream(bytes))
            {
                // Create a RijndaelManaged object
                // with the specified key and IV.
                using (var aesAlg = new RijndaelManaged())
                {
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                    // Get the initialization vector from the encrypted stream
                    aesAlg.IV = ReadByteArray(msDecrypt);

                    // Create a decrytor to perform the stream transform.
                    var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            result = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return result;
        }

        public static byte[] Encrypt(string plainText, string sharedSecret, byte[] salt)
        {
            if (string.IsNullOrEmpty(plainText))
            {
                throw new ArgumentNullException("plainText");
            }

            if (string.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException("sharedSecret");
            }

            // generate the key from the shared secret and the salt
            using (var key = new Rfc2898DeriveBytes(sharedSecret, Salt.Concat(salt).ToArray()))
            {
                // Create a RijndaelManaged object
                using (var aesAlg = new RijndaelManaged())
                {
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                    // Create a decryptor to perform the stream transform.
                    var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                    // Create the streams used for encryption.
                    using (var msEncrypt = new MemoryStream())
                    {
                        // prepend the IV
                        msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                        msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                // Write all data to the stream.
                                swEncrypt.Write(plainText);
                            }
                        }

                        return msEncrypt.ToArray();
                    }
                }
            }
        }

        public bool ComparePasswords(string first, byte[] second, byte[] salt)
        {
            try
            {
                var decripted = Decrypt(second, string.Format(DefaultSecret, first), salt);
                return first == decripted;
            }
            catch (CryptographicException)
            {
                return false;
            }
        }

        public PasswordDescriptor EncryptPassword(string password)
        {
            var salt = new byte[Random.Next(10, 32)];
            Random.NextBytes(salt);
            return new PasswordDescriptor
            {
                Password = Encrypt(password, string.Format(DefaultSecret, password), salt),
                Salt = salt
            };
        }

        public PasswordDescriptor GenerateRandomPassword()
        {
            var password = RandomPassword(10);
            var encripted = EncryptPassword(password);
            encripted.DecriptedPassword = password;
            return encripted;
        }

        private static string RandomPassword(int size)
        {
            var builder = new StringBuilder();

            for (var i = 0; i < size; i++)
            {
                var sign = PasswordCharacters[Random.Next(0, PasswordCharacters.Count())];
                builder.Append(sign);
            }

            return builder.ToString();
        }

        private static byte[] ReadByteArray(Stream s)
        {
            var rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new CryptographicException("Stream did not contain properly formatted byte array");
            }

            var buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new CryptographicException("Did not read byte array properly");
            }

            return buffer;
        }
    }
}