﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Logic.Properties;

namespace SolvesIt.BSSystem.CommonModule.Logic.CustomFields
{
    internal class DateTerminalValueProvider : TerminalValueProvider
    {
        private const char MonthMaskCharacter = '\x05';
        private const char YearMaskCharacter = '\x06';

        public DateTerminalValueProvider()
            : base(new[]
            {
                new TerminalDescriptor
                {
                    DefaultValue = "{yyyy}",
                    Template = "{yyyy}",
                    Name = () => Resources.DateTerminalYYYYName,
                    Description = () => Resources.DateTerminalYYYYDescription,
                },
                new TerminalDescriptor
                {
                    DefaultValue = "{yy}",
                    Template = "{yy}",
                    Name = () => Resources.DateTerminalYYName,
                    Description = () => Resources.DateTerminalYYDescription,
                },
                new TerminalDescriptor
                {
                    DefaultValue = "{MM}",
                    Template = "{MM}",
                    Name = () => Resources.DateTerminalMMName,
                    Description = () => Resources.DateTerminalMMDescription,
                }
            })
        {
        }

        public override string GetMask(CustomFieldGenerationDescriptor descriptor)
        {
            var maskCharacter = descriptor.Terminal[1] == 'y' ? YearMaskCharacter : MonthMaskCharacter;
            return "{" + new string(maskCharacter, descriptor.Terminal.Length - 2) + "}";
        }

        public override Task<string> GetValue(CustomFieldGenerationDescriptor descriptor)
        {
            var result = DateTime.Today.ToString(descriptor.Terminal.Substring(1, descriptor.Terminal.Length - 2));
            if (descriptor.CustomFieldEditableValue != null)
            {
                var tokenValue = descriptor.CustomFieldEditableValue.Substring(
                    descriptor.Token.IndexInSchema,
                    descriptor.Token.Length);

                if (tokenValue != descriptor.Terminal)
                {
                    result = tokenValue.Substring(1, tokenValue.Length - 2);
                }
            }

            return Task.FromResult(result);
        }

        protected override bool ValidateValue(string value, string terminal, out string errorDescription)
        {
            if (terminal == "{yyyy}")
            {
                errorDescription = Resources.DateTerminalYYYYErroneousValueDescription;
                return Regex.IsMatch(value, @"\d{4}|y{4}");
            }

            if (terminal == "{yy}")
            {
                errorDescription = Resources.DateTerminalYYErroneousValueDescription;
                return Regex.IsMatch(value, @"\d{2}|y{2}");
            }

            if (terminal == "{MM}")
            {
                errorDescription = Resources.DateTerminalMMErroneousValueDescription;
                return Regex.IsMatch(value, @"0\d|1[12]|M{2}");
            }

            throw new InvalidOperationException();
        }

        protected override bool IsValueFixed(string value, string terminal)
        {
            if (terminal == "{yyyy}")
            {
                return Regex.IsMatch(value, @"\d{4}");
            }

            if (terminal == "{yy}")
            {
                return Regex.IsMatch(value, @"\d{2}");
            }

            if (terminal == "{MM}")
            {
                return Regex.IsMatch(value, @"0\d|1[12]");
            }

            throw new InvalidOperationException();
        }
    }
}