using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Logic.CustomFields
{
    public interface ICustomFieldsGenerator
    {
        Task<string> BuildCustomFieldValueAsync(
            CustomFieldDefinition customFieldDefinitionEntity,
            IStorable instance,
            IEnumerable<Token> tokens,
            string customFieldEditableValue);

        string BuildSignMask(
            CustomFieldDefinition customFieldDefinition,
            IEnumerable<Token> tokens);

        bool CanGenerateValue(
            string targetKey,
            IEnumerable<Token> tokens,
            out IEnumerable<Token> unknownTokens);

        string BuildSignEditableValueAsync(
            CustomFieldDefinition customFieldDefinition,
            IEnumerable<Token> tokens,
            IStorable instance,
            string customFieldValue);

        IEnumerable<Token> ValidateSignValue(
            CustomFieldDefinition customFieldDefinition,
            IEnumerable<Token> tokens,
            string customFieldValue);

        bool IsSignValueFixed(
            CustomFieldDefinition customFieldDefinition,
            IEnumerable<Token> tokens,
            string customFieldValue);
    }
}