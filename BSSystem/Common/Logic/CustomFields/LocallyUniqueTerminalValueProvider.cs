using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Logic.Properties;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;

namespace SolvesIt.BSSystem.CommonModule.Logic.CustomFields
{
    internal class LocallyUniqueTerminalValueProvider : TerminalValueProvider
    {
        private const string FixedValueDetectionRegexTemplate = @"\d{{{0}}}";
        private const char MaskCharacter = '\x04';
        private const string Terminal = "{#+}";
        private const string ValueValidatorRegexTemplate = @"\d{{{0}}}|{1}";
        private static readonly Regex FormatCounter = new Regex("{(#)+}");
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly IEntityRepository<CustomFieldLastValue, ICustomFieldLastValue> _signValueRepository;

        public LocallyUniqueTerminalValueProvider(
            IEntityRepository<CustomFieldLastValue, ICustomFieldLastValue> signValueRepository,
            IDatabaseAccessor databaseAccessor)
            : base(new[]
            {
                new TerminalDescriptor
                {
                    Template = Terminal,
                    DefaultValue = "{##}",
                    Name = () => Resources.LocallyUniqueTerminalHashName,
                    Description = () => Resources.LocallyUniqueTerminalHashDescription
                }
            })
        {
            _signValueRepository = signValueRepository;
            _databaseAccessor = databaseAccessor;
        }

        public override double ExecutionOrder => 1;

        public override string GetMask(CustomFieldGenerationDescriptor descriptor)
        {
            var digitsCount = GetDigitsCount(descriptor.Terminal);

            return "{" + new string(MaskCharacter, digitsCount) + "}";
        }

        public override async Task<string> GetValue(CustomFieldGenerationDescriptor descriptor)
        {
            if (descriptor.CustomFieldEditableValue != null)
            {
                var tokenValue = descriptor.CustomFieldEditableValue.Substring(
                    descriptor.Token.IndexInSchema,
                    descriptor.Token.Length);

                if (tokenValue != descriptor.Terminal)
                {
                    return tokenValue.Substring(1, tokenValue.Length - 2);
                }
            }

            var digitsCount = GetDigitsCount(descriptor.Terminal);

            var customFieldValue = await _databaseAccessor.Query<CustomFieldLastValue>()
                .Where(x => x.ConstPart == descriptor.PartialResult && x.CustomFieldDefinition.IsEquivalent(descriptor.CustomFieldDefinition))
                .SingleOrDefaultAsync();

            customFieldValue = customFieldValue ??
                        new CustomFieldLastValue
                        {
                            ConstPart = descriptor.PartialResult,
                            CustomFieldDefinition = descriptor.CustomFieldDefinition,
                            IdentityPart = 0
                        };
            customFieldValue.IdentityPart++;
            await _signValueRepository.WriteAsync(customFieldValue);

            return customFieldValue.IdentityPart.ToString("D" + digitsCount);
        }

        protected override bool IsValueFixed(
            string value,
            string terminal)
        {
            var digitsCount = GetDigitsCount(terminal);
            var regex = string.Format(FixedValueDetectionRegexTemplate, digitsCount);

            return Regex.IsMatch(value, regex);
        }

        protected override bool ValidateValue(string value, string terminal, out string errorDescription)
        {
            var digitsCount = GetDigitsCount(terminal);
            var regex = string.Format(ValueValidatorRegexTemplate, digitsCount, new string('#', digitsCount));
            var isInError = Regex.IsMatch(value, regex);

            if (isInError)
            {
                errorDescription = string.Format(CultureInfo.CurrentUICulture, Resources.LocallyUniqueTerminalAtErroneousValueDescription, digitsCount);
            }
            else
            {
                errorDescription = null;
            }

            return isInError;
        }

        private static int GetDigitsCount(string terminal)
        {
            return FormatCounter.Match(terminal).Groups[1].Captures.Count;
        }
    }
}