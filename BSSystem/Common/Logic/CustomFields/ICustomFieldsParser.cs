using System.Collections.Generic;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;

namespace SolvesIt.BSSystem.CommonModule.Logic.CustomFields
{
    public interface ICustomFieldsParser
    {
        IEnumerable<Token> Parse(string schema);

        IEnumerable<string> Validate(string schema, out IEnumerable<Token> tokens);
    }
}