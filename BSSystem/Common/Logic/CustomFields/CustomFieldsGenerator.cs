using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.CommonModule.Logic.CustomFields
{
    internal class CustomFieldsGenerator : ICustomFieldsGenerator
    {
        private readonly IDictionary<string, IEnumerable<ITerminalValueProvider>> _terminalValueProviders;

        public CustomFieldsGenerator(IEnumerable<ITerminalValueProvider> terminalValueProviders)
        {
            _terminalValueProviders =
                terminalValueProviders
                    .GroupBy(x => x.TargetName ?? string.Empty)
                    .ToDictionary(x => x.Key, x => x.ToArray().AsEnumerable());

            var genericTerminals =
                _terminalValueProviders[string.Empty].SelectMany(x => x.HandledTokens).ToArray();
        }

        public bool CanGenerateValue(string targetKey, IEnumerable<Token> tokens, out IEnumerable<Token> unknownTokens)
        {
            var providers = GetProviders(targetKey);
            unknownTokens =
                tokens.Where(token => !token.IsLiteral && providers.SingleOrDefault(x => x.CanHandle(token.Value)) == null).ToArray();
            return unknownTokens.Any();
        }

        public string BuildSignEditableValueAsync(
            CustomFieldDefinition customFieldDefinition,
            IEnumerable<Token> tokens,
            IStorable instance,
            string customFieldValue)
        {
            var context = new CustomFieldGenerationContext(tokens);
            var providers = GetProviders(customFieldDefinition, tokens);

            foreach (var provider in providers.OrderBy(x => x.Value.ExecutionOrder))
            {
                var descriptor = new CustomFieldGenerationDescriptor
                {
                    Terminal = provider.Key.Value,
                    Token = provider.Key,
                    Instance = instance,
                    CustomFieldDefinition = customFieldDefinition,
                    PartialResult = context.GetCurrentValue(),
                    CustomFieldValue = customFieldValue
                };

                var signPart = provider.Value.GetEditionValue(descriptor);
                context[provider.Key] = signPart;
            }

            return context.GetCurrentValue();
        }

        public IEnumerable<Token> ValidateSignValue(
            CustomFieldDefinition customFieldDefinition,
            IEnumerable<Token> tokens,
            string customFieldValue)
        {
            var providers = GetProviders(customFieldDefinition, tokens);
            var invalidTokens = new List<Token>();
            foreach (var provider in providers.OrderBy(x => x.Value.ExecutionOrder))
            {
                var descriptor = new CustomFieldGenerationDescriptor
                {
                    Terminal = provider.Key.Value,
                    Token = provider.Key,
                    CustomFieldDefinition = customFieldDefinition,
                    CustomFieldValue = customFieldValue
                };

                var isValid = provider.Value.IsEditionValueValid(descriptor);
                if (!isValid)
                {
                    invalidTokens.Add(provider.Key);
                }
            }

            return invalidTokens;
        }

        public bool IsSignValueFixed(
            CustomFieldDefinition customFieldDefinition,
            IEnumerable<Token> tokens,
            string customFieldValue)
        {
            var providers = GetProviders(customFieldDefinition, tokens);
            foreach (var provider in providers.OrderBy(x => x.Value.ExecutionOrder))
            {
                var descriptor = new CustomFieldGenerationDescriptor
                {
                    Terminal = provider.Key.Value,
                    Token = provider.Key,
                    CustomFieldDefinition = customFieldDefinition,
                    CustomFieldValue = customFieldValue
                };

                var isValid = provider.Value.IsValueFixed(descriptor);
                if (!isValid)
                {
                    return false;
                }
            }

            return true;
        }

        public async Task<string> BuildCustomFieldValueAsync(
            CustomFieldDefinition customFieldDefinitionEntity,
            IStorable instance,
            IEnumerable<Token> tokens,
            string customFieldEditableValue)
        {
            var context = new CustomFieldGenerationContext(tokens);
            var providers = GetProviders(customFieldDefinitionEntity, tokens);

            foreach (var provider in providers.OrderBy(x => x.Value.ExecutionOrder))
            {
                var descriptor = new CustomFieldGenerationDescriptor
                {
                    Terminal = provider.Key.Value,
                    Token = provider.Key,
                    Instance = instance,
                    CustomFieldDefinition = customFieldDefinitionEntity,
                    PartialResult = context.GetCurrentValue(),
                    CustomFieldEditableValue = customFieldEditableValue
                };

                var signPart = await provider.Value.GetValue(descriptor);
                context[provider.Key] = signPart;
            }

            return context.GetCurrentValue();
        }

        public string BuildSignMask(
            CustomFieldDefinition customFieldDefinition,
           IEnumerable<Token> tokens)
        {
            var context = new CustomFieldGenerationContext(tokens);
            var providers = GetProviders(customFieldDefinition, tokens);

            foreach (var provider in providers.OrderBy(x => x.Value.ExecutionOrder))
            {
                var descriptor = new CustomFieldGenerationDescriptor
                {
                    Terminal = provider.Key.Value,
                    Token = provider.Key,
                    CustomFieldDefinition = customFieldDefinition,
                    PartialResult = context.GetCurrentValue()
                };

                var signPart = provider.Value.GetMask(descriptor);
                context[provider.Key] = signPart;
            }

            return context.GetCurrentValue();
        }

        private Dictionary<Token, ITerminalValueProvider> GetProviders(
            CustomFieldDefinition customFieldDefinition,
            IEnumerable<Token> tokens)
        {
            var providers = new Dictionary<Token, ITerminalValueProvider>();

            foreach (var token in tokens)
            {
                if (!token.IsLiteral)
                {
                    var provider =
                        GetProviders(customFieldDefinition.TableKey)
                            .Single(x => x.CanHandle(token.Value));
                    providers.Add(token, provider);
                }
            }

            return providers;
        }

        private IEnumerable<ITerminalValueProvider> GetProviders(string tableName)
        {
            var providers = _terminalValueProviders[string.Empty];

            if (_terminalValueProviders.ContainsKey(tableName))
            {
                providers = providers
                    .Concat(_terminalValueProviders[tableName])
                    .ToArray();
            }

            return providers;
        }
    }
}