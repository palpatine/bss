﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.CommonModule.Logic.CustomFields
{
    [SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")]
    public sealed class SignableEntityServiceExtension<TPersistenceModel, TEntityIndicator, TSignsAssignment>
        : IServiceExtension<TPersistenceModel>
        where TPersistenceModel : TEntityIndicator, ISignableEditModel
        where TEntityIndicator : IStorable
        where TSignsAssignment : VersionedEntity, ICustomFieldAssignmentDescriptor, new()
    {
        private readonly ICustomFieldsGenerator _customFieldsGenerator;
        private readonly ICustomFieldsParser _customFieldsParser;
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly IEntityRepository<TSignsAssignment, ICustomFieldAssignmentDescriptor> _signRepository;
        private readonly IEnumerable<IEntityValidator<ISignableEditModel>> _validators;

        public SignableEntityServiceExtension(
            IEntityRepository<TSignsAssignment,
                ICustomFieldAssignmentDescriptor> signRepository,
            IEnumerable<ICustomFieldsAssignmentValidator> validators,
            ICustomFieldsParser customFieldsParser,
            ICustomFieldsGenerator customFieldsGenerator,
            IDatabaseAccessor databaseAccessor)
        {
            _signRepository = signRepository;
            _validators = validators;
            _customFieldsParser = customFieldsParser;
            _customFieldsGenerator = customFieldsGenerator;
            _databaseAccessor = databaseAccessor;
        }

        public async Task FillModelAsync(TPersistenceModel model)
        {
            var customFields = await GetCustomFields(model);
            var definitions = await GetDefinitions(customFields);
            var sets = customFields.Join(
                definitions,
                x => x.CustomFieldDefinition.Id,
                x => x.Id,
                (x, y) => new
                {
                    CustomField = x,
                    Definition = y
                });

            model.CustomFields = sets.Select(x =>
            {
                var tokens = _customFieldsParser.Parse(x.Definition.Schema);

                var editionValue = x.CustomField.Value == null ? null : _customFieldsGenerator.BuildSignEditableValueAsync(x.Definition, tokens, model, x.CustomField.Value);
                return new CustomFieldAssignmentEditModel
                {
                    CustomFieldAssignment = x.CustomField,
                    CustomFieldDefinition = x.Definition,
                    ValueEnabled = x.CustomField.Value != null,
                    Value = editionValue
                };
            });
        }

        public async Task OnReadyToSaveAsync(TPersistenceModel model)
        {
            var currentCustomFields = await _databaseAccessor.Query<TSignsAssignment>()
                .Where(x => !x.IsDeleted && x.CustomFieldTarget.Id == model.Id)
                .ToListAsync();

            var set = model.CustomFields.FullJoin(
                currentCustomFields,
                x => x.CustomFieldDefinitionMarker.Id,
                x => x.CustomFieldDefinition.Id,
                (x, y) => new { Model = x, Current = y });

            foreach (var element in set)
            {
                var definition = await _databaseAccessor.Query<CustomFieldDefinition>()
                    .SingleAsync(x => x.IsEquivalent(element.Model.CustomFieldDefinitionMarker));

                if (element.Model != null && element.Current != null)
                {
                    if (definition.IsEditable)
                    {
                        var value = await GetValue(definition, model, element.Model);
                        element.Current.Value = value;
                    }

                    element.Current.CustomFieldDefinition = element.Model.CustomFieldDefinitionMarker;
                    element.Current.CustomFieldTarget = model;

                    await _signRepository.WriteAsync(element.Current);
                }
                else if (element.Model != null)
                {
                    var value = await GetValue(definition, model, element.Model);
                    var entity = new TSignsAssignment
                    {
                        Value = value,
                        CustomFieldDefinition = element.Model.CustomFieldDefinitionMarker,
                        CustomFieldTarget = model
                    };

                    await _signRepository.WriteAsync(entity);
                }
                else if (element.Current != null)
                {
                    await _signRepository.DeleteAsync(element.Current);
                }
            }
        }

        public async Task<IEnumerable<ValidationError>> ValidateAsync(TPersistenceModel model)
        {
            return (await _validators.SelectManyAsync(x => x.ValidateModelAsync(model))).ToArray();
        }

        private async Task<IEnumerable<ICustomFieldAssignmentDescriptor>> GetCustomFields(TPersistenceModel model)
        {
            return await _databaseAccessor.Query<TSignsAssignment>()
                .Where(x => !x.IsDeleted && x.CustomFieldTarget.Id == model.Id)
                .ToListAsync();
        }

        private async Task<IEnumerable<CustomFieldDefinition>> GetDefinitions(IEnumerable<ICustomFieldAssignmentDescriptor> customFields)
        {
            var definitionsIds = customFields.Select(z => z.CustomFieldDefinition.Id);
            return await _databaseAccessor.Query<CustomFieldDefinition>()
                .Where(x => !x.IsDeleted && definitionsIds.Contains(x.Id))
                .ToListAsync();
        }

        private async Task<string> GetValue(CustomFieldDefinition definition, TPersistenceModel model, CustomFieldAssignmentEditModel customFieldModel)
        {
            if (definition.IsNullable && !customFieldModel.ValueEnabled)
            {
                return null;
            }

            var tokens = _customFieldsParser.Parse(definition.Schema);

            var value = await _customFieldsGenerator.BuildCustomFieldValueAsync(definition, model, tokens, customFieldModel.Value);
            return value;
        }
    }
}