﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;

namespace SolvesIt.BSSystem.CommonModule.Logic.CustomFields
{
    internal class CustomFieldsParser : ICustomFieldsParser
    {
        private static readonly Regex SchemaParser = new Regex(@"((?<literal>[^{}]+)?(?<terminal>{[^{}]+?})?)*");
        public IEnumerable<Token> Parse(string schema)
        {
            IEnumerable<Token> tokens;
            var errors = Validate(schema, out tokens);
            if (errors != null && errors.Any())
            {
                throw new InvalidOperationException(string.Join(", ", errors));
            }

            return tokens;
        }

        public IEnumerable<string> Validate(string schema, out IEnumerable<Token> tokens)
        {
            var errors = new List<string>();

            var tokenMatches = SchemaParser.Matches(schema);
            var matches = tokenMatches.Cast<Match>()
                .Where(x => x.Success);

            var tokensSet = new List<Token>();

            foreach (var match in matches)
            {
                var literal = match.Groups["literal"];
                var terminal = match.Groups["terminal"];
                if (literal.Success)
                {
                    tokensSet.AddRange(literal.Captures.Cast<Capture>()
                        .Select(capture => new Token
                        {
                            IndexInSchema = capture.Index,
                            IsLiteral = true,
                            Length = capture.Length,
                            Value = capture.Value
                        }));
                }

                if (terminal.Success)
                {
                    tokensSet.AddRange(match.Groups["terminal"].Captures.Cast<Capture>()
                        .Select(capture => new Token
                        {
                            IndexInSchema = capture.Index,
                            IsLiteral = false,
                            Length = capture.Length,
                            LengthOfValue = capture.Length - 2,
                            Value = capture.Value
                        }));
                }
            }

            tokens = tokensSet
               .OrderBy(x => x.IndexInSchema)
               .ToList();

            var position = 0;
            var indexInValue = 0;
            foreach (var token in tokens)
            {
                if (token.IndexInSchema != position)
                {
                    errors.Add((position + 1).ToString(CultureInfo.InvariantCulture));
                    return errors;
                }

                token.IndexInValue = indexInValue;
                if (token.IsLiteral)
                {
                    indexInValue += token.Length;
                }
                else
                {
                    indexInValue += token.Length - 2;
                }

                position += token.Length;
            }

            return errors;
        }
    }
}