using System;
using System.Collections.Generic;
using System.Linq;
using SolvesIt.BSSystem.CommonModule.Abstraction.CustomFields;

namespace SolvesIt.BSSystem.CommonModule.Logic.CustomFields
{
    internal class CustomFieldGenerationContext
    {
        private readonly Dictionary<Token, string> _tokens;

        public CustomFieldGenerationContext(IEnumerable<Token> tokens)
        {
            _tokens = tokens.ToDictionary(x => x, x => x.Value);
        }

        public string this[Token token]
        {
            get
            {
                return _tokens[token];
            }
            set
            {
                if (!_tokens.ContainsKey(token))
                {
                    throw new ArgumentException("invalid token", "token");
                }

                _tokens[token] = value;
            }
        }

        public string GetCurrentValue()
        {
            return string.Concat(_tokens.OrderBy(x => x.Key.IndexInSchema).Select(x => x.Value));
        }
    }
}