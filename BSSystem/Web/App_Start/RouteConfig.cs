﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SolvesIt.BSSystem.Web
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            var mvcParameters = new
            {
                controller = "Home",
                action = "Index",
                id = UrlParameter.Optional
            };
            routes.MapRoute(
                "Mvc",
                "{controller}/{action}/{id}",
                mvcParameters);

            routes.MapRoute(
                "RelationPanel",
                "{controller}/RelationDetailPanel/{entities}/{elements}",
                new { action = "RelationDetailPanel" });
        }
    }
}