﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc.Unity;
using SolvesIt.BSSystem.Web.Configuration;
using SolvesIt.BSSystem.Web.Controllers;

namespace SolvesIt.BSSystem.Web
{
    public static class EnvironmentBootstrapper
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Container is a global object that is released when app domain is released.")]
        public static void Bootstrap()
        {
            var container = new UnityContainer();
            var registrar = new UnityRegistrar(container, () => new PerResolveLifetimeManager());
            var moduleDlls = DetectModules();

            var locator = new UnityServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => locator);
            container.RegisterInstance<IServiceLocator>(locator);

            var bootstrapper = new SystemBootstrapper(registrar, moduleDlls);
            registrar.EnableEnumerableResolution();
            bootstrapper.Bootstrap();

            var services = GlobalConfiguration.Configuration.Services;
            var controllerTypes = services.GetHttpControllerTypeResolver()
                .GetControllerTypes(services.GetAssembliesResolver());

            foreach (var controllerType in controllerTypes)
            {
                container.RegisterType(controllerType);
            }

            ControllerBuilder.Current.SetControllerFactory(new ControllerFactory(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }

        private static IEnumerable<Assembly> DetectModules()
        {
#if AZURE
            var moduleDlls = new Assembly[] {
                Assembly.GetExecutingAssembly(),
                Assembly.GetAssembly(typeof(SolvesIt.BSSystem.Core.Logic.Environment.Bootstrapper)),
                Assembly.GetAssembly(typeof(SolvesIt.BSSystem.CommonModule.Logic.Environment.Bootstrapper)),
                Assembly.GetAssembly(typeof(SolvesIt.BSSystem.WttModule.Logic.Environment.Bootstrapper)),
                Assembly.GetAssembly(typeof(Qdarc.Modules.Common.Bootstrap.Bootstrapper))
            };
#else

            var modulesConfiguration = (ModulesConfigurationSection)ConfigurationManager.GetSection(
                "modulesConfiguration");
            var binFolder = modulesConfiguration.ModulesLocation;
            var moduleDlls = Directory.GetFiles(binFolder)
                .Where(x =>
                    Equals(Path.GetExtension(x), ".dll")
                    &&
                    (Path.GetFileNameWithoutExtension(x).StartsWith("Qdarc", StringComparison.Ordinal) ||
                     Path.GetFileNameWithoutExtension(x).StartsWith("SolvesIt", StringComparison.Ordinal)))
                .Select(Assembly.LoadFrom)
                .ToArray();
#endif
            return moduleDlls;
        }
    }
}