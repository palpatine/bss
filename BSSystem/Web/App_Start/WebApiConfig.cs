﻿using System.Web.Http;

namespace SolvesIt.BSSystem.Web
{
    public static class WebApiConfig
    {
        public static string UrlPrefixRelative => "~/api";

        public static void Register(HttpRouteCollection routes)
        {
            routes.MapHttpRoute(
                name: "AssignmentsApi",
                routeTemplate: "api/Assignments/{action}/{master}/{id}/{related}",
                defaults: new { controller = "Assignments" });

            routes.MapHttpRoute(
               name: "AssignmentsListApi",
               routeTemplate: "api/Assignments/{action}/{master}/{related}",
               defaults: new { controller = "Assignments" });

            routes.MapHttpRoute(
                name: "CollectionApi",
                routeTemplate: "api/List/{controller}/{action}",
                defaults: new { });

            var defaultApiParameters = new
            {
                id = RouteParameter.Optional,
                action = RouteParameter.Optional,
                actionid = RouteParameter.Optional,
                subaction = RouteParameter.Optional,
                subactionid = RouteParameter.Optional
            };
            routes.MapHttpRoute(
                 name: "DefaultApi",
                 routeTemplate: "api/{controller}/{id}/{action}/{actionid}/{subaction}/{subactionid}",
                 defaults: defaultApiParameters);
        }
    }
}