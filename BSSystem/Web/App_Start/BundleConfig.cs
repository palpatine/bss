﻿using System.Web.Optimization;
using AngularTemplates.Bundling;
using AngularTemplates.Compile;

namespace SolvesIt.BSSystem.Web
{
    public static class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/lessjs").Include(
                "~/Scripts/Lib/lessjs/options.js",
                "~/Scripts/Lib/lessjs/less.js"));

            var bundle = new ScriptBundle("~/bundles/Lib").Include(
                "~/Scripts/Lib/jquery-1.10.2.js",
                "~/Scripts/Lib/jquery.cookie.js",
                ////
                "~/Scripts/Lib/Angular/*.js",
                ////
                "~/Scripts/Lib/MomentJs/moment.js",
                ////
                "~/Scripts/Lib/MomentJs/momentTimezone.js",
                "~/Scripts/Lib/Bootstrap/*.js",
                "~/Scripts/Lib/jquery.dataTables.js",
                "~/Scripts/Lib/Bootstrap/plugins/dataTables/dataTables.bootstrap.js",
                "~/Scripts/Lib/Bootstrap/plugins/flot/*.js",
                "~/Scripts/Lib/Bootstrap/plugins/metisMenu/*.js",
                "~/Scripts/Lib/Bootstrap/plugins/morris/*.js",
                "~/Scripts/Lib/respond.js",
                "~/Scripts/Lib/underscore-1.8.3.js",
                "~/Scripts/Lib/underscore.string.js",
                "~/Scripts/Lib/underscore.inflections.js",
                "~/Scripts/Lib/jquery-chosen.js",
                "~/Scripts/Lib/Chart.js",
                ////
                "~/Scripts/Lib/Angular/Modules/*.js",
                "~/Scripts/Lib/Angular/Modules/adapt/*.js",
                "~/Scripts/Lib/Angular/Modules/checkbox/*.js",
                "~/Scripts/Lib/Angular/Modules/Calendar/*.js",
                "~/Scripts/Lib/Angular/Modules/http-auth/*.js",
                "~/Scripts/Lib/angular-bootstap-colorpicker/*.js");
            bundles.Add(bundle);

            bundles.Add(new Bundle("~/Content/BootstrapLess")
                //// Core variables and mixins
               .Include(
                "~/Content/Lib/Bootstrap/variables.less",
                //// Utilities
                "~/Content/Lib/Bootstrap/mixins/hide-text.less",
                "~/Content/Lib/Bootstrap/mixins/opacity.less",
                "~/Content/Lib/Bootstrap/mixins/image.less",
                "~/Content/Lib/Bootstrap/mixins/labels.less",
                "~/Content/Lib/Bootstrap/mixins/reset-filter.less",
                "~/Content/Lib/Bootstrap/mixins/resize.less",
                "~/Content/Lib/Bootstrap/mixins/responsive-visibility.less",
                "~/Content/Lib/Bootstrap/mixins/size.less",
                "~/Content/Lib/Bootstrap/mixins/tab-focus.less",
                "~/Content/Lib/Bootstrap/mixins/text-emphasis.less",
                "~/Content/Lib/Bootstrap/mixins/text-overflow.less",
                "~/Content/Lib/Bootstrap/mixins/vendor-prefixes.less",
                //// Components
                "~/Content/Lib/Bootstrap/mixins/alerts.less",
                "~/Content/Lib/Bootstrap/mixins/buttons.less",
                "~/Content/Lib/Bootstrap/mixins/panels.less",
                "~/Content/Lib/Bootstrap/mixins/pagination.less",
                "~/Content/Lib/Bootstrap/mixins/list-group.less",
                "~/Content/Lib/Bootstrap/mixins/nav-divider.less",
                "~/Content/Lib/Bootstrap/mixins/forms.less",
                "~/Content/Lib/Bootstrap/mixins/progress-bar.less",
                "~/Content/Lib/Bootstrap/mixins/table-row.less",
                //// Skins
                "~/Content/Lib/Bootstrap/mixins/background-variant.less",
                "~/Content/Lib/Bootstrap/mixins/border-radius.less",
                "~/Content/Lib/Bootstrap/mixins/gradients.less",
                //// Layout
                "~/Content/Lib/Bootstrap/mixins/clearfix.less",
                "~/Content/Lib/Bootstrap/mixins/center-block.less",
                "~/Content/Lib/Bootstrap/mixins/nav-vertical-align.less",
                "~/Content/Lib/Bootstrap/mixins/grid-framework.less",
                "~/Content/Lib/Bootstrap/mixins/grid.less",
                //// Reset and dependencies
                "~/Content/Lib/Bootstrap/normalize.less",
                "~/Content/Lib/Bootstrap/print.less",
                "~/Content/Lib/Bootstrap/glyphicons.less",
                "~/Content/Lib/Bootstrap/bootswatch.less",
                //// Core CSS
                "~/Content/Lib/Bootstrap/scaffolding.less",
                "~/Content/Lib/Bootstrap/type.less",
                "~/Content/Lib/Bootstrap/code.less",
                "~/Content/Lib/Bootstrap/grid.less",
                "~/Content/Lib/Bootstrap/tables.less",
                "~/Content/Lib/Bootstrap/forms.less",
                "~/Content/Lib/Bootstrap/buttons.less",
                //// Components
                "~/Content/Lib/Bootstrap/component-animations.less",
                "~/Content/Lib/Bootstrap/dropdowns.less",
                "~/Content/Lib/Bootstrap/button-groups.less",
                "~/Content/Lib/Bootstrap/input-groups.less",
                "~/Content/Lib/Bootstrap/navs.less",
                "~/Content/Lib/Bootstrap/navbar.less",
                "~/Content/Lib/Bootstrap/breadcrumbs.less",
                "~/Content/Lib/Bootstrap/pagination.less",
                "~/Content/Lib/Bootstrap/pager.less",
                "~/Content/Lib/Bootstrap/labels.less",
                "~/Content/Lib/Bootstrap/badges.less",
                "~/Content/Lib/Bootstrap/jumbotron.less",
                "~/Content/Lib/Bootstrap/thumbnails.less",
                "~/Content/Lib/Bootstrap/alerts.less",
                "~/Content/Lib/Bootstrap/progress-bars.less",
                "~/Content/Lib/Bootstrap/media.less",
                "~/Content/Lib/Bootstrap/list-group.less",
                "~/Content/Lib/Bootstrap/panels.less",
                "~/Content/Lib/Bootstrap/responsive-embed.less",
                "~/Content/Lib/Bootstrap/wells.less",
                "~/Content/Lib/Bootstrap/close.less",
                //// Components w/ JavaScript
                "~/Content/Lib/Bootstrap/modals.less",
                "~/Content/Lib/Bootstrap/tooltip.less",
                "~/Content/Lib/Bootstrap/popovers.less",
                "~/Content/Lib/Bootstrap/carousel.less",
                //// Utility classes
                "~/Content/Lib/Bootstrap/utilities.less",
                "~/Content/Lib/Bootstrap/responsive-utilities.less",
                ////Angular
                "~/Content/Lib/Bootstrap/_angularIntegration.less",
                "~/Content/Lib/angular-bootstap-colorpicker/colorpicker.less"));

            bundles.Add(new Bundle("~/Content/adaptLess")
                //// Core variables and mixins
                .Include(
                "~/Content/Lib/Bootstrap/variables.less",
                "~/Content/Lib/adapt/mixins.less",
                "~/Content/Lib/adapt/tablelite.less",
                "~/Content/Lib/adapt/tableajax.less",
                "~/Content/Lib/adapt/loadingindicator.less",
                "~/Content/Lib/adapt/infinitedropdown.less"));

            bundles.Add(new Bundle("~/Content/sb-admin-2Less")
                //// Core variables and mixins
                .Include(
                "~/Content/Lib/Bootstrap/variables.less",
                "~/Content/Lib/Template/sb-admin-2.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/variables.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/mixins.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/path.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/core.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/larger.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/fixed-width.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/list.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/bordered-pulled.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/spinning.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/rotated-flipped.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/stacked.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/icons.less"));

            bundles.Add(new Bundle("~/Content/SiteLess").Include(
                "~/Content/Lib/Template/font-awesome-4.3.0/less/variables.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/mixins.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/path.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/core.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/larger.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/fixed-width.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/list.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/bordered-pulled.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/spinning.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/rotated-flipped.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/stacked.less",
                "~/Content/Lib/Template/font-awesome-4.3.0/less/icons.less",
                "~/Content/Lib/Bootstrap/variables.less",
                 "~/Content/Lib/Bootstrap/glyphicons.less",
                "~/Content/Site/Icons.less",
                "~/Content/Site/Site.less",
                "~/Content/Site/Widgets/BssCalendar.less"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/Lib/Bootstrap/plugins/*.css",
                "~/Content/Lib/Bootstrap/plugins/metisMenu/*.css"));

            var bssScripts = new ScriptBundle("~/bundles/Bss")
                .Include("~/Scripts/System/Bss/Bootstrap.js")
                .IncludeDirectory("~/Scripts/System/Bss/Core/Integration", "*.js", true)
                .IncludeDirectory("~/Scripts/System/Widgets", "*.js", true)
                .Include("~/Scripts/System/LogOn/LogonController.js")
                .IncludeDirectory("~/Scripts/System/Bss/Core", "*.js", true)
                .IncludeDirectory("~/Scripts/System/Bss", "*.js", true)
                .IncludeDirectory("~/Areas/Common/Scripts", "*.js", true)
                .IncludeDirectory("~/Areas/Wtt/Scripts", "*.js", true);
            bundles.Add(bssScripts);

            var logon = new ScriptBundle("~/bundles/logon")
            .Include("~/Scripts/System/Bss/Bootstrap.js")
                .Include("~/Scripts/System/LogOn/LogonApplication.js")
                .Include("~/Scripts/System/LogOn/LogonController.js");
            bundles.Add(logon);

            var options = new TemplateCompilerOptions
            {
                ModuleName = "bss",
                Prefix = "~/",
            };
            var angularTemplates = new TemplateBundle("~/Content/tpls", options)
                .IncludeDirectory("~/Content/Templates", "*.html", true);
            bundles.Add(angularTemplates);
        }
    }
}