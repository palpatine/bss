namespace SolvesIt.BSSystem.Web.Authorization
{
    public sealed class AuthenticationResult
    {
        public bool IsSessionValid { get; set; }

        public bool WhereSessionDetailsProvided { get; set; }
    }
}