﻿using System;

namespace SolvesIt.BSSystem.Web.Authorization
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class DefaultPermissionContainerAttribute : Attribute
    {
        public DefaultPermissionContainerAttribute(string permissionContainerKey)
        {
            PermissionContainerKey = permissionContainerKey;
        }

        public string PermissionContainerKey { get; private set; }
    }
}