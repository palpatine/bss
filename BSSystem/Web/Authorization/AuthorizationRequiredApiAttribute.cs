﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Microsoft.Practices.ServiceLocation;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Web.Controllers.Api;

namespace SolvesIt.BSSystem.Web.Authorization
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class AuthorizationRequiredApiAttribute : Attribute, IAuthenticationFilter
    {
        private bool _shouldChallengeRequest;

        public bool AllowMultiple => false;

        public async Task AuthenticateAsync(
            HttpAuthenticationContext context,
            CancellationToken cancellationToken)
        {
            var validator = ServiceLocator.Current.GetInstance<IAuthenticationValidator>();
            var result = await validator.TryAuthenticateAsync(context);
            if (!result.IsSessionValid)
            {
                _shouldChallengeRequest = !result.WhereSessionDetailsProvided;
                if (HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session.Abandon();
                }

                context.ErrorResult = new AuthenticationFailureResult(context.Request);
                return;
            }

            var apiController = context.ActionContext.ControllerContext.Controller as BaseApiController;
            if (apiController != null)
            {
                apiController.ResolveSession(context.Principal);
            }

            if (!await HandleRightsModel(context))
            {
                context.ErrorResult = new ForbiddenResult(context.Request);
            }
        }

        public Task ChallengeAsync(
            HttpAuthenticationChallengeContext context,
            CancellationToken cancellationToken)
        {
            if (_shouldChallengeRequest)
            {
                var challenge = new AuthenticationHeaderValue("Basic");
                context.Result = new AddChallengeOnUnauthorizedResult(challenge, context.Result);
            }

            return Task.FromResult(0);
        }

        private static int? DetermineIdentifierValue(HttpAuthenticationContext filterContext)
        {
            var actionDescriptor = (ReflectedHttpActionDescriptor)filterContext.ActionContext.ActionDescriptor;
            var parameters = actionDescriptor.MethodInfo.GetParameters().OrderBy(p => p.ParameterType.IsClass);

            int? result = null;
            foreach (var parameter in parameters)
            {
                if (parameter.Name.Equals("Id", StringComparison.OrdinalIgnoreCase))
                {
                    if (parameter.ParameterType != typeof(int))
                    {
                        throw new InvalidOperationException("Parameter with name equals Id must be int.");
                    }

                    var name = parameter.Name;
                    result = int.Parse(filterContext.ActionContext.ControllerContext.RouteData.Values[name].ToString(), CultureInfo.InvariantCulture);
                    break;
                }

                if (parameter.ParameterType.IsClass && parameter.ParameterType != typeof(string))
                {
                    var properties = parameter.ParameterType.GetProperties();
                    var property = properties.SingleOrDefault(x => x.Name.Equals("Id", StringComparison.OrdinalIgnoreCase));

                    if (property != null)
                    {
                        if (property.PropertyType != typeof(int))
                        {
                            throw new InvalidOperationException("Class member with name equals Id must be int.");
                        }

                        result = int.Parse(filterContext.ActionContext.ControllerContext.RouteData.Values[property.Name].ToString(), CultureInfo.InvariantCulture);
                        break;
                    }
                }
            }

            return result;
        }

        private async Task<bool> HandleRightsModel(HttpAuthenticationContext filterContext)
        {
            var actionDescriptor = (ReflectedHttpActionDescriptor)filterContext.ActionContext.ActionDescriptor;
            var permissionHelper = ServiceLocator.Current.GetInstance<IPermissionHelper>();
            var permissionPath = permissionHelper.GetPermissionPath(
                filterContext.ActionContext.ControllerContext.Controller.GetType(),
                actionDescriptor.MethodInfo,
                filterContext.ActionContext.ControllerContext.RouteData.Values);

            if (permissionPath == null)
            {
                return true;
            }

            var permissionService = ServiceLocator.Current.GetInstance<IPermissionService>();
            var permission = (await permissionService.GetCurrentUserEffectivePermissions())
                .SingleOrDefault(p => p.Path == permissionPath);

            if (permission == null)
            {
                return false;
            }

            if (permission.IsGlobal)
            {
                return true;
            }

            var contextId = DetermineIdentifierValue(filterContext);
            if (contextId == null)
            {
                return true;
            }

            var permissionAssingments = (await permissionService.GetUserEffectivePermissionAssignments())
                .Single(pc => pc.PermissionId == permission.Id && pc.RelatedTableKey == permissionPath.Split('|')[0]);

            var permissionsContextValues =
                await permissionService.GetCurrentUserEffectivePermissionsContextValues(permissionAssingments);

            return permissionsContextValues.Any(id => id == contextId.Value);
        }
    }
}