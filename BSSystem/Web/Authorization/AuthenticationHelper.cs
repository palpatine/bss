﻿using System;
using System.Globalization;
using System.Web;
using System.Web.Security;
using SolvesIt.BSSystem.Core.Abstraction.Environment;

namespace SolvesIt.BSSystem.Web.Authorization
{
    internal sealed class AuthenticationHelper : IAuthenticationHelper
    {
        private readonly IConfigurationProvider _configuration;

        public AuthenticationHelper(IConfigurationProvider configuration)
        {
            _configuration = configuration;
        }

        public string AuthorizationCookieName => string.Format(
            CultureInfo.InvariantCulture,
            "{0}{1}",
            FormsAuthentication.FormsCookieName,
            HttpContext.Current.Request.Url.Port);

        public void ClearAuthentication()
        {
            RemoveCookie(AuthorizationCookieName);
        }

        public FormsAuthenticationTicket SetAuthenticationCookie(
            int userId,
            string userName,
            int organizationId,
            Guid sessionIdentifier)
        {
            var ticket = new FormsAuthenticationTicket(
                1,
                userName,
                DateTime.Now,
                DateTime.Now.Add(_configuration.SessionTimeout),
                false,
                string.Join("|", userId.ToString(CultureInfo.InvariantCulture), organizationId.ToString(CultureInfo.InvariantCulture), sessionIdentifier.ToString()));

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(AuthorizationCookieName, encryptedTicket)
            {
                HttpOnly = true,
                Secure = true
            };

            HttpContext.Current.Response.Cookies.Add(cookie);

            return ticket;
        }

        private static void RemoveCookie(string cookieName)
        {
            if (HttpContext.Current.Request.Cookies[cookieName] != null)
            {
                HttpContext.Current.Request.Cookies.Remove(cookieName);
                var cookie = new HttpCookie(cookieName) { Expires = DateTime.Now.AddDays(-1d) };
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
    }
}