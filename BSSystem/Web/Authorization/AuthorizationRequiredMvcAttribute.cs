using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Practices.ServiceLocation;

namespace SolvesIt.BSSystem.Web.Authorization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class AuthorizationRequiredMvcAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var validator = ServiceLocator.Current.GetInstance<IAuthenticationValidator>();
            var result = validator.TryAuthenticate(httpContext);
            if (!result.IsSessionValid)
            {
                if (HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session.Abandon();
                    return false;
                }
            }

            return base.AuthorizeCore(httpContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult("Mvc", new RouteValueDictionary { { "controller", "Authentication" }, { "action", "LogOn" } });
        }
    }
}