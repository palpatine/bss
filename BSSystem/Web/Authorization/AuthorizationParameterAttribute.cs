﻿using System;

namespace SolvesIt.BSSystem.Web.Authorization
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter)]
    public sealed class AuthorizationParameterAttribute : Attribute
    {
    }
}