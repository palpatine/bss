﻿using System;
using System.Web;
using System.Web.Security;

namespace SolvesIt.BSSystem.Web.Authorization
{
    public interface IAuthenticationHelper
    {
        string AuthorizationCookieName { get; }

        void ClearAuthentication();

        FormsAuthenticationTicket SetAuthenticationCookie(
            int userId,
            string userName,
            int organizationId,
            Guid sessionIdentifier);
    }
}