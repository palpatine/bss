﻿using System;
using System.Net;
using System.Security.Principal;
using System.Web.Http;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.Web.Authorization
{
    public sealed class SessionDataProvider : ISessionDataProvider
    {
        private readonly Func<BssIdentity> _providerFactory;

        private BssIdentity _provider;

        public SessionDataProvider(Func<BssIdentity> identity)
        {
            _providerFactory = identity;
        }

        public IUser CurrentUser => Provider.CurrentUser;

        public IOrganization Scope => Provider.CurrentOrganization;

        public ISession Session => Provider.Session;

        private BssIdentity Provider
        {
            get
            {
                if (_provider == null)
                {
                    try
                    {
                        _provider = _providerFactory();
                    }
                    catch (Microsoft.Practices.Unity.ResolutionFailedException)
                    {
                        throw new HttpResponseException(HttpStatusCode.Unauthorized);
                    }
                }

                return _provider;
            }
        }

        public void Resolve(IPrincipal principal)
        {
            _provider = (BssIdentity)principal;
        }
    }
}