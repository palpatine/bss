using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Authorization
{
    public interface IPermissionHelper
    {
        string GetPermissionPath<TValue>(Expression<Func<TValue>> methodReference);

        string GetPermissionPath(
            Type controllerType,
            MethodInfo methodInfo,
            IDictionary<string, object> routeValues);

        Task<AccessibleItems> GetAccessibleItemsAsync<TValue>(Expression<Func<TValue>> methodReference);
    }
}