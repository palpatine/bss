﻿using System;
using System.Net;
using System.Web.Helpers;
using System.Web.Mvc;

// ReSharper disable CheckNamespace
namespace SolvesIt.BSSystem.Web

// ReSharper restore CheckNamespace
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class ValidateAntiForgeryTokenAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var request = filterContext.HttpContext.Request;

            // Only validate POSTs
            if (request.HttpMethod == WebRequestMethods.Http.Post)
            {
                // Ajax POSTs and normal form posts have to be treated differently when it comes to
                // validating the AntiForgeryToken
                if (request.IsAjaxRequest())
                {
                    var antiForgeryCookie = request.Cookies[AntiForgeryConfig.CookieName];

                    var cookieValue = antiForgeryCookie != null
                        ? antiForgeryCookie.Value
                        : null;

                    AntiForgery.Validate(cookieValue, request.Headers["__RequestVerificationToken"]);
                }
                else
                {
                    new System.Web.Mvc.ValidateAntiForgeryTokenAttribute()
                        .OnAuthorization(filterContext);
                }
            }
        }
    }
}