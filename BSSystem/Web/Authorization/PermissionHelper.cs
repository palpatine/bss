using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Logic.Services;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Authorization
{
    public sealed class PermissionHelper : IPermissionHelper
    {
        private readonly PermissionService _permissionService;

        public PermissionHelper(PermissionService permissionService)
        {
            _permissionService = permissionService;
        }

        public async Task<AccessibleItems> GetAccessibleItemsAsync<TValue>(Expression<Func<TValue>> methodReference)
        {
            var permissionPath = GetPermissionPath(methodReference);
            var permission = (await _permissionService.GetCurrentUserEffectivePermissions())
                .SingleOrDefault(x => x.Path == permissionPath);

            var accessibleItems = Enumerable.Empty<int>();

            if (permission != null && !permission.IsGlobal)
            {
                var pa = (await _permissionService.GetUserEffectivePermissionAssignments())
                    .SingleOrDefault(x => x.PermissionId == permission.Id);
                accessibleItems = await _permissionService.GetCurrentUserEffectivePermissionsContextValues(pa);
            }

            return new AccessibleItems()
            {
                GetAll = permission != null && permission.IsGlobal,
                Values = accessibleItems
            };
        }

        public string GetPermissionPath<TValue>(Expression<Func<TValue>> methodReference)
        {
            return GetPermissionPath(methodReference.GetDeclaringType(), methodReference.GetMethod(), null);
        }

        public string GetPermissionPath(
            Type controllerType,
            MethodInfo methodInfo,
            IDictionary<string, object> routeValues)
        {
            if (methodInfo.IsVirtual)
            {
                methodInfo = controllerType.GetMethods()
                      .SingleOrDefault(x => x.Name == methodInfo.Name && x.GetParameters().Select(y => y.ParameterType).SequenceEqual(methodInfo.GetParameters().Select(y => y.ParameterType))) ?? methodInfo;
            }

            var permissionAttribute = methodInfo.GetCustomAttributes<RequiredPermissionAttribute>(true).SingleOrDefault();
            if (permissionAttribute == null)
            {
                return null;
            }

            var container = permissionAttribute.PermissionContainerKey;
            if (string.IsNullOrEmpty(container) && !string.IsNullOrEmpty(permissionAttribute.PermissionContainerKeyRequestParameterName))
            {
                container = (string)routeValues[permissionAttribute.PermissionContainerKeyRequestParameterName];
            }

            if (string.IsNullOrEmpty(container))
            {
                var defaultContainerAttribute = controllerType.GetCustomAttribute<DefaultPermissionContainerAttribute>();
                var defaultContainer = defaultContainerAttribute == null ? null : defaultContainerAttribute.PermissionContainerKey;
                container = defaultContainer;
            }

            if (container == null)
            {
                return "UnknownPermission|Access|Denied";
            }

            if (container.StartsWith("|", StringComparison.Ordinal))
            {
                container = container.Substring(1);
            }

            var permissionKey = string.Empty;
            if (!string.IsNullOrEmpty(permissionAttribute.PermissionKeyRequestParameterName))
            {
                permissionKey = "|" + (string)routeValues[permissionAttribute.PermissionKeyRequestParameterName];
            }

            var permissionPath = string.Format(
                CultureInfo.InvariantCulture,
                @"{0}{1}{2}",
                container,
                permissionAttribute.PermissionPartialPath,
                permissionKey);
            return permissionPath;
        }
    }
}