﻿using System;
using System.Globalization;
using System.Security.Principal;
using System.Web.Security;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;

namespace SolvesIt.BSSystem.Web.Authorization
{
    [Serializable]
    public sealed class BssIdentity : FormsIdentity, IPrincipal
    {
        public BssIdentity(FormsAuthenticationTicket ticket)
            : base(ticket)
        {
            var splited = ticket.UserData.Split('|');
            CurrentUserId = int.Parse(splited[0], CultureInfo.InvariantCulture);
            CurrentOrganizationId = int.Parse(splited[1], CultureInfo.InvariantCulture);
            SessionGuid = Guid.Parse(splited[2]);
        }

        public int CurrentOrganizationId { get; }

        public IUser CurrentUser => new UserMarker(CurrentUserId);

        public IOrganization CurrentOrganization => new OrganizationMarker(CurrentOrganizationId);

        public int CurrentUserId { get; }

        public ISession Session => new SessionMarker(SessionId);

        public Guid SessionGuid { get; private set; }

        public int SessionId { get; set; }

        public IIdentity Identity => this;

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}