﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Security;
using Qdarc.Modules.Common.Ioc;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Web.Controllers;

namespace SolvesIt.BSSystem.Web.Authorization
{
    public sealed class AuthenticationValidator : IAuthenticationValidator
    {
        private readonly ICache _cache;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly IAuthenticationHelper _helper;
        private readonly IRegistrar _registrar;
        private readonly ISessionService _sessionService;
        private readonly Func<IUserService> _userServiceFactory;

        public AuthenticationValidator(
            IAuthenticationHelper helper,
            ISessionService sessionService,
            IConfigurationProvider configurationProvider,
            ICache cache,
            IRegistrar registrar,
            IDatabaseAccessor databaseAccessor,
            Func<IUserService> userServiceFactory)
        {
            _helper = helper;
            _sessionService = sessionService;
            _configurationProvider = configurationProvider;
            _cache = cache;
            _registrar = registrar;
            _databaseAccessor = databaseAccessor;
            _userServiceFactory = userServiceFactory;
        }

        public AuthenticationResult TryAuthenticate(HttpContextBase httpContext)
        {
            var authCookie = httpContext.Request.Cookies.Get(_helper.AuthorizationCookieName);
            if (authCookie == null)
            {
                return new AuthenticationResult();
            }

            BssIdentity identity;
            if (!VerifyAutorizationValue(authCookie.Value, out identity))
            {
                return new AuthenticationResult { WhereSessionDetailsProvided = true };
            }

            var principal = new GenericPrincipal(identity, new string[] { });
            httpContext.User = principal;
            return new AuthenticationResult { WhereSessionDetailsProvided = true, IsSessionValid = true };
        }

        public async Task<AuthenticationResult> TryAuthenticateAsync(
            HttpAuthenticationContext httpContext)
        {
            BssIdentity identity = null;
            if (httpContext.Request.Headers.Contains("Cookie"))
            {
                var cookeisRaw = httpContext.Request.Headers.GetValues("Cookie").FirstOrDefault();
                if (cookeisRaw == null)
                {
                    return new AuthenticationResult();
                }

                var cookeName = _helper.AuthorizationCookieName;
                var cookeValue = cookeisRaw.Split(';')
                    .Select(x => x.Split('='))
                    .Where(x => x[0].Trim() == cookeName)
                    .Select(x => x[1])
                    .FirstOrDefault();

                if (!VerifyAutorizationValue(cookeValue, out identity))
                {
                    return new AuthenticationResult { WhereSessionDetailsProvided = true };
                }
            }

            if (identity == null && httpContext.Request.Headers.Contains("Authorization"))
            {
                var value = httpContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
                identity = await VerifyAuthorizationBasicAsync(value);
                if (identity == null)
                {
                    return new AuthenticationResult { WhereSessionDetailsProvided = true };
                }
            }
            else if (identity == null)
            {
                return new AuthenticationResult();
            }

            httpContext.Principal = identity;
            return new AuthenticationResult { WhereSessionDetailsProvided = true, IsSessionValid = true };
        }

        private bool TryGetTicket(
            string authCookieValue,
            out FormsAuthenticationTicket ticket)
        {
            if (authCookieValue == null)
            {
                ticket = null;
                return false;
            }

            try
            {
                ticket = FormsAuthentication.Decrypt(authCookieValue);
            }
            catch (Exception)
            {
                ticket = null;
                _helper.ClearAuthentication();
                return false;
            }

            if (ticket == null || ticket.Expired)
            {
                return false;
            }

            return true;
        }

        private async Task<BssIdentity> VerifyAuthorizationBasicAsync(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            value = value.Split(' ')[1].Trim();
            var decodedAuth = Encoding.UTF8.GetString(Convert.FromBase64String(value));
            var splits = decodedAuth.Split(':');
            var loginSplits = splits[0].Split('\\');
            if (loginSplits.Count() != 2)
            {
                return null;
            }

            var domain = loginSplits[0];
            var username = loginSplits[1];
            var password = splits[1];
            var userService = _userServiceFactory();
            var data = await userService.GetUserAndOrganizationByLoginAsync(domain, username, password);

            if (data != null)
            {
                var session = new Session
                {
                    User = data.Item1,
                    Guid = Guid.NewGuid(),
                    RemoteIP = new byte[1],
                    TimeStart = DateTime.UtcNow,
                    TimeEnd = DateTime.UtcNow.Add(_configurationProvider.SessionTimeout)
                };

                await _sessionService.WriteAsync(session);
                var ticket = _helper.SetAuthenticationCookie(data.Item1.Id, username, data.Item2.Id, session.Guid);
                var identity = new BssIdentity(ticket)
                {
                    SessionId = session.Id
                };
                return identity;
            }

            return null;
        }

        private bool VerifyAutorizationValue(
            string authCookieValue,
            out BssIdentity identity)
        {
            FormsAuthenticationTicket ticket;
            if (!TryGetTicket(authCookieValue, out ticket))
            {
                identity = null;
                return false;
            }

            var bssIdentity = new BssIdentity(ticket);
            identity = bssIdentity;
            if (!VerifySession(identity))
            {
                return false;
            }

            _helper.SetAuthenticationCookie(
                identity.CurrentUserId,
                identity.Name,
                identity.CurrentOrganizationId,
                identity.SessionGuid);
            _registrar.RegisterInstancePerHttpRequest(bssIdentity);
            return true;
        }

        private bool VerifySession(BssIdentity identity)
        {
            var sessionData = _cache.GetGlobalCacheValue(
                WebConstants.UserSessionCacheKey + identity.SessionGuid,
                () => _databaseAccessor.Query<Session>().Where(
                    x =>
                        x.User.Id == identity.CurrentUser.Id
                        && x.Guid == identity.SessionGuid
                        && x.TimeEnd > DateTime.UtcNow)
                    .SingleOrDefault(),
                _configurationProvider.SessionTimeout);

            if (sessionData == null)
            {
                return false;
            }

            identity.SessionId = sessionData.Id;
            var sessionTimeout = DateTime.UtcNow.Add(_configurationProvider.SessionTimeout);

            if (sessionData.TimeEnd < sessionTimeout.Subtract(new TimeSpan(0, 0, 0, 20)))
            {
                sessionData.TimeEnd = sessionTimeout;
                _sessionService.Write(sessionData);
            }

            return true;
        }
    }
}