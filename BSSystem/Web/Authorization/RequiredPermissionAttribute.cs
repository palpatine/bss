﻿using System;

namespace SolvesIt.BSSystem.Web.Authorization
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class RequiredPermissionAttribute : Attribute
    {
        public RequiredPermissionAttribute(string permissionPartialPath)
        {
            PermissionPartialPath = permissionPartialPath;
        }

        public string PermissionContainerKey { get; set; }

        public string PermissionPartialPath { get; private set; }

        public string PermissionContainerKeyRequestParameterName { get; set; }

        public string PermissionKeyRequestParameterName { get; set; }
    }
}