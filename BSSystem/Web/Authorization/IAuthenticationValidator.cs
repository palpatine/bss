﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;

namespace SolvesIt.BSSystem.Web.Authorization
{
    internal interface IAuthenticationValidator
    {
        AuthenticationResult TryAuthenticate(HttpContextBase httpContext);

        Task<AuthenticationResult> TryAuthenticateAsync(HttpAuthenticationContext httpContext);
    }
}