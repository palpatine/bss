﻿using System.Collections.Generic;
using SolvesIt.BSSystem.Core.Logic.Notification;

namespace SolvesIt.BSSystem.Web.Configuration
{
    public sealed class GlobalMessageParametersProvider : IGlobalMessageParametersProvider
    {
        public IDictionary<string, string> GetAttributes()
        {
            return new Dictionary<string, string>();
        }
    }
}