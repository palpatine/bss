﻿using System.Configuration;

namespace SolvesIt.BSSystem.Web.Configuration
{
    public sealed class ModulesConfigurationSection : ConfigurationSection
    {
        private const string ModulesLocationKey = "modulesLocation";

        [ConfigurationProperty(ModulesLocationKey)]
        public string ModulesLocation
        {
            get { return (string)this[ModulesLocationKey]; }
            set { this[ModulesLocationKey] = value; }
        }
    }
}