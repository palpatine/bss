﻿using System;
using System.Configuration;
using SolvesIt.BSSystem.Core.Abstraction.Environment;

namespace SolvesIt.BSSystem.Web.Configuration
{
    internal sealed class ConfigurationProvider : IConfigurationProvider
    {
        public string ConnectionString => ConfigurationManager.ConnectionStrings["BSS_DB"].ConnectionString;

        public TimeSpan SessionTimeout => new TimeSpan(0, 15, 0);

        public bool IsCacheEnabled
        {
            get
            {
                var disableCache = ConfigurationManager.AppSettings["IsCacheEnabled"];
                return disableCache == null || bool.Parse(disableCache);
            }
        }
    }
}