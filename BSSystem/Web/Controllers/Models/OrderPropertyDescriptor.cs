namespace SolvesIt.BSSystem.Web.Controllers.Models
{
    internal sealed class OrderPropertyDescriptor
    {
        public string DisplayName { get; set; }

        public bool OrderDescending { get; set; }

        public string PropertyName { get; set; }
    }
}