using System;
using System.Net.Http;
using System.Web.Http;

namespace SolvesIt.BSSystem.Web.Controllers.Models
{
    internal sealed class AssignmentsCallDescriptor
    {
        public IHttpActionResult Error { get; set; }

        public Type JunctionType { get; set; }

        public Type MasterType { get; set; }

        public Type RelatedType { get; set; }
    }
}