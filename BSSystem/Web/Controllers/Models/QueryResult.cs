using System.Linq;

namespace SolvesIt.BSSystem.Web.Controllers.Models
{
    public sealed class QueryResult
    {
        public IQueryable Content { get; set; }

        public int? Count { get; set; }
    }
}