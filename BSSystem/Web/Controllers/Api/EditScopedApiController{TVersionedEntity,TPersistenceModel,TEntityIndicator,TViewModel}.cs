﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    [SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes", Justification = "Base class")]
    public abstract class EditScopedApiController<TVersionedEntity, TPersistenceModel, TEntityIndicator, TViewModel>
        : EditApiController<TVersionedEntity, TPersistenceModel, TEntityIndicator, TViewModel>
        where TVersionedEntity : VersionedEntity, IScopedEntity, TEntityIndicator, new()
        where TPersistenceModel : TEntityIndicator
        where TEntityIndicator : IStorable, IMarker
        where TViewModel : class, IViewModel
    {
        protected EditScopedApiController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<TVersionedEntity, TPersistenceModel, TEntityIndicator> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, entityService, permissionHelper, settingService)
        {
        }

        protected override ITableQuery<TVersionedEntity> BaseQueryFromGet(AccessibleItems accessibleItems)
        {
            var getAll = accessibleItems.GetAll;
            var accessibleItemsValues = accessibleItems.Values;
            var query = DatabaseAccessor.Query<TVersionedEntity>()
                .Where(x => !x.IsDeleted
                            && x.Scope.IsEquivalent(SessionDataProvider.Scope)
                            && (getAll || accessibleItemsValues.Contains(x.Id)));
            return query;
        }
    }
}