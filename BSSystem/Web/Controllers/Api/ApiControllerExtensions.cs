using System.Web.Http;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    public static class ApiControllerExtensions
    {
        public static IHttpActionResult NoContent(this ApiController controller)
        {
            return new NoContentResult(controller);
        }
    }
}