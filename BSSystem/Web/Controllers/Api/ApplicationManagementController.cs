﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Enumerations;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.Properties;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    [AuthorizationRequiredApi]
    public sealed class ApplicationManagementController : BaseApiController
    {
        private readonly IMenuService _menuService;

        private readonly IPermissionService _permissionService;

        public ApplicationManagementController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IPermissionService permissionService,
            IMenuService menuService,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, settingService)
        {
            _permissionService = permissionService;
            _menuService = menuService;
        }

        public async Task<ApplicationDescriptor> Get()
        {
            var userDisplayName = await DatabaseAccessor.Query<User>()
                .Where(x => x.Id == SessionDataProvider.CurrentUser.Id)
                .Select(x => x.DisplayName)
                .SingleAsync();

            var organizationDisplayName = await DatabaseAccessor.Query<Organization>()
                .Where(x => x.Id == SessionDataProvider.Scope.Id)
                .Select(x => x.DisplayName)
                .SingleAsync();

            var modules = await DatabaseAccessor.Query<ModuleOrganization>()
                .Where(
                    x =>
                        !x.IsDeleted && x.EntityStatus == EntityStatus.Active &&
                        x.Organization.IsEquivalent(SessionDataProvider.Scope))
                .Join(DatabaseAccessor.Query<Module>(), x => x.Module, (mo, m) => m.Key)
                .ToListAsync();

            var menuPositions = (await _menuService.GetCurrentUserMenu()).ToArray();
            var sections = menuPositions.Where(x => x.Id > 0)
                .GroupBy(x => x.ParentId)
                .ToDictionary(
                        x => x.Key ?? 0,
                        x => x.Select(item => new MenuSectionViewModel
                        {
                            Id = item.Id,
                            IconName = item.IconName,
                            Key = item.Key,
                            NavigationPosition = (NavigationPositionKind)(item.NavigationPosition ?? 0),
                            Title = WebResources.ResourceManager.GetString("MenuSectionViewModel_" + item.Key) ?? $"[MenuSectionViewModel_{item.Key}]",
                        }).ToArray());

            var items = menuPositions.Where(x => x.Id < 0)
                 .GroupBy(x => x.ParentId)
                .ToDictionary(
                    x => x.Key ?? 0,
                    x => x.Select(item => new MenuItemViewModel
                    {
                        IconName = item.IconName,
                        Key = item.Key,
                        Title = WebResources.ResourceManager.GetString("MenuItemViewModel_" + item.Key) ?? $"[MenuItemViewModel_{item.Key}]",
                        DisplayShortName = WebResources.ResourceManager.GetString("MenuItemViewModel_DisplayShortName_" + item.Key) ?? $"[MenuItemViewModel_DisplayShortName_{item.Key}]",
                    })
                    .ToArray());

            foreach (var item in sections.Values.SelectMany(x => x))
            {
                if (sections.ContainsKey(item.Id))
                {
                    item.MenuSections = sections[item.Id];
                }

                if (items.ContainsKey(item.Id))
                {
                    item.MenuItems = items[item.Id];
                }
            }

            var result = new ApplicationDescriptor
            {
                UserData = new UserData
                {
                    User = new NamedViewModel
                    {
                        DisplayName = userDisplayName,
                        Id = SessionDataProvider.CurrentUser.Id,
                    },
                    Organization = new NamedViewModel
                    {
                        DisplayName = organizationDisplayName,
                        Id = SessionDataProvider.CurrentUser.Id,
                    },
                    Modules = modules,
                    EffectivePermissions = (await _permissionService.GetCurrentUserEffectivePermissions())
                        .Select(p => new UserDataPermissionDescriptor
                        {
                            Path = p.Path,
                            IsGlobal = p.IsGlobal
                        })
                },
                MenuSections = sections[0]
            };

            return result;
        }
    }
}