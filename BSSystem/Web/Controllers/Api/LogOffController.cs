using System;
using System.Threading.Tasks;
using System.Web.Http;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    public sealed class LogOffController : BaseApiController
    {
        private readonly IAuthenticationHelper _helper;
        private readonly ISessionService _sessionService;

        public LogOffController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IAuthenticationHelper helper,
            ISessionService sessionService,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, settingService)
        {
            _helper = helper;
            _sessionService = sessionService;
        }

        public async Task<IHttpActionResult> Get()
        {
            _helper.ClearAuthentication();

            Session session = await DatabaseAccessor.Query<Session>()
                .SingleAsync(x => StorableExtensions.IsEquivalent(x, SessionDataProvider.Session));
            session.TimeEnd = DateTime.UtcNow;
            await _sessionService.WriteAsync(session);

            return Ok();
        }
    }
}