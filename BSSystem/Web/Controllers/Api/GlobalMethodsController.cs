﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Environment.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    [GlobalMethodsController]
    public sealed class GlobalMethodsController : BaseApiController
    {
        private readonly ILockRepository _lockRepository;

        public GlobalMethodsController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            ILockRepository lockRepository,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, settingService)
        {
            _lockRepository = lockRepository;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Unlock()
        {
            IEnumerable<string> operationTokens;
            Request.Headers.TryGetValues("OperationToken", out operationTokens);
            if (operationTokens == null || operationTokens.Count() != 1)
            {
                return BadRequest("Operation token invalid");
            }

            var operationToken = operationTokens.Single();

            await _lockRepository.RemoveLockAsync(operationToken);

            return Ok();
        }
    }
}