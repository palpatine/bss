﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    internal class NoContentResult : IHttpActionResult
    {
        private readonly ApiController _controller;

        public NoContentResult(ApiController controller)
        {
            _controller = controller;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(_controller.Request.CreateResponse(HttpStatusCode.NoContent));
        }
    }
}