﻿using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Controllers.Models;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    internal sealed class ConfigurationController : BaseApiController
    {
        public ConfigurationController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, settingService)
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        public TableConfiguration GetOrderPropertiesColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<OrderPropertyDescriptor>();
            AddCreateRemoveActionsColumn(builder);
            builder.AddNonEntityColumn(x => x.DisplayName)
                .SetTemplateUrl(TemplatesMetadata.BssTableTemplates.Configuration.Sort.ColumnChooser);
            builder.AddNonEntityColumn(x => x.OrderDescending)
                .SetHeaderTemplate(string.Empty)
                .SetTemplateUrl(TemplatesMetadata.BssTableTemplates.Configuration.Sort.DirectionChooser);
            AddUpDownActionsColumn(builder);

            return builder.Build();
        }

        private static void AddCreateRemoveActionsColumn<TViewModel>(TableConfigurationBuilder<TViewModel> builder)
        {
            var column = new ColumnDefinition
            {
                ColumnHeaderTemplateUrl = TemplatesMetadata.BssTableTemplates.Configuration.Sort.ColumnAdder,
                TemplateUrl = TemplatesMetadata.BssTableTemplates.Configuration.Sort.ColumnRemover,
                IsActionsColumn = true
            };

            builder.Add(new ColumnProvider(column));
        }

        private static void AddUpDownActionsColumn<TViewModel>(TableConfigurationBuilder<TViewModel> builder)
        {
            var column = new ColumnDefinition
            {
                TemplateUrl = TemplatesMetadata.BssTableTemplates.Configuration.Sort.OrderChooser,
                IsActionsColumn = true
            };

            builder.Add(new ColumnProvider(column));
        }
    }
}