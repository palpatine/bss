﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Properties;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    public sealed class LogOnController : ApiController
    {
        private readonly IAuthenticationHelper _authenticationHelper;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly ISessionService _sessionService;
        private readonly IUserService _userService;

        public LogOnController(
            IUserService userService,
            IConfigurationProvider configurationProvider,
            ISessionService sessionService,
            IAuthenticationHelper authenticationHelper)
        {
            _userService = userService;
            _configurationProvider = configurationProvider;
            _sessionService = sessionService;
            _authenticationHelper = authenticationHelper;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post(AuthenticationViewModel viewModel)
        {
            if (!string.IsNullOrWhiteSpace(viewModel.Login)
                && !string.IsNullOrWhiteSpace(viewModel.Password))
            {
                var data = await _userService.GetUserAndOrganizationByLoginAsync(viewModel.Login, viewModel.Password);

                if (data != null)
                {
                    var session = new Session
                    {
                        User = data.Item1,
                        Guid = Guid.NewGuid(),
                        RemoteIP = GetIpAddress().GetAddressBytes(),
                        TimeStart = DateTime.UtcNow,
                        TimeEnd = DateTime.UtcNow.Add(_configurationProvider.SessionTimeout)
                    };

                    await _sessionService.WriteAsync(session);
                    _authenticationHelper.SetAuthenticationCookie(data.Item1.Id, viewModel.Login, data.Item2.Id, session.Guid);

                    return Ok(new ValidationResult { IsValid = true, Errors = Enumerable.Empty<ValidationErrorViewModel>() });
                }
            }

            return Ok(new ValidationResult
            {
                IsValid = false,
                Errors = new[]
            {
                new ValidationErrorViewModel
                {
                    Message = WebResources.InvalidLoginOrPassword
                }
            }
            });
        }

        private static IPAddress GetIpAddress()
        {
            ////var ip = Request.ServerVariables["REMOTE_ADDR"];
            ////ip = ip.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Last().Trim();

            ////IPAddress address;

            ////if (IPAddress.TryParse(ip, out address))
            ////{
            ////    if (address.AddressFamily == AddressFamily.InterNetwork)
            ////    {
            ////        return address;
            ////    }
            ////}

            return new IPAddress(0);
        }
    }
}