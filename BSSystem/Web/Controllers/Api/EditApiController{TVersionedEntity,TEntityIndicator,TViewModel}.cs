﻿using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes", Justification = "Base class")]
    public abstract class EditApiController<TVersionedEntity, TEntityIndicator, TViewModel>
        : EditApiController<TVersionedEntity, TVersionedEntity, TEntityIndicator, TViewModel>
        where TVersionedEntity : VersionedEntity, TEntityIndicator, new()
        where TEntityIndicator : IStorable, IMarker
        where TViewModel : class, IViewModel
    {
        protected EditApiController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<TVersionedEntity, TEntityIndicator> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, entityService, permissionHelper, settingService)
        {
        }
    }
}