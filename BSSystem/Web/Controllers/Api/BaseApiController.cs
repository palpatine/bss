﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Models;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.Properties;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    [AuthorizationRequiredApi]
    public class BaseApiController : ApiController
    {
        private static readonly ConcurrentDictionary<Type, IReadOnlyCollection<PropertyInfo>> _propertiesForTimeZoneShift
            = new ConcurrentDictionary<Type, IReadOnlyCollection<PropertyInfo>>();

        private readonly IQueryStringParser _queryStringParser;
        private readonly ISettingService _settingService;

        protected BaseApiController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            ISettingService settingService)
        {
            _queryStringParser = queryStringParser;
            _settingService = settingService;
            SessionDataProvider = sessionDataProvider;
            DatabaseAccessor = databaseAccessor;
        }

        protected IDatabaseAccessor DatabaseAccessor { get; private set; }
        protected ISessionDataProvider SessionDataProvider { get; }

        public void ResolveSession(IPrincipal principal)
        {
            SessionDataProvider.Resolve(principal);
        }

        protected static DateTime? ConvertToDateTime(DateTimeOffset? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            return ConvertToDateTime(date.Value);
        }

        protected static DateTime? ConvertToDateTime(DateTimeOffset date)
        {
            var dateTime = date.DateTime;
            if (dateTime < WebConstants.MinimalAccepableDate || dateTime > WebConstants.MaximalAccepableDate)
            {
                return null;
            }

            return dateTime;
        }

        protected static bool IsFilteredResultsEmpty(
            IQueryable query)
        {
            var method =
                typeof(Queryable).GetMethods().Single(x => x.Name == "Any" && x.GetParameters().Count() == 1)
                    .MakeGenericMethod(query.ElementType);
            try
            {
                var result = (bool)method.Invoke(null, new object[] { query.RemovePagingAndSorting() });
                return !result;
            }
            catch (TargetInvocationException ex)
            {
                if (ex.InnerException is HttpResponseException)
                {
                    throw ex.InnerException;
                }

                throw;
            }
        }

        protected async Task<DateTime?> ConvertToDateAsync(DateTimeOffset? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            return await ConvertToDateAsync(date.Value);
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected async Task<DateTime> ConvertToDateAsync(DateTimeOffset date)
        {
            var dateTime = date.Date;
            if (dateTime.Kind == DateTimeKind.Unspecified)
            {
                dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
            }

            if (dateTime.Kind != DateTimeKind.Utc)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, WebResources.DatesMustBeProvidedInUTC));
            }

            if (dateTime < WebConstants.MinimalAccepableDate)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(WebResources.MinimalAcceptableDate, WebConstants.MinimalAccepableDate)));
            }

            if (dateTime > WebConstants.MaximalAccepableDate)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(WebResources.MaximalAcceptableDate, WebConstants.MaximalAccepableDate)));
            }

            var timeZoneId = (await _settingService.GetCurrentUserEffectiveSettings())
                .Single(x => x.Key == SettingsCommon.TimeZone)
                .Value;
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            dateTime = dateTime.Add(-timeZone.GetUtcOffset(DateTime.UtcNow));
            return dateTime;
        }

        protected async Task<IHttpActionResult> FinalizeModelAsync<TViewModel>(
            IQueryable<TViewModel> query)
            where TViewModel : IViewModel
        {
            var resultQuery = ProcessQueryString(query);

            if (IsFilteredResultsEmpty(resultQuery.Content))
            {
                return NotFound();
            }

            var method =
                typeof(Queryable).GetMethods().Single(x => x.Name == "SingleOrDefault" && x.GetParameters().Count() == 1)
                    .MakeGenericMethod(resultQuery.Content.ElementType);
            try
            {
                var result = method.Invoke(null, new object[] { resultQuery.Content });
                await ProcessOrganizationTimeZoneShiftAsync(typeof(TViewModel), new[] { result }.AsQueryable());
                return Ok(result);
            }
            catch (TargetInvocationException ex)
            {
                if (ex.InnerException is HttpResponseException)
                {
                    throw ex.InnerException;
                }

                throw;
            }
        }

        protected virtual async Task<IHttpActionResult> FinalizeQueryAsync<TViewModel>(
            IQueryable<TViewModel> query,
            Func<IHttpActionResult> onNoContent = null)
        {
            var resultQuery = ProcessQueryString(query);

            if (IsFilteredResultsEmpty(resultQuery.Content))
            {
                if (onNoContent == null)
                {
                    return this.NoContent();
                }

                return onNoContent();
            }

            resultQuery.Content = await ProcessOrganizationTimeZoneShiftAsync(typeof(TViewModel), resultQuery.Content);

            if (resultQuery.Count == null)
            {
                return Ok(resultQuery.Content);
            }

            return Ok(resultQuery);
        }

        protected QueryResult ProcessQueryString<TViewModel>(
            IQueryable<TViewModel> query)
        {
            var url = HttpContext.Current.Request.RawUrl;
            var isQueryAvailable = url.IndexOf("?", StringComparison.Ordinal) != -1;
            IQueryable resultQuery = query;

            if (isQueryAvailable)
            {
                var queryString = HttpUtility.UrlDecode(url.Substring(url.IndexOf("?", StringComparison.Ordinal) + 1));
                QueryMetadata queryMetadata;
                resultQuery = _queryStringParser.Apply(query, queryString, out queryMetadata);

                if (!queryMetadata.IsValid || !AreQueryOptionsValid(queryMetadata))
                {
                    throw new HttpResponseException(HttpStatusCode.NotAcceptable);
                }

                if (queryMetadata.IncludeInlineCount)
                {
                    var method =
                        typeof(Queryable).GetMethods().Single(x => x.Name == "Count" && x.GetParameters().Count() == 1)
                            .MakeGenericMethod(query.ElementType);

                    try
                    {
                        var count = (int)method.Invoke(null, new object[] { query.RemovePagingAndSorting() });

                        var result = new QueryResult
                        {
                            Content = resultQuery,
                            Count = count
                        };

                        return result;
                    }
                    catch (TargetInvocationException ex)
                    {
                        if (ex.InnerException is HttpResponseException)
                        {
                            throw ex.InnerException;
                        }

                        throw;
                    }
                }
            }

            return new QueryResult
            {
                Content = resultQuery
            };
        }

        protected IHttpActionResult ProcessValidationErrors(
            IEnumerable<ValidationError> validationErrors,
            IStorable entity = null,
            string messageOnSuccess = null)
        {
            if (validationErrors == null || !validationErrors.Any())
            {
                return Ok(new ValidationResult
                {
                    IsValid = true,
                    Errors = Enumerable.Empty<ValidationErrorViewModel>(),
                    Id = entity?.Id,
                    Message = messageOnSuccess
                });
            }

            var errors = validationErrors.Select(x => new ValidationErrorViewModel
            {
                Message = x.ValidationMessage,
                ErrorCode = x.ErrorCode,
                Reference = x.Reference,
                AdditionalValue = x.AdditionalValue
            });

            return Ok(new ValidationResult { Errors = errors });
        }

        private static bool AreQueryOptionsValid(QueryMetadata options)
        {
            if (options.IsSkipped && options.IsTopped && !options.IsOrdered)
            {
                return false;
            }

            if (options.IsSkipped && !options.IsTopped)
            {
                return false;
            }

            if (options.IsTopped && options.TopValue < 1)
            {
                return false;
            }

            if (options.IsSkipped && options.SkipValue < 0)
            {
                return false;
            }

            return true;
        }

        private static ICollection<object> Enumerate(IQueryable content)
        {
            var list = new List<object>();

            foreach (var item in content)
            {
                list.Add(item);
            }

            return list;
        }

        private async Task<IQueryable> ProcessOrganizationTimeZoneShiftAsync(Type type, IQueryable content)
        {
            var timeZoneSetting =
                (await _settingService.GetCurrentUserEffectiveSettings())
                    .Single(x => x.Key == SettingsCommon.TimeZone)
                    .Value;
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneSetting);

            var data = Enumerate(content).ToArray();
            if (data.Length == 0)
            {
                return content;
            }

            var first = data[0];

            var propertiesToShift = _propertiesForTimeZoneShift.GetOrAdd(first.GetType(), key =>
            {
                var properties = new List<PropertyInfo>();

                foreach (var property in first.GetType().GetProperties())
                {
                    var modelProperty = type.GetProperty(property.Name);
                    var sqlType = StorableHelper.GetSqlTypeAttribute(type, modelProperty);
                    if ((sqlType != null && sqlType.Type == SqlDbType.SmallDateTime) || typeof(IViewModel).IsAssignableFrom(property.PropertyType))
                    {
                        properties.Add(property);
                    }
                }

                return properties.AsReadOnly();
            });

            foreach (var item in data)
            {
                foreach (var property in propertiesToShift)
                {
                    var value = property.GetValue(item);
                    var model = value as IViewModel;

                    if (model == null && value != null)
                    {
                        var date = (DateTimeOffset)value;
                        var newValue = date.Add(timeZone.BaseUtcOffset);
                        property.SetValue(item, newValue);
                    }
                    else if (model != null)
                    {
                        await ProcessOrganizationTimeZoneShiftAsync(model.GetType(), new[] { model }.AsQueryable());
                    }
                }
            }

            return data.AsQueryable();
        }
    }
}