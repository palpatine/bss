using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Models;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.Properties;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Linq")]
    public sealed class AssignmentsController : BaseApiController
    {
        private static readonly IDictionary<Type, Type> EntitySubstituedsForColumnsDefinition = new Dictionary<Type, Type> { { typeof(Module), typeof(ModuleViewModel) } };
        private readonly IAssignmentService _assignmentService;

        public AssignmentsController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IAssignmentService assignmentService,
            ISettingService settingService)
            : base(
                databaseAccessor,
                sessionDataProvider,
                queryStringParser,
                settingService)
        {
            _assignmentService = assignmentService;
        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Net.Http.HttpRequestMessageExtensions.CreateErrorResponse(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,System.String)", Justification = "Generic API error")]
        [RequiredPermission(PermissionsCommon.Keys.Details, PermissionContainerKeyRequestParameterName = "master")]
        [ResponseType(typeof(IQueryable<JobRoleAssignmentViewModel>))]
        public async Task<IHttpActionResult> Get(
            string master,
            int id,
            string related)
        {
            var descriptor = GetAssignmentsCallDescriptor(master, related);

            if (descriptor.Error != null)
            {
                return descriptor.Error;
            }

            var masterType = descriptor.MasterType;
            var relatedType = descriptor.RelatedType;
            var junctionType = descriptor.JunctionType;

            var masterMarker = StorableHelper.GetMarkerInstance(
                masterType,
                id);

            if (typeof(IJobRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType))
            {
                return await GetJobRoleAsync(
                    masterType,
                    masterMarker,
                    relatedType,
                    junctionType);
            }

            if (typeof(IRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType))
            {
                return await GetRoleAsync(
                    masterType,
                    masterMarker,
                    relatedType,
                    junctionType);
            }

            if (typeof(IAssignmentJunctionEntity).IsAssignableFrom(junctionType)
                || (typeof(IJunctionEntity).IsAssignableFrom(junctionType) && typeof(IDatedEntity).IsAssignableFrom(junctionType)))
            {
                return await GetAssignmentAsync(
                    masterType,
                    masterMarker,
                    relatedType,
                    junctionType);
            }

            return BadRequest(
                string.Format(CultureInfo.InvariantCulture, WebResources.UnknownConfiguration, junctionType.Name));
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [ResponseType(typeof(TableConfiguration))]
        public IHttpActionResult GetDisplayColumnsConfiguration(
            string master,
            string related)
        {
            var descriptor = GetAssignmentsCallDescriptor(master, related);

            if (descriptor.Error != null)
            {
                return descriptor.Error;
            }

            var relatedType = descriptor.RelatedType;
            var junctionType = descriptor.JunctionType;

            relatedType = EntitySubstituedsForColumnsDefinition.ContainsKey(relatedType) ? EntitySubstituedsForColumnsDefinition[relatedType] : relatedType;

            if (typeof(IJobRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType))
            {
                return Ok(GetJobRoleDisplayColumnsConfiguration(junctionType, relatedType));
            }

            if (typeof(IRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType))
            {
                return Ok(GetRoleDisplayColumnsConfiguration(junctionType, relatedType));
            }

            if (typeof(IAssignmentJunctionEntity).IsAssignableFrom(junctionType)
                || (typeof(IJunctionEntity).IsAssignableFrom(junctionType) && typeof(IDatedEntity).IsAssignableFrom(junctionType)))
            {
                return Ok(GetAssignmentDisplayColumnsConfiguration(junctionType, relatedType));
            }

            throw new NotSupportedException();
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [ResponseType(typeof(TableConfiguration))]
        public IHttpActionResult GetEditColumnsConfiguration(
            string master,
            string related)
        {
            var descriptor = GetAssignmentsCallDescriptor(master, related);
            if (descriptor.Error != null)
            {
                return descriptor.Error;
            }

            var relatedType = descriptor.RelatedType;
            var junctionType = descriptor.JunctionType;

            relatedType = EntitySubstituedsForColumnsDefinition.ContainsKey(relatedType) ? EntitySubstituedsForColumnsDefinition[relatedType] : relatedType;

            if (typeof(IJobRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType))
            {
                return Ok(GetJobRoleEditColumnsConfiguration(junctionType, relatedType));
            }

            if (typeof(IRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType))
            {
                return Ok(GetRoleEditColumnsConfiguration(junctionType, relatedType));
            }

            if (typeof(IAssignmentJunctionEntity).IsAssignableFrom(junctionType)
                || (typeof(IJunctionEntity).IsAssignableFrom(junctionType) && typeof(IDatedEntity).IsAssignableFrom(junctionType)))
            {
                return Ok(GetBasicEditColumnsConfiguration(junctionType, relatedType));
            }

            throw new NotSupportedException();
        }

        [ResponseType(typeof(IQueryable<NamedViewModel>))]
        public async Task<IHttpActionResult> GetRelatedNamedMarkers(
            string master,
            int id,
            string related)
        {
            var descriptor = GetAssignmentsCallDescriptor(master, related);

            if (descriptor.Error != null)
            {
                return descriptor.Error;
            }

            var masterType = descriptor.MasterType;
            var relatedType = descriptor.RelatedType;
            var junctionType = descriptor.JunctionType;

            var masterMarker = StorableHelper.GetMarkerInstance(
                masterType,
                id);

            return await GetAssignmentNamedMarkersAsync(
                masterType,
                masterMarker,
                relatedType,
                junctionType);
        }

        [HttpPost]
        [RequiredPermission(
            PermissionsCommon.Keys.Details + PermissionsCommon.Containers.Assignment,
            PermissionContainerKeyRequestParameterName = "master",
            PermissionKeyRequestParameterName = "related")]
        [ResponseType(typeof(IEnumerable<ValidationError>))]
        public async Task<IHttpActionResult> Set(
            string master,
            int id,
            string related,
            [FromBody] IEnumerable<JobRoleAssignmentPersistenceViewModel> assignments)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var descriptor = GetAssignmentsCallDescriptor(master, related);
            if (descriptor.Error != null)
            {
                return descriptor.Error;
            }

            var masterType = descriptor.MasterType;
            var relatedType = descriptor.RelatedType;
            var junctionType = descriptor.JunctionType;

            var markerType = StorableHelper.GetMarkerType(masterType);
            var masterMarker = StorableHelper.GetMarkerInstance(
                markerType,
                id);
            var relatedMarker = StorableHelper.GetMarkerType(relatedType);

            Task<IEnumerable<ValidationError>> persisanceTask;
            if (typeof(IJobRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType))
            {
                var method =
                    ExpressionExtensions.GetMethod(() => PersistJobRoleAssignmentAsync<IStorable, IMarker>(null, null))
                        .GetGenericMethodDefinition()
                        .MakeGenericMethod(markerType, relatedMarker);

                persisanceTask = (Task<IEnumerable<ValidationError>>)method.Invoke(this, new object[] { masterMarker, assignments });
            }
            else if (typeof(IRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType))
            {
                var method =
                    ExpressionExtensions.GetMethod(() => PersistRoleAssignmentAsync<IStorable, IMarker>(null, null))
                        .GetGenericMethodDefinition()
                        .MakeGenericMethod(markerType, relatedMarker);

                persisanceTask = (Task<IEnumerable<ValidationError>>)method.Invoke(this, new object[] { masterMarker, assignments });
            }
            else if (typeof(IAssignmentJunctionEntity).IsAssignableFrom(junctionType)
                     || (typeof(IJunctionEntity).IsAssignableFrom(junctionType) && typeof(IDatedEntity).IsAssignableFrom(junctionType)))
            {
                var method =
                    ExpressionExtensions.GetMethod(() => PersistAssignmentAsync<IStorable, IMarker>(null, null))
                        .GetGenericMethodDefinition()
                        .MakeGenericMethod(markerType, relatedMarker);

                persisanceTask =
                    (Task<IEnumerable<ValidationError>>)method.Invoke(this, new object[] { masterMarker, assignments });
            }
            else
            {
                return BadRequest("Unknown configuration {junctionType.Name}.");
            }

            var validationErrors = await persisanceTask;
            return ProcessValidationErrors(validationErrors);
        }

        private static TableConfiguration GetAssignmentDisplayColumnsConfiguration(
            Type junctionType,
            Type relatedType)
        {
            var builder = new TableConfigurationBuilder<AssignmentViewModel>();
            builder.AddDetailsActionColumn(relatedType, x => x.Element.Id);
            builder.AddColumn(x => x.Element.DisplayName, relatedType)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.DateStart, junctionType)
                .CanSort();
            builder.AddColumn(x => x.DateEnd, junctionType)
                .CanSort();
            return builder.Build();
        }

        private static Expression<Func<IJunctionEntity, IAssignmentRelatedEntity, DatedViewModel>> GetAssignmentNamedMarkersSelector()
        {
            return (j, r) => new DatedViewModel
            {
                Id = r.Id,
                DateStart = r.DateStart > ((IDatedEntity)j).DateStart ? r.DateStart : ((IDatedEntity)j).DateStart,
                DateEnd = r.DateEnd < ((IDatedEntity)j).DateEnd ? r.DateEnd : ((IDatedEntity)j).DateEnd,
                DisplayName = r.DisplayName
            };
        }

        private static Expression<Func<IJunctionEntity, IScopedEntity, AssignmentViewModel>> GetAssignmentSelector()
        {
            return (j, r) => new AssignmentViewModel
            {
                Id = j.Id,
                Element = new DetailedEntityViewModel
                {
                    Id = r.Id,
                    DateStart = ((IDatedEntity)r).DateStart,
                    DateEnd = ((IDatedEntity)r).DateEnd,
                    DisplayName = ((INamedEntity)r).DisplayName
                },
                DateStart = ((IDatedEntity)j).DateStart,
                DateEnd = ((IDatedEntity)j).DateEnd
            };
        }

        private static TableConfiguration GetBasicEditColumnsConfiguration(
            Type junctionType,
            Type relatedType)
        {
            var builder = new TableConfigurationBuilder<AssignmentViewModel>();
            builder.AddColumn(new ColumnDefinition
            {
                ColumnHeaderTemplateUrl = TemplatesMetadata.AssignmentEditorTemplates.CommandsColumnHeader,
                TemplateUrl = TemplatesMetadata.AssignmentEditorTemplates.CommandsColumn,
                IsActionsColumn = true
            });
            builder.AddColumn(x => x.Element.DisplayName, relatedType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.ElementColumn)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.DateStart, junctionType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.DateStartColumn)
                .CanSort();
            builder.AddColumn(x => x.DateEnd, junctionType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.DateEndColumn)
                .CanSort();
            return builder.Build();
        }

        private static TableConfiguration GetJobRoleEditColumnsConfiguration(
            Type junctionType,
            Type relatedType)
        {
            var builder = new TableConfigurationBuilder<JobRoleAssignmentViewModel>();
            builder.AddColumn(new ColumnDefinition
            {
                ColumnHeaderTemplateUrl = TemplatesMetadata.AssignmentEditorTemplates.CommandsColumnHeader,
                TemplateUrl = TemplatesMetadata.AssignmentEditorTemplates.CommandsColumn,
                IsActionsColumn = true
            });
            builder.AddColumn(x => x.Element.DisplayName, relatedType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.ElementColumn)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Role.DisplayName, typeof(RoleDefinition))
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.RoleColumn)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.JobKind, null)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.JobKindColumn)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.JobValue, null)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.JobValueColumn)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart, junctionType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.DateStartColumn)
                .CanSort();
            builder.AddColumn(x => x.DateEnd, junctionType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.DateEndColumn)
                .CanSort();
            return builder.Build();
        }

        private static TableConfiguration GetRoleDisplayColumnsConfiguration(
            Type junctionType,
            Type relatedType)
        {
            var builder = new TableConfigurationBuilder<RoleAssignmentViewModel>();
            builder.AddDetailsActionColumn(relatedType, x => x.Element.Id);
            builder.AddColumn(x => x.Element.DisplayName, relatedType)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Role.DisplayName, typeof(RoleDefinition))
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart, junctionType)
                .CanSort();
            builder.AddColumn(x => x.DateEnd, junctionType)
                .CanSort();
            return builder.Build();
        }

        private static TableConfiguration GetRoleEditColumnsConfiguration(
            Type junctionType,
            Type relatedType)
        {
            var builder = new TableConfigurationBuilder<RoleAssignmentViewModel>();
            builder.AddColumn(new ColumnDefinition
            {
                ColumnHeaderTemplateUrl = TemplatesMetadata.AssignmentEditorTemplates.CommandsColumnHeader,
                TemplateUrl = TemplatesMetadata.AssignmentEditorTemplates.CommandsColumn,
                IsActionsColumn = true
            });
            builder.AddColumn(x => x.Element.DisplayName, relatedType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.ElementColumn)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Role.DisplayName, typeof(RoleDefinition))
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.RoleColumn)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart, junctionType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.DateStartColumn)
                .CanSort();
            builder.AddColumn(x => x.DateEnd, junctionType)
                .SetTemplateUrl(TemplatesMetadata.AssignmentEditorTemplates.DateEndColumn)
                .CanSort();
            return builder.Build();
        }

        private async Task<IHttpActionResult> GetAssignmentAsync(
            Type masterType,
            IMarker masterMarker,
            Type relatedType,
            Type junctionType)
        {
            var typesOrdered = new[] { masterType, relatedType }
                .OrderBy(x => x.Name)
                .ToArray();

            if (typesOrdered[0] == masterType)
            {
                var query = DatabaseAccessor.Query<IJunctionEntity>(junctionType)
                    .Where(junction => junction.First.Id == masterMarker.Id && !junction.IsDeleted)
                    .Join(
                        DatabaseAccessor.Query<IScopedEntity>(relatedType)
                            .Where(element => element.Scope.IsEquivalent(SessionDataProvider.Scope)),
                        junction => junction.Second,
                        r => r,
                        GetAssignmentSelector())
                    .AsQueryable();

                return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
            }

            if (typeof(IAssignmentRelatedEntity).IsAssignableFrom(relatedType))
            {
                var query = DatabaseAccessor.Query<IJunctionEntity>(junctionType)
                    .Where(junction => junction.Second.Id == masterMarker.Id && !junction.IsDeleted)
                    .Join(
                        DatabaseAccessor.Query<IScopedEntity>(relatedType)
                            .Where(element => element.Scope.IsEquivalent(SessionDataProvider.Scope)),
                        junction => junction.First,
                        r => r,
                        GetAssignmentSelector())
                    .AsQueryable();
                return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
            }

            if (typeof(Module) == relatedType)
            {
                var query = DatabaseAccessor.Query<IJunctionEntity>(junctionType)
                    .Where(junction => junction.Second.Id == masterMarker.Id && !junction.IsDeleted)
                    .Join(
                        DatabaseAccessor.Query<Module>(relatedType),
                        junction => junction.First,
                        r => r,
                        (j, r) => new ModuleAssignmentViewModel
                        {
                            Id = j.Id,
                            Element = new ModuleViewModel
                            {
                                Id = r.Id,
                                Key = r.Key,
                                Required = r.Required
                            },
                            DateStart = ((IDatedEntity)j).DateStart,
                            DateEnd = ((IDatedEntity)j).DateEnd
                        })
                    .ToList()
                    .AsQueryable();
                return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
            }

            throw new InvalidOperationException();
        }

        private async Task<IHttpActionResult> GetAssignmentNamedMarkersAsync(
            Type masterType,
            IMarker masterMarker,
            Type relatedType,
            Type junctionType)
        {
            var typesOrdered = new[] { masterType, relatedType }
                .OrderBy(x => x.Name)
                .ToArray();

            if (typesOrdered[0] == masterType)
            {
                var query = DatabaseAccessor.Query<IJunctionEntity>(junctionType)
                    .Where(junction => junction.First.Id == masterMarker.Id && !junction.IsDeleted)
                    .Join(
                        DatabaseAccessor.Query<IAssignmentRelatedEntity>(relatedType)
                            .Where(element => element.Scope.IsEquivalent(SessionDataProvider.Scope)),
                        junction => junction.Second,
                        r => r,
                        GetAssignmentSelector())
                    .AsQueryable();

                return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
            }
            else
            {
                var query = DatabaseAccessor.Query<IJunctionEntity>(junctionType)
                    .Where(junction => junction.Second.Id == masterMarker.Id && !junction.IsDeleted)
                    .Join(
                        DatabaseAccessor.Query<IAssignmentRelatedEntity>(relatedType)
                            .Where(element => element.Scope.IsEquivalent(SessionDataProvider.Scope)),
                        junction => junction.First,
                        r => r,
                        GetAssignmentNamedMarkersSelector())
                    .AsQueryable();

                return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
            }
        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Net.Http.HttpRequestMessageExtensions.CreateErrorResponse(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,System.String)", Justification = "Generic API error")]
        private AssignmentsCallDescriptor GetAssignmentsCallDescriptor(
            string master,
            string related)
        {
            Type masterType;
            if (!StorableHelper.TryGetEntityType(
                master,
                out masterType))
            {
                return new AssignmentsCallDescriptor
                {
                    Error = BadRequest(string.Format(CultureInfo.InvariantCulture, WebResources.UnknownEntityType, master))
                };
            }

            Type relatedType;
            if (!StorableHelper.TryGetEntityType(
                related,
                out relatedType))
            {
                return new AssignmentsCallDescriptor
                {
                    Error = BadRequest(string.Format(CultureInfo.InvariantCulture, WebResources.UnknownEntityType, related))
                };
            }

            Type junctionType;
            if (!StorableHelper.TryGetJunctionTable(
                masterType,
                relatedType,
                out junctionType))
            {
                return new AssignmentsCallDescriptor
                {
                    Error = BadRequest(string.Format(CultureInfo.InvariantCulture, WebResources.NoRelation, masterType.Name, relatedType))
                };
            }

            return new AssignmentsCallDescriptor
            {
                MasterType = masterType,
                JunctionType = junctionType,
                RelatedType = relatedType
            };
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Linq")]
        private async Task<IHttpActionResult> GetJobRoleAsync(
            Type masterType,
            IMarker masterMarker,
            Type relatedType,
            Type junctionType)
        {
            var typesOrdered = new[] { masterType, relatedType }
                .OrderBy(x => x.Name)
                .ToArray();

            if (typesOrdered[0] == masterType)
            {
                return await GetJobRoleWhenFirstIsMasterAsync(masterType, masterMarker, relatedType, junctionType);
            }

            return await GetJobRoleWhenSecondIsMasterAsync(masterType, masterMarker, relatedType, junctionType);
        }

        private TableConfiguration GetJobRoleDisplayColumnsConfiguration(
            Type junctionType,
            Type relatedType)
        {
            var builder = new TableConfigurationBuilder<JobRoleAssignmentViewModel>();
            builder.AddDetailsActionColumn(relatedType, x => x.Element.Id);
            builder.AddColumn(x => x.Element.DisplayName, relatedType)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Role.DisplayName, typeof(RoleDefinition))
                .CanSort()
                .CanFilter();
            var url = new UrlHelper(Request);
            var parameters = new Dictionary<string, object>
            {
                { "controller", "AssignmentTemplates" },
                { "action", "DisplayJobKind" }
            };
            var templateAddress = url.Link(
                "mvc",
                parameters);
            builder.AddColumn(x => x.JobKind, null)
                .SetTemplateUrl(templateAddress)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.JobValue, null).CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart, junctionType)
                .CanSort();
            builder.AddColumn(x => x.DateEnd, junctionType)
                .CanSort();
            return builder.Build();
        }

        private async Task<IHttpActionResult> GetJobRoleWhenFirstIsMasterAsync(Type masterType, IMarker masterMarker, Type relatedType, Type junctionType)
        {
            var query = DatabaseAccessor.Query<IJobRoleAssignmentJunctionEntity>(junctionType)
                .Where(junction => junction.First.Id == masterMarker.Id && !junction.IsDeleted)
                .Join(
                    DatabaseAccessor.Query<RoleDefinition>().Where(x => !x.IsDeleted),
                    x => x.RoleDefinition,
                    (j, rd) => new { Junction = j, RoleDefinition = rd })
                .Join(
                    DatabaseAccessor.Query<IAssignmentRelatedEntity>(relatedType)
                        .Where(element => element.Scope.IsEquivalent(SessionDataProvider.Scope)),
                    jrd => jrd.Junction.Second,
                    r => r,
                    (jrd, r) => new JobRoleAssignmentViewModel
                    {
                        Id = jrd.Junction.Id,
                        DateStart = jrd.Junction.DateStart,
                        DateEnd = jrd.Junction.DateEnd,
                        Element = new DetailedEntityViewModel
                        {
                            Id = r.Id,
                            DateStart = r.DateStart,
                            DateEnd = r.DateEnd,
                            DisplayName = r.DisplayName
                        },
                        Role = new DetailedEntityViewModel
                        {
                            Id = jrd.RoleDefinition.Id,
                            DateStart = jrd.RoleDefinition.DateStart,
                            DateEnd = jrd.RoleDefinition.DateEnd,
                            DisplayName = jrd.RoleDefinition.DisplayName
                        },
                        JobKind = jrd.Junction.JobTime != null ? JobKind.Time :
                            (jrd.Junction.JobHours != null ? JobKind.Hours : JobKind.None),
                        JobValue = jrd.Junction.JobTime ?? jrd.Junction.JobHours
                    })
                .AsQueryable();

            return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
        }

        private async Task<IHttpActionResult> GetJobRoleWhenSecondIsMasterAsync(Type masterType, IMarker masterMarker, Type relatedType, Type junctionType)
        {
            var query = DatabaseAccessor.Query<IJobRoleAssignmentJunctionEntity>(junctionType)
                .Where(junction => junction.Second.Id == masterMarker.Id && !junction.IsDeleted)
                .Join(
                    DatabaseAccessor.Query<RoleDefinition>().Where(x => !x.IsDeleted),
                    x => x.RoleDefinition,
                    (j, rd) => new { Junction = j, RoleDefinition = rd })
                .Join(
                    DatabaseAccessor.Query<IAssignmentRelatedEntity>(relatedType)
                        .Where(element => element.Scope.IsEquivalent(SessionDataProvider.Scope)),
                    jrd => jrd.Junction.First,
                    r => r,
                    (jrd, r) => new JobRoleAssignmentViewModel
                    {
                        Id = jrd.Junction.Id,
                        DateStart = jrd.Junction.DateStart,
                        DateEnd = jrd.Junction.DateEnd,
                        Element = new DetailedEntityViewModel
                        {
                            Id = r.Id,
                            DateStart = r.DateStart,
                            DateEnd = r.DateEnd,
                            DisplayName = r.DisplayName
                        },
                        Role = new DetailedEntityViewModel
                        {
                            Id = jrd.RoleDefinition.Id,
                            DateStart = jrd.RoleDefinition.DateStart,
                            DateEnd = jrd.RoleDefinition.DateEnd,
                            DisplayName = jrd.RoleDefinition.DisplayName
                        },
                        JobKind = jrd.Junction.JobTime != null ? JobKind.Time :
                            (jrd.Junction.JobHours != null ? JobKind.Hours : JobKind.None),
                        JobValue = jrd.Junction.JobTime ?? jrd.Junction.JobHours
                    })
                .AsQueryable();

            return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Linq")]
        private async Task<IHttpActionResult> GetRoleAsync(
            Type masterType,
            IMarker masterMarker,
            Type relatedType,
            Type junctionType)
        {
            var typesOrdered = new[] { masterType, relatedType }
                .OrderBy(x => x.Name)
                .ToArray();

            if (typesOrdered[0] == masterType)
            {
                var query = DatabaseAccessor.Query<IRoleAssignmentJunctionEntity>(junctionType)
                    .Where(junction => junction.First.Id == masterMarker.Id && !junction.IsDeleted)
                    .Join(
                        DatabaseAccessor.Query<RoleDefinition>().Where(x => !x.IsDeleted),
                        x => x.RoleDefinition,
                        (j, rd) => new { Junction = j, RoleDefinition = rd })
                    .Join(
                        DatabaseAccessor.Query<IAssignmentRelatedEntity>(relatedType)
                            .Where(element => element.Scope.IsEquivalent(SessionDataProvider.Scope)),
                        jrd => jrd.Junction.Second,
                        r => r,
                        (jrd, r) => new RoleAssignmentViewModel
                        {
                            Id = jrd.Junction.Id,
                            DateStart = jrd.Junction.DateStart,
                            DateEnd = jrd.Junction.DateEnd,
                            Element = new DetailedEntityViewModel
                            {
                                Id = r.Id,
                                DateStart = r.DateStart,
                                DateEnd = r.DateEnd,
                                DisplayName = r.DisplayName
                            },
                            Role = new DetailedEntityViewModel
                            {
                                Id = jrd.RoleDefinition.Id,
                                DateStart = jrd.RoleDefinition.DateStart,
                                DateEnd = jrd.RoleDefinition.DateEnd,
                                DisplayName = jrd.RoleDefinition.DisplayName
                            }
                        })
                    .AsQueryable();

                return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
            }
            else
            {
                var query = DatabaseAccessor.Query<IRoleAssignmentJunctionEntity>(junctionType)
                    .Where(junction => junction.Second.Id == masterMarker.Id && !junction.IsDeleted)
                    .Join(
                        DatabaseAccessor.Query<RoleDefinition>().Where(x => !x.IsDeleted),
                        x => x.RoleDefinition,
                        (j, rd) => new { Junction = j, RoleDefinition = rd })
                    .Join(
                        DatabaseAccessor.Query<IAssignmentRelatedEntity>(relatedType)
                            .Where(element => element.Scope.IsEquivalent(SessionDataProvider.Scope)),
                        jrd => jrd.Junction.First,
                        r => r,
                        (jrd, r) => new RoleAssignmentViewModel
                        {
                            Id = jrd.Junction.Id,
                            DateStart = jrd.Junction.DateStart,
                            DateEnd = jrd.Junction.DateEnd,
                            Element = new DetailedEntityViewModel
                            {
                                Id = r.Id,
                                DateStart = r.DateStart,
                                DateEnd = r.DateEnd,
                                DisplayName = r.DisplayName
                            },
                            Role = new DetailedEntityViewModel
                            {
                                Id = jrd.RoleDefinition.Id,
                                DateStart = jrd.RoleDefinition.DateStart,
                                DateEnd = jrd.RoleDefinition.DateEnd,
                                DisplayName = jrd.RoleDefinition.DisplayName
                            }
                        })
                    .AsQueryable();

                return await FinalizeQueryAsync(query, () => VerifyNoContent(masterType, masterMarker));
            }
        }

        private async Task<IEnumerable<AssignmentJobRoleUdt<TElement>>> MapAsync<TElement>(
            IEnumerable<JobRoleAssignmentPersistenceViewModel> dataModel)
            where TElement : IMarker
        {
            var data = new List<AssignmentJobRoleUdt<TElement>>();

            foreach (var x in dataModel)
            {
                var item = new AssignmentJobRoleUdt<TElement>
                {
                    Id = x.Id,
                    Element = (TElement)StorableHelper.GetMarkerInstance(typeof(TElement), x.ElementId),
                    DateStart = await ConvertToDateAsync(x.DateStart.Date),
                    DateEnd = await ConvertToDateAsync(x.DateEnd),
                    JobTime = x.JobKind == JobKind.Time ? x.JobValue : null,
                    JobHours = x.JobKind == JobKind.Hours ? (int?)x.JobValue : null,
                    RoleDefinition = new RoleDefinitionMarker(x.RoleId)
                };
                data.Add(item);
            }

            return data;
        }

        private async Task<IEnumerable<AssignmentRoleUdt<TElement>>> MapAsync<TElement>(
            IEnumerable<RoleAssignmentPersistenceViewModel> dataModel)
            where TElement : IMarker
        {
            var data = new List<AssignmentRoleUdt<TElement>>();

            foreach (var x in dataModel)
            {
                var item = new AssignmentRoleUdt<TElement>
                {
                    Id = x.Id,
                    Element = (TElement)StorableHelper.GetMarkerInstance(typeof(TElement), x.ElementId),
                    DateStart = await ConvertToDateAsync(x.DateStart.Date),
                    DateEnd = await ConvertToDateAsync(x.DateEnd),
                    RoleDefinition = new RoleDefinitionMarker(x.RoleId)
                };

                data.Add(item);
            }

            return data;
        }

        private async Task<IEnumerable<AssignmentUdt<TElement>>> MapAsync<TElement>(
            IEnumerable<AssignmentPersistenceViewModel> dataModel)
            where TElement : IMarker
        {
            var data = new List<AssignmentUdt<TElement>>();

            foreach (var x in dataModel)
            {
                var item = new AssignmentUdt<TElement>
                {
                    Id = x.Id,
                    Element = (TElement)StorableHelper.GetMarkerInstance(typeof(TElement), x.ElementId),
                    DateStart = await ConvertToDateAsync(x.DateStart.Date),
                    DateEnd = await ConvertToDateAsync(x.DateEnd)
                };

                data.Add(item);
            }

            return data;
        }

        private async Task<IEnumerable<ValidationError>> PersistAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<AssignmentPersistenceViewModel> source)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
        {
            var assignments = await MapAsync<TSlave>(source);
            return await _assignmentService.UpdateAssignmentAsync(entity, assignments);
        }

        private async Task<IEnumerable<ValidationError>> PersistJobRoleAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<JobRoleAssignmentPersistenceViewModel> source)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
        {
            var assignments = await MapAsync<TSlave>(source);
            return await _assignmentService.UpdateAssignmentAsync(entity, assignments);
        }

        private async Task<IEnumerable<ValidationError>> PersistRoleAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<RoleAssignmentPersistenceViewModel> source)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
        {
            var assignments = await MapAsync<TSlave>(source);
            return await _assignmentService.UpdateAssignmentAsync(entity, assignments);
        }

        private IHttpActionResult VerifyNoContent(Type master, IMarker masterMarker)
        {
            var masterExists = DatabaseAccessor
                .Query<IVersionedEntity>(master)
                .Where(x => x.IsEquivalent<IStorable>(masterMarker) && !x.IsDeleted)
                .Any();

            if (masterExists)
            {
                return this.NoContent();
            }

            return Ok(new ValidationResult
            {
                IsValid = false,
                Errors = new[]
                {
                    new ValidationErrorViewModel
                    {
                        ErrorCode = "TargetNotFound"
                    }
                }
            });
        }
    }
}