﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Api
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes", Justification = "Base class")]
    public abstract class EditApiController<TVersionedEntity, TPersistenceModel, TEntityIndicator, TViewModel>
        : BaseApiController
        where TVersionedEntity : VersionedEntity, TEntityIndicator, new()
        where TPersistenceModel : TEntityIndicator
        where TEntityIndicator : IStorable, IMarker
        where TViewModel : class, IViewModel
    {
        protected EditApiController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<TVersionedEntity, TPersistenceModel, TEntityIndicator> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, settingService)
        {
            EntityService = entityService;
            PermissionHelper = permissionHelper;
        }

        protected IVersionedEntityService<TVersionedEntity, TPersistenceModel, TEntityIndicator> EntityService
        {
            get;
        }

        protected IPermissionHelper PermissionHelper { get; }

        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Keys.Write)]
        [HttpDelete]
        public virtual async Task<IHttpActionResult> Delete(
            [FromUri] int id)
        {
            IEnumerable<string> operationTokens;
            Request.Headers.TryGetValues("OperationToken", out operationTokens);
            if (operationTokens == null || operationTokens.Count() != 1)
            {
                return BadRequest("Operation token invalid");
            }

            var operationToken = operationTokens.Single();

            var validationErrors =
                await EntityService.DeleteAsync(GetMarker(id), operationToken);

            return ProcessValidationErrors(validationErrors);
        }

        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public virtual async Task<IHttpActionResult> Get()
        {
            var accessibleItems = await PermissionHelper.GetAccessibleItemsAsync(() => Get());
            var baseQuery = BaseQueryFromGet(accessibleItems);
            var viewModelProjection = await ProjectToViewModelAsync(baseQuery);
            var result = await FinalizeQueryAsync(viewModelProjection);
            return result;
        }

        [RequiredPermission(PermissionsCommon.Keys.Details)]
        public virtual async Task<IHttpActionResult> Get(int id)
        {
            var accessibleItems = await PermissionHelper.GetAccessibleItemsAsync(() => Get(id));
            var baseQuery = BaseQueryFromGet(accessibleItems);
            var filtered = baseQuery.Where(x => x.Id == id);
            var viewModelProjection = await ProjectToViewModelAsync(filtered);
            var result = await FinalizeModelAsync(viewModelProjection);
            return result;
        }

        [HttpPost]
        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Containers.Assignment, PermissionKeyRequestParameterName = "actionId")]
        public virtual async Task<IHttpActionResult> JunctionLock(
            int id,
            string actionId)
        {
            if (id < 1)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Type type;
            if (!StorableHelper.TryGetEntityType(actionId, out type))
            {
                return BadRequest("Unknown entity type.");
            }

            Type junctionType;
            if (StorableHelper.TryGetJunctionTable(typeof(TVersionedEntity), type, out junctionType))
            {
                var relationProperty =
                    junctionType.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)
                        .Single(x => x.PropertyType == typeof(TEntityIndicator));

                var marker = GetMarker(id);
                var operationToken = Guid.NewGuid().ToString();
                var validationErrors =
                    await EntityService.TryLockAsync(junctionType, relationProperty, marker, operationToken);

                return ProcessLockResult(validationErrors, operationToken);
            }
            else
            {
                var relationProperty =
                   type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)
                       .SingleOrDefault(x => x.PropertyType == typeof(TEntityIndicator));

                if (relationProperty == null)
                {
                    return BadRequest($"There is no relation between {type.Name} and {typeof(TVersionedEntity).Name}");
                }

                var marker = GetMarker(id);
                var operationToken = Guid.NewGuid().ToString();
                var validationErrors =
                    await EntityService.TryLockAsync(type, relationProperty, marker, operationToken);

                return ProcessLockResult(validationErrors, operationToken);
            }
        }

        [HttpPost]
        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Keys.Write)]
        public virtual async Task<IHttpActionResult> Lock(
            int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }

            var marker = GetMarker(id);
            var operationToken = Guid.NewGuid().ToString();
            var validationErrors = await EntityService.TryLockAsync(marker, operationToken);

            return ProcessLockResult(validationErrors, operationToken);
        }

        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Keys.Write)]
        [ResponseType(typeof(ValidationResult))]
        [HttpPost]
        public virtual async Task<IHttpActionResult> Post(
            TViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = await PrepareEditModelAsync(model);

            if (entity == null)
            {
                return Ok(new ValidationResult
                {
                    IsValid = false,
                    Errors = new[]
                      {
                        new ValidationErrorViewModel
                        {
                            ErrorCode = "TargetNotFound",
                            Message = ValidationErrors.TargetNotFound
                        }
                    }
                });
            }

            var validationErrors = await EntityService.SaveAsync(entity);
            return ProcessValidationErrors(validationErrors, entity);
        }

        protected virtual ITableQuery<TVersionedEntity> BaseQueryFromGet(AccessibleItems accessibleItems)
        {
            var getAll = accessibleItems.GetAll;
            var accessibleItemsValues = accessibleItems.Values;
            var query = DatabaseAccessor.Query<TVersionedEntity>()
                .Where(x => !x.IsDeleted
                            && (getAll || accessibleItemsValues.Contains(x.Id)));
            return query;
        }

        protected virtual TPersistenceModel PrepareEditModel(TViewModel model)
        {
            throw new NotImplementedException("Prepare Edit Model or Prepare Edit Model Asynchronous must be overriden");
        }

        protected virtual async Task<TPersistenceModel> PrepareEditModelAsync(TViewModel model)
        {
            await Task.Delay(0);
            return PrepareEditModel(model);
        }

        protected abstract ITableQuery<TViewModel> ProjectToViewModel(ITableQuery<TVersionedEntity> query);

        protected virtual Task<IQueryable<TViewModel>> ProjectToViewModelAsync(ITableQuery<TVersionedEntity> query)
        {
            return Task.FromResult(ProjectToViewModel(query).AsQueryable());
        }

        private static TEntityIndicator GetMarker(int id)
        {
            return StorableHelper.GetMarkerInstance<TEntityIndicator, TEntityIndicator>(id);
        }

        private IHttpActionResult ProcessLockResult(
            IEnumerable<ValidationError> validationErrors,
            string operationToken)
        {
            if (validationErrors == null || !validationErrors.Any())
            {
                return Ok(
                    new LockResultViewModel
                    {
                        OperationToken = operationToken,
                        ValidationResult = new ValidationResult
                        {
                            IsValid = true,
                            Errors = Enumerable.Empty<ValidationErrorViewModel>()
                        }
                    });
            }

            var errors = validationErrors.Select(x => new ValidationErrorViewModel
            {
                AdditionalValue = x.AdditionalValue,
                ErrorCode = x.ErrorCode,
                Message = x.ValidationMessage,
                Reference = x.Reference
            });

            return Ok(
                new LockResultViewModel
                {
                    ValidationResult = new ValidationResult
                    {
                        Errors = errors
                    }
                });
        }
    }
}