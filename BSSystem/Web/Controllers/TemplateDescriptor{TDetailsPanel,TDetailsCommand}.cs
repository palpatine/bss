﻿using System;
using System.Collections.Generic;
using SolvesIt.BSSystem.Web.Controllers.Templates.Extensions;

namespace SolvesIt.BSSystem.Web.Controllers
{
    public sealed class TemplateDescriptor<TDetailsPanel, TDetailsCommand>
        where TDetailsPanel : DetailsPanel
        where TDetailsCommand : DetailsCommand
    {
        private readonly Func<IEnumerable<TDetailsCommand>> _detailsCommands;
        private readonly Func<IEnumerable<TDetailsPanel>> _detailsPanels;

        public TemplateDescriptor(
            Func<IEnumerable<TDetailsPanel>> detailsPanels,
            Func<IEnumerable<TDetailsCommand>> detailsCommands)
        {
            _detailsPanels = detailsPanels;
            _detailsCommands = detailsCommands;
        }

        public IEnumerable<TDetailsCommand> DetailsCommands => _detailsCommands();

        public IEnumerable<TDetailsPanel> DetailsPanels => _detailsPanels();
    }
}