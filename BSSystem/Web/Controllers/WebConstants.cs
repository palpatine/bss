﻿using System;

namespace SolvesIt.BSSystem.Web.Controllers
{
    internal static class WebConstants
    {
        public const string UserSessionCacheKey = "UserSession_";
        public static DateTime MaximalAccepableDate { get; } = new DateTime(2079, 6, 6);
        public static DateTime MinimalAccepableDate { get; } = new DateTime(1900, 1, 1);
    }
}