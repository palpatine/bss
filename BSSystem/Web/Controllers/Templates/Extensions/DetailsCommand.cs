﻿using System;

namespace SolvesIt.BSSystem.Web.Controllers.Templates.Extensions
{
    public abstract class DetailsCommand
    {
        public Func<string> ExecuteMethodFactory { get; set; }

        public Func<string> Name { get; set; }

        public string TypescriptController { get; set; }
    }
}