﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Routing;

namespace SolvesIt.BSSystem.Web.Controllers.Templates.Extensions
{
    public abstract class DetailsPanel
    {
        public string Action { get; set; }

        public string Controller { get; set; }

        public string DisplayPermission { get; set; }

        public string EditPermission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public RouteValueDictionary RouteValues { get; set; }

        public string TypescriptController { get; set; }

        public IEnumerable<string> Modules { get; set; }
    }
}