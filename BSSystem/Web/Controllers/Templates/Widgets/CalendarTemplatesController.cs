﻿using System.Web.Mvc;

namespace SolvesIt.BSSystem.Web.Controllers.Templates.Widgets
{
    public sealed class CalendarTemplatesController : Controller
    {
        public PartialViewResult WeeklyViewAbbreviatedColumnHeader()
        {
            return PartialView();
        }

        public PartialViewResult WeeklyViewColumnHeader()
        {
            return PartialView();
        }
    }
}