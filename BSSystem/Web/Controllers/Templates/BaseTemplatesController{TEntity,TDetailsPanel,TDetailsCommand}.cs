﻿using System;
using System.Globalization;
using System.Web.Mvc;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Templates.Extensions;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Templates;

namespace SolvesIt.BSSystem.Web.Controllers.Templates
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes", Justification = "Base class")]
    public abstract class BaseTemplatesController<TEntity, TDetailsPanel, TDetailsCommand> : Controller
        where TEntity : Entity
        where TDetailsPanel : DetailsPanel
        where TDetailsCommand : DetailsCommand
    {
        private readonly string _singularEntities;
        private readonly string _singularEntitiesPascalCase;
        private readonly string _createPermission;

        protected BaseTemplatesController(
            TemplateDescriptor<TDetailsPanel, TDetailsCommand> descriptor,
            string createPermission)
            : this(descriptor, typeof(TEntity).Name, createPermission)
        {
        }

        protected BaseTemplatesController(
            TemplateDescriptor<TDetailsPanel, TDetailsCommand> descriptor,
            string singularEntitiesPascalCase,
            string createPermission)
        {
            Descriptor = descriptor;
            _singularEntitiesPascalCase = singularEntitiesPascalCase;
            _createPermission = createPermission;
            _singularEntities = char.ToLower(_singularEntitiesPascalCase[0], CultureInfo.InvariantCulture) + _singularEntitiesPascalCase.Substring(1);
        }

        public TemplateDescriptor<TDetailsPanel, TDetailsCommand> Descriptor { get; }

        public PartialViewResult DataCreator()
        {
            var model = new DataCreatorTemplateModel
            {
                Entities = _singularEntitiesPascalCase,
                TypescriptController = _singularEntities + "DetailsCreatorController",
                Permission = _createPermission
            };

            return PartialView(model);
        }

        public PartialViewResult DataEditor()
        {
            return PartialView();
        }

        public PartialViewResult Details()
        {
            var detailsModel = new DetailsTemplateModel
            {
                Panels = Descriptor.DetailsPanels,
                Entities = _singularEntitiesPascalCase,
                Commands = Descriptor.DetailsCommands,
            };

            return PartialView(detailsModel);
        }

        public PartialViewResult DetailsDisplayer()
        {
            return PartialView();
        }

        public PartialViewResult DetailsPanel()
        {
            var controller = GetType().Name.Replace("Controller", string.Empty);
            var entityQualifiedName = StorableHelper.GetQualifiedEntityName<TEntity>();

            var model = new DetailsPanelTemplateModel
            {
                EditAction = ExpressionExtensions.GetMethod(() => DataEditor()).Name,
                DetailsAction = ExpressionExtensions.GetMethod(() => DetailsDisplayer()).Name,
                Controller = controller,
                EditTypescriptController = _singularEntities + "DetailsEditorController",
                DetailsTypescriptController = _singularEntities + "DetailsDisplayerController",
                Entities = _singularEntitiesPascalCase,
                DisplayPermissionPath = string.Format(CultureInfo.InvariantCulture, "{0}|Details", entityQualifiedName),
                EditPermissionPath = string.Format(CultureInfo.InvariantCulture, "{0}|Details|Write", entityQualifiedName)
            };

            return PartialView(model);
        }

        public PartialViewResult List()
        {
            var area = (string)Request.RequestContext.RouteData.DataTokens["area"];
            area = char.ToUpper(area[0], CultureInfo.InvariantCulture) + area.Substring(1);
            var dataSourceParameters =
                new
                {
                    controller = _singularEntitiesPascalCase,
                    action = "Get"
                };
            var columnsSoruceParameters =
                new
                {
                    action = "ListColumnsConfiguration",
                    controller = _singularEntitiesPascalCase
                };
            var model = new EntitiesListTemplateViewModel
            {
                Entities = _singularEntitiesPascalCase,
                DataSource = @Url.HttpRouteUrl(
                    area + "CollectionApi",
                    dataSourceParameters),
                ColumnsSource = @Url.HttpRouteUrl(
                    area + "CollectionApi",
                    columnsSoruceParameters),
            };

            return PartialView(model);
        }

        public PartialViewResult ValidationMessages()
        {
            return PartialView();
        }

        protected PartialViewResult BreadcrumbControlPanel(BreadcrumbControlPanelTemplateModel model)
        {
            return PartialView("BreadcrumbControlPanel", model);
        }
    }
}