using System;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Controllers.Templates.Extensions;

namespace SolvesIt.BSSystem.Web.Controllers.Templates
{
    public abstract class TemplatesExtensionsBootstrapper : IBootstrapper
    {
        private readonly string _area;

        private readonly PluralizationService _pluralizationService;

        private IRegistrar _registrar;

        protected TemplatesExtensionsBootstrapper()
        {
            _pluralizationService = PluralizationService
                .CreateService(new CultureInfo("en-US"));

            _area = Regex.Match(GetType().Namespace, @"SolvesIt\.BSSystem\.Web\.Areas.(.*?)\.Extensions").Groups[1].Value;
        }

        public double Order => 0;

        public void Bootstrap(IRegistrar registrar)
        {
            _registrar = registrar;
            CreateDetailsPanels();
            CreateDetailsCommands();
        }

        protected void Create<TDetailsPanel, TController>(
            Expression<Func<TController, ActionResult>> template,
            string displayPermission = null,
            string editPermission = null,
            bool useTypescriptController = true)
            where TDetailsPanel : DetailsPanel, new()
            where TController : Controller
        {
            var panel = new TDetailsPanel();
            panel.Action = template.GetMethod().Name;
            panel.Controller = typeof(TController).Name.Replace("Controller", string.Empty);
            panel.RouteValues = new RouteValueDictionary();
            panel.RouteValues.Add("area", _area);
            panel.DisplayPermission = displayPermission;
            panel.EditPermission = editPermission;
            if (useTypescriptController)
            {
                panel.TypescriptController = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0}{1}{2}Controller",
                    char.ToLower(typeof(TDetailsPanel).Name[0], CultureInfo.InvariantCulture),
                    typeof(TDetailsPanel).Name.Substring(1, typeof(TDetailsPanel).Name.IndexOf("DetailsPanel", StringComparison.Ordinal) - 1),
                    panel.Action);
            }

            _registrar.RegisterMultiple(panel);
        }

        protected void CreateApiCallCommand<TDetailsCommand>(
            Func<UrlHelper, string> apiCall,
            Func<string> name)
          where TDetailsCommand : DetailsCommand, new()
        {
            var command = new TDetailsCommand
            {
                TypescriptController = "apiExecutingController",
                ExecuteMethodFactory = () => string.Format(CultureInfo.InvariantCulture, "execute({0})", apiCall(new UrlHelper(HttpContext.Current.Request.RequestContext))),
                Name = name
            };

            _registrar.RegisterMultiple(command);
        }

        protected void CreateAssignmentDetailsPanel<TDetailsPanel, TEntityIndicator, TRelatedEntityIndicator>()
            where TDetailsPanel : DetailsPanel, new()
            where TEntityIndicator : IMarker
            where TRelatedEntityIndicator : IMarker
        {
            var masterName = typeof(TEntityIndicator).Name.Substring(1);
            var panel = new TDetailsPanel();
            var controller = typeof(AssignmentTemplatesController).Name.Replace("Controller", string.Empty);
            panel.Action =
                ExpressionExtensions.GetMethod(
                    (AssignmentTemplatesController x) => x.RelationDetailPanel(null, null))
                    .Name;

            panel.RouteValues = new RouteValueDictionary();
            panel.RouteValues.Add("entities", StorableHelper.GetQualifiedEntityName<TEntityIndicator>());
            panel.RouteValues.Add("elements", StorableHelper.GetQualifiedEntityName<TRelatedEntityIndicator>());
            panel.RouteValues.Add("area", string.Empty);
            panel.Controller = controller;

            var typescriptController = string.Format(
                CultureInfo.InvariantCulture,
                "{0}{1}{2}EditorController",
                char.ToLower(masterName[0], CultureInfo.InvariantCulture),
                masterName.Substring(1),
                _pluralizationService.Pluralize(typeof(TRelatedEntityIndicator).Name.Substring(1)));
            panel.TypescriptController = typescriptController;
            var entityQualifiedName = StorableHelper.GetQualifiedEntityName<TEntityIndicator>();
            panel.DisplayPermission = string.Format(
                CultureInfo.InvariantCulture,
                "{0}|Details",
                entityQualifiedName);
            panel.EditPermission = string.Format(
                CultureInfo.InvariantCulture,
                "{0}|Details|Assignment|{1}",
                entityQualifiedName,
                StorableHelper.GetQualifiedEntityName<TRelatedEntityIndicator>());
            panel.Modules = new[]
            {
                StorableHelper.GetSchemaName(typeof(TEntityIndicator)),
                StorableHelper.GetSchemaName(typeof(TRelatedEntityIndicator))
            };
            _registrar.RegisterMultiple(panel);
        }

        protected virtual void CreateDetailsCommands()
        {
        }

        protected virtual void CreateDetailsPanels()
        {
        }
    }
}