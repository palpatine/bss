﻿using System;
using System.Web.Mvc;

namespace SolvesIt.BSSystem.Web.Controllers.Templates.Core
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class DefaultActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var url = filterContext.HttpContext.Request.Url.ToString();
            if (!url.EndsWith("/", StringComparison.Ordinal))
            {
                filterContext.Result = new RedirectResult(url + "/");
            }
        }
    }
}