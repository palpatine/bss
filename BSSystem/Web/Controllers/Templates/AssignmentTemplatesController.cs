﻿using System;
using System.Globalization;
using System.Net;
using System.Web.Mvc;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Templates
{
    public sealed class AssignmentTemplatesController : Controller
    {
        public ActionResult RelationDetailPanel(
            string entities,
            string elements)
        {
            Type masterType;
            if (!StorableHelper.TryGetEntityType(
                entities,
                out masterType))
            {
                return new HttpStatusCodeResult(
                    HttpStatusCode.BadRequest,
                    string.Format(CultureInfo.InvariantCulture, "Unknown entity type {0}.", entities));
            }

            Type relatedType;
            if (!StorableHelper.TryGetEntityType(
                elements,
                out relatedType))
            {
                return new HttpStatusCodeResult(
                        HttpStatusCode.BadRequest,
                        string.Format(CultureInfo.InvariantCulture, "Unknown entity type {0}.", elements));
            }

            Type junctionType;
            if (!StorableHelper.TryGetJunctionTable(
                masterType,
                relatedType,
                out junctionType))
            {
                return new HttpStatusCodeResult(
                        HttpStatusCode.BadRequest,
                        string.Format(CultureInfo.InvariantCulture, "There is no relation between {0} and {1}.", masterType.Name, relatedType));
            }

            var canAssignRole = typeof(IRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType);
            var canManageTime = typeof(IRoleAssignmentJunctionEntity).IsAssignableFrom(junctionType);

            var entitiesName = masterType.Name;
            var elementsName = relatedType.Name;

            var model = CreateRelationModel(entitiesName, elementsName, canAssignRole, canManageTime);
            return PartialView("RelationDetailPanel", model);
        }

        public PartialViewResult DisplayJobKind()
        {
            return PartialView();
        }

        public PartialViewResult ValidationErrors(string id)
        {
            return PartialView((object)id);
        }

        public PartialViewResult ValidationMessages()
        {
            return PartialView();
        }

        private static RelationDetailsPanelTemplateViewModel CreateRelationModel(
            string entitites,
            string elements,
            bool canAssignRole,
            bool canManageTime)
        {
            var model = new RelationDetailsPanelTemplateViewModel
            {
                CanAssignJobTime = canManageTime,
                CanAssignRole = canAssignRole,
                Entities = entitites,
                Elements = elements
            };
            return model;
        }
    }
}