﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Practices.Unity;

namespace SolvesIt.BSSystem.Web.Controllers
{
    /// <summary>
    ///     Represents the custom controller factory that use Ioc to create instance.
    /// </summary>
    public class ControllerFactory : DefaultControllerFactory
    {
        private readonly IUnityContainer _container;

        public ControllerFactory(IUnityContainer container)
        {
            _container = container;
        }

        /// <summary>
        ///     Overrides the GetControllerInstance method to return a Controller instance using IoC.
        /// </summary>
        /// <param name="requestContext">The current web context.</param>
        /// <param name="controllerType">Type of controller to return.</param>
        /// <returns>Controller of the supplied type.</returns>
        protected override IController GetControllerInstance(
            RequestContext requestContext,
            Type controllerType)
        {
            var controller = (controllerType == null)
                ? base.GetControllerInstance(requestContext, controllerType)
                : (IController)_container.Resolve(controllerType);
            return controller;
        }
    }
}