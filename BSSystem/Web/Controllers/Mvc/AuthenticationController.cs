﻿using System.Web.Mvc;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Mvc
{
    public sealed class AuthenticationController : Controller
    {
        [SslActionFilter]
        public ActionResult LogOn()
        {
            var viewModel = new AuthenticationViewModel();
            return View(viewModel);
        }

        [SslActionFilter]
        public ActionResult LogOnPanel()
        {
            var viewModel = new AuthenticationViewModel();
            return PartialView(viewModel);
        }

        public ActionResult ValidationMessages()
        {
            return PartialView();
        }
    }
}