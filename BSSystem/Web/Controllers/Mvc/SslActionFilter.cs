using System;
using System.Web.Mvc;

namespace SolvesIt.BSSystem.Web.Controllers.Mvc
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class SslActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsSecureConnection)
            {
                var url = filterContext.HttpContext.Request.Url.ToString().Replace("http:", "https:");
                filterContext.Result = new RedirectResult(url);
            }
        }
    }
}