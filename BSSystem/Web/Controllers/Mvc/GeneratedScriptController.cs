﻿using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Mvc
{
    [AuthorizationRequiredMvc]
    public sealed class GeneratedScriptController : Controller
    {
        private readonly IMenuService _menuService;

        public GeneratedScriptController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        public async Task<JavaScriptResult> GeneratedScript()
        {
#if DEBUG
            ViewBag.IsDebug = true;
#else
            ViewBag.IsDebug = false;
#endif

            var menuPositions = (await _menuService.GetCurrentUserMenu()).ToArray();
            var sections = menuPositions.Where(x => x.Id > 0)
                .GroupBy(x => x.ParentId)
                .ToDictionary(
                        x => x.Key ?? 0,
                        x => x.Select(item => new MenuSectionViewModel
                        {
                            Id = item.Id,
                            Key = item.Key,
                        }).ToArray());

            foreach (var item in sections.Values.SelectMany(x => x))
            {
                if (sections.ContainsKey(item.Id))
                {
                    item.MenuSections = sections[item.Id];
                }
            }

            var script = RenderRazorViewToString("GeneratedScript", sections[0]);
            return JavaScript(script);
        }

        public string RenderRazorViewToString(
            string viewName,
            object model)
        {
            ViewData.Model = model;
            using (var writer = new StringWriter(CultureInfo.InvariantCulture))
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, writer);
                viewResult.View.Render(viewContext, writer);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return writer.GetStringBuilder().ToString();
            }
        }
    }
}