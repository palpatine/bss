﻿using System.Web.Mvc;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Templates.Core;

namespace SolvesIt.BSSystem.Web.Controllers.Mvc
{
    public sealed class HomeController : Controller
    {
        [AuthorizationRequiredMvc]
        [DefaultActionFilter]
        public ActionResult Index()
        {
            return View();
        }
    }
}