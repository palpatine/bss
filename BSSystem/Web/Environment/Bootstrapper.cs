﻿using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Notification;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Configuration;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;

namespace SolvesIt.BSSystem.Web.Environment
{
    public sealed class Bootstrapper : IBootstrapper
    {
        public double Order => 0;

        public void Bootstrap(IRegistrar registrar)
        {
            registrar.RegisterAsSingleton<IConfigurationProvider, ConfigurationProvider>();
            registrar.RegisterSingle<IAuthenticationHelper, AuthenticationHelper>();
            registrar.RegisterAsPerHttpRequest<ISessionDataProvider, SessionDataProvider>();
            registrar.RegisterSingle<IAuthenticationValidator, AuthenticationValidator>();
            registrar.RegisterSingle<IGlobalMessageParametersProvider, GlobalMessageParametersProvider>();
            var configurationProvider = registrar.Resolver.Resolve<IConfigurationProvider>();
            if (configurationProvider.IsCacheEnabled)
            {
                registrar.RegisterSingle<ICache, Cache>();
            }
            else
            {
                registrar.RegisterSingle<ICache, NoCache>();
            }

            registrar.RegisterSingle<IQueryStringParser, QueryStringParser>();
            registrar.RegisterSingle<IPermissionHelper, PermissionHelper>();
        }
    }
}