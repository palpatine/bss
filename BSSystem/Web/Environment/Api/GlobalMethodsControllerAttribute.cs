using System;

namespace SolvesIt.BSSystem.Web.Environment.Api
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class GlobalMethodsControllerAttribute : Attribute
    {
    }
}