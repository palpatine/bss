﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace SolvesIt.BSSystem.Web.Environment.Api
{
    public class HybridActionSelector : ApiControllerActionSelector
    {
        public override HttpActionDescriptor SelectAction(HttpControllerContext controllerContext)
        {
            object actionName, subactionName;
            bool hasActionName;
            bool hasSubActionName;

            if (controllerContext.ControllerDescriptor.ControllerType
                .GetCustomAttribute<GlobalMethodsControllerAttribute>() != null)
            {
                hasActionName = controllerContext.RouteData.Values.TryGetValue("controller", out actionName);
                hasSubActionName = controllerContext.RouteData.Values.TryGetValue("action", out subactionName);
            }
            else
            {
                hasActionName = controllerContext.RouteData.Values.TryGetValue("action", out actionName);
                hasSubActionName = controllerContext.RouteData.Values.TryGetValue("subaction", out subactionName);
            }

            var actionParams = new Dictionary<ReflectedHttpActionDescriptor, string[]>();

            var method = controllerContext.Request.Method;
            var allMethods = controllerContext.ControllerDescriptor.ControllerType.GetMethods(BindingFlags.Instance | BindingFlags.Public);
            var validMethods = Array.FindAll(allMethods, IsValidActionMethod);
            var actionDescriptors = GetActionDescriptors(controllerContext, validMethods, actionParams);
            return FindAction(controllerContext, method, actionDescriptors, actionParams, hasSubActionName, subactionName, hasActionName, actionName);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "WebApi will dispose of error response.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Net.Http.HttpRequestMessageExtensions.CreateErrorResponse(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,System.String)", Justification = "Generic API error")]
        private static HttpActionDescriptor FindAction(
            HttpControllerContext controllerContext,
            HttpMethod method,
            IEnumerable<ReflectedHttpActionDescriptor> actionDescriptors,
            IDictionary<ReflectedHttpActionDescriptor, string[]> actionParams,
            bool hasSubActionName,
            object subactionName,
            bool hasActionName,
            object actionName)
        {
            IEnumerable<ReflectedHttpActionDescriptor> actionsFoundSoFar;

            if (hasSubActionName)
            {
                actionsFoundSoFar =
                    actionDescriptors.Where(
                        i =>
                            string.Equals(i.ActionName, subactionName.ToString(), StringComparison.OrdinalIgnoreCase) &&
                            i.SupportedHttpMethods.Contains(method)).ToArray();
            }
            else if (hasActionName)
            {
                actionsFoundSoFar =
                    actionDescriptors.Where(
                        i =>
                            i.SupportedHttpMethods.Contains(method)
                            &&
                            (string.Equals(i.ActionName, method.ToString() + actionName.ToString(), StringComparison.OrdinalIgnoreCase)
                             || string.Equals(i.ActionName, actionName.ToString(), StringComparison.OrdinalIgnoreCase)))
                    .ToArray();
            }
            else
            {
                actionsFoundSoFar =
                    actionDescriptors.Where(
                        i =>
                            string.Equals(i.ActionName, method.ToString(), StringComparison.OrdinalIgnoreCase) && i.SupportedHttpMethods.Contains(method)).ToArray();
            }

            var actionsFound =
                FindActionUsingRouteAndQueryParameters(actionParams, controllerContext, actionsFoundSoFar).ToArray();

            if (actionsFound == null || !actionsFound.Any())
            {
                throw new HttpResponseException(controllerContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "Cannot find a matching action."));
            }

            if (actionsFound.Count() > 1)
            {
                throw new HttpResponseException(controllerContext.Request.CreateErrorResponse(HttpStatusCode.Ambiguous, "Multiple matches found."));
            }

            return actionsFound.FirstOrDefault();
        }

        private static IEnumerable<ReflectedHttpActionDescriptor> GetActionDescriptors(
            HttpControllerContext controllerContext,
            IEnumerable<MethodInfo> validMethods,
            IDictionary<ReflectedHttpActionDescriptor, string[]> actionParams)
        {
            var actionDescriptors = new HashSet<ReflectedHttpActionDescriptor>();

            foreach (
                var actionDescriptor in
                    validMethods.Select(m => new ReflectedHttpActionDescriptor(controllerContext.ControllerDescriptor, m)))
            {
                actionDescriptors.Add(actionDescriptor);
                var value =
                    actionDescriptor.ActionBinding.ParameterBindings
                        .Where(b => !b.Descriptor.IsOptional && b.Descriptor.ParameterType.UnderlyingSystemType.IsPrimitive)
                        .Select(b => b.Descriptor.Prefix ?? b.Descriptor.ParameterName)
                        .ToArray();
                actionParams.Add(actionDescriptor, value);
            }

            return actionDescriptors;
        }

        private static bool IsValidActionMethod(MethodInfo methodInfo)
        {
            if (methodInfo.IsSpecialName)
            {
                return false;
            }

            return !methodInfo.GetBaseDefinition().DeclaringType.IsAssignableFrom(typeof(ApiController));
        }

        private static IEnumerable<ReflectedHttpActionDescriptor> FindActionUsingRouteAndQueryParameters(
            IDictionary<ReflectedHttpActionDescriptor, string[]> actionParams,
            HttpControllerContext controllerContext,
            IEnumerable<ReflectedHttpActionDescriptor> actionsFound)
        {
            var routeParameterNames = new HashSet<string>(controllerContext.RouteData.Values.Keys, StringComparer.OrdinalIgnoreCase);

            if (routeParameterNames.Contains("controller"))
            {
                routeParameterNames.Remove("controller");
            }

            if (routeParameterNames.Contains("action"))
            {
                routeParameterNames.Remove("action");
            }

            if (routeParameterNames.Contains("subaction"))
            {
                routeParameterNames.Remove("subaction");
            }

            var hasQueryParameters = controllerContext.Request.RequestUri != null && !string.IsNullOrEmpty(controllerContext.Request.RequestUri.Query);
            var hasRouteParameters = routeParameterNames.Count != 0;

            if (hasRouteParameters || hasQueryParameters)
            {
                var combinedParameterNames = new HashSet<string>(routeParameterNames, StringComparer.OrdinalIgnoreCase);
                if (hasQueryParameters)
                {
                    foreach (var queryNameValuePair in controllerContext.Request.GetQueryNameValuePairs())
                    {
                        combinedParameterNames.Add(queryNameValuePair.Key);
                    }
                }

                actionsFound = actionsFound.Where(descriptor => actionParams[descriptor].All(combinedParameterNames.Contains)).ToArray();

                if (actionsFound.Count() > 1)
                {
                    actionsFound = actionsFound
                        .GroupBy(descriptor => actionParams[descriptor].Length)
                        .OrderByDescending(g => g.Key)
                        .First()
                        .ToArray();
                }
            }
            else
            {
                actionsFound = actionsFound.Where(descriptor => actionParams[descriptor].Length == 0).ToArray();
            }

            return actionsFound;
        }
    }
}