using System;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Core.Abstraction.Environment;

namespace SolvesIt.BSSystem.Web.Environment
{
    internal sealed class NoCache : ICache
    {
        public TValue GetGlobalCacheValue<TValue>(
            string cacheId,
            Func<TValue> getItemCallback,
            TimeSpan duration)
            where TValue : class
        {
            return getItemCallback();
        }

        public TValue GetUsersCacheValue<TValue>(
            string cacheId,
            Func<TValue> getItemCallback,
            TimeSpan duration)
            where TValue : class
        {
            return getItemCallback();
        }

        public async Task<TValue> GetUsersCacheValueAsync<TValue>(
            string cacheId,
            Func<Task<TValue>> getItemCallback,
            TimeSpan duration)
            where TValue : class
        {
            return await getItemCallback();
        }

        public void RemoveUserCacheValue(string cacheId)
        {
        }

        public void RemoveGlobalCacheValue(string cacheId)
        {
        }
    }
}