using System;
using System.Threading.Tasks;
using System.Web;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.Web.Environment
{
    internal sealed class Cache : ICache
    {
        private readonly ISessionDataProvider _sessionDataProvider;

        public Cache(ISessionDataProvider sessionDataProvider)
        {
            _sessionDataProvider = sessionDataProvider;
        }

        public void RemoveGlobalCacheValue(string cacheId)
        {
            HttpRuntime.Cache.Remove(cacheId);
        }

        public void RemoveUserCacheValue(string cacheId)
        {
            RemoveGlobalCacheValue(GetUserChacheId(cacheId));
        }

        public TValue GetGlobalCacheValue<TValue>(
            string cacheId,
            Func<TValue> getItemCallback,
            TimeSpan duration)
            where TValue : class
        {
            var item = (TValue)HttpRuntime.Cache.Get(cacheId);

            if (item == null)
            {
                item = getItemCallback();
                if (item != null)
                {
                    HttpContext.Current.Cache.Insert(
                        cacheId,
                        item,
                        null,
                        DateTime.UtcNow.Add(duration),
                        System.Web.Caching.Cache.NoSlidingExpiration);
                }
            }

            return item;
        }

        public async Task<TValue> GetGlobalCacheValueAsync<TValue>(
            string cacheId,
            Func<Task<TValue>> getItemCallback,
            TimeSpan duration)
            where TValue : class
        {
            var item = (TValue)HttpRuntime.Cache.Get(cacheId);

            if (item == null)
            {
                item = await getItemCallback();
                if (item != null)
                {
                    HttpContext.Current.Cache.Insert(
                        cacheId,
                        item,
                        null,
                        DateTime.UtcNow.Add(duration),
                        System.Web.Caching.Cache.NoSlidingExpiration);
                }
            }

            return item;
        }

        public TValue GetUsersCacheValue<TValue>(
            string cacheId,
            Func<TValue> getItemCallback,
            TimeSpan duration)
            where TValue : class
        {
            return GetGlobalCacheValue(GetUserChacheId(cacheId), getItemCallback, duration);
        }

        public async Task<TValue> GetUsersCacheValueAsync<TValue>(
            string cacheId,
            Func<Task<TValue>> getItemCallback,
            TimeSpan duration)
            where TValue : class
        {
            return await GetGlobalCacheValueAsync(GetUserChacheId(cacheId), getItemCallback, duration);
        }

        private string GetUserChacheId(string cacheId)
        {
            var sessionId = _sessionDataProvider.Session.Id;
            return "sessionId:" + sessionId + "|" + cacheId;
        }
    }
}