using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Irony;
using Irony.Parsing;

namespace SolvesIt.BSSystem.Web.Environment.QueryStringParsing
{
    public class DateLiteral : Terminal
    {
        private char[] _stopChars;

        public DateLiteral(string name)
            : base(name)
        {
            Priority = TerminalPriority.High;
        }

        public override IList<string> GetFirsts()
        {
            return null;
        }

        public override void Init(GrammarData grammarData)
        {
            var numbers = Enumerable.Range(0, 10).Select(x => x.ToString(CultureInfo.InvariantCulture)[0]).ToArray();
            base.Init(grammarData);
            var stopCharSet = new CharHashSet();
            foreach (var term in grammarData.Terminals)
            {
                var firsts = term.GetFirsts();
                if (firsts == null)
                {
                    continue;
                }

                foreach (var first in firsts)
                {
                    if (!string.IsNullOrEmpty(first))
                    {
                        stopCharSet.Add(first[0]);
                    }
                }
            }

            _stopChars = stopCharSet.Except(numbers).ToArray();
        }

        public override Token TryMatch(
            ParsingContext context,
            ISourceStream source)
        {
            var stopIndex = source.Text.IndexOfAny(_stopChars, source.Location.Position + 1);
            if ((stopIndex == -1 && source.Text.Length < source.Location.Position + 10) ||
                (stopIndex != -1 && stopIndex < source.Location.Position + 10))
            {
                return null;
            }

            var text = source.Text.Substring(source.Location.Position, Math.Min(24, source.Text.Length - source.Location.Position));
            var match = Regex.Match(text, "^\\d{4}-\\d{2}-\\d{2}(T\\d\\d:\\d\\d:\\d\\d\\.\\d\\d\\dZ)?");
            if (match.Success)
            {
                source.PreviewPosition = source.Location.Position + match.Value.Length;
            }

            return source.CreateToken(OutputTerminal);
        }
    }
}