using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;

namespace SolvesIt.BSSystem.Web.Environment.QueryStringParsing
{
    public static class LinqRuntimeTypeBuilder
    {
        private static readonly AssemblyName AssemblyName = new AssemblyName() { Name = "DynamicLinqTypes" };
        private static readonly Dictionary<string, Type> BuiltTypes = new Dictionary<string, Type>();

        private static readonly ModuleBuilder ModuleBuilder =
                Thread.GetDomain()
                    .DefineDynamicAssembly(AssemblyName, AssemblyBuilderAccess.Run)
                    .DefineDynamicModule(AssemblyName.Name);

        public static Type GetDynamicType(Dictionary<string, Type> fields)
        {
            if (fields == null)
            {
                throw new ArgumentNullException(nameof(fields));
            }

            if (fields.Count == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(fields));
            }

            try
            {
                Monitor.Enter(BuiltTypes);
                string className = GetTypeKey(fields);

                if (BuiltTypes.ContainsKey(className))
                {
                    return BuiltTypes[className];
                }

                TypeBuilder typeBuilder = ModuleBuilder.DefineType(
                    className,
                    TypeAttributes.Public | TypeAttributes.Class | TypeAttributes.Serializable);

                foreach (var field in fields)
                {
                    var filed = typeBuilder.DefineField("_" + field.Key, field.Value, FieldAttributes.Private);
                    var propertyBuilder = typeBuilder.DefineProperty(
                        field.Key,
                        PropertyAttributes.None,
                        CallingConventions.HasThis,
                        field.Value,
                        Type.EmptyTypes);

                    var attributes =
                        MethodAttributes.Public |
                        MethodAttributes.HideBySig |
                        MethodAttributes.SpecialName;

                    MethodBuilder getMethodBuilder = typeBuilder.DefineMethod("get_" + field.Key, attributes, CallingConventions.HasThis, field.Value, Type.EmptyTypes);
                    ILGenerator getter = getMethodBuilder.GetILGenerator();
                    getter.Emit(OpCodes.Ldarg_0);
                    getter.Emit(OpCodes.Ldfld, filed);
                    getter.Emit(OpCodes.Ret);
                    propertyBuilder.SetGetMethod(getMethodBuilder);

                    MethodBuilder setMethodBuilder = typeBuilder.DefineMethod("set_" + field.Key, attributes, CallingConventions.HasThis, null, new[] { field.Value });
                    ILGenerator setter = setMethodBuilder.GetILGenerator();
                    setter.Emit(OpCodes.Ldarg_0);
                    setter.Emit(OpCodes.Ldarg_1);
                    setter.Emit(OpCodes.Stfld, filed);
                    setter.Emit(OpCodes.Ret);
                    propertyBuilder.SetSetMethod(setMethodBuilder);
                }

                BuiltTypes[className] = typeBuilder.CreateType();

                return BuiltTypes[className];
            }
            finally
            {
                Monitor.Exit(BuiltTypes);
            }
        }

        public static Type GetDynamicType(IEnumerable<PropertyInfo> fields)
        {
            return GetDynamicType(fields.ToDictionary(f => f.Name, f => f.PropertyType));
        }

        private static string GetTypeKey(Dictionary<string, Type> fields)
        {
            return fields
                .OrderBy(x => x.Key)
                .ThenBy(x => x.Value.FullName)
                .Aggregate(string.Empty, (current, field) => current + (field.Key + ";" + field.Value.FullName + "_"));
        }
    }
}