﻿using System.Collections.Generic;
using Irony;

namespace SolvesIt.BSSystem.Web.Environment.QueryStringParsing
{
    public sealed class QueryMetadata
    {
        public QueryMetadata()
        {
            IsValid = true;
        }

        public bool IsFiltered { get; set; }

        public bool IsOrdered { get; set; }

        public bool IsProjected { get; set; }

        public bool IsSkipped { get; set; }

        public bool IsTopped { get; set; }

        public bool IsValid { get; set; }

        public IEnumerable<LogMessage> Messages { get; set; }

        public int SkipValue { get; set; }

        public int TopValue { get; set; }

        public bool IncludeInlineCount { get; set; }
    }
}