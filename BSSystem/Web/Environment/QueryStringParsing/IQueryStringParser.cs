using System.Linq;

namespace SolvesIt.BSSystem.Web.Environment.QueryStringParsing
{
    public interface IQueryStringParser
    {
        IQueryable Apply<TInput>(
            IQueryable<TInput> query,
            string queryString);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#", Justification = "There is an overload without out parameter")]
        IQueryable Apply<TInput>(
            IQueryable<TInput> query,
            string queryString,
            out QueryMetadata queryMetadata);
    }
}