﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Irony.Parsing;
using Qdarc.Modules.Common.Utils;

namespace SolvesIt.BSSystem.Web.Environment.QueryStringParsing
{
    internal sealed class QueryStringParser : IQueryStringParser
    {
        private readonly QueryStringGrammar _queryStringGrammar;

        private QueryMetadata _queryMetadata;

        public QueryStringParser()
        {
            _queryStringGrammar = new QueryStringGrammar();
        }

        public IQueryable Apply<TInput>(
            IQueryable<TInput> query,
            string queryString)
        {
            QueryMetadata queryMetadata;
            return Apply(query, queryString, out queryMetadata);
        }

        public IQueryable Apply<TInput>(
            IQueryable<TInput> query,
            string queryString,
            out QueryMetadata queryMetadata)
        {
            _queryMetadata = queryMetadata = new QueryMetadata();
            var tree = Parse(queryString);

            if (tree == null)
            {
                return null;
            }

            var expression = Visit(query.Expression, tree.Root);

            return query.Provider.CreateQuery(expression);
        }

        private static Expression VisitDateLiteral(ParseTreeNode node)
        {
            return Expression.Constant(new DateTimeOffset(DateTimeOffset.Parse((string)node.Token.Value, CultureInfo.InvariantCulture).Ticks, default(TimeSpan)));
        }

        private static Expression VisitFalseConstant()
        {
            return Expression.Constant(false);
        }

        private static Expression VisitIdentifierBase(
            Expression expression,
            ParseTreeNode node)
        {
            var propertyInfo = expression.Type.GetProperties()
                .SingleOrDefault(x => x.Name.Equals(node.Token.Text, StringComparison.OrdinalIgnoreCase));
            if (propertyInfo == null)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Property {0} not found on {1}", node.Token.Text, expression.Type.Name));
            }

            return Expression.Property(expression, propertyInfo);
        }

        private static Expression VisitMaxDateConstant()
        {
            return Expression.Constant(new DateTime(2079, 06, 06, 0, 0, 0, 0, DateTimeKind.Utc));
        }

        private static Expression VisitMinDateConstant()
        {
            return Expression.Constant(new DateTime(1900, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
        }

        private static Expression VisitNowDateConstant()
        {
            return Expression.Constant(DateTime.UtcNow);
        }

        private static Expression VisitNullConstant()
        {
            return Expression.Constant(null, typeof(object));
        }

        private static Expression VisitNumberLiteralName(ParseTreeNode node)
        {
            return Expression.Constant(node.Token.Value);
        }

        private static Expression VisitStringLiteralName(ParseTreeNode node)
        {
            return Expression.Constant(node.Token.Value);
        }

        private static Expression VisitTrueConstant()
        {
            return Expression.Constant(true);
        }

        private ParseTree Parse(string queryString)
        {
            var language = new LanguageData(_queryStringGrammar);
            var parser = new Parser(language);
            if (!parser.Language.CanParse())
            {
                throw new InvalidOperationException("Query string grammar is invalid");
            }

            var parseTree = parser.Parse(queryString);

            if (parseTree.ParserMessages.Any())
            {
                _queryMetadata.IsValid = false;
                _queryMetadata.Messages = parseTree.ParserMessages.ToList();
                return null;
            }

            return parseTree;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Visiting dispatch method.")]
        private Expression Visit(
            Expression expression,
            ParseTreeNode node)
        {
            if (node.Term.Name == QueryStringGrammar.RootNonterminalName)
            {
                return VisitRoot(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.ActionNonterminalName)
            {
                return VisitAction(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.OrderByNonterminalName)
            {
                return VisitOrderBy(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.OrderListNonterminalName)
            {
                return VisitOrderList(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.OrderItemNonterminalName)
            {
                return VisitOrderItem(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.FilterNonterminalName)
            {
                return VisitFilter(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.FilterExpressionNonterminalName)
            {
                return VisitFilterExpression(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.ComparingFunctionCallNonterminalName)
            {
                return VisitComparingFunctionCall(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.ValueFunctionCallNonterminalName)
            {
                return VisitValueFunctionCall(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.IdentifierNonterminalName)
            {
                return VisitIdentifier(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.IdentifierTerminalName)
            {
                return VisitIdentifierBase(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.ComparisonNonterminalName)
            {
                return VisitComparision(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.LiteralValueNonterminalName)
            {
                return VisitLiteralValue(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.ValueNonterminalName)
            {
                return VisitValueNonTerminal(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.NumberLiteralName)
            {
                return VisitNumberLiteralName(node);
            }

            if (node.Term.Name == QueryStringGrammar.StringLiteralName)
            {
                return VisitStringLiteralName(node);
            }

            if (node.Term.Name == QueryStringGrammar.DateLiteralName)
            {
                return VisitDateLiteral(node);
            }

            if (node.Term.Name == QueryStringGrammar.MinDateConstant)
            {
                return VisitMinDateConstant();
            }

            if (node.Term.Name == QueryStringGrammar.MaxDateConstant)
            {
                return VisitMaxDateConstant();
            }

            if (node.Term.Name == QueryStringGrammar.NowDateConstant)
            {
                return VisitNowDateConstant();
            }

            if (node.Term.Name == QueryStringGrammar.TrueConstant)
            {
                return VisitTrueConstant();
            }

            if (node.Term.Name == QueryStringGrammar.FalseConstant)
            {
                return VisitFalseConstant();
            }

            if (node.Term.Name == QueryStringGrammar.NullConstant)
            {
                return VisitNullConstant();
            }

            if (node.Term.Name == QueryStringGrammar.IsNullNonterminalName)
            {
                return VisitIsNullFunctionCall(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.SelectNonterminalName)
            {
                return VisitSelectNonTerminal(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.TopNonterminalName)
            {
                return VisitTopNonTerminal(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.SkipNonterminalName)
            {
                return VisitSkipNonTerminal(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.InlineCountNonterminalName)
            {
                return VisitInlineCountNonTerminal(expression, node);
            }

            throw new NotImplementedException();
        }

        private Expression VisitAction(
            Expression expression,
            ParseTreeNode node)
        {
            return Visit(expression, node.ChildNodes[0]);
        }

        private Expression VisitComparingFunctionCall(
            Expression expression,
            ParseTreeNode node)
        {
            if (node.ChildNodes[0].ChildNodes[0].Token.Text.Equals(
                QueryStringGrammar.StartsWithMethodName,
                StringComparison.OrdinalIgnoreCase))
            {
                var first = Visit(expression, node.ChildNodes[0].ChildNodes[1]);
                var second = Visit(expression, node.ChildNodes[0].ChildNodes[2]);
                var method = ExpressionExtensions.GetMethod((string x) => x.StartsWith(null));
                return Expression.Call(first, method, second);
            }

            throw new NotImplementedException();
        }

        private Expression VisitComparision(
            Expression expression,
            ParseTreeNode node)
        {
            var left = Visit(expression, node.ChildNodes[0]);
            var @operator = node.ChildNodes[1].ChildNodes[0].Term.Name;
            var right = Visit(expression, node.ChildNodes[2]);

            if (right.Type != left.Type)
            {
                if (right.NodeType == ExpressionType.Constant)
                {
                    var constant = (ConstantExpression)right;
                    if (constant.Value == null)
                    {
                        var type = left.Type;
                        if (!type.IsClass && (!type.IsGenericType || typeof(Nullable<>) != type.GetGenericTypeDefinition()))
                        {
                            type = typeof(Nullable<>).MakeGenericType(type);
                        }

                        right = Expression.Constant(null, type);
                    }
                    else if (left.Type.IsGenericType && left.Type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        var value = Activator.CreateInstance(
                            left.Type,
                            new[] { Convert.ChangeType(constant.Value, left.Type.GetGenericArguments()[0], CultureInfo.InvariantCulture) });

                        right = Expression.Constant(value, left.Type);
                    }
                    else if (constant.Value is DateTimeOffset)
                    {
                        var date = ((DateTimeOffset)constant.Value).DateTime;
                        right = Expression.Constant(Convert.ChangeType(date, left.Type, CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        right = Expression.Constant(Convert.ChangeType(constant.Value, left.Type, CultureInfo.InvariantCulture));
                    }
                }
                else
                {
                    right = Expression.Convert(right, left.Type);
                }
            }

            switch (@operator)
            {
                case QueryStringGrammar.EqualOperator:
                    return Expression.Equal(left, right);

                case QueryStringGrammar.NotEqualsOperator:
                    return Expression.NotEqual(left, right);

                case QueryStringGrammar.LessThenOperator:
                    return Expression.LessThan(left, right);

                case QueryStringGrammar.LessThenOrEqualOperator:
                    return Expression.LessThanOrEqual(left, right);

                case QueryStringGrammar.GreaterThenOperator:
                    return Expression.GreaterThan(left, right);

                case QueryStringGrammar.GreaterThenOrEqualOperator:
                    return Expression.GreaterThanOrEqual(left, right);
            }

            throw new NotImplementedException();
        }

        private Expression VisitFilter(
            Expression expression,
            ParseTreeNode node)
        {
            _queryMetadata.IsFiltered = true;

            var inner = expression.Type.GetGenericArguments()[0];
            var parameter = Expression.Parameter(inner);

            var body = Visit(parameter, node.ChildNodes[2]);

            var methodDefinition = typeof(Queryable).GetMethods()
                .Single(x => x.Name == "Where"
                             && x.GetParameters().Count() == 2
                             &&
                             x.GetParameters()[1].ParameterType.GetGenericArguments()[0].GetGenericTypeDefinition() ==
                             typeof(Func<,>));
            var method = methodDefinition.MakeGenericMethod(inner);
            var filter = Expression.Lambda(body, parameter);
            return Expression.Call(method, expression, filter);
        }

        private Expression VisitFilterExpression(
            Expression expression,
            ParseTreeNode node)
        {
            if (node.ChildNodes.Count() == 3)
            {
                var left = Visit(expression, node.ChildNodes[0]);
                var @operator = node.ChildNodes[1].ChildNodes[0].Term.Name;
                var right = Visit(expression, node.ChildNodes[2]);
                if (@operator == QueryStringGrammar.AndOperator)
                {
                    return Expression.AndAlso(left, right);
                }
                else
                {
                    return Expression.OrElse(left, right);
                }
            }
            else if (node.ChildNodes.Count() == 2)
            {
                var value = Visit(expression, node.ChildNodes[1]);
                return Expression.Not(value);
            }
            else
            {
                return Visit(expression, node.ChildNodes[0]);
            }
        }

        private Expression VisitIdentifier(
            Expression expression,
            ParseTreeNode node)
        {
            var expr = Visit(expression, node.ChildNodes[0]);

            if (node.ChildNodes.Count == 3)
            {
                return Visit(expr, node.ChildNodes[2]);
            }

            return expr;
        }

        private IEnumerable<Expression> VisitIdentifierList(
            Expression expression,
            ParseTreeNode node)
        {
            var first = VisitList(expression, node.ChildNodes[0]);
            if (node.ChildNodes.Count == 2)
            {
                var second = VisitList(expression, node.ChildNodes[1]);
                return first.Concat(second);
            }

            return first;
        }

        private Expression VisitInlineCountNonTerminal(
            Expression expression,
            ParseTreeNode node)
        {
            if (node.ChildNodes[2].ChildNodes[0].Term.Name == QueryStringGrammar.AllPagesConstant)
            {
                _queryMetadata.IncludeInlineCount = true;
            }

            return expression;
        }

        private Expression VisitIsNullFunctionCall(
            Expression expression,
            ParseTreeNode node)
        {
            var parameters = VisitList(expression, node.ChildNodes[1]).ToArray();
            var current = parameters.First();

            for (var i = 1; i < parameters.Count(); i++)
            {
                current = Expression.Coalesce(current, parameters[i]);
            }

            return current;
        }

        private IEnumerable<Expression> VisitList(
            Expression expression,
            ParseTreeNode node)
        {
            if (node.Term.Name == QueryStringGrammar.ValuesListNonterminalName)
            {
                return VisitValuesList(expression, node);
            }

            if (node.Term.Name == QueryStringGrammar.IdentifierListNonterminalName)
            {
                return VisitIdentifierList(expression, node);
            }

            return new[] { Visit(expression, node) };
        }

        private Expression VisitLiteralValue(
            Expression expression,
            ParseTreeNode node)
        {
            return Visit(expression, node.ChildNodes[0]);
        }

        private Expression VisitOrderBy(
            Expression expression,
            ParseTreeNode node)
        {
            _queryMetadata.IsOrdered = true;
            return Visit(expression, node.ChildNodes[2]);
        }

        private Expression VisitOrderItem(
            Expression expression,
            ParseTreeNode node)
        {
            string methodName;
            string methodSuffix = string.Empty;
            if (node.ChildNodes.Count > 1 &&
                node.ChildNodes[1].Token.Text.Equals(
                    QueryStringGrammar.DescendingSortMarker,
                    StringComparison.OrdinalIgnoreCase))
            {
                methodSuffix = "Descending";
            }

            if (expression.NodeType == ExpressionType.Call &&
                ((MethodCallExpression)expression).Method.Name == "OrderBy")
            {
                methodName = "ThenBy" + methodSuffix;
            }
            else
            {
                methodName = "OrderBy" + methodSuffix;
            }

            var methodDefinition = typeof(Queryable).GetMethods()
                .Single(x => x.Name == methodName && x.GetParameters().Count() == 2);

            var inner = expression.Type.GetGenericArguments()[0];
            var parameter = Expression.Parameter(inner);
            var property = Visit(parameter, node.ChildNodes[0]);
            var method = methodDefinition.MakeGenericMethod(inner, property.Type);
            var function = Expression.Lambda(property, parameter);
            return Expression.Call(method, expression, function);
        }

        private Expression VisitOrderList(
            Expression expression,
            ParseTreeNode node)
        {
            var result = Visit(expression, node.ChildNodes[0]);
            if (node.ChildNodes.Count > 1)
            {
                result = Visit(result, node.ChildNodes[1]);
            }

            return result;
        }

        private Expression VisitRoot(
            Expression expression,
            ParseTreeNode node)
        {
            var result = Visit(expression, node.ChildNodes[0]);
            if (node.ChildNodes.Count > 1)
            {
                result = Visit(result, node.ChildNodes[1]);
            }

            return result;
        }

        private Expression VisitSelectNonTerminal(
            Expression expression,
            ParseTreeNode node)
        {
            _queryMetadata.IsProjected = true;
            var inner = expression.Type.GetGenericArguments()[0];
            var parameter = Expression.Parameter(inner);
            var list = VisitList(parameter, node.ChildNodes[2])
                .Cast<MemberExpression>()
                .ToDictionary(x => x.Member.Name, x => x);

            Type dynamicType = LinqRuntimeTypeBuilder.GetDynamicType(list.Values.Select(x => (PropertyInfo)x.Member));

            var bindings = dynamicType.GetProperties()
                .Select(p => Expression.Bind(p, list[p.Name]));

            Expression selector = Expression.Lambda(
                Expression.MemberInit(Expression.New(dynamicType.GetConstructor(Type.EmptyTypes)), bindings),
                parameter);

            return Expression.Call(
                typeof(Queryable),
                "Select",
                new Type[] { inner, dynamicType },
                expression,
                selector);
        }

        private Expression VisitSkipNonTerminal(
            Expression expression,
            ParseTreeNode node)
        {
            _queryMetadata.IsSkipped = true;
            var inner = expression.Type.GetGenericArguments()[0];
            var value = Visit(expression, node.ChildNodes[2]);
            _queryMetadata.SkipValue = (int)((ConstantExpression)value).Value;

            var method = typeof(Queryable).GetMethod("Skip").MakeGenericMethod(inner);

            return Expression.Call(method, expression, value);
        }

        private Expression VisitTopNonTerminal(
            Expression expression,
            ParseTreeNode node)
        {
            _queryMetadata.IsTopped = true;

            var inner = expression.Type.GetGenericArguments()[0];
            var value = Visit(expression, node.ChildNodes[2]);
            _queryMetadata.TopValue = (int)((ConstantExpression)value).Value;
            var method = typeof(Queryable).GetMethod("Take").MakeGenericMethod(inner);

            return Expression.Call(method, expression, value);
        }

        private Expression VisitValueFunctionCall(
            Expression expression,
            ParseTreeNode node)
        {
            return Visit(expression, node.ChildNodes[0]);
        }

        private Expression VisitValueNonTerminal(
            Expression expression,
            ParseTreeNode node)
        {
            return Visit(expression, node.ChildNodes[0]);
        }

        private IEnumerable<Expression> VisitValuesList(
            Expression expression,
            ParseTreeNode node)
        {
            return VisitList(expression, node.ChildNodes[0]).Concat(VisitList(expression, node.ChildNodes[1]));
        }
    }
}