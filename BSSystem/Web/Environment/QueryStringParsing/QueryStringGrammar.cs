﻿using Irony.Parsing;

namespace SolvesIt.BSSystem.Web.Environment.QueryStringParsing
{
    [Language("ODataUri", "1.0", "ODataUri")]
    public sealed class QueryStringGrammar : Grammar
    {
        public const string ActionNonterminalName = "Action";
        public const string AllPagesConstant = "allpages";
        public const string AndOperator = "and";
        public const string AscendingSortMarker = "asc";
        public const string BinaryOperatorNonterminalName = "BinaryOperator";
        public const string ComparingFunctionCallNonterminalName = "ComparingFunctionCall";
        public const string ComparisonNonterminalName = "Comparison";
        public const string DateLiteralName = "Date";
        public const string DescendingSortMarker = "desc";
        public const string EqualOperator = "eq";
        public const string FalseConstant = "false";
        public const string FilterExpressionNonterminalName = "FilterExpression";
        public const string FilterNonterminalName = "Filter";
        public const string GreaterThenOperator = "gt";
        public const string GreaterThenOrEqualOperator = "ge";
        public const string IdentifierListNonterminalName = "IdentifierList";
        public const string IdentifierNonterminalName = "Identifier";
        public const string IdentifierTerminalName = "IdentifierBase";
        public const string InlineCountNonterminalName = "InlineCount";
        public const string IsNullMethodName = "IsNull";
        public const string IsNullNonterminalName = "IsNull";
        public const string LessThenOperator = "lt";
        public const string LessThenOrEqualOperator = "le";
        public const string LiteralValueNonterminalName = "LiteralValue";
        public const string MaxDateConstant = "maxDate";
        public const string MinDateConstant = "minDate";
        public const string NegationOperator = "!";
        public const string NoneConstant = "none";
        public const string NotEqualsOperator = "ne";
        public const string NowDateConstant = "now";
        public const string NullConstant = "null";
        public const string NumberLiteralName = "Number";
        public const string OrderByNonterminalName = "OrderBy";
        public const string OrderItemNonterminalName = "OrderItem";
        public const string OrderListNonterminalName = "OrderList";
        public const string OrOperator = "or";
        public const string RootNonterminalName = "Root";
        public const string SelectNonterminalName = "Select";
        public const string SkipNonterminalName = "Skip";
        public const string StartsWithMethodName = "startsWith";
        public const string StartsWithNonterminalName = "StartsWith";
        public const string StringLiteralName = "String";
        public const string TopNonterminalName = "Top";
        public const string TrueConstant = "true";
        public const string ValueFunctionCallNonterminalName = "ValueFunctionCall";
        public const string ValueNonterminalName = "Value";
        public const string ValuesListNonterminalName = "ValuesList";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "eq", Justification = "Grammar token")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "IsNull", Justification = "Grammar token")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "startsWith", Justification = "Grammar token")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "orderby", Justification = "Grammar token")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)", Justification = "Grammar token")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "allpages", Justification = "Odata query string consistency")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "inlinecount", Justification = "Odata query string consistency")]
        public QueryStringGrammar()
            : base(false)
        {
            var number = TerminalFactory.CreateCSharpNumber(NumberLiteralName);
            number.Priority = TerminalPriority.Low;
            var stringLiteral = new StringLiteral(StringLiteralName, "'", StringOptions.NoEscapes | StringOptions.AllowsDoubledQuote);
            var dateLiteral = new DateLiteral(DateLiteralName);
            var identifierBase = TerminalFactory.CreateCSharpIdentifier(IdentifierTerminalName);

            var root = new NonTerminal(RootNonterminalName);
            var action = new NonTerminal(ActionNonterminalName);
            var select = new NonTerminal(SelectNonterminalName);
            var identifierList = new NonTerminal(IdentifierListNonterminalName);
            var filter = new NonTerminal(FilterNonterminalName);
            var filterExpression = new NonTerminal(FilterExpressionNonterminalName);
            var comparison = new NonTerminal(ComparisonNonterminalName);
            var binaryOperator = new NonTerminal(BinaryOperatorNonterminalName);
            var comparingFunctionCall = new NonTerminal(ComparingFunctionCallNonterminalName);
            var isNull = new NonTerminal(IsNullNonterminalName);
            var valuesList = new NonTerminal(ValuesListNonterminalName);
            var startsWith = new NonTerminal(StartsWithNonterminalName);
            var value = new NonTerminal(ValueNonterminalName);
            var orderBy = new NonTerminal(OrderByNonterminalName);
            var orderList = new NonTerminal(OrderListNonterminalName);
            var orderItem = new NonTerminal(OrderItemNonterminalName);
            var top = new NonTerminal(TopNonterminalName);
            var skip = new NonTerminal(SkipNonterminalName);
            var inlinecount = new NonTerminal(InlineCountNonterminalName);
            var literalValue = new NonTerminal(LiteralValueNonterminalName);
            var valueFunctionCall = new NonTerminal(ValueFunctionCallNonterminalName);
            var identifier = new NonTerminal(IdentifierNonterminalName);

            identifier.Rule = identifierBase
                              | identifier + "." + identifierBase;

            inlinecount.Rule = ToTerm("$inlinecount") + "=" + (ToTerm(AllPagesConstant) | NoneConstant);
            top.Rule = ToTerm("$top") + "=" + number;
            skip.Rule = ToTerm("$skip") + "=" + number;

            orderItem.Rule = identifier
                             | identifier + AscendingSortMarker
                             | identifier + DescendingSortMarker;
            orderList.Rule = orderItem
                             | orderList + "," + orderItem;

            orderBy.Rule = ToTerm("$orderby") + "=" + orderList;
            startsWith.Rule = ToTerm(StartsWithMethodName) + "(" + identifier + "," + stringLiteral + ")";

            literalValue.Rule =
            number
            | stringLiteral
            | dateLiteral
            | TrueConstant
            | FalseConstant
            | MinDateConstant
            | MaxDateConstant
            | NowDateConstant
            | NullConstant;

            value.Rule = identifier
                         | literalValue
                         | valueFunctionCall;

            valuesList.Rule = identifier + "," + value
                              | valuesList + "," + value;

            isNull.Rule = ToTerm(IsNullMethodName) + "(" + valuesList + ")";
            comparingFunctionCall.Rule = startsWith;
            valueFunctionCall.Rule = isNull;
            binaryOperator.Rule = ToTerm(EqualOperator) | NotEqualsOperator | LessThenOperator | GreaterThenOperator | LessThenOrEqualOperator | GreaterThenOrEqualOperator;
            comparison.Rule = value + binaryOperator + value;

            filterExpression.Rule = comparison
                                    | comparingFunctionCall
                                    | filterExpression + (ToTerm(AndOperator) | OrOperator) + filterExpression
                                    | ToTerm("(") + filterExpression + ")"
                                    | ToTerm(NegationOperator) + "(" + filterExpression + ")";

            filter.Rule = ToTerm("$filter") + "=" + filterExpression;
            identifierList.Rule = identifier
                                 | identifierList + "," + identifier;

            select.Rule = ToTerm("$select") + "=" + identifierList;

            action.Rule = select
                          | filter
                          | orderBy
                          | top
                          | skip
                          | inlinecount;
            root.Rule = action
                        | root + "&" + action;
            Root = root;

            RegisterOperators(1, OrOperator);
            RegisterOperators(2, AndOperator);
            RegisterOperators(6, EqualOperator, NotEqualsOperator);
            RegisterOperators(7, LessThenOperator, GreaterThenOperator, LessThenOrEqualOperator, GreaterThenOrEqualOperator);
            AddTermsReportGroup("unary operator", NegationOperator);
            MarkPunctuation("&", ",", "(", ")");
            AddTermsReportGroup("constant", number);
            AddTermsReportGroup("constant", TrueConstant, FalseConstant, NullConstant, MinDateConstant, MaxDateConstant, NowDateConstant);
        }
    }
}