namespace SolvesIt.BSSystem.Web.Extensions
{
    public enum InputKind
    {
        AutoDetect,
        Text,
        Number,
        DateTime,
        Email,
        CheckBox,
        Color,
        Hidden,
        Password,
        Date
    }
}