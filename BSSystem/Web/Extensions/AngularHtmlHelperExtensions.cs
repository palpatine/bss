﻿using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.Web.Extensions
{
    public static class AngularHtmlHelperExtensions
    {
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyDisplayFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            var variableName = propertyExpression.Parameters[0].Name;
            var properties = propertyExpression.GetProperties();
            var lastPropertyName = properties.Last().Name;
            var path = string.Join(".", properties.Select(x => char.ToLower(x.Name[0], CultureInfo.InvariantCulture) + x.Name.Substring(1)));

            var basicAccessor = string.Format(
                CultureInfo.InvariantCulture,
                "{0}.{1}",
                variableName,
                path);
            var propertyAccessor = basicAccessor;

            if (typeof(TValue) == typeof(DateTime) || typeof(TValue) == typeof(DateTimeOffset)
                || typeof(TValue) == typeof(DateTime?) || typeof(TValue) == typeof(DateTimeOffset?))
            {
                if (typeof(IMarker).IsAssignableFrom(typeof(TModel)))
                {
                    var sqlTypeProvider = StorableHelper.GetSqlTypeAttribute(propertyExpression);
                    if (sqlTypeProvider.Type == SqlDbType.SmallDateTime)
                    {
                        propertyAccessor = string.Format(CultureInfo.InvariantCulture, "({0} | dateFormatter)", propertyAccessor);
                    }
                    else
                    {
                        propertyAccessor = string.Format(CultureInfo.InvariantCulture, "({0} | dateTimeFormatter)", propertyAccessor);
                    }
                }
                else
                {
                    propertyAccessor = string.Format(CultureInfo.InvariantCulture, "({0} | dateFormatter)", propertyAccessor);
                }
            }

            if (UndefinedValueReplacementsProvider.ContainsKey(lastPropertyName))
            {
                propertyAccessor = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} || '{1}'",
                    propertyAccessor,
                    UndefinedValueReplacementsProvider.GetReplacementValue(lastPropertyName));
            }

            return new HtmlString(string.Format(CultureInfo.InvariantCulture, "<p class='form-control-static'>{{{{{0} || '&nbsp;'}}}}</p>", propertyAccessor));
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyEditNullableFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            bool isEditableOnEdition = true,
            object additionalViewData = null)
        {
            var commonAttributes = CreateCommonAttributes(propertyExpression, isEditableOnEdition, additionalViewData);
            return new HtmlString(string.Format(CultureInfo.InvariantCulture, "<bss-nullable-form-control {0}></bss-nullable-form-control>", commonAttributes));
        }

        [SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase", Justification = "This is not string normailzation")]
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyEditorFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            InputKind kind = InputKind.AutoDetect,
            bool isEditableOnEdition = true,
            object additionalViewData = null)
        {
            var sqlType = StorableHelper.GetSqlTypeAttribute(propertyExpression);
            var property = propertyExpression.GetProperty();
            var type = property.PropertyType;
            var isCustomControl = false;
            string inputType = null;
            string element = null;
            var commonAttributes = CreateCommonAttributes(propertyExpression, isEditableOnEdition, additionalViewData);

            if (IsText(kind, sqlType, type))
            {
                inputType = "text";
            }
            else if (IsNumber(kind, type))
            {
                inputType = "number";
            }
            else if (IsDate(kind, sqlType, type))
            {
                element = string.Format(CultureInfo.InvariantCulture, "<div bss-datepicker {0} />", commonAttributes);
                isCustomControl = true;
            }
            else if (IsNullableDate(kind, sqlType, type))
            {
                element = string.Format(
                    CultureInfo.InvariantCulture,
                    "<bss-nullable-form-control {0} template-url='\"~/Content/Templates/NullableFormControl/DateElement.tpl.html\"'></bss-nullable-form-control>",
                    commonAttributes);
                isCustomControl = true;
            }
            else if (IsDateTime(kind, sqlType))
            {
                element = string.Format(CultureInfo.InvariantCulture, "<datetimepicker {0} />", commonAttributes);
                isCustomControl = true;
            }
            else if (IsCheckBox(kind, type))
            {
                element = string.Format(
                    CultureInfo.InvariantCulture,
                    "<div ng-required class='form-control-static'><checkbox name {0}></checkbox></div>",
                    commonAttributes);
                isCustomControl = true;
            }
            else if (kind == InputKind.Hidden)
            {
                inputType = "hidden";
            }
            else if (kind == InputKind.Email)
            {
                inputType = "email";
            }
            else if (kind == InputKind.Color)
            {
                inputType = "color";
            }
            else if (kind == InputKind.Password)
            {
                inputType = "password";
            }
            else
            {
                inputType = "text";
            }

            var builer = new StringBuilder();
            if (isCustomControl)
            {
                builer.Append(element);
            }
            else
            {
                var entityType = StorableHelper.TryGetEntityType(typeof(TModel));
                var shouldValidateUniqueDisplayName = property.Name == "DisplayName" && sqlType != null &&
                                                      entityType != null;

                builer.AppendFormat(
                    CultureInfo.InvariantCulture,
                    "<input class='form-control' {0} type='{1}' ",
                    commonAttributes,
                    inputType);

                if (shouldValidateUniqueDisplayName)
                {
                    builer.AppendFormat("entities='{0}' bss-unique-display-name", entityType.Name);
                }

                builer.Append("></input>");
            }

            var messagesUri = new UrlHelper(HttpContext.Current.Request.RequestContext)
                //// ReSharper disable Mvc.ActionNotResolved
                .Action("ValidationMessages");
            //// ReSharper restore Mvc.ActionNotResolved
            builer.AppendFormat(
                "<div ng-if='formController.{1}.$invalid && formController.{1}.$dirty' ng-messages='formController.{1}.$error'  class='alert alert-warning' role='alert'>" +
                "<div ng-messages-include='{0}'></div></div>",
                messagesUri,
                property.Name);

            return new HtmlString(builer.ToString());
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyEditorFormColor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            bool isEditableOnEdition = true,
            object additionalViewData = null)
        {
            var commonAttributes = CreateCommonAttributes(propertyExpression, isEditableOnEdition, additionalViewData);
            var variableName = propertyExpression.Parameters[0].Name;
            var propertyName = propertyExpression.GetCamelCasePropertyPath();
            var element = string.Format(
                CultureInfo.InvariantCulture,
                "<input colorpicker class='form-control' {0} colorpicker-position='top' type='text' style='background-color:{{{{{1}.{2}}}}}' >",
                commonAttributes,
                variableName,
                propertyName);
            return new HtmlString(element);
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyEditSelectorFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            bool isEditableOnEdition = true,
            string source = null,
            object additionalViewData = null)
        {
            var propertyName = propertyExpression.GetCamelCasePropertyPath();
            source = source != null ? string.Format(CultureInfo.InvariantCulture, "{0}", source) : string.Format(CultureInfo.InvariantCulture, "vm.{0}SelectorSource", propertyName);
            var commonAttributes = CreateCommonAttributes(propertyExpression, isEditableOnEdition, additionalViewData);
            var element = string.Format(
                CultureInfo.InvariantCulture,
                "<bss-selector {0} source=\"{1}\" single-select='true'></bss-selector>",
                commonAttributes,
                source);
            return new HtmlString(element);
        }

        [SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase", Justification = "This is not string normailzation")]
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyEditTextAreaFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            bool isEditableOnEdition = true,
            object additionalViewData = null)
        {
            var commonAttributes = CreateCommonAttributes(propertyExpression, isEditableOnEdition, additionalViewData);
            var element = string.Format(CultureInfo.InvariantCulture, "<textarea  class='form-control vertical-resize' {0} ></textarea >", commonAttributes);
            return new HtmlString(element);
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyReferenceFor<TModel, TValue>(
            this HtmlHelper htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            var variableName = propertyExpression.Parameters[0].Name;
            var propertyName = propertyExpression.GetCamelCasePropertyPath();
            return new HtmlString(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", variableName, propertyName));
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyReferenceFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            var variableName = propertyExpression.Parameters[0].Name;
            var propertyName = propertyExpression.GetCamelCasePropertyPath();
            return new HtmlString(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", variableName, propertyName));
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyReferenceForBoolean<TModel>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, bool>> propertyExpression)
        {
            var variableName = propertyExpression.Parameters[0].Name;
            var propertyName = propertyExpression.GetCamelCasePropertyPath();
            return new HtmlString(string.Format(CultureInfo.InvariantCulture, "<div class='form-control-static'><checkbox ng-model='{0}.{1}'  disabled></checkbox></div>", variableName, propertyName));
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyReferenceForBoolean<TModel>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, bool?>> propertyExpression)
        {
            var variableName = propertyExpression.Parameters[0].Name;
            var propertyName = propertyExpression.GetCamelCasePropertyPath();
            return new HtmlString(string.Format(CultureInfo.InvariantCulture, "<div class='form-control-static'><checkbox ng-model='{0}.{1}'  disabled></checkbox></div>", variableName, propertyName));
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static HtmlString PropertyReferenceForColor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            var variableName = propertyExpression.Parameters[0].Name;
            var propertyName = propertyExpression.GetCamelCasePropertyPath();
            return new HtmlString(string.Format(CultureInfo.InvariantCulture, "<div style='background-color:{{{{{0}.{1}}}}}' class='color-placeholder' ></div>", variableName, propertyName));
        }

        private static string CreateCommonAttributes<TModel, TValue>(
            Expression<Func<TModel, TValue>> propertyExpression,
            bool isEditableOnEdition = true,
            object additionalViewData = null)
        {
            var additionalElements = ProcessAdditionalHtmlDescriptonElements(additionalViewData);
            var additionalElementsFromSql =
                ProcessAdditionalHtmlDescriptonElements(StorableHelper.GetSqlTypeAttribute(propertyExpression));
            additionalElements.Attributes += additionalElementsFromSql.Attributes;
            additionalElements.Classes += additionalElementsFromSql.Classes;

            var variableName = propertyExpression.Parameters[0].Name;
            var modelPropertyName = propertyExpression.GetCamelCasePropertyPath();
            var propertyName = propertyExpression.GetPropertyName();

            var commonAttributes = string.Format(
                CultureInfo.InvariantCulture,
                "ng-model='{0}.{1}' name='{2}' bss-server-validation='validationErrors' class='{3}' {4} {5}",
                variableName,
                modelPropertyName,
                propertyName,
                additionalElements.Classes,
                additionalElements.Attributes,
                isEditableOnEdition ? string.Empty : string.Format(CultureInfo.InvariantCulture, "ng-disabled='{0}.Id'", modelPropertyName));
            return commonAttributes;
        }

        private static bool IsCheckBox(
            InputKind kind,
            Type type)
        {
            return kind == InputKind.CheckBox || (kind == InputKind.AutoDetect && type == typeof(bool));
        }

        private static bool IsDate(
            InputKind kind,
            SqlTypeAttribute sqlType,
            Type type)
        {
            return kind == InputKind.Date || (kind == InputKind.AutoDetect
                                              && ((sqlType == null && (type == typeof(DateTime) || type == typeof(DateTimeOffset)))
                                                  || (sqlType != null && sqlType.Type == SqlDbType.SmallDateTime && !sqlType.IsNullable)));
        }

        private static bool IsDateTime(
            InputKind kind,
            SqlTypeAttribute sqlType)
        {
            return kind == InputKind.DateTime || (kind == InputKind.AutoDetect && sqlType != null && sqlType.Type == SqlDbType.DateTime);
        }

        private static bool IsNullableDate(
            InputKind kind,
            SqlTypeAttribute sqlType,
            Type type)
        {
            return kind == InputKind.AutoDetect
                   && ((sqlType == null && (type == typeof(DateTime?) || type == typeof(DateTimeOffset?)))
                       || (sqlType != null && sqlType.Type == SqlDbType.SmallDateTime && sqlType.IsNullable));
        }

        private static bool IsNumber(
            InputKind kind,
            Type type)
        {
            return kind == InputKind.Number || (kind == InputKind.AutoDetect && type == typeof(int));
        }

        private static bool IsText(
            InputKind kind,
            SqlTypeAttribute sqlType,
            Type type)
        {
            return kind == InputKind.Text
                   || (kind == InputKind.AutoDetect
                       && ((sqlType == null && type == typeof(string)) || (sqlType != null && sqlType.Type == SqlDbType.NVarChar)));
        }

        private static AdditionalHtmlDescriptionElements ProcessAdditionalHtmlDescriptonElements(object additionalViewData)
        {
            var result = new AdditionalHtmlDescriptionElements
            {
                Attributes = string.Empty,
                Classes = string.Empty
            };

            if (additionalViewData == null)
            {
                return result;
            }

            var name = additionalViewData.GetType()
                .GetProperty("name", BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);
            if (name != null)
            {
                throw new InvalidOperationException("name cannot be overridden");
            }

            var @class = additionalViewData.GetType()
                .GetProperty("class", BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);

            if (@class != null)
            {
                result.Classes = @class.GetValue(additionalViewData).ToString();
            }

            result.Attributes =
                string.Join(
                    " ",
                    additionalViewData.GetType()
                        .GetProperties()
                        .Where(x => x.Name != "name" && x.Name != "class")
                        .Select(htmlProperty => string.Format(CultureInfo.InvariantCulture, "{0}='{1}'", htmlProperty.Name.Replace("_", "-"), htmlProperty.GetValue(additionalViewData))));
            return result;
        }

        private static AdditionalHtmlDescriptionElements ProcessAdditionalHtmlDescriptonElements(SqlTypeAttribute propertySqlType)
        {
            var result = new AdditionalHtmlDescriptionElements
            {
                Attributes = string.Empty,
                Classes = string.Empty
            };

            if (propertySqlType != null)
            {
                if (!propertySqlType.IsNullable)
                {
                    result.Attributes += " required='true'";
                }

                if (StorableHelper.HasCapacity(propertySqlType.Type)
                    && propertySqlType.Capacity > 0)
                {
                    result.Attributes = string.Format(CultureInfo.InvariantCulture, "{0} maxlength='{1}'", result.Attributes, propertySqlType.Capacity);
                }
            }

            return result;
        }
    }
}