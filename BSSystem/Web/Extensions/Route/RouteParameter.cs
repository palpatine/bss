using System;
using System.Globalization;
using System.Linq.Expressions;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Extensions.Route
{
    public static class RouteParameter
    {
        public static RouteParameter<int> CurrentUserId()
        {
            var result = new RouteParameter<int>("state.userData.user.id");
            return result;
        }

        public static RouteParameter<TValue> From<TModel, TValue>(
            Expression<Func<TModel, TValue>> parameterPointer,
            string variableName)
        {
            var propertyPath = parameterPointer.GetCamelCasePropertyPath();
            var variable = string.Format(CultureInfo.InvariantCulture, "{0}.{1}", variableName, propertyPath);
            var result = new RouteParameter<TValue>(variable);
            return result;
        }

        public static RouteParameter<TValue> FromItem<TModel, TValue>(Expression<Func<TModel, TValue>> parameterPointer)
        {
            return From(parameterPointer, "item");
        }

        public static RouteParameter<TValue> FromModel<TModel, TValue>(Expression<Func<TModel, TValue>> parameterPointer)
        {
            return From(parameterPointer, "model");
        }

        public static RouteParameter<int> IdFromModel()
        {
            return FromModel((IStorable s) => s.Id);
        }

        public static RouteParameter<int> IdFromSection()
        {
            return From((IStorable s) => s.Id, "section");
        }
    }
}