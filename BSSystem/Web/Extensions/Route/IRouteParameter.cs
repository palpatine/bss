namespace SolvesIt.BSSystem.Web.Extensions.Route
{
    public interface IRouteParameter
    {
        string Value { get; }
    }
}