﻿namespace SolvesIt.BSSystem.Web.Extensions.Route
{
    public sealed class RouteParameter<T> : Impersonator<T>, IRouteParameter
    {
        public RouteParameter(string value)
        {
            Value = value;
        }

        public string Value { get; }
    }
}