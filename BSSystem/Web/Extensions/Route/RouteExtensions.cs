using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Web.Controllers.Api;

namespace SolvesIt.BSSystem.Web.Extensions.Route
{
    public static class RouteExtensions
    {
        public static string BssHttpRouteAngularExpression<TController, TResult>(
            this UrlHelper helper,
            Expression<Func<TController, TResult>> action)
               where TController : System.Web.Http.ApiController
        {
            Dictionary<string, string> postCreationReplacements;
            var address = PreprocessRoute(helper, action, out postCreationReplacements);

            foreach (var postCreationReplacement in postCreationReplacements)
            {
                var context = string.Format(CultureInfo.InvariantCulture, "' + {0} + '", postCreationReplacement.Value);
                address = address.Replace(postCreationReplacement.Key, context);
            }

            address = string.Format(CultureInfo.InvariantCulture, "'{0}'", address);

            var prerequirements = string.Join(" && ", postCreationReplacements.Values);
            if (prerequirements.Length > 0)
            {
                address = string.Format(CultureInfo.InvariantCulture, "{0} ? {1} : ''", prerequirements, address);
            }

            return address;
        }

        public static Uri BssHttpRouteUrl<TController, TResult>(
            this UrlHelper helper,
            Expression<Func<TController, TResult>> action)
            where TController : System.Web.Http.ApiController
        {
            Dictionary<string, string> postCreationReplacements;
            var address = PreprocessRoute(helper, action, out postCreationReplacements);

            foreach (var postCreationReplacement in postCreationReplacements)
            {
                address = address.Replace(postCreationReplacement.Key, postCreationReplacement.Value);
            }

            return new Uri(address);
        }

        private static string PreprocessRoute<TController, TResult>(
            UrlHelper helper,
            Expression<Func<TController, TResult>> action,
            out Dictionary<string, string> postCreationReplacements)
            where TController : System.Web.Http.ApiController
        {
            var actionMethod = action.GetMethod();
            var controller = typeof(TController).Name.Replace("Controller", string.Empty);
            string area;
            string route;

            if (typeof(TController) == typeof(AssignmentsController))
            {
                area = string.Empty;
                route = actionMethod.GetParameters().Any(x => x.Name == "id") ? "AssignmentsApi" : "AssignmentsListApi";
            }
            else
            {
                if (typeof(TController).Namespace.Contains("Web.Controllers"))
                {
                    area = string.Empty;
                }
                else
                {
                    var areaMatch = Regex.Match(typeof(TController).Namespace, @"Areas\.(.*?)\.Controllers");
                    if (!areaMatch.Success)
                    {
                        throw new InvalidOperationException("Controller namespace does not follow required pattern.");
                    }

                    area = areaMatch.Groups[1].Value;
                }

                route = actionMethod.GetParameters().Any(x => x.Name == "id") ? "EntityApi" : "CollectionApi";
            }

            var routeValues = ProcessArguments(action, out postCreationReplacements, controller, actionMethod);

            var address = helper.HttpRouteUrl(area + route, routeValues);
            return address;
        }

        private static RouteValueDictionary ProcessArguments<TController, TResult>(
            Expression<Func<TController, TResult>> action,
            out Dictionary<string, string> postCreationReplacements,
            string controller,
            MethodInfo actionMethod)
            where TController : System.Web.Http.ApiController
        {
            var call = (MethodCallExpression)action.Body;
            var routeValues = new RouteValueDictionary();
            routeValues.Add("controller", controller);
            routeValues.Add("action", actionMethod.Name);
            postCreationReplacements = new Dictionary<string, string>();

            using (var enumerator = call.Arguments.GetEnumerator())
            {
                foreach (var parameter in call.Method.GetParameters())
                {
                    enumerator.MoveNext();

                    if (parameter.ParameterType == typeof(HttpRequest))
                    {
                        continue;
                    }

                    object value;
                    if (enumerator.Current.NodeType == ExpressionType.Constant)
                    {
                        value = ((ConstantExpression)enumerator.Current).Value;
                    }
                    else if (enumerator.Current.NodeType == ExpressionType.MemberAccess)
                    {
                        var lambda = Expression.Lambda<Func<object>>(
                            Expression.Convert(enumerator.Current, typeof(object)));
                        var function = lambda.Compile();
                        var endValue = function();
                        value = endValue;
                    }
                    else
                    {
                        var convert = (UnaryExpression)enumerator.Current;
                        var lambda = Expression.Lambda<Func<IRouteParameter>>(
                            Expression.Convert(convert.Operand, typeof(IRouteParameter)));
                        var function = lambda.Compile();
                        var endValue = function().Value;
                        var id = Guid.NewGuid().ToString("N");
                        postCreationReplacements.Add(id, endValue);
                        value = id;
                    }

                    routeValues.Add(parameter.Name, value);
                }
            }

            return routeValues;
        }
    }
}