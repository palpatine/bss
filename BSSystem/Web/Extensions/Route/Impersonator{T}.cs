﻿namespace SolvesIt.BSSystem.Web.Extensions.Route
{
    public abstract class Impersonator<T>
    {
        protected Impersonator()
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2225:OperatorOverloadsHaveNamedAlternates", Justification = "this is fake conversion"),
        System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "impersonator", Justification = "conversion operator requires this parameter")]
        public static implicit operator T(Impersonator<T> impersonator)
        {
            return default(T);
        }
    }
}