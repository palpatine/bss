namespace SolvesIt.BSSystem.Web.Extensions
{
    public sealed class AdditionalHtmlDescriptionElements
    {
        public string Attributes { get; set; }

        public string Classes { get; set; }
    }
}