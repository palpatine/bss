﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Web.Properties;

namespace SolvesIt.BSSystem.Web
{
    public static class LocalizationHtmlHelperExtensions
    {
        public static MvcHtmlString LocalizedActionLink(
            this HtmlHelper htmlHelper,
            string actionName,
            string controllerName)
        {
            return LocalizedActionLink(
                htmlHelper,
                actionName,
                controllerName,
                new RouteValueDictionary(),
                new RouteValueDictionary());
        }

        public static MvcHtmlString LocalizedActionLink<TController, TValue>(
            this HtmlHelper htmlHelper,
            Expression<Func<TController, TValue>> action,
            object routeValues = null,
            object htmlAttributes = null)
        {
            var actionName = action.GetMethod().Name;
            var controllerName = typeof(TController).Name.Substring(
                0,
                typeof(TController).Name.IndexOf("Controller", StringComparison.Ordinal));
            return LocalizedActionLink(
                htmlHelper,
                actionName,
                controllerName,
                new RouteValueDictionary(routeValues),
                htmlAttributes);
        }

        public static MvcHtmlString LocalizedActionLink(
            this HtmlHelper htmlHelper,
            string actionName,
            string controllerName,
            RouteValueDictionary routeValues,
            object htmlAttributes)
        {
            return LocalizedActionLink(
                htmlHelper,
                actionName,
                controllerName,
                routeValues,
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString LocalizedActionLink(
            this HtmlHelper htmlHelper,
            string actionName,
            string controllerName,
            RouteValueDictionary routeValues,
            IDictionary<string, object> htmlAttributes)
        {
            var linkText = LocalizedValueFor(string.Format(CultureInfo.InvariantCulture, "{0}_{1}", controllerName, actionName));

            var value = "#/Forms" + UrlHelper.GenerateUrl(null, actionName, controllerName, null, null, null, routeValues, htmlHelper.RouteCollection, htmlHelper.ViewContext.RequestContext, true);

            var tagBuilder = new TagBuilder("a")
            {
                InnerHtml = (!string.IsNullOrEmpty(linkText)) ? HttpUtility.HtmlEncode(linkText) : string.Empty
            };

            tagBuilder.MergeAttributes<string, object>(htmlAttributes);
            tagBuilder.MergeAttribute("href", value);
            var tag = tagBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(tag);
        }

        public static MvcHtmlString LocalizedLabelFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.Label(LocalizedFieldDescription(propertyExpression), htmlAttributes);
        }

        public static MvcHtmlString LocalizedLabelFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            object htmlAttributes)
        {
            return htmlHelper.Label(LocalizedFieldDescription(propertyExpression), htmlAttributes);
        }

        public static MvcHtmlString LocalizedLabelFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            return htmlHelper.Label(LocalizedValueFor(propertyExpression));
        }

        public static MvcHtmlString LocalizedLabelFor(
            this HtmlHelper htmlHelper,
            string key)
        {
            return htmlHelper.Label(LocalizedValueFor(key));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static string LocalizedValueFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            return LocalizedValueFor(propertyExpression);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "htmlHelper", Justification = "This is an extension method for convenince of useage.")]
        public static string LocalizedValueFor<TModel>(
                    this HtmlHelper<TModel> htmlHelper,
                    string key)
        {
            return LocalizedValueFor(key);
        }

        public static string LocalizedValueFor(
            string key)
        {
            var value = WebResources.ResourceManager.GetString(key, CultureInfo.CurrentUICulture);
            if (value == null)
            {
                Debug.WriteLine(string.Format(CultureInfo.InvariantCulture, "Missing resource key {0}.", key));
                return string.Format(CultureInfo.InvariantCulture, "[{0}]", key);
            }

            return value;
        }

        public static string LocalizedValueFor<TModel, TValue>(
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            var properties = propertyExpression.GetProperties().ToArray();
            var property = properties.Last();
            var prefixedKey = properties.Aggregate(typeof(TModel).Name, (current, next) => string.Format(CultureInfo.InvariantCulture, "{0}_{1}", current, next.Name));
            var prefixedValue = WebResources.ResourceManager.GetString(prefixedKey, CultureInfo.CurrentUICulture);

            if (string.IsNullOrWhiteSpace(prefixedValue))
            {
                var value = WebResources.ResourceManager.GetString(property.Name, CultureInfo.CurrentUICulture);
                if (string.IsNullOrWhiteSpace(value))
                {
                    Debug.WriteLine("Missing resource key {0} or {1}.", property.Name, prefixedKey);
                    return string.Format(CultureInfo.InvariantCulture, "[{0}]", property.Name);
                }

                return value;
            }

            return prefixedValue;
        }

        private static string LocalizedFieldDescription<TModel, TValue>(
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            return LocalizedValueFor(propertyExpression) + ":";
        }
    }
}