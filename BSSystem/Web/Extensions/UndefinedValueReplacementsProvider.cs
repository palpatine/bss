using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using SolvesIt.BSSystem.Web.Properties;

namespace SolvesIt.BSSystem.Web.Extensions
{
    internal static class UndefinedValueReplacementsProvider
    {
        private static readonly Dictionary<string, string> DefaultValueReplacements = new Dictionary<string, string>
            {
                { "TimeEnd", "TimeEndNotSpecified" },
                { "DateEnd", "DateEndNotSpecified" },
            };

        public static bool ContainsKey(string propertyName)
        {
            return DefaultValueReplacements.ContainsKey(propertyName);
        }

        public static string GetReplacementValue(string propertyName)
        {
            var value = WebResources.ResourceManager.GetString(DefaultValueReplacements[propertyName]);
            if (value == null)
            {
                Debug.WriteLine("Missing resource key {0}.", (object)DefaultValueReplacements[propertyName]);
                return string.Format(CultureInfo.InvariantCulture, "[{0}]", DefaultValueReplacements[propertyName]);
            }

            return value;
        }
    }
}