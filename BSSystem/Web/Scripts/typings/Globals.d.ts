﻿interface IGlobals {
    rootPath: string;
    locale: string;
    firstDayOfWeek: number;
    debug: boolean;
}

declare var Globals: IGlobals;