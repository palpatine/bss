interface JQuery {
    chosen(options: ChosenOptions): JQuery;
    chosen(method: string): JQuery;
    chosen(): JQuery;
}

interface ChosenOptions {
    allow_single_deselect?: boolean;
    disable_search_threshold?: boolean;
    disable_search?: boolean;
    enable_split_word_search?: boolean;
    group_search?: boolean;
    search_contains?: boolean;
    single_backstroke_delete?: boolean;
    max_selected_options?: number;
    inherit_select_classes?: boolean;
    display_selected_options?: boolean;
    display_disabled_options?: boolean;
    placeholder_text_multiple?: string;
    placeholder_text?: string;
    default_multiple_text?: string;
    default_single_text?: string;
    placeholder_text_single?: string;
    no_results_text?: string;
    width?: string;
}