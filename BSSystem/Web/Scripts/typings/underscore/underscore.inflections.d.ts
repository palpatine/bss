﻿interface UnderscoreStatic {
    singularize(word: string): string;
    pluralize(word: string, count: number, includeNumber: boolean): string;
}