﻿
declare module Adapt {
    interface IAjaxConfig {
        url: string;
        method: string;
        params: any;
    }

    interface IPageLoadingService {
        (options: IPageLoadOptions): ng.IPromise<IPageLoadResult>;
    }

    interface IPageLoadResult {
        items: any[];
        currentPage: number;
        totalPages: number;
        pagingArray: number[];
        token: any;
    }

    interface IPageLoadOptions {
        pageNumber: number;
        ajaxConfig: IAjaxConfig;
        pageSize: number;
        orderProperties: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor[];
        //DEPRECATED
        sortKey?: string;
        //DEPRECATED
        sortDirection?: boolean;//true -> descending
        token: any;
    }

    interface IConfig {

    }

    interface IDebounceService {
        <T extends Function>(func: T, delay?: number, immediate?: boolean, ctx?: any): T;
    }

    interface IStrapUtilsService {
        evalObjectProperty(obj, property): any;
        applyFilter(value, filter, item): any;
        itemExistsInList(compareItem, list): boolean;
        itemsExistInList(items, list): boolean;
        addItemToList(item, list): void;
        removeItemFromList(item, list): void;
        addRemoveItemFromList(item, list): void;
        addItemsToList(items, list): void;
        addRemoveItemsFromList(items, list): void;
        moveItemInList(startPos, endPos, list): void;
        parse(items): any[];
        getObjectProperty(item, property): any;
    }
}