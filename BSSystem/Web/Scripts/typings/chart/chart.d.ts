
declare class Chart {

    constructor(context: any);

    Line(data: LineDataStructure, options?: LineDataOptions);

    Bar(data: BarDataStructure, options?: BarDataOptions);

}

interface ChartDataModel {
    data: ChartDataStructure;
    options: ChartDataOptions;
}

interface ChartDataStructure {
    labels: string[];
    datasets: ChartDataSet[];
}

interface LineDataStructure extends ChartDataStructure {
    datasets: LineDataSet[];
}

interface BarDataStructure extends ChartDataStructure {
    datasets: BarDataSet[];
}

interface ChartDataSet {
    /*
    * Dataset Description
    */
    description?: string;
    /*
    * Chart Fill color. You can use RGBA, RGB, HEX or HSL notation
    */
    fillColor?: string;
    /*
    * Chart edge color. You can use RGBA, RGB, HEX or HSL notation
    */
    strokeColor?: string;
    data: number[];
}

interface LineDataSet extends ChartDataSet {
    /*
    * Point color. You can use RGBA, RGB, HEX or HSL notation
    */
    pointColor?: string;
    /*
    * Point edge color. You can use RGBA, RGB, HEX or HSL notation
    */
    pointStrokeColor?: string;
}

interface BarDataSet extends ChartDataSet {
}

interface ChartDataOptions {
    /**
    * If we show the scale above the chart data
    */
    scaleOverlay?: boolean;
    /**
    * If we want to override with a hard coded scale
    */
    scaleOverride?: boolean;
    /**
    * The number of steps in a hard coded scale - Required if scaleOverride is true
    */
    scaleSteps?: number;
    /**
    * The value jump in the hard coded scale - Required if scaleOverride is true
    */
    scaleStepWidth?: number;
    /**
    * The scale starting value - Required if scaleOverride is true
    */
    scaleStartValue?: number;
    /**
    * Colour of the scale line
    */
    scaleLineColor?: string;
    /**
    * Pixel width of the scale line
    */
    scaleLineWidth?: number;
    /**
    * Whether to show labels on the scale
    */
    scaleShowLabels?: boolean;
    /**
    * Interpolated JS string - can access value
    */
    scaleLabel?: string;
    /**
    * Scale label font declaration for the scale label
    */
    scaleFontFamily?: string;
    /**
    * Scale label font size in pixels
    */
    scaleFontSize?: number;
    /**
    * Scale label font weight style
    */
    scaleFontStyle?: string;
    /**
    * Scale label font colour
    */
    scaleFontColor?: string;
    /**
    * Whether grid lines are shown across the chart
    */
    scaleShowGridLines?: boolean;
    /**
    * Colour of the grid lines
    */
    scaleGridLineColor?: string;
    /**
    * Width of the grid lines
    */
    scaleGridLineWidth?: number;
    /**
    * Whether to animate the chart
    */
    animation?: boolean;
    /**
    * Number of animation steps
    */
    animationSteps?: number;
    /**
    * Animation easing effect
    */
    animationEasing?: string;
    /**
    * Fires when the animation is complete
    */
    //onAnimationComplete(): void;
}

interface LineDataOptions extends ChartDataOptions {
    /**
    * Whether the line is curved between points
    */
    bezierCurve?: boolean;
    /**
    * Whether to show a dot for each point
    */
    pointDot?: boolean;
    /**
    * Radius of each point dot in pixels
    */
    pointDotRadius?: number;
    /**
    * Pixel width of point dot stroke
    */
    pointDotStrokeWidth?: number;
    /**
    * Whether to show a stroke for datasets
    */
    datasetStroke?: boolean;
    /**
    * Pixel width of dataset stroke
    */
    datasetStrokeWidth?: number;
    /**
    * Whether to fill the dataset with a colour
    */
    datasetFill?: boolean;
}

interface BarDataOptions extends ChartDataOptions {
    /**
    * If there is a stroke on each bar
    */
    barShowStroke?: boolean;
    /**
    * Pixel width of the bar stroke
    */
    barStrokeWidth?: number;
    /**
    * Spacing between each of the X value sets
    */
    barValueSpacing?: number;
    /**
    * Spacing between data sets within X values
    */
    barDatasetSpacing?: number;
}