﻿declare module angular {
    interface IAngularStatic {
        publish(eventName: string, ...args: any[]);
        subscribe(eventName: string, handler: (...args: any[]) => void);
        unsubscribe(eventName: string, handler: (...args: any[]) => void);
    }
}