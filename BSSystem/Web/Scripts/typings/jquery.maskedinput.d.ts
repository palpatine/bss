﻿interface JQuery {
    mask(mask: string, settings?: IMaskedInputSettings);
}

interface IMaskedInputSettings {
    placeholder?: string;
    completed: (input: string) => void;
}