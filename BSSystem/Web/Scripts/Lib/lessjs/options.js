﻿less = {
    async: false,                   // true | false*
    dumpLineNumbers: "comments",    // ''* | 'comments' | 'mediaquery' | 'all'
    env: "development",             // 'development'* | 'production'
    errorReporting: 'console',      // 'html'* | 'console' | 'function'
    fileAsync: false,               // true | false*
    logLevel: 2,                    // 0 | 1 | 2*
    poll: 1000,                     // The amount of time (in milliseconds) between polls while in watch mode.
    relativeUrls: false,            // true | false*
    rootpath: false                 // A path to add on to the start of every URL resource.
};