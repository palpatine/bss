'use strict';

angular.module('adaptv.adaptStrap.loadingindicator', [])
  .directive('adLoadingIcon', ['$adConfig', '$compile', function ($adConfig, $compile) {
    return {
      restrict: 'E',
      compile: function compile() {
        return {
          pre: function preLink(scope, element, attrs) {
            var loadingIconClass = attrs.loadingIconClass || $adConfig.iconClasses.loadingSpinner,
              ngStyleTemplate = (attrs.loadingIconSize) ?
                'ng-style="{\'font-size\': \'' + attrs.loadingIconSize + '\'}"' : '',
              template = '<i class="' + loadingIconClass + '" ' + ngStyleTemplate + '></i>';
            element.empty();
            element.append($compile(template)(scope));
          }
        };
      }
    };
  }])
  .directive('adLoadingOverlay', ['$adConfig', function ($adConfig) {
    return {
      restrict: 'E',
      templateUrl: 'loadingindicator/loadingindicator.tpl.html',
      scope: {
        loading: '=',
        zIndex: '@',
        position: '@',
        containerClasses: '@',
        loadingIconClass: '@',
        loadingIconSize: '@'
      },
      compile: function compile() {
        return {
          pre: function preLink(scope) {
            scope.loadingIconClass = scope.loadingIconClass || $adConfig.iconClasses.loading;
            scope.loadingIconSize = scope.loadingIconSize || '3em';
          }
        };
      }
    };
  }])
  .run(['$templateCache', function($templateCache) {
      $templateCache.put('loadingindicator/loadingindicator.tpl.html', '<div class="ad-loading-overlay-container"\
       ng-class="containerClasses"\
       ng-style="{\'z-index\': zIndex || \'1000\',\'position\': position || \'absolute\', opacity: 0.5}"\
       ng-show="loading">\
     <div class="wrapper">\
         <div class="loading-indicator">\
             <ad-loading-icon loading-icon-size="{{ loadingIconSize }}"\
      loading-icon-class="{{ loadingIconClass }}"></ad-loading-icon>\
</div>\
</div>\
</div>');
}]);
