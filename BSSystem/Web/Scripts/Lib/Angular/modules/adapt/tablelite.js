angular.module('adaptv.adaptStrap.tablelite', ['adaptv.adaptStrap.utils'])
/**
 * Use this directive if you need to render a simple table with local data source.
 */
    .directive('adTableLite', [
        '$parse', '$http', '$compile', '$filter',
        '$adConfig', 'adStrapUtils', 'adDebounce', 'adLoadLocalPage',
        function ($parse, $http, $compile, $filter, $adConfig, adStrapUtils, adDebounce, adLoadLocalPage) {
            'use strict';

            function _link(scope, element, attrs) {
                // We do the name spacing so the if there are multiple ad-table-lite on the scope,
                // they don't fight with each other.
                angular.extend(scope, {
                    attrs: attrs,
                    parent: scope.$parent,
                    //columnDefinition: scope.$parent.$eval(attrs.columnDefinition),
                    items: {
                        list: undefined,
                        allItems: undefined,
                        paging: {
                            currentPage: 1,
                            totalPages: undefined,
                            pageSize: undefined,
                            pageSizes: $parse(attrs.pageSizes)() || [10, 25, 50]
                        }
                    },
                    localConfig: {
                        localData: attrs.localDataSource ? adStrapUtils.parse(scope.$parent.$eval(attrs.localDataSource)) : null,
                        pagingArray: [],
                        selectable: attrs.selectedItems ? true : false,
                        draggable: attrs.draggable ? true : false,
                        iconDragable: $adConfig.iconClasses.draggable,
                        dragChange: scope.$parent.$eval(attrs.onDragChange),
                        showPaging: $parse(attrs.disablePaging)() ? false : true,
                        tableMaxHeight: attrs.tableMaxHeight
                    },
                    selectedItems: scope.$parent.$eval(attrs.selectedItems),
                    applyFilter: adStrapUtils.applyFilter,
                    isSelected: adStrapUtils.itemExistsInList,
                    addRemoveItem: adStrapUtils.addRemoveItemFromList,
                    addRemoveAll: adStrapUtils.addRemoveItemsFromList,
                    allSelected: adStrapUtils.itemsExistInList,
                    readProperty: adStrapUtils.getObjectProperty
                });

                scope.$parent.$watch($parse(attrs.localDataSource), function (value) {
                    if (value) {
                        scope.localConfig.localData = value;
                        scope.loadPage(1);
                    }
                });

                scope.$parent.$watch($parse(attrs.selectedItems), function (value) {
                    scope.selectedItems = value;
                });

                // ---------- Local data ---------- //
                var placeHolder = null,
                    pageButtonElement = null,
                    validDrop = false,
                    cacheDropElement = null,
                    initialPos;

                var findDefaultSorter = function (columnDefinitions) {
                    if (columnDefinitions) {
                        var sortedColumn = _.find(columnDefinitions, function (item) {
                            return item.sorted;
                        });

                        if (sortedColumn) {
                            scope.localConfig.predicate = sortedColumn.sortKey;
                            scope.localConfig.reverse = !!sortedColumn.reversed;
                        }
                    }
                };

                if (scope.columnDefinition) {
                    findDefaultSorter(scope.columnDefinition);
                }

                function moveElementNode(nodeToMove, relativeNode, dragNode) {
                    if (relativeNode.next()[0] === nodeToMove[0] || relativeNode.next()[0] === dragNode[0]) {
                        relativeNode.before(nodeToMove);
                    } else if (relativeNode.prev()[0] === nodeToMove[0] || relativeNode.prev()[0] === dragNode[0]) {
                        relativeNode.after(nodeToMove);
                    }
                }

                scope.items.paging.pageSize = scope.items.paging.pageSizes[0];

                // ---------- ui handlers ---------- //
                scope.loadPage = adDebounce(function (page) {
                    var itemsObject = scope.localConfig.localData,
                        params;
                    params = {
                        pageNumber: page,
                        pageSize: (scope.localConfig.showPaging) ? scope.items.paging.pageSize : itemsObject.length,
                        sortKey: scope.localConfig.predicate,
                        sortDirection: scope.localConfig.reverse,
                        localData: itemsObject
                    };

                    var response = adLoadLocalPage(params);
                    scope.items.list = response.items;
                    scope.items.allItems = response.allItems;
                    scope.items.paging.currentPage = response.currentPage;
                    scope.items.paging.totalPages = response.totalPages;
                    scope.localConfig.pagingArray = response.pagingArray;
                }, 100);

                scope.loadNextPage = function () {
                    if (scope.items.paging.currentPage + 1 <= scope.items.paging.totalPages) {
                        scope.loadPage(scope.items.paging.currentPage + 1);
                    }
                };

                scope.loadPreviousPage = function () {
                    if (scope.items.paging.currentPage - 1 > 0) {
                        scope.loadPage(scope.items.paging.currentPage - 1);
                    }
                };

                scope.loadLastPage = function () {
                    if (!scope.localConfig.disablePaging) {
                        scope.loadPage(scope.items.paging.totalPages);
                    }
                };

                scope.pageSizeChanged = function (size) {
                    scope.items.paging.pageSize = size;
                    scope.loadPage(1);
                };

                scope.sortByColumn = function (column) {
                    if (column.sortKey) {
                        if (column.sortKey !== scope.localConfig.predicate) {
                            scope.localConfig.predicate = column.sortKey;
                            scope.localConfig.reverse = false;
                        } else {
                            if (scope.localConfig.reverse === false) {
                                scope.localConfig.reverse = true;
                            } else {
                                scope.localConfig.reverse = false;
                            }
                        }
                        scope.loadPage(scope.items.paging.currentPage);
                    }
                };

                scope.onDragStart = function (data, dragElement) {
                    var parent = dragElement.parent();
                    placeHolder = $('<tr><td colspan=' + dragElement.find('td').length + '>&nbsp;</td></tr>');
                    initialPos = dragElement.index() + ((scope.items.paging.currentPage - 1) *
                        scope.items.paging.pageSize) - 1;
                    if (dragElement[0] !== parent.children().last()[0]) {
                        dragElement.next().before(placeHolder);
                    } else {
                        parent.append(placeHolder);
                    }
                };

                scope.onDragEnd = function () {
                    placeHolder.remove();
                };

                scope.onDragOver = function (data, dragElement, dropElement) {
                    if (placeHolder) {
                        // Restricts valid drag to current table instance
                        if (cacheDropElement) {
                            if (cacheDropElement[0] !== dropElement[0]) {
                                moveElementNode(placeHolder, dropElement, dragElement);
                                cacheDropElement = dropElement;
                            }
                        } else {
                            moveElementNode(placeHolder, dropElement, dragElement);
                            cacheDropElement = dropElement;
                        }
                    }
                };

                scope.onDropEnd = function (data, dragElement) {
                    var endPos;
                    if (placeHolder) {
                        // Restricts drop to current table instance
                        if (placeHolder.next()[0]) {
                            placeHolder.next().before(dragElement);
                        } else if (placeHolder.prev()[0]) {
                            placeHolder.prev().after(dragElement);
                        }
                        placeHolder.remove();
                        validDrop = true;
                        endPos = dragElement.index() + ((scope.items.paging.currentPage - 1) *
                          scope.items.paging.pageSize) - 1;
                        adStrapUtils.moveItemInList(initialPos, endPos, scope.localConfig.localData);

                        if (scope.localConfig.dragChange) {
                            scope.localConfig.dragChange(initialPos, endPos, data);
                        }
                    }
                    if (pageButtonElement) {
                        pageButtonElement.removeClass('btn-primary');
                        pageButtonElement = null;
                    }
                };

                scope.onNextPageButtonOver = function (data, dragElement, dropElement) {
                    if (pageButtonElement) {
                        pageButtonElement.removeClass('btn-primary');
                        pageButtonElement = null;
                    }
                    if (dropElement.attr('disabled') !== 'disabled') {
                        pageButtonElement = dropElement;
                        pageButtonElement.addClass('btn-primary');
                    }
                };

                scope.onNextPageButtonDrop = function (data, dragElement) {
                    var endPos;
                    if (pageButtonElement) {
                        validDrop = true;
                        if (pageButtonElement.attr('id') === 'btnPrev') {
                            endPos = (scope.items.paging.pageSize * (scope.items.paging.currentPage - 1)) - 1;
                        }
                        if (pageButtonElement.attr('id') === 'btnNext') {
                            endPos = scope.items.paging.pageSize * scope.items.paging.currentPage;
                        }
                        adStrapUtils.moveItemInList(initialPos, endPos, scope.localConfig.localData);
                        placeHolder.remove();
                        dragElement.remove();
                        if (scope.localConfig.dragChange) {
                            scope.localConfig.dragChange(initialPos, endPos, data);
                        }
                        pageButtonElement.removeClass('btn-primary');
                        pageButtonElement = null;
                    }
                };

                // ---------- initialization and event listeners ---------- //
                //We do the compile after injecting the name spacing into the template.
                scope.loadPage(1);
                attrs.tableClasses = attrs.tableClasses || 'table table-striped table-hover';
                attrs.paginationBtnGroupClasses = attrs.paginationBtnGroupClasses || 'btn-group btn-group-sm';
                scope.$watch(attrs.localDataSource, function () {
                    scope.loadPage(scope.items.paging.currentPage);
                }, true);
            }

            return {
                restrict: 'E',
                link: _link,
                scope: {
                    vm: "=",
                    columnDefinition: "="
                },
                templateUrl: '~/Content/Lib/adapt/Templates/tablelite.tpl.html'
            };
        }]);
