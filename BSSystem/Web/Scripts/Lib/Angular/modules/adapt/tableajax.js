//TODO: rewrite to TS

angular.module('adaptv.adaptStrap.tableajax', ['adaptv.adaptStrap.utils', 'adaptv.adaptStrap.loadingindicator'])
/**
 * Use this directive if you need to render a table that loads data from ajax.
 */
  .directive('adTableAjax',
  ['$parse', '$compile', '$templateCache', '$adConfig', '$modal', 'adLoadPage', 'adDebounce', 'adStrapUtils', '$controller', '$document', '$http',
    function ($parse, $compile, $templateCache, $adConfig, $modal, adLoadPage, adDebounce, adStrapUtils, $controller, $document, $http) {
        'use strict';
        function _link(scope, element, attrs) {
            var lastRequestToken;

            // ---------- ui handlers ---------- //
            scope.loadPage = adDebounce(function (page) {
                if (!scope.ajaxConfig.url) {
                    scope.items.list = null;
                    return;
                }
                lastRequestToken = Math.random();
                scope.localConfig.loadingData = true;
                var pageLoader = scope.$parent.$eval(attrs.pageLoader) || adLoadPage,
                  params = {
                      pageNumber: page,
                      pageSize: scope.items.paging.pageSize,
                      sortKey: scope.localConfig.predicate,
                      sortDirection: scope.localConfig.reverse,
                      ajaxConfig: scope.ajaxConfig,
                      token: lastRequestToken,
                      filterProperties: generateFilterProperties()
                  },
                  successHandler = function (response) {
                      if (response.token === lastRequestToken) {
                          scope.items.list = response.items;
                          scope.items.paging.totalPages = response.totalPages;
                          scope.items.paging.currentPage = response.currentPage;
                          scope.localConfig.pagingArray = response.pagingArray;
                          scope.localConfig.loadingData = false;
                      }
                  },
                  errorHandler = function () {
                      scope.localConfig.loadingData = false;
                  };

                pageLoader(params).then(successHandler, errorHandler);
            });

            scope.loadNextPage = function () {
                if (!scope.localConfig.loadingData) {
                    if (scope.items.paging.currentPage + 1 <= scope.items.paging.totalPages) {
                        scope.loadPage(scope.items.paging.currentPage + 1);
                    }
                }
            };

            scope.loadPreviousPage = function () {
                if (!scope.localConfig.loadingData) {
                    if (scope.items.paging.currentPage - 1 > 0) {
                        scope.loadPage(scope.items.paging.currentPage - 1);
                    }
                }
            };

            scope.loadLastPage = function () {
                if (!scope.localConfig.loadingData) {
                    if (scope.items.paging.currentPage !== scope.items.paging.totalPages) {
                        scope.loadPage(scope.items.paging.totalPages);
                    }
                }
            };

            scope.pageSizeChanged = function (size) {
                if (Number(size) !== scope.items.paging.pageSize) {
                    scope.items.paging.pageSize = Number(size);
                    scope.loadPage(1);
                }
            };

            scope.sortByColumn = function (column) {
                if (column.sortKey) {
                    var state = { predicate: scope.localConfig.predicate, reverse: scope.localConfig.reverse };
                    if (column.sortKey !== scope.localConfig.predicate) {
                        scope.localConfig.predicate = column.sortKey;
                        scope.localConfig.reverse = false;
                    } else {
                        if (scope.localConfig.reverse === true) {
                            scope.localConfig.reverse = false;
                        } else {
                            scope.localConfig.reverse = true;
                        }
                    }

                    var onSortChange = scope.$parent.$eval(attrs.onSortChanged);
                    if (onSortChange) {
                        onSortChange(state, scope.localConfig);
                    }
                    scope.loadPage(scope.items.paging.currentPage);
                }
            };

            scope.deleteConfirmation = function (parameters) {
                parameters.bodyMessage = 'Czy chesz usun�� element?';
                var modalInstance = $modal.open({
                    templateUrl: '~/Content/Lib/adapt/Templates/tableajaxConfirmation.tpl.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        parameters: function () {
                            return parameters;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    if (parameters.item) {
                        scope.deleteItem(parameters.vm, parameters.item);
                    } else {
                        scope.deleteMany(parameters.vm);
                    }
                }, function () {
                    if (!parameters.item) {
                        scope.cancelDeleting();
                    }
                });
            };

            scope.isAnyEdit = function () {
                return _.some(scope.items.list, function (x) { return x.$isEdited; });
            };

            scope.addItem = function (vm) {
                var newItem = { date: moment().toDate() };
                scope.items.list.splice(0, 0, newItem);
                scope.beginEdit(vm, newItem);
            };

            scope.beginEdit = function (vm, item) {
                $parse(tableConfiguration.beginEdit)({ vm: vm }, {
                    item: item,
                    onLocked: function (lockUserDisplayName) {
                        if (lockUserDisplayName)
                            alert(lockUserDisplayName);
                    }
                });
            };

            scope.cancelEdit = function (vm, item) {
                if (item.id) {
                    $parse(tableConfiguration.cancelEdit)({ vm: vm }, { item: item });
                    item.$isEdited = false;
                } else {
                    scope.items.list.splice(0, 1);
                }
            };

            scope.saveItem = function (vm) {
                $parse(tableConfiguration.saveItem)({ vm: vm }, {
                    onSuccess: function () {
                        scope.loadPage(scope.items.paging.currentPage);
                    }
                });
            };

            scope.deleteItem = function (vm, item) {
                $parse(tableConfiguration.deleteItem)({ vm: vm }, {
                    item: item,
                    onSuccess: function () {
                        scope.loadPage(scope.items.paging.currentPage);
                    }
                });
            };

            scope.inDeleting = false;
            scope.toogleDeletingState = function () {
                scope.inDeleting = !scope.inDeleting;
            };
            scope.cancelDeleting = function () {
                scope.inDeleting = false;
                _.each(scope.items.list, function (x) {
                    x.$toDelete = null;
                });
            };
            scope.deleteMany = function (vm) {
            };
            var toggleFilter = function (definition) {
                if (definition.filter.isFilterVisible) {
                    definition.filter.dispose();
                }
                else {

                    definition.filter.dispose = function () {
                        $document.unbind('click', closeHandler);
                        $document.unbind('scroll', scrollHandle);
                    };

                    var closeHandler = function (event) {
                        var menuElement = angular.element("div[id='filter" + definition.filter.id + "']");
                        if (menuElement == event.target || menuElement[0].contains(event.target)) {
                            return;
                        }
                        definition.filter.isFilterVisible = false;
                        definition.filter.dispose();
                        scope.$apply();
                    };
                    var scrollHandle = function (event) {
                        definition.filter.isFilterVisible = false;
                        definition.filter.dispose();
                        scope.$apply();
                    };
                    setTimeout(function () {
                        $document.bind('click', closeHandler);
                        $document.bind('scroll', scrollHandle);
                    });
                }

                definition.filter.isFilterVisible = !definition.filter.isFilterVisible;
            };

            var applyFilter = function (definition) {
                toggleFilter(definition);
                scope.loadPage(1);
            };

            var generateFilterProperties = function () {
                var filterProperties = { items: [] };

                _.each(scope.tableConfiguration.columnDefinition, function (item) {
                    if (item.filter && item.filter.value) {
                        var filter = {
                            propertyName: item.filter.filterKey,
                            propertyValue: item.filter.value,
                            filterOption: item.filter.comparitionOption

                        };

                        if (filterProperties.items.length) {
                            filterProperties.items[filterProperties.items.length - 1].isOr = false;
                        }

                        filterProperties.items.push(filter);
                    }
                });

                if (filterProperties.items.length)
                    return filterProperties;
                return null;
            };
            var initialize = function (tableConfiguration) {
                // We do the name spacing so the if there are multiple ad-table-ajax on the scope,
                // they don't fight with each other.
                angular.extend(scope, {
                    attrs: attrs,
                    parent: scope.$parent,
                    tableConfiguration: tableConfiguration,
                    items: {
                        list: undefined,
                        paging: {
                            currentPage: 1,
                            totalPages: undefined,
                            pageSize: undefined,
                            pageSizes: $parse(attrs.pageSizes)() || [10, 25, 50]
                        }
                    },
                    localConfig: {
                        pagingArray: [],
                        loadingData: false
                    },
                    ajaxConfig: scope.$parent.$eval(attrs.ajaxConfig),
                    applyFilter: applyFilter,
                    readProperty: adStrapUtils.getObjectProperty,
                    toggleFilter: toggleFilter
                });

                // ---- Configuration check ---- //

                if (tableConfiguration.isInlineEdit === true) {
                    if (!tableConfiguration.beginEdit)
                        throw 'Missing: tableConfiguration.beginEdit';
                    if (!tableConfiguration.cancelEdit)
                        throw 'Missing: tableConfiguration.cancelEdit';
                    if (!tableConfiguration.saveItem)
                        throw 'Missing: tableConfiguration.saveItem';
                    if (!tableConfiguration.deleteItem)
                        throw 'Missing: tableConfiguration.deleteItem';
                    if (!tableConfiguration.editorController)
                        throw 'Missing: tableConfiguration.editorController';

                    var hasEditTemplate = _.some(tableConfiguration.columnDefinition, function (x) {
                        return x.editTemplate > '';
                    });
                    if (!hasEditTemplate)
                        throw 'Missing: tableConfiguration.columnDefinition.editTemplate';
                }

                // ---------- Local data ---------- //
                scope.items.paging.pageSize = scope.items.paging.pageSizes[0];
                var sortedColumn = _.find(tableConfiguration.columnDefinition, function (item) {
                    return item.sorted;
                });
                if (sortedColumn) {
                    scope.localConfig.predicate = sortedColumn.sortKey;
                    scope.localConfig.reverse = !!sortedColumn.reversed;
                }

                var ctrlInstance, ctrlLocals = {};
                if (tableConfiguration.editorController) {
                    ctrlLocals.$scope = {};

                    ctrlInstance = $controller(tableConfiguration.editorController, ctrlLocals);
                    ctrlLocals.$scope.vm = ctrlInstance;
                    scope.ctrlInstance = ctrlInstance;
                    //if (modalOptions.controllerAs) {
                    //    modalScope[modalOptions.controllerAs] = ctrlInstance;
                    //}
                }
                // ---------- initialization and event listeners ---------- //
                //We do the compile after injecting the name spacing into the template.
                if (scope.ajaxConfig && scope.ajaxConfig.url)
                    scope.loadPage(1);

                // reset on parameter change
                scope.$parent.$watch($parse(attrs.ajaxConfig), function (value) {
                    scope.ajaxConfig = value;
                    scope.loadPage(1);
                });

                attrs.tableClasses = attrs.tableClasses || 'table table-striped table-hover';
                attrs.paginationBtnGroupClasses = attrs.paginationBtnGroupClasses || 'btn-group btn-group-sm';
            }

            if (attrs.tableConfigurationUrl) {
                $http.get(attrs.tableConfigurationUrl)
                    .success(initialize);
            } else {
                var configuration = scope.$parent.$eval(attrs.tableConfiguration);
                if (configuration)
                    initialize(configuration);

                scope.$watch(function () {
                    return scope.$parent.$eval(attrs.tableConfiguration);
                },
                    function (newValue) {
                        if (newValue)
                            initialize(newValue);
                    });
            }
        }

        return {
            restrict: 'E',
            link: _link,
            scope: {},
            templateUrl: '~/Content/Lib/adapt/Templates/tableajax.tpl.html'
        };
    }])
.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'parameters',
    function ($scope, $modalInstance, parameters) {
        $scope.bodyMessage = parameters.bodyMessage;

        $scope.ok = function () {
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
