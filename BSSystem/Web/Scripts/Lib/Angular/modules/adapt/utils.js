angular.module('adaptv.adaptStrap.utils', [])
  .factory('adStrapUtils', ['$filter', function ($filter) {
      var evalObjectProperty = function (obj, property) {
          var arr = property.split('.');
          if (obj) {
              while (arr.length) {
                  if (obj) {
                      obj = obj[arr.shift()];
                  }
              }
          }
          return obj;
      },
        applyFilter = function (value, filter, item) {
            var parts,
              filterOptions;

            if (value && ('function' === typeof value)) {
                return value(item);
            }
            if (filter) {
                parts = filter.split(':');
                filterOptions = parts[1];
                if (filterOptions) {
                    value = $filter(parts[0])(value, filterOptions);
                } else {
                    value = $filter(parts[0])(value);
                }
            }
            return value;
        },
        itemExistsInList = function (compareItem, list) {
            var exist = false;
            list.forEach(function (item) {
                if (angular.equals(compareItem, item)) {
                    exist = true;
                }
            });
            return exist;
        },
        itemsExistInList = function (items, list) {
            var exist = true,
              i;
            for (i = 0; i < items.length; i++) {
                if (itemExistsInList(items[i], list) === false) {
                    exist = false;
                    break;
                }
            }
            return exist;
        },
        addItemToList = function (item, list) {
            list.push(item);
        },
        removeItemFromList = function (item, list) {
            var i;
            for (i = list.length - 1; i > -1; i--) {
                if (angular.equals(item, list[i])) {
                    list.splice(i, 1);
                }
            }
        },
        addRemoveItemFromList = function (item, list) {
            var i,
              found = false;
            for (i = list.length - 1; i > -1; i--) {
                if (angular.equals(item, list[i])) {
                    list.splice(i, 1);
                    found = true;
                }
            }
            if (found === false) {
                list.push(item);
            }
        },
        addItemsToList = function (items, list) {
            items.forEach(function (item) {
                if (!itemExistsInList(item, list)) {
                    addRemoveItemFromList(item, list);
                }
            });
        },
        addRemoveItemsFromList = function (items, list) {
            if (itemsExistInList(items, list)) {
                list.length = 0;
            } else {
                addItemsToList(items, list);
            }
        },
        moveItemInList = function (startPos, endPos, list) {
            if (endPos < list.length) {
                list.splice(endPos, 0, list.splice(startPos, 1)[0]);
            }
        },
        parse = function (items) {
            var itemsObject = [];
            if (angular.isArray(items)) {
                itemsObject = items;
            } else {
                angular.forEach(items, function (item) {
                    itemsObject.push(item);
                });
            }
            return itemsObject;
        },
        getObjectProperty = function (item, property) {
            if (property && ('function' === typeof property)) {
                return property(item);
            }
            var arr = property.split('.');
            while (arr.length) {
                item = item[arr.shift()];
            }
            return item;
        };

      return {
          evalObjectProperty: evalObjectProperty,
          applyFilter: applyFilter,
          itemExistsInList: itemExistsInList,
          itemsExistInList: itemsExistInList,
          addItemToList: addItemToList,
          removeItemFromList: removeItemFromList,
          addRemoveItemFromList: addRemoveItemFromList,
          addItemsToList: addItemsToList,
          addRemoveItemsFromList: addRemoveItemsFromList,
          moveItemInList: moveItemInList,
          parse: parse,
          getObjectProperty: getObjectProperty
      };
  }])
  .factory('adDebounce', ['$timeout', '$q', function ($timeout, $q) {
      'use strict';
      var deb = function (func, delay, immediate, ctx) {
          var timer = null,
            deferred = $q.defer(),
            wait = delay || 300;
          return function () {
              var context = ctx || this,
                args = arguments,
                callNow = immediate && !timer,
                later = function () {
                    if (!immediate) {
                        deferred.resolve(func.apply(context, args));
                        deferred = $q.defer();
                    }
                };
              if (timer) {
                  $timeout.cancel(timer);
              }
              timer = $timeout(later, wait);
              if (callNow) {
                  deferred.resolve(func.apply(context, args));
                  deferred = $q.defer();
              }
              return deferred.promise;
          };
      };

      return deb;
  }])
  .directive('adCompileTemplate', ['$compile', function ($compile) {
      var compileTemplate = function (scope, element, attrs) {
          scope.$watch(
            function (scope) {
                return scope.$eval(attrs.adCompileTemplate);
            },
            function (value) {

                element.html(value);
                $compile(element.contents())(scope);
            }
          );
      };
      return compileTemplate;
  }])
    .directive('adBindTemplateToScope', ['$compile', function ($compile) {
        return {
            restrict: 'EA',
            replace: true,
            link: function (scope, $element, $attr, ctrl) {
                var parentScope = scope.$eval($attr.parentScope) || scope.$parent;
                var template = "<div ad-compile-transcluded-template='context.template' ></div>";
                var element = angular.element(template);
                var innerScope = parentScope.$new();
                innerScope.context = { templateUrl: "" };
                scope.$watch($attr.template, function (value) {
                    innerScope.context.template = value;
                });

                var data = scope.$eval($attr.parameters);
                if (data) {
                    angular.extend(innerScope, data);
                }

                var item = $compile(element)(innerScope);
                element.remove();
                $element.append(item);

                $element.on('$destroy', function () {
                    innerScope.$destroy();
                });
            }
        }
    }])
    .directive('adBindTemplateUrlToScope', ['$compile', function ($compile) {
        return {
            restrict: 'EA',
            replace: true,
            link: function (scope, $element, $attr, ctrl) {
                var parentScope = scope.$eval($attr.parentScope) || scope.$parent;
                var template = "<div ng-include='context.templateUrl' ng-style='context.ngStyle' style='{{context.style}}' ng-class='context.ngClass' class='{{context.class}}'></div>";
                var element = angular.element(template);
                var innerScope = parentScope.$new();
                innerScope.context = { templateUrl: "", ngStyle: scope.$eval($attr['ng-style']), style: $attr['style'], ngClass: scope.$eval($attr['ng-class']), 'class': $attr['class'] };
                scope.$watch($attr.templateUrl, function (value) {
                    innerScope.context.templateUrl = value;
                });
                var data = scope.$eval($attr.parameters);
                if (data) {
                    angular.extend(innerScope, data);
                }
                var item = $compile(element)(innerScope);
                element.remove();
                $element.append(item);
                $element.on('$destroy', function () {                    
                    innerScope.$destroy();
                });
            }
        }
    }])
    .directive('adTooltip', [function () {
        return {
            restrict: 'A',
            template: '<div ad-popup-in-body template-url="targetTemplateUrl" is-open="isOpen" ng-transclude></div>',
            transclude: true,
            link: function (scope, $element, $attr, ctrl) {

                scope.isOpen = false;

                scope.$watch($attr.templateUrl, function (value) {
                    scope.targetTemplateUrl = value;
                });

                $element.hover(function () {
                    if (!$attr.adTooltip || scope.$eval($attr.adTooltip)) {
                        scope.isOpen = true;
                        scope.$apply();
                    }
                },
                    function () {
                        scope.isOpen = false;
                        scope.$apply();
                    });
            }
        };

    }])

    .directive('adInjectTemplate', ['$compile', '$document', '$position', '$parse', function ($compile, $document, $position, $parse) {
        return {
            restrict: 'A',
            link: function (scope, $element, $attr, ctrl) {

                var id = ("adInjectTemplate" + Math.random()).replace('.', '');
                if ($attr.injectionId) {
                    $parse($attr.injectionId).assign(scope, id);
                }
                var template = '<div id="' + id + '" ng-include="context.templateUrl"></div>';

                var innerScope = scope.$new();
                innerScope.context = {};
                scope.$watch($attr.templateUrl, function (value) {
                    innerScope.context.templateUrl = value || '';
                });

                var element = angular.element(template);
                var item = $compile(element)(innerScope);
                element.remove();
                $document.find($attr.targetSelector).append(item);

                $element.on('$destroy', function () {
                    angular.element("#" + id).remove();
                    innerScope.$destroy();
                });
            }
        }
    }])

    .directive('adPopupInBody', ['$compile', '$document', '$position', '$parse', function ($compile, $document, $position, $parse) {
        return {
            restrict: 'A',
            link: function (scope, $element, $attr, ctrl) {

                var id = ("adPopupInBody" + Math.random()).replace('.', '');
                if ($attr.popupMenuId) {
                    $parse($attr.popupMenuId).assign(scope, id);
                }
                var parentPopup = $element.parents("[ad-popup]");
                var parentPopupId = parentPopup.attr("id") || "";

                var template = '<div ad-popup="'
                    + parentPopupId
                    + '" id="' +
                    id
                    + '" ng-style="{display: (context.isOpen && \'block\' ) || \'none\' , top: context.position.top+\'px\', left: context.position.left+\'px\'}" style="{{context.style}}" ' +
                'class= "dropdown-menu" ng-include="context.templateUrl"></div>';

                var anchorVertically = ($attr.anchorVertically || "left");
                var innerScope = scope.$new();
                innerScope.context = { style: $attr.popupStyle };
                var data = scope.$eval($attr.parameters);
                if (data) {
                    angular.extend(innerScope, data);
                }
                scope.$watch($attr.templateUrl, function (value) {
                    innerScope.context.templateUrl = value || '';
                });

                var created = false;
                var crateIfNotExists = function () {
                    if (created)
                        return;
                    created = true;
                    var element = angular.element(template);
                    var item = $compile(element)(innerScope);
                    element.remove();
                    $document.find('body').append(item);
                };

                var isOpen = $parse($attr.isOpen);

                var isinInnerPopup = function (target) {
                    var popupRootId = angular.element(target).attr("ad-popup") || angular.element(target).parents("[ad-popup]").attr("ad-popup");
                    if (popupRootId === id) {
                        return true;
                    }
                    if (popupRootId) {
                        return isinInnerPopup(angular.element("#" + popupRootId));
                    } else {
                        return false;
                    }
                }

                var closeDropdown = function (evt) {
                    if (!innerScope.context.isOpen
                        || angular.element(evt.target).attr("id") === id
                        || $.contains(angular.element("#" + id)[0], evt.target)
                        || isinInnerPopup(evt.target)) return;

                    evt.preventDefault();
                    evt.stopPropagation();

                    scope.$applyAsync(function () {
                        isOpen.assign(scope, false);
                    });
                }

                var escapeKeyBind = function (evt) {
                    if (evt.which === 27) {
                        closeDropdown(evt);
                    }
                }
                var watchDispose;
                $element.on('$destroy', function () {
                    $document.unbind('click', closeDropdown);
                    $document.unbind('keydown', escapeKeyBind);
                    angular.element("#" + id).remove();
                    innerScope.$destroy();
                    if (watchDispose) {
                        watchDispose();
                    }
                });

                if (anchorVertically === 'right') {
                    scope.$watch(function () {
                        return angular.element("#" + id).width();
                    }, function () {
                        innerScope.context.position = innerScope.context.position || {};
                        innerScope.context.position.left = $position.offset($element).left - angular.element("#" + id).width() + $element.width();
                    });
                } else if (anchorVertically === 'left') {
                    scope.$watch(function () {
                        return angular.element("#" + id).width();
                    }, function () {
                        innerScope.context.position = innerScope.context.position || {};
                        innerScope.context.position.left = $position.offset($element).left;
                    });
                }
                scope.$applyAsync(function () {
                    watchDispose = scope.$watch($attr.isOpen, function (value) {
                        if (value) {
                            crateIfNotExists();
                            innerScope.context.position = $position.offset($element);
                            innerScope.context.position.top = innerScope.context.position.top + $element.prop('offsetHeight');
                            if (anchorVertically === 'right') {
                                innerScope.context.position.left = innerScope.context.position.left - angular.element("#" + id).width() + $element.width();
                            } else if (anchorVertically === 'left') {
                                innerScope.context.position.left = innerScope.context.position.left;
                            }
                            setTimeout(function () {
                                $document.bind('click', closeDropdown);
                                $document.bind('keydown', escapeKeyBind);
                            }, 0);
                        } else {
                            $document.unbind('click', closeDropdown);
                            $document.unbind('keydown', escapeKeyBind);
                        }

                        innerScope.context.isOpen = value;
                    });
                });
            }
        }
    }])
     .directive('adCompileTranscludedTemplate', ['$anchorScroll', '$animate', '$compile', function ($anchorScroll, $animate, $compile) {
         return {
             restrict: 'A',
             priority: 400,
             terminal: true,
             transclude: 'element',
             controller: angular.noop,
             compile: function (element, attr) {
                 var onloadExp = attr.onload || '',
                      autoScrollExp = attr.autoscroll;

                 return function (scope, $element, $attr, ctrl, $transclude) {
                     var currentScope,
                         previousElement,
                         currentElement;

                     var cleanupLastIncludeContent = function () {
                         if (previousElement) {
                             previousElement.remove();
                             previousElement = null;
                         }
                         if (currentScope) {
                             currentScope.$destroy();
                             currentScope = null;
                         }
                         if (currentElement) {
                             $animate.leave(currentElement).then(function () {
                                 previousElement = null;
                             });
                             previousElement = currentElement;
                             currentElement = null;
                         }
                     };
                     $element.on('$destroy', function () {
                                 cleanupLastIncludeContent();
                             });


                     scope.$watch(attr.adCompileTranscludedTemplate, function ngIncludeWatchAction(template) {
                         var afterAnimation = function () {
                             if (angular.isDefined(autoScrollExp) && (!autoScrollExp || scope.$eval(autoScrollExp))) {
                                 $anchorScroll();
                             }
                         };

                         if (template) {
                             var newScope = scope.$new();

                             ctrl.template = template;

                             var clone = $transclude(newScope, function (clone) {
                                 cleanupLastIncludeContent();
                                 $animate.enter(clone, null, $element).then(afterAnimation);
                             });

                             clone.html(template);
                             $compile(clone.contents())(
                                   newScope,
                                       function namespaceAdaptedClone(c) {
                                           clone.empty();
                                           clone.append(c);
                                       },
                                   { futureParentElement: element });

                             currentScope = newScope;
                             currentElement = clone;
                             scope.$eval(onloadExp);                             
                         } else {
                             cleanupLastIncludeContent();
                             ctrl.template = null;
                         }
                     });
                 };
             }
         };       
     }])
  .factory('adLoadPage', ['$adConfig', '$http', 'adStrapUtils', 'bssODataQuerySerializer', '$q', 'PostDeserializationObjectProcessor',
      function ($adConfig, $http, adStrapUtils, bssODataQuerySerializer, $q, postDeserializationObjectProcessor) {
          return function (options) {
              var pagingConfig = angular.copy($adConfig.paging),
                  ajaxConfig = angular.copy(options.ajaxConfig);

              if (ajaxConfig.paginationConfig && ajaxConfig.paginationConfig.request) {
                  angular.extend(pagingConfig.request, ajaxConfig.paginationConfig.request);
              }
              if (ajaxConfig.paginationConfig && ajaxConfig.paginationConfig.response) {
                  angular.extend(pagingConfig.response, ajaxConfig.paginationConfig.response);
              }
              var orderPropertiesValue = null;
              if (options.orderProperties) {
                  orderPropertiesValue = options.orderProperties;
              }
              else if (options.sortKey) {
                  orderPropertiesValue = [{ propertyName: options.sortKey, orderDescending: options.sortDirection }];
              }

              var queryParameters = bssODataQuerySerializer.serialize({
                  pageSize: options.pageSize,
                  pageNumber: options.pageNumber,
                  orderProperties: orderPropertiesValue,
                  filterProperties: options.filterProperties
              });
              ajaxConfig.params = ajaxConfig.params || {};
              angular.extend(ajaxConfig.params, queryParameters);

              if (!options.ignoreTotalPages) {
                  ajaxConfig.params.$inlinecount = "allpages";
              }

              var promise;
              if (ajaxConfig.method === 'JSONP') {
                  promise = $http.jsonp(ajaxConfig.url + '?callback=JSON_CALLBACK', ajaxConfig);
              } else {
                  promise = $http(ajaxConfig);
              }

              var handlePagination = function (response) {
                  var TOTAL_PAGINATION_ITEMS = 5;
                  var minimumBound = options.pageNumber - Math.floor(TOTAL_PAGINATION_ITEMS / 2);
                  for (var i = minimumBound; i <= options.pageNumber; i++) {
                      if (i > 0) {
                          response.pagingArray.push(i);
                      }
                  }
                  while (response.pagingArray.length < TOTAL_PAGINATION_ITEMS) {
                      if (i > response.totalPages) {
                          break;
                      }
                      response.pagingArray.push(i);
                      i++;
                  }
              };

              var handleData = function (result) {
                  var response = {
                      items: options.ignoreTotalPages ? result : (adStrapUtils.evalObjectProperty(result, pagingConfig.response.itemsLocation) || []),
                      currentPage: options.pageNumber,
                      totalPages: options.ignoreTotalPages ? undefined : Math.ceil(
                          adStrapUtils.evalObjectProperty(result, pagingConfig.response.totalItems) /
                          options.pageSize
                      ),
                      pagingArray: [],
                      token: options.token
                  };
                  postDeserializationObjectProcessor.processDateValues(response.items, ajaxConfig.url);
                  return response;
              };

              return promise.then(function (result) {
                  var response;
                  if (result.status == 204) {
                      response = {
                          items: [],
                          currentPagge: 0,
                          totalPages: 0,
                          pagingArray: [],
                          token: options.token
                      };
                      handlePagination(response);
                      return response;
                  } else {
                      response = handleData(result.data);
                      handlePagination(response);
                      return response;
                  }
              });
          };
      }
  ])
  .factory('adLoadLocalPage', ['$filter', function ($filter) {
      return function (options) {
          var response = {
              items: undefined,
              currentPage: options.pageNumber,
              totalPages: undefined,
              pagingArray: [],
              token: options.token
          };
          var start = (options.pageNumber - 1) * options.pageSize,
            end = start + options.pageSize,
            i,
            itemsObject = options.localData,
            localItems;

          localItems = $filter('orderBy')(
            itemsObject,
            options.sortKey,
            options.sortDirection
          );
          response.items = localItems.slice(start, end);
          response.allItems = itemsObject;
          response.currentPage = options.pageNumber;
          response.totalPages = Math.ceil(
              itemsObject.length /
              options.pageSize
          );
          var TOTAL_PAGINATION_ITEMS = 5;
          var minimumBound = options.pageNumber - Math.floor(TOTAL_PAGINATION_ITEMS / 2);
          for (i = minimumBound; i <= options.pageNumber; i++) {
              if (i > 0) {
                  response.pagingArray.push(i);
              }
          }
          while (response.pagingArray.length < TOTAL_PAGINATION_ITEMS) {
              if (i > response.totalPages) {
                  break;
              }
              response.pagingArray.push(i);
              i++;
          }
          return response;
      };
  }]);