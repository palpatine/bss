﻿(function (angular) {

    var o = angular.element({});

    angular.subscribe = function () {
        o.on.apply(o, arguments);
    };

    angular.unsubscribe = function () {
        o.off.apply(o, arguments);
    };

    angular.publish = function () {
        o.trigger.apply(o, arguments);
    };

}(angular));