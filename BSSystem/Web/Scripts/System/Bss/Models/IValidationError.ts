﻿module SolvesIt.Bss.Models {
    export interface IValidationError {
        reference: string;
        message: string;
        errorCode: string;
        additionalValue: string;
    }
}