﻿module SolvesIt.Bss.Models {
    export interface IAssignmentJobRole extends IRoleAssignment {
        jobKind: IJobKind;
        jobValue: string;
    }
} 