﻿module SolvesIt.Bss.Models {
    export interface IUserPermissionsVerificationResponseViewModel {
        permissionPath: string;

        entities: number[];
    }
}