﻿module SolvesIt.Bss.Models{
    export interface IApplicationDescriptor {

        userData: SolvesIt.Bss.Models.IUserData;

        menuSections: IMenuSectionViewModel[];
    }
}