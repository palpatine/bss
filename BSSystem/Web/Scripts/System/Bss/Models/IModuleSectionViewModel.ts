﻿module SolvesIt.Bss.Models{
    export interface IMenuSectionViewModel {

        title: string;

        key: string;

        menuItems: IMenuItemViewModel[];

        menuSections: IMenuSectionViewModel[];

        iconName: string;

        navigationPosition: NavigationPositionKind;

        isOpen: boolean;
    }
}