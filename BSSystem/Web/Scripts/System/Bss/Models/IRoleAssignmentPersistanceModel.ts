﻿module SolvesIt.Bss.Models{

    export interface IRoleAssignmentPersistanceModel extends IAssignmentPersistanceModel {
        roleId: number;
    }
} 