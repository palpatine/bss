﻿module SolvesIt.Bss.Models{
    export interface IAssignmentPersistanceModel {
        id: number;
        elementId: number;
        dateStart: Date;
        dateEnd: Date;
    }
} 