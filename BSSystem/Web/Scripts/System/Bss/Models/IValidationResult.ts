﻿module SolvesIt.Bss.Models {
    export interface IValidationResult {
        isValid: boolean;
        errors: IValidationError[];
        id?: number;
        message?: string;
    }

}