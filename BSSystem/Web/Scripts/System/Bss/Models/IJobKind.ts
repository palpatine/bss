﻿module SolvesIt.Bss.Models{
    export interface IJobKind {
        id: string;
        displayName: string;
        pattern: RegExp;
    }

    export class JobKinds {
        static none: IJobKind = { id: "None", displayName: "Brak", pattern: /.*/ };

        static time: IJobKind = { id: "Time", displayName: "Etat", pattern: /^\d([.,]\d{1,3})?$/ };

        static hours: IJobKind = { id: "Hours", displayName: "Godziny", pattern: /^\d{1,10}$/ };
    }
}