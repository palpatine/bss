﻿module SolvesIt.Bss.Models{
    export interface IMenuItemViewModel {

        title: string;

        displayShortName: string;

        key: string;

        iconName: string;
    }
} 