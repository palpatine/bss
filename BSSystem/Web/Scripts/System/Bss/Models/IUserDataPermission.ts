﻿module SolvesIt.Bss.Models {
    export interface IUserDataPermission {
        path: string;

        isGlobal: boolean;

        promise: ng.IPromise<SolvesIt.Bss.Models.IUserPermissionsVerificationResponseViewModel>;

        entities: number[];
    }
}  