﻿module SolvesIt.Bss.Models{
    export interface IAssignmentJobRoleViewModel extends IRoleAssignmentViewModel {
        jobKind: number;
        jobValue: number;
    }
} 