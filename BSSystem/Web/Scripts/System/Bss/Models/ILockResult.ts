﻿module SolvesIt.Bss.Models{
    export interface ILockResult {
        operationToken: string;
        validationResult: SolvesIt.Bss.Models.IValidationResult
    }
}