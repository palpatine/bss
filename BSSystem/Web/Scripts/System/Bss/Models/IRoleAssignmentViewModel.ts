﻿module SolvesIt.Bss.Models{
    export interface IRoleAssignmentViewModel extends IAssignmentViewModel {
        roleId: number;
        roleDisplayName: string;
        roleDateStart: Date;
        roleDateEnd: Date;
    }
} 