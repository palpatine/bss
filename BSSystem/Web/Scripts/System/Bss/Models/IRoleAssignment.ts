﻿module SolvesIt.Bss.Models{

    export interface IRoleAssignment extends IAssignment {
        role: IDetailedMarker;
    }
} 