﻿module SolvesIt.Bss.Models {
    export interface IDetailedMarker extends INamedMarker {

        dateStart: Date;

        dateEnd: Date;
    }
} 