﻿module SolvesIt.Bss.Models{
    export interface IAssignmentJobRolePersistanceModel extends IRoleAssignmentPersistanceModel {
        jobKind: string;
        jobValue: number;
    }
} 