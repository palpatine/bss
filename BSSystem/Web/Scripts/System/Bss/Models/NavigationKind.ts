﻿module SolvesIt.Bss.Models{
    export enum NavigationPositionKind {
        Side = 1,
        Top,
    }
}