﻿module SolvesIt.Bss.Models{
    export interface IAssignment {
        id: number;
        dateStart: Date;
        dateEnd: Date;

        element: IDetailedMarker;
    }
}