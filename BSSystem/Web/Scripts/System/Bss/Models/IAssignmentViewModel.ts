﻿module SolvesIt.Bss.Models{

    export interface IAssignmentViewModel {
        id: number;
        displayName: string;
        elementId: number;
        elementDateStart: Date;
        elementDateEnd: Date;
        dateStart: Date;
        dateEnd: Date;
    }
} 