﻿module SolvesIt.Bss.Models {
    export interface INamedMarker extends IMarker {

        displayName: string;
    }
} 