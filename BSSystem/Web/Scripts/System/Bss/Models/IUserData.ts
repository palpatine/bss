﻿module SolvesIt.Bss.Models {
    export interface IUserData {

        effectivePermissions: IUserDataPermission[];

        user: SolvesIt.Bss.Common.Users.IUserNamedMarker;

        organization: SolvesIt.Bss.Common.Organizations.IOrganizationDetailedMarker;

        modules: string[];
    }
} 