﻿
module SolvesIt.Bss.Core {

    interface IAssignmentDisplayerAttributes {
        entities: string;
        elements: string;
        idSource: string;
        cacheId: string;
        name: string;
    }

    class AssignmentDisplayerControllerState {
        public tableConfigurationUrl: string;
        public source: string;
        public id: number;
    }

    class AssignmentDisplayerController {

        static $inject = ['stateCacheService', '$scope', '$compile', '$attrs', 'adLoadPage', 'routeFactory'];

        entities: string; //@
        elements: string; //@
        idSource: number; //=

        
        public state: AssignmentDisplayerControllerState;
        public cacheId: string;

        constructor(
            private stateCacheService: SolvesIt.Widgets.IStateCacheService,
            private $scope,
            private $compile: ng.ICompileService,
            private $attrs: IAssignmentDisplayerAttributes,
            private adLoadPage,
            private routeFactory: SolvesIt.Bss.Core.IRouteFactory
            ) {

            this.cacheId = $scope.$parent.$eval($attrs.cacheId);
            this.state = stateCacheService.getCacheFor(this.cacheId,() => new AssignmentDisplayerControllerState());

            this.state.tableConfigurationUrl = routeFactory.getAssignmentDisplayColumnsListFor(this.entities, this.elements);


            $scope.$watch(() => {
                return this.idSource;
            },
                (newValue) => {
                    if (!newValue) return;
                    this.state.source = routeFactory.getOdElementsFor(this.entities, newValue, this.elements);
                });
        }
    }



    class AssignmentDisplayerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict: string = 'E';
        scope = {
            entities: "@",
            elements: "@",
            idSource: "="
        };
        controller: string = 'assignmentDisplayerController';
        controllerAs: string = 'vm';
        bindToController = true;
        templateUrl: string = "~/Content/Templates/AssignmentDisplayer/Main.tpl.html";
    }
    SolvesIt.Bss.Application.getApplication()
        .controller('assignmentDisplayerController', AssignmentDisplayerController)
        .directive('bssAssignmentDisplay',(new AssignmentDisplayerDirective().factory()));
}