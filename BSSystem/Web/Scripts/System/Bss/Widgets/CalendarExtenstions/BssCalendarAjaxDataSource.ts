﻿module SolvesIt.Bss.Widgets.CalendarExtensions {
    interface IBssCalendarAjaxDataSourceAttributes {
        bssCalendarAjaxDataSource: string;
    }

    class BssCalendarAjaxDataSourceController implements SolvesIt.Widgets.Calendar.IDataProvider {
        public static $inject = ['$scope', '$attrs', '$q', '$injector'];
        private controller: SolvesIt.Widgets.Calendar.BssCalendarController;
        private address: string;
        private accessor: SolvesIt.Bss.Core.Accessors.ICalendarRelatedAccessor
        constructor(
            private $scope,
            private $attrs: IBssCalendarAjaxDataSourceAttributes,
            private $q: ng.IQService,
            private $injector: ng.auto.IInjectorService) {

            this.accessor = $injector.get<SolvesIt.Bss.Core.Accessors.ICalendarRelatedAccessor>($attrs.bssCalendarAjaxDataSource[0].toLowerCase() + $attrs.bssCalendarAjaxDataSource.substr(1) + "Accessor");
        }

        loadItems(dateRange: SolvesIt.Widgets.Calendar.IDateRange, displayCriteria: SolvesIt.Widgets.Calendar.IDisplayCriteria): ng.IPromise<SolvesIt.Widgets.Calendar.ICalendarItem[]> {
            return this.accessor.getAllForCalendar(null);
        }

        public setCalendarController(calendarController: SolvesIt.Widgets.Calendar.BssCalendarController) {
            this.controller = calendarController;
            this.controller.setDataProvider(this);
        }
    }

    class BssCalendarAjaxDataSourceHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ['bssCalendarAjaxDataSource', "^bssCalendar"];
        controller = "bssCalendarAjaxDataSourceController";

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var ownController = <BssCalendarAjaxDataSourceController>controller[0];
                    var bssCalendarController = <SolvesIt.Widgets.Calendar.BssCalendarController>controller[1];
                    ownController.setCalendarController(bssCalendarController);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarAjaxDataSource', (new BssCalendarAjaxDataSourceHandlerDirective()).factory())
        .controller('bssCalendarAjaxDataSourceController', BssCalendarAjaxDataSourceController);
} 