﻿module SolvesIt.Bss.Widgets.AssignmentEditor {
    interface IBssAssignmentEditorValidatorAttributes {
        bssAssignmentEditorValidator: string;
    }

    export interface IValidityHandler {
        $setValidity(key: string, isValid: boolean): void;
        $error: any;
    }

    export interface IBssAssignmentEditorValidatorController {
        setModelController(modelController: IViewValueProvider): void;
        register(modelController: IValidityHandler, id: number, propertyName: string): void;
        validate: (id: number) => void;
        model: IAssignmentEditorErrors;
    }

    export interface IViewValueProvider {
        $viewValue: IAssignmentEditorElement[];
    }

    export interface IAssignmentElementErrors {
        [propertyName: string]: IAssignmentElementPropertyErrors
    }

    export interface IAssignmentElementPropertyErrors {

        controller: IValidityHandler;
        errors: SolvesIt.Bss.Models.IValidationError[];
    }

    export interface IAssignmentEditorErrors {
        [id: number]: IAssignmentElementErrors;
    }

    class BssAssignmentEditorValidatorController implements BssAssignmentEditorValidatorController {
        private validationErrorReferencePattern = /\[(-?\d*?)\].(\w*)/;
        private modelController: IViewValueProvider;

        public model: IAssignmentEditorErrors = {};

        static $inject = [
            '$scope',
            '$attrs',
            'adDebounce'
        ];

        constructor(
            private $scope,
            $attrs: IBssAssignmentEditorValidatorAttributes,
            adDebounce) {
            this.validate = adDebounce(this.validateInner);
            $scope.errorsProvider = this;

            $scope.$watch(
                $attrs.bssAssignmentEditorValidator,
                (value: SolvesIt.Bss.Models.IValidationError[]) => {

                    _.each(this.model, (row) => {
                        _.each(row, (item) => {
                            if (item.controller) {
                                this.resetServerErrors(item.controller);
                                item.errors = [];
                            }
                        });
                    });

                    var values = this.modelController.$viewValue;
                    _.each(value, (error: SolvesIt.Bss.Models.IValidationError) => {
                        var matches = this.validationErrorReferencePattern.exec(error.reference);
                        if (matches && matches[1] && matches[2]) {
                            var id = parseInt(matches[1]);
                            var errorKey = "server" + error.errorCode;
                            var key = matches[2];
                            this.model[id][key].controller.$setValidity(errorKey, false);
                            this.model[id][key].errors.push(error);
                        }
                    });
                });
        }

        public setModelController(modelController: IViewValueProvider): void {
            this.modelController = modelController;
        }

        public register(modelController: IValidityHandler, id: number, propertyName: string): void {
            this.model[id] = this.model[id] || {};
            this.model[id][propertyName] = this.model[id][propertyName] || { controller: null, errors: [] };
            this.model[id][propertyName].controller = modelController;
        }

        private resetServerErrors(modelController: IValidityHandler) {
            _.each(modelController.$error, (er, key: string) => {
                if (key.indexOf("server") === 0) {
                    modelController.$setValidity(key, true);
                }
            });
        }

        public validate: (id: number) => void;

        private validateInner(id: number) {

            var properties = this.model[id];
            _.each(this.model[id], (current, propertyName) => {
                if (!current.errors) {
                    return;
                }
                var errors = current.errors;
                current.errors = [];
                this.resetServerErrors(current.controller);
                _.each(errors, (error: SolvesIt.Bss.Models.IValidationError) => {
                    if (error.errorCode == 'DateOverlapError' && error.additionalValue) {
                        var relatedId = parseInt(error.additionalValue);
                        var controller = this.model[relatedId][propertyName].controller;

                        var index = _.findIndex(this.model[relatedId][propertyName].errors, (err: SolvesIt.Bss.Models.IValidationError) => {
                            return err.additionalValue === id.toString();
                        });
                        this.model[relatedId][propertyName].errors.splice(index, 1);
                        if (!_.find(this.model[relatedId][propertyName].errors, (err: SolvesIt.Bss.Models.IValidationError) => {
                            return err.errorCode === error.errorCode;
                        })) {
                            controller.$setValidity("server" + error.errorCode, true);
                        }
                    }
                });
            });

            var values: IAssignmentEditorElement[] = this.modelController.$viewValue;
            _.each(values, (item) => {
                if (item.dateStart) {
                    if (item.dateEnd && item.dateEnd < item.$minDate || !item.dateEnd && item.$maxDate || item.$maxDate && item.dateEnd > item.$maxDate) {
                        this.model[item.id]['DateEnd'].controller.$setValidity("dateRangeOverlap", false);
                    } else {
                        this.model[item.id]['DateEnd'].controller.$setValidity("dateRangeOverlap", true);
                    }

                    if (item.dateEnd && item.dateEnd < item.dateStart) {
                        this.model[item.id]['DateStart'].controller.$setValidity("dateRangeInvalid", false);
                        this.model[item.id]['DateEnd'].controller.$setValidity("dateRangeInvalid", false);
                    } else {
                        this.model[item.id]['DateStart'].controller.$setValidity("dateRangeInvalid", true);
                        this.model[item.id]['DateEnd'].controller.$setValidity("dateRangeInvalid", true);
                    }
                }
            });
        }
    }

    Application.getApplication()
        .controller('bssAssignmentEditorValidatorController', BssAssignmentEditorValidatorController);


}