﻿module SolvesIt.Bss.Widgets.AssignmentEditor{
 
   
    class BssAssignmentEditorTableExtensionHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = "^bssTable";

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    bssTableController: SolvesIt.Widgets.Table.BssTableController
                    ): void {
                    bssTableController.tableBodyTemplate = '~/Content/Templates/AssignmentEditor/AssignmentEditorBssTableBody.tpl.html';
                    bssTableController.tableTemplate = '~/Content/Templates/AssignmentEditor/AssignmentEditorBssTableTable.tpl.html';
                    bssTableController.tableCellTemplate = '~/Content/Templates/AssignmentEditor/AssignmentEditorBssTableCell.tpl.html';
                }
            };
        }
    }

    Application.getApplication()
        .directive('bssAssignmentEditorTableExtension', (new BssAssignmentEditorTableExtensionHandlerDirective()).factory());
} 