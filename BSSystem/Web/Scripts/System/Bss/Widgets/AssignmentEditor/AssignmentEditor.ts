﻿module SolvesIt.Bss.Widgets.AssignmentEditor {
    export interface IAssignmentEditorDataProcessor {
        processData(data: SolvesIt.Bss.Models.IAssignment[], datesRangeRestriction: { dateStart: Date; dateEnd: Date }): void;
    }

    class AssignmentEditorDataProcessor implements IAssignmentEditorDataProcessor {
        public processData(data: SolvesIt.Bss.Models.IAssignment[], datesRangeRestriction: { dateStart: Date; dateEnd: Date }): void {
            _.each(data, (element: IAssignmentEditorElement) => {
                element.$minDate = datesRangeRestriction.dateStart;

                if (element.$minDate < element.element.dateStart) {
                    element.$minDate = element.element.dateStart;
                }

                if (element.role && element.$minDate < element.role.dateStart) {
                    element.$minDate = element.role.dateStart;
                }

                element.$maxDate = datesRangeRestriction.dateEnd;

                if (!element.$maxDate || element.element.dateEnd && element.$maxDate > element.element.dateEnd) {
                    element.$maxDate = element.element.dateEnd;
                }

                if (element.role && (!element.$maxDate || element.role.dateEnd && element.$maxDate > element.role.dateEnd)) {
                    element.$maxDate = element.role.dateEnd;
                }

                element.$roleMinDate = element.$minDate;
                element.$roleMaxDate = element.$maxDate;
            });
        }
    }

    Application.getApplication()
        .service("assignmentEditorDataProcessor", AssignmentEditorDataProcessor);

    interface IAssignmentEditorAttributes {
        entities: string;
        elements: string;
        roleManagement: string;
        jobTimeManagement: string;
    }

    interface IAssignmentEditorScope extends ng.IScope {

        vm: AssignmentEditorController;
    }

    export interface IAssignmentEditorElement extends SolvesIt.Bss.Models.IAssignmentJobRole {
        $minDate: Date;
        $maxDate: Date;
        $roleMinDate: Date;
        $roleMaxDate: Date;
    }

    class AssignmentEditorController {
        public static $inject = [
            '$scope',
            '$attrs',
            'routeFactory',
            'genericEntityAccessor',
            'oDataQueryParametersGeneratorFactory'
        ];

        private elementSource: SolvesIt.Bss.Models.IDetailedMarker[];
        private rolesSource: SolvesIt.Bss.Models.IDetailedMarker[];
        private assignmentId: number = -1;
        private modelController: ng.INgModelController;//link
        private availableRoles: SolvesIt.Bss.Models.IDetailedMarker[] = [];
        private jobKinds = [
            SolvesIt.Bss.Models.JobKinds.none,
            SolvesIt.Bss.Models.JobKinds.time,
            SolvesIt.Bss.Models.JobKinds.hours
        ];
        private datesRangeRestriction: { dateStart: Date; dateEnd: Date };//=
        private selected: IAssignmentEditorElement[];//=

        public validationErrors = {}; //=

        constructor(
            private scope: IAssignmentEditorScope,
            private $attrs: IAssignmentEditorAttributes,
            private routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            private genericEntityAccessor: SolvesIt.Bss.Core.Accessors.IGenericEntityAccessor,
            private oDataQueryParametersGeneratorFactory: SolvesIt.Widgets.QueryString.IODataQueryParametersGeneratorFactory) {

            this.scope.$watch(() => {
                return this.datesRangeRestriction;
            }, () => {
                if (!this.datesRangeRestriction) return;

                this.generateElementSource();
                if (this.$attrs.roleManagement === 'true')
                    this.readAvailableRoles();
            });
        }
        public getTableConfigurationUrl(): string {
            return this.routeFactory.getAssignmentEditColumnsListFor(this.$attrs.entities, this.$attrs.elements);
        }

        getAvailableRoles(item: IAssignmentEditorElement) {
            var roles = _.filter(
                this.availableRoles,
                (element: SolvesIt.Bss.Models.IDetailedMarker) =>
                    (!element.dateEnd || element.dateEnd > item.$roleMinDate)
                    && (!item.dateEnd || element.dateStart < item.$roleMaxDate));
            return roles;
        }

        private readAvailableRoles() {
            var generator = this.oDataQueryParametersGeneratorFactory.create();
            var part = generator.where()
                .property<string>("target.id")
                .eq(TargetKeysProvider.getRoleTarget(this.$attrs.entities, this.$attrs.elements))
                .and()
                .property<Date>("dateEnd").geOrNull(this.datesRangeRestriction.dateStart);

            if (this.datesRangeRestriction.dateEnd) {
                part
                    .and()
                    .property<Date>("dateStart").le(this.datesRangeRestriction.dateEnd);
            }

            var query = generator.generate();

            this.genericEntityAccessor.getAllMarkers(SolvesIt.Bss.Entities.roleDefinitions, query)
                .then(roles => {
                    this.availableRoles = roles;
                });
        }

        roleChanged(item: IAssignmentEditorElement) {
            if (item.$roleMinDate < item.role.dateStart) {
                item.$minDate = item.role.dateStart;
            } else {
                item.$minDate = item.$roleMinDate;
            }

            if (!item.$roleMinDate || item.role.dateEnd && item.$roleMaxDate < item.role.dateEnd) {
                item.$maxDate = item.role.dateEnd;
            } else {
                item.$maxDate = item.$roleMaxDate;
            }

            if (item.dateStart < item.$minDate) {
                item.dateStart = item.$minDate;
            }

            if (item.$maxDate && (!item.dateEnd || item.dateEnd > item.$maxDate)) {
                item.dateEnd = item.$maxDate;
            }

        }

        public remove(item: IAssignmentEditorElement): void {
            var index = this.selected.indexOf(item);
            this.selected.splice(index, 1);
            //var partition = _.partition(this.selected, (element: IAssignmentEditorElement) => {
              //  return element.id == item.id;
           // });

            // this.modelController.$setViewValue(partition[1]);
            //this.scope.$digest();
            // item.element = <any>"adf";
            //this.modelController.$validate();
        }

        public elementChanged(item: IAssignmentEditorElement): void {
            var minDate = this.datesRangeRestriction.dateStart;
            if (item.element && minDate < item.element.dateStart) {
                minDate = item.element.dateStart;
            }

            var maxDate = this.datesRangeRestriction.dateEnd;
            if (!maxDate || item.element && item.element.dateEnd && maxDate > item.element.dateEnd) {
                maxDate = item.element.dateEnd;
            }

            if (item.dateStart < minDate) {
                item.dateStart = minDate;
            }

            if (maxDate && (!item.dateEnd || item.dateEnd > maxDate)) {
                item.dateEnd = maxDate;
            }

            item.$maxDate = maxDate;
            item.$minDate = minDate;
            item.$roleMinDate = minDate;
            item.$roleMaxDate = maxDate;

            item.role = undefined;
        }

        private addItem() {
            var instance: IAssignmentEditorElement = {
                id: this.assignmentId,
                element: null,
                dateStart: this.datesRangeRestriction.dateStart,
                dateEnd: this.datesRangeRestriction.dateEnd,

                role: undefined,

                jobKind: this.jobKinds[0],
                jobValue: undefined,

                $maxDate: this.datesRangeRestriction.dateEnd,
                $minDate: this.datesRangeRestriction.dateStart,
                $roleMinDate: this.datesRangeRestriction.dateStart,
                $roleMaxDate: this.datesRangeRestriction.dateEnd
            };

            this.assignmentId--;
            this.selected.push(instance);
        }

        generateElementSource() {

            var query: SolvesIt.Widgets.QueryString.IQueryDescriptor = null;
            if (Entities.isDated(this.$attrs.elements)) {
                var generator = this.oDataQueryParametersGeneratorFactory.create();

                var part = generator
                    .where()
                    .property<Date>("dateEnd").geOrNull(this.datesRangeRestriction.dateStart);

                if (this.datesRangeRestriction.dateEnd) {
                    part
                        .and()
                        .property<Date>("dateStart").le(this.datesRangeRestriction.dateEnd);
                }

                query = generator.generate();
            }
            this.genericEntityAccessor.getAllMarkers(this.$attrs.elements, query)
                .then(result=> {
                    this.elementSource = result;
                });
        }
    }

    class AssignmentEditorDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict: string = 'E';
        require = ["bssAssignmentEditor", "ngModel"];
        scope = {
            selected: "=ngModel",
            datesRangeRestriction: "=",
            validationErrors: "=",
            formController: "="
        };
        controller: string = 'assignmentEditorController';
        controllerAs: string = 'vm';
        bindToController = true;
        templateUrl: string = "~/Content/Templates/AssignmentEditor/Main.tpl.html";

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(scope, element: ng.IAugmentedJQuery, attrs, controllers: any): void {
                    controllers[0].modelController = controllers[1];
                }
            }
        }
    }
    SolvesIt.Bss.Application.getApplication()
        .controller('assignmentEditorController', AssignmentEditorController)
        .directive('bssAssignmentEditor', (new AssignmentEditorDirective().factory()));
}