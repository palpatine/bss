﻿module SolvesIt.Bss.Widgets.AssignmentEditor {

    class AssignmentEditorValidationErrorTooltipDisplayerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict = "A";
        templateUrl = '~/Content/Templates/AssignmentEditor/AssignmentEditorValidationErrorTooltipDisplayer.tpl.html';
        transclude = true;

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {

                    scope.bssAssignmentEditorValidationErrorTooltipDisplayer = { isOpen: false };
                    instanceElement.hover(function () {
                        if (scope.errorsProvider.model[scope.item.id][scope.definition.columnKey].errors.length > 0) {
                            scope.bssAssignmentEditorValidationErrorTooltipDisplayer.isOpen = true;
                            scope.$apply();
                        }
                    },
                        function () {
                            scope.bssAssignmentEditorValidationErrorTooltipDisplayer.isOpen = false;
                            scope.$apply();
                        });
                }
            }
        };
    }

    Application.getApplication()
        .directive('bssAssignmentEditorValidationErrorTooltipDisplayer', (new AssignmentEditorValidationErrorTooltipDisplayerDirective()).factory());

}