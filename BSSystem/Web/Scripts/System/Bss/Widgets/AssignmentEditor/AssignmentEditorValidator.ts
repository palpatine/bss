﻿module SolvesIt.Bss.Widgets.AssignmentEditor {

    class BssAssignmentEditorValidatorDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict = "A";
        require = ["bssAssignmentEditorValidator", "ngModel"];
        controller = "bssAssignmentEditorValidatorController"

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {
                    var ownController = <IBssAssignmentEditorValidatorController>controller[0];
                    var modelController = <ng.INgModelController>controller[1];
                    ownController.setModelController(modelController);
                }
            };
        }
    }

    Application.getApplication()
        .directive('bssAssignmentEditorValidator', (new BssAssignmentEditorValidatorDirective()).factory());


    class BssAssignmentEditorElementValidatorController {

        modelController: ng.INgModelController;
        masterValidator: IBssAssignmentEditorValidatorController;

        public setControllers(modelController: ng.INgModelController, masterValidator: IBssAssignmentEditorValidatorController) {

            this.modelController = modelController;
            this.masterValidator = masterValidator;
        }

        public register(modelController: ng.INgModelController, propertyName: string): number {
            this.masterValidator.register(modelController, this.modelController.$viewValue.id, propertyName);
            return this.modelController.$viewValue.id;
        }

        public validate() {
            this.masterValidator.validate(this.modelController.$viewValue.id);
        }
    }

    class BssAssignmentEditorElementValidatorDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict = "A";
        require = ["bssAssignmentEditorElementValidator", "ngModel", "^bssAssignmentEditorValidator"];
        controller = "bssAssignmentEditorElementValidatorController";

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {
                    var ownController = <BssAssignmentEditorElementValidatorController>controller[0];
                    var modelController = <ng.INgModelController>controller[1];
                    var masterValidator = <IBssAssignmentEditorValidatorController>controller[2];
                    ownController.setControllers(modelController, masterValidator);
                }
            };
        }
    }

    Application.getApplication()
        .directive('bssAssignmentEditorElementValidator', (new BssAssignmentEditorElementValidatorDirective()).factory())
        .controller('bssAssignmentEditorElementValidatorController', BssAssignmentEditorElementValidatorController);

    class BssAssignmentEditorValueValidatorDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict = "A";
        require = ["ngModel", "^bssAssignmentEditorElementValidator"];
        

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {
                    var modelController = <ng.INgModelController>controller[0];
                    var masterValidator = <BssAssignmentEditorElementValidatorController>controller[1];

                    masterValidator.register(modelController, instanceAttributes.bssAssignmentEditorValueValidator);
                    
                    modelController.$validators["AssignmentEditorValidator"] = () => {
                        masterValidator.validate();
                        return true;
                    };
                }
            };
        }
    }

    Application.getApplication()
        .directive('bssAssignmentEditorValueValidator', (new BssAssignmentEditorValueValidatorDirective()).factory());
}