﻿
module SolvesIt.Bss.Widgets.AssignmentEditor {

    interface IDateRange {
        from: Date;
        to: Date;
    }

    interface IRolesAssignmentSourceItem extends SolvesIt.Bss.Models.INamedMarker {
        periods: IDateRange[]
    }

    interface IRolesAssignmentEditorAttributes {
        entitiesSource: string;
    }

    export interface IRolesAssignmentEditorValidator {
        dateStartValidationState: { [id: number]: { message?: string; isValid: boolean } };
        dateEndValidationState: { [id: number]: { message?: string; isValid: boolean } };
        validateElementDateStart(item: SolvesIt.Bss.Models.IAssignment): boolean;
        validateElementDateEnd(item: SolvesIt.Bss.Models.IAssignment): boolean;
        validate(): void;
    }

    class RolesAssignmentEditorController {

        public static $inject = [
            '$scope',
            'routeFactory'
        ];

        private columnDefinitions;
        private source: string;
        private assignmentId: number = -1;
        private selectorValue: SolvesIt.Bss.Models.INamedMarker = null;
        private modelController: ng.INgModelController;
        private availableRoles: SolvesIt.Bss.Models.INamedMarker[];
        private roleName: string;

        constructor(
            private scope,
            private routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            private roleDefinitionsAccessor: SolvesIt.Bss.Common.RoleDefinitions.IRoleDefinitionsAccessor) {
        }

        initialize(attrs: IRolesAssignmentEditorAttributes, modelController: ng.INgModelController) {

            if (!this.scope.localSource)
                throw "local source required";

            this.modelController = modelController;

            this.columnDefinitions = [
                {
                    columnHeaderDisplayName: "",
                    template: "<a ng-if='!item.readOnly' class='small-icon delete' ng-click='vm.remove(item)'></a>" +
                    "<i ng-if='item.readOnly' class='small-icon delete disabled'></i>" +
                    '<bss-actions kind="details" entities="' + attrs.entitiesSource + '" id="item.elementId" width-modifier="1">',
                    isActionsColumn: true
                },
                {
                    columnHeaderDisplayName: 'DisplayName',
                    displayProperty: 'displayName',
                    sortKey: 'displayName',
                    sorted: true
                },
                {
                    columnHeaderDisplayName: 'DateStart',
                    template: "<div tooltip='{{vm.dateStartValidationState[item.id].message}}'>" +
                    "<bss-datepicker ng-model='item.dateStart' is-open = 'item.isDateStartOpened' required ui-validate-watch='{dateStart:\"vm.validator.dateStartValidationState[item.id].isValid\"}' " +
                    "ui-validate='{dateStart:\"!vm.validator || vm.validator.validateElementDateStart(item)\"}' />" +
                    "</div>",
                    sortKey: 'dateStart'
                },
                {
                    columnHeaderDisplayName: 'DateEnd',
                    template: "<div tooltip='{{vm.dateStartValidationState[item.id].message}}'>" +
                    "<bss-datepicker ng-model='item.dateEnd' is-open = 'item.isDateEndOpened' ui-validate-watch='{dateEnd:\"vm.validator.dateEndValidationState[item.id].isValid\"}' " +
                    "ui-validate='{dateEnd:\"!vm.validator || vm.validator.validateElementDateEnd(item)\"}'/>" +
                    "</div>",
                    sortKey: 'dateEnd'
                }
            ];

            if (attrs.rolesFor) {
                this.roleDefinitionsAccessor.getAll({
                    filterProperties: [
                        {
                            propertyName: 'targetKey',
                            propertyValue: SolvesIt.Bss.TargetKeysProvider.getTargetKeyFor(attrs.rolesFor),
                            filterOption: SolvesIt.Bss.Core.FilterPropertyOption.eq
                        }
                    ]
                })
                    .success(data => {
                        this.availableRoles = data;
                        if (this.availableRoles.length == 1) {
                            this.roleName = this.availableRoles[0].displayName;
                        } else {
                            this.insertMultipleRolesColumn();
                        }
                    });
            }
        }

        public remove(item: SolvesIt.Bss.Models.IAssignment): void {
            var newValue = _.partition(this.scope.selected, (element: SolvesIt.Bss.Models.IAssignment) => {
                return element.id == item.id;
            })[1];
            this.modelController.$setViewValue(newValue);
        }

        public elementAdded(): void {
            if (!this.selectorValue) return;

            var instance: IAssignment =
                {
                    id: this.assignmentId,
                    displayName: this.selectorValue.displayName,
                    elementId: this.selectorValue.id,
                    dateStart: moment().startOf('day').toDate(),
                    dateEnd: null
                };

            this.assignmentId--;
            var newValue = this.scope.selected.splice(0);
            newValue.push(instance);
            this.modelController.$setViewValue(newValue);

            this.selectorValue = null; //this will cause recursive call with null value
        }

        assignValidator(validator: IAssignmentEditorValidator): void {
            this.scope.vm.validator = validator;
        }

        insertMultipleRolesColumn() { throw new Error("Not implemented"); }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssAssignmentEditor',
        [
            () => {
                return {
                    restrict: 'E',
                    require: "ngModel",
                    scope: {
                        selected: "=ngModel",
                        localSource: "=localEntitiesSource",
                        localRolesSource: "=" 
                    },
                    templateUrl: '/Content/Templates/SolvesIt.Bss/AssignmentEditor.tpl.html',
                    controller: RolesAssignmentEditorController,
                    controllerAs: "vm",
                    link: (scope, element: ng.IAugmentedJQuery, attrs: IRolesAssignmentEditorAttributes, modelController: ng.INgModelController) => {
                        (<RolesAssignmentEditorController>scope.vm).initialize(attrs, modelController);
                    }
                };
            }
        ]);
}