﻿module SolvesIt.Bss.Widgets {

    interface IArticleViewSizeAttriubtes extends ng.IAttributes {
        articleViewSize: string;
    }

    class ArticleViewSizeDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        public getInject(): string[] {
            return ['adDebounce'];
        }

        public createHandler(adDebounce: Adapt.IDebounceService): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes: IArticleViewSizeAttriubtes,
                    controller: any[]
                    ): void {

                    scope.$watch(instanceAttributes.articleViewSize, adDebounce((value) => {
                        var offset = instanceElement.children(0).outerHeight(true);
                        scope.$$articleVisibleHeight = value - offset;
                    }));
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('articleViewSize', (new ArticleViewSizeDirective()).factory());
} 