﻿module SolvesIt.Bss.Widgets {


    class DisplayNameValidaotrDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict: string = 'A';
        require = "ngModel";

        getInject(): string[] {
            return ['genericEntityAccessor', 'oDataQueryParametersGeneratorFactory', '$q', 'adDebounce'];
        }

        public createHandler(
            genericEntityAccessor: SolvesIt.Bss.Core.Accessors.IGenericEntityAccessor,
            oDataQueryParametersGeneratorFactory: SolvesIt.Widgets.QueryString.IODataQueryParametersGeneratorFactory,
            $q: ng.IQService,
            debounce: Adapt.IDebounceService)
            : SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    ngModelController: ng.INgModelController
                ): void {
                    var current;

                    var verifier = debounce((currentDeferer: ng.IDeferred<boolean>, generator: SolvesIt.Widgets.QueryString.IODataQueryParametersGenerator, id: number) => {
                        if (current != id) {
                            currentDeferer.resolve(true);
                            return;
                        }

                        genericEntityAccessor.getAllMarkers(instanceAttributes.entities, generator.generate())
                            .then((data) => {
                                if (data.length == 0) {
                                    currentDeferer.resolve(true);
                                } else {
                                    currentDeferer.reject();
                                }
                            });
                    }, 1000);

                    ngModelController.$asyncValidators['displayNameUniqueness'] = (value) => {
                        var generator = oDataQueryParametersGeneratorFactory.create();
                        var filter = generator.where()
                            .property("displayName")
                            .eq(value);

                        if (scope.model.id) {
                            filter
                                .and()
                                .property("id")
                                .ne(scope.model.id);
                        }

                        var defer = $q.defer<boolean>();

                        if (ngModelController.$dirty) {
                            current = Math.random();
                            verifier(defer, generator, current);
                        } else {
                            defer.resolve(true);
                        }

                        return defer.promise;
                    };
                }
            };
        }
    }

    Application.getApplication()
        .directive('bssUniqueDisplayName', (new DisplayNameValidaotrDirective()).factory());
} 