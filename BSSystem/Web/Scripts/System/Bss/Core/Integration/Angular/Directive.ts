﻿module SolvesIt.Bss.Core.Integration.Angular {
   

    export class Directive<THandler extends IDirectiveHandler> implements ng.IDirective {

        public factory(): ng.IDirectiveFactory {

            var directive = (...dependencies: any[]) => {

                var handler = this.createHandler.apply(this, dependencies);
                var result = {
                    link(
                        scope: ng.IScope,
                        instanceElement: ng.IAugmentedJQuery,
                        instanceAttributes: ng.IAttributes,
                        controller: any,
                        transclude: ng.ITranscludeFunction
                        ): void {
                        handler.link(scope, instanceElement, instanceAttributes, controller, transclude);
                    }
                };

                return angular.extend(result, this);
            };

            directive.$inject = this.getInject();

            return directive;
        }

        public getInject(): string[] {
            return [];
        }

        public createHandler(...dependencies: any[]): THandler {
            throw "Create handler method is abstract and must be overriden in directive";
        }


    }

    export class BasicDirective extends Directive<IDirectiveHandler> {
        public createHandler(...dependencies: any[]): IDirectiveHandler {
            return {
                link(): void {
                }
            }
        }
    }
}