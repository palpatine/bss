﻿module SolvesIt.Bss.Core.Integration.Angular {
    export interface IDirectiveHandler {
        link: ng.IDirectiveLinkFn;
    }
} 