﻿module SolvesIt.Bss.Core{
    class RouteFactory implements IRouteFactory {
        public static $inject = [
            'oDataQuerySerializer'
        ];

        constructor(private oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer) {
        }

        //
        // Locks
        //

        public setLockFor(entities: string, id: number): string {
            return "~/api/" + Entities.getModule(entities) + "/" + entities + "/" + id + "/lock";
        }

        public setJunctionLockFor(entities: string, id: number, relations: string): string {
            return "~/api/" + Entities.getModule(entities) + "/" + entities + "/" + id + "/junctionLock/" + Entities.getModule(relations) + "_" + _.singularize(relations);
        }

        public unlock(): string {
            return "~/api/unlock";
        }

        //
        // Odata
        //

        public getOdListFor(entities: string, query?: SolvesIt.Widgets.QueryString.IQueryDescriptor): string {
            var address = "~/api/" + Entities.getModule(entities) + "/" + entities;
            if (query != null) {
                var parameters = this.oDataQuerySerializer.serialize(query);
                var parametersText = "";
                _.each(parameters, (item: string, name: string) => {
                    parametersText += name + "=" + item;
                });

                if (parametersText.length > 0) {
                    address += "?" + parametersText;
                }
            }

            return address;
        }

        static detailedMarkerSelectionList: string = "$select=id,displayName,dateStart,dateEnd";

        static namedMarkerSelectionList: string = "$select=id,displayName";

        getOdListBasicsFor(entities: string, query?: SolvesIt.Widgets.QueryString.IQueryDescriptor): string {
            var address = "~/api/" + Entities.getModule(entities) + "/" + entities;
            if (query != null) {
                var parameters = this.oDataQuerySerializer.serialize(query);
                var parametersText = "";
                _.each(parameters, (item: string, name: string) => {
                    if (name == "$select") {
                        throw "Select not supported in basics";
                    }
                    parametersText += name + "=" + item;
                });

                if (parametersText.length > 0) {
                    address += "?" + parametersText + RouteFactory.detailedMarkerSelectionList;
                }
            } else {
                address += "?" + RouteFactory.detailedMarkerSelectionList;
            }

            return address;
        }

        public getOdDetailedMarkerFor(entities: string, id: number): string {
            return "~/api/" + Entities.getModule(entities) + "/" + entities + "/" + id + "?" + RouteFactory.detailedMarkerSelectionList;
        }

        public getOdNamedMarkerFor(entities: string, id: number): string {
            return "~/api/" + Entities.getModule(entities) + "/" + entities + "/" + id + "?" + RouteFactory.namedMarkerSelectionList;
        }

        public getOdDetailsFor(entities: string, id: number): string {
            return "~/api/" + Entities.getModule(entities) + "/" + entities + "/" + id;
        }

        public getOdElementsFor(entities: string, id: number, elements: string): string {
            return "~/api/Assignments/Get/"
                + Entities.getModule(entities) + "_" + _.singularize(entities) + "/"
                + id + "/"
                + Entities.getModule(elements) + "_" + _.singularize(elements);
        }

        public getOdElementsMarkersFor(entities: string, id: number, elements: string): string {
            return "~/api/Assignments/GetRelatedNamedMarkers/"
                + Entities.getModule(entities) + "_" + _.singularize(entities) + "/"
                + id + "/"
                + Entities.getModule(elements) + "_" + _.singularize(elements);
        }

        public saveOdRelationFor(entities: string, id: number, elements: string): string {
            return "~/api/Assignments/Set/" + Entities.getModule(entities) + "_" + _.singularize(entities)
                + "/" + id + "/" + Entities.getModule(elements) + "_" + _.singularize(elements);
        }

        public getAssignmentDisplayColumnsListFor(entities: string, elements: string): string {
            return "~/api/Assignments/GetDisplayColumnsConfiguration/" + Entities.getModule(entities) + "_" + _.singularize(entities)
                + "/" + Entities.getModule(elements) + "_" + _.singularize(elements);
        }

        public getAssignmentEditColumnsListFor(entities: string, elements: string): string {
            return "~/api/Assignments/GetEditColumnsConfiguration/" + Entities.getModule(entities) + "_" + _.singularize(entities)
                + "/" + Entities.getModule(elements) + "_" + _.singularize(elements);
        }

        public getListColumnsConfigurationFor(entities: string): string {
            return "~/api/" + Entities.getModule(entities) + "/list/" + entities + "/GetListColumnsConfiguration";
        }

        getRelevantPermissonsFor(entities: string, targetKey: string): string {
            return "~/api/" + Entities.getModule(entities) + "/list/" + entities + "/GetRelevantPermissions/" + targetKey;
        }

        getAssignedPermissionsFor(entities: string, id: number): string {
            return "~/api/" + Entities.getModule(entities) + "/" + entities + "/" + id + "/GetAssignedPermissions";
        }

        getSavePermissionsFor(entities: string, id: number): string {
            return "~/api/" + Entities.getModule(entities) + "/" + entities + "/" + id + "/PostAssignedPermissions";
        }

        getPermissionsAssignmentDisplayColumns(): string {
            return "~/api/" + Entities.getModule(Entities.permissions) + "/list/" + Entities.permissions + "/GetPermissionsAssignmentDisplayColumns";
        }

        getPermissionsAssignmentEditColumns(): string {
            return "~/api/" + Entities.getModule(Entities.permissions) + "/list/" + Entities.permissions + "/GetPermissionsAssignmentEditColumns";
        }

        //
        // Templates
        //

        public getDetailsTemplateFor(entities: string): string {
            return "~/" + Entities.getModule(entities) + "/" + _.singularize(entities) + "Templates/details";
        }

        public getCreateDataTemplateFor(entities: string): string {
            return "~/" + Entities.getModule(entities) + "/" + _.singularize(entities) + "Templates/dataCreator";
        }

        public getDetailedListTemplateFor(entities: string): string {
            return "~/" + Entities.getModule(entities) + "/" + _.singularize(entities) + "Templates/list";
        }

        getTemplateForOwnEntriesList(): string {
            return this.getDetailedListTemplateFor(SolvesIt.Bss.Entities.ownEntries);
        }

        getTemplateForOtherEntriesList(): string {
            return this.getDetailedListTemplateFor(SolvesIt.Bss.Entities.otherEntries);
        }

        getTemplateForOwnEntriesCalendar(): string {
            return "~/" + Entities.getModule(SolvesIt.Bss.Entities.ownEntries) + "/" + _.singularize(SolvesIt.Bss.Entities.ownEntries) + "Templates/Calendar";
        }

        getTemplateForOtherEntriesCalendar(): string {
            return "~/" + Entities.getModule(SolvesIt.Bss.Entities.otherEntries) + "/" + _.singularize(SolvesIt.Bss.Entities.otherEntries) + "Templates/Calendar";
        }

        public getListTemplateFor(entities: string): string {
            return "~/" + Entities.getModule(entities) + "/" + _.singularize(entities) + "Templates/list";
        }

        public getEditRelationTemplateFor(singleEntities: string, multipleEntities: string): string {
            return "~/" + Entities.getModule(singleEntities) + "/" + _.singularize(singleEntities) + "Templates/" + multipleEntities + "EditPanel";
        }

        public getChangePasswordTemplateFor(): string {
            return "~/" + Entities.getModule(Entities.users) + "/" + Entities.users + "Templates/ChangePassword";
        }

        public getUserProfileTemplate(): string {
            return "~/" + Entities.getModule(Entities.users) + "/" + Entities.users + "Templates/UserProfile";
        };

        //
        // Routing 
        //
        public getWhenCreateUrlFor(entities: string): string {
            return "/Forms/" + entities + "/Create";
        }

        public getWhenDetailsUrlFor(entities: string): string {
            return "/Forms/" + entities + "/Details/:id";
        }

        //
        // Organization
        //

        public createOrganizationAlias(organizationId: number): string {
            return "~/api/Organization/CreateAlias/" + organizationId;
        }

        //
        // Sections
        //

        public getOrganizationStructureLevel(): string {
            return "~/api/" + Entities.getModule(Entities.sections) + "/" + Entities.sections;
        }

        public getChangePasswordTarget(): string {
            return "~/api/Common/list/User/ChangePassword";
        }
    }

    class RouteFactoryProvider {
        public static $inject = [
            'bssODataQuerySerializerProvider'
        ];

        constructor(private oDataQuerySerializer: { $get(): SolvesIt.Widgets.QueryString.IODataQuerySerializer }) {
        }

        $get() {
            return new RouteFactory(this.oDataQuerySerializer.$get());
        }
    }

    Application.getApplication()
        .provider("routeFactory", RouteFactoryProvider);
} 