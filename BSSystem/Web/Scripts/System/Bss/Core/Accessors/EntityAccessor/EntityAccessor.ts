﻿module SolvesIt.Bss.Core.Accessors {

    export class EntityAccessor<TEntity> extends GenericEntityAccessor implements IEntityAccessor<TEntity> {

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor,
            public entities: string) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor);
        }

        public getAll(query: SolvesIt.Widgets.QueryString.IQueryDescriptor): ng.IPromise<TEntity[]> {
            var config: ng.IRequestConfig = {
                url: this.routeFactory.getOdListFor(this.entities),
                method: "get",
                params: this.oDataQuerySerializer.serialize(query)
            };

            var defer = this.$q.defer<TEntity[]>();
            this.$http<TEntity[]>(config)
                .then((response) => {
                    if (response.status == 200) {
                        this.postDeserializationObjectProcessor.processDateValues(response.data, config.url);
                        defer.resolve(response.data);
                    } else if (response.status == 204) {
                        defer.resolve([]);
                    } else {
                        defer.reject(response.status);
                    }
                });

            return defer.promise;
        }

        public getCollectionFromODataRoute<T>(url: string, parameters?: { [key: string]: string }): ng.IPromise<T[]> {
            var defer = this.$q.defer<T[]>();
            var config: ng.IRequestConfig = {
                method: "get",
                url: url,
                params: parameters
            };
            this.$http(config)
                .then((response) => {
                    if (response.status == 200) {
                        var result: T[];

                        if (Array.isArray(response.data))
                            result = <T[]>response.data;
                        else {
                            result = this.adStrapUtils.evalObjectProperty(response.data, this.$adConfig.response.itemsLocation);
                        }

                        this.postDeserializationObjectProcessor.processDateValues(result, url);
                        defer.resolve(result);
                    } else if (response.status == 204) {
                        defer.resolve([]);
                    } else {
                        defer.reject(response.status);
                    }
                });

            return defer.promise;
        }


        public markerFor(id: number): ng.IHttpPromise<SolvesIt.Bss.Models.IDetailedMarker> {
            return this.getDetailedMarkerFor(this.entities, id);
        }

        public dataFor(id: number): ng.IHttpPromise<TEntity> {
            var url = this.routeFactory.getOdDetailsFor(this.entities, id);
            return this.$http.get(url)
                .success(data => {
                    this.postDeserializationObjectProcessor.processDateValues([data], url);
                    return data;
                });
        }

        public junctionLock(id: number, secondEntity: string): ng.IPromise<SolvesIt.Bss.Models.ILockResult> {
            var defer = this.$q.defer<SolvesIt.Bss.Models.ILockResult>();
            var config: ng.IRequestConfig = {
                url: this.routeFactory.setJunctionLockFor(this.entities, id, secondEntity),
                method: "post",
            };

            this.$http(config).success((obj: any) => {
                if (obj['@odata.null'])
                    defer.resolve(null);
                else {
                    defer.resolve({
                        operationToken: obj.operationToken,
                        validationResult: obj.validationResult
                    });
                }
            });

            return defer.promise;
        }

        public lock(id: number): ng.IPromise<SolvesIt.Bss.Models.ILockResult> {
            var defer = this.$q.defer<SolvesIt.Bss.Models.ILockResult>();
            var config: ng.IRequestConfig = {
                url: this.routeFactory.setLockFor(this.entities, id),
                method: "post"
            };

            this.$http(config).success((obj: any) => {
                if (obj['@odata.null'])
                    defer.resolve(null);
                else {
                    defer.resolve({
                        operationToken: obj.operationToken,
                        validationResult: obj.validationResult
                    });
                }
            });

            return defer.promise;
        }

        public unlock(id: number, operationToken: string): ng.IHttpPromise<{}> {
            var config: ng.IRequestConfig = {
                url: this.routeFactory.unlock(),
                method: "post",
                headers: {
                    OperationToken: operationToken
                }
            };

            return this.$http(config);
        }

        public processHierarchicalData(data: { id: number; parentId: number }) {

        }

        saveData(model: TEntity, operationToken: string): ng.IHttpPromise<SolvesIt.Bss.Models.IValidationResult> {

            this.postDeserializationObjectProcessor.prepareForSerialization(model);

            var config: angular.IRequestConfig = {
                method: "post",
                data: model,
                url: this.routeFactory.getOdListFor(this.entities),
                headers: {
                    OperationToken: operationToken
                }
            };

            return this.$http(config);
        }

        public relationsFor(id: number, relatedEntity: string): ng.IPromise<any[]> {
            var override = this[relatedEntity + "For"];
            if (override) {
                return override.apply(this, [id]);
            } else {
                var url = this.routeFactory.getOdElementsFor(this.entities, id, relatedEntity);
                return this.getCollectionFromODataRoute<SolvesIt.Bss.Models.IAssignmentJobRoleViewModel>(url)
                    .then(data => {
                        var newData = this.mapAssignmentsRoleJobViewModels(data);
                        return newData;
                    });
            }
        }

        public saveRelations(id: number, assignments: SolvesIt.Bss.Models.IAssignmentJobRole[], relatedEntity: string): ng.IHttpPromise<SolvesIt.Bss.Models.IValidationResult> {
            var method = this["save" + relatedEntity.charAt(0).toUpperCase() + relatedEntity.slice(1)];
            if (method) {
                return method.apply(this, [id, assignments]);
            } else {
                return this.$http.post(
                    this.routeFactory.saveOdRelationFor(this.entities, id, relatedEntity),
                    this.mapAssignmentRoleJobs(assignments));
            }
        }

        public mapAssignment(element: SolvesIt.Bss.Models.IAssignment): SolvesIt.Bss.Models.IAssignmentPersistanceModel {
            return {
                id: element.id,
                elementId: element.element.id,
                dateStart: element.dateStart,
                dateEnd: element.dateEnd
            };
        }

        public mapAssignments(assignments: SolvesIt.Bss.Models.IAssignment[]): SolvesIt.Bss.Models.IAssignmentPersistanceModel[] {
            return _.map(assignments, (element: SolvesIt.Bss.Models.IAssignment) => {
                return this.mapAssignment(element);
            });
        }

        public mapAssignmentRoleJobs(assignments: SolvesIt.Bss.Models.IAssignmentJobRole[]): SolvesIt.Bss.Models.IAssignmentJobRolePersistanceModel[] {
            return _.map(assignments, (element) => {
                var item = <SolvesIt.Bss.Models.IAssignmentJobRolePersistanceModel>this.mapAssignment(element);

                if (element.role) {
                    item.roleId = element.role.id;
                }
                if (element.jobKind) {
                    item.jobKind = element.jobKind.id;
                    item.jobValue = parseFloat(element.jobValue);
                }
                return item;
            });
        }

        public mapAssignmentsRoleJobViewModels(assignments: SolvesIt.Bss.Models.IAssignmentJobRoleViewModel[]): SolvesIt.Bss.Models.IAssignmentJobRole[] {
            var result = _.map(assignments, item => {
                var element = <SolvesIt.Bss.Models.IAssignmentJobRole><any>item;

                if (item.jobKind) {
                    element.jobKind = item.jobKind == 1 ? SolvesIt.Bss.Models.JobKinds.none : (item.jobKind == 2 ? SolvesIt.Bss.Models.JobKinds.time : SolvesIt.Bss.Models.JobKinds.hours);
                    element.jobValue = item.jobValue ? item.jobValue.toString() : null;
                }

                return element;
            });

            return result;
        }

        public getListColumnsConfigurationFor(): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration> {
            return this.$http.get(this.routeFactory.getListColumnsConfigurationFor(this.entities));
        }

        getRelevantPermissions(targetKey: string): ng.IHttpPromise<SolvesIt.Bss.Common.Permissions.IPermissionViewModel[]> {
            return this.$http.get(this.routeFactory.getRelevantPermissonsFor(this.entities, targetKey));
        }

        getAssignedPermissions(id: number): ng.IPromise<SolvesIt.Bss.Common.Permissions.PermissionsStructure> {
            return this.$http.get<SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel[]>(this.routeFactory.getAssignedPermissionsFor(this.entities, id))
                .then(response => {
                    return new SolvesIt.Bss.Common.Permissions.PermissionsStructure(response.data);
                });
        }

        getPermissionsAssignmentDisplayColumns(): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration> {
            return this.$http.get(this.routeFactory.getPermissionsAssignmentDisplayColumns());
        }

        getPermissionsAssignmentEditColumns(): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration> {
            return this.$http.get(this.routeFactory.getPermissionsAssignmentEditColumns());
        }

        savePermissionsAssignment(id: number, data: SolvesIt.Bss.Common.Permissions.PermissionsStructure, operationToken: string)
            : ng.IHttpPromise<SolvesIt.Bss.Models.IValidationResult> {
            var config: ng.IRequestConfig = {
                method: "post",
                data: data.getPersistanceData(),
                url: this.routeFactory.getSavePermissionsFor(this.entities, id),
                headers: {
                    OperationToken: operationToken
                }
            };

            return this.$http(config);
        }
    }
} 