﻿module SolvesIt.Bss.Core.Accessors{
    export interface IGenericEntityAccessor {
        getDetailedMarkerFor(entities: string, id: number): ng.IHttpPromise<SolvesIt.Bss.Models.IDetailedMarker>;
        getNamedMarkerFor(entities: string, id: number): ng.IHttpPromise<SolvesIt.Bss.Models.INamedMarker>;
        getAllMarkers(entities: string, query: SolvesIt.Widgets.QueryString.IQueryDescriptor): ng.IPromise<SolvesIt.Bss.Models.IDetailedMarker[]>;
        getAssignmentDisplayColumnsList(entities: string, elements: string): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration>;
        getAssignmentEditColumnsList(entities:string, elements:string):ng.IHttpPromise<Directives.TableAjax.ITableConfiguration>;
    }

    export interface IBasicEntityAccessor extends IGenericEntityAccessor {

        markerFor(id: number): ng.IHttpPromise<SolvesIt.Bss.Models.IDetailedMarker>;

        junctionLock(id: number, secondEntity: string): ng.IPromise<SolvesIt.Bss.Models.ILockResult>;

        lock(id: number): ng.IPromise<SolvesIt.Bss.Models.ILockResult>;

        unlock(id: number, operationToken: string): ng.IHttpPromise<{}>;

        relationsFor(id: number, relatedEntity: string): ng.IPromise<any[]>;

        saveRelations(id: number, relations: any[], relatedEntity: string): ng.IHttpPromise<SolvesIt.Bss.Models.IValidationResult>;

        getListColumnsConfigurationFor(): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration>;
    }

    export interface IPermissionsRelatedAccessor extends SolvesIt.Bss.Core.Accessors.IBasicEntityAccessor {
        getRelevantPermissions(targetKey: string): ng.IHttpPromise<SolvesIt.Bss.Common.Permissions.IPermissionViewModel[]>;

        getAssignedPermissions(id: number): ng.IPromise<SolvesIt.Bss.Common.Permissions.PermissionsStructure>;

        getPermissionsAssignmentDisplayColumns(): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration>;

        getPermissionsAssignmentEditColumns(): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration>;

        savePermissionsAssignment(id: number, data: SolvesIt.Bss.Common.Permissions.PermissionsStructure, operationToken: string)
            : ng.IHttpPromise<SolvesIt.Bss.Models.IValidationResult>;
    }

    export interface IEntityAccessor<TEntity> extends IBasicEntityAccessor {

        getAll(query: SolvesIt.Widgets.QueryString.IQueryDescriptor): ng.IPromise<TEntity[]>;

        saveData(model: TEntity, operationToken: string): ng.IHttpPromise<SolvesIt.Bss.Models.IValidationResult>;

        dataFor(id: number): ng.IHttpPromise<TEntity>;
    }

    export interface ICalendarRelatedAccessor {
        getAllForCalendar(query: SolvesIt.Widgets.QueryString.IQueryDescriptor): ng.IPromise<SolvesIt.Widgets.Calendar.ICalendarItem[]>;
    }
} 