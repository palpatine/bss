﻿module SolvesIt.Bss.Core.Accessors{

    export class GenericEntityAccessor implements IGenericEntityAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            public adStrapUtils,
            public $adConfig,
            public $http: ng.IHttpService,
            public routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            public $q: ng.IQService,
            public oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            public postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
        }

        public getAllMarkers(entities: string, query: SolvesIt.Widgets.QueryString.IQueryDescriptor): ng.IPromise<SolvesIt.Bss.Models.IDetailedMarker[]> {
            var config: ng.IRequestConfig = {
                url: this.routeFactory.getOdListFor(entities),
                method: "get",
                params: this.oDataQuerySerializer.serialize(query)
            };

            var defer = this.$q.defer<SolvesIt.Bss.Models.IDetailedMarker[]>();
            this.$http(config)
                .then((response) => {
                if (response.status == 200) {
                    var result: SolvesIt.Bss.Models.IDetailedMarker[];

                    if (Array.isArray(response.data))
                        result = <SolvesIt.Bss.Models.IDetailedMarker[]>response.data;
                    else {
                        result = this.adStrapUtils.evalObjectProperty(response.data, this.$adConfig.response.itemsLocation);
                    }

                    this.postDeserializationObjectProcessor.processDateValues(result, config.url);
                    defer.resolve(result);
                } else if (response.status == 204) {
                    defer.resolve([]);
                }
                else {
                    defer.reject(response.status);
                }
            });

            return defer.promise;
        }

        public getDetailedMarkerFor(entities: string, id: number): ng.IHttpPromise<SolvesIt.Bss.Models.IDetailedMarker> {
            var url = this.routeFactory.getOdDetailedMarkerFor(entities, id);
            return this.$http.get(url)
                .success(data => {
                this.postDeserializationObjectProcessor.processDateValues([data], url);
                return data;
            });
        }

        public getNamedMarkerFor(entities: string, id: number): ng.IHttpPromise<SolvesIt.Bss.Models.INamedMarker> {
            var url = this.routeFactory.getOdNamedMarkerFor(entities, id);
            return this.$http.get(url)
                .success(data => {
                this.postDeserializationObjectProcessor.processDateValues([data], url);
                return data;
            });
        }

        public getAssignmentDisplayColumnsList(entities: string, elements: string): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration> {
            var url = this.routeFactory.getAssignmentDisplayColumnsListFor(entities, elements);
            return this.$http.get(url)
                .success(data => {
                this.postDeserializationObjectProcessor.processDateValues([data], url);
                return data;
            });
        }

        public getAssignmentEditColumnsList(entities: string, elements: string): ng.IHttpPromise<Directives.TableAjax.ITableConfiguration> {
            var url = this.routeFactory.getAssignmentEditColumnsListFor(entities, elements);
            return this.$http.get(url)
                .success(data => {
                this.postDeserializationObjectProcessor.processDateValues([data], url);
                return data;
            });
        }

    }

    Application.getApplication()
        .service("genericEntityAccessor", GenericEntityAccessor);
} 