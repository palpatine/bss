﻿module SolvesIt.Bss.Core.ApplicationManagement {
    export interface IBackendAccessor {

        getApplicationManagement(): ng.IHttpPromise<SolvesIt.Bss.Models.IApplicationDescriptor>;

        getLogOnPanel(): ng.IHttpPromise<string>;

        getCurrentUserData(): ng.IPromise<SolvesIt.Bss.Models.IUserData>;

        getAccessibleEntities(permissionPath: string): ng.IPromise<SolvesIt.Bss.Models.IUserPermissionsVerificationResponseViewModel>;

        initilizeState(): void;
    }

    class CoreBackendAccessor implements IBackendAccessor {

        applicationManagementDataPromise: ng.IHttpPromise<SolvesIt.Bss.Models.IApplicationDescriptor>;

        applicationDescriptor: SolvesIt.Bss.Models.IApplicationDescriptor;

        public static $inject = [
            '$http',
            'routeFactory',
            '$q'
        ];

        constructor(
            private $http: ng.IHttpService,
            private routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            private $q: ng.IQService) {
        }

        public initilizeState() {
            this.applicationManagementDataPromise = this.$http
                .get<SolvesIt.Bss.Models.IApplicationDescriptor>("~/api/ApplicationManagement")
                .success(data => {
                    this.applicationDescriptor = data;
                });
        }

        public getApplicationManagement(): ng.IHttpPromise<SolvesIt.Bss.Models.IApplicationDescriptor> {
            return this.applicationManagementDataPromise;
        }

        public getLogOnPanel(): ng.IHttpPromise<string> {
            return this.$http.get("~/Authentication/LogOnPanel");
        }

        public getCurrentUserData(): ng.IPromise<SolvesIt.Bss.Models.IUserData> {
            var defer = this.$q.defer<SolvesIt.Bss.Models.IUserData>();
            this.getApplicationManagement()
                .success(management => {
                    defer.resolve(management.userData);
                });
            return defer.promise;
        }

        public getAccessibleEntities(permissionPath: string): ng.IPromise<SolvesIt.Bss.Models.IUserPermissionsVerificationResponseViewModel> {
            return this.$http.post<SolvesIt.Bss.Models.IUserPermissionsVerificationResponseViewModel[]>(
                "~/api/Common/List/Permission/FindAccessibleEntities/",
                {
                    permissionPaths: [permissionPath]
                })
                .then(result => {
                    return result.data.length == 0 ? null : result.data[0];
                });
        }

    }

    Application.getApplication()
        .service("coreBackendAccessor", CoreBackendAccessor);
}