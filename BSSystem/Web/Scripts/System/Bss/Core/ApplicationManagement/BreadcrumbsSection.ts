﻿module SolvesIt.Bss.Core {

    export class BreadcrumbsSection {
        static $inject = [
            '$scope',
            '$q',
            'globalStateService',
            'stateCacheService'
        ];

        constructor(
            scope: SolvesIt.Bss.IBssSectionScope,
            $q: ng.IQService,
            globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService,
            stateCacheService: SolvesIt.Widgets.IStateCacheService
        ) {

            scope.section = new SolvesIt.Bss.SectionCard(
                $q,
                () => {
                    globalStateService.removeSectionCard(scope.section);
                    if (scope.section.casheKey) {
                        stateCacheService.remove(scope.section.casheKey);
                    }
                });

            scope.section.route = window.location.hash.substr(1);

            globalStateService.addSectionCard(scope.section)
                .then(() => {
                    scope.section.opened();
                    globalStateService.selectSectionCard(scope.section);
                });
        }
    }

    Application.getApplication()
        .controller("breadcrumbsSection", BreadcrumbsSection);
}