﻿module SolvesIt.Bss.Core.ApplicationManagement {
    export interface IGlobalStateService {

        getContentContainer(): JQuery;

        forceState(moduleSectionId: string, sectionGroupId: string): void;

        addSectionCard(descriptor: ISectionCard): ng.IHttpPromise<{}>;

        selectSectionCard(sectionCard: ISectionCard): void;

        removeSectionCard(sectionCard: ISectionCard): void;

        require(permissionPath: string, id: number, entities: string): ng.IPromise<boolean>;

        initilizeState(): ng.IPromise<boolean>;

        areAllModulesAvailable(modules: string[]): ng.IPromise<boolean>;
    }
} 