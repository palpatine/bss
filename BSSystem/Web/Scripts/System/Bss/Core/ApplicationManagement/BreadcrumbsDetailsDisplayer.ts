﻿module SolvesIt.Bss.Core {

    class BreadcrumbsDetailsDisplayer {
        static $inject = [
            '$scope',
            'globalStateService',
            '$q',
            '$routeParams'
        ];

        constructor(
            scope: SolvesIt.Bss.IBssSectionScope,
            globalStateService,
            $q: ng.IQService,
            $routeParams) {

            scope.section = new SolvesIt.Bss.SectionCard(
                $q,
                () => {
                    globalStateService.removeSectionCard(scope.section);
                });

            scope.section.route = window.location.hash.substr(1);

            scope.section.id = $routeParams.id;

            globalStateService.addSectionCard(scope.section)
                .then(() => {
                scope.section.opened();
                globalStateService.selectSectionCard(scope.section);
            });
        }
    }

    Application.getApplication()
        .controller("breadcrumbsDetailsDisplayer", BreadcrumbsDetailsDisplayer);
}