﻿module SolvesIt.Bss.Core.ApplicationManagement {

    class MainNavigationController {
        public static $inject = [
            '$scope',
            'globalStateService',
            '$http',
            '$location'
        ];

        isSideMenuHidden: boolean = false;
        isSideMenuToggleOn: boolean = false;

        minHeight: number = 100;

        constructor(
            private $scope: IApplicationScope,
            private globalStateService: GlobalStateService,
            private $http: ng.IHttpService,
            private $location: ng.ILocationService) {

            $scope.state = globalStateService.getState();
        }

        navigate(menuSection: SolvesIt.Bss.Models.IMenuSectionViewModel, menuItem: SolvesIt.Bss.Models.IMenuItemViewModel) {
            window.location.href = "#/" + menuSection.key + "/" + menuItem.key;
            menuSection.isOpen = false;
        }

        isMenuSectionActive(menuSection: SolvesIt.Bss.Models.IMenuSectionViewModel): boolean {
            if (!this.$scope.state.selectedMenuSection) {
                return false;
            }
            return menuSection.key == this.$scope.state.selectedMenuSection.key;
        }

        isMenuItemActive(menuItem: SolvesIt.Bss.Models.IMenuItemViewModel): boolean {
            if (!this.$scope.state.selectedMenuItem)
                return false;

            return this.$scope.state.selectedMenuItem.key == menuItem.key;
        }

        isSectionCardActive(sectionCard: ISectionCard): boolean {
            if (!this.$scope.state.selectedSectionCard) {
                return false;
            }
            return this.$scope.state.selectedSectionCard.route == sectionCard.route;
        }

        getSideWidth() {
            return this.$scope.state.isSideShort ? "width: 70px" : "width: 160px";
        }

        toggleVisibility(menuSection: SolvesIt.Bss.Models.IMenuSectionViewModel) {
            _.each(this.$scope.state.menuSections, (m: SolvesIt.Bss.Models.IMenuSectionViewModel) => { m.isOpen = false; });
            menuSection.isOpen = !menuSection.isOpen;
        }

        sizeChanged() {
            var topOffset = 64;
            var width = this.$scope.windowWidth;
            if (width < 768) {
                this.isSideMenuHidden = true;
                topOffset = 120; // 2-row-menu
            } else if (this.isSideMenuToggleOn) {
                this.isSideMenuHidden = true;
            } else {
                this.isSideMenuHidden = false;
            }

            var height = this.$scope.windowHeight;
            height = height - topOffset;
            if (height < 1) height = 1;
            if (height > topOffset) {
                this.minHeight = height;
            }
        }
        logout() {

            this.$http.get("~/api/logOff")
                .then(() => {
                    window.location.href = Globals.rootPath;
                });
        }
        reverseSideMenuState() {

            this.isSideMenuToggleOn = !this.isSideMenuToggleOn;
            this.isSideMenuHidden = this.isSideMenuToggleOn;
        }
    }


    interface IApplicationScope extends ng.IScope {
        vm: any;
        state: ApplicatonState;
        windowWidth: number;
        windowHeight: number;
    }

    class GlobalStateService implements IGlobalStateService {
        public static details: string = "Details";
        private routeSchema: RegExp = /^\/(.*?)(?:\/(.*?))?(?:\/(.*?))?(?:\/(.*?))?$/;
        private marker: number;

        public static $inject = [
            '$rootScope',
            'coreBackendAccessor',
            '$q'
        ];

        private state: ApplicatonState = new ApplicatonState();
        private modulesStructireLoadPromise: ng.IHttpPromise<SolvesIt.Bss.Models.IApplicationDescriptor>;

        public initilizeState(): ng.IPromise<boolean> {
            var defered = this.$q.defer<boolean>();
            this.coreBackendAccessor.initilizeState();
            this.modulesStructireLoadPromise =
            this.coreBackendAccessor.getApplicationManagement()
                .success((result) => {

                    if (this.state.userData &&
                        this.state.userData.user.id != result.userData.user.id) {
                        defered.reject();
                        window.location.href = Globals.rootPath;
                        return;
                    }

                    this.state.userData = result.userData;
                    this.state.menuSections = result.menuSections;
                    _.each(this.state.menuSections, (moduleSection: SolvesIt.Bss.Models.IMenuSectionViewModel) => {
                        moduleSection.isOpen = false;
                    });
                    defered.resolve(true);
                });

            return defered.promise;
        }

        constructor(
            private $rootScope: ng.IRootScopeService,
            private coreBackendAccessor: SolvesIt.Bss.Core.ApplicationManagement.IBackendAccessor,
            private $q: ng.IQService) {

            this.initilizeState();

            $rootScope.$on('$routeChangeStart', (event: ng.IAngularEvent, args: any[]) => {
                if (!args) {
                    return;
                }

                var next = args['$$route'];

                if (!next) {
                    return;
                }

                var match = this.routeSchema.exec(next.originalPath);

                if (!match || !match[0] || !match[1])
                    return;

                if (match[1] == 'Forms') {
                    if (!this.state.selectedMenuSection) {
                        this.modulesStructireLoadPromise
                            .success(() => {
                                var menuSectionKey = this.state.menuSections[0].key;
                                this.forceState(menuSectionKey, null, false);
                            });
                    }

                    return;
                }

                var requestedMenuSectionKey = match[1];
                var requestedMenuItemKey = match[2];
                var marker = Math.random();
                this.marker = marker;
                this.modulesStructireLoadPromise
                    .success(() => {
                        if (marker != this.marker) return;
                        this.forceState(requestedMenuSectionKey, requestedMenuItemKey);
                    });
            });

            $(window).on("resize", () => {
                this.calculateMainNavigatorWidth();
                this.calculateSideNavigationWidth();
            });

            this.calculateMainNavigatorWidth();
            this.calculateSideNavigationWidth();
        }

        private calculateSideNavigationWidth(): void {
            if ($(window).width() > 1200) {
                this.state.isSideShort = false;
            } else {
                this.state.isSideShort = true;
            }
        }

        private calculateMainNavigatorWidth(): void {
            if ((<any>$("body")).hasScrollBar()) {
                $("#Main").width($(window).width() - 47 - scrollBarWidth);
            } else {
                $("#Main").width($(window).width() - 47);
            }
        }

        public getContentContainer(): JQuery {
            return $("#Content");
        }

        public forceState(menuSectionKey: string, menuItemKey: string, redirectToDefault: boolean = true): void {
            if (!this.state.menuSections) {
                return;
            }

            var menuSection: SolvesIt.Bss.Models.IMenuSectionViewModel;
            if (menuSectionKey)
                menuSection = _.find(this.state.menuSections, (element: SolvesIt.Bss.Models.IMenuSectionViewModel) => element.key == menuSectionKey);
            else
                menuSection = this.state.menuSections[0];

            if (!menuSection) {
                throw "Menu section not fould " + menuSectionKey;
            }

            if (!menuSectionKey || !menuItemKey) {
                if (redirectToDefault) {
                    window.location.hash = "#/" + menuSection.key + "/" + menuSection.menuItems[0].key;
                    return;
                }
            }

            this.menuSectionSelected(menuSection);
            var menuItem: SolvesIt.Bss.Models.IMenuItemViewModel;

            if (!menuItemKey && !redirectToDefault) {
                menuItem = menuSection.menuItems[0];
            } else {
                menuItem = _.find(this.state.selectedMenuSection.menuItems, (element: SolvesIt.Bss.Models.IMenuItemViewModel) => element.key == menuItemKey);
                if (!menuItem) {
                    throw "Menu item not fould " + menuItemKey;
                }
            }

            this.menuItemSelected(menuItem);

            this.calculateMainNavigatorWidth();
            this.calculateSideNavigationWidth();
        }

        public removeSectionCard(descriptor: ISectionCard) {

            if (this.state.sectionCards.length === 1
                && this.state.selectedMenuSection.key == this.state.menuSections[0].key
                && this.state.selectedMenuSection.key == this.state.menuSections[0].menuItems[0].key) {
                return;
            }

            var index = this.state.sectionCards.indexOf(descriptor);
            if (index != -1) {
                this.state.sectionCards.splice(index, 1);
                if (this.state.sectionCards.length == 1) {
                    this.state.sectionCards[0].canClose = false;
                }
            }

            if (this.state.selectedSectionCard != descriptor) {
                return;
            }

            this.state.selectedSectionCard = null;

            if (this.state.sectionCards.length == 0) {
                window.location.hash = "#/" + this.state.menuSections[0].key + "/" + this.state.menuSections[0].menuItems[0].key;
            } else {
                window.location.hash = this.state.sectionCards[index == 0 ? 0 : index - 1].route;
            }

            angular.publish(Events.cardClosed, descriptor);
        }

        public addSectionCard(descriptor: ISectionCard): ng.IHttpPromise<{}> {
            return this.modulesStructireLoadPromise
                .success(() => {
                    var found = false;
                    this.state.sectionCards =
                    (this.state.sectionCards || []).map(element=> {
                        if (element.route == descriptor.route) {
                            found = true;
                            return descriptor;
                        } else {
                            return element;
                        }
                    });

                    if (!found) {
                        if (this.state.sectionCards.length == 1) {
                            this.state.sectionCards[0].canClose = true;
                        }
                        this.state.sectionCards.push(descriptor);
                        if (this.state.sectionCards.length == 1) {
                            this.state.sectionCards[0].canClose = false;
                        }
                    }
                });
        }

        public selectSectionCard(sectionCard: ISectionCard): void {
            if (this.state.selectedSectionCard === sectionCard) {
                return;
            }
            this.state.selectedSectionCard = sectionCard;
            sectionCard.selected();
        }

        public areAllModulesAvailable(modules: string[]): ng.IPromise<boolean> {

            return this.modulesStructireLoadPromise
                .then(() => {
                    return _.all(
                        modules,
                        (m) => {
                            return _.any(this.state.userData.modules, (availableModule) => { return availableModule === m });
                        });
                });
        }

        public require(permissionPath: string, id: number, entities: string): ng.IPromise<boolean> {
            return this.modulesStructireLoadPromise.then(() => {

                var permission = _.find(
                    this.state.userData.effectivePermissions,
                    (userDataPermission: SolvesIt.Bss.Models.IUserDataPermission) => {
                        return userDataPermission.path == permissionPath;
                    });

                if (!permission) {
                    return false;
                }

                if (permission.isGlobal) {
                    return true;
                } else if (permission.entities) {
                    return !!_.find(permission.entities, (entityId) => {
                        return id == entityId;
                    });
                } else {
                    if (!permission.promise) {
                        permission.promise = this.coreBackendAccessor.getAccessibleEntities(permissionPath);
                    }

                    return permission.promise
                        .then(result => {
                            permission.entities = result.entities;
                            return !!_.find(permission.entities, (entityId) => {
                                return id == entityId;
                            });
                        });
                }
            });
        }
        
        /*internal*/
        public getState(): ApplicatonState { return this.state; }

        private menuSectionSelected(menuSection: SolvesIt.Bss.Models.IMenuSectionViewModel): void {
            if (this.state.selectedMenuSection === menuSection) {
                return;
            }
            this.state.selectedMenuSection = menuSection;
            console.log(menuSection.key);
        }

        private menuItemSelected(menuItem: SolvesIt.Bss.Models.IMenuItemViewModel): void {
            if (this.state.selectedMenuItem === menuItem) {
                return;
            }

            this.state.selectedMenuItem = menuItem;
            console.log(menuItem.key);

            this.state.selectedSectionCard = undefined;
        }
    }

    interface IPersistantState {
        selectedMenuSectionKey: string;
        selectedMenuItemKey: string;
        selectedSectionCardRoute: string;
        sectionCards: {
            route: string;
            title: string;
            canClose: boolean;
        }[];
    }
    class ApplicatonState {
        selectedMenuSection: SolvesIt.Bss.Models.IMenuSectionViewModel;
        menuSections: SolvesIt.Bss.Models.IMenuSectionViewModel[];
        userData: SolvesIt.Bss.Models.IUserData;
        selectedMenuItem: SolvesIt.Bss.Models.IMenuItemViewModel;
        selectedSectionCard: ISectionCard;
        isSideShort: boolean;
        sectionCards: ISectionCard[];
    }

    Application.getApplication()
        .service("globalStateService", GlobalStateService)
        .controller('MainNavigationController', MainNavigationController);
}