﻿module SolvesIt.Bss.Core {

    class BreadcrumbsDataCreator {
        static $inject = [
            '$scope',
            '$q',
            'globalStateService'
        ];

        constructor(
            scope: SolvesIt.Bss.IBssSectionScope,
            $q: ng.IQService,
            globalStateService) {

            scope.section = new SolvesIt.Bss.SectionCard(
                $q,
                () => {
                    globalStateService.removeSectionCard(scope.section);
                    if (scope.section.id) {
                        window.location.href = '#/Forms/' + scope.section.entities + "/Details/" + scope.section.id;
                    }
                });

            scope.section.route = window.location.hash.substr(1);

            globalStateService.addSectionCard(scope.section)
                .then(() => {
                scope.section.opened();
                globalStateService.selectSectionCard(scope.section);
            });
        }
    }

    Application.getApplication()
        .controller("breadcrumbsDataCreator", BreadcrumbsDataCreator);
}