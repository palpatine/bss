﻿module SolvesIt.Bss.Core.ApplicationManagement{
    export interface IBssRouteParamsService extends ng.route.IRouteParamsService {
        action: string;
        id: string;
    }
} 