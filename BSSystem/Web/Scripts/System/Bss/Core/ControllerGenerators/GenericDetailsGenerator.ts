﻿module SolvesIt.Bss.Core.ControllerGenerators {


    export class GenericDetailsGenerator {

        constructor() {

        }

        private $extends(d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];

            function __() { this.constructor = d; }

            __.prototype = b.prototype;
            d.prototype = new __();
        }

        createEditor(area: any, entity: string) {

            var entityKey = entity[0].toLowerCase() + entity.substr(1);
            var __extends = this.$extends;

            (function (Area) {

                var Generated;
                (function (Generated) {

                   
                    Generated[entity + 'DetailsDisplayerController'] = (function (_super) {

                        var $class = function (stateCacheService, $scope, accessor) {
                            _super.call(
                                this,
                                stateCacheService,
                                entity + 'DetailsPanel:' + $scope.section.id + '/' + entity + "DetailsDisplayerController",
                                $scope,
                                accessor);
                        };

                        __extends($class, _super);

                        $class.$inject = [
                            'stateCacheService',
                            '$scope',
                            entityKey + 'Accessor'
                        ];

                        return $class;
                    })(SolvesIt.Bss.Core.Controllers.BaseDetailsDisplayerController);
                 
                    SolvesIt.Bss.Application.getApplication().controller(entityKey + "DetailsDisplayerController", Generated[entity + 'DetailsDisplayerController']);
                })(Generated = Area['Generated'] || (Area['Generated'] = {}));
            })(area);
        }
    }
}
