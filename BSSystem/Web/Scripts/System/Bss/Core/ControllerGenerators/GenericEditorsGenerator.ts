﻿module SolvesIt.Bss.Core.ControllerGenerators {


    export class GenericEditorsGenerator {

        constructor() {

        }

        private $extends(d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];

            function __() { this.constructor = d; }

            __.prototype = b.prototype;
            d.prototype = new __();
        }

        createEditor(area: any, entity: string) {

            var entityKey = entity[0].toLowerCase() + entity.substr(1);
            var __extends = this.$extends;

            (function (Area) {

                var Generated;
                (function (Generated) {

                    Generated[entity + 'DetailsEditorController'] = (function (_super) {

                        var $class = function (stateCacheService, $scope, accessor, globalStateService) {
                            _super.call(
                                this,
                                stateCacheService,
                                entity + 'DetailsPanel:' + $scope.section.id + '/' + entity + "DetailsEditorController",
                                $scope,
                                accessor,
                                entity,
                                globalStateService);
                        };

                        __extends($class, _super);

                        $class.$inject = [
                            'stateCacheService',
                            '$scope',
                            entityKey + 'Accessor',
                            'globalStateService'
                        ];

                        return $class;
                    })(SolvesIt.Bss.Core.Controllers.BaseDetailsEditorController);

                    Generated[entity + 'DetailsCreatorController'] = (function (_super) {

                        var $class = function (stateCacheService, $scope, accessor, globalStateService) {
                            _super.call(
                                this,
                                stateCacheService,
                                entity + "DetailsCreatorController",
                                $scope,
                                accessor,
                                entity,
                                globalStateService);
                        };

                        __extends($class, _super);

                        $class.prototype.getEmptyDataModelInstance = function () {
                            var model;
                            if (Area[_.pluralize(entity, 100, false)][entity + "ViewModel"]) {
                                model = new Area[_.pluralize(entity, 100, false)][entity + "ViewModel"]();
                                model.dateStart = new Date();
                            } else {
                                model = { dateStart: new Date() };
                            }
                            return model;
                        };

                        $class.$inject = [
                            'stateCacheService',
                            '$scope',
                            entityKey + 'Accessor',
                            'globalStateService'
                        ];

                        return $class;
                    })(SolvesIt.Bss.Core.Controllers.BaseDetailsEditorController);

                   

                    SolvesIt.Bss.Application.route(function (routeFactory) {
                        return routeFactory.getWhenCreateUrlFor(entity);
                    }, function (routeFactory) {
                            return {
                                templateUrl: routeFactory.getCreateDataTemplateFor(entity),
                                controller: 'breadcrumbsDataCreator'
                            };
                        });

                    SolvesIt.Bss.Application.getApplication().controller(entityKey + "DetailsCreatorController", Generated[entity + 'DetailsCreatorController']);
                    SolvesIt.Bss.Application.getApplication().controller(entityKey + "DetailsEditorController", Generated[entity + 'DetailsEditorController']);
                })(Generated = Area['Generated'] || (Area['Generated'] = {}));
            })(area);
        }
    }
}
