﻿module SolvesIt.Bss.Core.ControllerGenerators {


    export class GenericRelationGenerator {

        constructor() {

        }

        private $extends(d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];

            function __() { this.constructor = d; }

            __.prototype = b.prototype;
            d.prototype = new __();
        }

        createEditor(area: any, entity: string, relatedEntity: string) {

            var entityKey = entity[0].toLowerCase() + entity.substr(1);
            var relationKey = entityKey + _.pluralize(relatedEntity, 2, false);

            var __extends = this.$extends;

            (function (Area) {

                var Generated;
                (function (Generated) {

                    Generated[entity + "_" + relatedEntity + 'RelationEditorController'] = (function (_super) {

                        var $class = function (
                            globalStateService,
                            stateCacheService,
                            $scope,
                            accessor,
                            assignmentEditorDataProcessor,
                            $q) {
                            _super.call(
                                this,
                                globalStateService,
                                stateCacheService,
                                entity + 'DetailsPanel:' + $scope.section.id + '/' + relatedEntity + "RelationEditorController",
                                $scope,
                                accessor,
                                assignmentEditorDataProcessor,
                                $q,
                                relatedEntity);
                        };

                        __extends($class, _super);

                        $class.$inject = [
                            'globalStateService',
                            'stateCacheService',
                            '$scope',
                            entityKey + 'Accessor',
                            'assignmentEditorDataProcessor',
                            '$q'
                        ];

                        return $class;
                    })(SolvesIt.Bss.Core.Controllers.RelationEditorController);


                    SolvesIt.Bss.Application.getApplication().controller(relationKey + "EditorController", Generated[entity + "_" + relatedEntity + 'RelationEditorController']);

                })(Generated = Area['Generated'] || (Area['Generated'] = {}));
            })(area);
        }
    }
}
