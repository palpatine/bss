﻿module SolvesIt.Bss.Core {

    export interface IRouteFactory {
        //lock

        setLockFor(entities: string, id: number): string;

        setJunctionLockFor(entities: string, id: number, relations: string): string;

        unlock();

        //get data

        getOdListFor(entities: string, query?: SolvesIt.Widgets.QueryString.IQueryDescriptor): string;

        getOdListBasicsFor(entities: string, query?: SolvesIt.Widgets.QueryString.IQueryDescriptor): string;

        getOdDetailsFor(entities: string, id: number): string;

        getOdDetailedMarkerFor(entities: string, id: number): string;

        getOdNamedMarkerFor(entities: string, id: number): string;

        getOdElementsFor(entities: string, id: number, elements: string): string;

        getOdElementsMarkersFor(entities: string, id: number, elements: string): string;

        getOrganizationStructureLevel(): string;

        getChangePasswordTemplateFor(): string;

        getListColumnsConfigurationFor(entities: string): string;

        getAssignmentDisplayColumnsListFor(entities: string, elements: string): string;

        getAssignmentEditColumnsListFor(entities: string, elements: string): string;

        getRelevantPermissonsFor(entities: string, targetKey: string): string;

        getAssignedPermissionsFor(entities: string, id: number): string;

        getSavePermissionsFor(entities: string, id: number): string;

        getPermissionsAssignmentDisplayColumns(): string;

        getPermissionsAssignmentEditColumns(): string;

        //persistance

        getChangePasswordTarget(): string;

        saveOdRelationFor(entities: string, id: number, elements: string): string;

        createOrganizationAlias(organizationId: number): string

        //routing

        getWhenCreateUrlFor(entities: string): string;

        getWhenDetailsUrlFor(entities: string): string;


        //templates
        getDetailsTemplateFor(entities: string): string;

        getDetailedListTemplateFor(entities: string): string;

        getCreateDataTemplateFor(entities: string): string;

        getListTemplateFor(entities: string): string;

        getEditRelationTemplateFor(singleEntities: string, multipleEntities: string): string;

        getTemplateForOwnEntriesList(): string;

        getTemplateForOtherEntriesList(): string;


        getTemplateForOwnEntriesCalendar(): string;

        getTemplateForOtherEntriesCalendar(): string;

        getUserProfileTemplate(): string;
    }
} 