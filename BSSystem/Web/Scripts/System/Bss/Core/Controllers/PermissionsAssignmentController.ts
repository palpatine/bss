﻿module SolvesIt.Bss.Core.Controllers {
    class PermissionsAssignmentControllerState {
        public isEditionOpen: boolean;
        public permissions: Common.Permissions.PermissionsStructure;
        public columnDefinition: Directives.TableAjax.ITableConfiguration;
        public editColumnDefinition: Directives.TableAjax.ITableConfiguration;
        public operationId: string = null;
        public lockPromise: ng.IPromise<SolvesIt.Bss.Models.ILockResult>;
        public canDisplay: boolean;
        public canEdit: boolean;
        public arePermissionsResolved: boolean;
    }

    export class PermissionsAssignmentController {

        private id: number;
        private state: PermissionsAssignmentControllerState;
        private cacheKey: string;

        constructor(
            private stateCacheService: SolvesIt.Widgets.IStateCacheService,
            cacheId: string,
            displayPermissionPath: string,
            editPermissionPath: string,
            private $scope,
            private accessor: SolvesIt.Bss.Core.Accessors.IPermissionsRelatedAccessor,
            private globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {

            this.id = this.$scope.section.id;
            this.cacheKey = cacheId + ":" + this.id;
            this.state = this.stateCacheService.getCacheFor(
                this.cacheKey,
                () => {
                    var state = new PermissionsAssignmentControllerState();
                    var displayPermissionResolution = this.globalStateService.require(displayPermissionPath, this.$scope.section.id, this.$scope.section.entities)
                        .then((result) => {
                        state.canDisplay = result;
                    });
                    this.globalStateService.require(editPermissionPath, this.$scope.section.id, this.$scope.section.entities)
                        .then((result) => {
                        state.canEdit = result;
                        displayPermissionResolution.then(() => {
                            state.arePermissionsResolved = true;
                        });
                    });
                    return state;
                });
            if (!this.state.permissions)
                this.refresh();

            if (!this.state.columnDefinition)
                this.accessor.getPermissionsAssignmentDisplayColumns()
                    .success(data => {
                    this.state.columnDefinition = data;
                });
            var onClose = () => {
                angular.unsubscribe(Events.cardClosed, onClose);
                this.stateCacheService.remove(this.cacheKey);
            }

            angular.subscribe(Events.cardClosed, onClose);

        }

        public edit() {
            this.state.isEditionOpen = true;
            this.state.lockPromise = this.accessor.junctionLock(this.id, SolvesIt.Bss.Entities.permissions)
                .then((data: SolvesIt.Bss.Models.ILockResult) => {
                if (data.validationResult.isValid) {
                    this.state.operationId = data.operationToken;
                    this.refresh();
                    this.accessor.getPermissionsAssignmentEditColumns()
                        .success(items => {
                        this.state.editColumnDefinition = items;
                    });
                } else {
                    this.state.isEditionOpen = false;
                    alert(data.validationResult.errors[0].message);
                }
                return data;
            });
        }

        public save() {
            this.accessor.savePermissionsAssignment(this.id, this.state.permissions, this.state.operationId)
                .success((data: SolvesIt.Bss.Models.IValidationResult) => {
                if (data.isValid) {
                    this.state.isEditionOpen = false;
                    this.refresh();
                } else {
                    //todo: display validation errors
                }
            });
        }

        public cancel() {
            this.state.isEditionOpen = false;
            this.state.lockPromise
                .then((data: SolvesIt.Bss.Models.ILockResult) => {
                if (data.validationResult.isValid) {
                    this.accessor.unlock(this.id, this.state.operationId);
                }
                this.state.operationId = null;
                this.refresh();
            });
        }

        public permissionChanged(item: SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel) {
            this.state.permissions.permissionChanged(item);
        }

        private refresh() {
            var current = this.state.permissions ? this.state.permissions.itemsById : null;

            this.accessor.getAssignedPermissions(this.id)
                .then((data: Common.Permissions.PermissionsStructure) => {
                this.state.permissions = data;

                if (current) {
                    _.each(data.all, (item: { id: number; $$isExpanded: boolean; }) => {
                        item.$$isExpanded = current[item.id] ? (<any>current[item.id]).$$isExpanded : false;
                    });

                } else {
                    _.chain(data.items)
                       .each((item: any) => {
                        item.$$isExpanded = true;
                    });
                }
            });
        }
    }
}