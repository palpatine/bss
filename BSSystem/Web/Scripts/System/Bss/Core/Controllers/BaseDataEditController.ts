﻿module SolvesIt.Bss.Core.Controllers {
    class DetailsPanelControllerState {
        public isEditionOpen: boolean = false;
        public canEdit: boolean = false;
        public canDisplay: boolean = false;
        public arePermissionsResolved: boolean = false;
    }

    class DetailsPanelController {
        static $inject = [
            '$scope',
            'stateCacheService',
            '$q',
            'genericEntityAccessor',
            'globalStateService'
        ];
        public state: DetailsPanelControllerState;
        public cacheKey: string;
        public detailsSection: SolvesIt.Bss.SectionCard;
        public editSection: SolvesIt.Bss.SectionCard;
        private initialRequestPromise: ng.IHttpPromise<SolvesIt.Bss.Models.INamedMarker>;

        constructor(
            private $scope: SolvesIt.Bss.IBssSectionScope,
            private stateCacheService: SolvesIt.Widgets.IStateCacheService,
            private $q: ng.IQService,
            private genericEntityAccessor: SolvesIt.Bss.Core.Accessors.IGenericEntityAccessor,
            private globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {

            $scope.section.addOnCloseHandler(
                (confirm) => {
                    this.stateCacheService.remove(this.cacheKey);
                    confirm();
                });
            this.initialRequestPromise = genericEntityAccessor.getNamedMarkerFor($scope.section.entities, $scope.section.id)
                .success((data) => {
                    $scope.section.title = data.displayName;
                });
            this.editSection = new SolvesIt.Bss.SectionCard(this.$q, () => {
                this.openDetails();
            });
            this.editSection.tag = "editSection->";
            this.editSection.id = $scope.section.id;

            this.detailsSection = new SolvesIt.Bss.SectionCard(this.$q, () => {
            });
            this.detailsSection.tag = "detailsSection->";
            this.detailsSection.id = $scope.section.id;
        }

        public initialize(
            cacheKey: string,
            displayPermissionPath: string,
            editPermissionPath: string) {
            this.cacheKey = cacheKey + ":" + this.$scope.section.id;
            this.state = this.stateCacheService.getCacheFor(
                this.cacheKey,
                () => {
                    var state = new DetailsPanelControllerState();
                    var displayPermissionResolution = this.globalStateService.require(displayPermissionPath, this.$scope.section.id, this.$scope.section.entities)
                        .then((result) => {
                            state.canDisplay = result;
                        });
                    this.globalStateService.require(editPermissionPath, this.$scope.section.id, this.$scope.section.entities)
                        .then((result) => {
                            state.canEdit = result;
                            displayPermissionResolution.then(() => {
                                state.arePermissionsResolved = true;
                            });
                        });
                    return state;
                });

            this.editSection.tag += this.cacheKey;
            this.detailsSection.tag += this.cacheKey;
            this.initialRequestPromise.success(() => {
                if (this.state.isEditionOpen) {
                    this.openEdition();
                } else {
                    this.openDetails();
                }
            }).error(() => {
                this.detailsSection.close();
                this.editSection.close();
                this.$scope.$applyAsync((s: any) => s.section.close());
            });
        }

        public openEdition() {
            if (!this.state.isEditionOpen) {
                this.state.isEditionOpen = true;
                this.detailsSection.close();
            }
            this.editSection.opened();
        }

        public openDetails() {
            if (this.state.isEditionOpen) {
                this.state.isEditionOpen = false;
                this.editSection.close(false);
            }
            this.detailsSection.opened();
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller('detailsPanelController', DetailsPanelController);


    export class BaseDetailsEditorControllerState<TDataModel> {
        public model: TDataModel;
        public operationToken: string;
        public lockRetrival: ng.IPromise<SolvesIt.Bss.Models.ILockResult>;
        public isCreator: boolean;
        public validationErrors: { [reference: string]: string };
        public wasOpen: boolean = false;
    }

    export class BaseDetailsDisplayerControllerState<TDataModel> {
        public model: TDataModel;
    }

    export class BaseDetailsDisplayerController<TDataModel> {

        public model: TDataModel;
        public state: BaseDetailsDisplayerControllerState<TDataModel>;
        public cacheId: string;
        openHandlerDisposer: Bss.IDisposable;

        constructor(
            private stateCacheService: SolvesIt.Widgets.IStateCacheService,
            cacheId: string,
            public $scope,
            public accessor: SolvesIt.Bss.Core.Accessors.IEntityAccessor<TDataModel>) {

            this.cacheId = cacheId;

            var closeHandlerDisposer = $scope.section.addOnCloseHandler((confirm) => {
                this.stateCacheService.remove(this.cacheId);
                this.openHandlerDisposer();
                closeHandlerDisposer();
                confirm();
            });
        }

        public display() {

            this.openHandlerDisposer = this.$scope.section.addOpendedHander(() => {
                this.state = this.stateCacheService.getCacheFor(this.cacheId, () => new BaseDetailsDisplayerControllerState<TDataModel>());
                this.$scope.model = this.state.model;

                if (!this.state.model) {
                    this.accessor.dataFor(this.$scope.section.id)
                        .then(response => {
                            this.state.model = this.$scope.model = response.data;
                            this.$scope.section.title = this.$scope.model.displayName;
                            return response.data;
                        });
                }
            });
        }
    }
    export class BaseDetailsEditorController<TDataModel> {

        public cacheId: string;
        public state: BaseDetailsEditorControllerState<TDataModel>;
        openHandlerDisposer: Bss.IDisposable;

        constructor(
            private stateCacheService: SolvesIt.Widgets.IStateCacheService,
            cacheId: string,
            public $scope: Bss.IBssSectionScope,
            public accessor: SolvesIt.Bss.Core.Accessors.IEntityAccessor<TDataModel>,
            private entity: string,
            private globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {
            this.cacheId = cacheId;
            $scope.model = {};
            this.state = this.stateCacheService.getCacheFor(this.cacheId, () => new BaseDetailsEditorControllerState<TDataModel>());

            var closeHandlerDisposer = $scope.section.addOnCloseHandler((confirm) => {
                this.stateCacheService.remove(this.cacheId);
                this.openHandlerDisposer();
                closeHandlerDisposer();
                confirm();
            });
        }

        public refresh() {
            this.state.model = null;
            this.$scope.section.close();
        }

        public clearRelevantCache() {
            this.stateCacheService.remove(this.entity + "ListController");
            this.stateCacheService.removeSiblings(this.cacheId);
            this.stateCacheService.remove(this.cacheId);
        }

        private create(permission: string): void {
            this.openHandlerDisposer = this.$scope.section.addOpendedHander(() => {
                this.globalStateService.require(permission, null, this.entity)
                    .then(value => {
                        if (!value) {
                            this.$scope.section.close(false);
                        } else {
                            this.$scope.model = this.state.model = this.state.model || this.createEmptyDataModelInstance();
                            this.state.isCreator = true;
                        }
                    });
            });
        }

        public edit(permission: string) {
            this.openHandlerDisposer = this.$scope.section.addOpendedHander(() => {
               
                //todo: loading indicator
                if (this.state.wasOpen) {
                    //todo: refresh lock insead of getting new one
                    this.state.lockRetrival = this.accessor.lock(this.$scope.section.id)
                        .then((lockresult: SolvesIt.Bss.Models.ILockResult) => {
                            if (lockresult.validationResult.isValid) {
                                this.state.operationToken = lockresult.operationToken;
                                this.$scope.model = this.state.model;
                            } else {
                                alert(lockresult.validationResult.errors[0].message);
                            }

                            return lockresult;
                        }, () => {
                            this.$scope.section.close(false);
                            return null;
                        });
                } else {
                    this.state.lockRetrival = this.accessor.lock(this.$scope.section.id)
                        .then((lockresult: SolvesIt.Bss.Models.ILockResult) => {
                            if (lockresult.validationResult.isValid) {
                                this.state.operationToken = lockresult.operationToken;
                                return this.accessor.dataFor(this.$scope.section.id)
                                    .then(response => {
                                        this.state.model = this.$scope.model = response.data;
                                        this.state.wasOpen = true;
                                        return lockresult;
                                    });
                            } else {
                                alert(lockresult.validationResult.errors[0].message);
                            }
                            return lockresult;
                        }, () => {
                            this.$scope.section.close(false);
                            return null;
                        });
                }
            });
        }

        createEmptyDataModelInstance(): TDataModel {
            if (this.$scope.section.modelTemplate) {
                return <TDataModel>angular.extend({}, this.$scope.section.modelTemplate);
            }

            return this.getEmptyDataModelInstance();
        }

        getEmptyDataModelInstance(): TDataModel {
            throw new Error("Not implemented");
        }

        public save() {
            this.accessor.saveData(this.state.model, this.state.operationToken)
                .success(data => {

                    if (data.isValid) {
                        if (this.state.isCreator) {
                            this.$scope.section.id = data.id;
                            this.$scope.section.close(true);
                        } else {
                            this.refresh();
                        }

                        this.clearRelevantCache();
                        return;
                    }

                    this.$scope.validationErrors = this.state.validationErrors = {};
                    _.each(data.errors, error => {
                        var reference = error.reference;
                        this.$scope.validationErrors[reference] = error.message;
                        (<ng.INgModelController>this.$scope.formController[reference]).$setValidity("server", false);
                    });
                });
        }

        public cancel() {
            if (this.state.lockRetrival != null) {
                this.state.lockRetrival.then((lockResult: SolvesIt.Bss.Models.ILockResult) => {
                    this.accessor.unlock(this.$scope.section.id, lockResult.operationToken);
                });
            }

            if (this.state.isCreator) {
                this.$scope.section.close(false);
            } else {
                this.refresh();
            }
        }
    }
} 