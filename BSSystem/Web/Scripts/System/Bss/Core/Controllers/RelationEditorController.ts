﻿module SolvesIt.Bss.Core.Controllers {
    export class RelationEditorControllerState<TEntityModel> {
        public lockRetrival: ng.IPromise<SolvesIt.Bss.Models.ILockResult>;
        public model: SolvesIt.Bss.Models.IDetailedMarker;
        public assignmentsErrors: SolvesIt.Bss.Models.IValidationError[] = [];
        public mainErrors: SolvesIt.Bss.Models.IValidationError[] = [];
        public selected: SolvesIt.Bss.Models.IAssignment[] = [];
        public operationToken: string;
        public isEditionOpen: boolean = false;
        public canDisplay: boolean = false;
        public canEdit: boolean = false;
        public arePermissionsResolved: boolean = false;
    }

    export class RelationEditorController<TEntityModel> {

        id: number;
        state: RelationEditorControllerState<TEntityModel>;
        cacheId: string;
        displayerCacheId: string;
        editorCacheId: string;

        constructor(
            private globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService,
            private stateCacheService: SolvesIt.Widgets.IStateCacheService,
            cacheId: string,
            public $scope: SolvesIt.Bss.IBssSectionScope,
            public accessor: SolvesIt.Bss.Core.Accessors.IEntityAccessor<TEntityModel>,
            private assignmentEditorDataProcessor: SolvesIt.Bss.Widgets.AssignmentEditor.IAssignmentEditorDataProcessor,
            private $q: ng.IQService,
            private relatedEntities: string) {
            this.id = $scope.section.id;
            this.cacheId = cacheId;
            this.displayerCacheId = this.cacheId + "/displayer";
            this.editorCacheId = this.cacheId + "/editor";

            var disposer = $scope.section.addOnCloseHandler((confirm) => {
                this.cancel();
                this.stateCacheService.remove(this.cacheId);
                confirm();
                disposer();
            });
        }

        public initialize(displayPermissionPath: string, editPermissionPath: string, modules: string[]): void {
            this.state = this.stateCacheService.getCacheFor(this.cacheId, () => {
                var state = new RelationEditorControllerState<TEntityModel>();
                var modulesAvabilityResolution = this.globalStateService.areAllModulesAvailable(modules)
                    .then(value => {
                        if (!value) {
                            state.canDisplay = false;
                            state.canEdit = false;
                            state.arePermissionsResolved = true;
                        } else {
                            var displayPermissionResolution = this.globalStateService.require(displayPermissionPath, this.$scope.section.id, this.$scope.section.entities)
                                .then((result) => {
                                    state.canDisplay = result;
                                });
                            this.globalStateService.require(editPermissionPath, this.$scope.section.id, this.$scope.section.entities)
                                .then((result) => {
                                    state.canEdit = result;
                                    displayPermissionResolution.then(() => {
                                        state.arePermissionsResolved = true;
                                    });
                                });
                        }
                    });
                return state;
            });

            this.state.isEditionOpen = false;
        }

        public edit(): void {

            this.state.lockRetrival = this.accessor.junctionLock(this.id, this.relatedEntities)
                .then((lockResult: SolvesIt.Bss.Models.ILockResult) => {
                    if (lockResult.validationResult.isValid) {
                        this.state.isEditionOpen = true;
                        this.state.operationToken = lockResult.operationToken;
                        var modelRetriver = this.accessor.markerFor(this.id)
                            .success(data => {
                                this.state.model = this.$scope.model = data;
                            });

                        var selectedRetriver = this.accessor.relationsFor(this.id, this.relatedEntities)
                            .then(data => {
                                this.state.selected = data;
                            });

                        this.$q.all([modelRetriver, selectedRetriver])
                            .then(() => {
                                this.assignmentEditorDataProcessor.processData(this.state.selected, this.state.model);
                            });
                    } else {
                        this.state.isEditionOpen = false;
                        setTimeout(() => alert(lockResult.validationResult.errors[0].message), 10);
                    }
                    return lockResult;
                });
        }

        public save(): void {
            this.accessor.saveRelations(this.state.model.id, this.state.selected, this.relatedEntities)
                .success(data => {

                    if (data.isValid) {
                        this.state.isEditionOpen = false;
                        this.stateCacheService.remove(this.displayerCacheId);
                        this.stateCacheService.remove(this.editorCacheId);
                        return;
                    }

                    this.$scope.validationErrors = {};
                    this.state.mainErrors = _.filter<SolvesIt.Bss.Models.IValidationError>(data.errors, (element) => {
                        return !element.reference;
                    });

                    var assignmentsErrors = _.filter<SolvesIt.Bss.Models.IValidationError>(data.errors, (element) => {
                        return element.reference && element.reference.indexOf("assignments[") == 0;
                    });

                    _.each(assignmentsErrors, element => {
                        element.reference = element.reference.substring(("assignments").length);
                    });

                    this.state.assignmentsErrors = assignmentsErrors;
                });
        }

        public cancel(): void {
            this.stateCacheService.remove(this.displayerCacheId);
            this.stateCacheService.remove(this.editorCacheId);
            if (this.state.lockRetrival) {
                this.state.lockRetrival.then((lockResult: SolvesIt.Bss.Models.ILockResult) => {
                    this.accessor.unlock(this.id, lockResult.operationToken);
                    this.state.isEditionOpen = false;
                });
            }
        }
    }
} 