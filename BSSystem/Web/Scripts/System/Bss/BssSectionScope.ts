﻿module SolvesIt.Bss {

    export interface IBssSectionScope extends SolvesIt.Bss.IBssArticleScope {
        section: ISectionCard;
        model?: any;
        vm?: any;
        $parent: IBssSectionScope;
        validationErrors: { [reference: string]: string };
        formController?: ng.IFormController;
    }
}
 