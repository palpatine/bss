﻿module SolvesIt.Bss {

    class MessageBoxController {
        static $inject = ['$modalInstance', 'message'];

        constructor(
            private $modalInstance,
            private message) {
        }

        ok() {
            this.$modalInstance.dismiss('cancel');
        }
    }

    Application.getApplication()
        .controller('messageBoxController', MessageBoxController)
        .config([
        '$httpProvider', $httpProvider => {
            $httpProvider.interceptors.push([
                '$rootScope', '$q', '$injector', ($rootScope, $q, $injector: ng.auto.IInjectorService) => {
                    return {
                        responseError: rejection => {
                            if (!rejection.config.ignoreAuthModule) {
                                switch (rejection.status) {
                                    case 403:
                                        $injector.get<angular.ui.bootstrap.IModalService>('$modal').open({
                                            animation: true,
                                            templateUrl: '~/Content/Templates/MessageBox/Main.tpl.html',
                                            controller: 'messageBoxController',
                                            controllerAs: "vm",
                                            size: 'sm',
                                            resolve: {
                                                message: () => {
                                                    return "Brak uprawnień do wybranego elementu.";
                                                }
                                            }
                                        });
                                        break;
                                    case 404:
                                        $injector.get<angular.ui.bootstrap.IModalService>('$modal').open({
                                            animation: true,
                                            templateUrl: '~/Content/Templates/MessageBox/Main.tpl.html',
                                            controller: 'messageBoxController',
                                            controllerAs: "vm",
                                            size: 'sm',
                                            resolve: {
                                                message: () => {
                                                    return "Nie znaleziono elementu.";
                                                }
                                            }
                                        });
                                        break;
                                }
                            }
                            return $q.reject(rejection);
                        }
                    };
                }
            ]);
        }
    ]);
}