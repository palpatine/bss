﻿module SolvesIt.Bss {
    export class TargetKeysProvider {
        public static getTargetKeyFor(entities: string): string {
            if (entities == SolvesIt.Bss.Entities.users) {
                return TargetKeysProvider.users;
            }
            if (entities == SolvesIt.Bss.Entities.sections) {
                return TargetKeysProvider.sections;
            }
            if (entities == SolvesIt.Bss.Entities.positions) {
                return TargetKeysProvider.positions;
            }
            if (entities == SolvesIt.Bss.Entities.organizations) {
                return TargetKeysProvider.organizations;
            }
            if (entities == SolvesIt.Bss.Entities.topics) {
                return TargetKeysProvider.topics;
            }
            if (entities == SolvesIt.Bss.Entities.activities) {
                return TargetKeysProvider.activities;
            }
            if (entities == SolvesIt.Bss.Entities.ownEntries || entities == SolvesIt.Bss.Entities.otherEntries) {
                return TargetKeysProvider.entries;
            }

            throw "Unknown target key for entity " + entities;
        }

        public static getRoleTarget(entities: string, elements: string) {
            if (entities == SolvesIt.Bss.Entities.users) {
                return TargetKeysProvider.getTargetKeyFor(elements);
            }
            return TargetKeysProvider.getTargetKeyFor(entities);
        }

        public static users: string = "Common_User";
        public static sections: string = "Common_Section";
        public static positions: string = "Common_Position";
        public static companies: string = "Common_Company";
        public static organizations: string = "Common_Organization";
        public static topics: string = "Wtt_Topic";
        public static activities: string = "Wtt_Activity";
        public static entries: string = "Wtt_Entry";
    }
} 