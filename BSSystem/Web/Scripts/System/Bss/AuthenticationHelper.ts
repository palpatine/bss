﻿module SolvesIt.Bss.Core {

    //todo: convert authentication window to angular
    class AuthenticationHelper {
        private processing: boolean = false;
        private login: JQuery;
        private main: JQuery;

        constructor(
            private scope,
            private $compile: ng.ICompileService,
            private elem,
            private $http: ng.IHttpService,
            private authService,
            private coreBackendAccessor: SolvesIt.Bss.Core.ApplicationManagement.IBackendAccessor,
            private globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {
            this.main = this.elem;
        }

        public run(): void {
            //once Angular is started, remove class:
            setTimeout(() => { this.elem.toggle(); }, 0);

            this.scope.$on('event:auth-loginRequired', () => {
                if (this.processing) return;
                this.processing = true;
                this.coreBackendAccessor.getLogOnPanel()
                    .success(data => {
                        this.showLogin(data);
                    });
            });
            this.scope.$on('event:auth-loginConfirmed', () => { this.showContent(); });
        }

        private showLogin(data: string): void {
            this.login = this.$compile(data)(this.scope.$new());
            this.login.css("display", "block"); 
            this.main.css("display", "none");
            $("body").append(this.login);            
        }

        private showContent() {
            this.globalStateService.initilizeState()
                .then(() => {
                    this.login.css("display", "none");
                    this.main.css("display", "block");
                    this.login.remove();
                    this.login = null;
                    this.processing = false;
                });
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('authHandler', [
            '$http', '$compile', 'authService', 'coreBackendAccessor', 'globalStateService', ($http: ng.IHttpService, $compile: ng.ICompileService, authService, coreBackendAccessor, globalStateService) => {
                return {
                    restrict: 'A',
                    link: (scope, elem, attrs) => {
                        var helper = new AuthenticationHelper(scope, $compile, elem, $http, authService, coreBackendAccessor, globalStateService);
                        helper.run();
                    }
                };
            }
        ]);
} 