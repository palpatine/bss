﻿module SolvesIt.Bss {

    export interface IDisposable {
        (): void;
    }
    export interface ISectionCard {
        title: string;
        route: string;
        icon?: string;
        entities: string;
        canClose: boolean;
        addOnCloseHandler(handler: (confirm: () => void, success: boolean) => void): IDisposable;
        addOpendedHander(handler: () => void): IDisposable;
        addReadyHander(handler: () => void): IDisposable;
        close(success?: boolean): void;
        selected(): void;
        opened(): void;
        id?: number;
        modelTemplate?: any;
        casheKey?: string;
    }

    export class SectionCard implements ISectionCard {
        public title: string;
        public route: string;
        public icon: string;
        public canClose: boolean = true;
        public tag: string;

        public id: number;
        public modelTemplate: any;
        public entities: string;
        public casheKey: string;

        private innerClose: (success: boolean) => void;
        private selectedHandlers: { (): void }[] = [];
        private openedHandlers: { (): void }[] = [];
        private readyHandlers: { (): void }[] = [];
        private closeHandlers: { (confirm: () => void, success: boolean): void }[] = [];
        private deferer: ng.IDeferred<{}>;

        constructor(
            private $q: ng.IQService,
            close: (success: boolean) => void) {
            this.innerClose = close;
            this.deferer = $q.defer();
        }

        close(success?: boolean): void {
            var onClose = this.buildOnClose(success);
            if (onClose) {
                onClose(() => {
                    this.innerClose(success);
                    this.deferer = this.$q.defer();
                });
            } else {
                this.innerClose(success);
                this.deferer = this.$q.defer();
            }
        }

        addOnCloseHandler(handler: (confirm: () => void, success: boolean) => void): IDisposable {
            this.closeHandlers.push(handler);

            return () => {
                var index = this.closeHandlers.indexOf(handler);
                this.closeHandlers.splice(index, 1);
            }
        }

        buildOnClose(success: boolean): { (confirm: () => void): void } {
            var onClose = null;

            _.each(this.closeHandlers, (handler) => {
                if (onClose) {
                    var currnet = onClose;
                    onClose = (confirm) => {
                        currnet(() => {
                            handler(confirm, success);
                        });
                    }
                } else {
                    onClose = handler;
                }
            });

            // ReSharper disable ExpressionIsAlwaysConst false positive
            return onClose;
            // ReSharper restore ExpressionIsAlwaysConst
        }

        selected() {
            _.each(this.selectedHandlers, handler => {
                handler();
            });
        }

        addSelectedHander(handler: () => void): IDisposable {
            this.selectedHandlers.push(handler);

            return () => {
                var index = this.selectedHandlers.indexOf(handler);
                this.selectedHandlers.splice(index, 1);
            }
        }

        opened() {
            this.deferer.promise.then(() => {
                _.each(this.openedHandlers, handler => {
                    handler();
                });
            });
        }

        addOpendedHander(handler: () => void): IDisposable {
            this.openedHandlers.push(handler);

            return () => {
                var index = this.openedHandlers.indexOf(handler);
                this.openedHandlers.splice(index, 1);
            }
        }

        addReadyHander(handler: () => void): IDisposable {
            this.readyHandlers.push(handler);

            return () => {
                var index = this.readyHandlers.indexOf(handler);
                this.readyHandlers.splice(index, 1);
            }
        }

        ready() {
            _.each(this.readyHandlers, handler => {
                handler();
            });
            this.deferer.resolve();
        }
    }
} 