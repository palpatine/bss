var DEBUG;
var scrollBarWidth;
var configureRouting: (routeProvider) => void;

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
//$(function () {
//    $(window).bind("load resize", function () {

//    });
//});

module SolvesIt.Bss {

    export class Application {
        static mvc: ng.IModule;

        static route(routeAddress: string,
            configurationRetriver: (routeFactory: SolvesIt.Bss.Core.IRouteFactory) => ng.route.IRoute): void
        static route(routeAddressRetriver: (routeFactory: SolvesIt.Bss.Core.IRouteFactory) => string,
            configurationRetriver: (routeFactory: SolvesIt.Bss.Core.IRouteFactory) => ng.route.IRoute): void
        static route(routeAddressOrRetriverRetriver: any,
            configurationRetriver: (routeFactory: SolvesIt.Bss.Core.IRouteFactory) => ng.route.IRoute): void {
            Application.getApplication().config(
                [
                    '$routeProvider', 'routeFactoryProvider',
                    ($routeProvider: ng.route.IRouteProvider, routeFactory) => {
                        routeFactory = routeFactory.$get();
                        var routeAddress = _.isFunction(routeAddressOrRetriverRetriver) ? routeAddressOrRetriverRetriver(routeFactory) : routeAddressOrRetriverRetriver;
                        var route = configurationRetriver(routeFactory);
                        $routeProvider.when(routeAddress, route);
                    }]);
        }

        static GetLogonApplication(): ng.IModule {
            if (Application.mvc) {
                return Application.mvc;
            }

            Application.mvc = angular.module('bssLogon', [
                'ngMessages',
                'ui.bootstrap',
                'ui.utils'
            ])
                .config([
                '$httpProvider', $httpProvider => {
                    $httpProvider.interceptors.push($q => {
                        return {
                            request: (config: { url: string }) => {
                                config.url = config.url.replace(/^~\//, Globals.rootPath);
                                return config || $q.when(config);
                            }
                        };
                    });
                }
            ]);

            return Application.mvc;
        }

        static getApplication(): ng.IModule {
            if (Application.mvc) {
                return Application.mvc;
            }
            Application.mvc = angular.module('bss', [
                'ngMessages',
                'adaptv.adaptStrap',
                'ui.checkbox',
                'http-auth-interceptor',
                'ui.calendar',
                'ui.bootstrap',
                'ui.utils',
                'ngRoute',
                'colorpicker.module',
                'ui.bootstrap.datetimepicker']);

            Application.mvc.config(
                [
                    '$routeProvider',
                    ($routeProvider: ng.route.IRouteProvider) => {
                        configureRouting($routeProvider);
                        $routeProvider
                            .otherwise({
                            template: "Podany adres nie istnieje"
                        });
                    }
                ])
                .config([
                '$httpProvider', $httpProvider=> {
                    $httpProvider.defaults.cache = false;
                }
            ])
                .config([
                '$adConfigProvider', $adConfigProvider=> {
                    $adConfigProvider.paging.request = {
                        start: 'skip',
                        pageSize: 'limit',
                        page: 'page',
                        sortField: 'sort',
                        sortDirection: 'sortDirection',
                        sortAscValue: 'asc',
                        sortDescValue: 'desc'
                    };


                    $adConfigProvider.paging.response = {
                        totalItems: 'count',
                        itemsLocation: 'content',
                        format: 'json'
                    };

                    $adConfigProvider.iconClasses.expand = 'glyphicon glyphicon-plus-sign';
                    $adConfigProvider.iconClasses.collapse = 'glyphicon glyphicon-minus-sign';
                    $adConfigProvider.iconClasses.loadingSpinner = 'awaiter';
                    $adConfigProvider.iconClasses.firstPage = 'small-icon first';
                    $adConfigProvider.iconClasses.previousPage = 'small-icon previous';
                    $adConfigProvider.iconClasses.nextPage = 'small-icon next';
                    $adConfigProvider.iconClasses.lastPage = 'small-icon last';
                    $adConfigProvider.iconClasses.sortAscending = 'small-icon ascending';
                    $adConfigProvider.iconClasses.sortDescending = 'small-icon descending';
                    $adConfigProvider.iconClasses.sortable = 'small-icon sortable';
                    $adConfigProvider.iconClasses.draggable = 'glyphicon glyphicon-align-justify';
                    $adConfigProvider.iconClasses.selectedItem = 'glyphicon glyphicon-ok';
                }])
                .config(['$httpProvider', $httpProvider => {
                $httpProvider.interceptors.push($q => {
                    return {
                        request: (config: { url: string }) => {
                            config.url = config.url.replace(/^~\//, Globals.rootPath);
                            return config || $q.when(config);
                        }
                    };
                });
            }])
                .directive('resize', ['$window', $window => {
                return (scope, element, attr) => {

                    var w = angular.element($window);
                    scope.$watch(() => {
                        return {
                            'h': w.height(),
                            'w': w.width()
                        };
                    }, (newValue, oldValue) => {
                            scope.windowHeight = newValue.h;
                            scope.windowWidth = newValue.w;
                            scope.$eval(attr.notifier);
                        }, true);


                    w.bind('resize', () => {
                        scope.$apply();
                    });
                }
            }]);

            return Application.mvc;
        }

        static getScrollBarWidth(): number {
            var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
                widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
            $outer.remove();
            return 100 - widthWithScroll;
        }

        static calculateMainNavigatorWidth(): void {
            if ((<any>$("body")).hasScrollBar()) {
                $("#MainNavigation").width($(window).width() - 47 - scrollBarWidth);
            } else {
                $("#MainNavigation").width($(window).width() - 47);
            }
        }

        static bootstrap() {
            scrollBarWidth = this.getScrollBarWidth();

            var browser = this.detectBrowser();

            if (browser.indexOf("IE") != -1) {
                $("html").addClass("ie");
            } else {
                $("html").addClass("browser");
            }

            // FluidLayoutManager.Initialize();

            //$(window).on("resize", () => {
            //    this.calculateMainNavigatorWidth();
            //});
        }

        static detectBrowser(): string {
            var ua = navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
                return 'IE ' + (tem[1] || '');
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
            return M.join(' ');
        }
    }
}

($=> {
    $.fn.hasScrollBar = function () {
        return this.get(0).scrollHeight > this.height();
    };
})(jQuery);


$(() => {
    SolvesIt.Bss.Application.bootstrap();
});