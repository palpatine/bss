﻿module SolvesIt.Bss {

    export class Entities {
        static modules: string = "Module";
        static users: string = "User";
        static sections: string = "Section";
        static positions: string = "Position";
        static signDefinitions: string = "SignDefinition";
        static organizations: string = "Organization";
        static roleDefinitions: string = "RoleDefinition";
        static permissions: string = "Permission";
        static signTargets: string = "SignTarget";
        static applicableTerminals: string = "ApplicableTerminal";

        static topics: string = "Topic";
        static entryLimitExclusions: string = "EntryLimitExclusion";
        static topicKinds: string = "TopicKind";
        static activityKinds: string = "ActivityKind";
        static activities: string = "Activity";
        static ownEntries: string = "OwnEntry";
        static otherEntries: string = "OtherEntry";
        static detailedEntries: string = "DetailedEntry";

        static getModule(entities: string): string {
            switch (entities) {
                case Entities.users:
                case Entities.sections:
                case Entities.positions:
                case Entities.signDefinitions:
                case Entities.organizations:
                case Entities.modules:
                case Entities.roleDefinitions:
                case Entities.permissions:
                    return "Common";
                case Entities.topics:
                case Entities.entryLimitExclusions:
                case Entities.topicKinds:
                case Entities.activityKinds:
                case Entities.activities:
                case Entities.ownEntries:
                case Entities.otherEntries:
                    return "Wtt";
                default:
                    throw "Entities.ts: Module for entity " + entities + " is not defined";
            }
        }

        static isDated(entities: string) {
            if (entities == Entities.modules)
                return false;
            return true;
        }
    }
}