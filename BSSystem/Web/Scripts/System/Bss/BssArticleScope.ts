﻿module SolvesIt.Bss {
    
    export interface IBssArticleScope extends ng.IScope {
        $$articleVisibleHeight: number;
    }
} 