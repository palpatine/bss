﻿module LogOn {

    class LogOnAuthService {

        constructor() {
            setTimeout(() => { $('[ng-app="bssLogon"]').toggle(); }, 0);
        }

        loginConfirmed() {
            window.location.href = Globals.rootPath + window.location.hash;
        }
    }

    SolvesIt.Bss.Application.GetLogonApplication()
        .service('authService', LogOnAuthService);

}
