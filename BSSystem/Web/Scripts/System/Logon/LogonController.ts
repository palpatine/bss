﻿module SolvesIt.Bss.Core{
    interface ILogonModel {
        login: string;
        password: string;
        validationError: string;
    }

    export class LogonController {

        public static $inject = [
            '$scope',
            'authService',
            '$http'
        ];

        constructor(
            public $scope,
            private authService,
            private $http: ng.IHttpService) {

            $scope.model = {};
        }

        public onSubmit(): void {
            this.$http.post<SolvesIt.Bss.Models.IValidationResult>('~/api/List/LogOn/Post', {
                login: this.$scope.model.login,
                password: this.$scope.model.password
            })
                .success(data => {
                if (data.isValid) {
                    this.authService.loginConfirmed();
                }
                else {
                    this.$scope.model.validationError = data.errors[0].message;
                }
            });
        }
    }

    Application.getApplication()
        .controller('logonController', LogonController);
}