﻿/// <reference path="..\..\..\typings\moment\moment.d.ts" />
module SolvesIt.Widgets.QueryString {

    export interface IODataQueryParametersGeneratorFactory {
        create(): IODataQueryParametersGenerator;
    }

    export interface IODataQueryParametersGenerator {
        where(baseFilter?: FilterGroup): IFilterGenerator;
        generate(): IQueryDescriptor;
    }

    class ODataQueryParametersGeneratorFactory implements IODataQueryParametersGeneratorFactory {

        constructor() {
        }

        public create() {
            return new QueryParametersGeneratorInstance();
        }
    }

    class QueryParametersGeneratorInstance {
        filterGenerator: FilterGenerator;

        public where(baseFilter?: FilterGroup): IFilterGenerator {
            if (this.filterGenerator != null) {
                throw "filter already created";
            }
            this.filterGenerator = new FilterGenerator(baseFilter);
            return this.filterGenerator;
        }

        public generate(): IQueryDescriptor {
            var data: IQueryDescriptor = {
                filterProperties: this.filterGenerator.getData()
            };
            return data;
        }
    }


    class FilterGenerator implements IGroupFilterGenerator, IFilterGenerator {

        groups: FilterGroup[];
        currentGroup: FilterGroup;

        public static $inject = [
            'bssODataQuerySerializer'
        ];

        constructor(baseFilter?: FilterGroup) {
            this.currentGroup =
            {
                items: [],
                isNegated: false,
                isOr: undefined
            };
            if (baseFilter) {
                this.groups = [baseFilter];
                baseFilter.items.push(this.currentGroup);
                this.groups.push(this.currentGroup);
            } else {
                this.groups = [this.currentGroup];
            }
        }

        property<T>(property: string): IComparisionSelector<T> {

            var comparision = this.addProperty(property);
            return new ComparisionSelector(this, comparision);
        }

        addProperty(property: string): FilterPropertyDescriptor {
            var comparision: FilterPropertyDescriptor = new FilterPropertyDescriptor();
            comparision.propertyName = property;
            comparision.filterOption = FilterPropertyOption.eq;
            comparision.propertyValue = null;

            this.currentGroup.items.push(comparision);
            return comparision;
        }

        openGroup(): IFilterGenerator {
            var group: FilterGroup = new FilterGroup();

            group.items = [];
            group.isNegated = false;

            this.groups.push(group);
            this.currentGroup.items.push(group);
            this.currentGroup = group;
            return this;
        }

        openNegatedGroup(): IFilterGenerator {
            this.openGroup();
            this.currentGroup.isNegated = true;
            return this;
        }

        wrapInGroup(): void {
            var property = this.currentGroup.items.pop();
            this.openGroup();
            this.currentGroup.items.push(property);
        }

        and(): IFilterGenerator {
            this.currentGroup.items[this.currentGroup.items.length - 1].isOr = false;
            return this;
        }

        or(): IFilterGenerator {
            this.currentGroup.items[this.currentGroup.items.length - 1].isOr = true;
            return this;
        }

        getData(): FilterGroup {
            if (this.groups.length != 1) {
                throw "there are not closed groups in expression";
            }

            return this.groups[0];
        }

        closeGroup(): IGroupFilterGenerator {

            this.groups.pop();
            this.currentGroup = this.groups[this.groups.length - 1];
            return this;
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("oDataQueryParametersGeneratorFactory", ODataQueryParametersGeneratorFactory);

    class ComparisionSelector<T> implements IComparisionSelector<T> {

        constructor(
            private generator: FilterGenerator,
            private property: FilterPropertyDescriptor) {
        }

        startswith(value: string): IGroupFilterGenerator {
            this.property.filterOption = FilterPropertyOption.startswith;
            this.property.propertyValue = value;
            return this.generator;
        }

        eq(value: T): IGroupFilterGenerator {
            this.property.filterOption = FilterPropertyOption.eq;
            this.property.propertyValue = value;
            return this.generator;
        }
        ne(value: T): IGroupFilterGenerator {
            this.property.filterOption = FilterPropertyOption.ne;
            this.property.propertyValue = value;
            return this.generator;
        }
        gt(value: T): IGroupFilterGenerator {
            this.property.filterOption = FilterPropertyOption.gt;
            this.property.propertyValue = value;
            return this.generator;

        }
        ge(value: T): IGroupFilterGenerator {
            this.property.filterOption = FilterPropertyOption.ge;
            this.property.propertyValue = value;
            return this.generator;
        }
        lt(value: T): IGroupFilterGenerator {
            this.property.filterOption = FilterPropertyOption.lt;
            this.property.propertyValue = value;
            return this.generator;
        }
        le(value: T): IGroupFilterGenerator {
            this.property.filterOption = FilterPropertyOption.le;
            this.property.propertyValue = value;
            return this.generator;
        }

        eqOrNull(value: T): IGroupFilterGenerator {
            this.compareOrNull(value, FilterPropertyOption.eq);
            return this.generator;
        }
        neOrNull(value: T): IGroupFilterGenerator {
            this.compareOrNull(value, FilterPropertyOption.ne);
            return this.generator;
        }
        gtOrNull(value: T): IGroupFilterGenerator {
            this.compareOrNull(value, FilterPropertyOption.gt);
            return this.generator;
        }
        geOrNull(value: T): IGroupFilterGenerator {
            this.compareOrNull(value, FilterPropertyOption.ge);
            return this.generator;
        }
        ltOrNull(value: T): IGroupFilterGenerator {
            this.compareOrNull(value, FilterPropertyOption.lt);
            return this.generator;
        }
        leOrNull(value: T): IGroupFilterGenerator {
            this.compareOrNull(value, FilterPropertyOption.le);
            return this.generator;
        }


        compareOrNull(value: T, operator: FilterPropertyOption): void {
            this.generator.wrapInGroup();
            this.eq(null);
            this.property.isOr = true;

            var second = this.generator.addProperty(this.property.propertyName);
            second.propertyValue = value;
            second.filterOption = operator;
            this.generator.closeGroup();
        }

        inArray(value: T[]): IGroupFilterGenerator {
            this.checkInArray(value, false);
            return this.generator;
        }

        notInArray(value: T[]): IGroupFilterGenerator {
            this.checkInArray(value, true);
            return this.generator;
        }

        checkInArray(value: T[], isNegated: boolean): void {

            this.generator.wrapInGroup();
            this.generator.currentGroup.isNegated = isNegated;
            var current = this.property;
            _.each(value, (item: T, i: number) => {
                current.filterOption = FilterPropertyOption.le;
                current.propertyValue = value;
                current.isOr = true;

                if (value.length < i + 1) {
                    current = this.generator.addProperty(current.propertyName);
                }
            });

            this.generator.closeGroup();
        }
    }

    export interface IFilterGenerator {
        property<T>(property: string): IComparisionSelector<T>;

        openGroup(): IFilterGenerator;

        openNegatedGroup(): IFilterGenerator;
    }


    export interface IGroupFilterGenerator {
        and(): IFilterGenerator;
        or(): IFilterGenerator;
        closeGroup(): IGroupFilterGenerator;
    }

    export interface IComparisionSelector<T> {
        startswith(value: string): IGroupFilterGenerator;
        eq(value: T): IGroupFilterGenerator;
        ne(value: T): IGroupFilterGenerator;
        gt(value: T): IGroupFilterGenerator;
        ge(value: T): IGroupFilterGenerator;
        lt(value: T): IGroupFilterGenerator;
        le(value: T): IGroupFilterGenerator;

        inArray(value: T[]): IGroupFilterGenerator;

        eqOrNull(value: T): IGroupFilterGenerator;
        neOrNull(value: T): IGroupFilterGenerator;
        gtOrNull(value: T): IGroupFilterGenerator;
        geOrNull(value: T): IGroupFilterGenerator;
        ltOrNull(value: T): IGroupFilterGenerator;
        leOrNull(value: T): IGroupFilterGenerator;

    }

    export interface IQueryDescriptor {
        pageSize?: number;
        pageNumber?: number;
        orderProperties?: IOrderPropertyDescriptor[];
        filterProperties?: IFilterItem;
    }


    export interface IOrderPropertyDescriptor {
        propertyName: string;
        orderDescending: boolean;
        displayName: string;
    }

    export class FilterPropertyDescriptor implements IFilterItem {
        propertyName: string;
        propertyValue: any;
        filterOption: FilterPropertyOption;
        isOr: boolean;
    }

    export interface IFilterItem {
        isOr: boolean;
    }

    export class FilterGroup implements IFilterItem {
        items: IFilterItem[];
        isNegated: boolean;
        isOr: boolean;
    }

    export enum FilterPropertyOption {
        startswith = 1,
        eq,
        ne,
        gt,
        ge,
        lt,
        le
    }

    export interface IODataQuerySerializer {
        serialize(queryDescriptor: IQueryDescriptor): { [key: string]: string };
    }

    class ODataQuerySerializer implements IODataQuerySerializer {

        operators: { [id: number]: string; } =
        {
            1: 'startswith',
            2: 'eq',
            3: 'ne',
            4: 'gt',
            5: 'ge',
            6: 'lt',
            7: 'le'
        };

        public serialize(queryDescriptor: IQueryDescriptor): { [key: string]: string } {
            if (!queryDescriptor) {
                return {};
            }
            var elementsString: { [key: string]: string } = {};
            if (queryDescriptor.pageSize !== undefined && queryDescriptor.pageSize !== null) {

                if (queryDescriptor.pageNumber !== undefined && queryDescriptor.pageNumber !== null) {
                    elementsString['$skip'] = '' + queryDescriptor.pageSize * (queryDescriptor.pageNumber - 1);
                }
                elementsString['$top'] = '' + queryDescriptor.pageSize;
            }


            if (queryDescriptor.orderProperties !== undefined && queryDescriptor.orderProperties !== null && queryDescriptor.orderProperties.length > 0) {
                var orderingString = '';
                _.each(queryDescriptor.orderProperties, (s: IOrderPropertyDescriptor, i) => {
                    orderingString += s.propertyName;
                    if (s.orderDescending) {
                        orderingString += ' desc';
                    }
                    if (i + 1 !== queryDescriptor.orderProperties.length)
                        orderingString += ',';
                });
                elementsString['$orderby'] = orderingString;

            }


            if (queryDescriptor.filterProperties !== undefined && queryDescriptor.filterProperties !== null) {
                var current = <any>queryDescriptor.filterProperties;
                var filteringString = this.processFilter(current);
                elementsString['$filter'] = filteringString;
            }

            return elementsString;
        }


        private formatValue(propertyValue: any): any {
            var propValue = null;
            if (typeof (propertyValue) === 'string') {
                propValue = '\'' + propertyValue + '\'';
            } else if (typeof (propertyValue) === "number") {
                propValue = propertyValue;
            } else if (this.dateDescriptor(propertyValue).isDate) {
                propValue = this.dateDescriptor(propertyValue).dateValue;
            } else if (typeof (propertyValue) === "boolean") {
                propValue = propertyValue.toString();
            };

            return propValue;
        }

        private processFilter(current: IFilterItem): string {
            var filteringString = '';
            if (current instanceof FilterPropertyDescriptor) {
                var filter = <FilterPropertyDescriptor>current;
                var propValue = this.formatValue(filter.propertyValue);

                if (filter.filterOption === FilterPropertyOption.startswith) {
                    filteringString += this.operators[filter.filterOption] + '(' + filter.propertyName + ',' + propValue + ')';
                } else {
                    filteringString += filter.propertyName + ' ' + this.operators[filter.filterOption] + ' ' + propValue;
                }
            } else {
                var connector = <FilterGroup>current;
                filteringString += "(";

                _.each(connector.items, (s: IFilterItem) => {
                    filteringString += this.processFilter(s);
                });

                filteringString += ")";
            }

            if (current.isOr !== undefined) {
                if (current.isOr) {
                    filteringString += " or ";
                }
                else {
                    filteringString += " and ";
                }
            }

            return filteringString;
        }


        private dateDescriptor(val): { isDate: boolean; dateValue: any } {
            var isDateValue = false;
            var dateVal = null;
            if (Object.prototype.toString.call(val) === "[object Date]") {
                isDateValue = true;
                dateVal = moment(val).toISOString();
            }
            return { isDate: isDateValue, dateValue: dateVal };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .provider('bssODataQuerySerializer', () => {
        return {
            $get: () => new ODataQuerySerializer()
        };
    });
}