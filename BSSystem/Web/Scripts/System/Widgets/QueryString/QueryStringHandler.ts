﻿module SolvesIt.Widgets.QueryString {
    export interface IQueryStringHandler {
        createGenerator(): SolvesIt.Widgets.QueryString.IODataQueryParametersGenerator;
        serialize(queryDescriptor: SolvesIt.Widgets.QueryString.IQueryDescriptor): { [key: string]: string };
        apply(ajaxConfigParams: {}, queryDescriptor: SolvesIt.Widgets.QueryString.IQueryDescriptor): {};
        process<T>(data: T[], queryDescriptor: SolvesIt.Widgets.QueryString.IQueryDescriptor): T[];
    }

    class QueryStringHandler implements IQueryStringHandler {
        static $inject = [
            'bssODataQuerySerializer',
            'oDataQueryParametersGeneratorFactory'
        ];

        constructor(
            private oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            private oDataQueryParametersGeneratorFactory: SolvesIt.Widgets.QueryString.IODataQueryParametersGeneratorFactory) {
        }

        public createGenerator(): SolvesIt.Widgets.QueryString.IODataQueryParametersGenerator {
            return this.oDataQueryParametersGeneratorFactory.create();
        }

        public serialize(queryDescriptor: SolvesIt.Widgets.QueryString.IQueryDescriptor): { [key: string]: string } {
            return this.oDataQuerySerializer.serialize(queryDescriptor);
        }

        public apply(ajaxConfigParams: {}, queryDescriptor: SolvesIt.Widgets.QueryString.IQueryDescriptor): {} {
            var serialized = this.serialize(queryDescriptor);

            var clone = angular.extend({}, ajaxConfigParams);
            _.each(serialized, (item, key) => {
                if (!clone[key]) {
                    clone[key] = item;
                } else if (key == "$filter") {
                    clone['$filter'] = "(" + clone['$filter'] + ") and (" + item + ")";
                }
            });

            return clone;
        }

        processItems(item: any, filterItems: SolvesIt.Widgets.QueryString.IFilterItem[]): boolean {
            var result: boolean = null;
            var isOr: boolean;

            if (filterItems.length === 0) {
                console.log("invalid filter");
                throw "Invalid filter";
            }

            _.each(filterItems, filterItem => {
                if (filterItem instanceof SolvesIt.Widgets.QueryString.FilterPropertyDescriptor) {
                    if (result === null) {
                        result = this.processFilterPropertyDescriptor(item, <SolvesIt.Widgets.QueryString.FilterPropertyDescriptor>filterItem);
                    } else if (isOr === true) {
                        result = result || this.processFilterPropertyDescriptor(item, <SolvesIt.Widgets.QueryString.FilterPropertyDescriptor>filterItem);
                    } else if (isOr === false) {
                        result = result && this.processFilterPropertyDescriptor(item, <SolvesIt.Widgets.QueryString.FilterPropertyDescriptor>filterItem);
                    } else {
                        console.log("Invalid filter");
                        throw "Invalid filter";
                    }
                } else {
                    var group = <SolvesIt.Widgets.QueryString.FilterGroup>filterItem;
                    if (result === null) {
                        result = this.processFilterGroup(item, group);
                    } else if (isOr === true) {
                        result = result || this.processFilterGroup(item, group);
                    } else if (isOr === false) {
                        result = result && this.processFilterGroup(item, group);
                    } else {
                        console.log("Invalid filter");
                        throw "Invalid filter";
                    }
                }

                isOr = filterItem.isOr;
            });

            //// resharper is wrong
            // ReSharper disable ExpressionIsAlwaysConst 
            return result;
            // ReSharper restore ExpressionIsAlwaysConst
        }

        processFilterGroup(item: any, filterGroup: SolvesIt.Widgets.QueryString.FilterGroup): boolean {
            var result = this.processItems(item, filterGroup.items);
            if (filterGroup.isNegated) {
                return !result;
            }
            return result;
        }

        processFilterPropertyDescriptor(element: any, propertyDescriptor: SolvesIt.Widgets.QueryString.FilterPropertyDescriptor): boolean {
            var value = element[propertyDescriptor.propertyName];
            var result: boolean;
            if (propertyDescriptor.filterOption == SolvesIt.Widgets.QueryString.FilterPropertyOption.eq) {
                result = value == propertyDescriptor.propertyValue;
            } else if (propertyDescriptor.filterOption == SolvesIt.Widgets.QueryString.FilterPropertyOption.ge) {
                result = value >= propertyDescriptor.propertyValue;
            } else if (propertyDescriptor.filterOption == SolvesIt.Widgets.QueryString.FilterPropertyOption.gt) {
                result = value > propertyDescriptor.propertyValue;
            } else if (propertyDescriptor.filterOption == SolvesIt.Widgets.QueryString.FilterPropertyOption.le) {
                result = value <= propertyDescriptor.propertyValue;
            } else if (propertyDescriptor.filterOption == SolvesIt.Widgets.QueryString.FilterPropertyOption.lt) {
                result = value < propertyDescriptor.propertyValue;
            } else if (propertyDescriptor.filterOption == SolvesIt.Widgets.QueryString.FilterPropertyOption.ne) {
                result = value != propertyDescriptor.propertyValue;
            } else if (propertyDescriptor.filterOption == SolvesIt.Widgets.QueryString.FilterPropertyOption.startswith) {
                result = (<string>value).indexOf(propertyDescriptor.propertyValue) == 0;
            } else {
                console.log('unknown comparision');
                throw 'unknown comparision';
            }
            return result;
        }

        public process<T>(data: T[], queryDescriptor: SolvesIt.Widgets.QueryString.IQueryDescriptor): T[] {

            var result = _.filter(data, item => {
                return this.processItems(item, [queryDescriptor.filterProperties]);
            });

            return result;
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("queryStringHandler", QueryStringHandler);
} 