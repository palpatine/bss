﻿module SolvesIt.Widgets.Table{
    interface IBssTableHierarchicalDataSourceAttributes extends ng.IAttributes {
        bssTableHierarchicalDataSource: string;
    }

    class BssTableHierarchicalDataSourceController extends BssBaseTableHierarchicalDataSourceController {
        public static $inject = ['$scope', '$attrs'];

        constructor(
            $scope,
            private $attrs) {
            super($scope);

            this.$scope.$watch(this.$attrs.bssTableHierarchicalDataSource, value => {
                this.allItems = <any[]>value;
                this.loadItems();
            });
        }
        public onControllerSet() {
            this.controller.state.isLoading = false;
        }

    }


    class BssTableHierarchicalDataSourceHandlerDirective extends BssBaseTableHierarchicalDataSourceHandlerDirective {

        constructor() {
            super("bssTableHierarchicalDataSource");
        }
    }

    SolvesIt.Bss.   Application.getApplication()
        .directive('bssTableHierarchicalDataSource',(new BssTableHierarchicalDataSourceHandlerDirective()).factory())
        .controller('bssTableHierarchicalDataSourceController', BssTableHierarchicalDataSourceController);
} 