﻿module SolvesIt.Widgets.Table{
    interface IBssTableArrayColumnsDefinitionSourceAttributes extends ng.IAttributes {
        bssTableArrayColumnsDefinitionSource: string;
    }

    class BssTableArrayColumnsDefinitionSourceController {
        public static $inject = ['$scope', '$attrs', '$http'];
        private controller: BssTableController;
        private items: any[];

        constructor(
            private $scope,
            private $attrs: IBssTableArrayColumnsDefinitionSourceAttributes,
            private $http: ng.IHttpService) {

        }

        public setTableController(
            controller: BssTableController
            ): void {

            this.controller = controller;
            this.$scope.$watch(this.$attrs.bssTableArrayColumnsDefinitionSource, value => {
                this.controller.state.tableConfiguration = value;
            });
        }
    }

    class BssTableArrayColumnsDefinitionSourceHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ['bssTableArrayColumnsDefinitionSource', "^bssTable"];
        controller = "bssTableArrayColumnsDefinitionSourceController";

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var ownController = <BssTableArrayColumnsDefinitionSourceController>controller[0];
                    var bssTableController = <BssTableController>controller[1];
                    ownController.setTableController(bssTableController);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssTableArrayColumnsDefinitionSource',(new BssTableArrayColumnsDefinitionSourceHandlerDirective()).factory())
        .controller('bssTableArrayColumnsDefinitionSourceController', BssTableArrayColumnsDefinitionSourceController);
} 