﻿module Directives.TableAjax {

    export interface ITableConfiguration {
        columnDefinition: IColumnDefinition[];
    }

}