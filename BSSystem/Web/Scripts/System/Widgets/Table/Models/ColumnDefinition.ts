﻿module Directives.TableAjax {

    export class ColumnDefinition
        implements IColumnDefinition {

        columnHeaderTemplate: string;
        columnHeaderDisplayName: string;

        template: string;
        displayProperty: string;

        isActionsColumn: boolean;
        sortKey: string;
        sorted: boolean;
        sortOrder: number;
        sortDescending: boolean;
    }
}