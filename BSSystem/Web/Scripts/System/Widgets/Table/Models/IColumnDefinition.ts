﻿module Directives.TableAjax {

    export interface IColumnDefinition {
        columnHeaderTemplate?: string;
        columnHeaderTemplateUrl?: string;
        columnHeaderDisplayName?: string;

        template?: string;
        templateUrl?: string;
        displayProperty?: string;

        isActionsColumn?: boolean;
        sortKey?: string;
        sorted?: boolean;
        sortOrder?: number;
        sortDescending?: boolean;
    }
}