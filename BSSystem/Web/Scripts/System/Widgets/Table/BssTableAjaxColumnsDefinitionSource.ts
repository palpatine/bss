﻿module SolvesIt.Widgets.Table{
    interface IBssTableAjaxColumnsDefinitionSourceAttributes extends ng.IAttributes {
        bssTableAjaxColumnsDefinitionSource: string;
    }

    class BssTableAjaxColumnsDefinitionSourceController {
        public static $inject = ['$scope', '$attrs', '$http'];
        private controller: BssTableController;
        private items: any[];

        constructor(
            private $scope,
            private $attrs: IBssTableAjaxColumnsDefinitionSourceAttributes,
            private $http: ng.IHttpService) {

        }

        public setTableController(
            controller: BssTableController
            ): void {

            this.controller = controller;
            this.$scope.$watch(this.$attrs.bssTableAjaxColumnsDefinitionSource, value => {

                if (this.controller.state['btacds']
                    && this.controller.state['btacds'].columnsSource == value) {
                    return;
                }

                this.controller.state['btacds'] = { columnsSource: value };
                this.$http.get<Directives.TableAjax.ITableConfiguration>(value)
                    .success(data => {
                    this.controller.state.tableConfiguration = data;
                });
            });
        }
    }

    class BssTableAjaxColumnsDefinitionSourceHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ['bssTableAjaxColumnsDefinitionSource', "^bssTable"];
        controller = "bssTableAjaxColumnsDefinitionSourceController";

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var ownController = <BssTableAjaxColumnsDefinitionSourceController>controller[0];
                    var bssTableController = <BssTableController>controller[1];
                    ownController.setTableController(bssTableController);
                }
            };
        }
    }
    SolvesIt.Bss.
    Application.getApplication()
        .directive('bssTableAjaxColumnsDefinitionSource',(new BssTableAjaxColumnsDefinitionSourceHandlerDirective()).factory())
        .controller('bssTableAjaxColumnsDefinitionSourceController', BssTableAjaxColumnsDefinitionSourceController);
} 