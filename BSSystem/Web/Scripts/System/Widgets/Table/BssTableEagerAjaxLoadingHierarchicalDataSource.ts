﻿module SolvesIt.Widgets.Table{
    interface IBssTableEagerAjaxLoadingHierarchicalDataSource extends ng.IAttributes {
        bssTableEagerAjaxLoadingHierarchicalDataSource: string;
    }

    class BssTableEagerAjaxLoadingHierarchicalDataSourceController extends BssBaseTableHierarchicalDataSourceController {
        public static $inject = ['$scope', '$attrs', '$http'];

        constructor(
            $scope,
            private $attrs: IBssTableEagerAjaxLoadingHierarchicalDataSource,
            $http: ng.IHttpService) {
            super($scope);

            this.$scope.$watch(this.$attrs.bssTableEagerAjaxLoadingHierarchicalDataSource, value => {
                if (!value) return;
                if (this.controller.state['btealhd'] && this.controller.state['btealhd'].hierarchicalDataSource == value) {
                    return;
                }

                this.controller.state['btealhd'] = { hierarchicalDataSource: value };

                $http.get(value)
                    .success(data => {
                    this.allItems = new SolvesIt.Widgets.Structures.HierarchicalStructure(<SolvesIt.Widgets.Structures.IHierarchicalElement[]>data).items;
                    _.chain(this.allItems)
                        .where((x: SolvesIt.Widgets.Structures.IHierarchicalElement) => {
                        return x.parentId == null;
                    })
                        .each(x => {
                        x.$$isExpanded = true;
                    });
                    this.loadItems();
                    this.controller.state.isLoading = false;
                });
            });
        }
    }
    class BssTableEagerAjaxLoadingHierarchicalDataSourceHandlerDirective extends BssBaseTableHierarchicalDataSourceHandlerDirective {

        constructor() {
            super("bssTableEagerAjaxLoadingHierarchicalDataSource");
        }
    }

    SolvesIt.Bss.  Application.getApplication()
        .directive('bssTableEagerAjaxLoadingHierarchicalDataSource', (new BssTableEagerAjaxLoadingHierarchicalDataSourceHandlerDirective()).factory())
        .controller('bssTableEagerAjaxLoadingHierarchicalDataSourceController', BssTableEagerAjaxLoadingHierarchicalDataSourceController);
} 