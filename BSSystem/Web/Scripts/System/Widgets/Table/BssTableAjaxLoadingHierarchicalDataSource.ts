﻿module SolvesIt.Widgets.Table {
    interface IBssTableAjaxLoadingHierarchicalDataSource extends ng.IAttributes {
        bssTableAjaxLoadingHierarchicalDataSource: string;
    }

    class BssTableAjaxLoadingHierarchicalDataSourceController extends BssBaseTableHierarchicalDataSourceController {
        public static $inject = ['$scope', '$attrs', '$http', 'oDataQueryParametersGeneratorFactory', 'bssODataQuerySerializer'];

        constructor(
            $scope,
            private $attrs: IBssTableAjaxLoadingHierarchicalDataSource,
            private $http: ng.IHttpService,
            private oDataQueryParametersGeneratorFactory: SolvesIt.Widgets.QueryString.IODataQueryParametersGeneratorFactory,
            private bssODataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer) {
            super($scope);

            this.loadChildren = this.loadChildrenImpl;

            this.$scope.$watch(this.$attrs.bssTableAjaxLoadingHierarchicalDataSource, value => {
                if (!value) return;
                if (this.controller.state['btalhd'] && this.controller.state['btalhd'].hierarchicalDataSource == value) {
                    return;
                }

                this.controller.state['btalhd'] = { hierarchicalDataSource: value };
                var generator = oDataQueryParametersGeneratorFactory.create();
                generator.
                    where()
                    .property("parent")
                    .eq(null);
                var params = bssODataQuerySerializer.serialize(generator.generate());

                $http<SolvesIt.Widgets.Structures.IHierarchicalElement[]>({
                    method: 'get',
                    url: value,
                    params: params
                })
                    .success(data => {
                        this.allItems = data;
                        _.chain(this.allItems)
                            .where((x: SolvesIt.Widgets.Structures.IHierarchicalElement) => {
                                return x.parentId == null;
                            })
                            .each(x => {
                                x.$$isExpanded = true;
                                x.hasChildren = true;
                            });
                        this.loadItems();
                        this.controller.state.isLoading = false;
                    });
            });
        }

        public loadChildrenImpl(item: SolvesIt.Widgets.Structures.IHierarchicalElement): ng.IPromise<SolvesIt.Widgets.Structures.IHierarchicalElement[]> {
            var generator = this.oDataQueryParametersGeneratorFactory.create();
            generator.
                where()
                .property("parent.id")
                .eq(item.id);
            var params = this.bssODataQuerySerializer.serialize(generator.generate());

            return this.$http<SolvesIt.Widgets.Structures.IHierarchicalElement[]>({
                method: 'get',
                url: this.controller.state['btalhd'].hierarchicalDataSource,
                params: params
            })
                .then(value => {
                    _.each(value.data, v => { v.hasChildren = true; })
                    return value.data;
                });
        }
    }

    class BssTableAjaxLoadingHierarchicalDataSourceHandlerDirective extends BssBaseTableHierarchicalDataSourceHandlerDirective {

        constructor() {
            super("bssTableAjaxLoadingHierarchicalDataSource");
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssTableAjaxLoadingHierarchicalDataSource', (new BssTableAjaxLoadingHierarchicalDataSourceHandlerDirective()).factory())
        .controller('bssTableAjaxLoadingHierarchicalDataSourceController', BssTableAjaxLoadingHierarchicalDataSourceController);
} 