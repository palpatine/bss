﻿module SolvesIt.Widgets.Table{
    interface IBssTableAjaxPagingDataSourceAttributes extends ng.IAttributes {
        bssTableAjaxPagingDataSource: string;
        paginationBtnGroupClasses: string;
        pageSizes: string;
    }



    class BssTableAjaxPagingDataSourceController {
        public static $inject = ['$scope', '$attrs', 'adDebounce', 'adLoadPage', '$parse'];
        private lastRequestToken: number;
        private controller: BssTableController;
        private url: string;
        public paginationBtnGroupClasses: string;

        constructor(
            private $scope,
            private $attrs: IBssTableAjaxPagingDataSourceAttributes,
            private adDebounce,
            private adLoadPage,
            private $parse) {

        }

        public setTableController(controller: BssTableController) {
            this.controller = controller;
            this.controller.state.items.loader = this;

            var isFirstCall = true;
            this.$scope.$watch(() => {
                return this.controller.state.orderProperties;
            },() => {
                    if (!isFirstCall)
                        this.loadItems();
                    isFirstCall = false;
                }, true);

            this.controller.state.items.paging.pageSizes = this.controller.state.items.paging.pageSizes || this.$parse(this.$attrs.pageSizes)(this.$scope) || [10, 25, 50];
            this.controller.state.items.paging.pageSize = this.controller.state.items.paging.pageSize || this.controller.state.items.paging.pageSizes[0];
            this.paginationBtnGroupClasses = this.$attrs.paginationBtnGroupClasses || 'btn-group btn-group-sm';

            controller.footerTemplate = '~/Content/Templates/BssTable/Pager.tpl.html';

            var loadItemsInternalDebounced = this.adDebounce(this.loadItemsInternal);
            this.loadItems = (...args) => {
                controller.state.isLoading = true;
                loadItemsInternalDebounced.apply(this, args);
            };
            this.$scope.$watch(this.$attrs.bssTableAjaxPagingDataSource, value => {
                this.url = value;
                if (this.controller.state["btapds"]
                    && this.controller.state["btapds"].itemsSource == value) {
                    return;
                }
                this.controller.state["btapds"] = { itemsSource: value };
                this.loadItems(0);
            });
        }

        public loadItems: {
            (page?: number): void;
        }

        public loadLastPage() {
            this.loadItems(this.controller.state.items.paging.totalPages);
        }

        public loadNextPage() {
            this.loadItems(this.controller.state.items.paging.currentPage + 1);

        }

        public loadPreviousPage() {
            this.loadItems(this.controller.state.items.paging.currentPage - 1);
        }

        public pageSizeChanged(size: number) {
            this.controller.state.items.paging.pageSize = size;
            this.loadItems();
        }
        private loadItemsInternal(page?: number) {

            if (!this.url) {
                this.controller.state.items.list = null;
                return;
            }

            this.lastRequestToken = Math.random();

            var pageLoader = this.adLoadPage,
                params: Adapt.IPageLoadOptions = {
                    pageNumber: page || 1,
                    pageSize: this.controller.state.items.paging.pageSize,
                    orderProperties: this.controller.getOrderProperties(),
                    sortDirection: false,
                    ajaxConfig: {
                        url: this.url,
                        method: "GET",
                        params: null
                    },
                    token: this.lastRequestToken,
                    //filterProperties: this.controller.generateFilterProperties()
                },
                successHandler = response => {
                    if (response.token === this.lastRequestToken) {
                        this.controller.state.items.list = response.items;
                        this.controller.state.items.paging.totalPages = response.totalPages;
                        this.controller.state.items.paging.currentPage = response.currentPage;
                        //this.localConfig.pagingArray = response.pagingArray;
                        this.controller.state.isLoading = false;
                    }
                },
                errorHandler = () => {
                    this.controller.state.isLoading = false;
                };

            pageLoader(params).then(successHandler, errorHandler);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller('bssTableAjaxPagingDataSourceController', BssTableAjaxPagingDataSourceController);

    class BssTableAjaxPagingDataSourceHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ["bssTableAjaxPagingDataSource", "^bssTable"];
        controller: string = 'bssTableAjaxPagingDataSourceController';
       
        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes: IBssTableAjaxPagingDataSourceAttributes,
                    controller: any[]
                    ): void {
                    var ownController = <BssTableAjaxPagingDataSourceController>controller[0];
                    var bssTableController = <BssTableController>controller[1];
                    ownController.setTableController(bssTableController);
                }
            };
        }
    }

    SolvesIt.Bss. Application.getApplication()
        .directive('bssTableAjaxPagingDataSource',(new BssTableAjaxPagingDataSourceHandlerDirective()).factory());
} 