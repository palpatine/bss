﻿module SolvesIt.Widgets.Table {

    export interface IExtendedHierarchicalElement extends SolvesIt.Widgets.Structures.IHierarchicalElement {
        $$parent: IExtendedHierarchicalElement;

        $$level: number;

        $$isExpanding: boolean;
    }

    export class BssBaseTableHierarchicalDataSourceController {
        public controller: BssTableController;
        public allItems: SolvesIt.Widgets.Structures.IHierarchicalElement[];
        private items: SolvesIt.Widgets.Structures.IHierarchicalElement[];

        constructor(
            public $scope) {
        }

        public setTableController(
            controller: BssTableController
        ): void {

            this.controller = controller;
            this.controller.state.items.loader = this;
            this.onControllerSet();
        }

        public loadItems() {

            this.items = [];

            if (this.allItems) {

                this.items.splice.apply(this.items, [<any>0, 0].concat(this.allItems));

                _.each(this.items, (item: IExtendedHierarchicalElement) => {
                    item.$$parent = {
                        children: this.allItems,
                        $$parent: null,
                        $$level: null,
                        $$isExpanding: false,
                        $$isExpanded: false,
                        id: null,
                        parentId: null,
                        hasChildren: true
                    };
                    item.$$level = 0;
                });

                this.ensureExpansion(this.items.slice());

            }
            this.controller.state.items.list = this.items;
        }

        private ensureExpansion(items: any[]) {
            _.each(items, (item) => {
                if (item.$$isExpanded) {
                    item.$$isExpanded = false;
                    this.toggleDisplay(item);
                    if (item.children)
                        this.ensureExpansion(item.children);
                }
            });
        }

        public loadChildren: (item: SolvesIt.Widgets.Structures.IHierarchicalElement) => ng.IPromise<SolvesIt.Widgets.Structures.IHierarchicalElement[]>;

        public toggleDisplay(item: IExtendedHierarchicalElement) {
            item.$$isExpanded = !item.$$isExpanded;
            var index = this.items.indexOf(item);
            if (item.$$isExpanded) {

                var insert = () => {
                    if (!item.children) return;
                    this.items.splice.apply(this.items, [index + 1, <any>0].concat(item.children));
                    _.each(item.children, (el: any) => {
                        el.$$parent = item;
                        el.$$level = item.$$level + 1;
                    });
                };

                if (!item.children && item.hasChildren && this.loadChildren) {
                    item.$$isExpanding = true;
                    this.loadChildren(item)
                        .then((data) => {
                            item.children = data;
                            item.hasChildren = data && data.length != 0;
                            item.$$isExpanding = false;
                            insert();
                        });
                } else {
                    insert();
                }
            }
            else {
                var parent = item.$$parent;
                var referenceItem = item;
                var nextSibling = undefined;
                while (!nextSibling) {
                    nextSibling = parent.children[parent.children.indexOf(referenceItem) + 1];
                    referenceItem = parent;
                    parent = parent.$$parent;
                    if (!parent) {
                        break;
                    }
                }

                var nextSiblingId = nextSibling ? this.items.indexOf(nextSibling) : this.items.length;
                var itemsToRemove = nextSiblingId - index - 1;
                var removed = this.items.splice.apply(this.items, [index + 1, itemsToRemove]);
                _.each(removed, (el: any) => {
                    el.$$isExpanded = false;
                });
            }
        }

        public onControllerSet() { }
    }

    export class BssBaseTableHierarchicalDataSourceHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require: string[];
        controller: string;

        constructor(directiveName: string) {
            super();
            this.controller = directiveName + "Controller";
            this.require = [directiveName, "^bssTable"];
        }

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {
                    var ownController = <BssBaseTableHierarchicalDataSourceController>controller[0];
                    var bssTableController = <BssTableController>controller[1];
                    ownController.setTableController(bssTableController);
                    bssTableController.tableCellTemplate = '~/Content/Templates/BssTable/Hierarchical/Cell.tpl.html';
                }
            };
        }
    }

} 