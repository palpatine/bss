﻿module SolvesIt.Widgets.Table {
    interface IBssTableArrayDataSourceAttributes extends ng.IAttributes {
        bssTableArrayDataSource: string;
    }

    export interface IBssTableArrayDataSourceController {
        setTableController(controller: BssTableController): void;
        loadItems(): void;
    }

    class BssTableArrayDataSourceController {
        public static $inject = ['$scope', '$attrs'];
        private controller: BssTableController;
        private items: any[];
        constructor(
            private $scope,
            private $attrs) {

            var isFirstCall = true;
            this.$scope.$watch(() => {
                return this.controller.state.orderProperties;
            }, () => {
                if (!isFirstCall)
                    this.loadItems();
                isFirstCall = false;
            }, true);
        }

        public setTableController(
            controller: BssTableController
        ): void {
            this.controller = controller;
            this.controller.state.items.loader = this;
            this.controller.state.isLoading = false;
            this.$scope.$watch(this.$attrs.bssTableArrayDataSource, value => {
                this.items = <any[]>value;
                this.loadItems();
            });
        }

        private getValue(item: any, propertiesPath: string): any {
            var properties = propertiesPath.split(".");
            var value = item;
            _.each(properties, property => {
                if (value) {
                    value = value[property];
                }
            });

            return value;
        }


        public loadItems() {
            var orderProperties = this.controller.getOrderProperties();
            if (this.items) {
                this.items.sort((first, second) => {
                    var result = 0;
                    _.find(orderProperties, (order: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor) => {

                        var firstValue = this.getValue(first, order.propertyName);
                        var secondValue = this.getValue(second, order.propertyName);

                        if (firstValue != secondValue) {
                            if (order.orderDescending) {

                                if ((firstValue === null || firstValue === undefined) && (secondValue === null || secondValue === undefined)) {
                                    result = 0;
                                } else if (firstValue === null || firstValue === undefined) {
                                    result = -1;
                                } else if (secondValue === null || secondValue === undefined) {
                                    result = 1;
                                } else {
                                    result = firstValue < secondValue ? 1 : -1;
                                }
                            } else {
                                if ((firstValue === null || firstValue === undefined) && (secondValue === null || secondValue === undefined)) {
                                    result = 0;
                                } else if (firstValue === null || firstValue === undefined) {
                                    result = -1;
                                }
                                else if (secondValue === null || secondValue === undefined) {
                                    result = 1;
                                } else {
                                    result = firstValue > secondValue ? 1 : -1;
                                }
                            }

                            return true;
                        }

                        return false;
                    });

                    return result;
                });
            }

            this.controller.state.items.list = this.items || null;
        }
    }

    class BssTableArrayDataSourceHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ['bssTableArrayDataSource', "^bssTable"];
        controller = "bssTableArrayDataSourceController";

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {
                    var ownController = <BssTableArrayDataSourceController>controller[0];
                    var bssTableController = <BssTableController>controller[1];
                    ownController.setTableController(bssTableController);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssTableArrayDataSource', (new BssTableArrayDataSourceHandlerDirective()).factory())
        .controller('bssTableArrayDataSourceController', BssTableArrayDataSourceController);
} 