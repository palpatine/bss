﻿module SolvesIt.Widgets.Table{
    class BssTableOrderConfigurationPanelController {

        static $inject = ['$scope'];

        orderProperties: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor[];
        tableConfiguration: Directives.TableAjax.ITableConfiguration;

        properties: { [name: string]: { displayName: string; value: string; }[] };
        availableProperties: { displayName: string; value: string; }[];

        constructor($scope: ng.IScope) {

            $scope.$watch(() => {
                return this.tableConfiguration;
            },() => {
                    this.determineAvailableProperties();
                });

            $scope.$watch(() => {
                return this.orderProperties;
            },() => {
                    this.determineAvailableProperties();
                }, true);
        }

        private determineAvailableProperties() {
            this.properties = {};


            var sortable = _(this.tableConfiguration.columnDefinition).filter(x => {
                return !!x.sortKey;
            });

            this.availableProperties = sortable
                .filter(el => {
                return !_.find(this.orderProperties,(i) => {
                    return i.propertyName == el.sortKey;
                });
            })
                .map(el => {
                return { displayName: el.columnHeaderDisplayName || el.sortKey, value: el.sortKey }
            });

            _.each(sortable,(item) => {
                this.properties[item.sortKey] = sortable
                    .filter(el => {
                    var canUse = true;
                    _.find(this.orderProperties,(i) => {
                        canUse = i.propertyName != el.sortKey;
                        return !canUse;
                    });
                    return el.sortKey == item.sortKey || canUse;
                })
                    .map(el => {
                    return { displayName: el.columnHeaderDisplayName || el.sortKey, value: el.sortKey }
                });
            });
        }

        public toggleSort(item: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor) {
            item.orderDescending = !item.orderDescending;
        }

        public moveUp(item: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor) {
            var index = this.orderProperties.indexOf(item);
            this.orderProperties.splice(index, 1);
            this.orderProperties.splice(index - 1, 0, item);
        }

        public moveDown(item: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor) {
            var index = this.orderProperties.indexOf(item);
            this.orderProperties.splice(index, 1);
            this.orderProperties.splice(index + 1, 0, item);
        }

        public isFirst(item: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor) {
            return this.orderProperties[0] == item;
        }

        public isLast(item: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor) {
            return this.orderProperties[this.orderProperties.length - 1] == item;
        }

        public getValue(item): string {
            if (typeof item == "string") {
                return item;
            } else {
                return item.value;
            }
        }

        public add() {
            this.orderProperties.push({ propertyName: this.availableProperties[0].value, displayName: this.availableProperties[0].displayName, orderDescending: false });
        }

        public canAdd() {
            return true;
        }

        public remove(item: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor, evt) {
            evt.preventDefault();
            evt.stopPropagation();
            var index = this.orderProperties.indexOf(item);
            this.orderProperties.splice(index, 1);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller('bssTableOrderConfigurationPanelController', BssTableOrderConfigurationPanelController);
} 