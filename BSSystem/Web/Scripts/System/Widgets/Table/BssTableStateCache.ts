﻿module SolvesIt.Widgets.Table {

    interface IBssTableStateCacheAttributes extends ng.IAttributes {
        bssTableStateCache: string;
    }

    class BssTableStateCacheDirectiveController {
        public static $inject = ['$scope', '$attrs', 'stateCacheService'];

        constructor(
            private $scope,
            private $attrs,
            private stateCacheService: SolvesIt.Widgets.IStateCacheService) {
        }
        public setTableController(controller: BssTableController) {
            controller.cacheId = this.$scope.$eval(this.$attrs.bssTableStateCache);
            controller.state = this.stateCacheService.getCacheFor(controller.cacheId, () => controller.state);
        }
    }

    class BssTableStateCacheDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict: string = 'A';
        require = ['bssTableStateCache', "^bssTable"];
        controller = "bssTableStateCacheDirectiveController";
        priority = -10;

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var ownController = <BssTableStateCacheDirectiveController>controller[0];
                    var bssTableController = <BssTableController>controller[1];
                    ownController.setTableController(bssTableController);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssTableStateCache', (new BssTableStateCacheDirective()).factory())
        .controller('bssTableStateCacheDirectiveController', BssTableStateCacheDirectiveController);
} 