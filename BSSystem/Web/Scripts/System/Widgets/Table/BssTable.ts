﻿module SolvesIt.Widgets.Table {

    export interface IBssTableControllerAttributes {
        tableClasses: string;
        onSortChanged: string;
    }

    export interface IBssTableScope extends ng.IScope {
        ajaxConfig: Adapt.IAjaxConfig;
    }

    export class BssTableItemsDescriptor {
        list: any[];
        paging: BssTablePagingDescriptor = new BssTablePagingDescriptor();
        loader: {}
    }

    export class BssTablePagingDescriptor {
        pageSize: number;
        totalPages: number;
        currentPage: number;
        pageSizes: number[];
    }

    export class BssTableState {
        public items: BssTableItemsDescriptor = new BssTableItemsDescriptor();
        public orderProperties: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor[];
        [id: string]: any;
        public isLoading: boolean = true;
        public tableConfiguration: Directives.TableAjax.ITableConfiguration;
    }

    export class BssTableController {
        public static $inject = ['$scope', '$attrs', '$http', 'adDebounce', 'adStrapUtils', '$parse'];

        public tableTemplate = "~/Content/Templates/BssTable/Table.tpl.html";
        public footerTemplate = null;
        public tableHeaderTemplate = '~/Content/Templates/BssTable/Header.tpl.html';
        public tableBodyTemplate = '~/Content/Templates/BssTable/Body.tpl.html';
        public tableHeaderCellTemplate = '~/Content/Templates/BssTable/HeaderCell.tpl.html';
        public tableCellTemplate = '~/Content/Templates/BssTable/Cell.tpl.html';
        public configurationPanelTemplate = '~/Content/Templates/BssTable/ConfigurationPanel.tpl.html';

        public tableClasses: string;
        public fallbackSortPropertyName: string = "id";
        public canSort: boolean;
        public canConfigure: boolean; //'@'
        public isConfigurationPanelOpen: boolean = false;

        public state: BssTableState = new BssTableState();
        public cacheId: string;
        public parentScope: ng.IScope;

        constructor(
            public $scope: IBssTableScope,
            public $attrs: IBssTableControllerAttributes,
            private $http: ng.IHttpService,
            private adDebounce: Adapt.IDebounceService,
            private adStrapUtils: Adapt.IStrapUtilsService,
            private $parse: ng.IParseService) {
            this.parentScope = $scope.$parent;
            this.tableClasses = $attrs.tableClasses || 'table table-striped table-hover';

            $scope.$watch(() => {
                return this.state.tableConfiguration;
            }, () => {
                if (!this.state.tableConfiguration) return;
                this.canSort = !!_.find(this.state.tableConfiguration.columnDefinition, item => {
                    return !!item.sortKey;
                });

                var sortedColumns = _.filter(this.state.tableConfiguration.columnDefinition, item => {
                    return item.sorted;
                });

                this.state.orderProperties = _(sortedColumns).sortBy(defintion => {
                    return defintion.sortOrder || 0;
                }).map((defintion) => {
                    return { propertyName: defintion.sortKey, orderDescending: defintion.sortDescending, displayName: defintion.columnHeaderDisplayName || defintion.sortKey };
                });
            });
        }

        public toggleConfigurationPanel() {
            this.isConfigurationPanelOpen = !this.isConfigurationPanelOpen;
        }

        public readProperty(item: any, propertyPath: string): any {
            return this.adStrapUtils.getObjectProperty(item, propertyPath);
        }

        public isSorted(definition: Directives.TableAjax.IColumnDefinition) {
            return !!_.find(this.state.orderProperties, item => { return item.propertyName == definition.sortKey; });
        }

        public isSortOrderDescending(definition: Directives.TableAjax.IColumnDefinition) {
            var order = _.find(this.state.orderProperties, item => { return item.propertyName == definition.sortKey; });
            return order && order.orderDescending;
        }

        public getOrderProperties(): SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor[] {
            if (this.state.orderProperties && this.state.orderProperties.length) {
                return this.state.orderProperties;
            }

            return [{ propertyName: this.fallbackSortPropertyName, orderDescending: false, displayName: "" }];
        }

        public sortByColumn(column: Directives.TableAjax.IColumnDefinition) {

            if (column.sortKey) {
                var state = this.state.orderProperties.slice();

                var current: SolvesIt.Widgets.QueryString.IOrderPropertyDescriptor = _.find(this.state.orderProperties, (item) => {
                    return item.propertyName == column.sortKey;
                });

                if (current === undefined || current === null) {
                    this.state.orderProperties.push({ propertyName: column.sortKey, orderDescending: false, displayName: column.columnHeaderDisplayName || column.sortKey });
                } else {
                    if (current.orderDescending === true) {
                        this.state.orderProperties = _.filter(this.state.orderProperties, item => {
                            return item.propertyName != current.propertyName;
                        });
                    } else {
                        current.orderDescending = true;
                    }
                }

                var onSortChange = this.$scope.$parent.$eval(this.$attrs.onSortChanged);
                if (onSortChange) {
                    onSortChange(state, this.state.orderProperties);
                }
            }
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller('bssTableController', BssTableController);

    class BssTableDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'E';
        scope = {
            ajaxConfig: "=?",
            canConfigure: "@",
            localDataSource: "=?"
        };
        controller: string = 'bssTableController';
        controllerAs: string = 'vm';
        bindToController = true;
        templateUrl: string = '~/Content/Templates/BssTable/Main.tpl.html';
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssTable', (new BssTableDirective()).factory());

    class BssTableInlineEditHandler implements SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
        public static $inject = [];

        constructor() {

        }

        public link(
            scope: ng.IScope,
            instanceElement: ng.IAugmentedJQuery,
            instanceAttributes: ng.IAttributes,
            controller: BssTableController,
            transclude: ng.ITranscludeFunction
        ): void {
            controller.tableTemplate = '~/Content/Templates/BssTable/InlineEditTable.tpl.html';
            controller.tableHeaderTemplate = '~/Content/Templates/BssTable/InlineEditHeader.tpl.html';
            controller.tableBodyTemplate = '~/Content/Templates/BssTable/InlineEditBody.tpl.html';
            controller.tableCellTemplate = '~/Content/Templates/BssTable/InlineEditCellBody.tpl.html';
        }
    }

    class BssTableInlineEditHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.Directive<BssTableInlineEditHandler> {

        restrict: string = 'A';
        require = "^bssTable";
        public getInject(): string[] {
            return BssTableInlineEditHandler.$inject;
        }

        public createHandler(
            //$parse: ng.IParseService,
            //$compile: ng.ICompileService,
            //$templateCache: ng.ITemplateCacheService,
            //$adConfig: Adapt.IConfig,
            //$modal: ng.ui.bootstrap.IModalService,
            //adLoadPage: Adapt.IAjaxPageLoadingService,
            //adDebounce: Adapt.IDebounceService,
            //adStrapUtils: Adapt.IStrapUtilsService,
            $controller: BssTableController,
            $document: ng.IDocumentService,
            $http: ng.IHttpService): BssTableInlineEditHandler {
            return new BssTableInlineEditHandler(
            //    $parse,
            //    $compile,
            //    $templateCache,
            //    $adConfig,
            //    $modal,
            //    adLoadPage,
            //    adDebounce,
            //    adStrapUtils,
            //    $controller
            //    $document,
            //    $http
            );
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssTableInlineEdit', (new BssTableInlineEditHandlerDirective()).factory());
}