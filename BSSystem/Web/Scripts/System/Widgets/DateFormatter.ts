﻿module SolvesIt.Widgets.Formatters {
    SolvesIt.Bss.Application
        .getApplication()
        .filter('dateFormatter',
        () => {
            return (data, format) => {
                if (!data) {
                    return null;
                }
                return moment(data).format(format || "YYYY-MM-DD");
            };
        });
} 
