﻿module SolvesIt.Widgets.Checkbox {
    class BssCheckboxController {
        static $inject = ['$scope'];
        isChecked: boolean;
        isThreeState: boolean;
        checkboxInputElement: HTMLInputElement;
        modelController: ng.INgModelController;

        constructor($scope) {
            $scope.$watch(this.isChecked,
                (newValue, oldValue) => {
                    if (oldValue !== newValue) {
                        this.checkboxInputElement.indeterminate = $scope.isThreeState && newValue == null;
                        this.checkboxInputElement.checked = newValue;
                    }
                });
        }

        setCheckboxInputElement(input: HTMLInputElement) {
            this.checkboxInputElement = input;
            this.checkboxInputElement.indeterminate = this.isThreeState && this.isChecked == null;
            this.checkboxInputElement.checked = this.isChecked;
        }

        private updateState(): void {
            if (this.isChecked === false) {
                this.isChecked = true;
            } else if (this.isChecked === true && this.isThreeState) {
                this.isChecked = null;
            } else {
                this.isChecked = false;
            }
            this.modelController.$setViewValue(this.isChecked);
        }
    }

   SolvesIt.Bss.Application.getApplication()
        .controller('bssCheckboxController', BssCheckboxController);

    class BssCheckboxDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'E';
        scope = {
            isChecked: "=ngModel",
            isThreeState: "=",
            disabled: "="
        };
        controller: string = 'bssCheckboxController';
        controllerAs: string = 'vm';
        bindToController = true;
        templateUrl: string = '~/Content/Templates/BssCheckbox/Main.tpl.html';
        require = ['bssCheckbox', 'ngModel']

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link($scope, element: ng.IAugmentedJQuery, attrs, controller: any): void {
                    controller[0].setCheckboxInputElement(<HTMLInputElement>element[0].childNodes[0]);
                    controller[0].modelController = <ng.INgModelController>controller[1];
                }
            }
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCheckbox', (new BssCheckboxDirective()).factory());
}