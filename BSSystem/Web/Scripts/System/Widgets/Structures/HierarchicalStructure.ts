﻿module SolvesIt.Widgets.Structures{

    export interface IHierarchicalElement {
        id: number;
        parentId: number;
        children: IHierarchicalElement[];
        hasChildren: boolean;
        $$isExpanded?: boolean;
    }

    export class HierarchicalStructure<TElement extends IHierarchicalElement, TPersistanceModel> {

        public itemsById: { [key: number]: TElement };

        public items: TElement[];

        public all: TElement[];

        constructor(data: TElement[]) {
            this.all = data;
            var dataSets: _.Collection<SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel[]> = {};
            this.itemsById = {};

            _.each(data,(item) => {
                var parentId = item.parentId || Number.MIN_VALUE;
                dataSets[parentId] = dataSets[parentId] || [];
                dataSets[parentId].push(item);
                this.itemsById[item.id] = item;
            });

            _.each(dataSets,(item, key) => {
                if (key != Number.MIN_VALUE) {
                    this.itemsById[key].children = item;
                    this.itemsById[key].hasChildren = true;
                }
            });

            this.items = _.filter(data, item => { return item.parentId == null; });
        }

        public getPersistanceData(): TPersistanceModel[] {
            throw "HierarchicalStructure: getPersistanceData not implemented";
        }
    }
} 