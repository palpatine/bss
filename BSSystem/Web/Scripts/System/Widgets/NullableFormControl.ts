﻿module SolvesIt.Widgets.NullableFormControl {
    export interface INullableFormControlAttributes extends ng.IAttributes {
        required: string;
        templateUrl: string;
    }

    export class NullableFormControlController {

        public editorTemplate: string = "~/Content/Templates/NullableFormControl/InputElement.tpl.html"
        public checked: boolean;
        public ngModel: any;
        public isRequired: boolean;
        modelCtrl: ng.INgModelController;

        static $inject = ['$attrs', '$scope'];
        constructor(
            private $attrs: INullableFormControlAttributes,
            private $scope: ng.IScope) {
             
            $attrs.$observe('required', (value: boolean) => {
                if (value) {
                    this.checked = true;
                }

                if (this.isRequired && !value && !this.ngModel) {
                    this.checked = false;
                }

                this.isRequired = value;
            });

            this.editorTemplate = $scope.$eval($attrs.templateUrl) || this.editorTemplate;
        }

        public initialize(modelCtrl: ng.INgModelController) {

            this.modelCtrl = modelCtrl;

            this.checked = this.ngModel !== null && this.ngModel !== undefined;
            this.$scope.$watch('vm.ngModel', (value) => {
                this.checked = value !== null && value !== undefined;
            });
            this.modelCtrl.$viewChangeListeners.push(() => {
                this.checked = this.modelCtrl.$viewValue !== null && this.modelCtrl.$viewValue !== undefined;
            });
        }

        public change() {
            if (this.checked) {
                this.modelCtrl.$setViewValue(this.ngModel || "");
            } else {
                this.modelCtrl.$setViewValue(null);
            }
        }
    }

    SolvesIt.Bss.
        Application.getApplication()
        .controller("nullableFormControlController", NullableFormControlController)
        .directive('bssNullableFormControl',
        [
            () => {
                return {
                    restrict: 'E',
                    require: ['bssNullableFormControl', 'ngModel'],
                    scope: {
                        ngModel: "=",
                    },
                    templateUrl: "~/Content/Templates/NullableFormControl/Main.tpl.html",
                    controller: "nullableFormControlController",
                    controllerAs: "vm",
                    bindToController: true,

                    link: (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: INullableFormControlAttributes, controller) => {
                        var ownController = <NullableFormControlController>controller[0];
                        var modelController = <ng.INgModelController>controller[1];
                        ownController.initialize(modelController);
                    }
                };
            }
        ]);


}