﻿module SolvesIt.Bss {

    interface IPagingDescriptor {
        currentPage: number;
        totalPages: number;
        pageSize: number;
    }

    interface ISelectorAttributes {
        source: string;
        //selected: string;
        pageSize: string;
        localDataSource: string;
        entities: string;
        itemTemplate: string;
        singleSelect: string;
        ngChange: string;
        comparer: string;
        converter: string;
        singleElementName: string;
        multipleElementName: string;
        filterProperty: string;
        baseAjaxCallConfiguration: any;
    }

    class SelectorController {
        static $inject = [
            '$scope',
            '$attrs',
            '$parse',
            '$compile',
            '$templateCache',
            '$position',
            'adDebounce',
            'adLoadPage',
            'adLoadLocalPage',
            'adStrapUtils',
            '$document',
            '$http',
            'queryStringHandler'];

        public selectorAjaxConfig: Adapt.IAjaxConfig;
        public isLoadingData: boolean;
        public filterValue: string = "";
        public items;
        public position: { top: number; left: number } = { top: 0, left: 0 };
        public menuId;
        public ngModel;

        //private scope;
        //private attrs: ISelectorAttributes;
        private paging: IPagingDescriptor;
        //private adDebounce: { (func: Function, timeout: number): Function };
        //private adLoadLocalPage;
        // private adLoadPage;
        private lastRequestToken: number;
        public isOpen: boolean = false;
        private defaultItemTemplate: string = "item.displayName";
        public itemTemplate: string;
        // private adStrapUtils;
        public entities: string;
        private ngModelController: ng.INgModelController;
        hasMore: boolean = true;
        private query: SolvesIt.Widgets.QueryString.IQueryDescriptor;
        private selectorAjaxConfigDefaultParams: { $orderby: string; $filter: SolvesIt.Widgets.QueryString.FilterGroup; }


        constructor(
            private scope: ng.IScope,
            private attrs: ISelectorAttributes,
            private $parse: ng.IParseService,
            private $compile: ng.ICompileService,
            private $templateCache: ng.ITemplateCacheService,
            private $position,
            private adDebounce: { (func: Function, timeout: number): Function },
            private adLoadPage,
            private adLoadLocalPage,
            private adStrapUtils,
            private $document: ng.IDocumentService,
            private $http: ng.IHttpService,
            private queryStringHandler: SolvesIt.Widgets.QueryString.IQueryStringHandler) {

            this.scope.$parent.$watch(attrs.baseAjaxCallConfiguration, (value: { $orderby: string; $filter: SolvesIt.Widgets.QueryString.FilterGroup; }) => {
                this.selectorAjaxConfigDefaultParams = value || {
                    $orderby: 'displayName',
                    $filter: null
                };
                this.filterChanged();
            });

            this.paging = {
                currentPage: 1,
                totalPages: undefined,
                pageSize: Number(attrs.pageSize) || 20
            };

            if (attrs.itemTemplate) {
                this.itemTemplate = scope.$parent.$eval(attrs.itemTemplate) || this.defaultItemTemplate;
            }
            scope.$parent.$watch($parse(attrs.itemTemplate), (value) => {
                this.itemTemplate = value || this.defaultItemTemplate;
            });

            this.selectorAjaxConfig = {
                url: null,
                method: "GET",
                params: this.selectorAjaxConfigDefaultParams
            };

            scope.$parent.$watch<string>(attrs.source, (value) => {
                if (!value) return;
                this.selectorAjaxConfig.url = value;

                this.hasMore = true;
                this.loadPage(1);
            });

            scope.$parent.$watch($parse(attrs.localDataSource), (value) => {
                if (!value) return;

                this.loadPage(1);
            }, true);

            if (attrs.localDataSource && scope.$parent.$eval(attrs.localDataSource)) {
                this.loadPage(1);
            }



            var listContainer = $("#" + this.menuId).find('ul')[0];
            // infinite scroll handler
            var loadFunction = adDebounce(() => {
                // This is for infinite scrolling.
                // When the scroll gets closer to the bottom, load more items.
                if (listContainer.scrollTop + listContainer.offsetHeight >= listContainer.scrollHeight - 300) {
                    this.loadNextPage();
                }
            }, 50);

            var elementScrollHandler = event => {
                if (event.originalEvent && (<any>event.originalEvent).deltaY) {
                    listContainer.scrollTop += (<any>event.originalEvent).deltaY;
                    event.preventDefault();
                    event.stopPropagation();
                }
                loadFunction();
            };

            angular.element(listContainer).bind('scroll', elementScrollHandler);

            scope.$on("$destroy", () => {
                angular.element(listContainer).unbind('scroll', elementScrollHandler);
            });
        }

        public setModelController(
            ngModelController: ng.INgModelController) {
            this.ngModelController = ngModelController;
            ngModelController.$viewChangeListeners.push(() => {
                this.scope.$parent.$eval(this.attrs.ngChange);
            });
        }

        public toggle() {
            this.isOpen = !this.isOpen;
        }

        preventPropagation($event) {
            event.preventDefault();
            event.stopPropagation();
        }

        loadNextPage() {
            if (!this.isLoadingData) {
                this.loadPage(this.paging.currentPage + 1);
            }
        }

        loadPage(page: number): void {
            this.loadPage = <{ (pageNumber: number): void }>this.adDebounce(
                page => {
                    var lastRequestToken = Math.random();
                    this.lastRequestToken = lastRequestToken;
                    this.isLoadingData = true;
                    var pageLoader = this.adLoadPage,
                        params = {
                            pageNumber: page,
                            pageSize: this.paging.pageSize,
                            ajaxConfig: this.selectorAjaxConfig,
                            token: lastRequestToken,
                            localData: undefined,
                            ignoreTotalPages: true
                        },
                        successHandler = response => {
                            if (response.token === lastRequestToken) {
                                if (page === 1) {
                                    this.items = response.items;
                                } else {
                                    this.items = this.items.concat(response.items);
                                }

                                this.hasMore = response.items.length == params.pageSize;
                                this.paging.currentPage = response.currentPage;
                                this.isLoadingData = false;
                            }
                        },
                        errorHandler = () => {
                            this.isLoadingData = false;
                        };

                    if (!params.ajaxConfig.url) {
                        var localData = this.scope.$parent.$eval(this.attrs.localDataSource);
                        if (!localData) {
                            return;
                        }
                        if (this.query) {
                            params.localData = this.queryStringHandler.process(localData, this.query);
                        }
                        else {
                            params.localData = localData;
                        }
                        successHandler(this.adLoadLocalPage(params));
                    } else {
                        pageLoader(params).then(successHandler, errorHandler);
                    }
                }, 10);
            this.loadPage(page);
        }

        public filterChanged(): void {
            this.hasMore = true;
            if (this.filterValue) {
                var generator = this.queryStringHandler.createGenerator();
                generator.where(this.selectorAjaxConfigDefaultParams.$filter)
                    .property(this.scope.$eval(this.attrs.filterProperty) || "displayName")
                    .startswith(this.filterValue);
                this.query = generator.generate();
                this.selectorAjaxConfig.params = this.queryStringHandler.apply(this.selectorAjaxConfigDefaultParams, this.query);
            } else {
                this.selectorAjaxConfig.params = this.selectorAjaxConfigDefaultParams;
                this.query = null;
            }

            this.loadPage(1);
        }

        public itemSelected(selectedItem) {

            var item;
            var found;

            if (this.ngModel && this.ngModel.Value) {
                found = this.attrs.singleSelect === "true"
                    ? (this.areEqual(this.ngModel.Value, selectedItem) ? this.ngModel : null)
                    : _.find(this.ngModel, (el) => {
                        return this.areEqual(el, selectedItem);
                    });
            }

            if (found) {
                item = found;
            }
            else if (this.attrs.converter) {
                item = this.scope.$parent.$eval(this.attrs.converter, { item: selectedItem });
            } else {
                item = selectedItem;
            }

            if (this.attrs.singleSelect === "true") {
                this.ngModelController.$setViewValue(item);
                this.isOpen = false;
            } else {
                var vals = this.ngModel.splice(0);
                this.adStrapUtils.addRemoveItemFromList(item, vals);
                this.ngModelController.$setViewValue(vals);
            }

        }

        public isSelected(item) {
            return this.attrs.singleSelect === "true"
                && this.areEqual(this.ngModel, item)
                || this.attrs.singleSelect === "false" && _.find(this.ngModel, (item1) => { return this.areEqual(item1, item); });
        }

        private areEqual(item1, item2): boolean {
            if (this.attrs.comparer) {
                return this.scope.$parent.$eval(this.attrs.comparer, { item1: item1, item2: item2 });
            }
            else if (this.attrs.converter) {
                return this.scope.$parent.$eval(this.attrs.converter, { item: item1 }) == this.scope.$parent.$eval(this.attrs.converter, { item: item2 });
            }
            else if (typeof item1 == "object" && typeof item2 == "object" && item1 && item2) {
                return item1.id === item2.id;
            }

            return item1 === item2;
        }

        public getElementName() {
            if (this.ngModel.length == 1) {
                return this.attrs.singleElementName || 'element';
            } else {
                return this.attrs.multipleElementName || 'elementów';
            }
        }
    }

    class SelectorDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict = 'E';
        require = ['bssSelector', 'ngModel'];
        scope = {
            ngModel: "=",
            entities: '@'
        };
        controller = "selectorController";
        controllerAs = "vm";
        bindToController = true;
        templateUrl: string = '~/Content/Templates/Selector/Main.tpl.html';

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {
                    var ownController = <SelectorController>controller[0];
                    var modelController = <ng.INgModelController>controller[1];
                    ownController.setModelController(modelController);
                }
            };
        }
    }
    Application.getApplication()
        .directive('bssSelector', (new SelectorDirective()).factory())
        .controller('selectorController', SelectorController);
}
