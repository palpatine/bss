﻿module SolvesIt.Widgets.PopupCalendar {

    class BssPopupCalendarController {

        isOpen: boolean = false;
        selectorTemplate: string = '~/Content/Templates/BssPopupCalendar/Selector.tpl.html'


        toggleOpen($event) {
            $event.preventDefault();
            $event.stopPropagation();

            this.isOpen = !this.isOpen;
        }
    }

    class BssPopupCalendarDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'E';
        scope = {
            canConfigure: "@",
            ngModel: "="
        };
        require = "ngModel";
        controller: string = 'bssPopupCalendarController';
        controllerAs: string = 'vm';
        bindToController = true;
        templateUrl: string = '~/Content/Templates/BssPopupCalendar/Main.tpl.html';
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssPopupCalendar', (new BssPopupCalendarDirective()).factory())
        .controller('bssPopupCalendarController', BssPopupCalendarController);
}