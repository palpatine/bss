﻿module SolvesIt.Widgets.PopupCalendar {
    class BssCalendarInPopupConfigurationHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ["^bssCalendar"];

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var bssCalendarController = <Calendar.BssCalendarController>controller[0];
                    bssCalendarController
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarInPopupConfiguration', (new BssCalendarInPopupConfigurationHandlerDirective()).factory());
} 