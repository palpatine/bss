﻿module SolvesIt.Widgets.Formatters {
  SolvesIt.Bss.Application
        .getApplication()
        .filter('dateTimeFormatter',
        () => {
            return data => {
                if (!data) {
                    return null;
                }

                return moment(data).format("YYYY-MM-DD hh:mm:ss");
            }
        });
} 