﻿module SolvesIt.Widgets{

    export interface IStateCacheService {
        getCacheFor<T>(id: string, factory: () => T): T;
        remove(id: string);
        removeSiblings(id: string);
    }

    class StateCacheService implements IStateCacheService {
        static $inject = [
        ];

        caches: { [id: string]: any } = {};

        constructor() {
        }

        public getCacheFor<T>(id: string, factory: () => T): T {
            if (id == null)
                throw 'StateCacheService: cache entry key cannot start with "$" ' + id;

            var path = id.split('/');
            var current = this.caches;
            var result;
            _.each(path, (element, index) => {
                if (element[0] == '$') {
                    throw 'StateCacheService: intermediate cache entry key cannot start with "$" ' + element + " -> " + id;
                }
                var item = '$' + element;
                if (!current[item] && index == path.length - 1) {
                    current[item] = { $value: factory() };
                    result = current[item].$value;
                } else if (!current[item]) {
                    current[item] = {};
                    current = current[item];
                } else if (index == path.length - 1) {
                    if (!current[item].$value) {
                        current[item].$value = factory();
                    }
                    result = current[item].$value;
                }
                else {
                    current = current[item];
                }
            });

            return result;
        }

        public removeSiblings(id: string) {
            if (id == null) return;

            var path = id.split('/');
            path.splice(path.length - 1, 1);
            var current = this.caches;
            _.each(path, (element, index) => {
                if (element[0] == '$') {
                    throw 'StateCacheService: intermediate cache entry key cannot start with "$" ' + item + " -> " + id;
                }
                var item = '$' + element;

                if (!current[item] && index != path.length - 1) {
                    throw "StateCacheService: intermediate cache entry does not exist " + element + " -> " + id;
                } else if (index == path.length - 1) {
                    _.each(current[item], (el, key) => {
                        current[item][key] = null;
                    });
                }
                else {
                    current = current[item];
                }
            });
        }

        public remove(id: string) {
            if (id == null) return;

            var path = id.split('/');
            var current = this.caches;
            _.each(path, (element, index) => {
                if (element[0] == '$') {
                    throw 'StateCacheService: intermediate cache entry key cannot start with "$" ' + item + " -> " + id;
                }
                var item = '$' + element;

                if (!current[item] && index != path.length - 1) {
                    throw "StateCacheService: intermediate cache entry does not exist " + element + " -> " + id;
                } else if (index == path.length - 1) {
                    current[item] = null;
                }
                else {
                    current = current[item];
                }
            });
        }

    }
    SolvesIt.Bss.
    Application.getApplication()
        .service('stateCacheService', StateCacheService);
}