﻿module SolvesIt.Bss.Widgets{

    interface IRangeDatepickerAttributes {
        name: string;
        minValue: string;
        maxValue: string;
        width: string;
        onChange: string;
    }

    class RangeDatepicker {

        maxOpened: boolean = false;

        minOpened: boolean = false;

        constructor(
            private $parse: ng.IParseService,
            private $compile: ng.ICompileService,
            private $templateCache: ng.ITemplateCacheService,
            private scope: ng.IScope,
            private element: ng.IAugmentedJQuery,
            private attrs: IRangeDatepickerAttributes) {

            var mainTemplate = $templateCache.get<string>('bss/dateRangePicker.tpl.html')
                .replace(/%=name%/g, attrs.name)
                .replace(/%=minValue%/g, attrs.minValue)
                .replace(/%=maxValue%/g, attrs.maxValue)
                .replace(/%=width%/, attrs.width ? attrs.width : '300px');

            element.empty();
            element.append($compile(mainTemplate)(scope));
        }

        openMin($event) {
            $event.preventDefault();
            $event.stopPropagation();

            this.minOpened = true;
        }

        openMax($event) {
            $event.preventDefault();
            $event.stopPropagation();

            this.maxOpened = true;
        }

        onChange() {
            if (this.attrs.onChange)
                this.scope.$eval(this.attrs.onChange);
        }
    }

    Application.getApplication()
        .directive('bssRangeDatepicker',
        [
            '$parse', '$compile', '$templateCache',
            ($parse, $compile, $templateCache) => {
                return {
                    restrict: 'E',
                    transclude: true,
                    link: (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: IRangeDatepickerAttributes) => {
                        scope[attrs.name] = new RangeDatepicker(
                            $parse,
                            $compile,
                            $templateCache,
                            scope,
                            element,
                            attrs);
                    }
                };
            }
        ])
        .run([
            '$templateCache', $templateCache => {
            $templateCache.put('bss/dateRangePicker.tpl.html',
            '<div style="display:table;width:%=width%;"><div style="display:table-cell;width:50%;"><p class="input-group">\
                    <input type="text" class="form-control" datepicker-popup ng-model = "%=minValue%" is-open = "%=name%.minOpened"  \
            datepicker-options = "dateOptions" date-disabled = "disabled(date, mode)" required "%=name%_DateFrom" ng-change="%=name%.onChange()" />\
              <span class="input-group-btn" >\
                <button type="button" class="btn btn-default btn-date" ng-click ="%=name%.openMin($event)" ><i class="glyphicon glyphicon-calendar"></i></button>\
              </span>\
            </p></div>\
            <div style="display:table-cell;width:50%;"><p class="input-group" >\
                    <input type="text" class="form-control" datepicker-popup ng-model = "%=maxValue%" is-open = "%=name%.maxOpened"  \
            datepicker-options = "dateOptions" date-disabled = "disabled(date, mode)" required name="%=name%_DateTo" ng-change="%=name%.onChange()"/>\
              <span class="input-group-btn" >\
                <button type="button" class="btn btn-default btn-date" ng-click ="%=name%.openMax($event)" ><i class="glyphicon glyphicon-calendar"></i></button>\
              </span>\
            </p></div></div>');
            }
        ]);
} 