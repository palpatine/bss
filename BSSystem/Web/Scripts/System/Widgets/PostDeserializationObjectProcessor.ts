﻿module SolvesIt.Widgets {
    export interface IPostDeserializationObjectProcessor {
        processDateValues<T>(data: T[], url: string): void;
        prepareForSerialization(model: any);
    }

    class PostDeserializationObjectProcessor {
        coreDateProperties: string[] = ["dateStart", "dateEnd", "elementDateEnd", "elementDateStart", "roleDateStart", "roleDateEnd", "date", "periodStart", "periodEnd"];
        coreDateTimeProperties: string[] = ["deadline"];
        minimalDate: Date;
        constructor() {
            this.minimalDate = moment("1753-01-01T00:00:00Z").toDate();
        }

        public processDateValues<T>(data: T[], url: string): void {

            _.each(data, element => {
                _.each(this.coreDateProperties, item => {
                    if (element[item]) {
                        element[item] = moment(element[item]).toDate();
                        if (element[item] < this.minimalDate) {
                            console.log(element);
                            console.log(url);
                            throw "Date out of range " + item;
                        }
                    } else if (element[item] === null && item.lastIndexOf("Start") == item.length - ("Start").length) {
                        console.log(element);
                        console.log(url);
                        throw "Date is required " + item;
                    }
                });

                _.each(this.coreDateTimeProperties, item => {
                    if (element[item]) {
                        element[item] = moment(element[item]).toDate();
                    }
                });
            });
        }

        public prepareForSerialization(model: any) {
            _.each(this.coreDateProperties, item => {
                if (model[item]) {
                    model[item] = moment.utc([model[item].getFullYear(), model[item].getMonth(), model[item].getDate()]).toDate();
                    if (model[item] < this.minimalDate) {
                        console.log(model);
                        throw "Date out of range " + item;
                    }
                } else if (model[item] === null && item.lastIndexOf("Start") == item.length - ("Start").length) {
                    console.log(model);
                    throw "Date is required " + item;
                }
            });
        }
    }
    SolvesIt.Bss.
        Application.getApplication()
        .service("PostDeserializationObjectProcessor", PostDeserializationObjectProcessor);
} 