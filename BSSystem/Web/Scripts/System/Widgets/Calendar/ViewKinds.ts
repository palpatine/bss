﻿module SolvesIt.Widgets.Calendar {
    export interface IViewKind {
        name: string;

        viewClass: string;

        rows: ICalendarRow[];

        selection: any;

        getDates(displayCriteria: IDisplayCriteria): IDateRange;

        createGrid(displayCriteria: IDisplayCriteria, size?: number): void;

        getItems(row: ICalendarRow, cell: ICalendarCell, data: ICalendarItem[]): ICalendarItem[];

        mouseMove(cell: ICalendarCell, event: MouseEvent): void;

        mouseExit(cell: ICalendarCell, event: MouseEvent): void;

        mouseEnter(cell: ICalendarCell, event: MouseEvent): void;

        cellMouseDown(cell: ICalendarCell, event: MouseEvent): void;

        cellMouseUp(cell: ICalendarCell, event: MouseEvent): void;

        dragCancelled(): void;
    }

    export class ViewKind implements IViewKind {
        public name: string;
        public viewClass: string;
        public rowHeaderTemplateUrl;
        public cellTemplateUrl;
        public columnHeaderTemplateUrl;
        public backgroundTemplateUrl;
        public columnHeders: any[];
        public rows: ICalendarRow[];
        public rowHeaderClasses: any;
        public crossHeaderClasses: any;

        public selection: any;

        constructor(public controller: BssCalendarController) {
            this.rowHeaderClasses = {
                "bss-calendar-grid-row-header": true
            };
            this.crossHeaderClasses = {
                "bss-calendar-grid-cross-header": true
            }
        }

        public getDates(displayCriteria: IDisplayCriteria): IDateRange {
            throw "ViewKind: abstract method must be overriden getDates";
        }

        public getItems(row: ICalendarRow, cell: ICalendarCell, data: ICalendarItem[]): ICalendarItem[] {
            return _.filter(data, (item) => {
                var itemDate = new Date(item.date.getTime());
                itemDate.setHours(0, 0, 0, 0);
                return itemDate.getTime() == cell.date.getTime();
            });
        }

        public createGrid(displayCriteria: IDisplayCriteria, size?: number): void {
            throw "ViewKind: abstract method must be overriden getDates";
        }        

        public mouseMove(cell: ICalendarCell, event: MouseEvent): void {

        }

        public mouseExit(cell: ICalendarCell, event: MouseEvent): void {

        }

        public mouseEnter(cell: ICalendarCell, event: MouseEvent): void {
        }

        public cellMouseDown(cell: ICalendarCell, event: MouseEvent): void {
        }

        public cellMouseUp(cell: ICalendarCell, event: MouseEvent): void {
        }

        public dragCancelled(): void {
        }
    }
}