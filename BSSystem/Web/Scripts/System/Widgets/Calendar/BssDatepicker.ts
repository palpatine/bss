﻿module SolvesIt.Widgets.Calendar {

    class BssDatepickerController {
        ngModel: Date;// '=',
        public isOpen: boolean = false;
        maxDate: Date; // "=?",
        minDate: Date;// "=?",
        disabled: boolean;// "=?"
        selectorDisplayCriteria: SolvesIt.Widgets.Calendar.IDisplayCriteria = new SolvesIt.Widgets.Calendar.DisplayCriteria();
        calendarModel: SolvesIt.Widgets.Calendar.CalendarModel = new SolvesIt.Widgets.Calendar.CalendarModel();
        popupTemplate = '~/Content/Templates/BssCalendar/Datepicker/Popup.tpl.html';
        public textModel: string;
        static $inject = ['$scope', '$attrs'];
        modelController: ng.INgModelController;

        constructor(
            private $scope: ng.IScope,
            private $attrs) {

            var firstOfMonth = new Date();
            firstOfMonth.setDate(1);
            var firstOfNextMonth = new Date();
            firstOfNextMonth.setDate(1);
            firstOfNextMonth.setMonth(firstOfNextMonth.getMonth() + 1);

            this.selectorDisplayCriteria.period = new Period(firstOfMonth, firstOfNextMonth);

            $scope.$watch(
                () => { return this.ngModel; },
                (value: Date) => {
                    if (value) {
                        this.calendarModel.selection = new Period(value);
                        this.textModel = moment(this.calendarModel.selection.start).format("YYYY-MM-DD");
                    } else {
                        this.calendarModel.selection = null;
                        this.textModel = null;
                    }
                    this.modelController.$setValidity("format", true);
                });

            this.calendarModel.onSelectionChangeEnd =
            () => {
                this.ngModel = this.calendarModel.selection.start;
                this.textModel = moment(this.calendarModel.selection.start).format("YYYY-MM-DD");
                this.modelController.$setValidity("format", true);
                this.$scope.$applyAsync(() => {
                    this.isOpen = false;
                    console.log("closed");
                });
            }
        }

        onTextChange() {
            var value = moment.utc(this.textModel, "YYYY-MM-DD", true);
            if (value.isValid()) {
                this.ngModel = value.toDate();
                this.modelController.$setValidity("format", true);
            }
            else {
                this.modelController.$setValidity("format", false);
            }
        }

        toggleOpen($event) {
            this.isOpen = !this.isOpen;
        }
    }

    class BssDatepickerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict: string = 'EA';
        require = ['bssDatepicker', 'ngModel'];
        scope = {
            ngModel: '=',
            maxDate: "=?",
            minDate: "=?",
            disabled: "=?ngDisabled",
        }
        controllerAs: string = 'vm';
        bindToController = true;
        controller = "bssDatepickerController";
        templateUrl: string = '~/Content/Templates/BssCalendar/Datepicker/Main.tpl.html';

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {
                    var ownController = <BssDatepickerController>controller[0];
                    var modelController = <ng.INgModelController>controller[1];
                    ownController.modelController = modelController;
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssDatepicker', (new BssDatepickerDirective()).factory())
        .controller('bssDatepickerController', BssDatepickerController);
}