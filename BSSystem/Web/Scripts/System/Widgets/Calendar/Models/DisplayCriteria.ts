﻿module SolvesIt.Widgets.Calendar {

    export class DisplayCriteria implements SolvesIt.Widgets.Calendar.IDisplayCriteria {
        public period: SolvesIt.Widgets.Calendar.Period;
        isDirty: boolean;

        isValid(): boolean {
            return (this.period && this.period.start && this.period.end) ? true : false;
        }

        constructor() {
            this.period = new SolvesIt.Widgets.Calendar.Period();
            this.period.setPeriod(new Date(), 1);
        }
    }
} 