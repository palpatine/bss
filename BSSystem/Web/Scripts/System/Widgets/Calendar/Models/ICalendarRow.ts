﻿module SolvesIt.Widgets.Calendar {

    export interface ICalendarRow {
        cells: ICalendarCell[];
        classes?: any;
        styles?: any;
    }
} 