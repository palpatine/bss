﻿module SolvesIt.Widgets.Calendar {

    export interface ICalendarItem {
        date: Date;
    }
} 