﻿module SolvesIt.Widgets.Calendar {

    export interface IPhantomDistributedItem extends IDistributedCalendarItem {
        isPhantom: boolean;
        isLocked: boolean;
        item: IPhantomCalendarItem;
    }
} 