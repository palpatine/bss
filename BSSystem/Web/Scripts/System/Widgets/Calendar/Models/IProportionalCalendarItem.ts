﻿module SolvesIt.Widgets.Calendar {

    export interface IProportionalCalendarItem extends ICalendarItem {
        length: number;
    }
} 