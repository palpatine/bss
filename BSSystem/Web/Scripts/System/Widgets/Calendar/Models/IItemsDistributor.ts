﻿module SolvesIt.Widgets.Calendar {
    export interface IItemsDistributor {
        distributeItems(items: ICalendarItem[]): IDistributedCalendarItem[];

        getGridSize(items: ICalendarItem[]): number;

        itemResizingCancelled(): void;
        itemResizingProgress(event: MouseEvent): void;
        itemResizingBegin(item: IDistributedCalendarItem, event: MouseEvent): void;
        itemResizingEnd(event: MouseEvent): void;
    }
}