﻿module SolvesIt.Widgets.Calendar {
    export interface IDateRange {
        start: Date;
        end: Date;
    }


    export class Period implements IDateRange {
        start: Date;
        end: Date;

        constructor();
        constructor(start: Date, end?: Date);
        constructor(range: IDateRange);
        constructor(...args: any[]) {
            if (args[0] instanceof Date) {
                this.start = new Date(Date.UTC(args[0].getFullYear(), args[0].getMonth(), args[0].getDate()));
                if (!args[1]) {
                    this.end = new Date(this.start.getTime());
                    this.end.setDate(this.end.getDate() + 1);
                }
                else {
                    this.end = new Date(Date.UTC(args[1].getFullYear(), args[1].getMonth(), args[1].getDate()));                    
                }
            } else if (args[0]) {
                this.start = new Date(Date.UTC(args[0].start.getFullYear(), args[0].start.getMonth(), args[0].start.getDate()));
                this.end = new Date(Date.UTC(args[0].end.getFullYear(), args[0].end.getMonth(), args[0].end.getDate()));                
            }
        }

        static fullMonths(includes: Date, count: number): Period {
            var start = new Date(Date.UTC(includes.getFullYear(), includes.getMonth(), 1));
            var end = new Date(start.getTime());
            end.setMonth(end.getMonth() + count);
            return new Period(start, end);
        }

        public setPeriod(start: Date, days: number) {
            this.start = new Date(Date.UTC(start.getFullYear(), start.getMonth(), start.getDate()));            
            this.end = new Date(this.start.getTime());           
            this.end.setDate(this.end.getDate() + days);
        }

        public expandToWeeks() {
            if (this.start.getDay() != Globals.firstDayOfWeek) {
                this.start.setDate(this.start.getDate() - this.getPerviousFirstDayOfWeekOffset(this.start));
            }
            if (this.end.getDay() != Globals.firstDayOfWeek) {
                this.end.setDate(this.end.getDate() + this.getNextFirstDayOfWeekOffset(this.end));
            }
        }

        public expandToMonth() {
            this.start.setDate(1);
            if (this.end.getDay() != 1) {
                this.end.setMonth(this.end.getMonth() + 1);
                this.end.setDate(1);
            }
        }

        public getDaysCount(): number {
            return moment(this.end).diff(moment(this.start), 'days');
        }

        private getPerviousFirstDayOfWeekOffset(date: Date) {
            if (date.getDay() < Globals.firstDayOfWeek) {
                return date.getDay() - Globals.firstDayOfWeek + 7;

            } else {
                return date.getDay() - Globals.firstDayOfWeek;
            }
        }

        private getNextFirstDayOfWeekOffset(date: Date) {
            if (date.getDay() < Globals.firstDayOfWeek) {
                return Globals.firstDayOfWeek - date.getDay();

            } else {
                return 7 - date.getDay() + Globals.firstDayOfWeek;
            }
        }
    }
} 