﻿module SolvesIt.Widgets.Calendar {
    export interface ICalendarModel {
        selection: IDateRange;
        isDirty: boolean;
    }
}