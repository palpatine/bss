﻿module SolvesIt.Widgets.Calendar {

    export interface IDisplayCriteria {
        period: IDateRange;
        viewKindsConfiguration?: { [viewKindName: string]: any };
        pixelsPerMinute?: number;
        isDirty: boolean;
        isValid: () => boolean;
    }
}