﻿module SolvesIt.Widgets.Calendar {
    export interface IDistributedCell extends ICalendarCell {
        items: IDistributedCalendarItem[];
    }
} 