﻿module SolvesIt.Widgets.Calendar {

    export interface ICalendarCell {
        classes?: any;
        styles?: any;
        date: Date;
    }
} 