﻿module SolvesIt.Widgets.Calendar {

    export class CalendarModel implements SolvesIt.Widgets.Calendar.ICalendarModel {

        selection: SolvesIt.Widgets.Calendar.IDateRange;

        private isModelDirty: boolean;

        public get isDirty(): boolean {
            return this.isModelDirty;
        }

        public set isDirty(value: boolean) {
            if (this.isModelDirty == value) return;
            this.isModelDirty = value;
            if (!value && this.onSelectionChangeEnd) {
                this.onSelectionChangeEnd();
            } else if (this.onSelectionChangeBegin) {
                this.onSelectionChangeBegin();
            }
        }

        onSelectionChangeBegin: () => void = null;
        onSelectionChangeEnd: () => void = null;
    }
} 