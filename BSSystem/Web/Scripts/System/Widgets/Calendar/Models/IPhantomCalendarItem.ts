﻿module SolvesIt.Widgets.Calendar {

    export interface IPhantomCalendarItem extends IProportionalCalendarItem {
        remove: () => void;
    }
} 