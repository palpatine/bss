﻿module SolvesIt.Widgets.Calendar {
    export interface IDataProvider {
        loadItems(dateRange: IDateRange, displayCriteria: IDisplayCriteria): ng.IPromise<ICalendarItem[]>;
    }
} 