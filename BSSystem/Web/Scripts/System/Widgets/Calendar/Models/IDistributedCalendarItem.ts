﻿module SolvesIt.Widgets.Calendar {
    export interface IDistributedCalendarItem {
        item: ICalendarItem;
        classes?: any;
        styles?: any;
    }
}