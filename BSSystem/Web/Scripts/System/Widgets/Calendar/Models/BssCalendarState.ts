﻿module SolvesIt.Widgets.Calendar {

    export class BssCalendarState {
        public items: ICalendarItem[];
        public viewKinds: IViewKind[] = [];
        public viewKind: IViewKind;
        public selectedInterval: any;
        public displayCriteria: IDisplayCriteria;
        public currentItems: ICalendarItem[];
        constructor() {
        }
    }
} 