﻿module SolvesIt.Widgets.Calendar {


    class BssCalendarViewKindSelectorHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ["^bssCalendar"];
        priority = 10;

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var bssCalendarController = <BssCalendarController>controller[0];
                    bssCalendarController.state.viewKinds = [new DailyViewKind(bssCalendarController), new WeeklyViewKind(bssCalendarController)];

                    scope.$watch(() => { 
                        return bssCalendarController.state.displayCriteria;
                    }, (value: IDisplayCriteria) => {

                            if (!value.isValid()) {
                                return;
                            }

                            var daysCount = new Period(value.period).getDaysCount();
                            if (daysCount > 7) {
                                bssCalendarController.setView(bssCalendarController.state.viewKinds[1]);
                            }
                            else {
                                bssCalendarController.setView(bssCalendarController.state.viewKinds[0]);
                            }
                        },
                        true);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarViewKindSelector', (new BssCalendarViewKindSelectorHandlerDirective()).factory());
}  