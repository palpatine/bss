﻿module SolvesIt.Widgets.Calendar {

    export interface IProportionalItemResizeEventArgs {
        item: IProportionalCalendarItem;
        isStarting;
        isCommited;
        isCancelled;
    }

    interface IBssCalendarProportionalItemsDistributorAttributes extends ng.IAttributes {
        bssCalendarProportionalItemsDistributor: string;
        onItemResize;
    }

    interface IProportionalDistributedCalendarItem extends IDistributedCalendarItem {
        item: IProportionalCalendarItem;
    }

    class BssCalendarItemsDistributorController implements IItemsDistributor {

        public getItemTemplateUrl(item: ICalendarItem): string {
            throw "BssCalendarItemsDistributorController: getItemTemplateUrl not implemented";
        }

        distributeItems(items: ICalendarItem[]): IDistributedCalendarItem[] {
            throw "BssCalendarItemsDistributorController: distributeItems not implemented";
        }

        getGridSize(items: ICalendarItem[]): number {
            throw "BssCalendarItemsDistributorController: getGridSize not implemented";
        }

        itemResizingCancelled(): void { }
        itemResizingProgress(event: MouseEvent): void { }
        itemResizingBegin(item: IDistributedCalendarItem, event: MouseEvent): void { }
        itemResizingEnd(event: MouseEvent): void { }
    }

    export interface IBssCalendarProportionalItemsConfiguration {
        itemTemplateUrl?: string;
        itemTemplates?: IBssCalendarProportionalItemsTemplatesConfiguration[];
        hoursCount: number;
        minimalDisplayableDuration?: number;
    }

    export interface IBssCalendarProportionalItemsTemplatesConfiguration {
        length: number;
        url: string;
    }

    class BssCalendarProportionalItemsDistributorController implements IItemsDistributor {
        controller: BssCalendarController;

        static $inject = [
            '$scope',
            '$attrs'
        ];

        resizingItem: IProportionalDistributedCalendarItem;
        oryginalLength: number;
        resizingStart: MouseEvent;

        constructor(
            private $scope: ng.IScope,
            private $attrs: IBssCalendarProportionalItemsDistributorAttributes) {
        }

        public setCalendarController(controller: BssCalendarController) {
            this.controller = controller;
            this.controller.setItemsDistributor(this);
        }

        getItemTemplateUrl(item: IProportionalDistributedCalendarItem): string {
            var viewKind = this.controller.state.viewKind.name;
            var configuration: IBssCalendarProportionalItemsConfiguration = this.controller.state.displayCriteria.viewKindsConfiguration[viewKind];
            if (configuration.itemTemplates) {
                var itemTemplateConfiguation = _.chain(<IBssCalendarProportionalItemsTemplatesConfiguration[]>configuration.itemTemplates)
                    .sortBy(x => { return -x.length; })
                    .find((config) => {
                        return config.length < item.item.length;
                    })
                    .value();

                return itemTemplateConfiguation.url;
            }

            return configuration.itemTemplateUrl;
        }

        public getConfuguration(): IBssCalendarProportionalItemsConfiguration {
            var viewKind = this.controller.state.viewKind.name;
            var configuration = this.controller.state.displayCriteria.viewKindsConfiguration[viewKind];
            configuration.minimalDisplayableDuration = configuration.minimalDisplayableDuration || 0;
            return configuration;
        }

        public getGridSize(items: IProportionalCalendarItem[]): number {
            var configuration = this.getConfuguration();
            if (!configuration
                || !configuration.hoursCount) {
                return null;
            }

            if (items.length) {
                var groups: { [id: number]: number} = {};

                _.each(items, (item) => {
                    groups[item.date.getTime()] = groups[item.date.getTime()] ? groups[item.date.getTime()] + item.length : item.length;
                });
                return Math.ceil(<number>_.reduce(groups, (value, group) => {
                    return group > value ? group : value;
                }, configuration.hoursCount * 60) / 60);
            }

            return configuration.hoursCount;
        }

        public distributeItems(items: IProportionalCalendarItem[]): IProportionalDistributedCalendarItem[] {
            var result: IProportionalDistributedCalendarItem[] = [];
            var configuration = this.getConfuguration();


            if (configuration.hoursCount) {
                _.each(items, (item) => {
                    var distributedItem: IProportionalDistributedCalendarItem;
                    if (item.length < configuration.minimalDisplayableDuration) {
                        distributedItem = {
                            item: item,
                            styles: {
                                'width': "calc((100% - 15px) / " + (configuration.minimalDisplayableDuration / item.length) + ")",
                                'height': 29,
                            }
                        };
                    } else {
                        distributedItem = {
                            item: item,
                            styles: {
                                'margin-right': 15 + "px",
                                width: "calc(100% - 15px)",
                                'height': (this.controller.state.displayCriteria.pixelsPerMinute * item.length) - 1,
                            }
                        };
                    }
                    result.push(distributedItem);
                });
            } else {
                _.each(items, (item) => {
                    var distributedItem: IProportionalDistributedCalendarItem = {
                        item: item,
                        styles: {
                            'margin-top': 2 + 'px',
                            'margin-left': 5 + "px",
                            'margin-right': 5 + "px"
                        }
                    };

                    result.push(distributedItem);
                });
            }

            return result;
        }

        public itemResizingCancelled(): void {
            this.resizingItem.item.length = this.oryginalLength;
            this.controller.distributeItems();

            if (this.$attrs.onItemResize) {
                var args: IProportionalItemResizeEventArgs = {
                    isCancelled: true,
                    isCommited: false,
                    isStarting: false,
                    item: this.resizingItem.item
                };
                this.$scope.$eval(this.$attrs.onItemResize, { args: args });
            }

            this.resizingItem = null;
            this.resizingStart = null;
            this.oryginalLength = null;
        }

        public itemResizingProgress(event: MouseEvent): void {
            var delta = (event.pageY - this.resizingStart.pageY) * this.controller.state.displayCriteria.pixelsPerMinute;

            var minimalDuration = this.getConfuguration().minimalDisplayableDuration;

            if (this.oryginalLength + delta <= (minimalDuration || 0)) {
                var cell = angular.element(event.target).parents(".bss-calendar-grid-cell");
                var width = cell.width();
                var offset = cell.offset();
                if (!offset) return;
                var length = (event.pageX - offset.left) * 1.0 / width * minimalDuration;
                this.resizingItem.item.length = Math.max(length, 5);
            }
            else {
                this.resizingItem.item.length = Math.max(this.oryginalLength + delta, 5);
            }
            this.controller.distributeItems();
        }

        public itemResizingBegin(item: IProportionalDistributedCalendarItem, event: MouseEvent): void {
            this.resizingItem = item;
            this.resizingStart = event;
            this.oryginalLength = item.item.length;

            if (this.$attrs.onItemResize) {
                var args: IProportionalItemResizeEventArgs = {
                    isCancelled: false,
                    isCommited: false,
                    isStarting: true,
                    item: this.resizingItem.item
                };
                this.$scope.$eval(this.$attrs.onItemResize, { args: args });
            }
        }

        public itemResizingEnd(event: MouseEvent): void {
            this.itemResizingProgress(event);

            if (this.$attrs.onItemResize) {
                var args: IProportionalItemResizeEventArgs = {
                    isCancelled: false,
                    isCommited: true,
                    isStarting: false,
                    item: this.resizingItem.item
                };
                this.$scope.$eval(this.$attrs.onItemResize, { args: args });
            }
        }
    }


    class BssCalendarProportionalItemsDistributorDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ['bssCalendarProportionalItemsDistributor', "^bssCalendar"];
        controller = "bssCalendarProportionalItemsDistributorController";
        priority = 10;

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                ): void {
                    var ownController = <BssCalendarProportionalItemsDistributorController>controller[0];
                    var bssCalendarController = <BssCalendarController>controller[1];
                    ownController.setCalendarController(bssCalendarController);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarProportionalItemsDistributor', (new BssCalendarProportionalItemsDistributorDirective()).factory())
        .controller('bssCalendarProportionalItemsDistributorController', BssCalendarProportionalItemsDistributorController);
}