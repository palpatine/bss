﻿module SolvesIt.Widgets.Calendar {

   export interface IBssCalendarAttributes {
        bssCalendar: string;
    }

    export class BssCalendarController {
        public static $inject = ['$scope', '$attrs', '$element', '$http', 'adDebounce', 'adStrapUtils', '$parse'];

        configuration: BssCalendarConfiguration = new BssCalendarConfiguration();
        public cacheId;
        public configurationPanelTemplate = '~/Content/Templates/BssCalendar/ConfigurationPanel.tpl.html';
        public bodyTemplate = '~/Content/Templates/BssCalendar/Body.tpl.html';
        public footerTemplate = '~/Content/Templates/BssCalendar/Footer.tpl.html';
        public monthSelectorTemplate = '~/Content/Templates/BssCalendar/MonthSelector.tpl.html';
        public itemWrapperTemplate = '~/Content/Templates/BssCalendar/ItemWrapper.tpl.html';
        public viewSelectorTemplate = '~/Content/Templates/BssCalendar/ViewSelector.tpl.html';

        public isConfigurationPanelOpen: boolean = false;
        public canConfigure;//@
        public ngModel: ICalendarModel;//=
        public state = new BssCalendarState();
        public parentScope;

        private dataProvider: IDataProvider;
        private itemsDistributor: IItemsDistributor;
        private gridProcessors: { (): void }[] = [];
        private selectionProcessors: { (): void }[] = [];

        public updateData: () => void;

        private isDragging = false;
        private isResizingItem = false;

        private dragStart: Date;

        constructor(
            public $scope: ng.IScope,
            public $attrs: IBssCalendarAttributes,
            private $element: ng.IAugmentedJQuery,
            private $http: ng.IHttpService,
            private adDebounce: Adapt.IDebounceService,
            private adStrapUtils: Adapt.IStrapUtilsService,
            private $parse: ng.IParseService) {
            this.parentScope = $scope.$parent;
            this.updateData = adDebounce(this.updateDataImpl);
            this.processSelection = adDebounce(this.processSelectionImpl, 5);

            if ($attrs.bssCalendar) {
                $scope.$parent.$watch<IDisplayCriteria>($attrs.bssCalendar, (value) => {
                    this.state.displayCriteria = value;
                });
            }

            $scope.$watch(
                () => this.state.displayCriteria,
                (value: IDisplayCriteria) => {
                    if (!value.isValid()) {
                        return;
                    }

                    this.state.viewKind.createGrid(value);
                    this.updateData();
                },
                true);
        }

        public toggleConfigurationPanel(): void {
            this.isConfigurationPanelOpen = !this.isConfigurationPanelOpen;
        }
        public setDataProvider(dataProvider: IDataProvider): void {
            this.dataProvider = dataProvider;
            this.updateData();
        }

        public setItemsDistributor(itemsDistributor: IItemsDistributor): void {
            this.itemsDistributor = itemsDistributor;
            this.updateData();
        }

        public addGridProcessor(gridProcessor: () => void): void {
            this.gridProcessors.push(gridProcessor);
        }

        public addSelectionProcessor(selectionProcessor: () => void) {
            this.selectionProcessors.push(selectionProcessor);
        }

        public setView(viewKind: IViewKind): void {
            this.state.viewKind = viewKind;
            if (this.state.displayCriteria) {
                this.state.viewKind.createGrid(this.state.displayCriteria);
                this.updateData();
            }
        }

        public cancelDrag() {
            var isDragging = this.isDragging;
            this.isDragging = false;
            if (this.ngModel && isDragging) {
                this.ngModel.isDirty = false;
                this.state.viewKind.dragCancelled();
                this.ngModel.selection = this.state.viewKind.selection;
                this.processSelection();
            }
            var isResizingItem = this.isResizingItem;
            this.isResizingItem = false;
            if (isResizingItem) {
                this.itemsDistributor.itemResizingCancelled();
            }
        }

        public mouseMove(cell: ICalendarCell, event: MouseEvent): void {
            if (this.ngModel && this.isDragging) {

                this.state.viewKind.mouseMove(cell, event);
                this.ngModel.selection = this.state.viewKind.selection;
                this.processSelection();
            }
            else if (this.isResizingItem) {
                this.itemsDistributor.itemResizingProgress(event);
            }
        }

        public mouseExit(cell: ICalendarCell, event: MouseEvent): void {
            if (!this.ngModel || !this.isDragging) {
                return;
            }

            this.state.viewKind.mouseExit(cell, event);
            this.ngModel.selection = this.state.viewKind.selection;
            this.processSelection();
        }

        public mouseEnter(cell: ICalendarCell, event: MouseEvent): void {

            if (!this.ngModel || !this.isDragging) {
                return;
            }

            this.state.viewKind.mouseEnter(cell, event);
            this.ngModel.selection = this.state.viewKind.selection;
            this.processSelection();
        }

        public durationAdjusterMouseDown(distributedItem: IDistributedCalendarItem, event: MouseEvent) {
            event.stopPropagation();
            event.preventDefault();
            this.isResizingItem = true;
            this.itemsDistributor.itemResizingBegin(distributedItem, event);
          
        }

        public cellMouseDown(cell: ICalendarCell, event: MouseEvent): void {
            if (!this.ngModel) {
                return;
            }

            if (this.isDragging) {
                this.cancelDrag();
                return;
            }

            this.isDragging = true;
            this.ngModel.isDirty = true;
            this.state.viewKind.cellMouseDown(cell, event);
            this.ngModel.selection = this.state.viewKind.selection;
            this.processSelection();
        }

        public cellMouseUp(cell: ICalendarCell, event: MouseEvent): void {

            if (this.isDragging) {
                this.isDragging = false;

                if (!this.ngModel) {
                    return;
                }

                this.ngModel.isDirty = false;
                this.state.viewKind.cellMouseUp(cell, event);
                this.ngModel.selection = this.state.viewKind.selection;
                this.processSelection();
            }
            else if (this.isResizingItem) {
                this.itemsDistributor.itemResizingEnd(event);
                this.isResizingItem = false;
            }
        }

        private processSelection: () => void;
        private processSelectionImpl() {
            _.each(this.selectionProcessors, item => { item(); });
        }

        public merge(first: any, second: any): any {

            if (!first) {
                return second;
            }

            if (!second) {
                return first;
            }

            return angular.extend(first, second);
        }

        public isToday(date: Date): boolean {
            var today = new Date();
            today.setHours(0, 0, 0, 0);

            var toCompare = new Date(date.getTime());
            toCompare.setHours(0, 0, 0, 0);

            return today.getTime() == toCompare.getTime();
        }



        public updateDataImpl(): void {
            if (!this.state.displayCriteria.isValid() || this.state.displayCriteria.isDirty) {
                return;
            }

            if (this.dataProvider) {
                this.dataProvider
                    .loadItems(this.state.viewKind.getDates(this.state.displayCriteria), this.state.displayCriteria)
                    .then((items) => {
                    this.state.currentItems = items;
                    this.distributeItems();
                });
            }
        }

        public distributeItems() {
            var size = this.itemsDistributor.getGridSize(this.state.currentItems);
            this.createGrid(this.state.displayCriteria, size);

            _.each(this.state.viewKind.rows, (row) => {
                _.each(row.cells, (cell: IDistributedCell) => {
                    cell.items = this.itemsDistributor.distributeItems(this.state.viewKind.getItems(row, cell, this.state.currentItems));
                });
            });
        }

        private createGrid(displayCriteria: IDisplayCriteria, size: number) {
            this.state.viewKind.createGrid(displayCriteria, size);
            _.each(this.gridProcessors, (item) => {
                item();
            });
        }
    }

    class BssCalendarDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'AE';
        scope = {
            canConfigure: "@",
            ngModel: "="
        };
        require = ["ngModel"];
        controller: string = 'bssCalendarController';
        controllerAs: string = 'vm';
        bindToController = true;
        templateUrl: string = '~/Content/Templates/BssCalendar/Main.tpl.html';
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendar', (new BssCalendarDirective()).factory())
        .controller('bssCalendarController', BssCalendarController);

}