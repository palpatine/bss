﻿module SolvesIt.Widgets.Calendar {

    export class WeeklyViewKind extends ViewKind {
        public name: string = "weekly";
        public viewClass: string = "weekly-view";
        public cellTemplateUrl = '~/Content/Templates/BssCalendar/Weekly/Cell.tpl.html';
        public columnHeaderTemplateUrl = '~/CalendarTemplates/WeeklyViewAbbreviatedColumnHeader';
        public selection: Period;
        public dragStart: Date;

        constructor(controller: BssCalendarController) {
            super(controller);

            this.columnHeders = [];
            for (var k = 0; k < 7; k++) {
                this.columnHeders.push({
                    day: (k + this.controller.configuration.firstDayOfWeek) % 7
                });
            }
        }

        getDates(displayCriteria: IDisplayCriteria): Period {

            var period = new Period(displayCriteria.period);
            period.expandToWeeks();
            return period;
        }

        createGrid(displayCriteria: IDisplayCriteria): void {

            var dates = this.getDates(displayCriteria);
            this.rows = [];
            var daysCount = dates.getDaysCount();
            var weeksCount = Math.ceil(daysCount / 7);
            var height = this.controller.state.displayCriteria.pixelsPerMinute * 60 * 4;

            var firstMonthInPeriod = dates.start.getMonth();

            for (var i = 0; i < weeksCount; i++) {
                var row = {
                    styles: { height: height },
                    cells: [],
                    firstMonthCell: null
                };
                this.rows.push(row);

                for (var j = 0; j < 7; j++) {
                    var date = new Date(dates.start.getTime());
                    date.setDate(date.getDate() + i * 7 + j);

                    var isOutOfPeriod = date < displayCriteria.period.start || date >= displayCriteria.period.end;
                    var classes = {
                        'bss-calendar-cell-out-of-period': isOutOfPeriod,
                        'bss-calendar-cell-first-week-in-month': date.getDate() <= 7,
                        'bss-calendar-cell-first-day-in-month': date.getDate() == 1,
                        'bss-calendar-cell-first-day-in-week': date.getDay() == this.controller.configuration.firstDayOfWeek,
                    };
                    var monthId = date.getMonth() - firstMonthInPeriod;
                    classes['bss-calendar-cell-month-' + monthId] = !isOutOfPeriod;

                    row.cells.push({
                        isOutOfPeriod: isOutOfPeriod,
                        date: date,
                        classes: classes
                    });
                }
                var firstMonthCell = _.find(row.cells, (cell) => {
                    return cell.date.getDate() == 1;
                });;
                row.firstMonthCell = firstMonthCell;
            }
        }

        public dragCancelled(): void {
        }

        public mouseMove(cell: ICalendarCell, event: MouseEvent): void {

        }

        public mouseExit(cell: ICalendarCell, event: MouseEvent): void {

        }

        public mouseEnter(cell: ICalendarCell, event: MouseEvent): void {

            if (cell.date < this.dragStart) {
                this.selection.end = new Date(this.dragStart.getTime());
                this.selection.start = new Date(cell.date.getTime());

            } else {
                this.selection.start = new Date(this.dragStart.getTime());
                this.selection.end = new Date(cell.date.getTime());
            }

            this.selection.end.setDate(this.selection.end.getDate() + 1);
        }

        public cellMouseDown(cell: ICalendarCell, event: MouseEvent): void {
            this.dragStart = new Date(cell.date.getTime());
            this.selection = new Period();
            this.selection.start = new Date(cell.date.getTime());
            this.selection.end = new Date(cell.date.getTime());
            this.selection.end.setDate(this.selection.end.getDate() + 1);
        }

        public cellMouseUp(cell: ICalendarCell, event: MouseEvent): void {
            if (cell.date < this.dragStart) {
                this.selection.end = this.dragStart;
                this.selection.start = new Date(cell.date.getTime());
            } else {
                this.selection.start = this.dragStart;
                this.selection.end = new Date(cell.date.getTime());
            }

            this.selection.end.setDate(this.selection.end.getDate() + 1);
        }

        public isSelected(date: Date): boolean {
            return this.selection && this.selection.start <= date && date < this.selection.end;
        }
    }
}