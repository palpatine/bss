﻿module SolvesIt.Widgets.Calendar {
    export class DailyViewKind extends ViewKind {
        public name: string = "daily";
        public viewClass: string = "daily-view";
        public rowHeaderTemplateUrl = '~/Content/Templates/BssCalendar/Daily/RowHeader.tpl.html';
        public cellTemplateUrl = '~/Content/Templates/BssCalendar/Daily/Cell.tpl.html';
        public columnHeaderTemplateUrl = '~/Content/Templates/BssCalendar/Daily/ColumnHeader.tpl.html';
        public backgroundTemplateUrl = '~/Content/Templates/BssCalendar/Daily/Background.tpl.html';
        public backgroundColumnHeaderTemplateUrl = '~/Content/Templates/BssCalendar/Daily/BackgroundColumnHeader.tpl.html';
        public backgroundRowHeaderTemplateUrl = '~/Content/Templates/BssCalendar/Daily/BackgroundRowHeader.tpl.html';
        public backgroundCellTemplateUrl = '~/Content/Templates/BssCalendar/Daily/BackgroundCell.tpl.html';
        public backgroundColumnHeders: any[];
        public backgroundRows: any[];
        public units: any[];
        public selection: Period;

        private dragStart: Date;
        private dragStartEvent: MouseEvent;


        constructor(controller: BssCalendarController) {
            super(controller);
        }

        getDates(displayCriteria: IDisplayCriteria): Period {
            return new Period(displayCriteria.period);
        }

        createGrid(displayCriteria: IDisplayCriteria, size?: number): void {

            size = size || this.controller.state.displayCriteria.viewKindsConfiguration[this.name].hoursCount;


            this.units = [];
            for (var a = 0; a < size; a++) {

                for (var j = 0; j < 2; j++) {
                    this.units.push({
                        id: a * 2 + j,
                        styles: { height: 30 * this.controller.state.displayCriteria.pixelsPerMinute }
                    });
                }
            }

            var dates = this.getDates(displayCriteria);

            this.columnHeders = [];
            this.rows = [
                {
                    cells: [],
                    styles: { height: size * 60 * this.controller.state.displayCriteria.pixelsPerMinute }
                }
            ];
            var daysCount = dates.getDaysCount();

            for (var i = 0; i < daysCount; i++) {
                var date = new Date(dates.start.getTime());
                date.setDate(date.getDate() + i);
                this.rows[0].cells.push({
                    date: date,
                });
                this.columnHeders.push({
                    date: date
                });

                this.backgroundColumnHeders = [];
                this.backgroundRows = [
                    {
                        cells: []
                    }
                ];
                this.backgroundColumnHeders.push({});
                this.backgroundRows[0].cells.push({});
            }
        }

        public dragCancelled(): void {
        }

        public mouseMove(cell: ICalendarCell, event: MouseEvent): void {
            this.update(event);
        }

        public mouseExit(cell: ICalendarCell, event: MouseEvent): void {
        }

        public mouseEnter(cell: ICalendarCell, event: MouseEvent): void {
        }

        public cellMouseDown(cell: ICalendarCell, event: MouseEvent): void {
            var selectionPrecision = this.controller.state.displayCriteria.viewKindsConfiguration[this.name].selectionPrecision || 5;
            var firstHour = this.controller.state.displayCriteria.viewKindsConfiguration[this.name].firstHour || 0;
            var eventOffsetY = event.pageY - angular.element(event.target).parents(".bss-calendar-grid-cell").offset().top;
            var minutes = firstHour * 60
                + Math.round((eventOffsetY * this.controller.state.displayCriteria.pixelsPerMinute) / selectionPrecision) * selectionPrecision;

            this.dragStart = new Date(cell.date.getTime());
            this.dragStart.setHours(0, minutes);
            this.dragStartEvent = event;
        }

        public cellMouseUp(__: ICalendarCell, event: MouseEvent): void {
            if (Math.abs(this.dragStartEvent.pageY - event.pageY) < 3) {
                this.selection = null;
                return;
            }

            this.update(event);
        }

        private update(event: MouseEvent) {
            if (this.selection == null && Math.abs(this.dragStartEvent.pageY - event.pageY) < 3) {
                return;
            }

            var selectionPrecision = this.controller.state.displayCriteria.viewKindsConfiguration[this.name].selectionPrecision || 5;
            var firstHour = this.controller.state.displayCriteria.viewKindsConfiguration[this.name].firstHour || 0;
            var minutes;
            var eventOffsetY = event.pageY - angular.element(event.target).parents(".bss-calendar-grid-cell").offset().top;

            minutes = firstHour * 60
                + Math.round((eventOffsetY * this.controller.state.displayCriteria.pixelsPerMinute) / selectionPrecision) * selectionPrecision;

            this.selection = this.selection || new Period();
            this.selection.start = this.dragStart;
            this.selection.end = new Date(this.dragStart.getTime());
            this.selection.end.setHours(0, minutes);

            if (this.selection.end < this.selection.start) {
                var date = this.selection.end;
                this.selection.end = this.selection.start;
                this.selection.start = date;
            }
        }
    }
}