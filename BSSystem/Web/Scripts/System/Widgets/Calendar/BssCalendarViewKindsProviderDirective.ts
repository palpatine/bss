﻿module SolvesIt.Widgets.Calendar {
    interface IBssCalendarViewKindsProviderAttriubtes extends ng.IAttributes {
        bssCalendarViewKinds: string;
        bssCalendarDefaultViewKind: string;
    }

    class BssCalendarViewKindsProviderDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict: string = 'A';
        require = ["^bssCalendar"];

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes: IBssCalendarViewKindsProviderAttriubtes,
                    controller: any[]
                    ): void {
                    var bssCalendarController = <BssCalendarController>controller[0];
                    var kinds = instanceAttributes.bssCalendarViewKinds.split(',');
                    var instances = [];
                    _.each(kinds, (kind) => {
                        var creator = "new " + kind + "(bssCalendarController)";
                        var viewKind = eval(creator);
                        instances.push(viewKind);
                    });

                    bssCalendarController.state.viewKinds = instances;
                    bssCalendarController.setView(instances[instanceAttributes.bssCalendarDefaultViewKind ? instanceAttributes.bssCalendarDefaultViewKind : 0]);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarViewKinds', (new BssCalendarViewKindsProviderDirective()).factory());
}