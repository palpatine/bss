﻿module SolvesIt.Widgets.Calendar {

    interface IBssCalendarStateCacheAttributes extends ng.IAttributes {
        bssCalendarStateCache: string;
    }

    class BssCalendarStateCacheDirectiveController {
        public static $inject = ['$scope', '$attrs', 'stateCacheService'];

        constructor(
            private $scope,
            private $attrs,
            private stateCacheService: SolvesIt.Widgets.IStateCacheService) {
        }
        public setCalendarController(controller: BssCalendarController) {
            controller.cacheId = this.$scope.$eval(this.$attrs.bssCalendarStateCache);
            controller.state = this.stateCacheService.getCacheFor(controller.cacheId, () => controller.state);
        }
    }

    class BssCalendarStateCacheDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict: string = 'A';
        require = ['bssCalendarStateCache', "^bssCalendar"];
        controller = "bssCalendarStateCacheDirectiveController";
        priority = -10;

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var ownController = <BssCalendarStateCacheDirectiveController>controller[0];
                    var bssCalendarController = <BssCalendarController>controller[1];
                    ownController.setCalendarController(bssCalendarController);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarStateCache', (new BssCalendarStateCacheDirective()).factory())
        .controller('bssCalendarStateCacheDirectiveController', BssCalendarStateCacheDirectiveController);
} 