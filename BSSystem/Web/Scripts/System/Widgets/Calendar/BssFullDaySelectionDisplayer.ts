﻿module SolvesIt.Widgets.Calendar {

    class BssFullDaySelectionDisplayerController {
        controller: BssCalendarController;

        public setCalendarController(controller: BssCalendarController) {
            this.controller = controller;
        }

        public process(): void {
            if (!this.controller.ngModel) {
                return;
            }

            _.each(this.controller.state.viewKind.rows, row => {
                _.each(row.cells, cell => {
                    cell.classes = this.merge(cell.classes, { 'bss-calendar-selected': this.isSelected(cell.date) });
                });
            });
        }

        private isSelected(date: Date): boolean {
            if (!this.controller.ngModel.selection) {
                return false;
            }

            var start = new Date(this.controller.ngModel.selection.start.getTime());
            start.setHours(0, 0, 0, 0);
            var end = new Date(this.controller.ngModel.selection.end.getTime());
            end.setHours(0, 0, 0, 0);
            return start <= date && end > date;
        }

        private merge(first: any, second: any): any {

            if (!first) {
                return second;
            }

            if (!second) {
                return first;
            }

            return angular.extend(first, second);
        }
    }

    class BssFullDaySelectionDisplayerHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ["^bssCalendar", "bssFullDaySelectionDisplayer"];
        controller = "bssFullDaySelectionDisplayerController";
        priority = 10;

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var bssCalendarController = <BssCalendarController>controller[0];
                    var ownController = <BssFullDaySelectionDisplayerController>controller[1];
                    ownController.setCalendarController(bssCalendarController);
                    bssCalendarController.addGridProcessor(() => ownController.process());
                    bssCalendarController.addSelectionProcessor(() => ownController.process());
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssFullDaySelectionDisplayer', (new BssFullDaySelectionDisplayerHandlerDirective()).factory())
        .controller('bssFullDaySelectionDisplayerController', BssFullDaySelectionDisplayerController);
}  