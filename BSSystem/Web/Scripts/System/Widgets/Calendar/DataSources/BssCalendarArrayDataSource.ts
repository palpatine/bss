﻿module SolvesIt.Widgets.Calendar {
    class BssCalendarArrayDataSourceController implements IDataProvider {
        public static $inject = ['$scope', '$attrs','$q'];
        private controller: BssCalendarController;
        private items: ICalendarItem[];
        constructor(
            private $scope,
            private $attrs,
            private $q:ng.IQService) {

        }

        loadItems(dateRange: IDateRange, displayCriteria: IDisplayCriteria): ng.IPromise<ICalendarItem[]> {
            var defferer = this.$q.defer();
            defferer.resolve(this.items);
            return defferer.promise;
        }

        public setCalendarController(calendarController: BssCalendarController) {
            this.controller = calendarController;
            this.controller.setDataProvider(this);

            this.items = [
                {
                    date: new Date('2015-06-02'),
                 //   length: 1
                }, {
                    date: new Date('2015-06-03'),
                  //  length: 2
                }, {
                    date: new Date('2015-06-04'),
                  //  length:2
                }, {
                    date: new Date('2015-06-04'),
                 //   length: 4
                }, {
                    date: new Date('2015-06-04'),
                //    length: 1
                }, {
                    date: new Date('2015-06-04'),
                  //  length: 2
                }, {
                    date: new Date('2015-06-06'),
                   // length: 5
                }, {
                    date: new Date('2015-06-09'),
                  //  length: 3
                }
            ];

        }
    }

    class BssCalendarArrayDataSourceHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ['bssCalendarArrayDataSource', "^bssCalendar"];
        controller = "bssCalendarArrayDataSourceController";

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var ownController = <BssCalendarArrayDataSourceController>controller[0];
                    var bssCalendarController = <BssCalendarController>controller[1];
                    ownController.setCalendarController(bssCalendarController);
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarArrayDataSource', (new BssCalendarArrayDataSourceHandlerDirective()).factory())
        .controller('bssCalendarArrayDataSourceController', BssCalendarArrayDataSourceController);
} 