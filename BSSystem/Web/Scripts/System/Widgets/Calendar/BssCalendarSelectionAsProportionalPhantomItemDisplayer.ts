﻿module SolvesIt.Widgets.Calendar {


    class BssCalendarSelectionAsProportionalPhantomItemDisplayerController {
        private controller: BssCalendarController;
        private phantom: IPhantomDistributedItem;

        public setCalendarController(controller: BssCalendarController): void {
            this.controller = controller;
        }


        public process(): void {
            if (!this.controller.ngModel || !this.controller.ngModel.selection) {
                if (this.phantom != null) {
                    this.phantom.item.remove();
                    this.phantom = null;
                }
                return;
            }

            if (this.controller.state.viewKind.name == "daily") {
                var config = this.controller.state.displayCriteria.viewKindsConfiguration["daily"];
                var date = new Date(this.controller.ngModel.selection.start.getTime());
                date.setHours(0, 0, 0, 0);
                var time = date.getTime();
                var length = (this.controller.ngModel.selection.end.getTime() - this.controller.ngModel.selection.start.getTime()) / (60 * 1000);
                var firstHour = config.firstHour || 0;
                var eventOffsetYStart = (this.controller.ngModel.selection.start.getHours() * 60 + this.controller.ngModel.selection.start.getMinutes() - firstHour * 60) / this.controller.state.displayCriteria.pixelsPerMinute;
                var eventOffsetYEnd = (this.controller.ngModel.selection.end.getHours() * 60 + this.controller.ngModel.selection.end.getMinutes() - firstHour * 60) / this.controller.state.displayCriteria.pixelsPerMinute;

                if (this.phantom != null && this.phantom.isLocked) {
                    this.phantom.item.remove();
                    this.phantom = null;
                }
                else if (this.phantom == null) {
                    this.phantom = {
                        isPhantom: true,
                        isLocked: false,
                        item: {
                            date: date,
                            length: length,
                            remove: null
                        },
                        classes: { "bss-calendar-item": true, "bss-calendar-phantom-item": true },
                        styles: {
                            position: "absolute",
                            top: eventOffsetYStart,
                            height: eventOffsetYEnd - eventOffsetYStart,
                            width: "100%"
                        }
                    };

                    _.find(this.controller.state.viewKind.rows, row => {
                        return !!_.find(<IDistributedCell[]>row.cells, (cell) => {
                            if (cell.date.getTime() == time) {
                                this.phantom.item.remove = () => {
                                    if (!cell.items || !this.phantom) return;

                                    var index = cell.items.indexOf(this.phantom);
                                    if (index != -1) {
                                        cell.items.splice(index, 1);
                                    }
                                };
                                cell.items = cell.items || [];
                                cell.items.push(this.phantom);
                                return true;
                            }

                            return false;
                        });
                    });
                } else if (!this.controller.ngModel.isDirty && !this.phantom.isLocked) {
                    this.phantom.isLocked = true;
                    this.phantom.item.length = length;
                    this.phantom.styles = {
                        position: "absolute",
                        top: eventOffsetYStart,
                        height: eventOffsetYEnd - eventOffsetYStart,
                        width: "100%"
                    };
                } else {
                    this.phantom.item.length = length;
                    this.phantom.styles = {
                        position: "absolute",
                        top: eventOffsetYStart,
                        height: eventOffsetYEnd - eventOffsetYStart,
                        width: "100%"
                    };
                }
            }
        }
    }

    class BssCalendarSelectionAsProportionalPhantomItemDisplayerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ['bssCalendarSelectionAsProportionalPhantomItemDisplayer', "^bssCalendar"];
        controller = "bssCalendarSelectionAsProportionalPhantomItemDisplayerController";
        priority = 10;

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var ownController = <BssCalendarSelectionAsProportionalPhantomItemDisplayerController>controller[0];
                    var bssCalendarController = <BssCalendarController>controller[1];
                    ownController.setCalendarController(bssCalendarController);

                    bssCalendarController.addGridProcessor(() => ownController.process());
                    bssCalendarController.addSelectionProcessor(() => ownController.process());
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarSelectionAsProportionalPhantomItemDisplayer', (new BssCalendarSelectionAsProportionalPhantomItemDisplayerDirective()).factory())
        .controller('bssCalendarSelectionAsProportionalPhantomItemDisplayerController', BssCalendarSelectionAsProportionalPhantomItemDisplayerController);
}