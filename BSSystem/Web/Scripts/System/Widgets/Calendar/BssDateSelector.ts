﻿module SolvesIt.Widgets.Calendar {

    interface IDateSelectorAttributes {
        isOpen;
        disabled;
        onChange: string;
    }

    class BssDateSelectorController {
        maxDate: Date // "=?",
        minDate: Date// "=?",
        disabled: boolean// "=?"
        selectorDisplayCriteria: SolvesIt.Widgets.Calendar.IDisplayCriteria;
        ngModel: SolvesIt.Widgets.Calendar.CalendarModel;

        static $inject = ['$scope', '$attrs'];

        constructor(
            private $scope: ng.IScope,
            private $attrs: IDateSelectorAttributes) {
        }

        public previous(): void {
            this.selectorDisplayCriteria.period.start.setMonth(this.selectorDisplayCriteria.period.start.getMonth() - 1);
            this.selectorDisplayCriteria.period.end.setMonth(this.selectorDisplayCriteria.period.end.getMonth() - 1);
            this.selectorDisplayCriteria.period.start.setDate(1);
            this.selectorDisplayCriteria.period.end.setDate(1);
        }

        public next(): void {
            this.selectorDisplayCriteria.period.start.setMonth(this.selectorDisplayCriteria.period.start.getMonth() + 1);
            this.selectorDisplayCriteria.period.end.setMonth(this.selectorDisplayCriteria.period.end.getMonth() + 1);
            this.selectorDisplayCriteria.period.start.setDate(1);
            this.selectorDisplayCriteria.period.end.setDate(1);
        }
    }

    class BssDateSelectorDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {
        restrict: string = 'EA';
        require = ['bssDateSelector', 'ngModel'];
        scope = {
            selectorDisplayCriteria: '=',
            ngModel: '=',
            disabled: "=?"
        }
        controllerAs: string = 'vm';
        bindToController = true;
        controller = "bssDateSelectorController";
        templateUrl: string = '~/Content/Templates/BssCalendar/DateSelector/Main.tpl.html';
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssDateSelector', (new BssDateSelectorDirective()).factory())
        .controller('bssDateSelectorController', BssDateSelectorController);
}