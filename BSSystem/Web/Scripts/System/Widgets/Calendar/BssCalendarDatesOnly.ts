﻿module SolvesIt.Widgets.Calendar {


    class BssCalendarDatesOnlyHandlerDirective extends SolvesIt.Bss.Core.Integration.Angular.BasicDirective {

        restrict: string = 'A';
        require = ["^bssCalendar"];
        priority = 10;

        public createHandler(): SolvesIt.Bss.Core.Integration.Angular.IDirectiveHandler {
            return {
                link(
                    scope: ng.IScope,
                    instanceElement: ng.IAugmentedJQuery,
                    instanceAttributes,
                    controller: any[]
                    ): void {
                    var bssCalendarController = <BssCalendarController>controller[0];
                    _.each(bssCalendarController.state.viewKinds, (item: ViewKind) => {
                        if (item.name == "weekly") {
                            item.cellTemplateUrl = '~/Content/Templates/BssCalendar/Weekly/DateOnlyCell.tpl.html';
                            item.rowHeaderTemplateUrl = '~/Content/Templates/BssCalendar/Weekly/DateOnlyRowHeader.tpl.html';
                            item.rowHeaderClasses = {
                                "bss-calendar-dates-only-row-header": true
                            };
                            item.crossHeaderClasses = {
                                "bss-calendar-dates-only-cross-header": true
                            }
                        }
                    });
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .directive('bssCalendarDatesOnly', (new BssCalendarDatesOnlyHandlerDirective()).factory());
} 