﻿module SolvesIt.Widgets.ActionsGenerator {
    interface IActionsGeneratorAttributes {
        kind: string;
        entities: string;
        permission: string;
        part: string;
        id: string;
        widthModifier: string;
        'class': string;
    }

    class ActionsGenerator {

        can: { [key: string]: boolean } = {};
        attrs: IActionsGeneratorAttributes;
        scope;
        globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService;
        public initialize(
            $compile,
            scope,
            element: ng.IAugmentedJQuery,
            attrs: IActionsGeneratorAttributes,
            globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {

            this.attrs = attrs;
            this.globalStateService = globalStateService;
            this.scope = scope;

            var kindsValue = <{ [name: string]: string }>scope.$eval(attrs.kind);

            var actionsDescriptor: { actions: string; count: number };
            if (kindsValue) {
                actionsDescriptor = this.processObjectKinds(kindsValue, attrs.class);
            } else {
                actionsDescriptor = this.processStringKinds(attrs.kind, attrs.class, attrs.part, attrs.entities);
            }
            scope.$ActionsGenerator = this;

            angular.element(element.parents('td')[0])
                .css('width', (actionsDescriptor.count + parseInt(attrs.widthModifier || '0')) * 32)
                .attr('nowrap', 'true');
            element.replaceWith($compile(actionsDescriptor.actions)(scope));
        }

        go(href: string) {
            window.location.href = href;
        }

        private processObjectKinds(kinds: { [name: string]: string }, classes: string): { actions: string; count: number } {
            var actions: string = '';
            var count: number = 0;
            _.each(kinds, (action, kind) => {
                count++;
                var template = '<div class="btn-group ' + classes + '" role="group" aria-label="..."> \
                                        <button type="button" class="btn btn-primary btn-xs" ' +
                    'ng-click="$parent.' + action + '" > \
                                            <span class="glyphicon glyphicon-' + kind + '"></span> \
                                        </button > \
                                    </div >';

                actions += template;
            });

            return { actions: actions, count: count };
        }

        private processStringKinds(kind: string, classes: string, part: string, entities: string): { actions: string; count: number } {
            var actions: string = '';
            var kinds = kind.split(',');
            _.each(kinds, (kind) => {
                kind = kind.trim();
                if (kind == 'Create') {

                    this.can[kind] = !this.attrs.permission;

                    if (this.attrs.permission) {
                        this.globalStateService.require(this.attrs.permission, null, entities)
                            .then((value) => this.can[kind] = value);
                    }

                    var createTemplate = '<div class="btn-group ' + classes + '" role="group" aria-label="..." > \
                                        <button type="button" class="btn btn-primary btn-xs" ng-if="$ActionsGenerator.can.'+ kind + '"' +
                        'ng-click="$ActionsGenerator.go(\'' + Globals.rootPath + '#/Forms/' + entities + '/' + kind + '\')" > \
                                            <span class="glyphicon glyphicon-plus"></span> \
                                        </button > \
                                    </div >';

                    actions += createTemplate;
                }
                else if (kind == 'Edit') {
                    var editTemplate = '<div class="btn-group ' + classes + '" role="group" aria-label="..."> \
                                        <button type="button" class="btn btn-primary btn-xs" ' +
                        'ng-click="$ActionsGenerator.go(\'' + Globals.rootPath + '#/Forms/' + entities + '/\'+id+\'/' + part + '\')" > \
                                            <span class="glyphicon glyphicon-pencil"></span> \
                                        </button > \
                                    </div >';

                    actions += editTemplate;
                }
                else {

                    this.can[kind] = !this.attrs.permission;

                    if (this.attrs.permission) {
                        this.globalStateService.require(this.attrs.permission, this.scope.id, entities)
                            .then((value) => this.can[kind] = value);
                    }

                    var template = '<div class="btn-group ' + classes + '" role="group" aria-label="..." > \
                                        <button type="button" class="btn btn-info btn-xs" ng-if="$ActionsGenerator.can.' + kind + '"' +
                        'ng-click="$ActionsGenerator.go(\'' + Globals.rootPath + '#/Forms/' + entities + '/' + kind + '/\'+id)" > \
                                            <span class="glyphicon glyphicon-search"></span> \
                                        </button > \
                                    </div >';
                    actions += template;
                }
            });

            return { actions: actions, count: kinds.length };
        }
    }

    SolvesIt.Bss.Application
        .getApplication()
        .directive('bssActions',
        [
            '$parse', '$compile', 'globalStateService',
            ($parse, $compile, globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) => {
                return {
                    restrict: 'E',
                    scope: {
                        id: "="
                    },
                    link: (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: IActionsGeneratorAttributes) => {
                        var directive = new ActionsGenerator();
                        directive.initialize(
                            $compile,
                            scope,
                            element,
                            attrs,
                            globalStateService);
                    }
                };
            }
        ]);
}