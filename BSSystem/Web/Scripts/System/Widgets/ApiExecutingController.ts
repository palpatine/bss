﻿module SolvesIt.Widgets.ApiExecutingController {
    class ApiExecutingController {

        static $inject = [
            '$http',
            '$modal'
        ];

        constructor(
            private $http: ng.IHttpService,
            private $modal: angular.ui.bootstrap.IModalService) {
        }

        public execute(url: string): void {
            this.$http.post<SolvesIt.Bss.Models.IValidationResult>(url, {})
                .success(result => {
                    this.$modal.open({
                        animation: true,
                        templateUrl: '~/Content/Templates/MessageBox/Main.tpl.html',
                        controller: 'messageBoxController',
                        controllerAs: "vm",
                        size: 'sm',
                        resolve: {
                            message: () => {
                                if (result.isValid) {
                                    return result.message;
                                } else {
                                    var message = '';

                                    _.each(result.errors, (error) => {
                                        message += error.message + " ";
                                    });

                                    return message;
                                }
                            }
                        }
                    });
                });
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller("apiExecutingController", ApiExecutingController);
}