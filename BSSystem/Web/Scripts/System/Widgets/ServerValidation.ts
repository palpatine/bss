﻿////http://icanmakethiswork.blogspot.com/search/label/server%20validation

module SolvesIt.Widgets{
    class ServerValidation {

        static template: string = '<div class="alert alert-warning" role="alert">' +
        '<i class="glyphicon glyphicon-warning-sign"></i> ' +
        '%error%</div>';

        public initialize(scope, element: ng.IAugmentedJQuery, attrs, ngModelController: ng.INgModelController) {

            var decorator = angular.element('<div></div>');
            element.after(decorator);

            scope.$watch(this.safeWatch(() => ngModelController.$error.server), showHideValidation);

            function showHideValidation(serverError: boolean) {

                // Display an error if serverError is true otherwise clear the element
                var errorHtml = "";
                if (serverError) {
                    // Aliasing serverError and name to make it more obvious what their purpose is
                    var errorDictionary = scope.$eval(attrs.bssServerValidation);
                    if (errorDictionary) {
                        var errorKey = attrs.name;
                        errorHtml = ServerValidation.template.replace(/%error%/, errorDictionary[errorKey] || "Unknown error occurred...");
                    }
                }
                decorator.html(errorHtml);
            }

            ngModelController.$viewChangeListeners.push(() => {
                ngModelController.$setValidity("server", true);
            });
            // wipe the server error message upon keyup or change events so can revalidate with server 
            //element.on("keyup change", () => {
            //    scope.$apply(() => { ngModelController.$setValidity("server", true); });
            //});
        }

        safeWatch(expression: { (): any }) {
            return () => {
                try {
                    return expression();
                }
                catch (e) {
                    return null;
                }
            };
        }
    }

  SolvesIt.Bss.Application.getApplication()
        .directive('bssServerValidation',
        [
            () => {
                return {
                    restrict: 'A',
                    require: "ngModel",
                    link: (scope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, ngModelController: ng.INgModelController) => {
                        var directive = new ServerValidation();
                        directive.initialize(
                            scope,
                            element,
                            attrs,
                            ngModelController);
                    }
                };
            }
        ]);
}