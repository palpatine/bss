﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Newtonsoft.Json.Serialization;
using SolvesIt.BSSystem.Web.Environment.Api;

namespace SolvesIt.BSSystem.Web
{
    public class WebApiApplication : HttpApplication
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi whey of doing things.")]
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(x =>
            {
                x.MapHttpAttributeRoutes();
                WebApiConfig.Register(x.Routes);
            });
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            EnvironmentBootstrapper.Bootstrap();

            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
#if DEBUG
            json.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
#endif
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            ClientDataTypeModelValidatorProvider.ResourceClassKey = "GlobalResources";
            DefaultModelBinder.ResourceClassKey = "GlobalResources";
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new AreaHttpControllerSelector(GlobalConfiguration.Configuration));
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpActionSelector), new HybridActionSelector());
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi whey of doing things.")]
        protected void Application_PostAuthorizeRequest()
        {
            if (IsWebApiRequest())
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("pl-PL");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("pl-PL");
            }
        }

        private static bool IsWebApiRequest()
        {
            return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(WebApiConfig.UrlPrefixRelative, StringComparison.Ordinal);
        }
    }
}