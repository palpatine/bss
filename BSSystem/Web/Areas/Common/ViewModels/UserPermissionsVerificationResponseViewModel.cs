﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class UserPermissionsVerificationResponseViewModel
    {
        public string PermissionPath { get; set; }

        public IEnumerable<int> Entities { get; set; }
    }
}