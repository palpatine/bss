﻿using System;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class SectionViewModel : NamedViewModel, ISection
    {
        public string Name { get; set; }

        [PropertySource(Ignore = true)]
        public NamedViewModel Parent { get; set; }

        public DateTimeOffset? DateEnd { get; set; }

        public DateTimeOffset DateStart { get; set; }
    }
}