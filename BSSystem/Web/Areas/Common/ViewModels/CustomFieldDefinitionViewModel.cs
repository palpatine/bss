﻿using System;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class CustomFieldDefinitionViewModel : NamedViewModel, ICustomFieldDefinition
    {
        public bool IsRequired { get; set; }

        public bool IsEditable { get; set; }

        public bool IsInViews { get; set; }

        public bool IsNullable { get; set; }

        public bool IsUnique { get; set; }

        public string Schema { get; set; }

        public string TableKey { get; set; }

        public DateTimeOffset? DateEnd { get; set; }

        public DateTimeOffset? DateStart { get; set; }
        public string Comment { get; set; }
    }
}