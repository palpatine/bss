﻿namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels.Templates
{
    public sealed class PermissionsAssignmentPanelTemplateViewModel
    {
        public string Elements { get; set; }

        public string ColumnsSource { get; set; }

        public string DataSource { get; set; }
    }
}
