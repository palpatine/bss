using System;
using System.Text.RegularExpressions;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class RoleDefinitionViewModel : NamedViewModel, IRoleDefinition
    {
        [PropertySource(Ignore = true)]
        public TargetViewModel Target { get; set; }

        public DateTimeOffset DateStart { get; set; }

        public DateTimeOffset? DateEnd { get; set; }

        public string Comment { get; set; }
    }
}