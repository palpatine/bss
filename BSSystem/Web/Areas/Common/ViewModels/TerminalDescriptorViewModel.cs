﻿using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class TerminalDescriptorViewModel : IViewModel
    {
        public string DefaultValue { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public string Template { get; set; }
    }
}