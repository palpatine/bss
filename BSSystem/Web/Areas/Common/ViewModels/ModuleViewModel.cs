﻿using System.Diagnostics;
using System.Globalization;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Properties;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public class ModuleViewModel : DetailedEntityViewModel, IModule
    {
        public ModuleViewModel()
        {
            DateStart = WebConstants.MinimalAccepableDate;
        }

        [PropertySource(Ignore = true)]
        public override string DisplayName
        {
            get
            {
                var value = WebResources.ResourceManager.GetString("Module_" + Key);

                if (value == null)
                {
                    value = WebResources.ResourceManager.GetString(Key);
                    if (value == null)
                    {
                        Debug.WriteLine("Missing resource key {0} or {1}", Key, "Module_" + Key);
                        value = string.Format(CultureInfo.InvariantCulture, "[{0}]", Key);
                    }
                }

                return value;
            }
        }

        public string Key { get; set; }

        public string Required { get; set; }
    }
}