﻿using System.Collections.Generic;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class ApplicableTerminalViewModel : IViewModel
    {
        public IEnumerable<ApplicableDescriptor> ApplicableDescriptors { get; set; }
    }
}