﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class ApplicableDescriptor
    {
        public string Key { get; set; }

        public IEnumerable<TerminalDescriptorViewModel> TerminalDescriptors { get; set; }
    }
}