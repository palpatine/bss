﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class UserPermissionsVerificationRequestViewModel
    {
        public IEnumerable<string> PermissionPaths { get; set; }
    }
}