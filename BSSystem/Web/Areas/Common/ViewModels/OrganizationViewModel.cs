﻿using System;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class OrganizationViewModel : NamedViewModel, IOrganization
    {
        public DateTimeOffset DateStart { get; set; }

        public DateTimeOffset? DateEnd { get; set; }

        [PropertySource(typeof(Company))]
        public string Name { get; set; }

        [PropertySource(typeof(Company))]
        public string FullName { get; set; }

        [PropertySource(typeof(Company))]
        public string CountryCode { get; set; }

        [PropertySource(typeof(Company))]
        public string VatNumber { get; set; }

        [PropertySource(typeof(Company))]
        public string OfficialNumber { get; set; }

        [PropertySource(typeof(Company))]
        public string StatisticNumber { get; set; }

        [PropertySource(typeof(Company))]
        public decimal? Phone { get; set; }

        [PropertySource(typeof(Company))]
        public decimal? Fax { get; set; }

        [PropertySource(typeof(Company))]
        public string Email { get; set; }
    }
}