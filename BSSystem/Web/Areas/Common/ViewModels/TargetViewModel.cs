using System.Diagnostics;
using System.Globalization;
using SolvesIt.BSSystem.Web.Properties;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class TargetViewModel
    {
        public string Id { get; set; }

        public string DisplayName
        {
            get
            {
                var value = WebResources.ResourceManager.GetString("TableKey_" + Id);

                if (value == null)
                {
                    value = WebResources.ResourceManager.GetString(Id);
                    if (value == null)
                    {
                        Debug.WriteLine("Missing resource key {0} or {1}", Id, "TableKey_" + Id);
                        value = string.Format(CultureInfo.InvariantCulture, "[{0}]", Id);
                    }
                }

                return value;
            }
        }
    }
}