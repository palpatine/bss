﻿namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class ChangePasswordViewModel
    {
        public string Password { get; set; }

        public string NewPassword { get; set; }

        public string NewPasswordRepeated { get; set; }
    }
}