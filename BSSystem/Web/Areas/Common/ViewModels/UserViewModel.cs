﻿using System;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.ViewModels
{
    public sealed class UserViewModel
        : NamedViewModel,
        IUser
    {
        public string FirstName { get; set; }

        public string Location { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Justification = "Users authentication string used during log in.")]
        [PropertySource(Ignore = true)]
        public string LoginName { get; set; }

        public string Mail { get; set; }

        public string Phone { get; set; }

        public string Surname { get; set; }

        public DateTimeOffset? DateEnd { get; set; }

        public DateTimeOffset DateStart { get; set; }

        public bool IsAlias { get; set; }
    }
}