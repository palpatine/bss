using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Templates;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Organizations;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Permissions;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Positions;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.RoleDefinitions;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Sections;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Users;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.Web.Extensions.Route;
using SolvesIt.BSSystem.Web.Properties;

namespace SolvesIt.BSSystem.Web.Areas.Common.Extensions
{
    public class CommonTemplatesExtensionsBootstrapper : TemplatesExtensionsBootstrapper
    {
        protected override void CreateDetailsCommands()
        {
            CreateApiCallCommand<OrganizationDetailsCommand>(
                    x => x.BssHttpRouteAngularExpression((OrganizationController c) => c.CreateAdminAlias(RouteParameter.IdFromSection())),
                    () => WebResources.CreateAlias);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected override void CreateDetailsPanels()
        {
            CreateAssignmentDetailsPanel<PositionDetailsPanel, IPosition, IUser>();
            CreateAssignmentDetailsPanel<UserDetailsPanel, IUser, IPosition>();
            CreateAssignmentDetailsPanel<UserDetailsPanel, IUser, ISection>();
            CreateAssignmentDetailsPanel<SectionDetailsPanel, ISection, IUser>();
            Create<PermissionDetailsPanel, PermissionTemplatesController>(
                x => x.UsersAssignment(),
                useTypescriptController: false);
            Create<PermissionDetailsPanel, PermissionTemplatesController>(
                x => x.RoleDefinitionsAssignment(),
                useTypescriptController: false);
            Create<PermissionDetailsPanel, PermissionTemplatesController>(
                x => x.OrganizationsAssignment(),
                useTypescriptController: false);
            Create<RoleDefinitionDetailsPanel, RoleDefinitionTemplatesController>(
                x => x.PermissionsAssignment(),
                PermissionsCommon.RoleDefinition.Details.Path,
                PermissionsCommon.RoleDefinition.Details.Assignment.Common_Permission.Path);
            Create<UserDetailsPanel, UserTemplatesController>(
                x => x.PermissionsAssignment(),
                PermissionsCommon.User.Details.Path,
                PermissionsCommon.User.Details.Assignment.Common_Permission.Path);
            Create<OrganizationDetailsPanel, OrganizationTemplatesController>(
                x => x.PermissionsAssignment(),
                PermissionsCommon.Organization.Details.Path,
                PermissionsCommon.Organization.Details.Assignment.Common_Permission.Path);
            CreateAssignmentDetailsPanel<OrganizationDetailsPanel, IOrganization, IModule>();
        }
    }
}