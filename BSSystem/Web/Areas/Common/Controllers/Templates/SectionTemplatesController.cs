﻿using System.Globalization;
using System.Web.Mvc;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Sections;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.Web.Extensions.Route;
using SolvesIt.BSSystem.Web.ViewModels.Core;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Templates
{
    public sealed class SectionTemplatesController : BaseTemplatesController<Section, SectionDetailsPanel, SectionDetailsCommand>
    {
        public SectionTemplatesController(
            TemplateDescriptor<SectionDetailsPanel, SectionDetailsCommand> templateDescriptor)
            : base(templateDescriptor, PermissionsCommon.Section.Details.Write.Path)
        {
        }

        public PartialViewResult DetailedList()
        {
            var area = (string)Request.RequestContext.RouteData.DataTokens["area"];
            area = char.ToUpper(area[0], CultureInfo.InvariantCulture) + area.Substring(1);

            var model = new EntitiesListTemplateViewModel
            {
                Entities = typeof(Section).Name,
                DataSource = @Url.BssHttpRouteAngularExpression((SectionController c) => c.Get()),
                ColumnsSource = @Url.BssHttpRouteAngularExpression((SectionController c) => c.GetListColumnsConfiguration()),
            };

            return PartialView(model);
        }
    }
}