﻿using System.Web.Mvc;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.RoleDefinitions;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels.Templates;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Templates
{
    public sealed class RoleDefinitionTemplatesController : BaseTemplatesController<RoleDefinition, RoleDefinitionDetailsPanel, RoleDefinitionDetailsCommand>
    {
        public RoleDefinitionTemplatesController(
            TemplateDescriptor<RoleDefinitionDetailsPanel, RoleDefinitionDetailsCommand> descriptor)
            : base(descriptor, PermissionsCommon.RoleDefinition.Details.Write.Path)
        {
        }

        public ActionResult PermissionsAssignment()
        {
            var model = new PermissionAssignmentTemplateModel
            {
                Entities = typeof(RoleDefinition).Name,
            };

            return PartialView(model);
        }
    }
}