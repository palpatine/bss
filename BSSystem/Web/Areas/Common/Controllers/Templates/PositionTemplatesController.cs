﻿using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Positions;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Templates
{
    public sealed class PositionTemplatesController : BaseTemplatesController<Position, PositionDetailsPanel, PositionDetailsCommand>
    {
        public PositionTemplatesController(
            TemplateDescriptor<PositionDetailsPanel, PositionDetailsCommand> templateDescriptor)
            : base(templateDescriptor, PermissionsCommon.Position.Details.Write.Path)
        {
        }
    }
}