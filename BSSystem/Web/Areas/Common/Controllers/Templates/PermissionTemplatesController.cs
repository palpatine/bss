﻿using System.Web.Mvc;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Permissions;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels.Templates;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.Web.Extensions.Route;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Templates
{
    public sealed class PermissionTemplatesController : BaseTemplatesController<Permission, PermissionDetailsPanel, PermissionDetailsCommand>
    {
        public PermissionTemplatesController(
            TemplateDescriptor<PermissionDetailsPanel, PermissionDetailsCommand> descriptor)
            : base(descriptor, null)
        {
        }

        public ActionResult OrganizationsAssignment()
        {
            var model = new PermissionsAssignmentPanelTemplateViewModel
            {
                Elements = typeof(Organization).Name,
                ColumnsSource = Url.BssHttpRouteAngularExpression((PermissionController x) => x.GetOrganizationAssignmentColumns()),
                DataSource = Url.BssHttpRouteAngularExpression((PermissionController x) => x.GetOrganizationsWithPermission(RouteParameter.IdFromModel()))
            };
            return PartialView(
                "Assignment",
                model);
        }

        public ActionResult RoleDefinitionsAssignment()
        {
            var model = new PermissionsAssignmentPanelTemplateViewModel
            {
                Elements = typeof(RoleDefinition).Name,
                ColumnsSource = Url.BssHttpRouteAngularExpression((PermissionController x) => x.GetRoleDefinitionAssignmentColumns()),
                DataSource = Url.BssHttpRouteAngularExpression((PermissionController x) => x.GetRoleDefinitionsWithPermission(RouteParameter.IdFromModel()))
            };
            return PartialView(
                "Assignment",
                model);
        }

        public ActionResult UsersAssignment()
        {
            var model = new PermissionsAssignmentPanelTemplateViewModel
            {
                Elements = typeof(User).Name,
                ColumnsSource = Url.BssHttpRouteAngularExpression((PermissionController x) => x.GetUserAssignmentColumns()),
                DataSource = Url.BssHttpRouteAngularExpression((PermissionController x) => x.GetUsersWithPermission(RouteParameter.IdFromModel()))
            };
            return PartialView(
                "Assignment",
                model);
        }
    }
}