﻿using System.Web.Mvc;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Organizations;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels.Templates;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Templates
{
    public sealed class OrganizationTemplatesController
        : BaseTemplatesController<Organization, OrganizationDetailsPanel, OrganizationDetailsCommand>
    {
        public OrganizationTemplatesController(
            TemplateDescriptor<OrganizationDetailsPanel, OrganizationDetailsCommand> descriptor)
            : base(descriptor, PermissionsCommon.Organization.Details.Write.Path)
        {
        }

        public ActionResult ModulesAssignment()
        {
            return PartialView();
        }

        public ActionResult PermissionsAssignment()
        {
            var model = new PermissionAssignmentTemplateModel
            {
                Entities = typeof(Organization).Name,
            };
            return PartialView(model);
        }
    }
}