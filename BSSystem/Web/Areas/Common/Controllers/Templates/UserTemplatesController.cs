﻿using System.Web.Mvc;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Users;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels.Templates;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Templates
{
    public sealed class UserTemplatesController : BaseTemplatesController<User, UserDetailsPanel, UserDetailsCommand>
    {
        public UserTemplatesController(
            TemplateDescriptor<UserDetailsPanel, UserDetailsCommand> descriptor)
            : base(descriptor, PermissionsCommon.User.Details.Write.Path)
        {
        }

        public PartialViewResult ChangePassword()
        {
            return PartialView();
        }

        public ActionResult PermissionsAssignment()
        {
            var model = new PermissionAssignmentTemplateModel
            {
                Entities = typeof(User).Name,
            };
            return PartialView(model);
        }

        public ActionResult UserProfile()
        {
            return PartialView();
        }
    }
}