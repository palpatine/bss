﻿using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.CustomFields;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Templates
{
    public sealed class CustomFieldDefinitionTemplatesController : BaseTemplatesController<CustomFieldDefinition, CustomFieldDefinitionDetailsPanel, CustomFieldDefinitionDetailsCommand>
    {
        public CustomFieldDefinitionTemplatesController(
            TemplateDescriptor<CustomFieldDefinitionDetailsPanel, CustomFieldDefinitionDetailsCommand> descriptor)
            : base(descriptor, PermissionsCommon.CustomFieldDefinition.Details.Write.Path)
        {
        }
    }
}