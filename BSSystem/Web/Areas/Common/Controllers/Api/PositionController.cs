﻿using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsCommon.Containers.Position)]
    public class PositionController
        : EditScopedApiController<Position, IPosition, PositionViewModel>
    {
        private readonly ISessionDataProvider _sessionDataProvider;

        public PositionController(
            ISessionDataProvider sessionDataProvider,
            IDatabaseAccessor databaseAccessor,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<Position, IPosition> positionService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, positionService, permissionHelper, settingService)
        {
            _sessionDataProvider = sessionDataProvider;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<PositionViewModel>();
            builder.AddDefaultActionsColumn(PermissionsCommon.Position.Details.Write.Path, PermissionsCommon.Position.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Name)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        protected override async Task<Position> PrepareEditModelAsync(PositionViewModel model)
        {
            if (model.Id == 0)
            {
                return new Position
                {
                    DisplayName = model.DisplayName,
                    Name = model.Name,
                    DateEnd = await ConvertToDateAsync(model.DateEnd),
                    DateStart = await ConvertToDateAsync(model.DateStart),
                    Scope = _sessionDataProvider.Scope
                };
            }
            else
            {
                var position = DatabaseAccessor.Query<Position>()
                .SingleOrDefault(x =>
                    x.Scope.IsEquivalent(_sessionDataProvider.Scope)
                    && x.IsEquivalent<IPosition>(model));

                if (position == null)
                {
                    return null;
                }

                position.DisplayName = model.DisplayName;
                position.Name = model.Name;
                position.DateEnd = await ConvertToDateAsync(model.DateEnd);
                position.DateStart = await ConvertToDateAsync(model.DateStart);

                return position;
            }
        }

        protected override ITableQuery<PositionViewModel> ProjectToViewModel(ITableQuery<Position> query)
        {
            return query.Select(x => new PositionViewModel
            {
                Id = x.Id,
                DisplayName = x.DisplayName,
                Name = x.Name,
                DateStart = x.DateStart,
                DateEnd = x.DateEnd
            });
        }
    }
}