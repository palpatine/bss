﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.Properties;
using SolvesIt.BSSystem.Web.ViewModels;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsCommon.Containers.Organization)]
    public class OrganizationController
        : EditApiController<Organization, OrganizationEditModel, IOrganization, OrganizationViewModel>
    {
        private readonly IOrganizationService _organizationService;

        public OrganizationController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IOrganizationService organizationService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(
                databaseAccessor,
                sessionDataProvider,
                queryStringParser,
                organizationService,
                permissionHelper,
                settingService)
        {
            _organizationService = organizationService;
        }

        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Keys.AddAlias)]
        [ResponseType(typeof(ValidationResult))]
        [HttpPost]
        public async Task<IHttpActionResult> CreateAdminAlias(int id)
        {
            var validationErrors = await _organizationService.CreateAdminAliasAsync(new OrganizationMarker(id));
            return ProcessValidationErrors(validationErrors, messageOnSuccess: WebResources.CreateAliasSuccessMessage);
        }

        [RequiredPermission(PermissionsCommon.Keys.Details)]
        [ResponseType(typeof(IEnumerable<PermissionAssignmentViewModel>))]
        public async Task<IHttpActionResult> GetAssignedPermissions(
            int id)
        {
            var query = DatabaseAccessor.GetOrganizationPermissions(new OrganizationMarker(id))
                .Select(x => new PermissionAssignmentViewModel
                {
                    Id = x.Id,
                    JunctionId = x.JunctionId,
                    Key = x.Key,
                    ParentId = x.ParentId,
                    IsContainer = x.IsContainer,
                })
                .AsQueryable();

            return await FinalizeQueryAsync(query);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<OrganizationViewModel>();
            builder.AddDefaultActionsColumn(PermissionsCommon.Organization.Details.Write.Path, PermissionsCommon.Organization.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Name)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateEnd)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Containers.Assignment + PermissionsCommon.Keys.CommonPermission)]
        [ResponseType(typeof(ValidationResult))]
        public async Task<IHttpActionResult> PostAssignedPermissions(
            int id,
           [FromBody]IEnumerable<PermissionAssignmentPersistenceViewModel> assignments)
        {
            var organization = new OrganizationMarker(id);
            var newItemId = 0;
            var editModel = assignments
                .Select(x => new JunctionUdt<ISystemPermission>
                {
                    Id = x.JunctionId ?? --newItemId,
                    Element = new SystemPermissionMarker(x.Id)
                }).ToArray();

            var validationErrors = await _organizationService.UpdatePermissionsAsync(organization, editModel);
            return ProcessValidationErrors(validationErrors);
        }

        protected override async Task<OrganizationEditModel> PrepareEditModelAsync(OrganizationViewModel model)
        {
            if (model.Id != 0)
            {
                var marker = new OrganizationMarker(model.Id);
                var exists = DatabaseAccessor.Query<Organization>()
                    .Any(x => x.IsEquivalent<IOrganization>(marker) && !x.IsDeleted);

                if (!exists)
                {
                    return null;
                }
            }

            return new OrganizationEditModel
            {
                Id = model.Id,
                DisplayName = model.DisplayName,
                DateStart = await ConvertToDateAsync(model.DateStart),
                DateEnd = await ConvertToDateAsync(model.DateEnd),
                Phone = model.Phone,
                Email = model.Email,
                Fax = model.Fax,
                Name = model.Name,
                OfficialNumber = model.OfficialNumber,
                StatisticNumber = model.StatisticNumber,
                VatNumber = model.VatNumber,
                CountryCode = model.CountryCode,
                FullName = model.FullName,
            };
        }

        protected override ITableQuery<OrganizationViewModel> ProjectToViewModel(ITableQuery<Organization> query)
        {
            return query.Join(
                      DatabaseAccessor.Query<Company>().Where(x => !x.IsDeleted),
                      x => x.Company,
                      (x, y) => new OrganizationViewModel
                      {
                          Id = x.Id,
                          DisplayName = x.DisplayName,
                          DateStart = x.DateStart,
                          DateEnd = x.DateEnd,
                          Phone = y.Phone,
                          Email = y.Email,
                          Fax = y.Fax,
                          Name = y.Name,
                          OfficialNumber = y.OfficialNumber,
                          StatisticNumber = y.StatisticNumber,
                          VatNumber = y.VatNumber,
                          CountryCode = y.CountryCode,
                          FullName = y.FullName
                      });
        }
    }
}