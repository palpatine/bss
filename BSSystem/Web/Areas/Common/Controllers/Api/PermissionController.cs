using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Enumerations;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsCommon.Containers.Permission)]
    public sealed class PermissionController
        : BaseApiController
    {
        private readonly IPermissionService _permissionService;
        private readonly ISessionDataProvider _sessionDataProvider;

        public PermissionController(
            ISessionDataProvider sessionDataProvider,
            IDatabaseAccessor databaseAccessor,
            IQueryStringParser queryStringParser,
            IPermissionService permissionService,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, settingService)
        {
            _sessionDataProvider = sessionDataProvider;
            _permissionService = permissionService;
        }

        [HttpPost]
        public async Task<IEnumerable<UserPermissionsVerificationResponseViewModel>> FindAccessibleEntities(
            HttpRequestMessage request,
            [FromBody]UserPermissionsVerificationRequestViewModel model)
        {
            var userPermissions = await _permissionService.GetCurrentUserEffectivePermissions();
            var userPermissionsAssignments = await _permissionService.GetUserEffectivePermissionAssignments();

            var result = new List<UserPermissionsVerificationResponseViewModel>();
            foreach (var path in model.PermissionPaths)
            {
                var permission = userPermissions.SingleOrDefault(x => x.Path == path);
                if (permission == null)
                {
                    continue;
                }

                if (permission.IsGlobal)
                {
                    result.Add(new UserPermissionsVerificationResponseViewModel
                    {
                        PermissionPath = path,
                        Entities = Enumerable.Empty<int>()
                    });
                }

                var assignment = userPermissionsAssignments.SingleOrDefault(x => x.PermissionId == permission.Id);

                if (assignment == null)
                {
                    continue;
                }

                var storables = await _permissionService.GetCurrentUserEffectivePermissionsContextValues(assignment);

                result.Add(new UserPermissionsVerificationResponseViewModel
                {
                    PermissionPath = path,
                    Entities = storables.ToArray()
                });
            }

            return result;
        }

        [RequiredPermission(PermissionsCommon.Keys.Read)]
        [ResponseType(typeof(IQueryable<PermissionViewModel>))]
        public async Task<IHttpActionResult> Get()
        {
            var set = DatabaseAccessor.GetOrganizationPermissions(_sessionDataProvider.Scope)
                .Where(x => x.JunctionId != null || x.Id < 0)
                .Select(x => new PermissionViewModel
                {
                    Id = x.Id,
                    IsContainer = x.IsContainer,
                    Key = x.Key,
                    ParentId = x.ParentId
                })
                .AsQueryable();

            return await FinalizeQueryAsync(set);
        }

        [RequiredPermission(PermissionsCommon.Keys.Read)]
        [ResponseType(typeof(PermissionViewModel))]
        public async Task<IHttpActionResult> Get(
            int id)
        {
            var marker = new PermissionMarker(id);
            var set = DatabaseAccessor.GetOrganizationPermissions(_sessionDataProvider.Scope)
                .Where(x => (x.JunctionId != null || x.IsContainer) && x.IsEquivalent<IPermission>(marker))
                   .Select(x => new PermissionViewModel
                   {
                       Id = x.Id,
                       IsContainer = x.IsContainer,
                       Key = x.Key,
                       ParentId = x.ParentId
                   })
                   .ToList()
                   .AsQueryable();

            return await FinalizeModelAsync(set);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<PermissionAssignmentViewModel>();
            AddDetailsColumn(builder);
            builder.AddColumn(x => x.IsContainer)
                .SetTemplateUrl(TemplatesMetadata.PermissionsAssignmentEditorTemplates.ContainerRightIndicator)
                .SetHeaderTemplate(string.Empty)
                .Width(20);
            builder
                .AddColumn(x => x.DisplayName)
                .IsExpansionColumn();
            return builder.Build();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        public TableConfiguration GetOrganizationAssignmentColumns()
        {
            return GetAssignmentColumns(typeof(Organization));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), ResponseType(typeof(IQueryable<AssignmentViewModel>))]
        public async Task<IHttpActionResult> GetOrganizationsWithPermission(
            int id)
        {
            var permissionMarker = new PermissionMarker(id);
            var set = DatabaseAccessor.Query<Permission>()
                .Where(p => p.IsEquivalent<IPermission>(permissionMarker) && !p.IsDeleted)
                .Join(
                    DatabaseAccessor.Query<SystemPermission>(),
                    p => p.SystemPermission,
                    sp => sp,
                    (p, sp) => p)
                .Join(
                    DatabaseAccessor.Query<ModuleOrganization>()
                       .Where(x => !x.IsDeleted && x.EntityStatus == EntityStatus.Active),
                    p => p.Scope,
                    mo => mo.Organization,
                    (p, mo) => p)
                .Join(
                    DatabaseAccessor.Query<Organization>()
                       .Where(x => !x.IsDeleted && x.EntityStatus == EntityStatus.Active),
                    p => p.Scope,
                    (up, o) => new TimelessAssignmentViewModel
                    {
                        Id = up.Id,
                        Element = new DetailedEntityViewModel
                        {
                            Id = o.Id,
                            DisplayName = o.DisplayName,
                            DateStart = o.DateStart,
                            DateEnd = o.DateEnd
                        }
                    })
             .AsQueryable();

            return await FinalizeQueryAsync(set);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        public TableConfiguration GetPermissionsAssignmentDisplayColumns()
        {
            var builder = new TableConfigurationBuilder<PermissionAssignmentViewModel>();
            AddDetailsColumn(builder);
            builder.AddColumn(x => x.DisplayName)
                .IsExpansionColumn();
            builder.AddColumn(x => x.IsContainer)
                .SetTemplateUrl(TemplatesMetadata.PermissionsAssignmentEditorTemplates.ContainerRightIndicator)
                .SetHeaderTemplate(string.Empty)
                .Width(20);
            builder.AddColumn(x => x.IsAssigned);
            return builder.Build();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        public TableConfiguration GetPermissionsAssignmentEditColumns()
        {
            var builder = new TableConfigurationBuilder<PermissionAssignmentViewModel>();
            AddDetailsColumn(builder);
            builder
                .AddColumn(x => x.DisplayName)
                .IsExpansionColumn();
            builder.AddColumn(x => x.IsContainer)
                .SetTemplateUrl(TemplatesMetadata.PermissionsAssignmentEditorTemplates.ContainerRightIndicator)
                .SetHeaderTemplate(string.Empty)
                .Width(20);
            builder
                .AddColumn(x => x.IsAssigned)
                .SetTemplateUrl(TemplatesMetadata.PermissionsAssignmentEditorTemplates.IsAssignedColumn);
            return builder.Build();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        public TableConfiguration GetRoleDefinitionAssignmentColumns()
        {
            return GetAssignmentColumns(typeof(RoleDefinition));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), ResponseType(typeof(IQueryable<AssignmentViewModel>))]
        public async Task<IHttpActionResult> GetRoleDefinitionsWithPermission(
            int id)
        {
            var permissionMarker = new PermissionMarker(id);
            var set = DatabaseAccessor.Query<PermissionRoleDefinition>()
                 .Where(x => x.Permission.IsEquivalent(permissionMarker) && !x.IsDeleted)
                 .Join(
                    DatabaseAccessor.Query<RoleDefinition>()
                        .Where(x => !x.IsDeleted && x.EntityStatus == EntityStatus.Active),
                     x => x.RoleDefinition,
                     (x, y) => new { RoleDefinition = y, PermissionRoleDefinition = x })
                 .Join(
                    DatabaseAccessor.Query<ModuleOrganization>()
                        .Where(x => !x.IsDeleted && x.EntityStatus == EntityStatus.Active),
                     x => x.RoleDefinition.Scope,
                     x => x.Organization,
                     (up, u) => new TimelessAssignmentViewModel
                     {
                         Id = up.PermissionRoleDefinition.Id,
                         Element = new DetailedEntityViewModel
                         {
                             Id = u.Id,
                             DisplayName = up.RoleDefinition.DisplayName,
                             DateStart = u.DateStart,
                             DateEnd = u.DateEnd
                         }
                     })
             .AsQueryable();

            return await FinalizeQueryAsync(set);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        public TableConfiguration GetUserAssignmentColumns()
        {
            return GetAssignmentColumns(typeof(User));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), ResponseType(typeof(IQueryable<AssignmentViewModel>))]
        public async Task<IHttpActionResult> GetUsersWithPermission(
            int id)
        {
            var permissionMarker = new PermissionMarker(id);
            var set = DatabaseAccessor.Query<PermissionUser>()
                 .Where(x => x.Permission.IsEquivalent(permissionMarker) && !x.IsDeleted)
                 .Join(
                    DatabaseAccessor.Query<User>()
                        .Where(x => !x.IsDeleted && x.EntityStatus == EntityStatus.Active),
                     x => x.User,
                     (x, y) => new { User = y, PermissionRoleDefinition = x })
                 .Join(
                    DatabaseAccessor.Query<ModuleOrganization>()
                        .Where(x => !x.IsDeleted && x.EntityStatus == EntityStatus.Active),
                     x => x.User.Scope,
                     x => x.Organization,
                     (up, u) => new TimelessAssignmentViewModel
                     {
                         Id = up.PermissionRoleDefinition.Id,
                         Element = new DetailedEntityViewModel
                         {
                             Id = u.Id,
                             DisplayName = up.User.DisplayName,
                             DateStart = u.DateStart,
                             DateEnd = u.DateEnd
                         }
                     })
             .AsQueryable();

            return await FinalizeQueryAsync(set);
        }

        private static void AddDetailsColumn(TableConfigurationBuilder<PermissionAssignmentViewModel> builder)
        {
            var idPath = ExpressionExtensions.GetCamelCasePropertyPath((IStorable x) => x.Id);

            var column = new ColumnDefinition
            {
                ColumnHeaderTemplate = string.Empty,
                Template =
                    string.Format(
                        CultureInfo.InvariantCulture,
                        @"<bss-actions kind='Details' entities='{0}' id='item.{1}' ng-if='item.{1} > 0' ></bss-actions>",
                        typeof(Permission).Name,
                        idPath),
                IsActionsColumn = true,
                Width = 32
            };
            builder.Add(new ColumnProvider(column));
        }

        private static TableConfiguration GetAssignmentColumns(Type relatedType)
        {
            var builder = new TableConfigurationBuilder<TimelessAssignmentViewModel>();
            builder.AddDetailsActionColumn(relatedType, x => x.Element.Id);
            builder.AddColumn(x => x.Element.DisplayName, relatedType)
                .CanSort()
                .SortedByDefault()
                .CanFilter();

            return builder.Build();
        }
    }
}