﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsCommon.Containers.User)]
    public class UserController
        : EditScopedApiController<User, UserEditModel, IUser, UserViewModel>
    {
        private readonly IAssignmentService _assignmentService;
        private readonly IUserService _userService;

        public UserController(
            ISessionDataProvider sessionDataProvider,
            IDatabaseAccessor databaseAccessor,
            IQueryStringParser queryStringParser,
            IUserService userService,
            IAssignmentService assignmentService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, userService, permissionHelper, settingService)
        {
            _userService = userService;
            _assignmentService = assignmentService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword(
          ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var serviceModel = new ChangePasswordModel()
            {
                NewPassword = model.NewPassword,
                NewPasswordRepeated = model.NewPasswordRepeated,
                Password = model.Password
            };

            var validationErrors = await _userService.ChangePasswordAsync(serviceModel);

            return ProcessValidationErrors(validationErrors);
        }

        [RequiredPermission(PermissionsCommon.Keys.Details)]
        [ResponseType(typeof(IEnumerable<PermissionAssignmentViewModel>))]
        public async Task<IHttpActionResult> GetAssignedPermissions(
            int id)
        {
            var query = DatabaseAccessor.GetUserPermissions(new UserMarker(id))
                .Select(x => new PermissionAssignmentViewModel
                {
                    Id = x.Id,
                    JunctionId = x.JunctionId,
                    Key = x.Key,
                    ParentId = x.ParentId,
                    IsContainer = x.IsContainer,
                })
                .AsQueryable();

            return await FinalizeQueryAsync(query);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<UserViewModel>();
            builder.AddDefaultActionsColumn(PermissionsCommon.User.Details.Write.Path, PermissionsCommon.User.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Surname)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.FirstName)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.Mail)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.Phone)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.LoginName)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.Location)
                .CanSort()
                .CanFilter();
            return builder.Build();
        }

        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Containers.Assignment + PermissionsCommon.Keys.CommonPermission)]
        [ResponseType(typeof(IEnumerable<ValidationError>))]
        public async Task<IHttpActionResult> PostAssignedPermissions(
            HttpRequestMessage request,
            int id,
           [FromBody]IEnumerable<PermissionAssignmentPersistenceViewModel> assignments)
        {
            var user = new UserMarker(id);
            var newItemId = 0;
            var editModel = assignments
                .Select(x => new JunctionUdt<IPermission>
                {
                    Id = x.JunctionId ?? --newItemId,
                    Element = new PermissionMarker(x.Id)
                }).ToArray();

            var validationErrors = await _assignmentService.UpdateAssignmentAsync(user, editModel);
            return ProcessValidationErrors(validationErrors);
        }

        protected override async Task<UserEditModel> PrepareEditModelAsync(UserViewModel model)
        {
            if (model.Id != 0)
            {
                var exists = DatabaseAccessor.Query<User>()
               .Any(x =>
                   x.Scope.IsEquivalent(SessionDataProvider.Scope)
                   && x.IsEquivalent<IUser>(model));

                if (!exists)
                {
                    return null;
                }
            }

            return new UserEditModel
            {
                Id = model.Id,
                FirstName = model.FirstName,
                DisplayName = model.DisplayName,
                Location = model.Location,
                LoginName = model.LoginName,
                Mail = model.Mail,
                Phone = model.Phone,
                Surname = model.Surname,
                DateEnd = await ConvertToDateAsync(model.DateEnd),
                DateStart = await ConvertToDateAsync(model.DateStart),
            };
        }

        protected override ITableQuery<UserViewModel> ProjectToViewModel(ITableQuery<User> query)
        {
            return query
                .LeftJoin(
                    DatabaseAccessor.Query<Login>().Where(x => !x.IsDeleted),
                    x => x,
                    x => x.User,
                    GetUserViewModelSelector());
        }

        private static Expression<Func<User, Login, UserViewModel>> GetUserViewModelSelector()
        {
            return (u, l) => new UserViewModel
            {
                Id = u.Id,
                DisplayName = u.DisplayName,
                FirstName = u.FirstName,
                Surname = u.Surname,
                Location = u.Location,
                Phone = u.Phone,
                LoginName = l.LoginName,
                Mail = u.Mail,
                DateEnd = u.DateEnd,
                DateStart = u.DateStart,
                IsAlias = u.IsAlias
            };
        }
    }
}