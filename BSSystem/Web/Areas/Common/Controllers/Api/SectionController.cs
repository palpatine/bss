﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsCommon.Containers.Section)]
    public class SectionController
        : EditScopedApiController<Section, ISection, SectionViewModel>
    {
        public SectionController(
            ISessionDataProvider sessionDataProvider,
            IDatabaseAccessor databaseAccessor,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<Section, ISection> sectionService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, sectionService, permissionHelper, settingService)
        {
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<SectionViewModel>();
            builder.AddDefaultActionsColumn(PermissionsCommon.Section.Details.Write.Path, PermissionsCommon.Section.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .IsExpansionColumn();
            builder.AddColumn(x => x.Name);

            return builder.Build();
        }

        protected override async Task<Section> PrepareEditModelAsync(SectionViewModel model)
        {
            if (model.Id == 0)
            {
                var section = new Section
                {
                    DisplayName = model.DisplayName,
                    Name = model.Name,
                    DateEnd = await ConvertToDateAsync(model.DateEnd),
                    DateStart = await ConvertToDateAsync(model.DateStart),
                    Scope = SessionDataProvider.Scope
                };

                if (model.Parent != null)
                {
                    section.Parent = new SectionMarker(model.Parent.Id);
                }

                return section;
            }
            else
            {
                var section = DatabaseAccessor.Query<Section>()
                .SingleOrDefault(x =>
                    x.Scope.IsEquivalent(SessionDataProvider.Scope)
                    && x.IsEquivalent<ISection>(model));

                if (section == null)
                {
                    return null;
                }

                section.DisplayName = model.DisplayName;
                section.Name = model.Name;
                section.DateEnd = await ConvertToDateAsync(model.DateEnd);
                section.DateStart = await ConvertToDateAsync(model.DateStart);

                if (model.Parent != null)
                {
                    section.Parent = new SectionMarker(model.Parent.Id);
                }

                return section;
            }
        }

        protected override ITableQuery<SectionViewModel> ProjectToViewModel(ITableQuery<Section> query)
        {
            return query.LeftJoin(
                DatabaseAccessor.Query<Section>(),
                s => s.Parent,
                ps => ps,
                (x, ps) => new SectionViewModel
                {
                    Id = x.Id,
                    DisplayName = x.DisplayName,
                    Name = x.Name,
                    Parent = new NamedViewModel
                    {
                        Id = x.Parent.Id,
                        DisplayName = ps.DisplayName
                    },
                    DateStart = x.DateStart,
                    DateEnd = x.DateEnd
                });
        }
    }
}