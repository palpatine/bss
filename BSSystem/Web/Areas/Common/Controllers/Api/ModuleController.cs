﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsCommon.Containers.Organization)]
    public class ModuleController : BaseApiController
    {
        public ModuleController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, settingService)
        {
        }

        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public async Task<IHttpActionResult> Get()
        {
            var query = DatabaseAccessor.Query<Module>()
                .Where(x => x.Key != "Integration")
              .Select(
                      (x) => new ModuleViewModel
                      {
                          Id = x.Id,
                          Key = x.Key,
                          Required = x.Required
                      })
               .AsQueryable();

            return await FinalizeQueryAsync(query);
        }
    }
}