﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsCommon.Containers.CustomFieldDefinition)]
    public class CustomFieldDefinitionController
        : EditScopedApiController<CustomFieldDefinition, CustomFieldDefinitionEditModel, ICustomFieldDefinition, CustomFieldDefinitionViewModel>
    {
        private readonly ICustomFieldDefinitionService _customFieldDefinitionService;

        public CustomFieldDefinitionController(
            ISessionDataProvider sessionDataProvider,
            IDatabaseAccessor databaseAccessor,
            IQueryStringParser queryStringParser,
            ICustomFieldDefinitionService customFieldDefinitionService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, customFieldDefinitionService, permissionHelper, settingService)
        {
            _customFieldDefinitionService = customFieldDefinitionService;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        [RequiredPermission(PermissionsCommon.Keys.Write)]
        [ResponseType(typeof(ApplicableTerminalViewModel))]
        [HttpGet]
        public IHttpActionResult GetApplicableTerminals()
        {
            var applicableTerminal = new ApplicableTerminalViewModel();
            applicableTerminal.ApplicableDescriptors = _customFieldDefinitionService.ApplicableTerminals
                .Select(x => new ApplicableDescriptor
                {
                    Key = x.Key.Key,
                    TerminalDescriptors = x.Value.Select(y => new TerminalDescriptorViewModel
                    {
                        DefaultValue = y.DefaultValue,
                        Description = y.Description(),
                        Name = y.Name(),
                        Template = y.Template
                    })
                });

            return Ok(applicableTerminal);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<CustomFieldDefinitionViewModel>();
            builder.AddDefaultActionsColumn(PermissionsCommon.CustomFieldDefinition.Details.Write.Path, PermissionsCommon.CustomFieldDefinition.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.TableKey)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.Schema)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateEnd)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.IsUnique)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.IsRequired)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.IsEditable)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.IsNullable)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.IsInViews)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        [RequiredPermission(PermissionsCommon.Keys.Write)]
        [ResponseType(typeof(IQueryable<TargetViewModel>))]
        public async Task<IHttpActionResult> GetSignTargets()
        {
            var query = _customFieldDefinitionService.SignableTargets.Select(x => new TargetViewModel
                {
                    Id = x.CustomFieldTarget.Key,
                }).AsQueryable();

            return await FinalizeQueryAsync(query);
        }

        protected override CustomFieldDefinitionEditModel PrepareEditModel(CustomFieldDefinitionViewModel model)
        {
            throw new NotImplementedException();
        }

        protected override ITableQuery<CustomFieldDefinitionViewModel> ProjectToViewModel(ITableQuery<CustomFieldDefinition> query)
        {
            return query.Select(x => new CustomFieldDefinitionViewModel
            {
                IsEditable = x.IsEditable,
                DisplayName = x.DisplayName,
                IsInViews = x.IsInViews,
                IsNullable = x.IsNullable,
                IsRequired = x.IsRequired,
                IsUnique = x.IsUnique,
                Schema = x.Schema,
                TableKey = x.TableKey,
                DateStart = x.DateStart,
                DateEnd = x.DateEnd,
                Id = x.Id
            });
        }
    }
}