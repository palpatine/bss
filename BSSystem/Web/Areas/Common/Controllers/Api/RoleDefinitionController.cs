using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Enumerations;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Functions;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;

namespace SolvesIt.BSSystem.Web.Areas.Common.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsCommon.Containers.RoleDefinition)]
    public class RoleDefinitionController
        : EditScopedApiController<RoleDefinition, RoleDefinition, IRoleDefinition, RoleDefinitionViewModel>
    {
        private readonly IAssignmentService _assignmentService;
        private readonly IRoleDefinitionService _roleDefinitionService;
        private readonly ISessionDataProvider _sessionDataProvider;

        public RoleDefinitionController(
            ISessionDataProvider sessionDataProvider,
            IDatabaseAccessor databaseAccessor,
            IQueryStringParser queryStringParser,
            IRoleDefinitionService roleDefinitionService,
            IAssignmentService assignmentService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, roleDefinitionService, permissionHelper, settingService)
        {
            _sessionDataProvider = sessionDataProvider;
            _roleDefinitionService = roleDefinitionService;
            _assignmentService = assignmentService;
        }

        [RequiredPermission(PermissionsCommon.Keys.Details)]
        [ResponseType(typeof(IEnumerable<PermissionAssignmentViewModel>))]
        public async Task<IHttpActionResult> GetAssignedPermissions(
            int id)
        {
            var query = DatabaseAccessor.GetRolePermissions(new RoleDefinitionMarker(id))
                .Select(x => new PermissionAssignmentViewModel
                {
                    Id = x.Id,
                    JunctionId = x.JunctionId,
                    Key = x.Key,
                    ParentId = x.ParentId,
                    IsContainer = x.IsContainer
                })
                .AsQueryable();

            return await FinalizeQueryAsync(query);
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsCommon.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<RoleDefinitionViewModel>();
            builder.AddDefaultActionsColumn(PermissionsCommon.RoleDefinition.Details.Write.Path, PermissionsCommon.RoleDefinition.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Target.DisplayName, (RoleDefinition d) => d.TableKey)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateEnd)
                .CanSort()
                .CanFilter();
            return builder.Build();
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Keys.Write)]
        public async Task<IHttpActionResult> GetRoleTargets()
        {
            var modulesAvailable = await DatabaseAccessor
                .Query<ModuleOrganization>()
                .Where(
                    x =>
                        !x.IsDeleted && x.EntityStatus == EntityStatus.Active &&
                        x.Organization.IsEquivalent(_sessionDataProvider.Scope))
                .Join(DatabaseAccessor.Query<Module>(), x => x.Module, (om, m) => new { ModuleKey = m.Key })
                .ToListAsync();

            var query = _roleDefinitionService
                .RoleTargets
                .Where(x => modulesAvailable.Any(z => x.RoleTarget.Key.StartsWith(z.ModuleKey)))
                .Select(x => new TargetViewModel
                {
                    Id = x.RoleTarget.Key
                })
                .AsQueryable();

            return await FinalizeQueryAsync(query);
        }

        [RequiredPermission(PermissionsCommon.Keys.Details + PermissionsCommon.Containers.Assignment + PermissionsCommon.Keys.CommonPermission)]
        [ResponseType(typeof(IEnumerable<ValidationError>))]
        public async Task<IHttpActionResult> PostAssignedPermissions(
            HttpRequestMessage request,
            int id,
            [FromBody] IEnumerable<PermissionAssignmentPersistenceViewModel> assignments)
        {
            var roleDefinition = new RoleDefinitionMarker(id);
            var newItemId = 0;
            var editModel = assignments
                .Select(x => new JunctionUdt<IPermission>
                {
                    Id = x.JunctionId ?? --newItemId,
                    Element = new PermissionMarker(x.Id)
                }).ToArray();

            var validationErrors = await _assignmentService.UpdateAssignmentAsync(roleDefinition, editModel);
            return ProcessValidationErrors(validationErrors);
        }

        protected override async Task<RoleDefinition> PrepareEditModelAsync(RoleDefinitionViewModel model)
        {
            if (model.Id == 0)
            {
                var roleDefintion = new RoleDefinition
                {
                    Comment = model.Comment,
                    TableKey = model.Target.Id,
                    DateStart = await ConvertToDateAsync(model.DateStart),
                    DateEnd = await ConvertToDateAsync(model.DateEnd),
                    DisplayName = model.DisplayName,
                    Scope = _sessionDataProvider.Scope
                };

                return roleDefintion;
            }
            else
            {
                var roleDefintion = DatabaseAccessor.Query<RoleDefinition>()
                    .SingleOrDefault(x => x.IsEquivalent<IRoleDefinition>(model) && x.Scope.IsEquivalent(_sessionDataProvider.Scope));

                if (roleDefintion == null)
                {
                    return null;
                }

                roleDefintion.Comment = model.Comment;
                roleDefintion.DisplayName = model.DisplayName;
                roleDefintion.TableKey = model.Target.Id;
                roleDefintion.DateStart = await ConvertToDateAsync(model.DateStart);
                roleDefintion.DateEnd = await ConvertToDateAsync(model.DateEnd);
                return roleDefintion;
            }
        }

        protected override ITableQuery<RoleDefinitionViewModel> ProjectToViewModel(ITableQuery<RoleDefinition> query)
        {
            return query
                .Select(x => new RoleDefinitionViewModel
                {
                    DisplayName = x.DisplayName,
                    Id = x.Id,
                    Target = new TargetViewModel
                    {
                        Id = x.TableKey
                    },
                    DateStart = x.DateStart,
                    DateEnd = x.DateEnd
                });
        }
    }
}