﻿using System.Web.Http;
using System.Web.Mvc;
using SolvesIt.BSSystem.Web.Environment.Api;

namespace SolvesIt.BSSystem.Web.Areas.Common
{
    public class CommonAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Common";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Common_mvc",
                "Common/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional });

            context.MapHttpRoute(
                name: "CommonCollectionApi",
                routeTemplate: "api/Common/List/{controller}/{action}",
                defaults: new { });

            var commonEntityApiParameters = new
            {
                id = RouteParameter.Optional,
                action = RouteParameter.Optional,
                actionid = RouteParameter.Optional,
                subaction = RouteParameter.Optional,
                subactionid = RouteParameter.Optional
            };
            context.MapHttpRoute(
                 name: "CommonEntityApi",
                 routeTemplate: "api/Common/{controller}/{id}/{action}/{actionid}/{subaction}/{subactionid}",
                 defaults: commonEntityApiParameters);
        }
    }
}