﻿module SolvesIt.Bss.Common.Positions {
    export interface IPositionViewModel extends SolvesIt.Bss.Models.IDetailedMarker {
        name: string;
        dateStart: Date;
        dateEnd: Date;
    }
} 