﻿module SolvesIt.Bss.Common.Positions {
    export class PositionViewModel implements IPositionViewModel {
        id: number;
        displayName: string;
        name: string;
        dateStart: Date;
        dateEnd: Date;
    }
} 