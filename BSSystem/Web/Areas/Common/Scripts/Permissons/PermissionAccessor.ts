﻿module SolvesIt.Bss.Common.Permissions {
    class PermissionAccessor extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IPermissionViewModel> implements IPermissionAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.permissions);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("permissionAccessor", PermissionAccessor);
} 