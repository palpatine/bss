module SolvesIt.Bss.Common.Rights {
    class RightsGroupController {
        static sectionAddress(): string {
            return "/Administration/RightsAdministration";
        }

        public getSectionAddress(): string { return RightsGroupController.sectionAddress(); }

        public static $inject = [
            '$scope',
        ];

        constructor(
            $scope) {
            $scope.tableConfiguration = {
                columnDefinition: [
                    {
                        columnHeaderDisplayName: "",
                        template: "<bss-actions kind='Assign' entities='Rights' id='item.id'/> ",
                        isActionsColumn: true
                    },
                    {
                        columnHeaderDisplayName: 'Name',
                        displayProperty: 'name',
                        sortKey: 'Name'
                    },
                    {
                        columnHeaderDisplayName: 'Description',
                        displayProperty: 'description',
                        sortKey: 'Description'
                    }
                ]
            };

            $scope.ajaxConfig = {
                url: '/api/Rights',
                method: 'GET',
                params: {
                }
            };
        }
    }

    SolvesIt.Bss.Application.getApplication().config(
        [
            '$routeProvider',
            ($routeProvider) => {
                $routeProvider
                    .when(RightsGroupController.sectionAddress(), {
                        controller: RightsGroupController,
                        templateUrl: "~/Rights/List"
                    });
            }
        ]);
}