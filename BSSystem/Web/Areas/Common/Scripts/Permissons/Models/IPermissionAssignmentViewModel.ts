﻿module SolvesIt.Bss.Common.Permissions {
    export interface IPermissionAssignmentViewModel extends IPermissionViewModel, SolvesIt.Widgets.Structures.IHierarchicalElement {
        isAssigned: boolean;
        junctionId: number;
        children: IPermissionAssignmentViewModel[];
        hasChildren: boolean;
    }

    export interface IPermissionAssignmentPersistanceViewModel {
        id: number;
        junctionId: number;
    }

    export class PermissionsStructure extends SolvesIt.Widgets.Structures.HierarchicalStructure<IPermissionAssignmentViewModel, IPermissionAssignmentPersistanceViewModel> {
        constructor(data: IPermissionAssignmentViewModel[]) {
            super(data);
            this.verifyDownHierarchy(this.items);
        }

        public permissionChanged(item: SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel) {
            if (item.isContainer) {
                if (item.isAssigned === null) {
                    item.isAssigned = false;
                }
            }

            if (item.children && (item.isContainer || !item.isAssigned)) {
                this.setPermissions(item.children, item.isAssigned);
            }

            if (item.parentId != null) {
                this.verifyUpHierarchy(this.itemsById[item.parentId], item.isAssigned);
            }
        }

        public getPersistanceData(): IPermissionAssignmentPersistanceViewModel[] {
            var elements = _.chain(this.all).filter((item) => { return (item.isAssigned === true || item.isAssigned === null) && item.id > 0; })
                .map(item => {
                return {
                    id: item.id,
                    junctionId: item.junctionId
                };
            })
                .value();

            return elements;
        }

        private areAllChildPermissionsGranted(item: SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel): boolean {
            return _.all(item.children, el => {
                return el.isAssigned === true && (!el.hasChildren || this.areAllChildPermissionsGranted(el));
            });
        }

        private areAllChildPermissionsRevoked(item: SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel): boolean {
            return _.all(item.children, el => {
                return el.isAssigned === false && (!el.hasChildren || this.areAllChildPermissionsRevoked(el));
            });
        }
        private verifyDownHierarchy(items: SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel[]): boolean {
            var areAllAssigned = true;
            var areAllNotAssigned = true;

            _.each(items, (item) => {
                var value;
                if (item.children) {
                    if (item.isContainer) {
                        value = item.isAssigned = this.verifyDownHierarchy(item.children);
                    } else {
                        value = this.verifyDownHierarchy(item.children) === item.isAssigned ? item.isAssigned : null;
                    }
                } else {
                    value = item.isAssigned;
                }

                areAllAssigned = areAllAssigned && (value === true);
                areAllNotAssigned = areAllNotAssigned && (value === false);
            });

            // ReSharper disable ConditionIsAlwaysConst two false positives
            return areAllAssigned ? true : (areAllNotAssigned ? false : null);
            // ReSharper restore ConditionIsAlwaysConst
        }

        private verifyUpHierarchy(item: SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel, value: boolean) {
            if (item.isContainer) {
                if (this.areAllChildPermissionsGranted(item))
                    item.isAssigned = true;
                else if (this.areAllChildPermissionsRevoked(item)) {
                    item.isAssigned = false;
                } else {
                    item.isAssigned = null;
                }
            } else if (value == true) {
                item.isAssigned = true;
            }

            if (item.parentId != null) {
                this.verifyUpHierarchy(this.itemsById[item.parentId], value);
            }
        }

        private setPermissions(items: SolvesIt.Bss.Common.Permissions.IPermissionAssignmentViewModel[], value: boolean) {
            _.each(items, item => {
                item.isAssigned = value;
                if (item.hasChildren) {
                    this.setPermissions(item.children, value);
                }
            });
        }
    }
}