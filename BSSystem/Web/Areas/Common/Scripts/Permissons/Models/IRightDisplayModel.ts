﻿module SolvesIt.Bss.Common.Rights {
    export interface IRightDisplayModel {
        canBeElevated: boolean;

        isElevated: boolean;

        displayName: string;
    }
}  