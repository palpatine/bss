﻿module SolvesIt.Bss.Common.Permissions {
    export interface IPermissionViewModel {
        id: number;

        parentId: number;

        key: string;

        displayName: string;

        isContainer: boolean;
    }
}