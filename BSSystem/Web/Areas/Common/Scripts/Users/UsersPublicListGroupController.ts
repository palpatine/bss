module SolvesIt.Bss.Common.Users {
    class UsersPublicListGroupController {
        static sectionAddress(): string {
            return "/Lists/UsersPublicList";
        }

        public getSectionAddress(): string { return UsersPublicListGroupController.sectionAddress(); }

        public static $inject = [
            '$scope',
            'routeFactory'
        ];

        constructor(
            $scope,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory) {
            $scope.tableConfiguration = {
                columnDefinition: [
                    {
                        columnHeaderTemplate: "",
                        template: "<bss-actions kind='Details' entities='Users' id='item.id'/> ",
                        isActionsColumn: true
                    },
                    {
                        columnHeaderDisplayName: 'Surname',
                        displayProperty: 'surname',
                        sortKey: 'surname',
                        sorted: true
                    },
                    {
                        columnHeaderDisplayName: 'FirstName',
                        displayProperty: 'firstName',
                        sortKey: 'firstName'
                    },
                    {
                        columnHeaderDisplayName: 'Mail',
                        displayProperty: 'mail',
                        sortKey: 'mail'
                    },
                    {
                        columnHeaderDisplayName: 'Phone',
                        displayProperty: 'phone',
                        sortKey: 'phone'
                    },
                    {
                        columnHeaderDisplayName: 'Login',
                        displayProperty: 'loginName',
                        sortKey: 'loginName'
                    },
                    {
                        columnHeaderDisplayName: 'PositionDisplayName',
                        template: "<bss-actions kind='Details' entities='Positions' id='item.positionId'></bss-actions>  {{item.positionDisplayName}}",
                        sortKey: 'positionDisplayName'
                    },
                    {
                        columnHeaderDisplayName: 'SectionDisplayName',
                        template: "<bss-actions kind='Details' entities='Sections' id='item.sectionId'></bss-actions> {{item.sectionDisplayName}}",
                        sortKey: 'sectionDisplayName'
                    },
                    {
                        columnHeaderDisplayName: 'Location',
                        displayProperty: 'Location',
                        sortKey: 'Location'
                    }
                ]
            };

            $scope.ajaxConfig = {
                url: routeFactory.getOdListFor(SolvesIt.Bss.Entities.users),
                method: 'GET',

                params: {
                }
            };
        }
    }
}