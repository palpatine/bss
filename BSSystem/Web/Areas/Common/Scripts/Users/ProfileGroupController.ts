module SolvesIt.Bss.Common.Users {
    class ProfileGroupController extends SolvesIt.Bss.Core.BreadcrumbsSection {
        static sectionAddress(): string {
            return "/Profile/Profile";
        }

        public getSectionAddress(): string { return ProfileGroupController.sectionAddress(); }

        public static $inject = [
            '$scope',
            'globalStateService',
            '$http',
            'coreBackendAccessor',
            'routeFactory',
            '$q',
            'stateCacheService'
        ];

        constructor(
            private $scope,
            private globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService,
            $http: ng.IHttpService,
            coreBackendAccessor: SolvesIt.Bss.Core.ApplicationManagement.IBackendAccessor,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            private $q: ng.IQService,
            private stateCacheService: SolvesIt.Widgets.IStateCacheService) {
            super($scope, $q, globalStateService, stateCacheService);

            //coreBackendAccessor.getApplicationManagement()
            //    .success((applicationDescriptor) => {
            //    $http({
            //        method: "GET",
            //        url: routeFactory.getOdDetailsFor(SolvesIt.Bss.Entities.users, applicationDescriptor.userData.user.id)
            //    })
            //        .then(result => {
            //        $scope.model = result.data;
            //    });
            //});
        }

        public cancel() {
            this.globalStateService.removeSectionCard(this.$scope.section);
            event.preventDefault();
        }
    }

    SolvesIt.Bss.Application.getApplication().config(
        ['$routeProvider', 'routeFactoryProvider',
            ($routeProvider, routeFactory) => {
                routeFactory = routeFactory.$get();
                var entitityType = SolvesIt.Bss.Entities.users;
                $routeProvider
                    .when(ProfileGroupController.sectionAddress(), {
                        controller: ProfileGroupController,
                        controllerAs: 'vm',
                        templateUrl: routeFactory.getUserProfileTemplate()
                    });
            }
        ]);
} 