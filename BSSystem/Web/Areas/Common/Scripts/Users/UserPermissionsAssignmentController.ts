﻿module SolvesIt.Bss.Common.Users {
    class UserPermissionsAssignmentController extends SolvesIt.Bss.Core.Controllers.PermissionsAssignmentController {
        static $inject = [
            'stateCacheService',
            '$scope',
            'userAccessor',
            'globalStateService'
        ];

        constructor(
            stateCacheService: SolvesIt.Widgets.IStateCacheService,
            $scope,
            accessor: IUserAccessor,
            globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {
            super(
                stateCacheService,
                "userPermissionsAssignmentController",
                "Common_User|Details",
                "Common_User|Details|Assignment|Common_Permission",
                $scope,
                accessor,
                globalStateService);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller("userPermissionsAssignmentController", UserPermissionsAssignmentController);
}                    