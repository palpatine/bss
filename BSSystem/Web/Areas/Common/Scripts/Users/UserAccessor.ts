﻿module SolvesIt.Bss.Common.Users {
    class UserAccessor extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IUserViewModel> implements IUserAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'

        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.users);
        }

        public detailsFor(id: number): ng.IHttpPromise<IDetailedUserViewModel> {
            return this.$http.get(this.routeFactory.getOdDetailsFor(SolvesIt.Bss.Entities.users, id));
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("userAccessor", UserAccessor);
} 