﻿module SolvesIt.Bss.Common.Users {
    export interface IUserAccessor extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<IUserViewModel>, Core.Accessors.IPermissionsRelatedAccessor {
    }
}