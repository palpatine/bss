﻿module SolvesIt.Bss.Common.Users {
    class ChangePasswordGroupController extends SolvesIt.Bss.Core.BreadcrumbsSection {

        static sectionAddress(): string {
            return "/Profile/ChangePassword";
        }

        public getSectionAddress(): string { return ChangePasswordGroupController.sectionAddress(); }

        public static $inject = [
            '$scope',
            'globalStateService',
            '$http',
            'routeFactory',
            '$q',
            'stateCacheService',
            '$modal'
        ];

        constructor(
            private $scope,
            private globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService,
            private $http: ng.IHttpService,
            private routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            private $q: ng.IQService,
            private stateCacheServcie: SolvesIt.Widgets.IStateCacheService,
            private $modal: angular.ui.bootstrap.IModalService) {
            super($scope, $q, globalStateService, stateCacheServcie);
            $scope.vm = this;
            $scope.model = {};
        }

        public save() {
            event.preventDefault();

            this.$http.post<SolvesIt.Bss.Models.IValidationResult>(
                this.routeFactory.getChangePasswordTarget(),
                this.$scope.model)
                .success(data => {
                    if (data.isValid) {

                        this.$modal.open({
                            animation: true,
                            templateUrl: '~/Content/Templates/MessageBox/Main.tpl.html',
                            controller: 'messageBoxController',
                            controllerAs: "vm",
                            size: 'sm',
                            resolve: {
                                message: () => {
                                    return "Hasło zostało zmienione.";
                                }
                            }
                        });

                        this.globalStateService.removeSectionCard(this.$scope.section);
                        return;
                    }

                    this.$scope.validationErrors = {};
                    _.each(data.errors, error=> {
                        this.$scope.validationErrors[error.reference] = error.message;
                        (<ng.INgModelController>this.$scope.form[error.reference]).$setValidity("server", false);
                    });
                });
        }

        public cancel() {
            this.globalStateService.removeSectionCard(this.$scope.section);
            event.preventDefault();
        }
    }

    SolvesIt.Bss.Application.getApplication().config(
        ['$routeProvider', 'routeFactoryProvider',
            ($routeProvider, routeFactory) => {
                routeFactory = routeFactory.$get();
                $routeProvider
                    .when(ChangePasswordGroupController.sectionAddress(), {
                        controller: ChangePasswordGroupController,
                        templateUrl: routeFactory.getChangePasswordTemplateFor()
                    });
            }
        ]);
}