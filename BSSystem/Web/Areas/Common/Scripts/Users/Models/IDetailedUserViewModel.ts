﻿module SolvesIt.Bss.Common.Users {
    export interface IDetailedUserViewModel extends IUserViewModel {
        sectionId: number;

        positionId: number;

        sectionDisplayName: string;

        positionDisplayName: string;
    }
} 