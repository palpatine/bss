﻿module SolvesIt.Bss.Common.Users {
    export interface IUserViewModel extends SolvesIt.Bss.Models.IDetailedMarker {
        jobTime: number;

        loginName: string;

        mail: string;

        phone: string;

        location: string;

        dateStart: Date;

        dateEnd: Date;

        firstName: string;

        surname: string;
    }
} 