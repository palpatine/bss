﻿module SolvesIt.Bss.Common.Users {
    export class UserViewModel implements IUserViewModel {
        id: number;

        displayName: string;

        jobTime: number;

        loginName: string;

        mail: string;

        phone: string;

        location: string;

        dateStart: Date;

        dateEnd: Date;

        firstName: string;

        surname: string;
    }
} 