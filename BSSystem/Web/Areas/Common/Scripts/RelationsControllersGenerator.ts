﻿module SolvesIt.Bss.Common {
    (() => {
        var generator = new SolvesIt.Bss.Core.ControllerGenerators.GenericRelationGenerator();
        var entities = [
            {
                master: SolvesIt.Bss.Entities.positions,
                slave: SolvesIt.Bss.Entities.users
            },
            {
                master: SolvesIt.Bss.Entities.sections,
                slave: SolvesIt.Bss.Entities.users
            },
            {
                master: SolvesIt.Bss.Entities.users,
                slave: SolvesIt.Bss.Entities.positions
            },
            {
                master: SolvesIt.Bss.Entities.users,
                slave: SolvesIt.Bss.Entities.sections
            },
            {
                master: SolvesIt.Bss.Entities.organizations,
                slave: SolvesIt.Bss.Entities.modules
            }
        ];
        _.each(entities, (entity) => {
            generator.createEditor(SolvesIt.Bss.Common, entity.master, entity.slave);
        });
    })();
} 