﻿module SolvesIt.Bss.Common {
    (() => {
        var detailsGenerator = new SolvesIt.Bss.Core.ControllerGenerators.GenericDetailsGenerator();
        var detailsEntities = [
            SolvesIt.Bss.Entities.organizations,
            SolvesIt.Bss.Entities.permissions,
            SolvesIt.Bss.Entities.positions,
            SolvesIt.Bss.Entities.roleDefinitions,
            SolvesIt.Bss.Entities.sections,
            SolvesIt.Bss.Entities.signDefinitions,
            SolvesIt.Bss.Entities.users
        ];
        _.each(detailsEntities, (entity) => {
            detailsGenerator.createEditor(SolvesIt.Bss.Common, entity);
        });

        var editorsGenerator = new SolvesIt.Bss.Core.ControllerGenerators.GenericEditorsGenerator();
        var editorsEntities = [
            SolvesIt.Bss.Entities.organizations,
            SolvesIt.Bss.Entities.permissions,
            SolvesIt.Bss.Entities.positions,
            SolvesIt.Bss.Entities.roleDefinitions,
            SolvesIt.Bss.Entities.sections,
            SolvesIt.Bss.Entities.signDefinitions,
            SolvesIt.Bss.Entities.users
        ];
        _.each(editorsEntities, (entity) => {
            editorsGenerator.createEditor(SolvesIt.Bss.Common, entity);
        });
    })();
} 