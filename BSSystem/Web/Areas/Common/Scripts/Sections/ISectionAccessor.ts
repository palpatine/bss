﻿module SolvesIt.Bss.Common.Sections {
    export interface ISectionAccessor extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<ISectionViewModel> {
        getOrganizationStructureLevel(parentId?: number): ng.IPromise<SolvesIt.Bss.Models.IDetailedMarker[]>;
        getActualOrganizationStructureLevel(parentId?: number): ng.IPromise<SolvesIt.Bss.Models.IDetailedMarker[]>;
    }
}