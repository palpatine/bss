﻿module SolvesIt.Bss.Common.Sections {
    export class SectionViewModel implements SolvesIt.Bss.Models.IDetailedMarker {
        id: number;

        displayName: string;

        name: string;

        parent: SolvesIt.Bss.Models.INamedMarker;

        dateStart: Date;

        dateEnd: Date;
    }
} 