﻿module SolvesIt.Bss.Common.Sections {
    export interface ISectionViewModel extends SolvesIt.Bss.Models.IDetailedMarker {
        name: string;

        parent: SolvesIt.Bss.Models.INamedMarker;

        dateStart: Date;

        dateEnd: Date;
    }
} 