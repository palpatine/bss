﻿module SolvesIt.Bss.Common.Sections {
    class SectionAccessor extends SolvesIt.Bss.Core.Accessors.EntityAccessor<ISectionViewModel> implements ISectionAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor',
            'oDataQueryParametersGeneratorFactory'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor,
            private oDataQueryParametersGeneratorFactory: SolvesIt.Widgets.QueryString.IODataQueryParametersGeneratorFactory) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.sections);
        }

        public detailsFor(id: number): ng.IHttpPromise<ISectionViewModel> {
            return this.$http.get(this.routeFactory.getOdDetailsFor(SolvesIt.Bss.Entities.sections, id));
        }

        public getOrganizationStructureLevel(parentId?: number): ng.IPromise<SolvesIt.Bss.Models.IDetailedMarker[]> {
            var generator = this.oDataQueryParametersGeneratorFactory.create();
            generator.where()
                .property(parentId ? "parent.id" : "parent")
                .eq(parentId || null);

            var query = this.oDataQuerySerializer.serialize(generator.generate());
            var url = this.routeFactory.getOrganizationStructureLevel();
            return this.getCollectionFromODataRoute<SolvesIt.Bss.Models.IDetailedMarker>(url, query);
        }

        public getActualOrganizationStructureLevel(parentId?: number): ng.IPromise<SolvesIt.Bss.Models.IDetailedMarker[]> {
            var generator = this.oDataQueryParametersGeneratorFactory.create();
            generator.where()
                .property(parentId ? "parent.id" : "parent")
                .eq(parentId || null)
                .and()
                .openGroup()
                .property("dateStart")
                .lt(new Date())
                .and()
                .property("dateEnd")
                .gtOrNull(new Date())
                .closeGroup();
            var query = this.oDataQuerySerializer.serialize(generator.generate());
            var url = this.routeFactory.getOrganizationStructureLevel();
            return this.getCollectionFromODataRoute<SolvesIt.Bss.Models.IDetailedMarker>(url, query);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("sectionAccessor", SectionAccessor);
} 