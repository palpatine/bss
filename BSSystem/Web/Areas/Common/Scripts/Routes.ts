﻿module SolvesIt.Bss.Common {
    var detailsEntitesSet = [
        SolvesIt.Bss.Entities.organizations,
        SolvesIt.Bss.Entities.users,
        SolvesIt.Bss.Entities.sections,
        SolvesIt.Bss.Entities.positions,
        SolvesIt.Bss.Entities.users,
        SolvesIt.Bss.Entities.signDefinitions,
        SolvesIt.Bss.Entities.roleDefinitions,
        SolvesIt.Bss.Entities.permissions
    ];

    var listEntitesSet = [
        SolvesIt.Bss.Entities.organizations,
        SolvesIt.Bss.Entities.users,
        SolvesIt.Bss.Entities.positions,
        SolvesIt.Bss.Entities.sections,
        SolvesIt.Bss.Entities.users,
        SolvesIt.Bss.Entities.signDefinitions,
        SolvesIt.Bss.Entities.roleDefinitions,
        SolvesIt.Bss.Entities.permissions
    ];

    _.each(detailsEntitesSet, (entities) => {
        SolvesIt.Bss.Application
            .route(
            routeFactory => { return routeFactory.getWhenDetailsUrlFor(entities); },
            routeFactory => {
                return {
                    templateUrl: routeFactory.getDetailsTemplateFor(entities),
                    controller: "breadcrumbsDetailsDisplayer"
                };
            });
    });

    _.each(listEntitesSet, (entities) => {
        SolvesIt.Bss.Application
            .route(
            "/Administration/" + entities,
            routeFactory => {
                return {
                    templateUrl: routeFactory.getDetailedListTemplateFor(entities),
                    controller: 'breadcrumbsSection'
                };
            });
    }); 
}