﻿module SolvesIt.Bss.Common.SignDefinitions {
    class SignDefinitionAccessor extends SolvesIt.Bss.Core.Accessors.EntityAccessor<ISignDefinitionViewModel> implements ISignDefinitionAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'

        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.signDefinitions);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("signDefinitionAccessor", SignDefinitionAccessor);
} 