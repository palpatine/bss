﻿module SolvesIt.Bss.Common.SignDefinitions {
    export interface ISignDefinitionViewModel {
    }

    export class SignDefinitionViewModel implements ISignDefinitionViewModel {
        id: number;

        displayName: string;

        dateStart: Date;

        dateEnd: Date;

        isRequired: boolean;

        isEditable: boolean;

        isInViews: boolean;

        isNullable: boolean;

        isUnique: boolean;

        schema: string;

        tableName: string;
    }
} 