﻿module SolvesIt.Bss.Common.Organizations {
    export interface IOrganizationAccessor
        extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<IOrganizationViewModel>, Core.Accessors.IPermissionsRelatedAccessor {
    }
} 