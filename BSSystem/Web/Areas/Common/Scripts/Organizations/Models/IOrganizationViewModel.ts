﻿module SolvesIt.Bss.Common.Organizations {
    export interface IOrganizationViewModel
        extends IOrganizationDetailedMarker {
        name: string;
        fullName: string;
        countryCode: string;
        vatNumber: string;
        officialNumber: string;
        statisticNumber: string;
        phone: number;
        fax: number;
        email: string;
    }
}