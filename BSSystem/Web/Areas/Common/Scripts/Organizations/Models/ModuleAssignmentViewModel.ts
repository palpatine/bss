﻿module SolvesIt.Bss.Common.Organizations {
    export interface IModuleAssignmentViewModel {
        displayName: string;
        id: number;
        key: string;
        required: string;
        dateStart: Date;
        dateEnd: Date;
    }
} 