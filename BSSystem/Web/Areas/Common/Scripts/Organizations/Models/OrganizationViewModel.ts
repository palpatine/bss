﻿module SolvesIt.Bss.Common.Organizations {
    export class OrganizationViewModel implements IOrganizationViewModel {
        id: number;

        displayName: string;

        dateStart: Date;

        dateEnd: Date;

        name: string;

        fullName: string;

        countryCode: string;

        vatNumber: string;

        officialNumber: string;

        statisticNumber: string;

        phone: number;

        fax: number;

        email: string;
    }
} 