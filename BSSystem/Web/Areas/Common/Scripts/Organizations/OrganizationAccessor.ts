﻿module SolvesIt.Bss.Common.Organizations {
    class OrganizationAccessor
        extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IOrganizationViewModel>
        implements IOrganizationAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.organizations);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("organizationAccessor", OrganizationAccessor);
}