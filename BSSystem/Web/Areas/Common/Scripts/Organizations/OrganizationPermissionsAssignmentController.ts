﻿module SolvesIt.Bss.Common.Organizations {
    class OrganizationPermissionsAssignmentController extends SolvesIt.Bss.Core.Controllers.PermissionsAssignmentController {
        static $inject = [
            'stateCacheService',
            '$scope',
            'organizationAccessor',
            'globalStateService'
        ];

        constructor(
            stateCacheService: SolvesIt.Widgets.IStateCacheService,
            $scope,
            accessor: IOrganizationAccessor,
            globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {
            super(
                stateCacheService,
                "organizationPermissionsAssignmentController",
                "Common_Organization|Details",
                "Common_Organization|Details|Assignment|Common_Permission",
                $scope,
                accessor,
                globalStateService);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller('organizationPermissionsAssignmentController', OrganizationPermissionsAssignmentController);
}