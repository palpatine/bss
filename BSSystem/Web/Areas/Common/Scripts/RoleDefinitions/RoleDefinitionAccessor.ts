﻿module SolvesIt.Bss.Common.RoleDefinitions {
    class RoleDefinitionAccessor extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IRoleDefinitionViewModel> implements IRoleDefinitionAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.roleDefinitions);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("roleDefinitionAccessor", RoleDefinitionAccessor);
} 