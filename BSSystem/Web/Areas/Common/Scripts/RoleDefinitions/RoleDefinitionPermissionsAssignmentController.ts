﻿module SolvesIt.Bss.Common.RoleDefinitions {
    class RoleDefinitionPermissionsAssignmentController extends SolvesIt.Bss.Core.Controllers.PermissionsAssignmentController {
        static $inject = [
            'stateCacheService',
            '$scope',
            'roleDefinitionAccessor',
            'globalStateService'
        ];

        constructor(
            stateCacheService: SolvesIt.Widgets.IStateCacheService,
            $scope,
            accessor: IRoleDefinitionAccessor,
            globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {
            super(
                stateCacheService,
                "roleDefinitionPermissionsAssignmentController",
                "Common_RoleDefinition|Details",
                "Common_RoleDefinition|Details|Assignment|Common_Permission",
                $scope,
                accessor,
                globalStateService);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller("roleDefinitionPermissionsAssignmentController", RoleDefinitionPermissionsAssignmentController);
}                    