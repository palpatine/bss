﻿module SolvesIt.Bss.Common.RoleDefinitions {
    export interface IRoleDefinitionViewModel extends SolvesIt.Bss.Models.IDetailedMarker {
        targetKey: string;
        dateStart: Date;
        dateEnd: Date;
    }
}