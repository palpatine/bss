﻿module SolvesIt.Bss.Common.RoleDefinitions {
    export class RoleDefinitionViewModel implements IRoleDefinitionViewModel {
        id: number;
        displayName: string;
        targetKey: string;
        dateStart: Date;
        dateEnd: Date;
    }
}