﻿module SolvesIt.Bss.Common.RoleDefinitions {
    export interface IRoleDefinitionAccessor extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<IRoleDefinitionViewModel>, Core.Accessors.IPermissionsRelatedAccessor {
    }
}