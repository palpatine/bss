﻿using System;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels
{
    public sealed class ActivityViewModel : NamedViewModel, IActivity
    {
        public string Color { get; set; }

        public string Comment { get; set; }

        public string Name { get; set; }

        public NamedViewModel ActivityKind { get; set; }

        public DateTimeOffset? DateEnd { get; set; }

        public DateTimeOffset DateStart { get; set; }
    }
}