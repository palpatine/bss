﻿using System;
using System.Globalization;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels
{
    public sealed class EntryViewModel : IViewModel, IEntry
    {
        public int Id { get; set; }

        public NamedViewModel User { get; set; }

        public NamedViewModel Topic { get; set; }

        public NamedViewModel Activity { get; set; }

        public DateTimeOffset Date { get; set; }

        public int Minutes { get; set; }

        [PropertySource(typeof(Activity))]
        public string Color { get; set; }

        public string Comment { get; set; }

        [PropertySource(Ignore = true)]
        public string DisplayName
        {
            get
            {
                if (User != null)
                {
                    return string.Format(CultureInfo.InvariantCulture, "{0:yyyy-MM-dd} - {1}", Date, User.DisplayName);
                }
                else
                {
                    return string.Format(CultureInfo.InvariantCulture, "{0:yyyy-MM-dd}", Date);
                }
            }
        }
    }
}