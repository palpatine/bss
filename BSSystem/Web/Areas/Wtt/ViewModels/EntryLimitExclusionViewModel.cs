﻿using System;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels
{
    public sealed class EntryLimitExclusionViewModel : IViewModel, IEntryLimitExclusion
    {
        public int Id { get; set; }

        public string Comment { get; set; }

        public DateTimeOffset? DateEnd { get; set; }

        public DateTimeOffset DateStart { get; set; }

        public DateTimeOffset PeriodEnd { get; set; }

        public DateTimeOffset PeriodStart { get; set; }

        public NamedViewModel User { get; set; }
    }
}
