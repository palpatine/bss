﻿using System;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels
{
    public sealed class TopicViewModel : NamedViewModel, ITopic
    {
        public string Comment { get; set; }

        public DateTimeOffset? DateEnd { get; set; }

        public DateTimeOffset DateStart { get; set; }

        public string Name { get; set; }

        public NamedViewModel TopicKind { get; set; }
    }
}