﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels
{
    public sealed class ActivityKindViewModel : NamedViewModel, IActivityKind
    {
        public string Name { get; set; }

        public DateTimeOffset DateStart { get; set; }

        public DateTimeOffset? DateEnd { get; set; }
    }
}
