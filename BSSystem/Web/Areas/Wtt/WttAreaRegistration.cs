﻿using System.Web.Http;
using System.Web.Mvc;
using SolvesIt.BSSystem.Web.Environment.Api;

namespace SolvesIt.BSSystem.Web.Areas.Wtt
{
    public class WttAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Wtt";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Wtt_mvc",
                "Wtt/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional });

            context.MapHttpRoute("WttCollectionApi", "api/Wtt/List/{controller}/{action}", new { });
            var wttEntityApiParameters = new
            {
                id = RouteParameter.Optional,
                action = RouteParameter.Optional,
                actionid = RouteParameter.Optional,
                subaction = RouteParameter.Optional,
                subactionid = RouteParameter.Optional
            };
            context.MapHttpRoute("WttEntityApi", "api/Wtt/{controller}/{id}/{action}/{actionid}/{subaction}/{subactionid}", wttEntityApiParameters);
        }
    }
}