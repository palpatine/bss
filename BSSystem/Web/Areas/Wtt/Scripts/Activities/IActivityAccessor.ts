﻿module SolvesIt.Bss.Wtt.Activities {
    export interface IActivityAccessor
        extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<IActivityViewModel>, SolvesIt.Bss.Core.Accessors.IPermissionsRelatedAccessor {
    }
} 