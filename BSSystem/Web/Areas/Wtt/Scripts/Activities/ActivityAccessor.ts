﻿module SolvesIt.Bss.Wtt.Activities {
    class ActivityAccessor
        extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IActivityViewModel>
        implements IActivityAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.activities);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("activityAccessor", ActivityAccessor);
}