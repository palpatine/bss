﻿module SolvesIt.Bss.Wtt.Activities {
    export class ActivityViewModel implements IActivityViewModel {
        id: number;

        displayName: string;

        dateStart: Date;

        dateEnd: Date;

        name: string;

        comment: string;

        color: string;

        activityKind: Wtt.ActivityKinds.IActivityKindDetailedMarker;
    }
} 