﻿module SolvesIt.Bss.Wtt.Activities {
    export interface IActivityViewModel
        extends IActivityDetailedMarker {
        name: string;
        comment: string;
        color: string;
        activityKind: Wtt.ActivityKinds.IActivityKindDetailedMarker;
    }
}