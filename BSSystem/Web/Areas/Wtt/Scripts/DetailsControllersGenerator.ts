﻿module SolvesIt.Bss.Wtt {
    (() => {
        var detailsGenerator = new SolvesIt.Bss.Core.ControllerGenerators.GenericDetailsGenerator();
        var detailsEntities = [
            SolvesIt.Bss.Entities.activities,
            SolvesIt.Bss.Entities.topics,
            SolvesIt.Bss.Entities.topicKinds,
            SolvesIt.Bss.Entities.activityKinds,
            SolvesIt.Bss.Entities.ownEntries,
            SolvesIt.Bss.Entities.otherEntries,
            SolvesIt.Bss.Entities.entryLimitExclusions
        ];
        _.each(detailsEntities, (entity) => {
            detailsGenerator.createEditor(Wtt, entity);
        });

        var editorsGenerator = new SolvesIt.Bss.Core.ControllerGenerators.GenericEditorsGenerator();
        var editorsEntities = [
            SolvesIt.Bss.Entities.activities,
            SolvesIt.Bss.Entities.topics,
            SolvesIt.Bss.Entities.topicKinds,
            SolvesIt.Bss.Entities.activityKinds,
            SolvesIt.Bss.Entities.otherEntries,
            SolvesIt.Bss.Entities.entryLimitExclusions
        ];
        _.each(editorsEntities, (entity) => {
            editorsGenerator.createEditor(Wtt, entity);
        });
    })();
} 