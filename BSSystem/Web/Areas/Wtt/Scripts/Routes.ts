﻿module SolvesIt.Bss.Wtt {
    var detailsEntitesSet = [
        SolvesIt.Bss.Entities.activities,
        SolvesIt.Bss.Entities.ownEntries,
        SolvesIt.Bss.Entities.otherEntries,
        SolvesIt.Bss.Entities.topics,
        SolvesIt.Bss.Entities.topicKinds,
        SolvesIt.Bss.Entities.activityKinds,
        SolvesIt.Bss.Entities.entryLimitExclusions

    ];

    var listEntitesSet = [
        SolvesIt.Bss.Entities.activities,
        SolvesIt.Bss.Entities.topics,
        SolvesIt.Bss.Entities.entryLimitExclusions

    ];

    _.each(detailsEntitesSet, (entities) => {
        SolvesIt.Bss.Application
            .route(
            routeFactory => {
                return routeFactory.getWhenDetailsUrlFor(entities);
            },
            routeFactory => {
                return {
                    templateUrl: routeFactory.getDetailsTemplateFor(entities),
                    controller: "breadcrumbsDetailsDisplayer"
                };
            });
    });

    _.each(listEntitesSet, (entities) => {
        SolvesIt.Bss.Application
            .route(
            "/Wtt/" + entities,
            routeFactory => {
                return {
                    templateUrl: routeFactory.getDetailedListTemplateFor(entities),
                    controller: 'breadcrumbsSection'
                };
            });
    });

    SolvesIt.Bss.Application
        .route(
        "/Administration/TopicKind",
        routeFactory => {
            return {
                templateUrl: routeFactory.getDetailedListTemplateFor(SolvesIt.Bss.Entities.topicKinds),
                controller: 'breadcrumbsSection'
            };
        });
    SolvesIt.Bss.Application
        .route(
        "/Administration/ActivityKind",
        routeFactory => {
            return {
                templateUrl: routeFactory.getDetailedListTemplateFor(SolvesIt.Bss.Entities.activityKinds),
                controller: 'breadcrumbsSection'
            };
        });

    SolvesIt.Bss.Application
        .route(
        "/Administration/ActivityKind",
        routeFactory => {
            return {
                templateUrl: routeFactory.getDetailedListTemplateFor(SolvesIt.Bss.Entities.activityKinds),
                controller: 'breadcrumbsSection'
            };
        });

    SolvesIt.Bss.Application
        .route(
        "/Wtt/" + SolvesIt.Bss.Entities.ownEntries + "List",
        routeFactory => {
            return {
                templateUrl: routeFactory.getTemplateForOwnEntriesList(),
                controller: 'breadcrumbsSection'
            };
        });
    SolvesIt.Bss.Application
        .route(
        "/Wtt/" + SolvesIt.Bss.Entities.ownEntries + "Calendar",
        routeFactory => {
            return {
                templateUrl: routeFactory.getTemplateForOwnEntriesCalendar(),
                controller: 'breadcrumbsSection'
            };
        });

    SolvesIt.Bss.Application
        .route(
        "/Wtt/" + SolvesIt.Bss.Entities.otherEntries + "List",
        routeFactory => {
            return {
                templateUrl: routeFactory.getTemplateForOtherEntriesList(),
                controller: 'breadcrumbsSection'
            };
        });

    SolvesIt.Bss.Application
        .route(
        "/Wtt/" + SolvesIt.Bss.Entities.otherEntries + "Calendar",
        routeFactory => {
            return {
                templateUrl: routeFactory.getTemplateForOtherEntriesCalendar(),
                controller: 'breadcrumbsSection'
            };
        });
}