﻿module SolvesIt.Bss.Wtt.Topics {
    export interface ITopicAccessor
        extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<ITopicViewModel>, SolvesIt.Bss.Core.Accessors.IPermissionsRelatedAccessor {
    }
} 