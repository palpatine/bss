﻿module SolvesIt.Bss.Wtt.Topics {
    export class TopicViewModel implements ITopicViewModel {
        id: number;

        displayName: string;

        dateStart: Date;

        dateEnd: Date;

        name: string;

        comment: string;

        topicKind:TopicKinds.ITopicKindDetailedMarker;
    }
} 