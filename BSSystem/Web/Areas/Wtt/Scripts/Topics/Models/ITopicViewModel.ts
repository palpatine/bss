﻿module SolvesIt.Bss.Wtt.Topics {
    export interface ITopicViewModel
        extends ITopicDetailedMarker {
        name: string;
        comment: string;
        dateStart: Date;
        dateEnd: Date;
        topicKind: TopicKinds.ITopicKindDetailedMarker
    }
}