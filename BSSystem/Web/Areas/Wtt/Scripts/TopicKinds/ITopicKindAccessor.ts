﻿module SolvesIt.Bss.Wtt.TopicKinds {
    export interface ITopicKindAccessor
        extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<ITopicKindViewModel>, SolvesIt.Bss.Core.Accessors.IPermissionsRelatedAccessor {
    }
} 