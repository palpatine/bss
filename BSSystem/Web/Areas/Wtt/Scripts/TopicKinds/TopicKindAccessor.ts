﻿module SolvesIt.Bss.Wtt.TopicKinds {
    class TopicKindAccessor
        extends SolvesIt.Bss.Core.Accessors.EntityAccessor<ITopicKindViewModel>
        implements ITopicKindAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.topicKinds);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("topicKindAccessor", TopicKindAccessor);
}