﻿module SolvesIt.Bss.Wtt.TopicKinds {
    export class TopicKindViewModel implements ITopicKindViewModel {
        id: number;

        displayName: string;

        dateStart: Date;

        dateEnd: Date;

        name: string;

        canAcceptEntries: boolean;
    }
} 