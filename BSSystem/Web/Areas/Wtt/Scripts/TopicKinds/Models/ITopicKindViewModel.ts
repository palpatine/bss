﻿module SolvesIt.Bss.Wtt.TopicKinds {
    export interface ITopicKindViewModel
        extends ITopicKindDetailedMarker {
        name: string;
        dateStart: Date;
        dateEnd: Date;
        canAcceptEntries: boolean;
    }
}