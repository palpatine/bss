﻿module SolvesIt.Bss.Wtt.OwnEntries {
    class OwnEntryDataEditorController extends SolvesIt.Bss.Core.Controllers.BaseDetailsEditorController<OwnEntryViewModel> {
        static $inject = [
            'stateCacheService',
            '$scope',
            'ownEntryAccessor',
            'oDataQueryParametersGeneratorFactory',
            'bssODataQuerySerializer',
            'routeFactory',
            'globalStateService'
        ];

        public topicConfiguration;
        public activityConfiguration;
        public activitiesSource = null;

        constructor(
            stateCacheService: SolvesIt.Widgets.IStateCacheService,
            $scope,
            accessor: SolvesIt.Bss.Core.Accessors.IEntityAccessor<OwnEntryViewModel>,
            private oDataQueryParametersGeneratorFactory: SolvesIt.Widgets.QueryString.IODataQueryParametersGeneratorFactory,
            private oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            private routeFactory: Core.IRouteFactory,
            globalStateService: SolvesIt.Bss.Core.ApplicationManagement.IGlobalStateService) {
            super(stateCacheService,
                SolvesIt.Bss.Entities.ownEntries + 'DetailsPanel:' + $scope.section.id + '/' + SolvesIt.Bss.Entities.ownEntries + "DetailsEditorController",
                $scope,
                accessor,
                SolvesIt.Bss.Entities.ownEntries,
                globalStateService);

            this.topicConfiguration = {
                $orderby: "element.displayName",
                $select: "element.id,element.displayName"
            };
        }

        public dateChanged() {
            if (!this.state.model.date) return;
            this.state.model.topic = null;
            var dateFilter = this.createDateFilter();

            this.topicConfiguration = {
                $orderby: "element.displayName",
                $filter: this.oDataQuerySerializer.serialize(dateFilter.generate())["$filter"],
                $select: "element.id,element.displayName"
            }
        }

        public topicChanged() {
            this.state.model.activity = null;
            var dateFilter = this.createDateFilter();
            this.activityConfiguration = {
                $orderby: "element.displayName",
                $filter: this.oDataQuerySerializer.serialize(dateFilter.generate())["$filter"],
                $select: "element.id,element.displayName"
            }
            if (this.state.model.topic != null) {
                this.activitiesSource = this.routeFactory.getOdElementsMarkersFor(Entities.topics, this.state.model.topic.id, Entities.activities);
            }
            else {
                this.activitiesSource = null;
            }
        }

        private createDateFilter(): SolvesIt.Widgets.QueryString.IODataQueryParametersGenerator {
            var filter = this.oDataQueryParametersGeneratorFactory.create();
            filter
                .where()
                .property("element.dateStart")
                .le(this.state.model.date)
                .and()
                .property("element.dateEnd")
                .geOrNull(this.state.model.date);
            return filter;
        }

        getEmptyDataModelInstance(): OwnEntryViewModel {
            return new OwnEntryViewModel();
        }
    }

    SolvesIt.Bss.Application.route(
        routeFactory => routeFactory.getWhenCreateUrlFor(SolvesIt.Bss.Entities.ownEntries),
        routeFactory => {
            return {
                templateUrl: routeFactory.getCreateDataTemplateFor(SolvesIt.Bss.Entities.ownEntries),
                controller: 'breadcrumbsDataCreator'
            };
        });

    SolvesIt.Bss.Application.getApplication().controller("ownEntryDetailsCreatorController", OwnEntryDataEditorController);
    SolvesIt.Bss.Application.getApplication().controller("ownEntryDetailsEditorController", OwnEntryDataEditorController);
} 