﻿module SolvesIt.Bss.Wtt.OwnEntries {
    class OwnEntriesCalendarController {
        private locks: { [id: number]: ng.IPromise<SolvesIt.Bss.Models.ILockResult> } = {};

        public state: OwnEntriesCalendarState;
        public cacheId: string = "OwnEntries/Calendar";
        public calendarCacheId: string = "OwnEntries/Calendar/Display";
        public isCreatorOpen: boolean = false;
        public phantomItem: SolvesIt.Widgets.Calendar.IPhantomCalendarItem;
        public creatorSection: SolvesIt.Bss.ISectionCard;

        static $inject = [
            '$scope',
            '$q',
            'stateCacheService',
            'ownEntryAccessor'
        ];

        constructor(
            private $scope: SolvesIt.Bss.IBssSectionScope,
            private $q: ng.IQService,
            stateCacheService: SolvesIt.Widgets.IStateCacheService,
            private ownEntryAccessor: IOwnEntryAccessor) {
            this.creatorSection = new SolvesIt.Bss.SectionCard(
                this.$q,
                (success) => {
                    this.isCreatorOpen = false;
                    if (this.phantomItem) {
                        this.phantomItem.remove();
                    }

                    if (success) {
                        this.state.displayCriteria.isDirty = true;
                        $scope.$applyAsync(() => {
                            this.state.displayCriteria.isDirty = false;
                        });
                    }
                });
            this.creatorSection.addReadyHander(() => {
                var model = new OwnEntryViewModel();
                model.date = this.phantomItem.date;
                model.minutes = this.phantomItem.length;

                this.creatorSection.modelTemplate = model;
            });

            this.state = stateCacheService.getCacheFor(this.cacheId, () => {
                var state = new OwnEntriesCalendarState();
                state.selectorDisplayCriteria.period.start.setDate(1);
                state.selectorDisplayCriteria.period.end.setMonth(state.selectorDisplayCriteria.period.end.getMonth() + 2);
                state.selectorDisplayCriteria.period.end.setDate(1);
                return state;
            });
            this.state.calendarModel.onSelectionChangeEnd = () => { this.onSelectionChangeEnd(); };
            this.state.calendarModel.onSelectionChangeBegin = () => { this.onSelectionChangeBegin(); };

            $scope.$watch(() => { return $scope.$$articleVisibleHeight; }, (value) => {
                if (($scope.$$articleVisibleHeight - 100) / this.state.displayCriteria.viewKindsConfiguration['daily'].hoursCount / 60 >= 1) {
                    this.state.displayCriteria.pixelsPerMinute = 1;
                } else {
                    this.state.displayCriteria.pixelsPerMinute = 0.5;
                }
            });

            $scope.$watch(() => {
                return this.state.displayCriteria.isDirty;
            }, (value) => {
                    if (value || this.state.displayCriteria.selection.getDaysCount() <= 7) {
                        return;
                    }

                    this.state.displayCriteria.selection.expandToWeeks();
                });
        }

        public onItemResize(args: SolvesIt.Widgets.Calendar.IProportionalItemResizeEventArgs) {
            var item = <IOwnEntryViewModel><any>args.item;

            if (args.isStarting) {
                this.locks[item.id] = this.ownEntryAccessor.lock(item.id);
            }
            else if (args.isCancelled) {
                if (this.locks[item.id])
                    this.locks[item.id].then((result: SolvesIt.Bss.Models.ILockResult) => {
                        if (result.validationResult.isValid) {
                            this.ownEntryAccessor.unlock(item.id, result.operationToken);
                        }
                    });
            }
            else if (args.isCommited) {
                if (this.locks[item.id])
                    this.locks[item.id].then((result: SolvesIt.Bss.Models.ILockResult) => {
                        if (result.validationResult.isValid) {
                            this.ownEntryAccessor.saveData(item, result.operationToken);
                        } else {
                            setTimeout(() => alert(result.validationResult.errors[0].message), 10);
                        }
                    });
            }
        }

        private onSelectionChangeEnd(): void {
            this.isCreatorOpen = true;

            this.creatorSection.opened();
        }

        private onSelectionChangeBegin(): void {
            if (this.isCreatorOpen) {
                this.isCreatorOpen = false;
                this.creatorSection.close(false);
            }
        }

        public addOwnEntry() {
            window.location.href = "#/Forms/OwnEntry/Create";
        }

        public formatMinutes(value: number) {
            return moment.utc(value * 1000 * 60).format("H[:]mm");
        }

        public isLastDayOfWeek(item: OwnEntryViewModel): boolean {
            return item.date.getDay() == (6 + Globals.firstDayOfWeek) % 7;
        }

        public previous(): void {
            this.state.selectorDisplayCriteria.period.start.setMonth(this.state.selectorDisplayCriteria.period.start.getMonth() - 1);
            this.state.selectorDisplayCriteria.period.end.setMonth(this.state.selectorDisplayCriteria.period.end.getMonth() - 1);
            this.state.selectorDisplayCriteria.period.start.setDate(1);
            this.state.selectorDisplayCriteria.period.end.setDate(1);
        }

        public next(): void {
            this.state.selectorDisplayCriteria.period.start.setMonth(this.state.selectorDisplayCriteria.period.start.getMonth() + 1);
            this.state.selectorDisplayCriteria.period.end.setMonth(this.state.selectorDisplayCriteria.period.end.getMonth() + 1);
            this.state.selectorDisplayCriteria.period.start.setDate(1);
            this.state.selectorDisplayCriteria.period.end.setDate(1);
        }

        public setToday(): void {
            var period = new SolvesIt.Widgets.Calendar.Period();
            period.setPeriod(new Date(), 1);
            this.state.displayCriteria.selection = period;
            this.setSelectorDisplayToToday();
        }

        public setThisWeek(): void {
            var period = new SolvesIt.Widgets.Calendar.Period();
            period.setPeriod(new Date(), 1);
            period.expandToWeeks();
            this.state.displayCriteria.selection = period;
            this.setSelectorDisplayToToday();
        }

        public setThisMonth(): void {
            var period = new SolvesIt.Widgets.Calendar.Period();
            period.setPeriod(new Date(), 1);
            period.expandToMonth();
            this.state.displayCriteria.selection = period;
            this.setSelectorDisplayToToday();
        }

        private setSelectorDisplayToToday() {
            var displayCriteriaPeriod = SolvesIt.Widgets.Calendar.Period.fullMonths(new Date(), 2);
            this.state.selectorDisplayCriteria.period = displayCriteriaPeriod;
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .controller("ownEntriesCalendarController", OwnEntriesCalendarController);
} 