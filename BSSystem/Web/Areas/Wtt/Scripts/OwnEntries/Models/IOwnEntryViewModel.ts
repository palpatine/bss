﻿module SolvesIt.Bss.Wtt.OwnEntries {
    export interface IOwnEntryViewModel
        extends IOwnEntryDetailedMarker {
        comment: string;
        topic: Wtt.Topics.ITopicDetailedMarker;
        activity: Wtt.Activities.IActivityDetailedMarker;
        user: SolvesIt.Bss.Common.Users.IUserNamedMarker;
        date: Date;
        minutes: number;
    }
}