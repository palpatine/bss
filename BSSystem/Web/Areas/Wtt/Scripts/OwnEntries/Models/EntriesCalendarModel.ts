﻿module SolvesIt.Bss.Wtt.OwnEntries {
    export class EntriesCalendarModel implements SolvesIt.Widgets.Calendar.IDisplayCriteria, SolvesIt.Widgets.Calendar.ICalendarModel {
        private selectedPeriod: SolvesIt.Widgets.Calendar.Period = new SolvesIt.Widgets.Calendar.Period();
        private isModelDirty: boolean;

        public get isDirty(): boolean {
            return this.isModelDirty;
        }

        public set isDirty(value: boolean) {
            this.isModelDirty = value;
            if (!value && this.onSelectionChangeAccepted) {
                this.onSelectionChangeAccepted();
            }
        }

        onSelectionChangeAccepted: () => void = null;

        public viewKindsConfiguration: { [viewKindName: string]: any } = {
            daily:
            {
                hoursCount: 8,
                minimalDisplayableDuration: 30,
                itemTemplates: [
                    {
                        length: 0,
                        url: "~/Wtt/OwnEntryTemplates/DailyMicroCalendarItem"
                    },
                    {
                        length: 5,
                        url: "~/Wtt/OwnEntryTemplates/DailySmallCalendarItem"
                    },
                    {
                        length: 30,
                        url: "~/Wtt/OwnEntryTemplates/DailyNormalCalendarItem"
                    }],
                phantomItemTemplateUrl: "~/Wtt/OwnEntryTemplates/DailyCalendarPhantomItem",
                canAdjustDurationByDragging: true
            },
            weekly: {
                itemTemplateUrl: "~/Wtt/OwnEntryTemplates/WeeklyCalendarItem",
                cellHeaderTemplateUrl: '~/Content/Templates/BssCalendar/Weekly/CellHeaderTemplate.tpl.html'
            }
        };

        public pixelsPerMinute = 0.5;

        constructor() {
            this.selectedPeriod.setPeriod(new Date(), 1);
        }

        isValid() {
            return (this.selection && this.selection.start && this.selection.end) ? true : false;
        }

        get selection(): SolvesIt.Widgets.Calendar.Period { return this.selectedPeriod; }
        set selection(value: SolvesIt.Widgets.Calendar.Period) {
            this.selectedPeriod = value;
        }

        get period(): SolvesIt.Widgets.Calendar.Period { return this.selectedPeriod; }
        set period(value: SolvesIt.Widgets.Calendar.Period) {
            this.selectedPeriod = value;
        }
    }
} 