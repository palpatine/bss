﻿module SolvesIt.Bss.Wtt.OwnEntries {
    export class OwnEntriesCalendarState {
        displayCriteria: EntriesCalendarModel = new EntriesCalendarModel();
        calendarModel: SolvesIt.Widgets.Calendar.CalendarModel = new SolvesIt.Widgets.Calendar.CalendarModel();
        selectorDisplayCriteria: SolvesIt.Widgets.Calendar.IDisplayCriteria = new SolvesIt.Widgets.Calendar.DisplayCriteria();
    }
} 