﻿module SolvesIt.Bss.Wtt.OwnEntries {
    class OwnEntryAccessor
        extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IOwnEntryViewModel>
        implements IOwnEntryAccessor, SolvesIt.Bss.Core.Accessors.ICalendarRelatedAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.ownEntries);
        }

        getAllForCalendar(query: SolvesIt.Widgets.QueryString.IQueryDescriptor): ng.IPromise<SolvesIt.Widgets.Calendar.IProportionalCalendarItem[]> {
            return this.getAll(query)
                .then(data => {
                return _.map(data, item => {
                    return angular.extend({
                        get length() {
                            return this.minutes;
                        },

                        set length(value: number) {
                            this.minutes = Math.round(value);
                        }
                    }, item);
                });
            });
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("ownEntryAccessor", OwnEntryAccessor);
}