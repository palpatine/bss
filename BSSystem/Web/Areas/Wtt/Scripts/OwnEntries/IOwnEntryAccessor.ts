﻿module SolvesIt.Bss.Wtt.OwnEntries {
    export interface IOwnEntryAccessor
        extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<IOwnEntryViewModel>, SolvesIt.Bss.Core.Accessors.IPermissionsRelatedAccessor {
    }
} 