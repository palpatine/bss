﻿module SolvesIt.Bss.Wtt.ActivityKinds {
    export interface IActivityKindViewModel
        extends IActivityKindDetailedMarker {
        name: string;
        dateStart: Date;
        dateEnd: Date;
    }
}