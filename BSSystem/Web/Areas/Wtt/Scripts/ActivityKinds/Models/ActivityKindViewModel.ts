﻿module SolvesIt.Bss.Wtt.ActivityKinds {
    export class ActivityKindViewModel implements IActivityKindViewModel {
        id: number;

        displayName: string;

        dateStart: Date;

        dateEnd: Date;

        name: string;
    }
} 