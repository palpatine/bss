﻿module SolvesIt.Bss.Wtt.ActivityKinds {
    class ActivityKindAccessor
        extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IActivityKindViewModel>
        implements IActivityKindAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.activityKinds);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("activityKindAccessor", ActivityKindAccessor);
}