﻿module SolvesIt.Bss.Wtt.ActivityKinds {
    export interface IActivityKindAccessor
        extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<IActivityKindViewModel>, SolvesIt.Bss.Core.Accessors.IPermissionsRelatedAccessor {
    }
} 