﻿module SolvesIt.Bss.Wtt {
    (() => {
        var generator = new SolvesIt.Bss.Core.ControllerGenerators.GenericRelationGenerator();
        var entities = [
            {
                master: SolvesIt.Bss.Entities.activities,
                slave: SolvesIt.Bss.Entities.topics
            },
            {
                master: SolvesIt.Bss.Entities.topics,
                slave: SolvesIt.Bss.Entities.activities
            },
            {
                master: SolvesIt.Bss.Entities.topics,
                slave: SolvesIt.Bss.Entities.users
            },
            {
                master: SolvesIt.Bss.Entities.users,
                slave: SolvesIt.Bss.Entities.topics
            },
            {
                master: SolvesIt.Bss.Entities.sections,
                slave: SolvesIt.Bss.Entities.activities
            },
            {
                master: SolvesIt.Bss.Entities.activities,
                slave: SolvesIt.Bss.Entities.sections
            },
        ];
        _.each(entities, (entity) => {
            generator.createEditor(Wtt, entity.master, entity.slave);
        });
    })();
} 