﻿module SolvesIt.Bss.Wtt.EntryLimitExclusions {
    class EntryLimitExclusionAccessor
        extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IEntryLimitExclusionViewModel>
        implements Wtt.EntryLimitExclusions.IEntryLimitExclusionAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.entryLimitExclusions);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("entryLimitExclusionAccessor", EntryLimitExclusionAccessor);
}