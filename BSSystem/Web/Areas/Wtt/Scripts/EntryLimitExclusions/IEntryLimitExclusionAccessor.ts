﻿module SolvesIt.Bss.Wtt.EntryLimitExclusions {
    export interface IEntryLimitExclusionAccessor
        extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<IEntryLimitExclusionViewModel> {
    }
} 