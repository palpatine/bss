﻿module SolvesIt.Bss.Wtt.EntryLimitExclusions {
    export interface IEntryLimitExclusionViewModel
        extends IEntryLimitExclusionDetailedMarker {
        id: number;

        periodStart: Date;

        periodEnd: Date;

        deadline: Date;

        comment: string;

        user: SolvesIt.Bss.Common.Users.IUserNamedMarker;
    }
}