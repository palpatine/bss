﻿module SolvesIt.Bss.Wtt.EntryLimitExclusions {
    export class EntryLimitExclusionViewModel implements IEntryLimitExclusionViewModel {
        id: number;

        periodStart: Date;

        periodEnd: Date;

        deadline: Date;

        comment: string;

        user: SolvesIt.Bss.Common.Users.IUserNamedMarker;
    }
} 