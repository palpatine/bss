﻿module SolvesIt.Bss.Wtt.OtherEntries {
    export class OtherEntryViewModel implements IOtherEntryViewModel {
        id: number;

        comment: string;
        topic: Wtt.Topics.ITopicDetailedMarker;
        activity: Wtt.Activities.IActivityDetailedMarker;
        user: SolvesIt.Bss.Common.Users.IUserNamedMarker;
        date: Date;
        minutes: number;
    }
} 