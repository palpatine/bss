﻿module SolvesIt.Bss.Wtt.OtherEntries {
    export interface IOtherEntryViewModel
        extends IOtherEntryDetailedMarker {
        comment: string;
        topic: Wtt.Topics.ITopicDetailedMarker;
        activity: Wtt.Activities.IActivityDetailedMarker;
        user: SolvesIt.Bss.Common.Users.IUserNamedMarker;
        date: Date;
        minutes: number;
    }
}