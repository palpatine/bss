﻿module SolvesIt.Bss.Wtt.OtherEntries {
    class OtherEntryAccessor
        extends SolvesIt.Bss.Core.Accessors.EntityAccessor<IOtherEntryViewModel>
        implements IOtherEntryAccessor {
        public static $inject = [
            'adStrapUtils',
            '$adConfig',
            '$http',
            'routeFactory',
            '$q',
            'bssODataQuerySerializer',
            'PostDeserializationObjectProcessor'
        ];

        constructor(
            adStrapUtils,
            $adConfig,
            $http: ng.IHttpService,
            routeFactory: SolvesIt.Bss.Core.IRouteFactory,
            $q: ng.IQService,
            oDataQuerySerializer: SolvesIt.Widgets.QueryString.IODataQuerySerializer,
            postDeserializationObjectProcessor: SolvesIt.Widgets.IPostDeserializationObjectProcessor) {
            super(adStrapUtils, $adConfig, $http, routeFactory, $q, oDataQuerySerializer, postDeserializationObjectProcessor, SolvesIt.Bss.Entities.otherEntries);
        }
    }

    SolvesIt.Bss.Application.getApplication()
        .service("otherEntryAccessor", OtherEntryAccessor);
}