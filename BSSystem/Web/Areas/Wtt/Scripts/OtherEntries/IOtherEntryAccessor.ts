﻿module SolvesIt.Bss.Wtt.OtherEntries {
    export interface IOtherEntryAccessor
        extends SolvesIt.Bss.Core.Accessors.IEntityAccessor<IOtherEntryViewModel>, SolvesIt.Bss.Core.Accessors.IPermissionsRelatedAccessor {
    }
} 