using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Sections;
using SolvesIt.BSSystem.Web.Areas.Common.Extensions.Users;
using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.Activities;
using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.Topics;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Extensions
{
    public class WttTemplatesExtensionsBootstrapper : TemplatesExtensionsBootstrapper
    {
        protected override void CreateDetailsCommands()
        {
        }

        protected override void CreateDetailsPanels()
        {
            CreateAssignmentDetailsPanel<TopicDetailsPanel, ITopic, IActivity>();
            CreateAssignmentDetailsPanel<TopicDetailsPanel, ITopic, IUser>();
            CreateAssignmentDetailsPanel<ActivityDetailsPanel, IActivity, ITopic>();
            CreateAssignmentDetailsPanel<ActivityDetailsPanel, IActivity, ISection>();
            CreateAssignmentDetailsPanel<UserDetailsPanel, IUser, ITopic>();
            CreateAssignmentDetailsPanel<SectionDetailsPanel, ISection, IActivity>();
        }
    }
}