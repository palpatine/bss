﻿using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsWtt.Containers.TopicKind)]
    public class TopicKindController
        : EditScopedApiController<TopicKind, ITopicKind, TopicKindViewModel>
    {
        public TopicKindController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<TopicKind, ITopicKind> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, entityService, permissionHelper, settingService)
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsWtt.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<TopicKindViewModel>();
            builder.AddDefaultActionsColumn(PermissionsWtt.TopicKind.Details.Write.Path, PermissionsWtt.TopicKind.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Name)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateEnd)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        protected override async Task<TopicKind> PrepareEditModelAsync(TopicKindViewModel model)
        {
            TopicKind editModel;
            if (model.Id != 0)
            {
                editModel = DatabaseAccessor.Query<TopicKind>()
                    .SingleOrDefault(
                        x =>
                            x.IsEquivalent<ITopicKind>(model) && !x.IsDeleted &&
                            x.Scope.IsEquivalent(SessionDataProvider.Scope));

                if (editModel == null)
                {
                    return null;
                }
            }
            else
            {
                editModel = new TopicKind();
                editModel.Scope = SessionDataProvider.Scope;
            }

            editModel.CanAcceptEntries = model.CanAcceptEntries;
            editModel.DateEnd = await ConvertToDateAsync(model.DateEnd);
            editModel.DateStart = await ConvertToDateAsync(model.DateStart);
            editModel.DisplayName = model.DisplayName;
            editModel.Name = model.Name;

            return editModel;
        }

        protected override ITableQuery<TopicKindViewModel> ProjectToViewModel(ITableQuery<TopicKind> query)
        {
            return query.Select((tk) => new TopicKindViewModel
            {
                Id = tk.Id,
                DisplayName = tk.DisplayName,
                Name = tk.Name,
                CanAcceptEntries = tk.CanAcceptEntries,
                DateStart = tk.DateStart,
                DateEnd = tk.DateEnd,
            });
        }
    }
}