﻿using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsWtt.Containers.ActivityKind)]
    public class ActivityKindController
        : EditScopedApiController<ActivityKind, IActivityKind, ActivityKindViewModel>
    {
        public ActivityKindController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<ActivityKind, IActivityKind> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, entityService, permissionHelper, settingService)
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsWtt.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<ActivityKindViewModel>();
            builder.AddDefaultActionsColumn(PermissionsWtt.ActivityKind.Details.Write.Path, PermissionsWtt.ActivityKind.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Name)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateEnd)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        protected override async Task<ActivityKind> PrepareEditModelAsync(ActivityKindViewModel model)
        {
            ActivityKind editModel;
            if (model.Id != 0)
            {
                editModel = DatabaseAccessor.Query<ActivityKind>()
                    .SingleOrDefault(
                        x =>
                            x.IsEquivalent<IActivityKind>(model) && !x.IsDeleted &&
                            x.Scope.IsEquivalent(SessionDataProvider.Scope));

                if (editModel == null)
                {
                    return null;
                }
            }
            else
            {
                editModel = new ActivityKind();
                editModel.Scope = SessionDataProvider.Scope;
            }

            editModel.DateEnd = await ConvertToDateAsync(model.DateEnd);
            editModel.DateStart = await ConvertToDateAsync(model.DateStart);
            editModel.DisplayName = model.DisplayName;
            editModel.Name = model.Name;

            return editModel;
        }

        protected override ITableQuery<ActivityKindViewModel> ProjectToViewModel(ITableQuery<ActivityKind> query)
        {
            return query.Select((activityKind) => new ActivityKindViewModel
            {
                Id = activityKind.Id,
                DisplayName = activityKind.DisplayName,
                Name = activityKind.Name,
                DateStart = activityKind.DateStart,
                DateEnd = activityKind.DateEnd,
            });
        }
    }
}