﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Constants;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Markers;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsWtt.Containers.Entry)]
    public class OtherEntryController
        : EditScopedApiController<Entry, IEntry, EntryViewModel>
    {
        public OtherEntryController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<Entry, IEntry> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, entityService, permissionHelper, settingService)
        {
        }

        [RequiredPermission(PermissionsWtt.Keys.Details + PermissionsWtt.Keys.Other + PermissionsCommon.Keys.Write)]
        [HttpDelete]
        public override async Task<IHttpActionResult> Delete(int id)
        {
            return await base.Delete(id);
        }

        [RequiredPermission(PermissionsWtt.Keys.Read + PermissionsWtt.Keys.Other)]
        public override Task<IHttpActionResult> Get()
        {
            return base.Get();
        }

        [RequiredPermission(PermissionsWtt.Keys.Details + PermissionsWtt.Keys.Other)]
        public override Task<IHttpActionResult> Get(int id)
        {
            return base.Get(id);
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsWtt.Keys.Read + PermissionsWtt.Keys.Other)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<EntryViewModel>();
            builder.AddDefaultActionsColumn("OtherEntry", PermissionsWtt.Entry.Details.Other.Write.Path, PermissionsWtt.Entry.Details.Path);
            builder.AddColumn(x => x.Date)
                .CanSort()
                .SortedDescendingByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Topic.DisplayName, (Topic x) => x.DisplayName)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.Activity.DisplayName, (Activity x) => x.DisplayName)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        [RequiredPermission(PermissionsWtt.Keys.Details + PermissionsWtt.Keys.Other + PermissionsCommon.Keys.Write)]
        public override async Task<IHttpActionResult> Lock(int id)
        {
            return await base.Lock(id);
        }

        [RequiredPermission(PermissionsWtt.Keys.Details + PermissionsWtt.Keys.Other + PermissionsCommon.Keys.Write)]
        public override async Task<IHttpActionResult> Post(EntryViewModel model)
        {
            return await base.Post(model);
        }

        protected override async Task<Entry> PrepareEditModelAsync(EntryViewModel model)
        {
            Entry entry;

            if (model.User == null || model.Activity == null || model.Topic == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            if (model.Id != 0)
            {
                entry = DatabaseAccessor.Query<Entry>()
                    .SingleOrDefault(x => x.Id == model.Id);

                if (entry == null)
                {
                    return null;
                }
            }
            else
            {
                entry = new Entry();
                entry.Scope = SessionDataProvider.Scope;
            }

            entry.Topic = new TopicMarker(model.Topic.Id);
            entry.Activity = new ActivityMarker(model.Activity.Id);
            entry.User = new UserMarker(model.User.Id);
            entry.Date = await ConvertToDateAsync(model.Date);
            entry.Comment = model.Comment;
            entry.Minutes = model.Minutes;

            return entry;
        }

        protected override ITableQuery<EntryViewModel> ProjectToViewModel(ITableQuery<Entry> query)
        {
            return query
                .Join(DatabaseAccessor.Query<Topic>(), x => x.Topic, (e, t) => new { Entry = e, TopicDisplayName = t.DisplayName })
                .Join(DatabaseAccessor.Query<Activity>(), x => x.Entry.Activity, (e, a) => new { e.Entry, e.TopicDisplayName, ActivityDisplayName = a.DisplayName, ActivityColor = a.Color })
                .Join(DatabaseAccessor.Query<User>(), x => x.Entry.User, (e, u) => new { e.Entry, e.TopicDisplayName, e.ActivityDisplayName, UserDisplayName = u.DisplayName, e.ActivityColor })
                .Select(x => new EntryViewModel
                {
                    Id = x.Entry.Id,
                    Activity = new NamedViewModel { DisplayName = x.ActivityDisplayName, Id = x.Entry.Activity.Id },
                    Topic = new NamedViewModel { DisplayName = x.TopicDisplayName, Id = x.Entry.Topic.Id },
                    User = new NamedViewModel { DisplayName = x.UserDisplayName, Id = x.Entry.User.Id },
                    Comment = x.Entry.Comment,
                    Date = x.Entry.Date,
                    Minutes = x.Entry.Minutes,
                    Color = x.ActivityColor
                });
        }
    }
}