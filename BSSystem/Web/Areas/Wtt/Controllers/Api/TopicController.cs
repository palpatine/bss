﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Markers;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsWtt.Containers.Topic)]
    public class TopicController
        : EditScopedApiController<Topic, ITopic, TopicViewModel>
    {
        public TopicController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<Topic, ITopic> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, entityService, permissionHelper, settingService)
        {
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsWtt.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<TopicViewModel>();
            builder.AddDefaultActionsColumn(PermissionsWtt.Topic.Details.Write.Path, PermissionsWtt.Topic.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Name)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.TopicKind.DisplayName, (TopicKind k) => k.DisplayName)
              .CanSort()
              .CanFilter();
            builder.AddColumn(x => x.DateStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateEnd)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override async Task<Topic> PrepareEditModelAsync(TopicViewModel model)
        {
            Topic editModel;
            if (model.Id != 0)
            {
                editModel = DatabaseAccessor.Query<Topic>()
                    .SingleOrDefault(
                        x =>
                            x.IsEquivalent<ITopic>(model) && !x.IsDeleted &&
                            x.Scope.IsEquivalent(SessionDataProvider.Scope));

                if (editModel == null)
                {
                    return null;
                }
            }
            else
            {
                editModel = new Topic
                {
                    Scope = SessionDataProvider.Scope
                };
            }

            editModel.Comment = model.Comment;
            editModel.DateEnd = await ConvertToDateAsync(model.DateEnd);
            editModel.DateStart = await ConvertToDateAsync(model.DateStart);
            editModel.DisplayName = model.DisplayName;
            editModel.Name = model.Name;
            editModel.TopicKind = new TopicKindMarker(model.TopicKind?.Id ?? 0);

            return editModel;
        }

        protected override ITableQuery<TopicViewModel> ProjectToViewModel(ITableQuery<Topic> query)
        {
            return query.Join(
                DatabaseAccessor.Query<TopicKind>(),
                x => x.TopicKind,
                GetTopicViewModelSelector());
        }

        private static Expression<Func<Topic, TopicKind, TopicViewModel>> GetTopicViewModelSelector()
        {
            return (t, tk) => new TopicViewModel
            {
                Id = t.Id,
                DisplayName = t.DisplayName,
                TopicKind = new NamedViewModel
                {
                    Id = tk.Id,
                    DisplayName = tk.DisplayName
                },
                Name = t.Name,
                Comment = t.Comment,
                DateStart = t.DateStart,
                DateEnd = t.DateEnd,
            };
        }
    }
}