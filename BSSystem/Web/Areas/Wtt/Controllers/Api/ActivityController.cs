﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Markers;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsWtt.Containers.Activity)]
    public class ActivityController
        : EditScopedApiController<Activity, IActivity, ActivityViewModel>
    {
        public ActivityController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<Activity, IActivity> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, entityService, permissionHelper, settingService)
        {
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods"),
            SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsWtt.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<ActivityViewModel>();
            builder.AddDefaultActionsColumn(PermissionsWtt.Activity.Details.Write.Path, PermissionsWtt.Activity.Details.Path);
            builder.AddColumn(x => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.Name)
                .CanSort()
                .CanFilter();

            builder.AddColumn(x => x.ActivityKind.DisplayName, (ActivityKind k) => k.DisplayName)
              .CanSort()
              .CanFilter();
            builder.AddColumn(x => x.DateStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateEnd)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        protected override async Task<Activity> PrepareEditModelAsync(ActivityViewModel model)
        {
            Activity editModel;

            if (model.ActivityKind == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            if (model.Id != 0)
            {
                editModel = DatabaseAccessor.Query<Activity>()
                    .SingleOrDefault(
                        x =>
                            x.IsEquivalent<IActivity>(model) && !x.IsDeleted &&
                            x.Scope.IsEquivalent(SessionDataProvider.Scope));

                if (editModel == null)
                {
                    return null;
                }
            }
            else
            {
                editModel = new Activity();
                editModel.Scope = SessionDataProvider.Scope;
            }

            editModel.Color = model.Color;
            editModel.Comment = model.Comment;
            editModel.DateEnd = await ConvertToDateAsync(model.DateEnd);
            editModel.DateStart = await ConvertToDateAsync(model.DateStart);
            editModel.DisplayName = model.DisplayName;
            editModel.Name = model.Name;
            editModel.ActivityKind = new ActivityKindMarker(model.ActivityKind.Id);

            return editModel;
        }

        protected override ITableQuery<ActivityViewModel> ProjectToViewModel(ITableQuery<Activity> query)
        {
            return query.Join(
                DatabaseAccessor.Query<ActivityKind>(),
                x => x.ActivityKind,
                GetActivityViewModelSelector());
        }

        private static Expression<Func<Activity, ActivityKind, ActivityViewModel>> GetActivityViewModelSelector()
        {
            return (t, tk) => new ActivityViewModel
            {
                Id = t.Id,
                DisplayName = t.DisplayName,
                ActivityKind = new NamedViewModel
                {
                    Id = tk.Id,
                    DisplayName = tk.DisplayName
                },
                Name = t.Name,
                Comment = t.Comment,
                DateStart = t.DateStart,
                DateEnd = t.DateEnd,
                Color = t.Color
            };
        }
    }
}