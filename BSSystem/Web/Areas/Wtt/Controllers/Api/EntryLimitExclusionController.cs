﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Web.Areas.Wtt.ViewModels;
using SolvesIt.BSSystem.Web.Authorization;
using SolvesIt.BSSystem.Web.Controllers.Api;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;
using SolvesIt.BSSystem.Web.ViewModels.Core;
using SolvesIt.BSSystem.Web.ViewModels.Core.Table;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Api
{
    [DefaultPermissionContainer(PermissionsWtt.Containers.EntryLimitExclusion)]
    public class EntryLimitExclusionController
        : EditScopedApiController<EntryLimitExclusion, IEntryLimitExclusion, EntryLimitExclusionViewModel>
    {
        public EntryLimitExclusionController(
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IQueryStringParser queryStringParser,
            IVersionedEntityService<EntryLimitExclusion, IEntryLimitExclusion> entityService,
            IPermissionHelper permissionHelper,
            ISettingService settingService)
            : base(databaseAccessor, sessionDataProvider, queryStringParser, entityService, permissionHelper, settingService)
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WebApi must be class methods")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "WebApi must use methods")]
        [RequiredPermission(PermissionsWtt.Keys.Read)]
        public TableConfiguration GetListColumnsConfiguration()
        {
            var builder = new TableConfigurationBuilder<EntryLimitExclusionViewModel>();
            builder.AddDefaultActionsColumn(PermissionsWtt.EntryLimitExclusion.Read.Path, PermissionsWtt.EntryLimitExclusion.Read.Path);
            builder.AddColumn(x => x.User.DisplayName, (User x) => x.DisplayName)
                .CanSort()
                .SortedByDefault()
                .CanFilter();
            builder.AddColumn(x => x.PeriodStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.PeriodEnd)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateStart)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.DateEnd)
                .CanSort()
                .CanFilter();
            builder.AddColumn(x => x.Comment)
                .CanSort()
                .CanFilter();

            return builder.Build();
        }

        protected override async Task<EntryLimitExclusion> PrepareEditModelAsync(EntryLimitExclusionViewModel model)
        {
            EntryLimitExclusion editModel;
            if (model.Id != 0)
            {
                editModel = DatabaseAccessor.Query<EntryLimitExclusion>()
                    .SingleOrDefault(
                        x =>
                            x.IsEquivalent<IEntryLimitExclusion>(model) && !x.IsDeleted &&
                            x.Scope.IsEquivalent(SessionDataProvider.Scope));

                if (editModel == null)
                {
                    return null;
                }
            }
            else
            {
                editModel = new EntryLimitExclusion();
                editModel.Scope = SessionDataProvider.Scope;
            }

            editModel.Comment = model.Comment;
            editModel.PeriodStart = await ConvertToDateAsync(model.PeriodStart);
            editModel.PeriodEnd = await ConvertToDateAsync(model.PeriodEnd);
            editModel.DateStart = await ConvertToDateAsync(model.DateStart);
            editModel.DateEnd = await ConvertToDateAsync(model.DateEnd);
            editModel.User = new UserMarker(model.User.Id);

            return editModel;
        }

        protected override ITableQuery<EntryLimitExclusionViewModel> ProjectToViewModel(ITableQuery<EntryLimitExclusion> query)
        {
            return query.Join(
                        DatabaseAccessor.Query<User>(),
                        x => x.User,
                        GetEntryLimitExclusionViewModelSelector());
        }

        private static Expression<Func<EntryLimitExclusion, User, EntryLimitExclusionViewModel>> GetEntryLimitExclusionViewModelSelector()
        {
            return (t, tk) => new EntryLimitExclusionViewModel
            {
                Id = t.Id,
                User = new NamedViewModel
                {
                    Id = tk.Id,
                    DisplayName = tk.DisplayName
                },
                Comment = t.Comment,
                PeriodStart = t.PeriodStart,
                PeriodEnd = t.PeriodEnd,
                DateStart = t.DateStart,
                DateEnd = t.DateEnd
            };
        }
    }
}