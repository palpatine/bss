﻿using System.Web.Mvc;
using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.Entries;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.Web.ViewModels.Templates;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Templates
{
    public sealed class OwnEntryTemplatesController : BaseTemplatesController<Entry, EntryDetailsPanel, EntryDetailsCommand>
    {
        public OwnEntryTemplatesController(TemplateDescriptor<EntryDetailsPanel, EntryDetailsCommand> descriptor)
            : base(descriptor, "OwnEntry", PermissionsWtt.Entry.Details.Own.Write.Path)
        {
        }

        public PartialViewResult Calendar()
        {
            return PartialView();
        }

        public PartialViewResult CalendarBreadcrumbControlPanel()
        {
            var model = new BreadcrumbControlPanelTemplateModel
            {
                HeaderText = "OWN CALLENDAR",
                ContentViewName = "CalendarBreadcrumbControlPanelContent"
            };

            return BreadcrumbControlPanel(model);
        }

        public PartialViewResult CalendarBreadcrumbControlPanelContent()
        {
            return PartialView();
        }

        public PartialViewResult CalendarDataCreator()
        {
            return PartialView();
        }

        public PartialViewResult DailyCalendarItem()
        {
            return PartialView();
        }

        public PartialViewResult DailyCalendarPhantomItem()
        {
            return PartialView();
        }

        public PartialViewResult DailyMicroCalendarItem()
        {
            return PartialView();
        }

        public PartialViewResult DailyNormalCalendarItem()
        {
            return PartialView();
        }

        public PartialViewResult DailySmallCalendarItem()
        {
            return PartialView();
        }

        public PartialViewResult WeeklyCalendarItem()
        {
            return PartialView();
        }
    }
}