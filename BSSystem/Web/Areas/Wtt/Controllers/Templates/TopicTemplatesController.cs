﻿using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.Topics;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Templates
{
    public sealed class TopicTemplatesController
        : BaseTemplatesController<Topic, TopicDetailsPanel, TopicDetailsCommand>
    {
        public TopicTemplatesController(TemplateDescriptor<TopicDetailsPanel, TopicDetailsCommand> descriptor)
            : base(descriptor, PermissionsWtt.Topic.Details.Write.Path)
        {
        }
    }
}