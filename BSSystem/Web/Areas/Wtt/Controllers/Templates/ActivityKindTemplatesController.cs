﻿using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.ActivityKinds;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Templates
{
    public sealed class ActivityKindTemplatesController : BaseTemplatesController<ActivityKind, ActivityKindDetailsPanel, ActivityKindDetailsCommand>
    {
        public ActivityKindTemplatesController(TemplateDescriptor<ActivityKindDetailsPanel, ActivityKindDetailsCommand> descriptor)
            : base(descriptor, PermissionsWtt.ActivityKind.Details.Write.Path)
        {
        }
    }
}