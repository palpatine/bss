﻿using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.Activities;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Templates
{
    public sealed class ActivityTemplatesController
        : BaseTemplatesController<Activity, ActivityDetailsPanel, ActivityDetailsCommand>
    {
        public ActivityTemplatesController(TemplateDescriptor<ActivityDetailsPanel, ActivityDetailsCommand> descriptor)
            : base(descriptor, PermissionsWtt.Activity.Details.Write.Path)
        {
        }
    }
}