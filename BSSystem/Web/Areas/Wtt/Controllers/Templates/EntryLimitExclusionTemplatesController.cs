﻿using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.EntryLimitExclusions;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Templates
{
    public sealed class EntryLimitExclusionTemplatesController
        : BaseTemplatesController<EntryLimitExclusion, EntryLimitExclusionDetailsPanel, EntryLimitExclusionDetailsCommand>
    {
        public EntryLimitExclusionTemplatesController(TemplateDescriptor<EntryLimitExclusionDetailsPanel, EntryLimitExclusionDetailsCommand> descriptor)
            : base(descriptor, PermissionsWtt.EntryLimitExclusion.Details.Write.Path)
        {
        }
    }
}