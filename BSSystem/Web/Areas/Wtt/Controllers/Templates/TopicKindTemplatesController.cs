﻿using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.TopicKinds;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Templates
{
    public sealed class TopicKindTemplatesController
        : BaseTemplatesController<TopicKind, TopicKindDetailsPanel, TopicKindDetailsCommand>
    {
        public TopicKindTemplatesController(TemplateDescriptor<TopicKindDetailsPanel, TopicKindDetailsCommand> descriptor)
            : base(descriptor, PermissionsWtt.TopicKind.Details.Write.Path)
        {
        }
    }
}