﻿using System.Web.Mvc;
using SolvesIt.BSSystem.Web.Areas.Wtt.Extensions.Entries;
using SolvesIt.BSSystem.Web.Controllers;
using SolvesIt.BSSystem.Web.Controllers.Templates;
using SolvesIt.BSSystem.Web.ViewModels.Templates;
using SolvesIt.BSSystem.WttModule.Abstraction.Constants;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.Web.Areas.Wtt.Controllers.Templates
{
    public sealed class OtherEntryTemplatesController : BaseTemplatesController<Entry, EntryDetailsPanel, EntryDetailsCommand>
    {
        public OtherEntryTemplatesController(TemplateDescriptor<EntryDetailsPanel, EntryDetailsCommand> descriptor)
            : base(descriptor, "OtherEntry", PermissionsWtt.Entry.Details.Other.Write.Path)
        {
        }

        public PartialViewResult Calendar()
        {
            return PartialView();
        }

        public PartialViewResult CalendarBreadcrumbControlPanel()
        {
            var model = new BreadcrumbControlPanelTemplateModel
            {
                HeaderText = "Other CALLENDAR",
                ContentViewName = "CalendarBreadcrumbControlPanelContent"
            };

            return BreadcrumbControlPanel(model);
        }

        public PartialViewResult CalendarBreadcrumbControlPanelContent()
        {
            return PartialView();
        }
    }
}