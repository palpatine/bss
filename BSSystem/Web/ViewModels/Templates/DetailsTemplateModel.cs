﻿using System.Collections.Generic;
using SolvesIt.BSSystem.Web.Controllers.Templates.Extensions;

namespace SolvesIt.BSSystem.Web.ViewModels.Templates
{
    public sealed class DetailsTemplateModel
    {
        public IEnumerable<DetailsCommand> Commands { get; set; }

        public DetailsPanelTemplateModel Details { get; set; }

        public string Entities { get; set; }

        public IEnumerable<DetailsPanel> Panels { get; set; }
    }
}