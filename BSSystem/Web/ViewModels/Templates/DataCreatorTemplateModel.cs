﻿namespace SolvesIt.BSSystem.Web.ViewModels.Templates
{
    public sealed class DataCreatorTemplateModel
    {
        public string Entities { get; set; }

        public string TypescriptController { get; set; }

        public string Permission { get; set; }
    }
}