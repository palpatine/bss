﻿namespace SolvesIt.BSSystem.Web.ViewModels.Templates
{
    public sealed class BreadcrumbControlPanelTemplateModel
    {
        public string ContentViewName { get; set; }

        public string HeaderText { get; set; }
    }
}