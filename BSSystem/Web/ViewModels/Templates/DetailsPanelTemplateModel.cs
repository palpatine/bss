﻿namespace SolvesIt.BSSystem.Web.ViewModels.Templates
{
    public sealed class DetailsPanelTemplateModel
    {
        public string Controller { get; set; }

        public string DetailsAction { get; set; }

        public string DetailsTypescriptController { get; set; }

        public string DisplayPermissionPath { get; set; }

        public string EditAction { get; set; }

        public string EditPermissionPath { get; set; }

        public string EditTypescriptController { get; set; }

        public string Entities { get; set; }
    }
}