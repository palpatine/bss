﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Web.ViewModels
{
    public sealed class ValidationResult
    {
        public IEnumerable<ValidationErrorViewModel> Errors { get; set; }

        public int? Id { get; set; }

        public bool IsValid { get; set; }

        public string Message { get; set; }
    }
}