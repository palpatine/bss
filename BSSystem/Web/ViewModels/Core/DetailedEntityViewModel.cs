﻿using System;
using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class DetailedEntityViewModel : IViewModel
    {
        [SqlType(SqlDbType.SmallDateTime, true, 16)]
        public DateTimeOffset? DateEnd { get; set; }

        [SqlType(SqlDbType.SmallDateTime, false, 16)]
        public DateTimeOffset DateStart { get; set; }

        public virtual string DisplayName { get; set; }

        public int Id { get; set; }
    }
}