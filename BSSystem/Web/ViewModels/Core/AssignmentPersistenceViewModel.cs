﻿using System;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class AssignmentPersistenceViewModel : IViewModel
    {
        public DateTimeOffset? DateEnd { get; set; }

        public DateTimeOffset DateStart { get; set; }

        public int ElementId { get; set; }

        public int Id { get; set; }
    }
}