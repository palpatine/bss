﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class RoleAssignmentPersistenceViewModel : AssignmentPersistenceViewModel
    {
        public int RoleId { get; set; }
    }
}