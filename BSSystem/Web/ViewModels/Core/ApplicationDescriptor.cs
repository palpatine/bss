﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class ApplicationDescriptor
    {
        public IEnumerable<MenuSectionViewModel> MenuSections { get; set; }

        public UserData UserData { get; set; }
    }
}