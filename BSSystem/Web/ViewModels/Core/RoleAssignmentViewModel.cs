﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class RoleAssignmentViewModel : AssignmentViewModel
    {
        public DetailedEntityViewModel Role { get; set; }
    }
}