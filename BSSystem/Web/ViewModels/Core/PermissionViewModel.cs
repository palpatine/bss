﻿using System.Diagnostics;
using System.Globalization;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Web.Properties;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class PermissionViewModel : IViewModel, IPermission
    {
        [PropertySource(Ignore = true)]
        public string Description
        {
            get
            {
                var value = WebResources.ResourceManager.GetString("PermissionDescription_" + Key);

                if (value == null)
                {
                    Debug.WriteLine("Missing resource key {0}", new[] { "PermissionDescription_" + Key });
                    value = string.Format(CultureInfo.InvariantCulture, "[{0}]", Key);
                }

                return value;
            }
        }

        [PropertySource(Ignore = true)]
        public string DisplayName
        {
            get
            {
                var value = WebResources.ResourceManager.GetString("Permission_" + Key);

                if (value == null)
                {
                    value = WebResources.ResourceManager.GetString(Key);
                    if (value == null)
                    {
                        Debug.WriteLine("Missing resource key {0} or {1}", Key, "Permission_" + Key);
                        value = string.Format(CultureInfo.InvariantCulture, "[{0}]", Key);
                    }
                }

                return value;
            }
        }

        public int Id { get; set; }

        [PropertySource(Ignore = true)]
        public bool IsContainer { get; set; }

        [PropertySource(typeof(SystemPermission))]
        public string Key { get; set; }

        [PropertySource(Ignore = true)]
        public int? ParentId { get; set; }
    }
}