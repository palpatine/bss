﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class UserDataPermissionDescriptor
    {
        public bool IsGlobal { get; set; }

        public string Path { get; set; }
    }
}