namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class MenuItemViewModel
    {
        public string DisplayShortName { get; set; }

        public string IconName { get; set; }

        public string Key { get; set; }

        public string Title { get; set; }
    }
}