﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class JobRoleAssignmentPersistenceViewModel : RoleAssignmentPersistenceViewModel
    {
        public JobKind JobKind { get; set; }

        public decimal? JobValue { get; set; }
    }
}