namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public enum JobKind
    {
        Undefined = 0,
        None = 1,
        Time,
        Hours
    }
}