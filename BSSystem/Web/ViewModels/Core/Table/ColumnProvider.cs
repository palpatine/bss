namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    internal sealed class ColumnProvider : IColumnDefinitionBuilder
    {
        private readonly ColumnDefinition _columnDefinition;

        public ColumnProvider(ColumnDefinition columnDefinition)
        {
            _columnDefinition = columnDefinition;
        }

        public ColumnDefinition Build()
        {
            return _columnDefinition;
        }
    }
}