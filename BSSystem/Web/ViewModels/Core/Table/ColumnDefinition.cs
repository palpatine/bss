﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    public sealed class ColumnDefinition
    {
        public string ColumnHeaderDisplayName { get; set; }

        public string ColumnHeaderTemplate { get; set; }

        public string ColumnHeaderTemplateUrl { get; set; }

        public string DisplayProperty { get; set; }

        public ColumnFilterDefinition Filter { get; set; }

        public bool IsActionsColumn { get; set; }

        public bool IsExpansionColumn { get; set; }

        public bool Sorted { get; set; }

        public string SortKey { get; set; }

        public string Template { get; set; }

        public string TemplateUrl { get; set; }

        public int Width { get; set; }

        public string ColumnKey { get; set; }
    }
}