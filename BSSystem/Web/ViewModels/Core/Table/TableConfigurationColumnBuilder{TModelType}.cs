﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Web.Extensions;
using SolvesIt.BSSystem.Web.Properties;

namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    internal abstract class TableConfigurationColumnBuilder<TModelType> : ITableConfigurationColumnBuilder<TModelType>, IColumnDefinitionBuilder
    {
        private readonly PropertyInfo _lastProperty;

        private readonly Type _modelType;

        private readonly string _resourceKey;

        private readonly SqlTypeAttribute _sqlTypeProvider;

        protected TableConfigurationColumnBuilder(
            Type entityType,
            IEnumerable<PropertyInfo> properties,
            string entityPropertyName = null)
        {
            _modelType = typeof(TModelType);
            _resourceKey = string.Join("_", properties.Select(x => x.Name));
            Definition.ColumnKey = properties.First().Name;

            _lastProperty = properties.Last();
            if (entityType != null)
            {
                var entityProperty = entityType.GetProperty(entityPropertyName ?? _lastProperty.Name);
                if (entityProperty == null)
                {
                    throw new InvalidOperationException("Entity property must exist.");
                }

                _sqlTypeProvider = StorableHelper.GetSqlTypeAttribute(entityType, entityProperty);
            }

            var path = string.Join(".", properties.Select(x => char.ToLower(x.Name[0], CultureInfo.InvariantCulture) + x.Name.Substring(1)));
            ModelPropertyPath = path;
        }

        public ColumnDefinition Definition { get; } = new ColumnDefinition();

        public string ModelPropertyPath { get; }

        public ColumnDefinition Build()
        {
            if (Definition.ColumnHeaderDisplayName == null
                && Definition.ColumnHeaderTemplate == null
                && Definition.ColumnHeaderTemplateUrl == null)
            {
                SetDefaultHeaderDisplayName();
            }

            if (Definition.DisplayProperty == null
                && Definition.Template == null
                && Definition.TemplateUrl == null)
            {
                SetDefaultValueDisplay();
            }

            return Definition;
        }

        public ITableConfigurationColumnBuilder<TModelType> CanFilter()
        {
            return this;
            /*
            Definition.Filter = new ColumnFilterDefinition();
            Definition.Filter.FilterKey = ModelPropertyPath;
            if (_sqlTypeProvider != null)
            {
                switch (_sqlTypeProvider.Type)
                {
                    case SqlDbType.VarChar:
                    case SqlDbType.Char:
                    case SqlDbType.NVarChar:
                    case SqlDbType.NChar:
                    case SqlDbType.NText:
                        {
                            Definition.Filter.ValueType = "string";
                            Definition.Filter.Capacity = _sqlTypeProvider.Capacity;
                            break;
                        }

                    case SqlDbType.BigInt:
                    case SqlDbType.Int:
                    case SqlDbType.SmallInt:
                        {
                            Definition.Filter.ValueType = "number";
                            Definition.Filter.Capacity = _sqlTypeProvider.Capacity;
                            break;
                        }

                    case SqlDbType.Real:
                    case SqlDbType.Decimal:
                    case SqlDbType.Float:
                        {
                            Definition.Filter.ValueType = "decimal";
                            Definition.Filter.Capacity = _sqlTypeProvider.Capacity;
                            break;
                        }

                    case SqlDbType.DateTime:
                    case SqlDbType.DateTime2:
                    case SqlDbType.DateTimeOffset:
                        {
                            Definition.Filter.ValueType = "dateTime";
                            break;
                        }

                    case SqlDbType.SmallDateTime:
                    case SqlDbType.Date:
                        {
                            Definition.Filter.ValueType = "date";
                            break;
                        }

                    case SqlDbType.Bit:
                        {
                            Definition.Filter.ValueType = "bit";
                            break;
                        }

                    default:
                        throw new NotSupportedException();
                }
            }
            else
            {
                if (_lastProperty.PropertyType == typeof(int)
                    || _lastProperty.PropertyType == typeof(long)
                    || _lastProperty.PropertyType == typeof(short))
                {
                    Definition.Filter.ValueType = "number";
                }
                else if (_lastProperty.PropertyType == typeof(double)
                         || _lastProperty.PropertyType == typeof(decimal)
                         || _lastProperty.PropertyType == typeof(float))
                {
                    Definition.Filter.ValueType = "decimal";
                }
                else if (_lastProperty.PropertyType == typeof(string)
                         || _lastProperty.PropertyType == typeof(char))
                {
                    Definition.Filter.ValueType = "string";
                }
                else if (_lastProperty.PropertyType == typeof(DateTime)
                         || _lastProperty.PropertyType == typeof(DateTime?)
                         || _lastProperty.PropertyType == typeof(DateTimeOffset)
                         || _lastProperty.PropertyType == typeof(DateTimeOffset?))
                {
                    Definition.Filter.ValueType = "dateTime";
                }
                else if (_lastProperty.PropertyType == typeof(bool)
                         || _lastProperty.PropertyType == typeof(bool?))
                {
                    Definition.Filter.ValueType = "bit";
                }
            }

            return this;*/
        }

        public ITableConfigurationColumnBuilder<TModelType> CanSort()
        {
            Definition.SortKey = ModelPropertyPath;
            return this;
        }

        public ITableConfigurationColumnBuilder<TModelType> HeaderFrom<TValue>(
            Expression<Func<TModelType, TValue>> property)
        {
            SetDisplayName(string.Join("_", property.GetProperties().Select(x => x.Name)));
            return this;
        }

        public ITableConfigurationColumnBuilder<TModelType> IsExpansionColumn()
        {
            Definition.IsExpansionColumn = true;
            return this;
        }

        public ITableConfigurationColumnBuilder<TModelType> SetHeaderTemplate(string template)
        {
            Definition.ColumnHeaderTemplate = template;
            return this;
        }

        public ITableConfigurationColumnBuilder<TModelType> SetTemplateUrl(string url)
        {
            Definition.TemplateUrl = url;
            return this;
        }

        public ITableConfigurationColumnBuilder<TModelType> SortedByDefault()
        {
            if (string.IsNullOrWhiteSpace(Definition.SortKey))
            {
                throw new InvalidOperationException("Column must be sortable before it can be sorted by default.");
            }

            Definition.Sorted = true;
            return this;
        }

        public ITableConfigurationColumnBuilder<TModelType> SortedDescendingByDefault()
        {
            if (string.IsNullOrWhiteSpace(Definition.SortKey))
            {
                throw new InvalidOperationException("Column must be sortable before it can be sorted by default.");
            }

            // todo: descending
            Definition.Sorted = true;
            return this;
        }

        public ITableConfigurationColumnBuilder<TModelType> Width(int valueInPixels)
        {
            Definition.Width = valueInPixels;
            return this;
        }

        protected abstract void DefaultDisplay(string undefinedValueReplacement);

        protected abstract void DisplayCheckbox(
            string undefinedValueReplacement,
            bool isTreeState);

        protected abstract void DisplayDate(string undefinedValueReplacement);

        protected abstract void DisplayDateTime(string undefinedValueReplacement);

        private void SetDefaultHeaderDisplayName()
        {
            SetDisplayName(_resourceKey);
        }

        private void SetDefaultValueDisplay()
        {
            string undefinedValueReplacement = null;
            if (UndefinedValueReplacementsProvider.ContainsKey(_lastProperty.Name))
            {
                undefinedValueReplacement = UndefinedValueReplacementsProvider.GetReplacementValue(_lastProperty.Name);
            }

            if (_sqlTypeProvider != null)
            {
                switch (_sqlTypeProvider.Type)
                {
                    case SqlDbType.SmallDateTime:
                        DisplayDate(undefinedValueReplacement);
                        break;

                    case SqlDbType.DateTime2:
                    case SqlDbType.DateTime:
                        DisplayDateTime(undefinedValueReplacement);
                        break;

                    case SqlDbType.Bit:
                        DisplayCheckbox(undefinedValueReplacement, _sqlTypeProvider.IsNullable);
                        break;

                    default:
                        DefaultDisplay(undefinedValueReplacement);
                        break;
                }
            }
            else
            {
                if (_lastProperty.PropertyType == typeof(DateTime)
                    || _lastProperty.PropertyType == typeof(DateTime?)
                    || _lastProperty.PropertyType == typeof(DateTimeOffset)
                    || _lastProperty.PropertyType == typeof(DateTimeOffset?))
                {
                    DisplayDateTime(undefinedValueReplacement);
                }
                else if (_lastProperty.PropertyType == typeof(bool))
                {
                    DisplayCheckbox(undefinedValueReplacement, false);
                }
                else if (_lastProperty.PropertyType == typeof(bool?))
                {
                    DisplayCheckbox(undefinedValueReplacement, true);
                }
                else
                {
                    DefaultDisplay(undefinedValueReplacement);
                }
            }
        }

        private void SetDisplayName(string resourceKey)
        {
            var value = WebResources.ResourceManager.GetString(_modelType.Name + "_ListHeader_" + resourceKey);
            if (value == null)
            {
                value = WebResources.ResourceManager.GetString("ListHeader_" + resourceKey);
                if (value == null)
                {
                    value = WebResources.ResourceManager.GetString(resourceKey);

                    if (value == null)
                    {
                        value = WebResources.ResourceManager.GetString(_lastProperty.Name);
                        if (value == null)
                        {
                            value = string.Format(CultureInfo.InvariantCulture, "[{0}]", _lastProperty.Name);
                            Debug.WriteLine(
                                "Missing resource key {0} or {1} or {2} or {3}.",
                                _lastProperty.Name,
                                resourceKey,
                                "ListHeader_" + resourceKey,
                                _modelType.Name + "_ListHeader_" + resourceKey);
                        }
                    }
                }
            }

            Definition.ColumnHeaderDisplayName = value;
        }
    }
}