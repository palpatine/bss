﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    internal sealed class TableConfigurationBuilder<TViewModel>
    {
        private readonly List<IColumnDefinitionBuilder> _columns = new List<IColumnDefinitionBuilder>();

        public void Add(IColumnDefinitionBuilder builder)
        {
            _columns.Add(builder);
        }

        public TableConfiguration Build()
        {
            return new TableConfiguration
            {
                ColumnDefinition = _columns.Select(x => x.Build()).ToArray()
            };
        }
    }
}