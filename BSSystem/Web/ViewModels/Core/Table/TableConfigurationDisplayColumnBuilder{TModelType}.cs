﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    internal class TableConfigurationDisplayColumnBuilder<TModelType> : TableConfigurationColumnBuilder<TModelType>
    {
        public TableConfigurationDisplayColumnBuilder(
            Type entityType,
            IEnumerable<PropertyInfo> properties,
            string entityPropertyName = null)
            : base(entityType, properties, entityPropertyName)
        {
        }

        protected override void DefaultDisplay(string undefinedValueReplacement)
        {
            if (undefinedValueReplacement != null)
            {
                Definition.Template = string.Format(
                    CultureInfo.InvariantCulture,
                    "{{{{item.{0} || '{1}'}}}}",
                    ModelPropertyPath,
                    undefinedValueReplacement);
            }
            else
            {
                Definition.DisplayProperty = ModelPropertyPath;
            }
        }

        protected override void DisplayCheckbox(
            string undefinedValueReplacement,
            bool isTreeState)
        {
            if (undefinedValueReplacement != null)
            {
                Definition.Template =
                    string.Format(
                    CultureInfo.InvariantCulture,
                        "<bss-checkbox ng-model='item.{0}' ng-if='item.{0} !== undefined' disabled='true'></bss-checkbox><span ng-if='item.{0} === undefined'>{1}</span>",
                        ModelPropertyPath,
                        undefinedValueReplacement);
            }
            else
            {
                Definition.Template = string.Format(
                    CultureInfo.InvariantCulture,
                    "<bss-checkbox ng-model='item.{0}' disabled='true' is-three-state=\"'{1}'\"></bss-checkbox>",
                    ModelPropertyPath,
                    isTreeState ? "true" : "false");
            }
        }

        protected override void DisplayDate(string undefinedValueReplacement)
        {
            if (undefinedValueReplacement != null)
            {
                Definition.Template = string.Format(
                    CultureInfo.InvariantCulture,
                    "{{{{(item.{0} | dateFormatter) || '{1}'}}}}",
                    ModelPropertyPath,
                    undefinedValueReplacement);
            }
            else
            {
                Definition.Template = string.Format(CultureInfo.InvariantCulture, "{{{{item.{0} | dateFormatter}}}}", ModelPropertyPath);
            }
        }

        protected override void DisplayDateTime(string undefinedValueReplacement)
        {
            if (undefinedValueReplacement != null)
            {
                Definition.Template = string.Format(
                    CultureInfo.InvariantCulture,
                    "{{{{(item.{0} | dateTimeFormatter) || '{1}'}}}}",
                    ModelPropertyPath,
                    undefinedValueReplacement);
            }
            else
            {
                Definition.Template = string.Format(CultureInfo.InvariantCulture, "{{{{item.{0} | dateTimeFormatter}}}}", ModelPropertyPath);
            }
        }
    }
}