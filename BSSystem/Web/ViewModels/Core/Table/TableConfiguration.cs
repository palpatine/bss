﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    public sealed class TableConfiguration
    {
        public IEnumerable<ColumnDefinition> ColumnDefinition { get; set; }
    }
}