﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    public sealed class ColumnFilterDefinition
    {
        public int Capacity { get; set; }

        public string FilterKey { get; set; }

        public string ValueType { get; set; }
    }
}