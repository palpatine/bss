using System;
using System.Globalization;
using System.Linq.Expressions;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    internal static class TableConfigurationBuilderExtensions
    {
        public static void AddColumn<TViewModel>(
            this TableConfigurationBuilder<TViewModel> builder,
            ColumnDefinition column)
        {
            builder.Add(new ColumnProvider(column));
        }

        public static ITableConfigurationColumnBuilder<TViewModel> AddColumn<TViewModel, TElement>(
            this TableConfigurationBuilder<TViewModel> builder,
            Expression<Func<TViewModel, TElement>> property)
        where TViewModel : IMarker
        {
            var columnBuilder = new TableConfigurationDisplayColumnBuilder<TViewModel>(typeof(TViewModel), property.GetProperties());
            builder.Add(columnBuilder);
            return columnBuilder;
        }

        public static ITableConfigurationColumnBuilder<TViewModel> AddColumn<TViewModel, TElement>(
            this TableConfigurationBuilder<TViewModel> builder,
            Expression<Func<TViewModel, TElement>> property,
            Type propertySourceEntity,
            string sourceEntityPropertyName = null)
        {
            var columnBuilder = new TableConfigurationDisplayColumnBuilder<TViewModel>(propertySourceEntity, property.GetProperties(), sourceEntityPropertyName);
            builder.Add(columnBuilder);
            return columnBuilder;
        }

        public static ITableConfigurationColumnBuilder<TViewModel> AddColumn<TViewModel, TElement, TPropertySource, TPropertyValue>(
            this TableConfigurationBuilder<TViewModel> builder,
            Expression<Func<TViewModel, TElement>> property,
            Expression<Func<TPropertySource, TPropertyValue>> propertySource)
        {
            var columnBuilder = new TableConfigurationDisplayColumnBuilder<TViewModel>(typeof(TPropertySource), property.GetProperties(), propertySource.GetDeclaredProperty().Name);
            builder.Add(columnBuilder);
            return columnBuilder;
        }

        public static void AddDefaultActionsColumn<TViewModel>(
            this TableConfigurationBuilder<TViewModel> builder,
            string createPermission,
            string detailsPermission)
            where TViewModel : IStorable
        {
            var entityType = StorableHelper.GetEntityType(typeof(TViewModel));
            var entitiesName = entityType.Name;
            AddDefaultActionsColumn(builder, entitiesName, createPermission, detailsPermission);
        }

        public static void AddDefaultActionsColumn<TViewModel>(
            this TableConfigurationBuilder<TViewModel> builder,
            string entitiesName,
            string createPermission,
            string detailsPermission)
           where TViewModel : IStorable
        {
            var column = new ColumnDefinition
            {
                ColumnHeaderTemplate = string.Format(CultureInfo.InvariantCulture, "<bss-actions kind='Create' entities='{0}' permission='{1}' />", entitiesName, createPermission),
                Template = string.Format(CultureInfo.InvariantCulture, "<bss-actions kind='Details' entities='{0}' id='item.id' permission='{1}' />", entitiesName, detailsPermission),
                IsActionsColumn = true
            };

            builder.Add(new ColumnProvider(column));
        }

        public static void AddDetailsActionColumn<TViewModel>(
            this TableConfigurationBuilder<TViewModel> builder,
            Type entityType,
            Expression<Func<TViewModel, int>> idIndicator)
        {
            var idPath = idIndicator.GetCamelCasePropertyPath();

            var column = new ColumnDefinition
            {
                ColumnHeaderTemplate = string.Empty,
                Template = string.Format(CultureInfo.InvariantCulture, "<bss-actions kind='Details' entities='{0}' id='item.{1}'/>", entityType.Name, idPath),
                IsActionsColumn = true
            };

            builder.Add(new ColumnProvider(column));
        }

        public static void AddDetailsActionColumn<TViewModel>(this TableConfigurationBuilder<TViewModel> builder)
            where TViewModel : IStorable
        {
            var entityType = StorableHelper.GetEntityType(typeof(TViewModel));
            AddDetailsActionColumn(builder, entityType, x => x.Id);
        }

        public static ITableConfigurationColumnBuilder<TViewModel> AddEditColumn<TViewModel, TElement>(
            this TableConfigurationBuilder<TViewModel> builder,
            Expression<Func<TViewModel, TElement>> property)
        where TViewModel : IMarker
        {
            var columnBuilder = new TableConfigurationEditColumnBuilder<TViewModel>(typeof(TViewModel), property.GetProperties());
            builder.Add(columnBuilder);
            return columnBuilder;
        }

        public static ITableConfigurationColumnBuilder<TViewModel> AddNonEntityColumn<TViewModel, TElement>(
            this TableConfigurationBuilder<TViewModel> builder,
            Expression<Func<TViewModel, TElement>> property)
        {
            var columnBuilder = new TableConfigurationDisplayColumnBuilder<TViewModel>(null, property.GetProperties());
            builder.Add(columnBuilder);
            return columnBuilder;
        }
    }
}