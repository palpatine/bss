﻿using System;
using System.Linq.Expressions;

namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    internal interface ITableConfigurationColumnBuilder<TModelType>
    {
        ITableConfigurationColumnBuilder<TModelType> CanFilter();

        ITableConfigurationColumnBuilder<TModelType> CanSort();

        ITableConfigurationColumnBuilder<TModelType> HeaderFrom<TValue>(Expression<Func<TModelType, TValue>> property);

        ITableConfigurationColumnBuilder<TModelType> IsExpansionColumn();

        ITableConfigurationColumnBuilder<TModelType> SetHeaderTemplate(string template);

        ITableConfigurationColumnBuilder<TModelType> SetTemplateUrl(string url);

        ITableConfigurationColumnBuilder<TModelType> SortedByDefault();

        ITableConfigurationColumnBuilder<TModelType> SortedDescendingByDefault();

        ITableConfigurationColumnBuilder<TModelType> Width(int valueInPixels);
    }
}