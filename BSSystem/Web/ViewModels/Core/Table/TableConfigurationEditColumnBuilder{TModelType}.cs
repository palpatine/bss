﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    internal class TableConfigurationEditColumnBuilder<TModelType>
        : TableConfigurationColumnBuilder<TModelType>
    {
        public TableConfigurationEditColumnBuilder(
            Type entityType,
            IEnumerable<PropertyInfo> properties,
            string entityPropertyName = null)
            : base(entityType, properties, entityPropertyName)
        {
        }

        protected override void DefaultDisplay(string undefinedValueReplacement)
        {
            Definition.Template = string.Format(CultureInfo.InvariantCulture, "<input type='text' ng-model='item.{0}' >", ModelPropertyPath);
        }

        protected override void DisplayCheckbox(
            string undefinedValueReplacement,
            bool isTreeState)
        {
            Definition.Template = string.Format(
                CultureInfo.InvariantCulture,
                "<bss-checkbox ng-model='item.{0}'  is-three-state=\"'{1}'\"></bss-checkbox>",
                ModelPropertyPath,
                isTreeState ? "true" : "false");
        }

        protected override void DisplayDate(string undefinedValueReplacement)
        {
            Definition.Template = string.Format(CultureInfo.InvariantCulture, "{{{{item.{0} | dateFormatter}}}}", ModelPropertyPath);
        }

        protected override void DisplayDateTime(string undefinedValueReplacement)
        {
            Definition.Template = string.Format(CultureInfo.InvariantCulture, "<bss-datepicker ng-model='item.{0}' ></bss-datepicker>", ModelPropertyPath);
        }
    }
}