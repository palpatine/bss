﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core.Table
{
    internal interface IColumnDefinitionBuilder
    {
        ColumnDefinition Build();
    }
}