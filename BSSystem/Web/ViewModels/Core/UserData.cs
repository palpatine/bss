﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class UserData
    {
        public IEnumerable<UserDataPermissionDescriptor> EffectivePermissions { get; set; }

        public NamedViewModel Organization { get; set; }

        public NamedViewModel User { get; set; }

        public IEnumerable<string> Modules { get; set; }
    }
}