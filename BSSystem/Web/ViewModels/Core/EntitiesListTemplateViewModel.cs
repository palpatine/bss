﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class EntitiesListTemplateViewModel
    {
        public string ColumnsSource { get; set; }

        public string DataSource { get; set; }

        public string Entities { get; set; }
    }
}