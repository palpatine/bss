﻿using System.Collections.Generic;
using SolvesIt.BSSystem.Core.Abstraction.Environment;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class MenuSectionViewModel
    {
        public string IconName { get; set; }

        public int Id { get; set; }

        public string Key { get; set; }

        public IEnumerable<MenuItemViewModel> MenuItems { get; set; }

        public IEnumerable<MenuSectionViewModel> MenuSections { get; set; }

        public NavigationPositionKind NavigationPosition { get; set; }

        public string Title { get; set; }
    }
}