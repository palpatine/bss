namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class LockResultViewModel
    {
        public string OperationToken { get; set; }

        public ValidationResult ValidationResult { get; set; }
    }
}