﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class TimelessAssignmentViewModel : IViewModel
    {
        public DetailedEntityViewModel Element { get; set; }

        public int Id { get; set; }
    }
}