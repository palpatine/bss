﻿using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class PermissionAssignmentViewModel : PermissionViewModel
    {
        [PropertySource(Ignore = true)]
        public bool IsAssigned => JunctionId.HasValue;

        [PropertySource(Ignore = true)]
        public int? JunctionId { get; set; }
    }
}