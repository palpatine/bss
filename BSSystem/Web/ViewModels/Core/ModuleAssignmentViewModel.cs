﻿using System;
using SolvesIt.BSSystem.Web.Areas.Common.ViewModels;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class ModuleAssignmentViewModel : IViewModel
    {
        public DateTimeOffset? DateEnd { get; set; }

        public DateTimeOffset DateStart { get; set; }

        public ModuleViewModel Element { get; set; }

        public int Id { get; set; }
    }
}