﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class AuthenticationViewModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Justification = "Used as sitring identifing user and organization that he wants to connect to.")]
        public string Login { get; set; }

        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}