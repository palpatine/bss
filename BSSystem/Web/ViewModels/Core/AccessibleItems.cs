﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class AccessibleItems
    {
        public bool GetAll { get; set; }

        public IEnumerable<int> Values { get; set; }
    }
}