﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class RelationDetailsPanelTemplateViewModel
    {
        public bool CanAssignJobTime { get; set; }

        public bool CanAssignRole { get; set; }

        public string Elements { get; set; }

        public string Entities { get; set; }
    }
}