﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class JobRoleAssignmentViewModel : RoleAssignmentViewModel
    {
        public JobKind JobKind { get; set; }

        public decimal? JobValue { get; set; }
    }
}