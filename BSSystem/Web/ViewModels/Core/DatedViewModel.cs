﻿using System;

namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class DatedViewModel : NamedViewModel
    {
        public DateTime? DateEnd { get; set; }

        public DateTime DateStart { get; set; }
    }
}