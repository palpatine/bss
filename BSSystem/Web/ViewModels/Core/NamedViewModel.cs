﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public class NamedViewModel
        : IViewModel
    {
        public string DisplayName { get; set; }

        public int Id { get; set; }
    }
}