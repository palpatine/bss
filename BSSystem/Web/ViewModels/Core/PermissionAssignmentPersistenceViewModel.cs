﻿namespace SolvesIt.BSSystem.Web.ViewModels.Core
{
    public sealed class PermissionAssignmentPersistenceViewModel
    {
        public int Id { get; set; }

        public int? JunctionId { get; set; }
    }
}