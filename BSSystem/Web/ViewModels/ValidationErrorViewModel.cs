﻿namespace SolvesIt.BSSystem.Web.ViewModels
{
    public sealed class ValidationErrorViewModel
    {
        public string AdditionalValue { get; set; }

        public string ErrorCode { get; set; }

        public string Message { get; set; }

        public string Reference { get; set; }
    }
}