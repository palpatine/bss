﻿using System;

namespace SolvesIt.BSSystem.Web.ViewModels.Api
{
    public sealed class SignForListViewModel
    {
        public DateTime? DateEnd { get; set; }

        public DateTime? DateStart { get; set; }

        public int Id { get; set; }

        public bool IsEditable { get; set; }

        public bool IsInViews { get; set; }

        public bool IsNullable { get; set; }

        public bool IsRequired { get; set; }

        public bool IsUnique { get; set; }

        public string Name { get; set; }

        public string Schema { get; set; }

        public string TargetName { get; set; }
    }
}