﻿interface JQueryStatic {
    cookie: {
        (key: string, value?: any, options?: ICookieOptions);
        json: boolean;
        defaults:ICookieOptions;
    }
}

interface ICookieOptions {
    expires: Date;
    path: string;
    domain: string;
    secure: boolean;
}