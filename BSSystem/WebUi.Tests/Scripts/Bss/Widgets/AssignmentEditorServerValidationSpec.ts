///<references path="../../typings/jasmine.d.ts" />

describe(
    "Assignment editor validator contorller should recognize server validation errors and match them with appropriate model controllers.", function () {

        interface ITestScope extends ng.IScope {
            errors: SolvesIt.Bss.Models.IValidationError[];
        }

        class TestHelper {

            static dateStartError: string = "DateStartError";
            static dateEndError: string = "DateEndError";

            public static createItem(id: number): SolvesIt.Bss.Widgets.AssignmentEditor.IAssignmentEditorElement {
                return {
                    id: id,
                    $minDate: undefined,
                    $maxDate: undefined,
                    $roleMinDate: undefined,
                    $roleMaxDate: undefined,
                    jobKind: undefined,
                    jobValue: undefined,
                    role: undefined,
                    dateStart: undefined,
                    dateEnd: undefined,
                    element: undefined,
                };
            }

            public static createDateOverlapError(masterId: number, relatedItemId: number): SolvesIt.Bss.Models.IValidationError {
                return {
                    additionalValue: relatedItemId.toString(),
                    message: null,
                    errorCode: "DateOverlapError",
                    reference: "[" + masterId + "].Element"
                };
            }

            public static createDateStartError(id: number): SolvesIt.Bss.Models.IValidationError {
                return {
                    additionalValue: "2015-07-29",
                    message: null,
                    errorCode: "DateStartError",
                    reference: "[" + id + "].DateStart"
                }
            }
        };

        var $rootScope: ng.IScope;
        var testSubject: SolvesIt.Bss.Widgets.AssignmentEditor.IBssAssignmentEditorValidatorController;
        var $scope: ITestScope;

        beforeEach(angular.mock.module('bss'));
        beforeEach(inject(function (_$controller_, _$rootScope_) {
            $rootScope = _$rootScope_;
            $scope = _$rootScope_.$new();
            $scope.errors = [];
            testSubject = _$controller_('bssAssignmentEditorValidatorController', {
                $scope: $scope,
                $attrs: { bssAssignmentEditorValidator: "errors" },
                adDebounce: (functionToDebounce: Function) => { return function () { functionToDebounce.apply(testSubject, arguments); } }
            });
        }));

        it("DateStartError is propagated to validity handler, server error gets set", function () {
            var item = TestHelper.createItem(2);

            testSubject.setModelController({
                $viewValue: [
                    item
                ]
            });
            var $setValiditySpy = jasmine.createSpy("$setValidity");
            var validityHandler: SolvesIt.Bss.Widgets.AssignmentEditor.IValidityHandler = {
                $error: {},
                $setValidity: $setValiditySpy
            };
            testSubject.register(validityHandler, 2, "DateStart");
            var errors: SolvesIt.Bss.Models.IValidationError[] = [
                TestHelper.createDateStartError(2)
            ];

            $scope.errors = errors;
            $scope.$digest();

            expect($setValiditySpy).toHaveBeenCalledWith("serverDateStartError", false);
            expect(testSubject.model[2]["DateStart"].errors).toBeDefined();
            expect(testSubject.model[2]["DateStart"].errors[0]).toBe(errors[0]);
        });

        it("Client side validation can be performed with serverDateStartError error present, server error gets reseted", function () {

            var item = TestHelper.createItem(2);

            testSubject.setModelController({
                $viewValue: [
                    item
                ]
            });

            var $setValiditySpy = jasmine.createSpy("$setValidity");
            var validityHandler: SolvesIt.Bss.Widgets.AssignmentEditor.IValidityHandler = {
                $error: {},
                $setValidity: $setValiditySpy
            };
            testSubject.register(validityHandler, 2, "DateStart");
            var errors: SolvesIt.Bss.Models.IValidationError[] = [
                TestHelper.createDateStartError(2)
            ];

            $scope.errors = errors;
            $scope.$digest();

            testSubject.validate(2);

            expect(testSubject.model[2]["DateStart"].errors.length).toBe(0);
        });

        it("DateOverlapError is propagated to validity handler, server error gets set", function () {
            var item1 = TestHelper.createItem(2);
            var item2 = TestHelper.createItem(-20);

            testSubject.setModelController({
                $viewValue: [
                    item1,
                    item2
                ]
            });
            var $setValiditySpy1 = jasmine.createSpy("$setValidity1");
            var validityHandler1: SolvesIt.Bss.Widgets.AssignmentEditor.IValidityHandler = {
                $error: {},
                $setValidity: $setValiditySpy1
            };
            var $setValiditySpy2 = jasmine.createSpy("$setValidity2");
            var validityHandler2: SolvesIt.Bss.Widgets.AssignmentEditor.IValidityHandler = {
                $error: {},
                $setValidity: $setValiditySpy2
            };
            testSubject.register(validityHandler1, 2, "Element");
            testSubject.register(validityHandler2, -20, "Element");
            var errors: SolvesIt.Bss.Models.IValidationError[] = [
                TestHelper.createDateOverlapError(2, -20),
                TestHelper.createDateOverlapError(-20, 2)
            ];

            $scope.errors = errors;
            $scope.$digest();

            expect($setValiditySpy1).toHaveBeenCalledWith("serverDateOverlapError", false);
            expect(testSubject.model[2]["Element"].errors).toBeDefined();
            expect(testSubject.model[2]["Element"].errors[0]).toBe(errors[0]);

            expect($setValiditySpy2).toHaveBeenCalledWith("serverDateOverlapError", false);
            expect(testSubject.model[-20]["Element"].errors).toBeDefined();
            expect(testSubject.model[-20]["Element"].errors[0]).toBe(errors[1]);
        });

        it("Client side validation can be performed with serverDateOverlapError error present, server error gets reseted", function () {
            var item1 = TestHelper.createItem(2);
            var item2 = TestHelper.createItem(-20);

            testSubject.setModelController({
                $viewValue: [
                    item1,
                    item2
                ]
            });
            var $setValiditySpy1 = jasmine.createSpy("$setValidity1");
            var validityHandler1: SolvesIt.Bss.Widgets.AssignmentEditor.IValidityHandler = {
                $error: {},
                $setValidity: $setValiditySpy1
            };
            var $setValiditySpy2 = jasmine.createSpy("$setValidity2");
            var validityHandler2: SolvesIt.Bss.Widgets.AssignmentEditor.IValidityHandler = {
                $error: {},
                $setValidity: $setValiditySpy2
            };
            testSubject.register(validityHandler1, 2, "Element");
            testSubject.register(validityHandler2, -20, "Element");
            var errors: SolvesIt.Bss.Models.IValidationError[] = [
                TestHelper.createDateOverlapError(2, -20),
                TestHelper.createDateOverlapError(-20, 2)
            ];

            $scope.errors = errors;
            $scope.$digest();

            testSubject.validate(2);

            expect(testSubject.model[2]["Element"].errors.length).toBe(0);
            expect(testSubject.model[-20]["Element"].errors.length).toBe(0);
        });

    });

