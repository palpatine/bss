// Karma configuration

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [
            //  { pattern: '../../Web/Content/**/*.*', watched: false, included: false, served: true },
            { pattern: 'GeneratedScriptStub.js', watched: true, included: true, served: true },
            { pattern: '../../Web/Scripts/Lib/*.js', watched: true, included: true, served: true },
            { pattern: '../../Web/Scripts/Lib/MomentJs/*.js', watched: true, included: true, served: true },
            //'../../../node_modules/angular/angular.js',
            //'../../../node_modules/angular-messages/angular-messages.js',
            //'../../../node_modules/angular-route/angular-route.js',
            { pattern: '../../Web/Scripts/Lib/Angular/angular.js', watched: true, included: true, served: true },
            //    '../../Web/Scripts/Lib/Angular/Modules/*.js',
            // '../../Web/Scripts/Lib/Angular/Modules/angular-scenario.js',
            //  '../../Web/Scripts/Lib/Angular/Modules/angular-animate.js',
            //  '../../Web/Scripts/Lib/Angular/Modules/angular-aria.js',
            //  '../../Web/Scripts/Lib/Angular/Modules/angular-cookies.js',
            //  '../../Web/Scripts/Lib/Angular/Modules/angular-loader.js',
            { pattern: '../../Web/Scripts/Lib/Angular/Modules/angular-messages.js', watched: true, included: true, served: true },
            //'../../Web/Scripts/Lib/Angular/Modules/angular-resource.js',
            { pattern: '../../Web/Scripts/Lib/Angular/Modules/angular-route.js', watched: true, included: true, served: true },
            //  '../../Web/Scripts/Lib/Angular/Modules/angular-sanitize.js',
            //  '../../Web/Scripts/Lib/Angular/Modules/angular-touch.js',
            { pattern: '../../Web/Scripts/Lib/Angular/Modules/adapt/*.js', watched: true, included: true, served: true },
            { pattern: '../../Web/Scripts/Lib/Angular/Modules/checkbox/*.js', watched: true, included: true, served: true },
            { pattern: '../../Web/Scripts/Lib/Angular/Modules/Calendar/*.js', watched: true, included: true, served: true },
            { pattern: '../../Web/Scripts/Lib/Angular/Modules/http-auth/*.js', watched: true, included: true, served: true },
            { pattern: '../../Web/Scripts/Lib/Bootstrap/*.js', watched: true, included: true, served: true },
            { pattern: '../../Web/Scripts/Lib/Bootstrap/plugins/**/*.js', watched: true, included: true, served: true },
            { pattern: '../../Web/Scripts/Lib/angular-bootstap-colorpicker/*.js', watched: true, included: true, served: true },
            //'../../Web/Scripts/Lib/Angular/**/*.js',
            //'../../Web/Scripts/Lib/**/*.js',
            { pattern: "../../Web/Scripts/System/Bss/Bootstrap.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/Integration/**/*.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Scripts/System/Widgets/**/*.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Scripts/System/LogOn/LogonController.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/Accessors/GenericEntityAccessor.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/**/*.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Scripts/System/Bss/**/*.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Areas/Common/Scripts/**/*.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Areas/Wtt/Scripts/**/*.js", watched: true, included: true, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Bootstrap.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/Integration/**/*.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Widgets/**/*.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/LogOn/LogonController.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/Accessors/GenericEntityAccessor.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/**/*.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/**/*.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Areas/Common/Scripts/**/*.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Areas/Wtt/Scripts/**/*.js.map", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Bootstrap.ts", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/Integration/**/*.ts", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Widgets/**/*.ts", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/LogOn/LogonController.ts", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/Accessors/GenericEntityAccessor.ts", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/Core/**/*.ts", watched: true, included: false, served: true },
            { pattern: "../../Web/Scripts/System/Bss/**/*.ts", watched: true, included: false, served: true },
            { pattern: "../../Web/Areas/Common/Scripts/**/*.ts", watched: true, included: false, served: true },
            { pattern: "../../Web/Areas/Wtt/Scripts/**/*.ts", watched: true, included: false, served: true },
            { pattern: '../../../node_modules/angular-mocks/angular-mocks.js', watched: true, included: true, served: true },
            //  { pattern: 'angular-mocks-override.js', watched: true, included: true, served: true },
            { pattern: "../../Web/Content/**/*.html", watched: false, included: true, served: true },
            { pattern: '**/*Spec.js', watched: true, included: true, served: true },
            { pattern: '**/*Spec.js.map', watched: true, included: false, served: true },
            { pattern: '**/*Spec.ts', watched: true, included: false, served: true }
        ],


        // list of files to exclude
        exclude: [
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            "../../Web/Content/**/*.html": "ng-html2js"
        },

        ngHtml2JsPreprocessor: {
            // strip this from the file path
            // stripPrefix: 'public/',
            //  stripSuffix: '.ext',
            // prepend this to the
            //  prependPrefix: 'served/',

            // or define a custom transform function
            // - cacheId returned is used to load template
            //   module(cacheId) will return template at filepath
            cacheIdFromPath: function (filepath) {
                var cacheId = filepath.substr(filepath.indexOf("/Web/") + 4);
                return cacheId;
            },

            // - setting this option will create only a single module that contains templates
            //   from all the files, so you can load them all with module('foo')
            // - you may provide a function(htmlPath, originalPath) instead of a string
            //   if you'd like to generate modules dynamically
            //   htmlPath is a originalPath stripped and/or prepended
            //   with all provided suffixes and prefixes
            moduleName: 'bss'
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'html', 'xml'],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_WARN,

        captureTimeout: 90000,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome', 'Firefox', 'IE'],
        // browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false
    });
}
