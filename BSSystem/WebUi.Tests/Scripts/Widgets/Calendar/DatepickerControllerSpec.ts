﻿///<references path="../../typings/jasmine.d.ts" />

describe(
    "Datepicker control", function () {
        var $compile: ng.ICompileService;
        var $rootScope: ng.IScope;
        var $sniffer;
        return;
        beforeEach(angular.mock.module('bss'));

        beforeEach(inject((_$compile_, _$rootScope_, _$sniffer_) => {
            $compile = _$compile_;
            $rootScope = _$rootScope_;            
            $sniffer = _$sniffer_;
        }))

        interface ITestScope extends ng.IScope {
            data: { value: Date };
            form: ng.IFormController;
        }

        it("should recognize correct date if it is typed in", function () {
            var $scope = <ITestScope>$rootScope.$new();
            $scope.data = {
                value: new Date(2015, 10, 1)
            };
            var element = angular.element("<form name='form'>" +
                "<bss-datepicker name='Date' ng-model='data.value'>" +
                "</bss-datepicker>" +
                "</form>");

            var template = $compile(element)($scope);

            $scope.$digest();
            template.find("input").val("2015-03-05");
            template.find("input").triggerHandler($sniffer.hasEvent('input') ? 'input' : 'change');
            template.find("input").triggerHandler("blur");

            expect($scope.data.value.toISOString()).toBe(new Date(Date.UTC(2015, 2, 5)).toISOString());
            expect($scope.form.$valid).toBe(true);
        });
    }
); 