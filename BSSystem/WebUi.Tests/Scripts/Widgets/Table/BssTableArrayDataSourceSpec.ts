﻿describe(
    "Bss table datasource", () => {
        interface ITestScope extends ng.IScope {
            data: any[];
        }

        var $rootScope: ng.IScope;
        var testSubject: SolvesIt.Widgets.Table.IBssTableArrayDataSourceController;
        var $scope: ITestScope;
        beforeEach(angular.mock.module('bss'));
        beforeEach(inject(function (_$controller_, _$rootScope_) {
            $rootScope = _$rootScope_;
            $scope = _$rootScope_.$new();
            testSubject = _$controller_('bssTableArrayDataSourceController', {
                $scope: $scope,
                $attrs: { bssTableArrayDataSource: "data" },
                adDebounce: (functionToDebounce: Function) => { return function () { functionToDebounce.apply(testSubject, arguments); } }
            });

        }));

        it("should assigne given array as table items", () => {
            $scope.data = [{ a: 'a' }, { a: 'b' }];
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list).toBe($scope.data);
        });

        it("should assigne null as table items when data is not provided", () => {
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list).toBeNull();
        });

        it("should assigne sorted by first level property data given appropriate order properties", () => {
            $scope.data = [{ a: 'b' }, { a: 'a' }];
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [{ propertyName: 'a' }];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list[0].a).toBe('a');
            expect(tableControllerMock.state.items.list[1].a).toBe('b');
        });

        it("should assigne sorted descencing by first level property data given appropriate order properties", () => {
            $scope.data = [{ a: 'b' }, { a: 'a' }];
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [{ propertyName: 'a', orderDescending: true }];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list[0].a).toBe('b');
            expect(tableControllerMock.state.items.list[1].a).toBe('a');
        });

        it("should assigne sorted by second level property data given appropriate order properties", () => {
            $scope.data = [{ a: { i: 'b' } }, { a: { i: 'a' } }];
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [{ propertyName: 'a.i' }];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list[0].a.i).toBe('a');
            expect(tableControllerMock.state.items.list[1].a.i).toBe('b');
        });

        it("should assigne sorted descencing by second level property data given appropriate order properties", () => {
            $scope.data = [{ a: { i: 'b' } }, { a: { i: 'a' } }];
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [{ propertyName: 'a.i', orderDescending: true }];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list[0].a.i).toBe('b');
            expect(tableControllerMock.state.items.list[1].a.i).toBe('a');
        });

        it("should assigne sorted by second level property with null on first level data given appropriate order properties", () => {
            $scope.data = [{ a: { i: 'b' } }, { a: null }, { a: { i: 'a' } }];
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [{ propertyName: 'a.i' }];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list[0].a).toBeNull();
            expect(tableControllerMock.state.items.list[1].a).not.toBeNull();
            expect(tableControllerMock.state.items.list[1].a.i).toBe('a');
            expect(tableControllerMock.state.items.list[2].a).not.toBeNull();
            expect(tableControllerMock.state.items.list[2].a.i).toBe('b');
        });

        it("should assigne sorted descending by second level property with null on first level data given appropriate order properties", () => {
            $scope.data = [{ a: { i: 'b' } }, { a: null }, { a: { i: 'a' } }];
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [{ propertyName: 'a.i', orderDescending: true }];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list[0].a).toBeNull();
            expect(tableControllerMock.state.items.list[1].a).not.toBeNull();
            expect(tableControllerMock.state.items.list[1].a.i).toBe('b');
            expect(tableControllerMock.state.items.list[2].a).not.toBeNull();
            expect(tableControllerMock.state.items.list[2].a.i).toBe('a');
        });

        it("should assigne sorted by multiple properties data given appropriate order properties", () => {
            $scope.data = [{ a: { i: 'd' }, b: 'c' }, { a: { i: 'b' }, b: 'c' }, { a: { i: 'a' }, b: "z" }];
            var tableControllerMock = {
                getOrderProperties: () => {
                    return [{ propertyName: 'b' }, { propertyName: 'a.i' }];
                },
                state: { items: { list: undefined } }
            };
            testSubject.setTableController(<any>tableControllerMock);
            $scope.$digest();
            testSubject.loadItems();

            expect(tableControllerMock.state.items.list[0].a.i).toBe('b');
            expect(tableControllerMock.state.items.list[1].a.i).toBe('d');
            expect(tableControllerMock.state.items.list[2].a.i).toBe('a');
        });


    }); 