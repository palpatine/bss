﻿
describe(
    "Nullable form control",  () => {

        var $compile: ng.ICompileService;
        var $rootScope: ng.IScope;
        var $sniffer;

        beforeEach(() => {
            var module = angular.module("bss_test", ["bss"])
            module.run(["$templateCache", ($templateCache) => {
                $templateCache.put("template/nullableFormControl/text.tpl.html",
                    "<input type='text' />");
            }]);
            angular.mock.module('bss_test')
        });

        beforeEach(inject((_$compile_, _$rootScope_, _$sniffer_) => {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            $sniffer = _$sniffer_;
        }));

        interface ITestScope extends ng.IScope {
            data: { value: string };
        }

        it("should not check checkbox on null value", () => {
            var $scope = <ITestScope>$rootScope.$new();
            $scope.data = {
                value: null
            };
            var element = angular.element(
                "<bss-nullable-form-control template-url='\"template/nullableFormControl/text.tpl.html\"' ng-model='data.value' />");

            var template = $compile(element)($scope);

            $scope.$digest();

            expect((<any>template.find("> *").scope()).vm.checked).toBeFalsy();
        });

        it("should check checkbox on not null value", () => {
            var $scope = <ITestScope>$rootScope.$new();
            $scope.data = {
                value: "VALUE"
            };
            var element = angular.element(
                "<bss-nullable-form-control template-url='\"template/nullableFormControl/text.tpl.html\"' ng-model='data.value' />");

            var template = $compile(element)($scope);

            $scope.$digest();

            expect((<any>template.find("> *").scope()).vm.checked).toBeTruthy();
        });

        it("should check checkbox on button click and set model value to empty string", () => {
            var $scope = <ITestScope>$rootScope.$new();
            $scope.data = {
                value: null
            };
            var element = angular.element(
                "<bss-nullable-form-control template-url='\"template/nullableFormControl/text.tpl.html\"' ng-model='data.value' />");

            var template = $compile(element)($scope);

            $scope.$digest();
            template.find("button").trigger("click");
            expect((<any>template.find("> *").scope()).vm.checked).toBeTruthy();
            expect($scope.data.value).toBe("");
        });

        it("should refresh checked state if data is set after control is processed", () => {
            var $scope = <ITestScope>$rootScope.$new();
            
            var element = angular.element(
                "<bss-nullable-form-control template-url='\"template/nullableFormControl/text.tpl.html\"' ng-model='data.value' />");

            var template = $compile(element)($scope);
            $scope.$digest();

            $scope.data = { 
                value: "VALUE"
            };

            $scope.$digest();

            expect((<any>template.find("> *").scope()).vm.checked).toBeTruthy();
        });

        it("with text input - should check checkbox on button click and set model value to empty string with lazy loading", () => {
            var $scope = <ITestScope>$rootScope.$new();
            var element = angular.element(
                "<bss-nullable-form-control template-url='\"~/Content/Templates/NullableFormControl/InputElement.tpl.html\"' ng-model='data.value' />");

            var template = $compile(element)($scope);
            $scope.$digest();

            $scope.data = {
                value: null
            };

            $scope.$digest();
            template.find("button").trigger("click");
            $scope.$digest();

            expect((<any>template.find("> *").scope()).vm.checked).toBeTruthy();
            expect($scope.data.value).toBe("");
        });

        //it("with datepicker - should check checkbox on button click and set model value to empty string with lazy loading", () => {
        //    var $scope = <ITestScope>$rootScope.$new();
        //    var element = angular.element(
        //        "<bss-nullable-form-control template-url='\"~/Content/Templates/NullableFormControl/DateElement.tpl.html\"' ng-model='data.value' />");

        //    var template = $compile(element)($scope);
        //    $scope.$digest();

        //    $scope.data = {
        //        value: null
        //    };

        //    $scope.$digest();
        //    template.find("button").trigger("click");
        //    $scope.$digest();

        //    expect((<any>template.find("> *").scope()).vm.checked).toBeTruthy();
        //    expect($scope.data.value).toBe("");
        //});

    });