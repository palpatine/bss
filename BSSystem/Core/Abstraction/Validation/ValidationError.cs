﻿namespace SolvesIt.BSSystem.Core.Abstraction.Validation
{
    public sealed class ValidationError
    {
        public ValidationError(
            string validationMessage,
            string errorCode,
            string reference = null,
            string additionalValue = null)
        {
            ErrorCode = errorCode;
            ValidationMessage = validationMessage;
            Reference = reference;
            AdditionalValue = additionalValue;
        }

        public string ErrorCode { get; set; }

        public string AdditionalValue { get; set; }

        public object Metadata { get; set; }

        public string Reference { get; private set; }

        public string ValidationMessage { get; private set; }
    }
}