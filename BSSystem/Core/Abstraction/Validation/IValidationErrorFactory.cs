﻿using System.Reflection;

namespace SolvesIt.BSSystem.Core.Abstraction.Validation
{
    public interface IValidationErrorFactory
    {
        ValidationError Create(
            string errorCode,
            PropertyInfo property,
            string baseReference,
            object[] messageParameters);
    }
}