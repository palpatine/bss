﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SolvesIt.BSSystem.Core.Abstraction.Validation
{
    public interface IValidationResult<TEntity>
    {
        TEntity Entity { get; set; }

        IEnumerable<ValidationError> Errors { get; }

        string BaseReference { get; set; }

        void AddError<TValue>(
            Expression<Func<TEntity, TValue>> reference,
            string sufix,
            params object[] messageParameters);

        void AddError(
            string errorCode,
            string reference,
            string localizedMessage);

        void AddErrorWithMetadata<TValue>(
            Expression<Func<TEntity, TValue>> reference,
            string sufix,
            object metadata,
            params object[] messageParameters);
    }
}