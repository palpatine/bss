﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Core.Abstraction.Validation
{
    public interface IJunctionValidationResult<TMaster, TModel>
    {
        IEnumerable<TModel> Assignments { get; }

        TMaster Master { get; }

        IEnumerable<ValidationError> Errors { get; }

        IValidationResult<TModel> GetValidator(TModel assignment);
    }
}