﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.Core.Abstraction.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class PropertySourceAttribute : Attribute
    {
        public PropertySourceAttribute(Type sourceEntityType)
        {
            SourceEntityType = sourceEntityType;
        }

        public PropertySourceAttribute()
        {
        }

        public PropertySourceAttribute(
            Type sourceEntityType,
            string propertyName)
        {
            SourceEntityType = sourceEntityType;
            PropertyName = propertyName;
        }

        public Type SourceEntityType { get; private set; }

        public string PropertyName { get; private set; }

        public bool Ignore { get; set; }
    }
}
