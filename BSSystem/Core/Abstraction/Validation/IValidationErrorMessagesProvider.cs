﻿namespace SolvesIt.BSSystem.Core.Abstraction.Validation
{
    public interface IValidationErrorMessagesProvider
    {
        bool TryGetErrorMessage(
            string errorCode,
            out string message);
    }
}