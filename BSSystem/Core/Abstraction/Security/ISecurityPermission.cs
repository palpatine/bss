﻿namespace SolvesIt.BSSystem.Core.Abstraction.Security
{
    public interface ISecurityPermission
    {
        string Path { get; }
    }
}
