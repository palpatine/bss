﻿namespace SolvesIt.BSSystem.Core.Abstraction.Security
{
    public interface ISecurityPermissionContainer
    {
        string Path { get; }
    }
}
