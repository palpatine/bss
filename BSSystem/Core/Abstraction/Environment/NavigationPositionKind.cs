﻿namespace SolvesIt.BSSystem.Core.Abstraction.Environment
{
    public enum NavigationPositionKind : byte
    {
        Side = 1,
        Top,
    }
}