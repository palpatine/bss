﻿using System;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.Core.Abstraction.Environment
{
    public interface ICache
    {
        TValue GetGlobalCacheValue<TValue>(
            string cacheId,
            Func<TValue> getItemCallback,
            TimeSpan duration)
            where TValue : class;

        TValue GetUsersCacheValue<TValue>(
            string cacheId,
            Func<TValue> getItemCallback,
            TimeSpan duration)
            where TValue : class;

        Task<TValue> GetUsersCacheValueAsync<TValue>(
            string cacheId,
            Func<Task<TValue>> getItemCallback,
            TimeSpan duration)
            where TValue : class;

        void RemoveUserCacheValue(string cacheId);

        void RemoveGlobalCacheValue(string cacheId);
    }
}