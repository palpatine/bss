﻿using System;

namespace SolvesIt.BSSystem.Core.Abstraction.Environment
{
    public interface IConfigurationProvider
    {
        string ConnectionString { get; }

        TimeSpan SessionTimeout { get; }

        bool IsCacheEnabled { get; }
    }
}