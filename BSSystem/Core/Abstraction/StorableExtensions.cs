﻿using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Abstraction
{
    public static class StorableExtensions
    {
        [QueryTranslationHander(typeof(IBssQueryTranslationHander))]
        public static bool IsEquivalent<T>(
            this T first,
            T second)
            where T : IStorable
        {
            if (Equals(first, default(T)) || Equals(second, default(T)))
            {
                return false;
            }

            return first.Id == second.Id;
        }
    }
}