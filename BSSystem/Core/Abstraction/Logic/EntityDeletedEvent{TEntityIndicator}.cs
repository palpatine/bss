﻿using System.Collections.Generic;
using Qdarc.Modules.Common.Communication;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Abstraction.Logic
{
    public sealed class EntityDeletedEvent<TEntityIndicator>
        : IEvent
        where TEntityIndicator : IStorable
    {
        public IEnumerable<TEntityIndicator> EntityList { get; set; }
    }
}