﻿using System.Collections;
using System.Collections.Generic;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Models.Extensibility
{
    public interface IEditModelExtesionsProvider
    {
        IEnumerable EditModelExtensions { get; }
    }

    public interface IEditModelExtesionsProvider<TExtesnsion> : IEditModelExtesionsProvider
        where TExtesnsion : IEditModelExtension
    {
        new IEnumerable<TExtesnsion> EditModelExtensions { get; set; }
    }
}