﻿using System;
using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess
{
    public abstract class VersionedEntity : Entity, IVersionedEntity
    {
        protected VersionedEntity()
        {
            IsDeleted = false;
        }

        [SqlReadOnly]
        [SqlType(SqlDbType.DateTime, true, 23)]
        public DateTime? TimeEnd { get; internal set; }

        [SqlReadOnly]
        [SqlType(SqlDbType.DateTime, false, 23)]
        public DateTime TimeStart { get; internal set; }

        [SqlReadOnly]
        [SqlType(SqlDbType.Bit, false, 1)]
        public bool IsDeleted { get; internal set; }

        [SqlType(SqlDbType.Int, false, 10)]
        public int ChangerId { get; internal set; }

        [SqlReadOnly]
        [SqlType(SqlDbType.UniqueIdentifier, false, 16)]
        public Guid Number { get; internal set; }
    }
}