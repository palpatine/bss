﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories
{
    public interface IEntityRepository<TEntity, TEntityIndicator>
        where TEntity : IEntity, TEntityIndicator, new()
        where TEntityIndicator : IStorable
    {
        Task DeleteAsync(TEntityIndicator indicator);

        Task DeleteAsync(Expression<Func<TEntity, bool>> where);

        void Write(TEntity entity);

        Task WriteAsync(TEntity entity);
    }
}