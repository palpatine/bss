﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories
{
    public interface ILockRepository
    {
        Task<IEnumerable<ValidationError>> TryLockAsync<TStorable>(
            TStorable entity,
            string operationToken)
            where TStorable : IStorable;

        Task<IEnumerable<ValidationError>> TryLockAsync(
            Type junctionTable,
            PropertyInfo relationProperty,
            IMarker referenceValue,
            string operationToken);

        Task RemoveLockAsync(string operationToken);
    }
}