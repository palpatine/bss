﻿using System.Data;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess
{
    public abstract class Entity : IEntity
    {
        [SqlType(SqlDbType.Int, false, 10)]
        public int Id { get; protected internal set; }

        public bool Modified { get; protected internal set; }
    }
}