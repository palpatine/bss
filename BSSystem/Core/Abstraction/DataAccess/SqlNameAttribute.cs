﻿using System;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public sealed class SqlNameAttribute : Attribute
    {
        public SqlNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}