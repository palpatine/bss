﻿using System;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public sealed class DefaultSchemaNameAttribute : Attribute
    {
        public DefaultSchemaNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}