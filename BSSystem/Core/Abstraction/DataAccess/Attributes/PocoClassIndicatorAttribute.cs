﻿using System;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public sealed class PocoClassIndicatorAttribute : Attribute
    {
    }
}