﻿using System;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SqlReadOnlyAttribute : Attribute
    {
    }
}