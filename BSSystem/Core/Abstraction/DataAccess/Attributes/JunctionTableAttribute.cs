﻿using System;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class JunctionTableAttribute : Attribute
    {
        public JunctionTableAttribute(
            Type firstEntity,
            Type secondEntity)
        {
            FirstEntity = firstEntity;
            SecondEntity = secondEntity;
        }

        public Type FirstEntity { get; private set; }

        public Type SecondEntity { get; private set; }
    }
}