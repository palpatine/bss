﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface IJunctionEntity : IVersionedEntity
    {
        IStorable First { get; }

        IStorable Second { get; }
    }
}