﻿using System;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface IVersionedEntity : IEntity
    {
        bool IsDeleted { get; }

        int ChangerId { get; }
    }
}