﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface IEntity : IStorable
    {
        bool Modified { get; }
    }
}