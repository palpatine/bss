using System;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public sealed class DatedEntityModel : IDatedEntity
    {
        public DateTime? DateEnd { get; set; }

        public DateTime DateStart { get; set; }

        public int Id { get; set; }
    }
}