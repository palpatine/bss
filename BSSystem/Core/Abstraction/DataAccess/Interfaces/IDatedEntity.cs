﻿using System;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces
{
    [PocoClassIndicator]
    public interface IDatedEntity : IMarker
    {
        DateTime? DateEnd { get; set; }

        DateTime DateStart { get; set; }
    }
}