﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql
{
    public interface IDatabaseAccessor
    {
        ITableQuery<TResult> Function<TResult>(
            string name,
            params ConstantExpression[] arguments);

        ITableQuery<TStorable> Query<TStorable, TOtherIndicator>(
            Expression<Func<TStorable, TOtherIndicator>> dyscriminator,
            TOtherIndicator other)
            where TStorable : IStorable, new()
            where TOtherIndicator : IStorable;

        ITableQuery<TStorable> Query<TStorable>()
            where TStorable : IStorable, new();

        ITableQuery<TValue> Values<TValue>(IEnumerable<TValue> values);

        ITableQuery<TStorable> Query<TStorable>(Type concreteType)
            where TStorable : IStorable;

        ITableQuery Query(Type concreteType);

        IDeletionQuery<TStorable> Delete<TStorable>()
            where TStorable : IStorable, new();
    }
}