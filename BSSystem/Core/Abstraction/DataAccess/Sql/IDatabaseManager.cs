﻿using System;
using Qdarc.Modules.Common.QueryGeneration;

namespace SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql
{
    public interface IDatabaseManager
    {
        ISqlConnection Connection { get; }

        void ComitTransaction();

        IDisposable OpenTransaction();
    }
}