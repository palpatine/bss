﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Abstraction
{
    public interface IEntityService<TEntity, TEntityIndicator>
        where TEntity : Entity
        where TEntityIndicator : IStorable
    {
        Task DeleteAsync(TEntityIndicator entity);

        Task DeleteAsync(Expression<Func<TEntity, bool>> where);

        IEnumerable<ValidationError> Write(TEntity entity);

        Task<IEnumerable<ValidationError>> WriteAsync(TEntity entity);
    }
}