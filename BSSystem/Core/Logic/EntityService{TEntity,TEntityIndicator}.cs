﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Logic;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Logic
{
    public abstract class EntityService<TEntity, TEntityIndicator>
        : IEntityService<TEntity, TEntityIndicator>
        where TEntity : Entity, TEntityIndicator, new()
        where TEntityIndicator : IStorable
    {
        private readonly IDatabaseManager _databaseManager;
        private readonly IEntityRepository<TEntity, TEntityIndicator> _entityRepository;
        private readonly IEventAggregator _eventAggregator;
        private readonly IEnumerable<IEntityValidator<TEntity>> _validators;
        private readonly IDatabaseAccessor _databaseAccessor;

        protected EntityService(
            IEventAggregator eventAggregator,
            IEntityRepository<TEntity, TEntityIndicator> entityRepository,
            IEnumerable<IEntityValidator<TEntity>> validators,
            IDatabaseManager databaseManager,
            IDatabaseAccessor databaseAccessor)
        {
            _eventAggregator = eventAggregator;
            _entityRepository = entityRepository;
            _validators = validators;
            _databaseManager = databaseManager;
            _databaseAccessor = databaseAccessor;
        }

        protected IDatabaseManager DatabaseManager => _databaseManager;

        protected IEntityRepository<TEntity, TEntityIndicator> EntityRepository => _entityRepository;

        public async Task DeleteAsync(TEntityIndicator entity)
        {
            using (_databaseManager.OpenTransaction())
            {
                await _eventAggregator.PublishAsync(new EntityDeletedEvent<TEntityIndicator> { EntityList = new[] { entity } });
                await _entityRepository.DeleteAsync(entity);
            }
        }

        public async Task DeleteAsync(Expression<Func<TEntity, bool>> where)
        {
            var deletedItems = await _databaseAccessor.Query<TEntity>()
                .Where(where)
                .ToListAsync();
            await _eventAggregator.PublishAsync(new EntityDeletedEvent<TEntityIndicator> { EntityList = deletedItems });
            await _entityRepository.DeleteAsync(where);
        }

        public virtual IEnumerable<ValidationError> Write(TEntity entity)
        {
            throw new NotSupportedException();
        }

        public async Task<IEnumerable<ValidationError>> WriteAsync(TEntity entity)
        {
            var errors = await ValidateAsync(entity);

            if (!errors.Any())
            {
                try
                {
                    await OnReadyToSaveAsync(entity);
                }
                catch (SqlValidationException ex)
                {
                    return new[] { new ValidationError(ex.ErrorCode, ex.ErrorCode) };
                }
            }

            return errors;
        }

        protected virtual async Task OnReadyToSaveAsync(TEntity entity)
        {
            await _entityRepository.WriteAsync(entity);
        }

        protected async Task<IEnumerable<ValidationError>> ValidateAsync(TEntity entity)
        {
            return (await _validators.SelectManyAsync(x => x.ValidateModelAsync(entity))).ToArray();
        }
    }
}