﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Logic
{
    public interface IAssignmentService
    {
        Task<IEnumerable<ValidationError>> UpdateAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<AssignmentUdt<TSlave>> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker;

        Task<IEnumerable<ValidationError>> UpdateAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<AssignmentJobRoleUdt<TSlave>> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker;

        Task<IEnumerable<ValidationError>> UpdateAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<AssignmentRoleUdt<TSlave>> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker;

        Task<IEnumerable<ValidationError>> UpdateAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<JunctionUdt<TSlave>> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker;
    }
}