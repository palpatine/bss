﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

[assembly: AssemblyTitle("SolvesIt.BSSystem.Common.Core")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("SolvesIt.BSSystem.Common.Core")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("9badbdc5-3811-4970-b1cd-7b495e03921d")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: InternalsVisibleTo("SolvesIt.BSSystem.Common.Tests")]
[assembly: InternalsVisibleTo("SolvesIt.BSSystem.WttModule.Logic.Tests")]
[assembly: DefaultSchemaName("Common")]