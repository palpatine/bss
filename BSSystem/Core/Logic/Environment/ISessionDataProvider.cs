﻿using System.Security.Principal;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.Environment
{
    public interface ISessionDataProvider
    {
        IUser CurrentUser { get; }

        ISession Session { get; }

        IOrganization Scope { get; }

        void Resolve(IPrincipal principal);
    }
}