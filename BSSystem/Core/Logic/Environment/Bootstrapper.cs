﻿using System.Net.Configuration;
using System.Web;
using System.Web.Configuration;
using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Notification;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.Core.Logic.Environment
{
    public sealed class Bootstrapper : IBootstrapper
    {
        public double Order => 0;

        public void Bootstrap(IRegistrar registrar)
        {
            registrar.RegisterAsPerHttpRequest<IDatabaseManager, DatabaseManager>();

            registrar.RegisterResolutionFactory(
                typeof(IEntityRepository<,>),
                (resolver, type) =>
                {
                    if (typeof(IVersionedEntity).IsAssignableFrom(type.GetGenericArguments()[0]))
                    {
                        var resultType = typeof(VersionedEntityRepository<,>).MakeGenericType(type.GetGenericArguments());
                        return resolver.Resolve(resultType);
                    }
                    else
                    {
                        var resultType = typeof(EntityRepository<,>).MakeGenericType(type.GetGenericArguments());
                        return resolver.Resolve(resultType);
                    }
                });

            registrar.RegisterResolutionFactory(
                typeof(IVersionedEntityRepository<,>),
                (resolver, type) =>
                {
                    var resultType = typeof(VersionedEntityRepository<,>).MakeGenericType(type.GetGenericArguments());
                    return resolver.Resolve(resultType);
                });
            registrar.RegisterSingle<IAssignmentService, AssignmentService>();
            registrar.RegisterSingle<IAssignmentsRepository, AssignmentsRepository>();
            registrar.RegisterSingle<IWhereClauseGenerator, WhereClauseGenerator>();
            registrar.RegisterAsSingleton<IStorableObjectsMetadataProvider, StorableObjectsMetadataProvider>();
            registrar.RegisterSingletonResolutionFactory(
                typeof(ITableNameResolver),
                (resolver, type) => resolver.Resolve<IStorableObjectsMetadataProvider>());
            registrar.RegisterSingle<IValidationErrorFactory, ValidationErrorFactory>();
            registrar.RegisterSingle(typeof(IValidationResult<>), typeof(ValidationResult<>));
            registrar.RegisterSingle<IMailSender, MailSender>();
            registrar.RegisterSingle<ITemplatesManager, TemplatesManager>();
            registrar.RegisterSingle<ISmtpConfigurationProvider, SmtpConfigurationProvider>();
            registrar.RegisterSingle<ILockRepository, LockRepository>();
            registrar.RegisterSingle<IObjectMapper, ObjectMapper>();
            registrar.RegisterSingle<IQueryParameterValueConverter, StorableParameterValueConverter>();
            registrar.RegisterSingle<IDatabaseAccessor, DatabaseAccessor>();
            registrar.RegisterSingletonResolutionFactory(
                typeof(MailSettingsSectionGroup),
                (x, y) =>
                {
                    var config =
                    WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                    var mailSection = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
                    return mailSection;
                });
        }
    }
}