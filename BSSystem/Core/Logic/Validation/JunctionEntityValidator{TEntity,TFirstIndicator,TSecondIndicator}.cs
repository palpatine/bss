﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Logic.Validation
{
    public class JunctionEntityValidator<TEntity, TFirstIndicator, TSecondIndicator> : IJunctionEntityValidator<TEntity>
        where TEntity : IJunctionEntity
        where TFirstIndicator : IMarker
        where TSecondIndicator : IMarker
    {
        public virtual Task ValidateAsync(IJunctionValidationResult<TSecondIndicator, AssignmentUdt<TFirstIndicator>> validator)
        {
            throw new NotImplementedException("Appropriate validation method must be overriden");
        }

        public virtual Task ValidateAsync(IJunctionValidationResult<TFirstIndicator, AssignmentUdt<TSecondIndicator>> validator)
        {
            throw new NotImplementedException("Appropriate validation method must be overriden");
        }

        public virtual Task ValidateAsync(IJunctionValidationResult<TSecondIndicator, AssignmentJobRoleUdt<TFirstIndicator>> validator)
        {
            throw new NotImplementedException("Appropriate validation method must be overriden");
        }

        public virtual Task ValidateAsync(IJunctionValidationResult<TFirstIndicator, AssignmentJobRoleUdt<TSecondIndicator>> validator)
        {
            throw new NotImplementedException("Appropriate validation method must be overriden");
        }

        public virtual Task ValidateAsync(IJunctionValidationResult<TSecondIndicator, AssignmentRoleUdt<TFirstIndicator>> validator)
        {
            throw new NotImplementedException("Appropriate validation method must be overriden");
        }

        public virtual Task ValidateAsync(IJunctionValidationResult<TFirstIndicator, AssignmentRoleUdt<TSecondIndicator>> validator)
        {
            throw new NotImplementedException("Appropriate validation method must be overriden");
        }

        public async Task<IEnumerable<ValidationError>> ValidateModelAsync(Type modelType, IStorable master, IEnumerable<object> model)
        {
            var slaveType = modelType.GetGenericArguments()[0];
            if (slaveType == typeof(TFirstIndicator))
            {
                if (modelType.GetGenericTypeDefinition() == typeof(AssignmentJobRoleUdt<>))
                {
                    var validationResult = new JunctionValidationResult<TFirstIndicator, AssignmentJobRoleUdt<TSecondIndicator>>((TFirstIndicator)master, (IEnumerable<AssignmentJobRoleUdt<TSecondIndicator>>)model);
                    await ValidateAsync(validationResult);
                    return validationResult.Errors;
                }

                if (modelType.GetGenericTypeDefinition() == typeof(AssignmentRoleUdt<>))
                {
                    var validationResult = new JunctionValidationResult<TFirstIndicator, AssignmentRoleUdt<TSecondIndicator>>((TFirstIndicator)master, (IEnumerable<AssignmentRoleUdt<TSecondIndicator>>)model);
                    await ValidateAsync(validationResult);
                    return validationResult.Errors;
                }

                if (modelType.GetGenericTypeDefinition() == typeof(AssignmentUdt<>))
                {
                    var validationResult = new JunctionValidationResult<TFirstIndicator, AssignmentUdt<TSecondIndicator>>((TFirstIndicator)master, (IEnumerable<AssignmentUdt<TSecondIndicator>>)model);
                    await ValidateAsync(validationResult);
                    return validationResult.Errors;
                }
            }
            else
            {
                if (modelType.GetGenericTypeDefinition() == typeof(AssignmentJobRoleUdt<>))
                {
                    var validationResult = new JunctionValidationResult<TSecondIndicator, AssignmentJobRoleUdt<TFirstIndicator>>((TSecondIndicator)master, (IEnumerable<AssignmentJobRoleUdt<TFirstIndicator>>)model);
                    await ValidateAsync(validationResult);
                    return validationResult.Errors;
                }

                if (modelType.GetGenericTypeDefinition() == typeof(AssignmentRoleUdt<>))
                {
                    var validationResult = new JunctionValidationResult<TSecondIndicator, AssignmentRoleUdt<TFirstIndicator>>((TSecondIndicator)master, (IEnumerable<AssignmentRoleUdt<TFirstIndicator>>)model);
                    await ValidateAsync(validationResult);
                    return validationResult.Errors;
                }

                if (modelType.GetGenericTypeDefinition() == typeof(AssignmentUdt<>))
                {
                    var validationResult = new JunctionValidationResult<TSecondIndicator, AssignmentUdt<TFirstIndicator>>((TSecondIndicator)master, (IEnumerable<AssignmentUdt<TFirstIndicator>>)model);
                    await ValidateAsync(validationResult);
                    return validationResult.Errors;
                }
            }

            throw new InvalidOperationException();
        }
    }
}