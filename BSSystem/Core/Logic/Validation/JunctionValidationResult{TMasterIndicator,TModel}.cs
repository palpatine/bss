﻿using System.Collections.Generic;
using System.Linq;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Logic.Validation
{
    internal class JunctionValidationResult<TMasterIndicator, TModel> : IJunctionValidationResult<TMasterIndicator, TModel>
        where TModel : IUdt
        where TMasterIndicator : IMarker
    {
        private readonly Dictionary<TModel, IValidationResult<TModel>> _validationResults = new Dictionary<TModel, IValidationResult<TModel>>();

        public JunctionValidationResult(TMasterIndicator master, IEnumerable<TModel> model)
        {
            Master = master;
            Assignments = model;
        }

        public IEnumerable<TModel> Assignments { get; }

        public IEnumerable<ValidationError> Errors
        {
            get { return _validationResults.SelectMany(x => x.Value.Errors); }
        }

        public TMasterIndicator Master { get; }

        public IValidationResult<TModel> GetValidator(TModel assignment)
        {
            if (!_validationResults.ContainsKey(assignment))
            {
                _validationResults.Add(assignment, Validator.GetResultSet(assignment, $"assignments[{assignment.Id}]."));
            }

            return _validationResults[assignment];
        }
    }
}