﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Logic.Validation
{
    internal sealed class ValidationErrorFactory : IValidationErrorFactory
    {
        private readonly IEnumerable<IValidationErrorMessagesProvider> _errorMessagesProviders;

        public ValidationErrorFactory(IEnumerable<IValidationErrorMessagesProvider> errorMessagesProviders)
        {
            _errorMessagesProviders = errorMessagesProviders;
        }

        public ValidationError Create(
            string errorCode,
            PropertyInfo property,
            string baseReference,
            object[] messageParameters)
        {
            var parts = errorCode.Split('_');
            string message = null;
            while (message == null && parts.Any())
            {
                var key = string.Join("_", parts);
                message = GetMessage(key);

                if (parts.Any())
                {
                    parts = parts.Skip(1).ToArray();
                }
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                message = errorCode;
                Debug.WriteLine($"Missing validation message for {errorCode}.");
            }
            else if (messageParameters != null && messageParameters.Any())
            {
                message = string.Format(message, messageParameters);
            }

            if (property != null)
            {
                return new ValidationError(message, errorCode, baseReference + property.Name);
            }

            return new ValidationError(message, errorCode, null);
        }

        private string GetMessage(string errorCode)
        {
            string message = null;
            foreach (var provider in _errorMessagesProviders)
            {
                if (provider.TryGetErrorMessage(errorCode, out message))
                {
                    break;
                }
            }

            return message;
        }
    }
}