﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Models.Extensibility;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.Core.Logic.Validation
{
    public class EntityValidator<TModel> : IEntityValidator<TModel>
    {
        private readonly IServiceLocator _serviceLocator;

        public EntityValidator(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
        }

        public virtual void Validate(IValidationResult<TModel> validator)
        {
        }

        public virtual Task ValidateAsync(IValidationResult<TModel> validator)
        {
            Validate(validator);
            return Task.Delay(0);
        }

        public async Task<IEnumerable<ValidationError>> ValidateModelAsync(TModel model)
        {
            var erros = new List<ValidationError>();
            erros.AddRange(await ValidateAsync(model));
            var extensionProvider = model as IEditModelExtesionsProvider;

            if (extensionProvider != null)
            {
                foreach (var extesnion in extensionProvider.EditModelExtensions)
                {
                    var validator =
                        (IEntityValidator)_serviceLocator.GetService(typeof(IEntityValidator<>)
                        .MakeGenericType(extesnion.GetType()));
                    erros.AddRange(await validator.ValidateModelAsync(extesnion));
                }
            }

            return erros;
        }

        public async Task<IEnumerable<ValidationError>> ValidateModelAsync(object model)
        {
            if (typeof(TModel) != model.GetType())
            {
                throw new ArgumentException("Model must be of type " + typeof(TModel).FullName, "model");
            }

            return await ValidateModelAsync((TModel)model);
        }

        protected virtual async Task<IEnumerable<ValidationError>> ValidateAsync(TModel model)
        {
            var result = Validator.GetResultSet(model);
            ValidateBySql(result);
            ValidateCommonRules(result);
            await ValidateAsync(result);
            model.Normalize();
            return result.Errors;
        }

        private static void ValidateDisplayName<TStorableModel>(
            IValidationResult<TStorableModel> result,
            Type entity)
            where TStorableModel : IStorable
        {
            var displayNameProperty =
                typeof(TStorableModel).GetProperty(ExpressionExtensions.GetPropertyName((INamedEntity n) => n.DisplayName));
            var parameter = Expression.Parameter(typeof(TStorableModel));
            var retriver = Expression.Property(parameter, displayNameProperty);
            var function = Expression.Lambda<Func<TStorableModel, string>>(retriver, parameter);
            result.MustBeUnique(function, entity);
        }

        private void ValidateBySql(IValidationResult<TModel> validator)
        {
            var modelType = typeof(TModel);

            foreach (
                var property in
                    modelType.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.GetProperty |
                                            BindingFlags.Public | BindingFlags.Instance)
                                            .Where(p => p.PropertyType == typeof(string)))
            {
                var propertyParameter = Expression.Parameter(modelType);
                var body = Expression.Property(propertyParameter, property);
                var lambda = Expression.Lambda(body, propertyParameter);
                var reference = (Expression<Func<TModel, string>>)lambda;

                var sqltype = StorableHelper.GetSqlTypeAttribute(modelType, property);
                if (sqltype == null)
                {
                    continue;
                }

                switch (sqltype.Type)
                {
                    case SqlDbType.Char:
                    case SqlDbType.NChar:
                    case SqlDbType.VarChar:
                    case SqlDbType.NVarChar:
                        if (sqltype.IsNullable)
                        {
                            if (sqltype.Capacity > 0)
                            {
                                validator.NotLongerThen(
                                    reference,
                                    sqltype.Capacity);
                            }
                        }
                        else
                        {
                            if (sqltype.Capacity > 0)
                            {
                                validator.NotEmptyNorLongerThen(reference, sqltype.Capacity);
                            }
                            else
                            {
                                validator.NotEmpty(reference);
                            }
                        }

                        break;
                }
            }
        }

        private void ValidateCommonRules(IValidationResult<TModel> result)
        {
            var entity = StorableHelper.GetEntityType(typeof(TModel));

            if (typeof(IDatedEntity).IsAssignableFrom(entity))
            {
                var dateStartProperty =
                    typeof(TModel).GetProperty(ExpressionExtensions.GetPropertyName((IDatedEntity d) => d.DateStart));
                var firstParameter = Expression.Parameter(typeof(TModel));
                var dateStartRetriver = Expression.Property(firstParameter, dateStartProperty);
                var dateStartConverted = Expression.Convert(dateStartRetriver, typeof(DateTime?));
                var dateStartFunction = Expression.Lambda<Func<TModel, DateTime?>>(dateStartConverted, firstParameter).Compile();

                var secondParameter = Expression.Parameter(typeof(TModel));
                var dateEndProperty =
                    typeof(TModel).GetProperty(ExpressionExtensions.GetPropertyName((IDatedEntity d) => d.DateEnd));
                var dateEndRetriver = Expression.Property(secondParameter, dateEndProperty);
                var dateEndConverted = Expression.Convert(dateEndRetriver, typeof(DateTime?));
                var dateEndFunction = Expression.Lambda<Func<TModel, DateTime?>>(dateEndConverted, secondParameter);

                result.MustBeAfterOrEqual(dateEndFunction, dateStartFunction);
            }

            if (typeof(INamedEntity).IsAssignableFrom(entity))
            {
                if (!typeof(IStorable).IsAssignableFrom(typeof(TModel)))
                {
                    throw new InvalidOperationException();
                }

                var methodDefinition = ExpressionExtensions.GetMethod(() => ValidateDisplayName<IStorable>(null, null)).GetGenericMethodDefinition();
                var method = methodDefinition.MakeGenericMethod(typeof(TModel));
                method.Invoke(null, new object[] { result, entity });
            }
        }
    }
}