﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.Core.Logic.Validation
{
    public static class Validator
    {
        private const string EmailSufix = "Email";

        private const string MustBeAfterSufix = "BeAfter";
        private const string MustBeBeforeSufix = "BeBefore";
        private const string MustBeUniqueSufix = "MustBeUnique";
        private const string MustBeEmptySufix = "BeEmpty";
        private const string MustMatchSufix = "MustMatch";
        private const string MustNotMatchSufix = "MustNotMatch";
        private const string NotEmptySufix = "Empty";
        private const string NotLongerSufix = "MaxLength";

        private static readonly Regex EmailValidationRegex = new Regex(
            @"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$",
            RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public static IValidationResult<TEntity> GetResultSet<TEntity>(
            TEntity entity,
            string baseReference = null)
        {
            var validationResult = ServiceLocator.Current.GetInstance<IValidationResult<TEntity>>();
            validationResult.BaseReference = baseReference ?? string.Empty;
            validationResult.Entity = entity;
            return validationResult;
        }

        public static IValidationResult<TEntity> MustBeAfterOrEqual<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, DateTime?>> reference,
            Func<TEntity, DateTime?> compared)
        {
            var retriever = reference.Compile();
            var value = retriever(validationResult.Entity);
            var secondValue = compared(validationResult.Entity);

            if (!value.HasValue)
            {
                return validationResult;
            }

            if (!secondValue.HasValue)
            {
                validationResult.AddError(reference, MustBeEmptySufix);
                return validationResult;
            }

            if (value.Value < secondValue.Value)
            {
                validationResult.AddError(reference, MustBeAfterSufix, secondValue.Value.ToShortDateString());
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustBeAfterOrEqual<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, DateTime?>> reference,
            DateTime? compared)
        {
            var retriever = reference.Compile();
            var value = retriever(validationResult.Entity);

            if (!value.HasValue)
            {
                return validationResult;
            }

            if (!compared.HasValue)
            {
                validationResult.AddError(reference, MustBeEmptySufix);
                return validationResult;
            }

            if (value.Value < compared.Value)
            {
                validationResult.AddError(reference, MustBeAfterSufix, compared.Value.ToShortDateString());
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustBeBeforeOrEqual<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, DateTime?>> reference,
            DateTime? compared)
        {
            var retriever = reference.Compile();
            var value = retriever(validationResult.Entity);

            if (!value.HasValue)
            {
                return validationResult;
            }

            if (!compared.HasValue)
            {
                validationResult.AddError(reference, MustBeEmptySufix);
                return validationResult;
            }

            if (value.Value > compared.Value)
            {
                validationResult.AddError(reference, MustBeBeforeSufix, compared.Value.ToShortDateString());
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustBeUnique<TEntity, TValue>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, TValue>> reference)
            where TEntity : IStorable
        {
            var entityType = StorableHelper.GetEntityType(typeof(TEntity));
            return MustBeUnique(validationResult, reference, entityType);
        }

        public static IValidationResult<TEntity> MustBeUnique<TEntity, TValue>(
           this IValidationResult<TEntity> validationResult,
           Expression<Func<TEntity, TValue>> reference,
           Type entityType)
           where TEntity : IStorable
        {
            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);

            if (value == null)
            {
                return NotEmpty(validationResult, reference);
            }

            var where = GenerateUniquenesFilter(
                validationResult,
                reference,
                entityType,
                value);

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var notUnique = databaseAccessor.Query<IStorable>(entityType).Any(where);

            if (notUnique)
            {
                validationResult.AddError(reference, MustBeUniqueSufix);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustBeUnique<TEntity, TValue, TEntityIndicator, TOther>(
           this IValidationResult<TEntity> validationResult,
           Expression<Func<TEntity, TValue>> reference,
           Expression<Func<TOther, TEntityIndicator>> relationReference)
            where TEntity : TEntityIndicator
            where TEntityIndicator : IMarker
            where TOther : IEntity
        {
            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);

            if (value == null)
            {
                return NotEmpty(validationResult, reference);
            }

            var entityType = typeof(TOther);
            var property = relationReference.GetProperty();
            var where = GenerateUniquenesFilter(
                validationResult,
                reference,
                entityType,
                value,
                property);

            var databaseAccessor = ServiceLocator.Current.GetInstance<IDatabaseAccessor>();
            var notUnique = databaseAccessor.Query<IStorable>(entityType).Any(where);

            if (notUnique)
            {
                validationResult.AddError(reference, MustBeUniqueSufix);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustBeUniqueOrNull<TEntity, TValue>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, TValue>> reference)
            where TEntity : IStorable
        {
            var entityType = StorableHelper.GetEntityType(typeof(TEntity));

            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);

            if (value != null)
            {
                return MustBeUnique(validationResult, reference, entityType);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustBeUniqueOrNull<TEntity, TValue, TEntityIndicator, TOther>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, TValue>> reference,
            Expression<Func<TOther, TEntityIndicator>> relationReference)
            where TEntity : TEntityIndicator
            where TEntityIndicator : IMarker
            where TOther : IEntity
        {
            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);

            if (value != null)
            {
                return MustBeUnique(validationResult, reference, relationReference);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustBeUniqueOrNull<TEntity, TValue>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, TValue>> reference,
            Type entityType)
            where TEntity : IStorable
        {
            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);

            if (value != null)
            {
                return MustBeUnique(validationResult, reference, entityType);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustMatch<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, string>> reference,
            Func<TEntity, string> compared)
        {
            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);
            var secondValue = compared(validationResult.Entity);

            if (value == null || !Equals(value, secondValue))
            {
                validationResult.AddError(reference, MustMatchSufix);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> MustNotMatch<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, string>> reference,
            Func<TEntity, string> compared)
        {
            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);
            var secondValue = compared(validationResult.Entity);

            if (value == null || Equals(value, secondValue))
            {
                validationResult.AddError(reference, MustNotMatchSufix);
            }

            return validationResult;
        }

        public static void Normalize<TEntity>(
            this TEntity entity)
        {
            var properties = typeof(TEntity)
                .GetProperties()
                .Where(x => x.PropertyType == typeof(string));

            foreach (var property in properties)
            {
                var value = (string)property.GetValue(entity);
                if (value != null)
                {
                    if (value == string.Empty)
                    {
                        value = null;
                    }
                    else
                    {
                        value = value.Trim();
                    }

                    property.SetValue(entity, value);
                }
            }
        }

        public static IValidationResult<TEntity> NotEmpty<TEntity, TValue>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, TValue>> reference)
        {
            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);

            if (Equals(value, default(TValue))
                || (typeof(TValue) == typeof(string) && string.IsNullOrWhiteSpace((string)value)))
            {
                validationResult.AddError(reference, NotEmptySufix);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> NotEmpty<TEntity, TValue>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, TValue>> reference,
            object value,
            object metadata = null)
        {
            if (Equals(value, default(TValue))
                || (typeof(TValue) == typeof(string) && string.IsNullOrWhiteSpace((string)value)))
            {
                validationResult.AddErrorWithMetadata(reference, NotEmptySufix, metadata);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> NotEmptyNorLongerThen<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, string>> reference,
            int maxLength)
        {
            var retriever = reference.Compile();
            var value = retriever(validationResult.Entity);

            if (string.IsNullOrWhiteSpace(value))
            {
                validationResult.AddError(reference, NotEmptySufix);
            }
            else if (value.Length > maxLength)
            {
                validationResult.AddError(reference, NotLongerSufix, maxLength.ToString());
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> NotEmptyOrNull<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, string>> reference)
        {
            var retriever = reference.Compile();
            var value = (object)retriever(validationResult.Entity);

            if (value != null && string.Empty.Equals(value))
            {
                validationResult.AddError(reference, NotEmptySufix);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> NotLongerThen<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, string>> reference,
            int maxLength)
        {
            var retriever = reference.Compile();
            var value = retriever(validationResult.Entity);

            if (value != null && value.Length > maxLength)
            {
                validationResult.AddError(reference, NotLongerSufix, maxLength.ToString());
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> ValidateDatesConsistencyWithMasterEntity<TEntity>(
            this IValidationResult<TEntity> validationResult,
            IDatedEntity master)
            where TEntity : IDatedEntity
        {
            var slave = validationResult.Entity;
            if (slave.DateStart < master.DateStart)
            {
                validationResult.AddError(x => x.DateStart, MustBeAfterSufix, master.DateStart.ToShortDateString());
            }

            if (master.DateEnd.HasValue && slave.DateStart > master.DateEnd.Value)
            {
                validationResult.AddError(x => x.DateStart, MustBeBeforeSufix, master.DateEnd.Value.ToShortDateString());
            }

            if ((!slave.DateEnd.HasValue && master.DateEnd.HasValue)
            || (slave.DateEnd.HasValue && master.DateEnd.HasValue && slave.DateEnd.Value > master.DateEnd.Value))
            {
                validationResult.AddError(x => x.DateEnd, MustBeBeforeSufix, master.DateEnd.Value.ToShortDateString());
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> ValidateValue<TEntity, TValue>(
                    this IValidationResult<TEntity> validationResult,
            Expression<Func<bool>> reference,
            Expression<Func<TEntity, TValue>> property,
            params string[] messageParameters)
        {
            var retriever = reference.Compile();
            var value = retriever();

            if (!value)
            {
                var name = reference.GetMember().Name;
                var suffix = char.ToUpper(name[0]) + name.Substring(1);
                validationResult.AddError(property, suffix, messageParameters);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> ValidEmail<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, string>> reference)
        {
            var retriever = reference.Compile();
            var value = retriever(validationResult.Entity);

            return ValidEmail(validationResult, reference, value);
        }

        public static IValidationResult<TEntity> ValidEmail<TEntity>(
            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, string>> reference,
            string value)
        {
            if (value == null || !EmailValidationRegex.Match(value).Success)
            {
                validationResult.AddError(reference, EmailSufix);
            }

            return validationResult;
        }

        public static IValidationResult<TEntity> ValidEmailOrNull<TEntity>(
                            this IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, string>> reference)
        {
            var retriever = reference.Compile();
            var value = retriever(validationResult.Entity);
            if (value != null)
            {
                return ValidEmail(validationResult, reference, value);
            }

            return validationResult;
        }

        private static Expression<Func<IStorable, bool>> GenerateUniquenesFilter<TEntity, TValue>(
            IValidationResult<TEntity> validationResult,
            Expression<Func<TEntity, TValue>> reference,
            Type entityType,
            object value,
            PropertyInfo propertyToCompareWithEntityId = null)
            where TEntity : IStorable
        {
            var propertyname = reference.GetPropertyName();
            var parameter = Expression.Parameter(typeof(IStorable));
            var parameterCast = Expression.Convert(parameter, entityType);
            var propertyCheckdForUniquness = Expression.Property(parameterCast, propertyname);
            var uniquePropertyComparition = Expression.Equal(propertyCheckdForUniquness, Expression.Constant(value));

            MemberExpression idProperty;
            if (propertyToCompareWithEntityId != null)
            {
                idProperty = Expression.Property(Expression.Property(parameterCast, propertyToCompareWithEntityId), ExpressionExtensions.GetProperty((IStorable x) => x.Id));
            }
            else
            {
                var property = ExpressionExtensions.GetProperty((IStorable x) => x.Id);
                idProperty = Expression.Property(parameter, property);
            }

            var notSelf = Expression.NotEqual(idProperty, Expression.Constant(validationResult.Entity.Id));

            var body = Expression.AndAlso(notSelf, uniquePropertyComparition);
            var organizationPropertyName = ExpressionExtensions.GetPropertyName((IScopedEntity x) => x.Scope);
            var organizationProperty = entityType.GetProperty(organizationPropertyName);

            if (organizationProperty != null)
            {
                var sessionDataProvider = ServiceLocator.Current.GetInstance<ISessionDataProvider>();
                var organizationPropertyAccess = Expression.Property(parameterCast, organizationProperty);
                var isEquivalentMethod =
                    ExpressionExtensions.GetMethod(() => StorableExtensions.IsEquivalent<IOrganization>(null, null));

                var organizationFilter = Expression.Call(null, isEquivalentMethod, organizationPropertyAccess, Expression.Constant(sessionDataProvider.Scope));

                body = Expression.AndAlso(body, organizationFilter);
            }

            if (typeof(IVersionedEntity).IsAssignableFrom(entityType))
            {
                var isDeletedPropertyName = ExpressionExtensions.GetPropertyName((VersionedEntity x) => x.IsDeleted);
                var isDeletedProperty = Expression.Property(parameterCast, isDeletedPropertyName);
                body = Expression.AndAlso(Expression.Not(isDeletedProperty), body);
            }

            var where = Expression.Lambda<Func<IStorable, bool>>(body, parameter);
            return where;
        }
    }
}