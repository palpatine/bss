﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Repositories;

namespace SolvesIt.BSSystem.Core.Logic.Validation
{
    internal sealed class ValidationResult<TEntity> : IValidationResult<TEntity>
    {
        private readonly List<ValidationError> _errors = new List<ValidationError>();
        private readonly IValidationErrorFactory _validationErrorFactory;

        public ValidationResult(IValidationErrorFactory validationErrorFactory)
        {
            _validationErrorFactory = validationErrorFactory;
        }

        public string BaseReference { get; set; }

        public TEntity Entity { get; set; }

        public IEnumerable<ValidationError> Errors => _errors;

        public void AddError(
            string errorCode,
            string reference,
            string localizedMessage)
        {
            var error = new ValidationError(localizedMessage, errorCode, BaseReference + reference);
            _errors.Add(error);
        }

        public void AddError<TValue>(
            Expression<Func<TEntity, TValue>> reference,
            string sufix,
            params object[] messageParameters)
        {
            AddErrorWithMetadata(reference, sufix, null, messageParameters);
        }

        public void AddErrorWithMetadata<TValue>(
            Expression<Func<TEntity, TValue>> reference,
            string sufix,
            object metadata,
            params object[] messageParameters)
        {
            var property = reference.GetProperty();
            var errorKey = GetResourceKey(property.Name, sufix);
            var error = _validationErrorFactory.Create(errorKey, property, BaseReference, messageParameters);
            error.Metadata = metadata;
            _errors.Add(error);
        }

        private string GetResourceKey(
            string propertyName,
            string sufix)
        {
            var entityName = typeof(TEntity).Name;

            if (entityName.EndsWith("EditModel"))
            {
                entityName = entityName.Substring(0, entityName.IndexOf("EditModel"));
            }

            return $"{entityName}_{propertyName}_{sufix}";
        }
    }
}