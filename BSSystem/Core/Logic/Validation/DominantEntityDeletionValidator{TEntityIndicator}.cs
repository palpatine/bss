﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Logic.Validation
{
    public abstract class DominantEntityDeletionValidator<TEntityIndicator> : IEntityDeletionValidator<TEntityIndicator>
        where TEntityIndicator : IStorable
    {
        public async Task<IEnumerable<ValidationError>> ValidateDeletionAsync(TEntityIndicator entity)
        {
            if (await CheckSlaveExistanceAsync(entity))
            {
                return new[]
                {
                    new ValidationError(ValidationErrors.SlaveExists ?? "[SlaveExists]", "SlaveExists")
                };
            }

            return Enumerable.Empty<ValidationError>();
        }

        protected abstract Task<bool> CheckSlaveExistanceAsync(TEntityIndicator entity);
    }
}