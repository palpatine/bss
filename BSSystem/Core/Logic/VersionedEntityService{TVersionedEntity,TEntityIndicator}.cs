﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Qdarc.Modules.Common.Communication;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.Core.Logic
{
    public class VersionedEntityService<TVersionedEntity, TEntityIndicator>
        : VersionedEntityService<TVersionedEntity, TVersionedEntity, TEntityIndicator>,
          IVersionedEntityService<TVersionedEntity, TEntityIndicator>
        where TVersionedEntity : VersionedEntity, TEntityIndicator, new()
        where TEntityIndicator : IStorable
    {
        private readonly ISessionDataProvider _sessionDataProvider;

        public VersionedEntityService(
            IEventAggregator eventAggregator,
            ILockRepository lockRepository,
            IEnumerable<IServiceExtension<TVersionedEntity>> extensions,
            IVersionedEntityRepository<TVersionedEntity, TEntityIndicator> repository,
            IEnumerable<IEntityValidator<TVersionedEntity>> validators,
            IDatabaseManager databaseManager,
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider,
            IEnumerable<IEntityDeletionValidator<TEntityIndicator>> deletionValidators)
            : base(
                   eventAggregator,
                   lockRepository,
                   extensions,
                   repository,
                   validators,
                   databaseManager,
                   databaseAccessor,
                   deletionValidators)
        {
            _sessionDataProvider = sessionDataProvider;
        }

        protected override async Task OnReadyToSaveAsync(TVersionedEntity model)
        {
            var organizationScopedEntity = model as IScopedEntity;

            if (organizationScopedEntity != null &&
                !organizationScopedEntity.Scope.IsEquivalent(_sessionDataProvider.Scope))
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            await Repository.WriteAsync(model);
        }
    }
}