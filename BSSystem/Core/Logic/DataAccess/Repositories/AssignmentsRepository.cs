using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Repositories
{
    internal sealed class AssignmentsRepository : BaseRepository, IAssignmentsRepository
    {
        private readonly ISessionDataProvider _sessionDataProvider;

        public AssignmentsRepository(
            IConfigurationProvider configurationProvider,
            IWhereClauseGenerator whereClauseGenerator,
            IDatabaseManager databaseManager,
            ISessionDataProvider sessionDataProvider)
            : base(configurationProvider, whereClauseGenerator, databaseManager)
        {
            _sessionDataProvider = sessionDataProvider;
        }

        public async Task<IEnumerable<ValidationResultUdt>> UpdateEntityAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator master,
            IEnumerable<IDataRecord> newListValue,
            string newListType)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
        {
            Type junctionType;
            var entityType = StorableHelper.GetEntityType(typeof(TEntityIndicator));
            var secondEntityType = StorableHelper.GetEntityType(typeof(TSlave));

            if (!StorableHelper.TryGetJunctionTable(entityType, secondEntityType, out junctionType))
            {
                throw new InvalidOperationException($"No junction table for {entityType.Name} and {secondEntityType.Name}.");
            }

            using (DatabaseManager.OpenTransaction())
            {
                var procedureName = StorableHelper.GetQualifiedUpdateProcedureName(junctionType);
                var sqlCommand = DatabaseManager.Connection.CreateSqlCommand(procedureName, CommandType.StoredProcedure);

                if (typeof(TEntityIndicator) != typeof(OrganizationMarker) || typeof(TSlave) != typeof(ModuleMarker))
                {
                    sqlCommand.Parameters.AddWithValue("@ScopeId", _sessionDataProvider.Scope.Id);
                }

                sqlCommand.Parameters.AddWithValue("@ChangerId", _sessionDataProvider.CurrentUser.Id);
                var masterProperty = junctionType.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)
                    .Single(z => z.PropertyType.IsAssignableFrom(typeof(TEntityIndicator)));
                var masterColumnName = StorableHelper.GetQualifiedColumnName(masterProperty);
                sqlCommand.Parameters.AddWithValue("@MasterColumnName", masterColumnName);
                sqlCommand.Parameters.AddWithValue("@MasterId", master.Id);
                var tableParameter = sqlCommand.Parameters.Add("@NewList", SqlDbType.Structured);
                tableParameter.Value = newListValue;
                tableParameter.TypeName = newListType;
                var validationResults = await sqlCommand.ExecuteWithValidationAsync();

                DatabaseManager.ComitTransaction();
                return validationResults;
            }
        }
    }
}