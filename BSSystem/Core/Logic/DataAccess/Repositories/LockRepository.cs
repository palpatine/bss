﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Repositories
{
    internal sealed class LockRepository : ILockRepository
    {
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly IDatabaseManager _databaseManager;
        private readonly ISessionDataProvider _sessionDataProvider;

        public LockRepository(
            IDatabaseManager databaseManager,
            ISessionDataProvider sessionDataProvider,
            IDatabaseAccessor databaseAccessor)
        {
            _databaseManager = databaseManager;
            _sessionDataProvider = sessionDataProvider;
            _databaseAccessor = databaseAccessor;
        }

        public async Task RemoveLockAsync(string operationToken)
        {
            using (_databaseManager.OpenTransaction())
            {
                var sqlCommand = _databaseManager.Connection.CreateSqlCommand("[Common].[RemoveLock]", CommandType.StoredProcedure);
                sqlCommand.Parameters.AddWithValue("@OperationToken", operationToken);
                sqlCommand.Parameters.AddWithValue("@UserId", _sessionDataProvider.CurrentUser.Id);
                sqlCommand.Parameters.AddWithValue("@SessionId", _sessionDataProvider.Session.Id);

                await sqlCommand.ExecuteNonQueryAsync();
                _databaseManager.ComitTransaction();
            }
        }

        public async Task<IEnumerable<ValidationError>> TryLockAsync<TStorable>(
            TStorable entity,
            string operationToken)
            where TStorable : IStorable
        {
            if (!await CheckStorableValidationAsync(entity))
            {
                return new[] { new ValidationError(ValidationErrors.TargetNotFound, "TargetNotFound") };
            }

            var tableName = StorableHelper.GetQualifiedTableName(entity.GetType());

            using (_databaseManager.OpenTransaction())
            {
                var sqlCommand = _databaseManager.Connection.CreateSqlCommand("[Common].[Trylock]", CommandType.StoredProcedure);
                sqlCommand.Parameters.AddWithValue("@OperationToken", operationToken);
                sqlCommand.Parameters.AddWithValue("@UserId", _sessionDataProvider.CurrentUser.Id);
                sqlCommand.Parameters.AddWithValue("@SessionId", _sessionDataProvider.Session.Id);
                sqlCommand.Parameters.AddWithValue("@TableName", tableName);
                sqlCommand.Parameters.AddWithValue("@RecordId", entity.Id);

                var errors = await ReadErrorsAsync(sqlCommand);
                _databaseManager.ComitTransaction();
                return errors;
            }
        }

        public async Task<IEnumerable<ValidationError>> TryLockAsync(
             Type junctionTable,
             PropertyInfo relationProperty,
             IMarker referenceValue,
             string operationToken)
        {
            if (!await CheckStorableValidationAsync(referenceValue))
            {
                return new[] { new ValidationError(ValidationErrors.TargetNotFound, "TargetNotFound") };
            }

            var tableName = StorableHelper.GetQualifiedTableName(junctionTable);
            var columnName = StorableHelper.GetQualifiedColumnName(relationProperty);

            using (_databaseManager.OpenTransaction())
            {
                var sqlCommand = _databaseManager.Connection.CreateSqlCommand("[Common].[TryJunctionLock]", CommandType.StoredProcedure);
                sqlCommand.Parameters.AddWithValue("@OperationToken", operationToken);
                sqlCommand.Parameters.AddWithValue("@UserId", _sessionDataProvider.CurrentUser.Id);
                sqlCommand.Parameters.AddWithValue("@SessionId", _sessionDataProvider.Session.Id);
                sqlCommand.Parameters.AddWithValue("@TableName", tableName);
                sqlCommand.Parameters.AddWithValue("@ColumnName", columnName);
                sqlCommand.Parameters.AddWithValue("@ColumnValue", referenceValue.Id);

                var errors = await ReadErrorsAsync(sqlCommand);
                _databaseManager.ComitTransaction();
                return errors;
            }
        }

        private static async Task<IEnumerable<ValidationError>> ReadErrorsAsync(ISqlCommand sqlCommand)
        {
            using (var reader = await sqlCommand.ExecuteReaderAsync())
            {
                var validationResultUdtList = new List<ValidationResultUdt>();
                while (await reader.ReadAsync())
                {
                    var additionaValyeKey = ExpressionExtensions.GetPropertyName((ValidationResultUdt a) => a.AdditionalValue);
                    var fieldKey = ExpressionExtensions.GetPropertyName((ValidationResultUdt a) => a.Field);
                    var idKey = ExpressionExtensions.GetPropertyName((ValidationResultUdt a) => a.Id);
                    var messageCodeKey = ExpressionExtensions.GetPropertyName((ValidationResultUdt a) => a.MessageCode);

                    var validatioResultUdt = new ValidationResultUdt()
                    {
                        AdditionalValue = reader[additionaValyeKey] as string,
                        Field = reader[fieldKey] as string,
                        Id = reader[idKey] as int?,
                        MessageCode = (string)reader[messageCodeKey]
                    };
                    validationResultUdtList.Add(validatioResultUdt);
                }

                var groupedErrors = validationResultUdtList.GroupBy(x => x.MessageCode, x => x);
                var errors = groupedErrors.Select(x =>
                {
                    var message = string.Format(
                        CultureInfo.InvariantCulture,
                        ValidationErrors.ResourceManager.GetString(x.Key) ?? "[" + x.Key + "]",
                        string.Join(", ", x.Select(y => y.AdditionalValue)));
                    return new ValidationError(message, x.Key);
                }).ToList();
                return errors;
            }
        }

        private async Task<bool> CheckJunctionTableValidationAsync(IJunctionEntity junctionEntity)
        {
            var firstType = StorableHelper.GetEntityType(junctionEntity.First.GetType());
            var secondType = StorableHelper.GetEntityType(junctionEntity.Second.GetType());
            if (typeof(IScopedEntity).IsAssignableFrom(firstType)
             && typeof(IScopedEntity).IsAssignableFrom(secondType))
            {
                var firstEntity = await _databaseAccessor
                .Query<IScopedEntity>(junctionEntity.First.GetType())
                .Where(x => x.Id == junctionEntity.First.Id && x.Scope.IsEquivalent(_sessionDataProvider.Scope))
                .SingleOrDefaultAsync();

                var versionedEntity = firstEntity as VersionedEntity;
                if (firstEntity == null || (versionedEntity != null && versionedEntity.IsDeleted))
                {
                    return false;
                }
            }

            return true;
        }

        private async Task<bool> CheckStorableValidationAsync(IStorable storable)
        {
            if (storable.Id <= 0)
            {
                return false;
            }

            if (storable is Entity)
            {
                var versionedEntity = storable as VersionedEntity;
                if (versionedEntity != null && versionedEntity.IsDeleted)
                {
                    return false;
                }

                var organizationScopedEntity = storable as IScopedEntity;
                if (organizationScopedEntity != null && !organizationScopedEntity.Scope.IsEquivalent(_sessionDataProvider.Scope))
                {
                    return false;
                }

                var assignmentEntity = storable as IJunctionEntity;
                if (assignmentEntity != null)
                {
                    return await CheckJunctionTableValidationAsync(assignmentEntity);
                }
            }
            else
            {
                var storableType = StorableHelper.GetEntityType(storable.GetType());
                if (typeof(IScopedEntity).IsAssignableFrom(storableType))
                {
                    var readedEntity = await _databaseAccessor
                        .Query<IScopedEntity>(storableType)
                        .Where(x => x.Id == storable.Id && x.Scope.IsEquivalent(_sessionDataProvider.Scope))
                        .SingleOrDefaultAsync();

                    var versionedEntity = readedEntity as IVersionedEntity;
                    if (readedEntity == null || (versionedEntity != null && versionedEntity.IsDeleted))
                    {
                        return false;
                    }

                    return true;
                }

                if (typeof(IJunctionEntity).IsAssignableFrom(storableType))
                {
                    var junctionEntity = await _databaseAccessor
                        .Query<IJunctionEntity>(storableType)
                        .Where(x => x.Id == storable.Id)
                        .SingleAsync();

                    var versionedEntity = junctionEntity as VersionedEntity;
                    if (versionedEntity != null && versionedEntity.IsDeleted)
                    {
                        return false;
                    }

                    return await CheckJunctionTableValidationAsync(junctionEntity);
                }

                var entityExists = await _databaseAccessor
                    .Query<IVersionedEntity>(storableType)
                    .Where(x => x.Id == storable.Id && !x.IsDeleted)
                    .AnyAsync();
                return entityExists;
            }

            return true;
        }
    }
}