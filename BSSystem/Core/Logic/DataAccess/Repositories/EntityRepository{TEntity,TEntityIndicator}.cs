﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Sql;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Repositories
{
    internal sealed class EntityRepository<TEntity, TEntityIndicator> :
        BaseRepository,
        IEntityRepository<TEntity, TEntityIndicator>
        where TEntity : Entity, TEntityIndicator, new()
        where TEntityIndicator : IStorable
    {
        private const string DeleteCommandTemplate = "DELETE FROM {0} WHERE {1};";

        private const string InsertCommandTemplate =
            "INSERT INTO {0} ({1}) VALUES ({2}); SELECT [Id] FROM {0} WHERE [Id] = ISNULL(SCOPE_IDENTITY(), @@IDENTITY);";

        private const string UpdateCommandTemplate = "UPDATE {0} SET {1} WHERE [Id] = @Id; SELECT @Id";
        private readonly IEntityDescriptor<TEntity> _entityDescriptor;

        public EntityRepository(
            IStorableObjectsMetadataProvider enitityMetadataProvider,
            IConfigurationProvider configurationProvider,
            IWhereClauseGenerator whereClauseGenerator,
            IDatabaseManager databaseManager)
            : base(configurationProvider, whereClauseGenerator, databaseManager)
        {
            _entityDescriptor = enitityMetadataProvider.GetEntityDescriptor<TEntity>();
        }

        public async Task DeleteAsync(TEntityIndicator indicator)
        {
            if (indicator.Id == 0)
            {
                throw new InvalidOperationException("Entity was not save to Database.");
            }

            await DeleteAsync(x => x.Id == indicator.Id);
        }

        public async Task DeleteAsync(Expression<Func<TEntity, bool>> where)
        {
            var whereQuery = WhereClauseGenerator.Generate(where);

            var command = string.Format(
                DeleteCommandTemplate,
                _entityDescriptor.SqlName,
                whereQuery.Clause);
            using (var connection = CreateConnection())
            {
                var sqlCommand = connection.CreateSqlCommand(command);
                whereQuery.BindParameters(sqlCommand);

                await sqlCommand.ExecuteNonQueryAsync();
            }
        }

        public void Write(TEntity entity)
        {
            if (!entity.Modified)
            {
                return;
            }

            var command = PrepareCommandText(entity);

            using (var connection = CreateConnection())
            {
                var sqlCommand = connection.CreateSqlCommand(command);
                _entityDescriptor.MapWriteValues(sqlCommand, entity);
                entity.Id = (int)sqlCommand.ExecuteScalar();
                entity.Modified = false;
            }
        }

        public async Task WriteAsync(TEntity entity)
        {
            if (!entity.Modified)
            {
                return;
            }

            var command = PrepareCommandText(entity);

            using (var connection = CreateConnection())
            {
                var sqlCommand = connection.CreateSqlCommand(command);
                _entityDescriptor.MapWriteValues(sqlCommand, entity);
                entity.Id = (int)(await sqlCommand.ExecuteScalarAsync());
                entity.Modified = false;
            }
        }

        private string PrepareCommandText(TEntity entity)
        {
            string command;
            if (entity.Id == 0)
            {
                command = string.Format(
                    InsertCommandTemplate,
                    _entityDescriptor.SqlName,
                    _entityDescriptor.FormatInsertColumnsList(),
                    _entityDescriptor.FormatInsertValuesSection());
            }
            else
            {
                command = string.Format(
                    UpdateCommandTemplate,
                    _entityDescriptor.SqlName,
                    _entityDescriptor.FormatUpdateSetSection());
            }

            return command;
        }
    }
}