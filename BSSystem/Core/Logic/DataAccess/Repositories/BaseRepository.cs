﻿using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Sql;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Repositories
{
    public abstract class BaseRepository
    {
        protected BaseRepository(
            IConfigurationProvider configurationProvider,
            IWhereClauseGenerator whereClauseGenerator,
            IDatabaseManager databaseManager)
        {
            ConfigurationProvider = configurationProvider;
            WhereClauseGenerator = whereClauseGenerator;
            DatabaseManager = databaseManager;
        }

        protected IConfigurationProvider ConfigurationProvider { get; }

        protected IDatabaseManager DatabaseManager { get; }

        protected IWhereClauseGenerator WhereClauseGenerator { get; private set; }

        protected ISqlConnection CreateConnection()
        {
            if (DatabaseManager.Connection != null)
            {
                return DatabaseManager.Connection;
            }

            return new SqlConnectionWrapper(ConfigurationProvider.ConnectionString);
        }
    }
}