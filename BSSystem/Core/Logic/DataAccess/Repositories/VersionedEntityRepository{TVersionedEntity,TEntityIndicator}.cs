﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Repositories
{
    public class VersionedEntityRepository<TVersionedEntity, TEntityIndicator> :
        BaseRepository,
        IVersionedEntityRepository<TVersionedEntity, TEntityIndicator>
        where TVersionedEntity : VersionedEntity, TEntityIndicator, new()
        where TEntityIndicator : IStorable
    {
        private const string DeleteCommandTemplate =
            "UPDATE {0} SET [ChangerId] = @ChangerId, [TimeEnd] = GETUTCDATE() WHERE ([IsDeleted] = 0) AND {1};";

        private const string InsertCommandTemplate =
            "INSERT INTO {0} ({1}) VALUES ({2}); SELECT [Id] FROM {0} WHERE [Id] = ISNULL(SCOPE_IDENTITY(), @@IDENTITY);";

        private const string UpdateCommandTemplate = "UPDATE {0} SET {1} WHERE [Id] = @Id; SELECT @Id";

        private readonly IVersionedEntityDescriptor<TVersionedEntity> _versionedEntityDescriptor;

        public VersionedEntityRepository(
            IStorableObjectsMetadataProvider enitityMetadataProvider,
            ISessionDataProvider sessionDataProvider,
            IConfigurationProvider configurationProvider,
            IWhereClauseGenerator whereClauseGenerator,
            IDatabaseManager databaseManager)
            : base(configurationProvider, whereClauseGenerator, databaseManager)
        {
            _versionedEntityDescriptor = enitityMetadataProvider.GetVersionedEntityDescriptor<TVersionedEntity>();
            SessionDataProvider = sessionDataProvider;
        }

        protected ISessionDataProvider SessionDataProvider { get; }

        public async Task DeleteAsync(TEntityIndicator indicator)
        {
            if (indicator.Id == 0)
            {
                throw new InvalidOperationException("Entity was not save to Database.");
            }

            await DeleteAsync(x => x.Id == indicator.Id);
        }

        public async Task DeleteAsync(Expression<Func<TVersionedEntity, bool>> where)
        {
            var whereQuery = WhereClauseGenerator.Generate(where);

            var command = string.Format(
                DeleteCommandTemplate,
                _versionedEntityDescriptor.SqlName,
                whereQuery.Clause);
            using (var connection = CreateConnection())
            {
                var sqlCommand = connection.CreateSqlCommand(command);
                whereQuery.BindParameters(sqlCommand);
                sqlCommand.Parameters.AddWithValue("@ChangerId", SessionDataProvider.CurrentUser.Id);

                await sqlCommand.ExecuteNonQueryAsync();
            }
        }

        public void Write(TVersionedEntity versionedEntity)
        {
            if (!versionedEntity.Modified)
            {
                return;
            }

            var command = PrepareWriteCommandText(versionedEntity);

            using (var connection = CreateConnection())
            {
                var sqlCommand = connection.CreateSqlCommand(command);
                _versionedEntityDescriptor.MapWriteValues(sqlCommand, versionedEntity, SessionDataProvider);
                versionedEntity.Id = (int)sqlCommand.ExecuteScalar();
                versionedEntity.ChangerId = SessionDataProvider.CurrentUser.Id;
                versionedEntity.Modified = false;
            }
        }

        public async Task WriteAsync(TVersionedEntity versionedEntity)
        {
            if (!versionedEntity.Modified)
            {
                return;
            }

            var command = PrepareWriteCommandText(versionedEntity);

            using (var connection = CreateConnection())
            {
                var sqlCommand = connection.CreateSqlCommand(command);
                _versionedEntityDescriptor.MapWriteValues(sqlCommand, versionedEntity, SessionDataProvider);
                versionedEntity.Id = (int)(await sqlCommand.ExecuteScalarAsync());
                versionedEntity.ChangerId = SessionDataProvider.CurrentUser.Id;
                versionedEntity.Modified = false;
            }
        }

        private string PrepareWriteCommandText(TVersionedEntity versionedEntity)
        {
            string command;
            if (versionedEntity.Id == 0)
            {
                command = string.Format(
                    InsertCommandTemplate,
                    _versionedEntityDescriptor.SqlName,
                    _versionedEntityDescriptor.FormatInsertColumnsList(),
                    _versionedEntityDescriptor.FormatInsertValuesSection());
            }
            else
            {
                command = string.Format(
                    UpdateCommandTemplate,
                    _versionedEntityDescriptor.SqlName,
                    _versionedEntityDescriptor.FormatUpdateSetSection());
            }

            return command;
        }
    }
}