﻿using System;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public interface IMarkerTypeProvider
    {
        Type GetMarkerFor(Type type);
    }
}
