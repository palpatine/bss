﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    internal abstract class BaseDescriptor<TStorable>
        : IDescriptor<TStorable>
        where TStorable : IStorable
    {
        private IEnumerable<string> _insertColumns;
        private IEnumerable<PropertyInfo> _properties;
        private IEnumerable<string> _readColumns;
        private IEnumerable<string> _writeColumns;

        protected BaseDescriptor()
        {
            var type = typeof(TStorable);

            SqlName = $"[{StorableHelper.GetSchemaName(type)}].[{StorableHelper.GetEntityName(type)}]";
        }

        public string SqlName { get; set; }

        protected IEnumerable<PropertyInfo> FullProperties
        {
            get
            {
                if (_properties == null)
                {
                    _properties = GetFullProperties();
                }

                return _properties;
            }
        }

        protected IEnumerable<string> InsertColumns
        {
            get
            {
                if (_insertColumns == null)
                {
                    _insertColumns = GetInsertColumns();
                }

                return _insertColumns;
            }
        }

        protected virtual IEnumerable<PropertyInfo> InsertProperties
        {
            get
            {
                var result = FullProperties.Where(p => p.DeclaringType == typeof(TStorable));
                return result;
            }
        }

        protected IEnumerable<string> ReadColumns
        {
            get
            {
                if (_readColumns == null)
                {
                    _readColumns = GetReadColumns();
                }

                return _readColumns;
            }
        }

        protected virtual IEnumerable<PropertyInfo> ReadProperties
        {
            get
            {
                var result = FullProperties;
                return result;
            }
        }

        protected IEnumerable<string> WriteColumns
        {
            get
            {
                if (_writeColumns == null)
                {
                    _writeColumns = GetWriteColumns();
                }

                return _writeColumns;
            }
        }

        protected virtual IEnumerable<PropertyInfo> WriteProperties
        {
            get
            {
                var result = FullProperties.Where(p => p.DeclaringType == typeof(TStorable));
                return result;
            }
        }

        public string FormatColumnName(
            PropertyInfo property,
            string tableAlias = null)
        {
            if (typeof(TStorable) != property.DeclaringType &&
                !property.DeclaringType.IsAssignableFrom(typeof(TStorable)))
            {
                var message =
                    $"Property '{property.DeclaringType.Name + "." + property.Name}' must be declared on '{typeof(TStorable).Name}'";
                throw new NotSupportedException(message);
            }

            var name = StorableHelper.GetColumnName(property);
            return FormatColumnName(name, tableAlias);
        }

        public string FormatInsertColumnsList()
        {
            return string.Join(", ", InsertColumns.Select(x => FormatColumnName(x)));
        }

        public string FormatReadColumnsList(string columnAlias = null)
        {
            return string.Join(", ", ReadColumns.Select(x => FormatColumnName(x, columnAlias)));
        }

        public string FormatWriteColumnsList()
        {
            return string.Join(", ", WriteColumns.Select(x => FormatColumnName(x)));
        }

        void IDescriptor.MapValues(
            object entity,
            ISqlDataReader reader)
        {
            var stroable = (TStorable)entity;
            MapValues(stroable, reader);
        }

        public void MapValues(
            TStorable entity,
            ISqlDataReader reader)
        {
            foreach (var item in FullProperties)
            {
                if (item.PropertyType == typeof(DateTime))
                {
                    var attribute = item.GetCustomAttribute<SqlTypeAttribute>();
                    if (attribute == null || attribute.Type != SqlDbType.Date)
                    {
                        var utcDate = DateTime.SpecifyKind((DateTime)reader[item.Name], DateTimeKind.Utc);
                        item.SetValue(entity, utcDate);
                    }
                    else
                    {
                        var value = reader[item.Name];
                        if (value is DBNull)
                        {
                            item.SetValue(entity, null);
                        }
                        else
                        {
                            item.SetValue(entity, ((DateTime)value).Date);
                        }
                    }
                }
                else if (item.PropertyType == typeof(DateTime?))
                {
                    var readValue = reader[item.Name];
                    if (readValue.GetType() != typeof(DBNull))
                    {
                        var attribute = item.GetCustomAttribute<SqlTypeAttribute>();
                        if (attribute == null || attribute.Type != SqlDbType.Date)
                        {
                            var utcDate = DateTime.SpecifyKind((DateTime)readValue, DateTimeKind.Utc);
                            item.SetValue(entity, utcDate);
                        }
                        else
                        {
                            item.SetValue(entity, ((DateTime)reader[item.Name]).Date);
                        }
                    }
                }
                else if (item.PropertyType == typeof(IPAddress))
                {
                    var readValue = reader[item.Name];
                    if (readValue.GetType() != typeof(DBNull))
                    {
                        var value = new IPAddress((byte[])readValue);
                        item.SetValue(entity, value);
                    }
                }
                else if (typeof(IStorable).IsAssignableFrom(item.PropertyType))
                {
                    var readValue = reader[StorableHelper.GetColumnName(item)];
                    if (readValue.GetType() != typeof(DBNull))
                    {
                        var markerType = StorableHelper.GetMarkerType(item.PropertyType);
                        var marker = Activator.CreateInstance(markerType, readValue);
                        item.SetValue(entity, marker);
                    }
                }
                else
                {
                    var readValue = reader[StorableHelper.GetColumnName(item)];
                    item.SetValue(entity, readValue is DBNull ? null : readValue);
                }
            }
        }

        protected static string FormatParameterName(PropertyInfo property)
        {
            if (typeof(IStorable).IsAssignableFrom(property.PropertyType))
            {
                return $"@{property.Name}Id";
            }

            return $"@{property.Name}";
        }

        protected virtual IEnumerable<PropertyInfo> GetFullProperties()
        {
            return typeof(TStorable).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => p.GetCustomAttribute<SqlTypeAttribute>() != null);
        }

        protected virtual IEnumerable<string> GetInsertColumns()
        {
            return InsertProperties.Select(StorableHelper.GetColumnName).ToArray();
        }

        protected virtual IEnumerable<string> GetReadColumns()
        {
            return ReadProperties.Select(StorableHelper.GetColumnName).ToArray();
        }

        protected virtual IEnumerable<string> GetWriteColumns()
        {
            return WriteProperties.Select(StorableHelper.GetColumnName).ToArray();
        }

        private string FormatColumnName(
            string name,
            string tableAlias = null)
        {
            if (tableAlias == null)
            {
                return $"[{name}]";
            }

            return $"[{tableAlias}].[{name}]";
        }
    }
}