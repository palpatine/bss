﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public interface IDtoDescriptor<TDto> : IDescriptor<TDto>
        where TDto : IDto
    {
    }
}