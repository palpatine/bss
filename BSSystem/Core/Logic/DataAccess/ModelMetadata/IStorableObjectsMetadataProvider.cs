﻿using System;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public interface IStorableObjectsMetadataProvider
    {
        IDtoDescriptor<TDto> GetDtoDescriptor<TDto>()
            where TDto : IDto;

        IEntityDescriptor<TEntity> GetEntityDescriptor<TEntity>()
            where TEntity : IEntity;

        IVersionedEntityDescriptor<TVersionedEntity> GetVersionedEntityDescriptor<TVersionedEntity>()
            where TVersionedEntity : IVersionedEntity;

        IDescriptor<TStorable> GetDescriptor<TStorable>()
            where TStorable : IStorable;

        IDescriptor GetDescriptor(Type targetType);
    }
}