﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    internal sealed class DtoDescriptor<TDto> : BaseDescriptor<TDto>, IDtoDescriptor<TDto>
        where TDto : Dto
    {
    }
}