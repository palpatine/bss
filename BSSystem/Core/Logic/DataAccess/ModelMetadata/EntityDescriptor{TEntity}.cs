﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    internal sealed class EntityDescriptor<TEntity> :
        CommonEntityDescriptor<TEntity>,
        IEntityDescriptor<TEntity>
        where TEntity : Entity
    {
        public void MapWriteValues(
            ISqlCommand command,
            TEntity entity)
        {
            if (entity.Id > 0)
            {
                command.Parameters.AddWithValue(FormatParameterName(IdProperty), entity.Id);
            }

            foreach (var item in WriteProperties)
            {
                if (IsNullInNullableGenericType(entity, item) || IsNullString(entity, item))
                {
                    command.Parameters.AddWithValue(FormatParameterName(item), DBNull.Value);
                }
                else if (item.PropertyType == typeof(DateTime) || item.PropertyType == typeof(DateTime?))
                {
                    var attribute = item.GetCustomAttribute<SqlTypeAttribute>();
                    if (attribute == null || attribute.Type != SqlDbType.Date)
                    {
                        var date = (DateTime)item.GetValue(entity);

                        if (date.Kind != DateTimeKind.Utc)
                        {
                            throw new InvalidOperationException();
                        }

                        command.Parameters.AddWithValue(FormatParameterName(item), date);
                    }
                    else
                    {
                        command.Parameters.AddWithValue(FormatParameterName(item), (DateTime)item.GetValue(entity));
                    }
                }
                else if (item.PropertyType == typeof(IPAddress))
                {
                    var readValue = (IPAddress)item.GetValue(entity);

                    if (readValue == null)
                    {
                        var name = FormatParameterName(item);
                        command.Parameters.Add(name, SqlDbType.VarBinary, 4);
                        command.Parameters[name].Value = DBNull.Value;
                    }
                    else
                    {
                        command.Parameters.AddWithValue(FormatParameterName(item), readValue.GetAddressBytes());
                    }
                }
                else if (typeof(IStorable).IsAssignableFrom(item.PropertyType))
                {
                    var readValue = (IStorable)item.GetValue(entity);

                    if (readValue == null)
                    {
                        command.Parameters.AddWithValue(FormatParameterName(item), DBNull.Value);
                    }
                    else
                    {
                        command.Parameters.AddWithValue(FormatParameterName(item), readValue.Id);
                    }
                }
                else
                {
                    command.Parameters.AddWithValue(FormatParameterName(item), item.GetValue(entity));
                }
            }
        }
    }
}