﻿using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public interface IEntityDescriptor<TEntity> : IDescriptor<TEntity>
        where TEntity : IEntity
    {
        string FormatInsertValuesSection();

        string FormatUpdateSetSection();

        void MapWriteValues(
            ISqlCommand sqlCommand,
            TEntity entity);
    }
}