﻿using System;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public static class DtoDescriptor
    {
        public static IDescriptor Create(Type dtoType)
        {
            if (!typeof(Dto).IsAssignableFrom(dtoType))
            {
                throw new ArgumentException("Type must be a DTO.", "dtoType");
            }

            var type = typeof(DtoDescriptor<>).MakeGenericType(dtoType);
            return (IDescriptor)Activator.CreateInstance(type);
        }
    }
}