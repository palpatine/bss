﻿using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public interface ICommonEntityDescriptor<TEntity> : IDescriptor<TEntity>
        where TEntity : IEntity
    {
    }
}