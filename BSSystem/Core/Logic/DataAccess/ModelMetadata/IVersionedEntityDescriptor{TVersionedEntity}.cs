﻿using System.Data.SqlClient;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public interface IVersionedEntityDescriptor<TVersionedEntity> : IDescriptor<TVersionedEntity>
        where TVersionedEntity : IVersionedEntity
    {
        string FormatInsertValuesSection();

        string FormatUpdateSetSection();

        void MapWriteValues(
            ISqlCommand sqlCommand,
            TVersionedEntity entity,
            ISessionDataProvider sessionDataProvider);
    }
}