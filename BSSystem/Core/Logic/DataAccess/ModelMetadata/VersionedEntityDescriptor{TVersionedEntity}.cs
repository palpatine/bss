﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.Environment;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    internal class VersionedEntityDescriptor<TVersionedEntity>
        : CommonEntityDescriptor<TVersionedEntity>,
            IVersionedEntityDescriptor<TVersionedEntity>
        where TVersionedEntity : VersionedEntity
    {
        protected PropertyInfo ChangerIdProperty { get; } = ExpressionExtensions.GetProperty((TVersionedEntity e) => e.ChangerId);

        protected override IEnumerable<PropertyInfo> InsertProperties
        {
            get
            {
                var result = FullProperties
                    .Where(
                        p =>
                            p.GetCustomAttribute<SqlReadOnlyAttribute>() == null &&
                            p.DeclaringType == typeof(TVersionedEntity))
                    .Concat(
                        FullProperties.Where(
                            p =>
                                p.GetCustomAttribute<SqlReadOnlyAttribute>() == null &&
                                p.DeclaringType == typeof(VersionedEntity)));
                return result;
            }
        }

        protected override IEnumerable<PropertyInfo> WriteProperties
        {
            get
            {
                var result = FullProperties
                    .Where(
                        p =>
                            p.GetCustomAttribute<SqlReadOnlyAttribute>() == null &&
                            p.DeclaringType == typeof(TVersionedEntity))
                    .Concat(
                        FullProperties.Where(
                            p =>
                                p.GetCustomAttribute<SqlReadOnlyAttribute>() == null &&
                                p.DeclaringType == typeof(VersionedEntity)));
                return result;
            }
        }

        public void MapWriteValues(
            ISqlCommand command,
            TVersionedEntity versionedEntity,
            ISessionDataProvider provider)
        {
            if (versionedEntity.Id > 0)
            {
                CreateParameter(command, IdProperty, versionedEntity.Id);
            }

            foreach (var item in WriteProperties
                .Where(p => p.GetCustomAttribute<SqlReadOnlyAttribute>() == null))
            {
                if (item.Name == ChangerIdProperty.Name)
                {
                    CreateParameter(command, ChangerIdProperty, provider.CurrentUser.Id);
                }
                else if (IsNullInNullableGenericType(versionedEntity, item) || IsNullString(versionedEntity, item))
                {
                    CreateParameter(command, item, DBNull.Value);
                }
                else if (item.PropertyType == typeof(DateTime) || item.PropertyType == typeof(DateTime?))
                {
                    var attribute = item.GetCustomAttribute<SqlTypeAttribute>();
                    if (attribute == null || attribute.Type != SqlDbType.Date)
                    {
                        var date = (DateTime)item.GetValue(versionedEntity);

                        if (date.Kind != DateTimeKind.Utc)
                        {
                            throw new InvalidOperationException();
                        }

                        CreateParameter(command, item, date);
                    }
                    else
                    {
                        var unspecifiedDate = (DateTime)item.GetValue(versionedEntity);

                        if (unspecifiedDate.Kind == DateTimeKind.Local)
                        {
                            throw new InvalidOperationException(
                                "Date must be of unspecified or utc kind for properties marked with SqlDateAttribute.");
                        }

                        CreateParameter(command, item, unspecifiedDate);
                    }
                }
                else if (typeof(IStorable).IsAssignableFrom(item.PropertyType))
                {
                    var readValue = (IStorable)item.GetValue(versionedEntity);

                    if (readValue == null)
                    {
                        CreateParameter(command, item, DBNull.Value);
                    }
                    else
                    {
                        CreateParameter(command, item, readValue.Id);
                    }
                }
                else
                {
                    var readValue = item.GetValue(versionedEntity);
                    if (readValue == null)
                    {
                        CreateParameter(command, item, DBNull.Value);
                    }
                    else
                    {
                        CreateParameter(command, item, readValue);
                    }
                }
            }
        }

        private void CreateParameter(
            ISqlCommand command,
            PropertyInfo property,
            object value)
        {
            var typeMetadata = property.GetCustomAttribute<SqlTypeAttribute>();
            ISqlParameter parameter;
            if (StorableHelper.HasCapacity(typeMetadata.Type))
            {
                parameter = command.Parameters.Add(FormatParameterName(property), typeMetadata.Type, typeMetadata.Capacity);
            }
            else
            {
                parameter = command.Parameters.Add(FormatParameterName(property), typeMetadata.Type);
            }

            parameter.Value = value ?? DBNull.Value;
        }
    }
}