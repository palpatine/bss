﻿using System.Data.SqlClient;
using Qdarc.Modules.Common.QueryGeneration;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public interface IDescriptor
    {
        string SqlName { get; }

        string FormatInsertColumnsList();

        void MapValues(
            object entity,
            ISqlDataReader reader);
    }
}