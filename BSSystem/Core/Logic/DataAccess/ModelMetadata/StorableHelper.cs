﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.SqlServer.Server;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.Validation;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public static class StorableHelper
    {
        private const string AssemblyTemplate = "SolvesIt.BSSystem.{0}Module.Abstraction";
        private const string EntityTemplate = "SolvesIt.BSSystem.{0}Module.Abstraction.DataAccess.Entities.{1}";
        private const string ModuleRetrivalRegularExpression = @"SolvesIt\.BSSystem\.(.*?)Module\.Abstraction\.DataAccess\.Entities";
        private static readonly object Lock = new object();
        private static readonly ConcurrentDictionary<Type, Type> Markers;
        private static IEnumerable<IMarkerTypeProvider> _markerProviders;

        static StorableHelper()
        {
            Markers = new ConcurrentDictionary<Type, Type>();
        }

        private static IEnumerable<IMarkerTypeProvider> MarkerProviders
        {
            get
            {
                if (_markerProviders == null)
                {
                    _markerProviders = ServiceLocator.Current.GetAllInstances<IMarkerTypeProvider>();
                }

                return _markerProviders;
            }
        }

        public static IEnumerable<SqlDataRecord> ConvertToDataRecords<TUdt>(IEnumerable<TUdt> data)
            where TUdt : IUdt
        {
            if (!data.Any())
            {
                return null;
            }

            var properties = typeof(TUdt).GetProperties()
                .OrderBy(x => x.GetCustomAttribute<SqlColumnOrderAttribute>().Order)
                .ToArray();

            var columns = properties.Select(x =>
            {
                var attribute = x.GetCustomAttribute<SqlTypeAttribute>();
                SqlMetaData metaData;
                if (HasCapacity(attribute.Type))
                {
                    metaData = new SqlMetaData(
                        GetColumnName(x),
                        attribute.Type,
                        attribute.Capacity);
                }
                else
                {
                    metaData = new SqlMetaData(
                        GetColumnName(x),
                        attribute.Type);
                }

                return metaData;
            })
                .ToArray();

            var records = new List<SqlDataRecord>();

            foreach (var element in data)
            {
                var record = new SqlDataRecord(columns);
                record.SetValues(properties.Select(property =>
                {
                    var value = property.GetValue(element);
                    if (value == null)
                    {
                        return DBNull.Value;
                    }

                    var storable = value as IStorable;
                    if (storable != null)
                    {
                        return storable.Id;
                    }

                    return value;
                }).ToArray());
                records.Add(record);
            }

            return records;
        }

        public static string GetColumnName(PropertyInfo property)
        {
            if (typeof(IStorable).IsAssignableFrom(property.PropertyType) || property.PropertyType.IsEnum)
            {
                return property.Name + "Id";
            }

            return property.Name;
        }

        public static Type GetEntityIndicator(Type type)
        {
            var entityIndicator = TryGetEntityIndicator(type);
            if (entityIndicator == null)
            {
                throw new InvalidOperationException();
            }

            return entityIndicator;
        }

        public static string GetEntityName(Type type)
        {
            if (typeof(Dto).IsAssignableFrom(type)
                || typeof(Entity).IsAssignableFrom(type))
            {
                return GetEntityNameAndJunctionLevel(type).Item1;
            }

            var kind = GetEntityIndicator(type);

            return kind.Name.Substring(1);
        }

        public static Type GetEntityType(Type type)
        {
            var entityType = TryGetEntityType(type);
            if (entityType == null)
            {
                throw new InvalidOperationException();
            }

            return entityType;
        }

        public static TMarker GetMarkerInstance<TStorable, TMarker>(int id)
            where TStorable : IStorable
        {
            return (TMarker)GetMarkerInstance(typeof(TStorable), id);
        }

        public static IMarker GetMarkerInstance(
            Type storableType,
            int id)
        {
            var markerType = GetMarkerType(storableType);
            var marker = (IMarker)Activator.CreateInstance(markerType, id);
            return marker;
        }

        public static Type GetMarkerType(Type type)
        {
            Type markerType;
            if (Markers.TryGetValue(type, out markerType))
            {
                return markerType;
            }

            lock (Lock)
            {
                if (type.IsClass && !type.IsAbstract && typeof(IMarker).IsAssignableFrom(type) && !typeof(Entity).IsAssignableFrom(type))
                {
                    Markers.TryAdd(type, type);
                    return type;
                }

                markerType = MarkerProviders.Select(x => x.GetMarkerFor(type)).SingleOrDefault(x => x != null);

                if (markerType == null)
                {
                    var name = GetEntityName(type) + "Marker";

                    var typeNameSections = type.AssemblyQualifiedName.Split(',')[0].Split('.');
                    var nameSpace = typeNameSections[typeNameSections.Length - 2];

                    var qualifiedName = type.AssemblyQualifiedName.Replace(
                        nameSpace + "." + type.Name,
                        "Markers." + name);

                    markerType = Type.GetType(qualifiedName);
                }

                Markers.TryAdd(type, markerType);

                if (markerType != null)
                {
                    Markers.TryAdd(markerType, markerType);
                }

                return markerType;
            }
        }

        public static string GetQualifiedColumnName(PropertyInfo property)
        {
            return string.Format(CultureInfo.InvariantCulture, "[{0}]", GetColumnName(property));
        }

        public static string GetQualifiedEntityName<TEntityIndicator>()
            where TEntityIndicator : IStorable
        {
            var entityType = GetEntityType(typeof(TEntityIndicator));
            var moduleMatch = Regex.Match(entityType.Namespace, ModuleRetrivalRegularExpression);

            if (!moduleMatch.Success)
            {
                throw new InvalidOperationException("Namespace of given entity is wrong.");
            }

            var module = moduleMatch.Groups[1].Value;
            var name = entityType.Name;
            return $"{module}_{name}";
        }

        public static string GetQualifiedTableName(Type type)
        {
            return string.Format(CultureInfo.InvariantCulture, "[{0}].[{1}]", GetSchemaName(type), GetEntityName(type));
        }

        public static string GetQualifiedUpdateProcedureName(Type type)
        {
            return $"[{GetSchemaName(type)}].[Update_{GetEntityName(type)}]";
        }

        public static string GetSchemaName(Type storableType)
        {
            var schemaNameProvider = storableType.Assembly.GetCustomAttribute<DefaultSchemaNameAttribute>();
            if (schemaNameProvider == null)
            {
                throw new InvalidOperationException(
                    "Assembly containing storable objects must be marked with DefaultSchemaNameAttribute");
            }

            return schemaNameProvider.Name;
        }

        public static SqlTypeAttribute GetSqlTypeAttribute<TModel, TValue>(
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            var property = propertyExpression.GetProperty();
            return GetSqlTypeAttribute(typeof(TModel), property);
        }

        public static SqlTypeAttribute GetSqlTypeAttribute(
            Type modelType,
            PropertyInfo property)
        {
            var entityProperty = property;
            if (modelType.BaseType != typeof(Entity) && modelType.BaseType != typeof(VersionedEntity))
            {
                var propertySource = property.GetCustomAttribute<PropertySourceAttribute>();

                Type entityType;
                string propertyName;

                if (propertySource != null)
                {
                    if (propertySource.Ignore)
                    {
                        return null;
                    }

                    entityType = propertySource.SourceEntityType;
                    propertyName = propertySource.PropertyName ?? property.Name;
                }
                else
                {
                    entityType = TryGetEntityType(modelType);

                    if (entityType == null)
                    {
                        return property.GetCustomAttribute<SqlTypeAttribute>();
                    }

                    propertyName = property.Name;
                }

                entityProperty = entityType.GetProperty(propertyName);
            }

            if (entityProperty == null)
            {
                throw new InvalidOperationException($"Cannot determine SQLType for property {property.DeclaringType.Name}.{property.Name}.");
            }

            var sqltype = entityProperty.GetCustomAttribute<SqlTypeAttribute>();
            return sqltype;
        }

        public static string GetTableKey<TEntity>()
            where TEntity : IEntity
        {
            return GetTableKey(typeof(TEntity));
        }

        public static string GetTableKey(Type type)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}_{1}", GetSchemaName(type), GetEntityName(type));
        }

        public static bool HasCapacity(SqlDbType sqlType)
        {
            switch (sqlType)
            {
                case SqlDbType.Image:
                case SqlDbType.Text:
                case SqlDbType.UniqueIdentifier:
                case SqlDbType.Date:
                case SqlDbType.Time:
                case SqlDbType.DateTime2:
                case SqlDbType.DateTimeOffset:
                case SqlDbType.TinyInt:
                case SqlDbType.SmallInt:
                case SqlDbType.Int:
                case SqlDbType.SmallDateTime:
                case SqlDbType.Money:
                case SqlDbType.DateTime:
                case SqlDbType.Xml:
                case SqlDbType.Timestamp:
                case SqlDbType.NText:
                case SqlDbType.SmallMoney:
                case SqlDbType.Float:
                case SqlDbType.BigInt:
                case SqlDbType.Real:
                case SqlDbType.Bit:
                case SqlDbType.Decimal:
                    return false;

                case SqlDbType.VarBinary:
                case SqlDbType.VarChar:
                case SqlDbType.Binary:
                case SqlDbType.Char:
                case SqlDbType.NVarChar:
                case SqlDbType.NChar:
                    return true;
            }

            throw new InvalidOperationException();
        }

        public static bool IsPocoClassIndicator(Type type)
        {
            return type.GetCustomAttribute<PocoClassIndicatorAttribute>() != null;
        }

        public static Type TryGetEntityIndicator(Type type)
        {
            if (type.IsInterface && typeof(IStorable).IsAssignableFrom(type))
            {
                if (IsPocoClassIndicator(type))
                {
                    throw new InvalidOperationException();
                }

                return type;
            }

            var kinds = type.GetInterfaces()
                .Where(x => !IsPocoClassIndicator(x))
                .ToList();

            if (kinds.Count == 1)
            {
                return kinds.Single();
            }

            var kind = kinds.SingleOrDefault(x => kinds.Except(new[] { x }).All(x.IsAssignableFrom));

            return kind;
        }

        public static Type TryGetEntityType(Type type)
        {
            var entityIndicator = TryGetEntityIndicator(type);
            if (entityIndicator == null)
            {
                return null;
            }

            var name = GetEntityName(type);

            var typeNameSections = entityIndicator.AssemblyQualifiedName.Split(',')[0].Split('.');
            var nameSpace = typeNameSections[typeNameSections.Length - 2];

            var qualifiedName = entityIndicator.AssemblyQualifiedName.Replace(nameSpace + "." + entityIndicator.Name, "Entities." + name);

            return Type.GetType(qualifiedName);
        }

        public static bool TryGetEntityType(
            string tableKey,
            out Type entityType)
        {
            var data = tableKey.Split('_');
            if (data.Length != 2)
            {
                entityType = null;
                return false;
            }

            var module = data[0];
            var className = data[1];

            var entityTypeName = string.Format(CultureInfo.InvariantCulture, EntityTemplate, module, className);
            var assemblyName = string.Format(CultureInfo.InvariantCulture, AssemblyTemplate, module);
            Assembly assembly;
            try
            {
                assembly = Assembly.Load(assemblyName);
            }
            catch (FileNotFoundException)
            {
                entityType = null;
                return false;
            }

            entityType = assembly.GetType(entityTypeName, false);
            return entityType != null;
        }

        public static bool TryGetJunctionTable(
            Type first,
            Type second,
            out Type junctionType)
        {
            if (!typeof(IEntity).IsAssignableFrom(first)
                || !typeof(IEntity).IsAssignableFrom(second))
            {
                junctionType = null;
                return false;
            }

            var junctionTypeName = string.Join(string.Empty, new[] { first.Name, second.Name }.OrderBy(x => x));

            junctionType = first.Assembly.GetType($"{first.Namespace}.{junctionTypeName}", false)
                           ??
                           second.Assembly.GetType($"{second.Namespace}.{junctionTypeName}", false);

            if (junctionType == null)
            {
                return false;
            }

            return junctionType.GetCustomAttribute<JunctionTableAttribute>() != null;
        }

        private static Tuple<string, int> GetEntityNameAndJunctionLevel(Type type)
        {
            if (typeof(Entity) == type || typeof(Dto) == type)
            {
                throw new InvalidOperationException();
            }

            var junction = type.GetCustomAttribute<JunctionTableAttribute>();

            if (junction == null)
            {
                return Tuple.Create(type.Name, 0);
            }

            var types = new[]
            {
                junction.FirstEntity,
                junction.SecondEntity
            }
            .OrderBy(x => x.Name)
            .ToArray();

            var first = GetEntityNameAndJunctionLevel(types[0]);
            var second = GetEntityNameAndJunctionLevel(types[1]);
            var level = Math.Max(first.Item2, second.Item2) + 1;
            return Tuple.Create($"{first.Item1}{new string('_', level)}{second.Item1}", level);
        }
    }
}