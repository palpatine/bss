﻿using System;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    internal static class EntityDescriptor
    {
        public static IDescriptor Create(Type entityType)
        {
            if (!typeof(Entity).IsAssignableFrom(entityType))
            {
                throw new ArgumentException("Type must be an entity.", "type");
            }

            var type = typeof(EntityDescriptor<>).MakeGenericType(entityType);
            return (IDescriptor)Activator.CreateInstance(type);
        }
    }
}