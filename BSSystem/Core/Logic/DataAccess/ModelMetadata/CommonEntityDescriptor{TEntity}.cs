﻿using System;
using System.Linq;
using System.Reflection;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    internal abstract class CommonEntityDescriptor<TEntity> :
        BaseDescriptor<TEntity>,
        ICommonEntityDescriptor<TEntity>
        where TEntity : Entity
    {
        protected PropertyInfo IdProperty { get; } = ExpressionExtensions.GetProperty((TEntity e) => e.Id);

        public virtual string FormatInsertValuesSection()
        {
            return string.Join(", ", InsertProperties.Select(FormatParameterName));
        }

        public virtual string FormatUpdateSetSection()
        {
            return string.Join(
                ", ",
                WriteProperties.Select(x => $"{FormatColumnName(x)} = {FormatParameterName(x)}"));
        }

        protected static bool IsNullInNullableGenericType(
            TEntity entity,
            PropertyInfo item)
        {
            return item.PropertyType.IsGenericType &&
                   item.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) && item.GetValue(entity) == null;
        }

        protected static bool IsNullString(
            TEntity entity,
            PropertyInfo item)
        {
            return item.PropertyType == typeof(string) && string.IsNullOrWhiteSpace((string)item.GetValue(entity));
        }
    }
}