﻿using System.Data.SqlClient;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    public interface IDescriptor<TStorable> : IDescriptor
        where TStorable : IStorable
    {
    }
}