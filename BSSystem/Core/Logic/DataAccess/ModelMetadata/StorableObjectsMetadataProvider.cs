﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    internal sealed class StorableObjectsMetadataProvider : IStorableObjectsMetadataProvider, ITableNameResolver
    {
        private readonly IDictionary<Type, IDescriptor> _dtoDescriptors = new Dictionary<Type, IDescriptor>();
        private readonly IDictionary<Type, IDescriptor> _entityDescriptors = new Dictionary<Type, IDescriptor>();

        private readonly object _lock = new object();

        private readonly IDictionary<Type, IDescriptor> _versionedEntityDescriptors =
            new Dictionary<Type, IDescriptor>();

        public IDescriptor GetDescriptor<TStorable>()
            where TStorable : IStorable
        {
            var item = typeof(TStorable);
            return GetDescriptor(item);
        }

        public IDescriptor GetDescriptor(Type item)
        {
            if (typeof(IVersionedEntity).IsAssignableFrom(item))
            {
                if (!_versionedEntityDescriptors.ContainsKey(item))
                {
                    lock (_lock)
                    {
                        if (!_versionedEntityDescriptors.ContainsKey(item))
                        {
                            var descriptor = VersionedEntityDecriptor.Create(item);
                            _versionedEntityDescriptors.Add(item, descriptor);
                        }
                    }
                }

                return _versionedEntityDescriptors[item];
            }

            if (typeof(IEntity).IsAssignableFrom(item))
            {
                if (!_entityDescriptors.ContainsKey(item))
                {
                    lock (_lock)
                    {
                        if (!_entityDescriptors.ContainsKey(item))
                        {
                            var descriptor = EntityDescriptor.Create(item);
                            _entityDescriptors.Add(item, descriptor);
                        }
                    }
                }

                return _entityDescriptors[item];
            }

            if (!_dtoDescriptors.ContainsKey(item))
            {
                lock (_lock)
                {
                    if (!_dtoDescriptors.ContainsKey(item))
                    {
                        var descriptor = DtoDescriptor.Create(item);
                        _dtoDescriptors.Add(item, descriptor);
                    }
                }
            }

            return _dtoDescriptors[item];
        }

        public IDtoDescriptor<TDto> GetDtoDescriptor<TDto>()
            where TDto : IDto
        {
            var item = typeof(TDto);
            if (!_dtoDescriptors.ContainsKey(item))
            {
                lock (_lock)
                {
                    if (!_dtoDescriptors.ContainsKey(item))
                    {
                        var descriptor = DtoDescriptor.Create(item);
                        _dtoDescriptors.Add(item, descriptor);
                    }
                }
            }

            return (IDtoDescriptor<TDto>)_dtoDescriptors[item];
        }

        public IEntityDescriptor<TEntity> GetEntityDescriptor<TEntity>()
            where TEntity : IEntity
        {
            var item = typeof(TEntity);
            if (!_entityDescriptors.ContainsKey(item))
            {
                lock (_lock)
                {
                    if (!_entityDescriptors.ContainsKey(item))
                    {
                        var descriptor = EntityDescriptor.Create(item);
                        _entityDescriptors.Add(item, descriptor);
                    }
                }
            }

            return (IEntityDescriptor<TEntity>)_entityDescriptors[item];
        }

        public IVersionedEntityDescriptor<TVersionedEntity> GetVersionedEntityDescriptor<TVersionedEntity>()
            where TVersionedEntity : IVersionedEntity
        {
            var item = typeof(TVersionedEntity);
            if (!_versionedEntityDescriptors.ContainsKey(item))
            {
                lock (_lock)
                {
                    if (!_versionedEntityDescriptors.ContainsKey(item))
                    {
                        var descriptor = VersionedEntityDecriptor.Create(item);
                        _versionedEntityDescriptors.Add(item, descriptor);
                    }
                }
            }

            return (IVersionedEntityDescriptor<TVersionedEntity>)_versionedEntityDescriptors[item];
        }

        public string GetTableName(Type type)
        {
            return GetDescriptor(type).SqlName;
        }

        IDescriptor<TStorable> IStorableObjectsMetadataProvider.GetDescriptor<TStorable>()
        {
            return (IDescriptor<TStorable>)GetDescriptor<TStorable>();
        }

        public string GetColumnName(
            ITableReferenceProvider tableReferenceProvider,
            PropertyInfo property)
        {
            if (!typeof(IStorable).IsAssignableFrom(property.DeclaringType)
                && !typeof(IDto).IsAssignableFrom(property.DeclaringType))
            {
                return null;
            }

            if (tableReferenceProvider.Type != property.DeclaringType
                && property.DeclaringType.IsInterface
                && property.DeclaringType.IsAssignableFrom(tableReferenceProvider.Type))
            {
                var map = tableReferenceProvider.Type.GetInterfaceMap(property.DeclaringType);
                var id = map.InterfaceMethods.ToList().IndexOf(property.GetGetMethod());
                var classMember = map.TargetMethods[id];
                var mappedProperty = tableReferenceProvider.Type.GetProperties(BindingFlags.Instance |
                                                                               BindingFlags.NonPublic |
                                                                               BindingFlags.Public)
                    .Single(x => x.GetGetMethod(true) == classMember);

                var attribute = mappedProperty.GetCustomAttribute<SqlNameAttribute>();
                if (attribute != null)
                {
                    return attribute.Name;
                }
            }
            else
            {
                var attribute = property.GetCustomAttribute<SqlNameAttribute>();
                if (attribute != null)
                {
                    return attribute.Name;
                }
            }

            var nameSuffix = string.Empty;

            if (property.PropertyType.IsEnum || typeof(IStorable).IsAssignableFrom(property.PropertyType))
            {
                nameSuffix = "Id";
            }

            return $"[{property.Name}{nameSuffix}]";
        }
    }
}