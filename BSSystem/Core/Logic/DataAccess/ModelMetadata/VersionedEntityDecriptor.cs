﻿using System;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata
{
    internal static class VersionedEntityDecriptor
    {
        public static IDescriptor Create(Type entityType)
        {
            if (!typeof(VersionedEntity).IsAssignableFrom(entityType))
            {
                throw new ArgumentException("Type must be an versionedEntity.", "type");
            }

            var type = typeof(VersionedEntityDescriptor<>).MakeGenericType(entityType);
            return (IDescriptor)Activator.CreateInstance(type);
        }
    }
}