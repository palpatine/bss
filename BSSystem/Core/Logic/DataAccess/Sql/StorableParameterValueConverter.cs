﻿using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    internal class StorableParameterValueConverter : QueryParameterValueConverter<IStorable>
    {
        public override object Convert(IStorable value)
        {
            return value.Id;
        }
    }
}