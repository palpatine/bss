﻿namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    internal sealed class OrderDescriptor
    {
        public OrderDescriptor(
            string column,
            OrderDirection direction = OrderDirection.Ascencing)
        {
            Column = column;
            Direction = direction;
        }

        public string Column { get; private set; }

        public OrderDirection Direction { get; private set; }
    }
}