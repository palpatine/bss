﻿using System;
using System.Data.SqlClient;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    internal sealed class DatabaseManager : IDatabaseManager
    {
        private readonly IConfigurationProvider _configurationProvider;

        private SqlConnectionWrapper _connection;

        private bool _isTransactionCommited;

        public DatabaseManager(IConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
        }

        public ISqlConnection Connection { get; private set; }

        private SqlTransaction Transaction { get; set; }

        public void ComitTransaction()
        {
            if (Transaction == null)
            {
                return;
            }

            Transaction.Commit();
            _isTransactionCommited = true;
        }

        public IDisposable OpenTransaction()
        {
            if (Transaction != null)
            {
                var transaction = Transaction;
                Transaction = null;
                return new Disposer(() => { Transaction = transaction; });
            }

            Connection = _connection = new SqlConnectionWrapper(_configurationProvider.ConnectionString, false);
            Transaction = Connection.BeginTransaction();

            return new Disposer(CloseConnection);
        }

        private void CloseConnection()
        {
            if (!_isTransactionCommited)
            {
                Transaction.Rollback();
            }

            _connection.Dispose();
            Transaction = null;
            _isTransactionCommited = false;
            _connection = null;
            Connection = null;
        }
    }
}