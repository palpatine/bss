﻿using System;
using System.Linq.Expressions;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    public interface IWhereClauseGenerator
    {
        WhereClause Generate<TStorable>(
            Expression<Func<TStorable, bool>> where,
            string tablePrefix = null)
            where TStorable : IStorable, new();

        WhereClause Generate(
            Type type,
            LambdaExpression where,
            string tablePrefix = null);
    }
}