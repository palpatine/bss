﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.QueryGeneration.Attributes;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    internal class WhereClauseGenerator
        : ExpressionVisitor,
        IWhereClauseGenerator
    {
        private readonly Stack<WhereClause> _stack = new Stack<WhereClause>();
        private int _parameterCounter;
        private string _tablePrefix;

        public WhereClause Generate<TStorable>(
            Expression<Func<TStorable, bool>> where,
            string tablePrefix = null)
            where TStorable : IStorable, new()
        {
            return Generate(typeof(TStorable), where, tablePrefix);
        }

        public WhereClause Generate(
            Type type,
            LambdaExpression where,
            string tablePrefix = null)
        {
            _tablePrefix = tablePrefix;
            Visit(where);
            var result = _stack.Pop();

            EnsureBooleanComparision(where.Body, result);

            if (_stack.Any())
            {
                throw new InvalidOperationException();
            }

            return result;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            var value = base.VisitBinary(node);
            string comparisionOperator = null;

            switch (node.NodeType)
            {
                case ExpressionType.GreaterThan:
                    comparisionOperator = ">";
                    break;

                case ExpressionType.Equal:
                    comparisionOperator = "=";
                    break;

                case ExpressionType.GreaterThanOrEqual:
                    comparisionOperator = ">=";
                    break;

                case ExpressionType.LessThan:
                    comparisionOperator = "<";
                    break;

                case ExpressionType.LessThanOrEqual:
                    comparisionOperator = "<=";
                    break;

                case ExpressionType.NotEqual:
                    comparisionOperator = "<>";
                    break;

                case ExpressionType.AndAlso:
                    comparisionOperator = "AND";
                    break;

                case ExpressionType.OrElse:
                    comparisionOperator = "OR";
                    break;

                default:
                    throw new InvalidOperationException();
            }

            var right = _stack.Pop();
            var left = _stack.Pop();

            if (node.NodeType == ExpressionType.AndAlso || node.NodeType == ExpressionType.OrElse)
            {
                EnsureBooleanComparision(node.Left, left);
                EnsureBooleanComparision(node.Right, right);
            }
            else
            {
                var parameter = right.Parameters.Single().Value;
                if (parameter == null)
                {
                    string comparision;

                    if (comparisionOperator == "=")
                    {
                        comparision = "is null";
                    }
                    else if (comparisionOperator == "!=")
                    {
                        comparision = "is not null";
                    }
                    else
                    {
                        throw new InvalidOperationException("value cannot be compared with null using " + comparisionOperator);
                    }

                    var isNullClauser = new WhereClause
                    {
                        Clause = $"({left.Clause} {comparision})",
                        Parameters = left.Parameters
                    };

                    _stack.Push(isNullClauser);
                    return value;
                }

                var nodeLeftAttribute = ((MemberExpression)node.Left).Member.GetCustomAttribute<SqlTypeAttribute>();

                if (node.Left.Type == typeof(DateTime)
                    && node.Left.NodeType == ExpressionType.MemberAccess
                    && ((MemberExpression)node.Left).Expression.NodeType == ExpressionType.Parameter
                    && (nodeLeftAttribute == null || nodeLeftAttribute.Type != SqlDbType.Date))
                {
                    var date = (DateTime)parameter;
                    var utc = date.ToUniversalTime();
                    right.Parameters[right.Parameters.Single().Key] = utc;
                }
                else if (node.Left.Type == typeof(DateTime?)
                         && node.Left.NodeType == ExpressionType.MemberAccess
                         && ((MemberExpression)node.Left).Expression.NodeType == ExpressionType.Parameter
                         && (nodeLeftAttribute == null || nodeLeftAttribute.Type != SqlDbType.Date))
                {
                    var date = (DateTime?)parameter;
                    if (date.HasValue)
                    {
                        var utc = date.Value.ToUniversalTime();
                        right.Parameters[right.Parameters.Single()
                            .Key] = utc;
                    }
                }
            }

            var clauser = new WhereClause
            {
                Clause = $"({left.Clause} {comparisionOperator} {right.Clause})",
                Parameters = left.Parameters.Concat(right.Parameters.EmptyIfNull()).ToDictionary(x => x.Key, x => x.Value)
            };

            _stack.Push(clauser);

            return value;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var parameterName = GetParameterName();
            var value = node.Value;
            var storable = value as IStorable;
            if (storable != null)
            {
                value = storable.Id;
            }

            var clause = new WhereClause
            {
                Clause = parameterName,
                Parameters = new Dictionary<string, object> { { parameterName, value } }
            };

            _stack.Push(clause);

            return base.VisitConstant(node);
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            Visit(node.Body);
            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Expression.NodeType == ExpressionType.Parameter)
            {
                _stack.Push(new WhereClause(FormatColumnName(node.Member)));
                return node;
            }

            if (node.Expression.NodeType == ExpressionType.MemberAccess)
            {
                var innerMember = (MemberExpression)node.Expression;
                if (innerMember.Expression.NodeType == ExpressionType.Parameter)
                {
                    _stack.Push(new WhereClause(FormatColumnName(innerMember.Member)));
                    return node;
                }

                if (innerMember.Expression.NodeType == ExpressionType.Convert)
                {
                    var unaryExpression = (UnaryExpression)innerMember.Expression;
                    if (unaryExpression.Operand.NodeType == ExpressionType.Parameter)
                    {
                        _stack.Push(new WhereClause(FormatColumnName(innerMember.Member)));
                        return node;
                    }
                }
            }

            if (node.Expression.NodeType == ExpressionType.Convert)
            {
                var unaryExpression = (UnaryExpression)node.Expression;
                if (unaryExpression.Operand.NodeType == ExpressionType.Parameter)
                {
                    _stack.Push(new WhereClause(FormatColumnName(node.Member)));
                    return node;
                }
            }

            var method = Expression.Lambda<Func<object>>(Expression.Convert(node, typeof(object))).Compile();
            var data = method();
            var storable = data as IStorable;
            if (storable != null)
            {
                data = storable.Id;
            }

            var parameterName = GetParameterName();
            var clause = new WhereClause
            {
                Clause = parameterName,
                Parameters = new Dictionary<string, object> { { parameterName, data } }
            };

            _stack.Push(clause);

            return node;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method == ExpressionExtensions.GetMethod<string>(x => x.StartsWith(string.Empty)))
            {
                return HandeStartsWith(node);
            }

            if (node.Method == ExpressionExtensions.GetMethod<string>(x => x.Contains(string.Empty)))
            {
                return HandeStringContains(node);
            }

            if (node.Method == ExpressionExtensions.GetMethod<ITableQuery<int>>(x => x.Contains(0)))
            {
                return HandeIntQueryContains(node);
            }

            if (IsEnumerableContains(node.Method) || IsListContains(node.Method))
            {
                return HandleCollectionContains(node);
            }

            if (node.Method.IsGenericMethod
                &&
                node.Method.GetGenericMethodDefinition() ==
                ExpressionExtensions.GetMethod(() => StorableExtensions.IsEquivalent<IStorable>(null, null))
                    .GetGenericMethodDefinition())
            {
                return HandeStorableEquivalence(node);
            }

            throw new NotSupportedException();
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            _stack.Push(new WhereClause
            {
                Clause = FormatIdColumnName()
            });
            return node;
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Not)
            {
                var value = base.VisitUnary(node);

                var argument = _stack.Pop();

                if (node.Operand.NodeType == ExpressionType.MemberAccess
                    && ((MemberExpression)node.Operand).Expression.NodeType == ExpressionType.Parameter
                    && node.Type == typeof(bool))
                {
                    _stack.Push(new WhereClause($"({argument.Clause} = 0)", argument.Parameters));
                }
                else
                {
                    _stack.Push(new WhereClause($"(NOT {argument.Clause})", argument.Parameters));
                }

                return value;
            }

            if (node.NodeType == ExpressionType.Convert)
            {
                var method =
                    Expression.Lambda<Func<object>>(Expression.Convert(node.Operand, typeof(object))).Compile();
                var data = method();

                var parameterName = GetParameterName();
                var clause = new WhereClause
                {
                    Clause = parameterName,
                    Parameters = new Dictionary<string, object> { { parameterName, data } }
                };

                _stack.Push(clause);

                return node;
            }

            throw new InvalidOperationException();
        }

        private static bool IsEnumerableContains(MethodInfo method)
        {
            return method.IsGenericMethod
                   &&
                   method.GetGenericMethodDefinition() ==
                   ExpressionExtensions.GetMethod<IEnumerable<string>>(x => x.Contains(string.Empty))
                       .GetGenericMethodDefinition();
        }

        private static bool IsListContains(MethodInfo method)
        {
            return method == ExpressionExtensions.GetMethod<List<string>>(x => x.Contains(string.Empty));
        }

        private void EnsureBooleanComparision(
            Expression node,
            WhereClause clause)
        {
            if (node.NodeType == ExpressionType.MemberAccess)
            {
                var memberExpression = (MemberExpression)node;
                var property = (PropertyInfo)memberExpression.Member;
                if (property.PropertyType == typeof(bool))
                {
                    var parameterName = GetParameterName();
                    clause.Clause = $"({clause.Clause} = {parameterName})";
                    clause.Parameters.Add(parameterName, true);
                }
            }
        }

        private string FormatColumnName(MemberInfo member)
        {
            var property = (PropertyInfo)member;

            var nameSuffix = string.Empty;
            if (typeof(IStorable).IsAssignableFrom(property.PropertyType))
            {
                nameSuffix = "Id";
            }

            if (_tablePrefix == null)
            {
                return $"[{property.Name}{nameSuffix}]";
            }

            return $"[{_tablePrefix}].[{property.Name}{nameSuffix}]";
        }

        private string FormatIdColumnName()
        {
            if (_tablePrefix == null)
            {
                return string.Format("[Id]");
            }

            return $"[{_tablePrefix}].[Id]";
        }

        private string GetParameterName()
        {
            _parameterCounter++;
            var parameterName = "@Parameter" + _parameterCounter;
            return parameterName;
        }

        private Expression HandeIntQueryContains(MethodCallExpression node)
        {
            var argument = node.Arguments.ElementAt(0);
            var memberExpression = (MemberExpression)argument;
            var query = (ITableQuery<int>)((FieldInfo)memberExpression.Member).GetValue(((ConstantExpression)memberExpression.Expression).Value);
            var innerQuery = query.Provider.ParseQueryExpression(query.Expression);

            Visit(node.Arguments.ElementAt(1));

            var right = _stack.Pop();
            var parameters = right.Parameters.Concat(innerQuery.ParameterValues.ToDictionary(x => x.Item1, x => x.Item2)).ToDictionary(x => x.Key, x => x.Value);

            _stack.Push(new WhereClause
            {
                Clause = $"({right.Clause} in ({innerQuery.QueryText}))",
                Parameters = parameters
            });

            return node;
        }

        private Expression HandeStartsWith(MethodCallExpression node)
        {
            var argument = node.Arguments.ElementAt(0);
            Visit(argument);
            Visit(node.Object);

            var right = _stack.Pop();
            var left = _stack.Pop();

            _stack.Push(new WhereClause
            {
                Clause = $"({right.Clause} like ({left.Clause} + N'%'))",
                Parameters = left.Parameters
            });

            return node;
        }

        private Expression HandeStorableEquivalence(MethodCallExpression node)
        {
            var first = node.Arguments.ElementAt(0);
            var second = node.Arguments.ElementAt(1);
            if (first.NodeType == ExpressionType.MemberAccess)
            {
                var member = (MemberExpression)first;

                if (member.Expression.NodeType == ExpressionType.Parameter)
                {
                    Visit(second);
                    var parameter = _stack.Pop();
                    _stack.Push(new WhereClause
                    {
                        Clause = $"({FormatColumnName(member.Member)} = {parameter.Clause})",
                        Parameters = parameter.Parameters
                    });
                    return node;
                }
                else if (member.Expression.NodeType == ExpressionType.Convert)
                {
                    var unaryExpression = (UnaryExpression)member.Expression;
                    if (unaryExpression.Operand.NodeType == ExpressionType.Parameter)
                    {
                        Visit(second);
                        var parameter = _stack.Pop();
                        _stack.Push(new WhereClause
                        {
                            Clause = $"({FormatColumnName(member.Member)} = {parameter.Clause})",
                            Parameters = parameter.Parameters
                        });
                        return node;
                    }
                }
            }
            else if (first.NodeType == ExpressionType.Parameter)
            {
                Visit(second);
                var parameter = _stack.Pop();
                _stack.Push(new WhereClause
                {
                    Clause = $"({FormatIdColumnName()} = {parameter.Clause})",
                    Parameters = parameter.Parameters
                });
                return node;
            }
            else if (first.NodeType == ExpressionType.Convert)
            {
                var unaryExpression = (UnaryExpression)first;
                if (unaryExpression.Operand.NodeType == ExpressionType.Parameter)
                {
                    Visit(second);
                    var parameter = _stack.Pop();
                    _stack.Push(new WhereClause
                    {
                        Clause = $"({FormatIdColumnName()} = {parameter.Clause})",
                        Parameters = parameter.Parameters
                    });
                    return node;
                }
            }

            throw new NotSupportedException();
        }

        private Expression HandeStringContains(MethodCallExpression node)
        {
            var argument = node.Arguments.ElementAt(0);
            Visit(argument);
            Visit(node.Object);

            var right = _stack.Pop();
            var left = _stack.Pop();

            _stack.Push(new WhereClause
            {
                Clause = $"({right.Clause} like (N'%' + {left.Clause} + N'%')",
                Parameters = left.Parameters
            });

            return node;
        }

        private Expression HandleCollectionContains(MethodCallExpression node)
        {
            var parameterSource = node.Object ?? node.Arguments[0];
            Visit(node.Arguments[node.Object == null ? 1 : 0]);

            var method =
                Expression.Lambda<Func<IEnumerable>>(Expression.Convert(parameterSource, typeof(IEnumerable)))
                    .Compile();
            var data = method();
            var left = _stack.Pop();

            var paramters = data.Cast<object>().ToDictionary(
                x => GetParameterName(),
                x =>
                {
                    var storable = x as IStorable;
                    if (storable != null)
                    {
                        return storable.Id;
                    }

                    return x;
                });

            if (paramters.Any())
            {
                _stack.Push(new WhereClause
                {
                    Clause = $"({left.Clause} IN ({string.Join(",", paramters.Keys)}))",
                    Parameters = paramters
                });
            }
            else
            {
                _stack.Push(new WhereClause
                {
                    Clause = "(FALSE)",
                });
            }

            return node;
        }
    }
}