﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    internal sealed class ObjectMapper : IObjectMapper
    {
        private static readonly ConcurrentDictionary<Type, Func<object>> BasicInstanceCreators =
            new ConcurrentDictionary<Type, Func<object>>();

        private static readonly ConcurrentDictionary<Type, Func<int, IMarker>> MarkersInstanceCreators =
            new ConcurrentDictionary<Type, Func<int, IMarker>>();

        private readonly IStorableObjectsMetadataProvider _storableObjectsMetadataProvider;

        public ObjectMapper(
            IStorableObjectsMetadataProvider storableObjectsMetadataProvider)
        {
            _storableObjectsMetadataProvider = storableObjectsMetadataProvider;
        }

        public static bool HasColumn(
            ISqlDataReader reader,
            string columnName)
        {
            return reader.GetSchemaTable()
                .Rows
                .Cast<DataRow>()
                .Any(row => row["ColumnName"].Equals(columnName));
        }

        public object MapToObject(
            Type targetType,
            Type selectionType,
            ISqlDataReader reader,
            IEnumerable<ColumnReference> columns)
        {
            if (StorableHelper.IsPocoClassIndicator(targetType))
            {
                if (selectionType == null)
                {
                    throw new InvalidOperationException();
                }

                targetType = selectionType;
            }

            if (typeof(IEntity).IsAssignableFrom(targetType)
                || typeof(IDto).IsAssignableFrom(targetType))
            {
                if (targetType.IsInterface)
                {
                    targetType = StorableHelper.GetMarkerType(targetType);
                    return MapMarker(targetType, reader);
                }

                return MapEntityOrDto(targetType, reader);
            }

            if (targetType.IsInterface && typeof(IMarker).IsAssignableFrom(targetType))
            {
                return StorableHelper.GetMarkerInstance(targetType, (int)reader[0]);
            }

            if (targetType == typeof(string) || (!targetType.IsClass && !targetType.IsInterface))
            {
                return reader[0];
            }

            return MapProjection(targetType, reader);
        }

        private Func<object> CreateBasicInstanceCreator(Type entityType)
        {
            return BasicInstanceCreators.GetOrAdd(
                entityType,
                type =>
                {
                    var instanceCreation = Expression.New(type);
                    var creator = Expression.Lambda<Func<object>>(instanceCreation).Compile();
                    return creator;
                });
        }

        private Func<int, IMarker> CreateMarkerInstanceCreator(Type markerType)
        {
            return MarkersInstanceCreators.GetOrAdd(
                markerType,
                type =>
                {
                    var parameter = Expression.Parameter(typeof(int));
                    var instanceCreation = Expression.New(type.GetConstructors().Single(), parameter);
                    var creator = Expression.Lambda<Func<int, IMarker>>(instanceCreation, parameter).Compile();
                    return creator;
                });
        }

        private object MapEntityOrDto(
            Type targetType,
            ISqlDataReader reader)
        {
            var entityCreator = CreateBasicInstanceCreator(targetType);
            var entity = entityCreator();
            var descriptor = _storableObjectsMetadataProvider.GetDescriptor(targetType);
            descriptor.MapValues(entity, reader);

            var versioned = entity as VersionedEntity;
            if (versioned != null)
            {
                versioned.Modified = false;
            }

            return entity;
        }

        private object MapMarker(
            Type targetType,
            ISqlDataReader reader)
        {
            var markerCreator = CreateMarkerInstanceCreator(targetType);
            var instance = markerCreator((int)reader[0]);

            return instance;
        }

        private object MapProjection(
            Type targetType,
            ISqlDataReader reader,
            string valuePrefix = null)
        {
            var constructor = targetType.GetConstructors().Single();

            var parameters = constructor.GetParameters();
            if (!parameters.Any())
            {
                var entityCreator = CreateBasicInstanceCreator(targetType);
                var instance = entityCreator();
                foreach (var propertyInfo in targetType.GetProperties())
                {
                    var columnName = propertyInfo.Name;
                    if (valuePrefix != null)
                    {
                        columnName = $"{valuePrefix}.{columnName}";
                    }

                    if (propertyInfo.PropertyType.IsClass && !propertyInfo.PropertyType.IsArray && propertyInfo.PropertyType != typeof(string))
                    {
                        var value = MapProjection(propertyInfo.PropertyType, reader, columnName);
                        propertyInfo.SetValue(instance, value);
                    }
                    else if (HasColumn(reader, columnName))
                    {
                        var columnValue = reader[columnName];
                        var value = SetValueToMapProjection(columnValue, propertyInfo.PropertyType);
                        if (value == null && propertyInfo.Name == "Id")
                        {
                            return null;
                        }

                        ////todo: some custom handling for dates and other sql attributes that we have
                        propertyInfo.SetValue(instance, value);
                    }
                }

                return instance;
            }
            else
            {
                var values = parameters
                    .Select(x =>
                    {
                        var columnName = x.Name;
                        if (valuePrefix != null)
                        {
                            columnName = $"{valuePrefix}.{columnName}";
                        }

                        if (x.ParameterType.IsClass && !x.ParameterType.IsArray && x.ParameterType != typeof(string))
                        {
                            var value = MapProjection(x.ParameterType, reader, columnName);
                            return value;
                        }
                        else
                        {
                            var parameter = SetValueToMapProjection(reader[columnName], x.ParameterType);
                            return parameter;
                        }
                    })
                    .ToArray();

                var instance = Activator.CreateInstance(targetType, values);
                return instance;
            }
        }

        private object SetValueToMapProjection(
            object value,
            Type type)
        {
            if (typeof(IMarker).IsAssignableFrom(type))
            {
                value = StorableHelper.GetMarkerInstance(type, (int)value);
            }

            if (value is DBNull)
            {
                value = null;
            }

            if (value is DateTime)
            {
                var date = (DateTime)value;
                value = new DateTime(date.Ticks, DateTimeKind.Utc);
            }

            if (value != null)
            {
                if (type == typeof(DateTimeOffset)
                    || type == typeof(DateTimeOffset?))
                {
                    value = new DateTimeOffset((DateTime)value);
                }
            }

            return value;
        }
    }
}