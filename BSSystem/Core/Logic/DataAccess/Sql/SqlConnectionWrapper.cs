﻿using System;
using System.Data;
using System.Data.SqlClient;
using Qdarc.Modules.Common.QueryGeneration;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    internal sealed class SqlConnectionWrapper : ISqlConnection
    {
        private readonly SqlConnection _connection;
        private readonly bool _isDisposable;
        private SqlTransaction _transaction;

        public SqlConnectionWrapper(
            string connectionString,
            bool isDisposable = true)
        {
            _connection = new SqlConnection(connectionString);
            _isDisposable = isDisposable;
            _connection.Open();
        }

        public bool Trace => false;

        public SqlTransaction BeginTransaction()
        {
            _transaction = _connection.BeginTransaction();
            return _transaction;
        }

        public ISqlCommand CreateSqlCommand(
            string commandText,
            CommandType commandtype = CommandType.Text)
        {
            var command = new SqlCommand(
                commandText,
                _connection,
                _transaction);
            command.CommandType = commandtype;
            return new SqlCommandWrapper(command);
        }

        void IDisposable.Dispose()
        {
            if (_isDisposable)
            {
                Dispose();
            }
        }

        public void Dispose()
        {
            _connection.Close();
            _connection.Dispose();
        }
    }
}