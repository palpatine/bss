﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    public sealed class WhereClause
    {
        public WhereClause()
        {
        }

        public WhereClause(string clause)
        {
            Clause = clause;
            Parameters = new Dictionary<string, object>();
        }

        public WhereClause(
            string clause,
            Dictionary<string, object> parameters)
        {
            Clause = clause;
            Parameters = parameters;
        }

        public string Clause { get; set; }

        public bool IsFalse => Clause == "(FALSE)";

        public Dictionary<string, object> Parameters { get; set; }

        public void BindParameters(ISqlCommand sqlCommand)
        {
            foreach (var item in Parameters.EmptyIfNull())
            {
                sqlCommand.Parameters.AddWithValue(item.Key, item.Value);
            }
        }

        internal WhereClause Merge(WhereClause where)
        {
            if (IsFalse || where.IsFalse)
            {
                return new WhereClause
                {
                    Clause = "(FALSE)"
                };
            }

            return new WhereClause
            {
                Clause = $"({Clause}) AND ({@where.Clause})",
                Parameters =
                    Parameters.EmptyIfNull()
                        .Concat(where.Parameters.EmptyIfNull())
                        .EmptyIfNull()
                        .ToDictionary(x => x.Key, x => x.Value)
            };
        }
    }
}