﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Environment;

namespace SolvesIt.BSSystem.Core.Logic.DataAccess.Sql
{
    internal sealed class DatabaseAccessor : IDatabaseAccessor
    {
        private readonly IDatabaseManager _databaseManager;

        public DatabaseAccessor(IDatabaseManager databaseManager)
        {
            _databaseManager = databaseManager;
        }

        public IDeletionQuery<TStorable> Delete<TStorable>()
            where TStorable : IStorable, new()
        {
            var tableQuery = new TableQuery<TStorable>(GetConnection);
            return tableQuery;
        }

        public ITableQuery<TResult> Function<TResult>(
            string name,
            params ConstantExpression[] arguments)
        {
            var query = new FunctionQuery<TResult>(GetConnection, name, arguments);
            return query;
        }

        public ITableQuery<TResult> Values<TResult>(
            IEnumerable<TResult> values)
        {
            var query = new ValuesQuery<TResult>(GetConnection, values);
            return query;
        }

        public ITableQuery<TStorable> Query<TStorable, TOtherIndicator>(
            Expression<Func<TStorable, TOtherIndicator>> dyscriminator,
            TOtherIndicator other)
            where TStorable : IStorable, new()
            where TOtherIndicator : IStorable
        {
            var tableQuery = new TableQuery<TStorable>(GetConnection);
            return tableQuery;
        }

        public ITableQuery<TStorable> Query<TStorable>()
            where TStorable : IStorable, new()
        {
            var tableQuery = new TableQuery<TStorable>(GetConnection);
            return tableQuery;
        }

        public ITableQuery<TStorable> Query<TStorable>(Type concreteType)
            where TStorable : IStorable
        {
            var type = typeof(TableQuery<>).MakeGenericType(concreteType);
            var query = (ITableQuery<TStorable>)Activator.CreateInstance(type, new object[] { new Func<ISqlConnection>(GetConnection) });
            return query;
        }

        public ITableQuery Query(Type concreteType)
        {
            var type = typeof(TableQuery<>).MakeGenericType(concreteType);
            var query = (ITableQuery)Activator.CreateInstance(type, new object[] { new Func<ISqlConnection>(GetConnection) });
            return query;
        }

        private ISqlConnection GetConnection()
        {
            ISqlConnection connecton;
            if (_databaseManager.Connection != null)
            {
                connecton = _databaseManager.Connection;
            }
            else
            {
                var configurationProvider = ServiceLocator.Current.GetInstance<IConfigurationProvider>();
                connecton = new SqlConnectionWrapper(configurationProvider.ConnectionString);
            }

            return connecton;
        }
    }
}