﻿using System.Globalization;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    /// <summary>
    ///     An interface for file template provider.
    /// </summary>
    public interface ITemplatesManager
    {
        /// <summary>
        ///     Gets the file info.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="culture">The culture.</param>
        /// <param name="fallbackCulture">The fallback culture.</param>
        /// <returns>File template informations.</returns>
        TemplateInfo GetTemplate(
            string templateName,
            CultureInfo culture,
            CultureInfo fallbackCulture);
    }
}