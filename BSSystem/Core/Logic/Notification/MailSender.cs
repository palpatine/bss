﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    internal sealed class MailSender : IMailSender
    {
        private static readonly TaskFactory TaskFactory = new TaskFactory(new ConcurrencyLimitingTaskScheduler(1));

        private readonly IGlobalMessageParametersProvider _attributesProvider;
        private readonly ISmtpConfigurationProvider _smtpConfigurationProvider;

        private readonly ITemplatesManager _templatesManager;

        public MailSender(
            ITemplatesManager templateFileProvider,
            IGlobalMessageParametersProvider attributesProvider,
            ISmtpConfigurationProvider smtpConfigurationProvider)
        {
            _templatesManager = templateFileProvider;
            _attributesProvider = attributesProvider;
            _smtpConfigurationProvider = smtpConfigurationProvider;
        }

        public bool Send(
            string templateName,
            EmailAddressList recipients,
            IDictionary<string, string> additionalAttributes = null)
        {
            var template = _templatesManager.GetTemplate(templateName, null, null);
            if (template == null)
            {
                throw new InvalidOperationException(
                    $"Template was not found {templateName}.");
            }

            TaskFactory.StartNew(
                SendMail,
                new MailMessageDescirptor
                {
                    SmtpConfiguration = _smtpConfigurationProvider,
                    Template = template,
                    Recipients = recipients,
                    Attributes = _attributesProvider,
                    AdditionalAttributes = additionalAttributes,
                });

            return true;
        }

        private void SendMail(object mailInfo)
        {
            var info = (MailMessageDescirptor)mailInfo;
            ////todo: use ISmtpConfigurationProvider instead of default configuraiton
            var smtpClient = new SmtpClient();

            var subject = EmailWritter.WriteSubject(
                info.Template.Subject ?? string.Empty,
                _attributesProvider.GetAttributes(),
                info.AdditionalAttributes);

            var content = EmailWritter.WriteBody(
                info.Template.Content,
                _attributesProvider.GetAttributes(),
                info.AdditionalAttributes);

            var message = new MailMessage(
                _smtpConfigurationProvider.SenderAddress.ToString(),
                info.Recipients.ToString(),
                subject,
                content);
            try
            {
                smtpClient.Send(message);
            }
            catch
            {
            }
        }
    }
}