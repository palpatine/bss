﻿using System.Collections.Generic;
using System.Text;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    public class EmailAddressList
    {
        public EmailAddressList()
        {
        }

        public EmailAddressList(
            EmailAddress address,
            params EmailAddress[] addresses)
            : this()
        {
            Addresses.Add(address);
            Addresses.AddRange(addresses);
        }

        private List<EmailAddress> Addresses { get; } = new List<EmailAddress>();

        public void Add(EmailAddress address)
        {
            Addresses.Add(address);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var emailAddress in Addresses)
            {
                sb.AppendFormat("{0};", emailAddress);
            }

            return sb.ToString(0, sb.Length - 1);
        }
    }
}