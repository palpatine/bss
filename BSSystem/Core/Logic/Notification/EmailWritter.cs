﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    /// <summary>
    ///     An internal class for processing email template with attributes.
    /// </summary>
    internal sealed class EmailWritter
    {
        /// <summary>
        ///     Writes the body.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="attributes">The attributes.</param>
        /// <param name="additionalAttributes">The additional attributes.</param>
        /// <returns>
        ///     Email body read from template file and processed with attributes.
        /// </returns>
        public static string WriteBody(
            string content,
            IDictionary<string, string> attributes,
            IDictionary<string, string> additionalAttributes)
        {
            var processedContent = ProcessTemplate(attributes, additionalAttributes, content);
            return processedContent;
        }

        /// <summary>
        ///     Writes the subject.
        /// </summary>
        /// <param name="subject">The subject.</param>
        /// <param name="attributes">The attributes.</param>
        /// <param name="additionalAttributes">The additional attributes.</param>
        /// <returns>Email subject processed with attributes.</returns>
        public static string WriteSubject(
            string subject,
            IDictionary<string, string> attributes,
            IDictionary<string, string> additionalAttributes)
        {
            subject = ProcessTemplate(attributes, additionalAttributes, subject);
            return subject;
        }

        /// <summary>
        ///     Processes the template.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <param name="additionalAttributes">The additional attributes.</param>
        /// <param name="test">The text.</param>
        /// <returns>Text processed with attributes.</returns>
        private static string ProcessTemplate(
            IDictionary<string, string> attributes,
            IDictionary<string, string> additionalAttributes,
            string test)
        {
            if (attributes != null)
            {
                foreach (var attribute in attributes)
                {
                    test = test.Replace("${" + attribute.Key + "}", attribute.Value);
                }
            }

            if (additionalAttributes != null)
            {
                foreach (var attribute in additionalAttributes)
                {
                    test = test.Replace("${" + attribute.Key + "}", attribute.Value);
                }
            }

            return test;
        }
    }
}