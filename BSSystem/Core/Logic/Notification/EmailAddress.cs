﻿namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    /// <summary>
    ///     Class for storing email address.
    /// </summary>
    public class EmailAddress
    {
        /// <summary>
        ///     Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        ///     Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="string" /> that represents this instance.</returns>
        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(Name)
                ? Address
                : $"\"{Name}\" <{Address}>";
        }
    }
}