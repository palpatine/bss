﻿namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    /// <summary>
    ///     Class for storing file template informations.
    /// </summary>
    public sealed class TemplateInfo
    {
        /// <summary>
        ///     Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        public string Content { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is HTML formatted.
        /// </summary>
        /// <value><c>true</c> if this instance is HTML formatted; otherwise, <c>false</c>.</value>
        public bool IsHtmlFormatted { get; set; }

        /// <summary>
        ///     Gets or sets the subject.
        /// </summary>
        /// <value>The subject.</value>
        public string Subject { get; set; }

        /// <summary>
        ///     Gets or sets the name of the template.
        /// </summary>
        /// <value>The name of the template.</value>
        public string TemplateName { get; set; }
    }
}