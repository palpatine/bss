﻿namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    /// <summary>
    ///     An interface for email SMTP configuration provider.
    /// </summary>
    public interface ISmtpConfigurationProvider
    {
        /// <summary>
        ///     Gets value of host address.
        /// </summary>
        /// <value>Host address.</value>
        string Host { get; }

        /// <summary>
        ///     Gets value of email account password.
        /// </summary>
        /// <value>Email account password.</value>
        string Password { get; }

        /// <summary>
        ///     Gets value of port number.
        /// </summary>
        /// <value>Port number.</value>
        string Port { get; }

        /// <summary>
        ///     Gets the sender address.
        /// </summary>
        /// <value>The sender address.</value>
        EmailAddress SenderAddress { get; }

        /// <summary>
        ///     Gets value of email account name.
        /// </summary>
        /// <value>Email account name.</value>
        string Username { get; }

        /// <summary>
        ///     Gets a value indicating whether [use SSL].
        /// </summary>
        /// <value><c>true</c> if [use SSL]; otherwise, <c>false</c>.</value>
        bool UseSsl { get; }
    }
}