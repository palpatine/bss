﻿using System.Net.Configuration;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    ////todo: try use preffered administrator settings stored in application configuration table
    internal sealed class SmtpConfigurationProvider : ISmtpConfigurationProvider
    {
        private MailSettingsSectionGroup _mailSettings;

        public SmtpConfigurationProvider(MailSettingsSectionGroup mailSettingsSectionGroup)
        {
            _mailSettings = mailSettingsSectionGroup;
        }

        public string Host => _mailSettings.Smtp.Network.Host;

        public string Password => _mailSettings.Smtp.Network.Password;

        public string Port => _mailSettings.Smtp.Network.Port.ToString();

        public EmailAddress SenderAddress => new EmailAddress { Address = _mailSettings.Smtp.From, Name = "BSS System" };

        public string Username => _mailSettings.Smtp.Network.UserName;

        public bool UseSsl => _mailSettings.Smtp.Network.EnableSsl;
    }
}