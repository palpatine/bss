﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    public interface IMailSender
    {
        bool Send(
            string templateName,
            EmailAddressList recipients,
            IDictionary<string, string> additionalAttributes = null);
    }
}