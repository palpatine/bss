﻿using System;
using System.Globalization;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    internal sealed class TemplatesManager : ITemplatesManager
    {
        public TemplateInfo GetTemplate(
            string templateName,
            CultureInfo culture,
            CultureInfo fallbackCulture)
        {
            var key = templateName + "_" + culture;

            ////todo: read from database by key form templates table

            if (templateName == SystemNotificationTemplates.NewUserRegistration.TempalateName)
            {
                return new TemplateInfo
                {
                    Content = @"Witaj ${FirstName} ${LastName}

Zostało utworzone konto w systemie BSSytem. Towje dane do logowania podane są poniżej.

    Login: ${Login}
    Hasło: ${Password}

Wiadomość została wygenerowana automatycznie prosimy na nią nie odpowiadać.

---------------------------

Hello ${FirstName} ${LastName}

An account in BSSystem was created for you. Your data required to login are as follows.

    Login: ${Login}
    Password: ${Password}

This message was autognerated pleas do not respond to it.",
                    IsHtmlFormatted = false,
                    Subject = "Nowe konto w BSSystem",
                    TemplateName = SystemNotificationTemplates.NewUserRegistration.TempalateName
                };
            }

            if (templateName == SystemNotificationTemplates.PasswordReset.TempalateName)
            {
                return new TemplateInfo
                {
                    Content = @"Witaj ${FirstName} ${LastName}

Twoje hasło do system BSSytem zostało zresetowane. Towje nowe dane do logowania podane są poniżej.

    Hasło: ${Password}

Wiadomość została wygenerowana automatycznie prosimy na nią nie odpowiadać.

---------------------------

Hello ${FirstName} ${LastName}

Your password to BSSystem was reseted. Your new data required to login are as follows.

    Password: ${Password}

This message was autognerated pleas do not respond to it.",
                    IsHtmlFormatted = false,
                    Subject = "Hasło do BSSystem zostało zresetowane",
                    TemplateName = SystemNotificationTemplates.PasswordReset.TempalateName
                };
            }

            throw new InvalidOperationException();
        }
    }
}