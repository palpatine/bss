﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    /// <summary>
    ///     Struct for status object to async mail send call.
    /// </summary>
    internal struct MailMessageDescirptor
    {
        /// <summary>
        ///     Gets or sets the additional attributes.
        /// </summary>
        /// <value>The additional attributes.</value>
        public IDictionary<string, string> AdditionalAttributes { get; set; }

        /// <summary>
        ///     Gets or sets the attributes.
        /// </summary>
        /// <value>The attributes.</value>
        public IGlobalMessageParametersProvider Attributes { get; set; }

        public EmailAddressList Recipients { get; set; }

        /// <summary>
        ///     Gets or sets the SMTP configuration.
        /// </summary>
        /// <value>The SMTP configuration.</value>
        public ISmtpConfigurationProvider SmtpConfiguration { get; set; }

        /// <summary>
        ///     Gets or sets the file info.
        /// </summary>
        /// <value>The file info.</value>
        public TemplateInfo Template { get; set; }
    }
}