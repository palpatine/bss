﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    /// <summary>
    ///     An interface for file templates attributes provider.
    /// </summary>
    public interface IGlobalMessageParametersProvider
    {
        /// <summary>
        ///     Gets the attributes.
        /// </summary>
        /// <returns>Gets the attributes.</returns>
        IDictionary<string, string> GetAttributes();
    }
}