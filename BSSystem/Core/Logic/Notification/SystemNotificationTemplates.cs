﻿using System.Collections.Generic;

namespace SolvesIt.BSSystem.Core.Logic.Notification
{
    public static class SystemNotificationTemplates
    {
        public sealed class NewUserRegistration
        {
            public const string TempalateName = "NewUserRegistration";
            private const string FirstName = "FirstName";
            private const string LastName = "LastName";
            private const string Login = "Login";
            private const string Password = "Password";

            public NewUserRegistration(
                string firstName,
                string lastName,
                string login,
                string password)
            {
                Attributes = new Dictionary<string, string>
                {
                    { FirstName, firstName },
                    { LastName, lastName },
                    { Login, login },
                    { Password, password },
                };
            }

            public IDictionary<string, string> Attributes { get; private set; }
        }

        public sealed class PasswordReset
        {
            public const string TempalateName = "PasswordReset";

            private const string FirstName = "FirstName";
            private const string LastName = "LastName";
            private const string Login = "Login";
            private const string Password = "Password";

            public PasswordReset(
                string firstName,
                string lastName,
                string login,
                string password)
            {
                Attributes = new Dictionary<string, string>
                {
                    { FirstName, firstName },
                    { LastName, lastName },
                    { Login, login },
                    { Password, password },
                };
            }

            public IDictionary<string, string> Attributes { get; private set; }
        }
    }
}