﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.Core.Logic.Validation;

namespace SolvesIt.BSSystem.Core.Logic
{
    internal sealed class AssignmentService : IAssignmentService
    {
        private readonly IAssignmentsRepository _assignmentsRepository;
        private readonly IServiceLocator _serviceLocator;

        public AssignmentService(
            IAssignmentsRepository assignmentsRepository,
            IServiceLocator serviceLocator)
        {
            _assignmentsRepository = assignmentsRepository;
            _serviceLocator = serviceLocator;
        }

        public async Task<IEnumerable<ValidationError>> UpdateAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<JunctionUdt<TSlave>> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
        {
            var validationErrors = await ValidateAsync<TEntityIndicator, TSlave, JunctionUdt<TSlave>>(entity, assignments);
            if (validationErrors.Any())
            {
                return validationErrors;
            }

            var newListValue = StorableHelper.ConvertToDataRecords(assignments);
            var newListType = typeof(JunctionUdt<>).GetCustomAttribute<SqlNameAttribute>().Name;
            var results = await _assignmentsRepository.UpdateEntityAssignmentAsync<TEntityIndicator, TSlave>(entity, newListValue, newListType);
            return ProcessAssignmentsValidationErrors(results);
        }

        public async Task<IEnumerable<ValidationError>> UpdateAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<AssignmentUdt<TSlave>> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
        {
            var validationErrors = await ValidateAsync<TEntityIndicator, TSlave, AssignmentUdt<TSlave>>(entity, assignments);
            if (validationErrors.Any())
            {
                return validationErrors;
            }

            var newListValue = StorableHelper.ConvertToDataRecords(assignments);
            var newListType = typeof(AssignmentUdt<>).GetCustomAttribute<SqlNameAttribute>().Name;
            var results = await _assignmentsRepository.UpdateEntityAssignmentAsync<TEntityIndicator, TSlave>(entity, newListValue, newListType);
            return ProcessAssignmentsValidationErrors(results);
            ////todo: await LockRepository.RemoveLockAsync(operationToken);
        }

        public async Task<IEnumerable<ValidationError>> UpdateAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<AssignmentJobRoleUdt<TSlave>> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
        {
            var validationErrors = await ValidateAsync<TEntityIndicator, TSlave, AssignmentJobRoleUdt<TSlave>>(entity, assignments);
            if (validationErrors.Any())
            {
                return validationErrors;
            }

            var newListValue = StorableHelper.ConvertToDataRecords(assignments);
            var newListType = typeof(AssignmentJobRoleUdt<>).GetCustomAttribute<SqlNameAttribute>().Name;
            var results = await _assignmentsRepository.UpdateEntityAssignmentAsync<TEntityIndicator, TSlave>(entity, newListValue, newListType);
            return ProcessAssignmentsValidationErrors(results);
            ////todo:   await LockRepository.RemoveLockAsync(operationToken);
        }

        public async Task<IEnumerable<ValidationError>> UpdateAssignmentAsync<TEntityIndicator, TSlave>(
            TEntityIndicator entity,
            IEnumerable<AssignmentRoleUdt<TSlave>> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
        {
            var validationErrors = await ValidateAsync<TEntityIndicator, TSlave, AssignmentRoleUdt<TSlave>>(entity, assignments);
            if (validationErrors.Any())
            {
                return validationErrors;
            }

            var newListValue = StorableHelper.ConvertToDataRecords(assignments);
            var newListType = typeof(AssignmentRoleUdt<>).GetCustomAttribute<SqlNameAttribute>().Name;
            var results = await _assignmentsRepository.UpdateEntityAssignmentAsync<TEntityIndicator, TSlave>(entity, newListValue, newListType);
            return ProcessAssignmentsValidationErrors(results);
            ////todo:   await LockRepository.RemoveLockAsync(operationToken);
        }

        private static ValidationError[] ProcessAssignmentsValidationErrors(IEnumerable<ValidationResultUdt> results)
        {
            return results.Select(x =>
            {
                string reference = null;
                if (x.Id != null)
                {
                    reference = $"assignments[{x.Id}].{x.Field}";
                }

                var message = ValidationErrors.ResourceManager.GetString(x.MessageCode);
                if (message != null)
                {
                    message = string.Format(
                        message,
                        x.AdditionalValue);
                }
                else
                {
                    message = $"[{x.MessageCode}]";
                }

                return new ValidationError(
                     message,
                    x.MessageCode,
                    reference,
                    x.AdditionalValue);
            }).ToArray();
        }

        private async Task<IEnumerable<ValidationError>> ValidateAsync<TEntityIndicator, TSlave, TJunctionModel>(
                                            TEntityIndicator entity,
            IEnumerable<TJunctionModel> assignments)
            where TEntityIndicator : IStorable
            where TSlave : IMarker
            where TJunctionModel : IUdt
        {
            var firstEntityType = StorableHelper.GetEntityType(typeof(TEntityIndicator));
            var secondEntityType = StorableHelper.GetEntityType(typeof(TSlave));
            Type junctionType;
            if (!StorableHelper.TryGetJunctionTable(firstEntityType, secondEntityType, out junctionType))
            {
                throw new InvalidOperationException();
            }

            var validatorType = typeof(IJunctionEntityValidator<>).MakeGenericType(junctionType);
            var validators = (IEnumerable<IJunctionEntityValidator>)_serviceLocator.GetAllInstances(validatorType);
            var validationErrors = await validators.SelectManyAsync(x => x.ValidateModelAsync(typeof(TJunctionModel), entity, assignments.Cast<object>()));
            return validationErrors;
        }
    }
}