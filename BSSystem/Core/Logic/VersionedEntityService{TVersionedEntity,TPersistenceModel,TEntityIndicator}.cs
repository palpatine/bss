﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Communication;
using Qdarc.Modules.Common.QueryGeneration;
using Qdarc.Modules.Common.Utils;
using SolvesIt.BSSystem.CommonModule.Abstraction;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Repositories;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Logic;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;

namespace SolvesIt.BSSystem.Core.Logic
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")]
    public abstract class VersionedEntityService<TVersionedEntity, TPersistenceModel, TEntityIndicator>
        : IVersionedEntityService<TVersionedEntity, TPersistenceModel, TEntityIndicator>
        where TVersionedEntity : VersionedEntity, TEntityIndicator, new()
        where TPersistenceModel : TEntityIndicator
        where TEntityIndicator : IStorable
    {
        private readonly IEnumerable<IEntityDeletionValidator<TEntityIndicator>> _deletionValidators;

        private readonly IEventAggregator _eventAggregator;

        private readonly IEnumerable<IServiceExtension<TPersistenceModel>> _extensions;

        private readonly IEnumerable<IEntityValidator<TPersistenceModel>> _validators;

        protected VersionedEntityService(
                    IEventAggregator eventAggregator,
                    ILockRepository lockRepository,
                    IEnumerable<IServiceExtension<TPersistenceModel>> extensions,
                    IVersionedEntityRepository<TVersionedEntity, TEntityIndicator> repository,
                    IEnumerable<IEntityValidator<TPersistenceModel>> validators,
                    IDatabaseManager databaseManager,
                    IDatabaseAccessor databaseAccessor,
                    IEnumerable<IEntityDeletionValidator<TEntityIndicator>> deletionValidators)
        {
            _eventAggregator = eventAggregator;
            LockRepository = lockRepository;
            _extensions = extensions;
            Repository = repository;
            _validators = validators;
            DatabaseManager = databaseManager;
            DatabaseAccessor = databaseAccessor;
            _deletionValidators = deletionValidators;
        }

        protected IDatabaseAccessor DatabaseAccessor { get; }

        protected IDatabaseManager DatabaseManager { get; }

        protected ILockRepository LockRepository { get; }

        protected IVersionedEntityRepository<TVersionedEntity, TEntityIndicator> Repository { get; }

        public async Task<IEnumerable<ValidationError>> DeleteAsync(
            TEntityIndicator entity,
            string operationToken)
        {
            var errors = await ValidateDeletionAsync(entity);
            if (!errors.Any())
            {
                return await DeleteAsync(x => x.IsEquivalent(entity), operationToken);
            }

            return errors;
        }

        public async Task<IEnumerable<ValidationError>> DeleteAsync(
            Expression<Func<TVersionedEntity, bool>> where,
            string operationToken)
        {
            var deletedItemsIds = await DatabaseAccessor.Query<TVersionedEntity>()
                .Where(x => !x.IsDeleted)
                .Where(where)
                .SelectAsync(x => x.Id);

            var deletedItems =
                deletedItemsIds.Select(StorableHelper.GetMarkerInstance<TVersionedEntity, TEntityIndicator>).ToArray();

            await _eventAggregator.PublishAsync(new EntityTryLockEvent<TEntityIndicator>
            {
                EntityList = deletedItems,
                OperationToken = operationToken
            });
            try
            {
                using (DatabaseManager.OpenTransaction())
                {
                    await _eventAggregator.PublishAsync(new EntityDeletedEvent<TEntityIndicator>
                    {
                        EntityList = deletedItems
                    });
                    await Repository.DeleteAsync(where);
                    await LockRepository.RemoveLockAsync(operationToken);

                    DatabaseManager.ComitTransaction();
                }
            }
            catch (SqlValidationException ex)
            {
                return new[] { new ValidationError(ex.ErrorCode, ex.ErrorCode) };
            }

            return Enumerable.Empty<ValidationError>();
        }

        public async Task RemoveLockAsync(string operationToken)
        {
            await LockRepository.RemoveLockAsync(operationToken);
        }

        public async Task<IEnumerable<ValidationError>> SaveAsync(TPersistenceModel model)
        {
            var errors = await ValidateAsync(model);

            if (!errors.Any())
            {
                try
                {
                    await OnReadyToSaveAsync(model);

                    if (_extensions != null)
                    {
                        foreach (var serviceExtension in _extensions)
                        {
                            await serviceExtension.OnReadyToSaveAsync(model);
                        }
                    }
                }
                catch (SqlValidationException ex)
                {
                    return new[] { new ValidationError(ex.ErrorCode, ex.ErrorCode) };
                }
            }

            return errors;
        }

        public virtual async Task<IEnumerable<ValidationError>> TryLockAsync(
            TEntityIndicator marker,
            string operationToken)
        {
            return await LockRepository.TryLockAsync(marker, operationToken);
        }

        public virtual async Task<IEnumerable<ValidationError>> TryLockAsync(
            Type junctionTable,
            PropertyInfo relationProperty,
            IMarker referenceValue,
            string operationToken)
        {
            return await LockRepository.TryLockAsync(junctionTable, relationProperty, referenceValue, operationToken);
        }

        protected abstract Task OnReadyToSaveAsync(TPersistenceModel model);

        protected async Task<IEnumerable<ValidationError>> ValidateAsync(TPersistenceModel model)
        {
            var validationErrors = await _validators.SelectManyAsync(x => x.ValidateModelAsync(model));
            IEnumerable<ValidationError> extensionsValidationErrors = null;

            if (_extensions != null)
            {
                extensionsValidationErrors = await _extensions.SelectManyAsync(x => x.ValidateAsync(model));
            }

            return validationErrors.Concat(extensionsValidationErrors.EmptyIfNull()).ToArray();
        }

        protected async Task<IEnumerable<ValidationError>> ValidateDeletionAsync(TEntityIndicator entity)
        {
            var validationErrors = await _deletionValidators.SelectManyAsync(x => x.ValidateDeletionAsync(entity));

            return validationErrors.ToArray();
        }
    }
}