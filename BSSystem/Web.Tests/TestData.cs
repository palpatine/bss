using System;

namespace SolvesIt.BSSystem.Web.Tests
{
    internal class TestData
    {
        public bool BooleanProperty { get; set; }

        public DateTime DateProperty { get; set; }

        public decimal DecimalProperty { get; set; }

        public double DoubleProperty { get; set; }

        public float FloatProperty { get; set; }

        public int IntProperty { get; set; }

        public string StringProperty { get; set; }
    }
}