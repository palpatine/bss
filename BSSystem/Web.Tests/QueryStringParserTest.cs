﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Tests.Common;
using SolvesIt.BSSystem.Web.Environment.QueryStringParsing;

namespace SolvesIt.BSSystem.Web.Tests
{
    [TestClass]
    public class QueryStringParserTest
    {
        [TestMethod]
        public void FilterByCombiningComparionsAnd()
        {
            var data = new[]
            {
                new TestData { IntProperty = 3, StringProperty = "C" },
                new TestData { IntProperty = 1, StringProperty = "A" },
                new TestData { IntProperty = 2, StringProperty = "B" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=IntProperty eq 1 and StringProperty eq 'A'");
            queryable.Select(x => x.IntProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1 });
        }

        [TestMethod]
        public void FilterByCombiningComparionsNegation()
        {
            var data = new[]
            {
                new TestData { IntProperty = 3, StringProperty = "C", DoubleProperty = 2.1 },
                new TestData { IntProperty = 1, StringProperty = "A", DoubleProperty = 2.1 },
                new TestData { IntProperty = 2, StringProperty = "B", DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(
                data.AsQueryable(),
                "$filter=!(IntProperty eq 1 or StringProperty eq 'B') and DoubleProperty eq 2.1");
            queryable.Select(x => x.IntProperty).AllCorrespondingElementsShouldBeEqual(new[] { 3 });
        }

        [TestMethod]
        public void FilterByCombiningComparionsOr()
        {
            var data = new[]
            {
                new TestData { IntProperty = 3, StringProperty = "C" },
                new TestData { IntProperty = 1, StringProperty = "A" },
                new TestData { IntProperty = 2, StringProperty = "B" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=IntProperty eq 1 or StringProperty eq 'B'");
            queryable.Select(x => x.IntProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1, 2 });
        }

        [TestMethod]
        public void FilterByCombiningComparionsParenties()
        {
            var data = new[]
            {
                new TestData { IntProperty = 3, StringProperty = "C", DoubleProperty = 2.1 },
                new TestData { IntProperty = 1, StringProperty = "A", DoubleProperty = 2.1 },
                new TestData { IntProperty = 2, StringProperty = "B", DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(
                data.AsQueryable(),
                "$filter=(IntProperty eq 1 or StringProperty eq 'B') and DoubleProperty eq 2.1");
            queryable.Select(x => x.IntProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1, 2 });
        }

        [TestMethod]
        public void FilterByDateProperty()
        {
            var data = new[]
            {
                new TestData { DateProperty = new DateTime(2014, 5, 1) },
                new TestData { DateProperty = new DateTime(2016, 5, 1) },
                new TestData { DateProperty = new DateTime(2015, 5, 1) }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=dateProperty eq 2016-05-01");
            queryable.Select(x => x.DateProperty).AllCorrespondingElementsShouldBeEqual(new[] { new DateTime(2016, 5, 1) });
        }

        [TestMethod]
        public void FilterByDecimalProperty()
        {
            var data = new[]
            {
                new TestData { DecimalProperty = 3.1M },
                new TestData { DecimalProperty = 1.1M },
                new TestData { DecimalProperty = 2.1M }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DecimalProperty eq 1.1");
            queryable.Select(x => x.DecimalProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1.1M });
        }

        [TestMethod]
        public void FilterByDoubleProperty()
        {
            var data = new[]
            {
                new TestData { DoubleProperty = 3.1 },
                new TestData { DoubleProperty = 1.1 },
                new TestData { DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DoubleProperty eq 1.1");
            queryable.Select(x => x.DoubleProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1.1 });
        }

        [TestMethod]
        public void FilterByDoublePropertyGreaterThen()
        {
            var data = new[]
            {
                new TestData { DoubleProperty = 3.1 },
                new TestData { DoubleProperty = 1.1 },
                new TestData { DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DoubleProperty gt 2.1");
            queryable.Select(x => x.DoubleProperty).AllCorrespondingElementsShouldBeEqual(new[] { 3.1 });
        }

        [TestMethod]
        public void FilterByDoublePropertyGreaterThenOrEqual()
        {
            var data = new[]
            {
                new TestData { DoubleProperty = 3.1 },
                new TestData { DoubleProperty = 1.1 },
                new TestData { DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DoubleProperty ge 2.1");
            queryable.Select(x => x.DoubleProperty).AllCorrespondingElementsShouldBeEqual(new[] { 3.1, 2.1 });
        }

        [TestMethod]
        public void FilterByDoublePropertyLowerThen()
        {
            var data = new[]
            {
                new TestData { DoubleProperty = 3.1 },
                new TestData { DoubleProperty = 1.1 },
                new TestData { DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DoubleProperty lt 2.1");
            queryable.Select(x => x.DoubleProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1.1 });
        }

        [TestMethod]
        public void FilterByDoublePropertyLowerThenOrEqual()
        {
            var data = new[]
            {
                new TestData { DoubleProperty = 3.1 },
                new TestData { DoubleProperty = 1.1 },
                new TestData { DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DoubleProperty le 2.1");
            queryable.Select(x => x.DoubleProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1.1, 2.1 });
        }

        [TestMethod]
        public void FilterByDoublePropertyNotEqual()
        {
            var data = new[]
            {
                new TestData { DoubleProperty = 3.1 },
                new TestData { DoubleProperty = 1.1 },
                new TestData { DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DoubleProperty ne 1.1");
            queryable.Select(x => x.DoubleProperty).AllCorrespondingElementsShouldBeEqual(new[] { 3.1, 2.1 });
        }

        [TestMethod]
        public void FilterByFalseConstant()
        {
            var data = new[]
            {
                new TestData { BooleanProperty = true },
                new TestData { BooleanProperty = false },
                new TestData { BooleanProperty = true }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=BooleanProperty eq false");
            queryable.Select(x => x.BooleanProperty).AllCorrespondingElementsShouldBeEqual(new[] { false });
        }

        [TestMethod]
        public void FilterByFloatProperty()
        {
            var data = new[]
            {
                new TestData { FloatProperty = 3.1f },
                new TestData { FloatProperty = 1.1f },
                new TestData { FloatProperty = 2.1f }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=FloatProperty eq 1.1");
            queryable.Select(x => x.FloatProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1.1f });
        }

        [TestMethod]
        public void FilterByIntProperty()
        {
            var data = new[]
            {
                new TestData { IntProperty = 3 },
                new TestData { IntProperty = 1 },
                new TestData { IntProperty = 2 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=IntProperty eq 1");
            queryable.Select(x => x.IntProperty).AllCorrespondingElementsShouldBeEqual(new[] { 1 });
        }

        [TestMethod]
        public void FilterByIsNullMethodCall()
        {
            var data = new[]
            {
                new TestData { StringProperty = "BBB" },
                new TestData { StringProperty = null },
                new TestData { StringProperty = "CCC" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter = IsNull(StringProperty, 'A') eq 'A'");
            queryable.Select(x => x.StringProperty).AllCorrespondingElementsShouldBeEqual(new string[] { null });
        }

        [TestMethod]
        public void FilterByMaxDateConstant()
        {
            var data = new[]
            {
                new TestData { DateProperty = new DateTime(2014, 5, 1) },
                new TestData { DateProperty = new DateTime(2079, 06, 06, 0, 0, 0, 0) },
                new TestData { DateProperty = new DateTime(2015, 5, 1) }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DateProperty eq maxDate");
            queryable.Select(x => x.DateProperty).AllCorrespondingElementsShouldBeEqual(new[] { new DateTime(2079, 6, 6, 0, 0, 0, 0) });
        }

        [TestMethod]
        public void FilterByMinDateConstant()
        {
            var data = new[]
            {
                new TestData { DateProperty = new DateTime(2014, 5, 1) },
                new TestData { DateProperty = new DateTime(1900, 1, 1) },
                new TestData { DateProperty = new DateTime(2015, 5, 1) }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DateProperty eq minDate");
            queryable.Select(x => x.DateProperty).AllCorrespondingElementsShouldBeEqual(new[] { new DateTime(1900, 1, 1) });
        }

        [TestMethod]
        public void FilterByNowConstant()
        {
            var current = DateTime.UtcNow;
            var data = new[]
            {
                new TestData { DateProperty = new DateTime(2014, 5, 1) },
                new TestData { DateProperty = current.AddDays(1) },
                new TestData { DateProperty = current.AddDays(5) }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=DateProperty lt now");
            queryable.Select(x => x.DateProperty).AllCorrespondingElementsShouldBeEqual(new[] { new DateTime(2014, 5, 1) });
        }

        [TestMethod]
        public void FilterByNullConstant()
        {
            var data = new[]
            {
                new TestData { StringProperty = "A" },
                new TestData { StringProperty = null },
                new TestData { StringProperty = "A" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=StringProperty eq null");
            queryable.Select(x => x.StringProperty).AllCorrespondingElementsShouldBeEqual(new string[] { null });
        }

        [TestMethod]
        public void FilterByStartsWithMethodCall()
        {
            var data = new[]
            {
                new TestData { StringProperty = "BBB" },
                new TestData { StringProperty = "AAA" },
                new TestData { StringProperty = "CCC" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter = StartsWith(StringProperty, 'A')");
            queryable.Select(x => x.StringProperty).AllCorrespondingElementsShouldBeEqual(new[] { "AAA" });
        }

        [TestMethod]
        public void FilterByStringProperty()
        {
            var data = new[]
            {
                new TestData { StringProperty = "C" },
                new TestData { StringProperty = "A" },
                new TestData { StringProperty = "B" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=StringProperty eq 'C'");
            queryable.Select(x => x.StringProperty).AllCorrespondingElementsShouldBeEqual(new[] { "C" });
        }

        [TestMethod]
        public void FilterByTrueConstant()
        {
            var data = new[]
            {
                new TestData { BooleanProperty = false },
                new TestData { BooleanProperty = true },
                new TestData { BooleanProperty = false }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=BooleanProperty eq true");
            queryable.Select(x => x.BooleanProperty).AllCorrespondingElementsShouldBeEqual(new[] { true });
        }

        [TestMethod]
        public void OrderByMultiplePropertiesDefaultSort()
        {
            var data = new[]
            {
                new TestData { StringProperty = "C", IntProperty = 3 },
                new TestData { StringProperty = "C", IntProperty = 1 },
                new TestData { StringProperty = "C", IntProperty = 2 },
                new TestData { StringProperty = "A", IntProperty = 3 },
                new TestData { StringProperty = "A", IntProperty = 1 },
                new TestData { StringProperty = "A", IntProperty = 2 },
                new TestData { StringProperty = "B", IntProperty = 3 },
                new TestData { StringProperty = "B", IntProperty = 1 },
                new TestData { StringProperty = "B", IntProperty = 2 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$orderBy=StringProperty,IntProperty");
            queryable.Select(x => x.StringProperty + x.IntProperty).AllCorrespondingElementsShouldBeEqual(new[] { "A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3" });
        }

        [TestMethod]
        public void OrderByMultiplePropertiesMixedSort()
        {
            var data = new[]
            {
                new TestData { StringProperty = "C", IntProperty = 3 },
                new TestData { StringProperty = "C", IntProperty = 1 },
                new TestData { StringProperty = "C", IntProperty = 2 },
                new TestData { StringProperty = "A", IntProperty = 3 },
                new TestData { StringProperty = "A", IntProperty = 1 },
                new TestData { StringProperty = "A", IntProperty = 2 },
                new TestData { StringProperty = "B", IntProperty = 3 },
                new TestData { StringProperty = "B", IntProperty = 1 },
                new TestData { StringProperty = "B", IntProperty = 2 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$orderBy=StringProperty,IntProperty desc");
            queryable.Select(x => x.StringProperty + x.IntProperty).AllCorrespondingElementsShouldBeEqual(new[] { "A3", "A2", "A1", "B3", "B2", "B1", "C3", "C2", "C1" });
        }

        [TestMethod]
        public void OrderByPropertyDefaultSort()
        {
            var data = new[]
            {
                new TestData { StringProperty = "C" },
                new TestData { StringProperty = "A" },
                new TestData { StringProperty = "B" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$orderBy=StringProperty");
            queryable.Select(x => x.StringProperty).AllCorrespondingElementsShouldBeEqual(new[] { "A", "B", "C" });
        }

        [TestMethod]
        public void OrderByPropertyDefaultSortAsc()
        {
            var data = new[]
            {
                new TestData { StringProperty = "C" },
                new TestData { StringProperty = "A" },
                new TestData { StringProperty = "B" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$orderBy=StringProperty asc");
            queryable.Select(x => x.StringProperty).AllCorrespondingElementsShouldBeEqual(new[] { "A", "B", "C" });
        }

        [TestMethod]
        public void OrderByPropertyDefaultSortDesc()
        {
            var data = new[]
            {
                new TestData { StringProperty = "C" },
                new TestData { StringProperty = "A" },
                new TestData { StringProperty = "B" }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$orderBy=StringProperty desc");
            queryable.Select(x => x.StringProperty).AllCorrespondingElementsShouldBeEqual(new[] { "C", "B", "A" });
        }

        [TestMethod]
        public void ParseInvalidQuery()
        {
            var data = new[]
            {
                new TestData { DateProperty = new DateTime(2014, 5, 1) },
                new TestData { DateProperty = new DateTime(2016, 5, 1) },
                new TestData { DateProperty = new DateTime(2015, 5, 1) }
            };
            QueryMetadata queryMetadata;
            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(data.AsQueryable(), "$filter=dateProperty", out queryMetadata);
            queryable.ShouldBeNull();
            queryMetadata.IsValid.ShouldBeFalse("Server acceped payload but validation error was expected");
        }

        [TestMethod]
        public void Projection()
        {
            var data = new[]
            {
                new TestData { IntProperty = 3, StringProperty = "C", DoubleProperty = 2.1 },
                new TestData { IntProperty = 1, StringProperty = "A", DoubleProperty = 2.1 },
                new TestData { IntProperty = 2, StringProperty = "B", DoubleProperty = 2.1 }
            };

            var queryable = new QueryStringParser().Apply(
                data.AsQueryable(),
                "$select=IntProperty,StringProperty");
            var type = queryable.ElementType;
            var properties = type.GetProperties().OrderBy(x => x.Name);
            properties.Select(x => x.Name).AllCorrespondingElementsShouldBeEqual(new[] { "IntProperty", "StringProperty" });
            var values = queryable.Cast<object>().ToArray();
            values.Count().ShouldBeEqual(3);
            ((int)type.GetProperty("IntProperty").GetValue(values.First())).ShouldBeEqual(3);
        }

        [TestMethod]
        public void Skip()
        {
            var data = new[]
            {
                new TestData { IntProperty = 3, StringProperty = "C", DoubleProperty = 2.1 },
                new TestData { IntProperty = 1, StringProperty = "A", DoubleProperty = 2.1 },
                new TestData { IntProperty = 2, StringProperty = "B", DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(
                data.AsQueryable(),
                "$skip=2");

            queryable.Count().ShouldBeEqual(1);
            queryable.Single().IntProperty.ShouldBeEqual(2);
        }

        [TestMethod]
        public void Top()
        {
            var data = new[]
            {
                new TestData { IntProperty = 3, StringProperty = "C", DoubleProperty = 2.1 },
                new TestData { IntProperty = 1, StringProperty = "A", DoubleProperty = 2.1 },
                new TestData { IntProperty = 2, StringProperty = "B", DoubleProperty = 2.1 }
            };

            var queryable = (IQueryable<TestData>)new QueryStringParser().Apply(
                data.AsQueryable(),
                "$top=1");

            queryable.Count().ShouldBeEqual(1);
            queryable.Single().IntProperty.ShouldBeEqual(3);
        }
    }
}