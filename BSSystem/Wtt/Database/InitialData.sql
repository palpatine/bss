﻿DECLARE @MinDate SMALLDATETIME = '1900-01-01';
INSERT [Common].[Module](
    [Key]
  , [MajorVersion]
  , [DevVersion]
  , [Required])
VALUES('Wtt', 1, 0, 'Common');
DECLARE @ModuleId INT = SCOPE_IDENTITY();

INSERT [Common].[Module_Organization](
    [OrganizationId]
  , [ModuleId]
  , [DateStart]
  , [ChangerId])
VALUES(1, @ModuleId, @MinDate, 1);

BEGIN--PERMISSIONS
    DECLARE  @IdOffset INT
    SET IDENTITY_INSERT [Common].[SystemPermission] ON

    BEGIN --Topic
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Wtt_Topic',	NULL,		'Wtt_Topic',								 0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',		@IdOffset+1,'Wtt_Topic|Read',							 1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',		@IdOffset+1,'Wtt_Topic|Details',						 1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',		@IdOffset+3,'Wtt_Topic|Details|Write',					 1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Assignment',	@IdOffset+3,'Wtt_Topic|Details|Assignment',				 0, 1, 1)
          , (@IdOffset+6, @ModuleId, 'Common_User',	@IdOffset+5,'Wtt_Topic|Details|Assignment|Common_User',	 1, 0, 1)
          , (@IdOffset+7, @ModuleId, 'Wtt_Activity',@IdOffset+5,'Wtt_Topic|Details|Assignment|Wtt_Activity', 1, 0, 1)

        INSERT [Common].[SystemPermissionContext]
            ([SystemPermissionId], [TableKey])
        VALUES
             (@IdOffset+1, 'Wtt_Topic')
           , (@IdOffset+2, 'Wtt_Topic')
           , (@IdOffset+3, 'Wtt_Topic')
           , (@IdOffset+4, 'Wtt_Topic')
           , (@IdOffset+5, 'Wtt_Topic')
           , (@IdOffset+6, 'Wtt_Topic')
           , (@IdOffset+7, 'Wtt_Topic')
    END
    BEGIN --TopicKind
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Wtt_TopicKind', NULL,		   'Wtt_TopicKind',								   0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',		  @IdOffset+1, 'Wtt_TopicKind|Read',						   1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',		  @IdOffset+1, 'Wtt_TopicKind|Details',						   1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',		  @IdOffset+3, 'Wtt_TopicKind|Details|Write',				   1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Assignment',	  @IdOffset+3, 'Wtt_TopicKind|Details|Assignment',			   0, 1, 1)
          , (@IdOffset+6, @ModuleId, 'Common_User',	  @IdOffset+5, 'Wtt_TopicKind|Details|Assignment|Common_User', 1, 0, 1)
    END
    BEGIN --Activity
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Wtt_Activity',	NULL,		 'Wtt_Activity',									0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',			@IdOffset+1, 'Wtt_Activity|Read',								1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',			@IdOffset+1, 'Wtt_Activity|Details',							1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',			@IdOffset+3, 'Wtt_Activity|Details|Write',						1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Assignment',		@IdOffset+3, 'Wtt_Activity|Details|Assignment',					0, 1, 1)
          , (@IdOffset+6, @ModuleId, 'Common_Section',	@IdOffset+5, 'Wtt_Activity|Details|Assignment|Common_Section',	1, 0, 1)
          , (@IdOffset+7, @ModuleId, 'Common_Position',	@IdOffset+5, 'Wtt_Activity|Details|Assignment|Common_Position',	1, 0, 1)
          , (@IdOffset+8, @ModuleId, 'Wtt_Topic',		@IdOffset+5, 'Wtt_Activity|Details|Assignment|Wtt_Topic',		1, 0, 1)
    END
    BEGIN --ActivityKind
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Wtt_ActivityKind', NULL,		  'Wtt_ActivityKind',								 0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',		     @IdOffset+1, 'Wtt_ActivityKind|Read',						     1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',		     @IdOffset+1, 'Wtt_ActivityKind|Details',						 1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',		     @IdOffset+3, 'Wtt_ActivityKind|Details|Write',				     1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Assignment',	     @IdOffset+3, 'Wtt_ActivityKind|Details|Assignment',			 0, 1, 1)
          , (@IdOffset+6, @ModuleId, 'Common_User',	     @IdOffset+5, 'Wtt_ActivityKind|Details|Assignment|Common_User', 1, 0, 1)
    END
    BEGIN --Entry
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Wtt_Entry', NULL,		   'Wtt_Entry',						0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',	  @IdOffset+1, 'Wtt_Entry|Read',				0, 1, 1)
          , (@IdOffset+3, @ModuleId, 'Own',		  @IdOffset+2, 'Wtt_Entry|Read|Own',			1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Other',	  @IdOffset+2, 'Wtt_Entry|Read|Other',			1, 0, 1)
          , (@IdOffset+5, @ModuleId, 'Details',	  @IdOffset+1, 'Wtt_Entry|Details',				0, 1, 1)
          , (@IdOffset+6, @ModuleId, 'Own',		  @IdOffset+5, 'Wtt_Entry|Details|Own',			1, 0, 1)
          , (@IdOffset+7, @ModuleId, 'Other',	  @IdOffset+5, 'Wtt_Entry|Details|Other',		1, 0, 1)
          , (@IdOffset+8, @ModuleId, 'Write',	  @IdOffset+6, 'Wtt_Entry|Details|Own|Write',	1, 0, 1)
          , (@IdOffset+9, @ModuleId, 'Write',	  @IdOffset+7, 'Wtt_Entry|Details|Other|Write',	1, 0, 1)

        INSERT [Common].[SystemPermissionContext]
            ([SystemPermissionId], [TableKey])
        VALUES
             (@IdOffset+1, 'Wtt_Topic')
           , (@IdOffset+2, 'Wtt_Topic')
           , (@IdOffset+3, 'Wtt_Topic')
           , (@IdOffset+4, 'Wtt_Topic')
           , (@IdOffset+5, 'Wtt_Topic')
           , (@IdOffset+6, 'Wtt_Topic')
           , (@IdOffset+7, 'Wtt_Topic')
           , (@IdOffset+8, 'Wtt_Topic')
           , (@IdOffset+9, 'Wtt_Topic')

           , (@IdOffset+1, 'Common_Section')
           , (@IdOffset+2, 'Common_Section')
           , (@IdOffset+4, 'Common_Section')
           , (@IdOffset+5, 'Common_Section')
           , (@IdOffset+7, 'Common_Section')
           , (@IdOffset+9, 'Common_Section')
    END
    BEGIN --EntryLimitExclusion
        SELECT @IdOffset=ISNULL(MAX([Id]), 0) FROM [Common].[SystemPermission]
        INSERT [Common].[SystemPermission]
            ([Id], [ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@IdOffset+1, @ModuleId, 'Wtt_EntryLimitExclusion', NULL,		 'Wtt_EntryLimitExclusion',			      0, 1, 1)
          , (@IdOffset+2, @ModuleId, 'Read',	                @IdOffset+1, 'Wtt_EntryLimitExclusion|Read',	      1, 0, 1)
          , (@IdOffset+3, @ModuleId, 'Details',	                @IdOffset+1, 'Wtt_EntryLimitExclusion|Details',	      1, 0, 1)
          , (@IdOffset+4, @ModuleId, 'Write',	                @IdOffset+3, 'Wtt_EntryLimitExclusion|Details|Write', 1, 0, 1)
    END

    SET IDENTITY_INSERT [Common].[SystemPermission] OFF

    DECLARE @ParentId INT 
    BEGIN --User
        SET @ParentId = (SELECT TOP 1 [Id] FROM [Common].[SystemPermission] WHERE [Path]='Common_User|Details|Assignment')
        INSERT [Common].[SystemPermission]
            ([ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@ModuleId, 'Wtt_Topic', @ParentId, 'Common_User|Details|Assignment|Wtt_Topic', 1, 0, 1)
    END
    BEGIN --Position
        SET @ParentId = (SELECT TOP 1 [Id] FROM [Common].[SystemPermission] WHERE [Path]='Common_Position|Details|Assignment')
        INSERT [Common].[SystemPermission]
            ([ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@ModuleId, 'Wtt_Activity', @ParentId, 'Common_Position|Details|Assignment|Wtt_Activity', 1, 0, 1)
    END
    BEGIN --Section
        SET @ParentId = (SELECT TOP 1 [Id] FROM [Common].[SystemPermission] WHERE [Path]='Common_Section|Details|Assignment')
        INSERT [Common].[SystemPermission]
            ([ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@ModuleId, 'Wtt_Activity', @ParentId, 'Common_Section|Details|Assignment|Wtt_Activity', 1, 0, 1)
    END
    BEGIN --Settings
        SET @ParentId = (SELECT TOP 1 [Id] FROM [Common].[SystemPermission] WHERE [Path]='Settings')
        INSERT [Common].[SystemPermission]
            ([ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@ModuleId, 'Controlling',  @ParentId,     'Settings|Controlling',                  1, 0, 1)

        SET @ParentId = SCOPE_IDENTITY();
        INSERT [Common].[SystemPermission]
            ([ModuleId], [Key], [ParentId], [Path], [IsUserPermission], [IsContainer], [IsOrganizationDefault])
        VALUES
            (@ModuleId, 'EntryRestriction', @ParentId, 'Settings|Controlling|EntryRestriction', 1, 0, 1)
    END

    INSERT [Common].[Permission](
        [ScopeId]
      , [SystemPermissionId]
      , [CustomPermissionId]
      , [ChangerId])
    SELECT
        1 AS [ScopeId]
      , [csp].[Id] AS [SystemPermissionId]
      , NULL AS [CustomPermissionId]
      , 1 AS [ChangerId]
    FROM [Common].[SystemPermission] AS [csp]
    WHERE [csp].[ModuleId] = @ModuleId

    INSERT [Common].[Permission_User](
        [ScopeId]
      , [UserId]
      , [PermissionId]
      , [ChangerId])
    SELECT
        1 AS [ScopeId]
      , 1 AS [UserId]
      , [cp].[Id] AS [PermissionId]
      , 1 AS [ChangerId]
    FROM [Common].[Permission] AS [cp]
    JOIN [Common].[SystemPermission] AS [csp] ON [csp].[Id] = [cp].[SystemPermissionId]
    WHERE [csp].[ModuleId] = @ModuleId
END--PERMISSIONS

BEGIN --Menu
    DECLARE @MenuSectionId INT

    BEGIN --WTT
        INSERT [Common].[MenuSection]
            ([Key], [Path], [ParentId], [NavigationPosition], [Order], [IconName])
        VALUES
            ('Wtt', 'Wtt', NULL, 1, 0, 'entries')
        SET @MenuSectionId = SCOPE_IDENTITY();

        INSERT [Common].[MenuItem]
            ([Key], [MenuSectionId], [Order], [IconName], [PermissionPath])
        VALUES
            ('OwnEntryList',        @MenuSectionId, 0, 'ownentrylist',        'Wtt_Entry|Read|Own')
          , ('OwnEntryCalendar',    @MenuSectionId, 1, 'ownentrycalendar',    'Wtt_Entry|Read|Own')
          , ('OtherEntryList',      @MenuSectionId, 2, 'otherentrylist',      'Wtt_Entry|Read|Other')
          , ('OtherEntryCalendar',  @MenuSectionId, 3, 'otherentrycalendar',  'Wtt_Entry|Read|Other')
          , ('Topic',               @MenuSectionId, 4, 'topic',               'Wtt_Topic|Read')
          , ('Activity',            @MenuSectionId, 5, 'activity',           'Wtt_Activity|Read')
          , ('EntryLimitExclusion', @MenuSectionId, 5, 'entrylimitexclusion', 'Wtt_EntryLimitExclusion|Read')
    END

    BEGIN --Administration
        SET @MenuSectionId = (SELECT TOP 1 [Id] FROM [Common].[MenuSection] WHERE [Path]='Administration')

        INSERT [Common].[MenuItem]
            ([Key], [MenuSectionId], [Order], [IconName], [PermissionPath])
        VALUES
            ('TopicKind',          @MenuSectionId, 10, 'topicKinds',       'Wtt_TopicKind|Read')
          , ('ActivityKind',       @MenuSectionId, 10, 'activityKinds',    'Wtt_Activity|Read')
    END

    BEGIN --Settings
        SET @MenuSectionId = (SELECT TOP 1 [Id] FROM [Common].[MenuSection] WHERE [Path]='Settings')

        INSERT [Common].[MenuItem]
            ([Key], [MenuSectionId], [Order], [IconName], [PermissionPath])
        VALUES
            ('Controlling', @MenuSectionId, 1, NULL, 'Settings|Controlling')
    END
END

BEGIN --SETTINGS
    DECLARE @MenuItemId INT = (
        SELECT TOP 1 [mi].[Id]
        FROM [Common].[MenuItem] As [mi]
        JOIN [Common].[MenuSection] AS [ms] ON [ms].[Id]=[mi].[MenuSectionId] ANd [ms].[Path] = 'Settings'
        WHERE [mi].[Key] = 'Controlling'
    )
    INSERT [Common].[SettingPanel]
        ([Key], [MenuItemId], [Order], [PermissionPath])
    VALUES
        ('EntryRestrictions', @MenuItemId, 0, 'Settings|Controlling|EntryRestriction')
    DECLARE @SettingPanelId INT = SCOPE_IDENTITY();

    INSERT INTO [Common].[SystemSetting]
        ([ModuleId], [SettingPanelId], [Key], [EditorType], [EditorParameter], [DefaultValue], [CanBeOverridenByOrganization], [CanBeOverridenByUser])
    VALUES 
          (@ModuleId, @SettingPanelId, 'MaximumDaysAllowingToEditPastEntry', 'number', '{ "minValue": 1; "canBeNull": true;  "decimalPlaces":0 }', null, 1, 0)
        , (@ModuleId, @SettingPanelId, 'MaximumDaysAllowingToEditFutureEntry', 'number', '{ "minValue": 1; "canBeNull": true;  "decimalPlaces":0 }', null, 1, 0)
        , (@ModuleId, @SettingPanelId, 'EntryGranularity', 'enum', '{ "canBeNull":false; "values": [ { "resourceKey": "Minute"; "value": 1 }, { "resourceKey": "FiveMinutes"; "value": 5 }, { "resourceKey": "TenMinutes"; "value": 10 }, { "resourceKey": "QuoterOfAnHour"; "value": 15 }, { "resourceKey": "HalfAnHour"; "value": 30 }, { "resourceKey": "Hour"; "value": 60 }, { "resourceKey": "TwoHours"; "value": 1 }, ] }', '1', 1, 0)
        , (@ModuleId, @SettingPanelId, 'EntryDefaultUnit', 'enum', '{ "canBeNull":false; "values": [ { "resourceKey": "Minute"; "value": 1 }, { "resourceKey": "Hour"; "value": 60 }, { "resourceKey": "Day"; "value": 480 }] }', '60', 1, 1)
        , (@ModuleId, @SettingPanelId, 'MaximumEntryTimeValue', 'number', '{ "minValue": 1; "canBeNull": true;  "decimalPlaces":0 }', null, 1, 0)
END