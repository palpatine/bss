﻿ALTER TABLE [Wtt].[Topic_User]
ADD CONSTRAINT [FK_Topic_User_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Wtt].[Topic_User]
ADD CONSTRAINT [FK_Topic_User_TopicId] FOREIGN KEY ([TopicId]) 
  REFERENCES [Wtt].[Topic] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Wtt].[Topic_User]
ADD CONSTRAINT [FK_Topic_User_RoleDefinitionId] FOREIGN KEY ([RoleDefinitionId]) 
  REFERENCES [Common].[RoleDefinition] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
