﻿CREATE TABLE [Wtt].[Topic_User] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]          INT              NOT NULL,
    [UserId]           INT              NOT NULL,
    [TopicId]          INT              NOT NULL,
    [RoleDefinitionId] INT              NOT NULL,
    [DateStart]        SMALLDATETIME    NOT NULL,
    [DateEnd]          SMALLDATETIME    NULL,
    [ChangerId]        INT              NOT NULL,
    [Number]           UNIQUEIDENTIFIER CONSTRAINT [DF_Topic_User_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]        DATETIME         CONSTRAINT [DF_Topic_User_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]          DATETIME         NULL,
    CONSTRAINT [PK_Topic_User] PRIMARY KEY ([Id])
);
