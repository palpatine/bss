﻿ALTER TABLE [Wtt].[Activity_Section]
ADD CONSTRAINT [FK_Activity_Section_ActivityId] FOREIGN KEY ([ActivityId]) 
  REFERENCES [Wtt].[Activity] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Wtt].[Activity_Section]
ADD CONSTRAINT [FK_Activity_Section_SectionId] FOREIGN KEY ([SectionId]) 
  REFERENCES [Common].[Section] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO
