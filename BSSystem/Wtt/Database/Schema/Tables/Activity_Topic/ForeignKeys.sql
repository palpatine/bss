﻿ALTER TABLE [Wtt].[Activity_Topic]
ADD CONSTRAINT [FK_Activity_Topic_ActivityId] FOREIGN KEY ([ActivityId]) 
  REFERENCES [Wtt].[Activity] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Wtt].[Activity_Topic]
ADD CONSTRAINT [FK_Activity_Topic_TopicId] FOREIGN KEY ([TopicId]) 
  REFERENCES [Wtt].[Topic] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO
