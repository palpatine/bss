﻿CREATE TABLE [Wtt].[Activity_Topic] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]          INT              NOT NULL,
    [ActivityId]       INT              NOT NULL,
    [TopicId]          INT              NOT NULL,
    [DateStart]        SMALLDATETIME    NOT NULL,
    [DateEnd]          SMALLDATETIME    NULL,
    [ChangerId]        INT              NOT NULL,
    [Number]           UNIQUEIDENTIFIER CONSTRAINT [DF_Activity_Topic_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]        DATETIME         CONSTRAINT [DF_Activity_Topic_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]          DATETIME         NULL,
    CONSTRAINT [PK_Activity_Topic] PRIMARY KEY ([Id])
);
