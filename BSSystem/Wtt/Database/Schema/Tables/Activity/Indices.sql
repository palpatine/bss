﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[Activity]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
