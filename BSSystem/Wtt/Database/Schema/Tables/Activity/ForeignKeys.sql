﻿ALTER TABLE [Wtt].[Activity]
ADD CONSTRAINT [FK_Activity_ActivityKindId] FOREIGN KEY ([ActivityKindId]) 
  REFERENCES [Wtt].[ActivityKind] (Id) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
