﻿CREATE TABLE [Wtt].[Activity] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]        INT              NOT NULL,
    [ActivityKindId] INT              NOT NULL,
    [DisplayName]    NVARCHAR (25)    NOT NULL,
    [Name]           NVARCHAR (50)    NOT NULL,
    [Comment]        NVARCHAR (MAX)   NULL,
    [Color]		     VARCHAR(30)      NULL,
    [DateStart]      SMALLDATETIME    NOT NULL,
    [DateEnd]        SMALLDATETIME    NULL,
    [ChangerId]      INT              NOT NULL,
    [Number]         UNIQUEIDENTIFIER CONSTRAINT [DF_Activity_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_Activity_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]        DATETIME         NULL,
    CONSTRAINT [PK_Activity] PRIMARY KEY ([Id])
);
