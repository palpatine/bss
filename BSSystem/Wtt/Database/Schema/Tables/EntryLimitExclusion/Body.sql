﻿CREATE TABLE [Wtt].[EntryLimitExclusion] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]        INT              NOT NULL,
    [UserId]		 INT			  NOT NULL,
    [PeriodStart]    SMALLDATETIME    NOT NULL,
    [PeriodEnd]      SMALLDATETIME    NOT NULL,
    [DateStart]      SMALLDATETIME    NOT NULL,
    [DateEnd]        SMALLDATETIME    NULL,
    [ChangerId]      INT              NOT NULL,
    [Comment]		 NVARCHAR(max)	  NULL,
    [Number]         UNIQUEIDENTIFIER CONSTRAINT [DF_EntryLimitExclusion_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_EntryLimitExclusion_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]        DATETIME         NULL,
    CONSTRAINT [PK_EntryLimitExclusion] PRIMARY KEY ([Id])
);
