﻿ALTER TABLE [Wtt].[EntryLimitExclusion]
ADD CONSTRAINT [FK_EntryLimitExclusion_UserId] FOREIGN KEY ([UserId]) 
  REFERENCES [Common].[User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION

