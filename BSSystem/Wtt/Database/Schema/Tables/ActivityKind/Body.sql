﻿CREATE TABLE [Wtt].[ActivityKind] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]          INT              NOT NULL,
    [OrganizationId]   INT              NOT NULL,
    [DisplayName]      NVARCHAR (25)    NOT NULL,
    [Name]             NVARCHAR (50)    NOT NULL,    
    [DateStart]        SMALLDATETIME    NOT NULL,
    [DateEnd]          SMALLDATETIME    NULL,
    [ChangerId]        INT              NOT NULL,
    [Number]           UNIQUEIDENTIFIER CONSTRAINT [DF_ActivityKind_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]        DATETIME         CONSTRAINT [DF_ActivityKind_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]          DATETIME         NULL,
    CONSTRAINT [PK_ActivityKind] PRIMARY KEY ([Id])
);
