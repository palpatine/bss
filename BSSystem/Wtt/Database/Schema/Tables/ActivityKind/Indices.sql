﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[ActivityKind]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
