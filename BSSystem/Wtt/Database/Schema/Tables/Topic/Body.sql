﻿CREATE TABLE [Wtt].[Topic] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]        INT              NOT NULL,
    [TopicKindId]    INT			  NOT NULL,
    [DisplayName]    NVARCHAR (25)    NOT NULL,
    [Name]           NVARCHAR (50)    NOT NULL,
    [Comment]        NVARCHAR (max)   NULL,
    [DateStart]      SMALLDATETIME    NOT NULL,
    [DateEnd]        SMALLDATETIME    NULL,
    [ChangerId]      INT              NOT NULL,
    [Number]         UNIQUEIDENTIFIER CONSTRAINT [DF_Topic_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_Topic_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]        DATETIME         NULL,
    CONSTRAINT [PK_Topic] PRIMARY KEY ([Id])
);
