﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[Topic]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
