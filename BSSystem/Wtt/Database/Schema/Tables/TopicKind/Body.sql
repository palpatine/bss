﻿CREATE TABLE [Wtt].[TopicKind] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]          INT              NOT NULL,
    [DisplayName]      NVARCHAR (25)    NOT NULL,
    [Name]             NVARCHAR (50)    NOT NULL,    
    [DateStart]        SMALLDATETIME    NOT NULL,
    [DateEnd]          SMALLDATETIME    NULL,
    [ChangerId]        INT              NOT NULL,
    [CanAcceptEntries] BIT			    NOT NULL,
    [Number]           UNIQUEIDENTIFIER CONSTRAINT [DF_TopicKind_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]        DATETIME         CONSTRAINT [DF_TopicKind_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]          DATETIME         NULL,
    CONSTRAINT [PK_TopicKind] PRIMARY KEY ([Id])
);
