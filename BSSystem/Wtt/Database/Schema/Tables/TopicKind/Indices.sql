﻿CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[TopicKind]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
