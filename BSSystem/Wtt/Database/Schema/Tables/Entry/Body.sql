﻿CREATE TABLE [Wtt].[Entry] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]        INT              NOT NULL,
    [TopicId]		 INT              NOT NULL,
    [ActivityId]	 INT              NOT NULL,
    [UserId]		 INT              NOT NULL,
    [Date]			 SMALLDATETIME    NOT NULL,
    [Minutes]     	 INT              NOT NULL,
    [Comment]        NVARCHAR (MAX)   NULL,
    [ChangerId]      INT              NOT NULL,
    [Number]         UNIQUEIDENTIFIER CONSTRAINT [DF_Entry_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]      DATETIME         CONSTRAINT [DF_Entry_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]        DATETIME         NULL,
    CONSTRAINT [PK_Entry] PRIMARY KEY ([Id])
);
