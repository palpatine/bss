﻿ALTER TABLE [Wtt].[Activity_Position]
ADD CONSTRAINT [FK_Activity_Position_ActivityId] FOREIGN KEY ([ActivityId]) 
  REFERENCES [Wtt].[Activity] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [Wtt].[Activity_Position]
ADD CONSTRAINT [FK_Activity_Position_PositionId] FOREIGN KEY ([PositionId]) 
  REFERENCES [Common].[Position] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO
