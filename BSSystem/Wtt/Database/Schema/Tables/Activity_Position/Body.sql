﻿CREATE TABLE [Wtt].[Activity_Position] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [ScopeId]          INT              NOT NULL,
    [ActivityId]       INT              NOT NULL,
    [PositionId]       INT              NOT NULL,
    [DateStart]        SMALLDATETIME    NOT NULL,
    [DateEnd]          SMALLDATETIME    NULL,
    [ChangerId]        INT              NOT NULL,
    [Number]           UNIQUEIDENTIFIER CONSTRAINT [DF_Activity_Position_Number] DEFAULT (newid()) NOT NULL,
    [TimeStart]        DATETIME         CONSTRAINT [DF_Activity_Position_TimeStart] DEFAULT (getutcdate()) NOT NULL,
    [TimeEnd]          DATETIME         NULL,
    CONSTRAINT [PK_Position_Topic] PRIMARY KEY ([Id])
);
