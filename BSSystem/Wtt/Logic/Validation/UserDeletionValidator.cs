﻿using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    public sealed class UserDeletionValidator : DominantEntityDeletionValidator<IUser>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public UserDeletionValidator(IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<bool> CheckSlaveExistanceAsync(IUser entity)
        {
            var slaveExists = await
                    _databaseAccessor.Query<EntryLimitExclusion>()
                    .Where(x => !x.IsDeleted
                              && x.User.IsEquivalent(entity))
                    .Select(x => x.Id)
                .UnionAll(
                    _databaseAccessor.Query<Entry>()
                    .Where(x => !x.IsDeleted
                            && x.User.IsEquivalent(entity))
                    .Select(x => x.Id))
                .UnionAll(
                    _databaseAccessor.Query<TopicUser>()
                    .Where(x => !x.IsDeleted
                            && x.User.IsEquivalent(entity))
                    .Select(x => x.Id))
                .AnyAsync();

            return slaveExists;
        }
    }
}
