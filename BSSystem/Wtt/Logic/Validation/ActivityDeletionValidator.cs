﻿using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    public sealed class ActivityDeletionValidator : DominantEntityDeletionValidator<IActivity>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public ActivityDeletionValidator(IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<bool> CheckSlaveExistanceAsync(IActivity entity)
        {
            var slaveExists = await
                    _databaseAccessor.Query<ActivityPosition>()
                    .Where(x => !x.IsDeleted
                              && x.Activity.IsEquivalent(entity))
                    .Select(x => x.Id)
                .UnionAll(
                    _databaseAccessor.Query<ActivitySection>()
                    .Where(x => !x.IsDeleted
                            && x.Activity.IsEquivalent(entity))
                    .Select(x => x.Id))
                .UnionAll(
                    _databaseAccessor.Query<Entry>()
                    .Where(x => !x.IsDeleted
                            && x.Activity.IsEquivalent(entity))
                    .Select(x => x.Id))
                .UnionAll(
                    _databaseAccessor.Query<ActivityTopic>()
                    .Where(x => !x.IsDeleted
                            && x.Activity.IsEquivalent(entity))
                    .Select(x => x.Id))
                .AnyAsync();

            return slaveExists;
        }
    }
}
