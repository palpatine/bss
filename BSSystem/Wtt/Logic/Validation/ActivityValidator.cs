﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    internal class ActivityValidator : EntityValidator<Activity>
    {
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ISessionDataProvider _sessionDataProvider;

        public ActivityValidator(
            IServiceLocator serviceLocator,
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider)
            : base(serviceLocator)
        {
            _databaseAccessor = databaseAccessor;
            _sessionDataProvider = sessionDataProvider;
        }

        public override async Task ValidateAsync(IValidationResult<Activity> validator)
        {
            await VerifyTopicKind(validator);
            await ValidateSlavesDatesAsync(validator);
        }

        private async Task ValidateSlavesDatesAsync(IValidationResult<Activity> validator)
        {
            var slavesDates = await
                    _databaseAccessor.Query<ActivityPosition>()
                    .Where(x => !x.IsDeleted
                              && x.Activity.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd })
                .UnionAll(
                    _databaseAccessor.Query<ActivitySection>()
                    .Where(x => !x.IsDeleted
                            && x.Activity.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd }))
                .UnionAll(
                    _databaseAccessor.Query<Entry>()
                    .Where(x => !x.IsDeleted
                            && x.Activity.IsEquivalent(validator.Entity))
                    .Select(x => new { DateStart = x.Date, DateEnd = (DateTime?)x.Date }))
                .UnionAll(
                    _databaseAccessor.Query<ActivityTopic>()
                    .Where(x => !x.IsDeleted
                            && x.Activity.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd }))
                .ToListAsync();

            if (!slavesDates.Any())
            {
                return;
            }

            var extremeSlavesDate = new
            {
                DateStart = slavesDates.Min(x => x.DateStart),
                DateEnd = slavesDates.Max(x => x.DateEnd)
            };

            validator
                .MustBeBeforeOrEqual(x => x.DateStart, extremeSlavesDate.DateStart)
                .MustBeAfterOrEqual(x => x.DateEnd, extremeSlavesDate.DateEnd);
        }

        private async Task VerifyTopicKind(IValidationResult<Activity> validator)
        {
            var activityKind = await _databaseAccessor.Query<ActivityKind>()
                .Where(x => !x.IsDeleted && x.Scope.IsEquivalent(_sessionDataProvider.Scope) && x.IsEquivalent(validator.Entity.ActivityKind))
                .Select(x => new DatedEntityModel { Id = x.Id, DateStart = x.DateStart, DateEnd = x.DateEnd })
                    .SingleOrDefaultAsync();
            if (activityKind == null)
            {
                validator.AddError(x => x.ActivityKind, "NotFound");
            }
            else
            {
                validator.ValidateDatesConsistencyWithMasterEntity(activityKind);
            }
        }
    }
}