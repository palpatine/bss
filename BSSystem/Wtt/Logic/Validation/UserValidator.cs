﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    internal class UserValidator : EntityValidator<UserEditModel>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public UserValidator(
            IServiceLocator serviceLocator,
            IDatabaseAccessor databaseAccessor)
            : base(serviceLocator)
        {
            _databaseAccessor = databaseAccessor;
        }

        public override async Task ValidateAsync(IValidationResult<UserEditModel> validator)
        {
            await ValidateSlavesDatesAsync(validator);
        }

        private async Task ValidateSlavesDatesAsync(IValidationResult<UserEditModel> validator)
        {
            var slavesDates = await
                    _databaseAccessor.Query<EntryLimitExclusion>()
                    .Where(x => !x.IsDeleted
                              && x.User.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd })
                .UnionAll(
                    _databaseAccessor.Query<Entry>()
                    .Where(x => !x.IsDeleted
                            && x.User.IsEquivalent(validator.Entity))
                    .Select(x => new { DateStart = x.Date, DateEnd = (DateTime?)x.Date }))
                .UnionAll(
                    _databaseAccessor.Query<TopicUser>()
                    .Where(x => !x.IsDeleted
                            && x.User.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd }))
                .ToListAsync();

            if (!slavesDates.Any())
            {
                return;
            }

            var extremeSlavesDate = new
            {
                DateStart = slavesDates.Min(x => x.DateStart),
                DateEnd = slavesDates.Max(x => x.DateEnd)
            };

            validator
                .MustBeBeforeOrEqual(x => x.DateStart, extremeSlavesDate.DateStart)
                .MustBeAfterOrEqual(x => x.DateEnd, extremeSlavesDate.DateEnd);
        }
    }
}