﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    public sealed class RoleDefinitionValidator : EntityValidator<RoleDefinition>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public RoleDefinitionValidator(
            IServiceLocator serviceLocator,
            IDatabaseAccessor databaseAccessor)
            : base(serviceLocator)
        {
            _databaseAccessor = databaseAccessor;
        }

        public override async Task ValidateAsync(IValidationResult<RoleDefinition> validator)
        {
            await ValidateSlavesDatesAsync(validator);
        }

        private async Task ValidateSlavesDatesAsync(IValidationResult<RoleDefinition> validator)
        {
            var slavesDates = await
                    _databaseAccessor.Query<TopicUser>()
                    .Where(x => !x.IsDeleted
                              && x.RoleDefinition.IsEquivalent(validator.Entity))
                    .SelectAsync(x => new { x.DateStart, x.DateEnd });

            if (!slavesDates.Any())
            {
                return;
            }

            var extremeSlavesDate = new
            {
                DateStart = slavesDates.Min(x => x.DateStart),
                DateEnd = slavesDates.Max(x => x.DateEnd)
            };

            validator
                .MustBeBeforeOrEqual(x => x.DateStart, extremeSlavesDate.DateStart)
                .MustBeAfterOrEqual(x => x.DateEnd, extremeSlavesDate.DateEnd);
        }
    }
}