﻿using System.Data;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    public sealed class SectionDeletionValidator : DominantEntityDeletionValidator<ISection>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public SectionDeletionValidator(IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<bool> CheckSlaveExistanceAsync(ISection entity)
        {
            var slaveExists = await
                    _databaseAccessor.Query<ActivitySection>()
                    .Where(x => !x.IsDeleted
                              && x.Section.IsEquivalent(entity))
                .AnyAsync();

            return slaveExists;
        }
    }
}