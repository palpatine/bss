﻿using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    public sealed class TopicDeletionValidator : DominantEntityDeletionValidator<ITopic>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public TopicDeletionValidator(IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<bool> CheckSlaveExistanceAsync(ITopic entity)
        {
            var slaveExists = await
                    _databaseAccessor.Query<ActivityTopic>()
                    .Where(x => !x.IsDeleted
                              && x.Topic.IsEquivalent(entity))
                    .Select(x => x.Id)
                .UnionAll(
                    _databaseAccessor.Query<Entry>()
                    .Where(x => !x.IsDeleted
                            && x.Topic.IsEquivalent(entity))
                    .Select(x => x.Id))
                .UnionAll(
                    _databaseAccessor.Query<TopicUser>()
                    .Where(x => !x.IsDeleted
                            && x.Topic.IsEquivalent(entity))
                    .Select(x => x.Id))
                .AnyAsync();

            return slaveExists;
        }
    }
}
