﻿using System;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    internal class EntryValidator : EntityValidator<Entry>
    {
        private readonly IDatabaseAccessor _accessor;
        private readonly ISessionDataProvider _sessionDataProvider;

        public EntryValidator(
            IServiceLocator serviceLocator,
            ISessionDataProvider sessionDataProvider,
            IDatabaseAccessor accessor)
            : base(serviceLocator)
        {
            _sessionDataProvider = sessionDataProvider;
            _accessor = accessor;
        }

        public async override Task ValidateAsync(IValidationResult<Entry> validator)
        {
            await VerifyUser(validator);
            await VerifyActivity(validator);
            await VerifyTopic(validator);
        }

        private async Task VerifyActivity(IValidationResult<Entry> validator)
        {
            var activitySection = _accessor.Query<ActivitySection>()
                .Where(x => !x.IsDeleted
                            && x.DateStart <= validator.Entity.Date
                            && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date)
                .Join(
                    _accessor.Query<SectionUser>()
                        .Where(x => !x.IsDeleted
                                    && x.DateStart <= validator.Entity.Date
                                    && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date
                                    && x.User.IsEquivalent(validator.Entity.User)),
                    x => x.Section,
                    x => x.Section,
                    (ase, sus) => new { ase.Activity, ase.Id });

            var activityTopic = _accessor.Query<ActivityTopic>()
                .Where(x => !x.IsDeleted
                            && x.DateStart <= validator.Entity.Date
                            && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date)
                .Join(
                    _accessor.Query<TopicUser>()
                        .Where(x => !x.IsDeleted
                                    && x.DateStart <= validator.Entity.Date
                                    && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date
                                    && x.User.IsEquivalent(validator.Entity.User)),
                    x => x.Topic,
                    x => x.Topic,
                    (at, tu) => new { at.Activity, at.Id });

            var activityPosition = _accessor.Query<ActivityPosition>()
                .Where(x => !x.IsDeleted
                            && x.DateStart <= validator.Entity.Date
                            && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date)
                .Join(
                    _accessor.Query<PositionUser>()
                        .Where(x => !x.IsDeleted
                                    && x.DateStart <= validator.Entity.Date
                                    && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date
                                    && x.User.IsEquivalent(validator.Entity.User)),
                    x => x.Position,
                    x => x.Position,
                    (ap, pu) => new { ap.Activity, ap.Id });

            var isActivtyValid = await _accessor.Query<Activity>()
                .Where(x => x.IsEquivalent(validator.Entity.Activity)
                            && !x.IsDeleted
                            && x.Scope.IsEquivalent(_sessionDataProvider.Scope)
                            && x.DateStart <= validator.Entity.Date
                            && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date)
                .LeftJoin(activitySection, x => x, x => x.Activity, (a, ase) => new { ActivityId = a.Id, ActivitySection = (int?)ase.Id })
                .LeftJoin(activityTopic, x => x.ActivityId, x => x.Activity.Id, (a, at) => new { a.ActivityId, a.ActivitySection, ActivityTopic = (int?)at.Id })
                .LeftJoin(activityPosition, x => x.ActivityId, x => x.Activity.Id, (a, ap) => new { a.ActivityId, a.ActivitySection, a.ActivityTopic, ActivityPosition = (int?)ap.Id })
                .Where(x => x.ActivitySection != null || x.ActivityTopic != null || x.ActivityPosition != null)
                .AnyAsync();

            if (!isActivtyValid)
            {
                validator.AddError(x => x.Activity, "NotFound");
            }
        }

        private async Task VerifyTopic(IValidationResult<Entry> validator)
        {
            var isTopicValid = await _accessor.Query<Topic>()
                .Where(x => x.IsEquivalent(validator.Entity.Topic)
                            && !x.IsDeleted
                            && x.Scope.IsEquivalent(_sessionDataProvider.Scope)
                            && x.DateStart <= validator.Entity.Date
                            && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date)
                .Join(
                    _accessor.Query<TopicUser>()
                        .Where(x => !x.IsDeleted
                                    && x.DateStart <= validator.Entity.Date
                                    && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date
                                    && x.User.IsEquivalent(validator.Entity.User)),
                    x => x,
                    x => x.Topic,
                    (t, tu) => new { t.Id })
                .AnyAsync();

            if (!isTopicValid)
            {
                validator.AddError(x => x.Topic, "NotFound");
            }
        }

        private async Task VerifyUser(IValidationResult<Entry> validator)
        {
            var isUserValid = await _accessor.Query<User>()
                .Where(x => x.IsEquivalent(validator.Entity.User)
                            && !x.IsDeleted
                            && x.Scope.IsEquivalent(_sessionDataProvider.Scope)
                            && x.DateStart <= validator.Entity.Date
                            && (x.DateEnd ?? DateTime.MaxValue) >= validator.Entity.Date)
                .AnyAsync();

            if (!isUserValid)
            {
                validator.AddError(x => x.User, "NotFound");
            }
        }
    }
}