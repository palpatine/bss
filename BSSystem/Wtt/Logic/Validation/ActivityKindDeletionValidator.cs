﻿using System.Data;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    public sealed class ActivityKindDeletionValidator : DominantEntityDeletionValidator<IActivityKind>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public ActivityKindDeletionValidator(IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<bool> CheckSlaveExistanceAsync(IActivityKind entity)
        {
            var slaveExists = await
                    _databaseAccessor.Query<Activity>()
                    .Where(x => !x.IsDeleted
                              && x.ActivityKind.IsEquivalent(entity))
                .AnyAsync();

            return slaveExists;
        }
    }
}