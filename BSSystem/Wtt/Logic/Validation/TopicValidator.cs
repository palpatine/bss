﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    internal sealed class TopicValidator : EntityValidator<Topic>
    {
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ISessionDataProvider _sessionDataProvider;

        public TopicValidator(
            IServiceLocator serviceLocator,
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider)
            : base(serviceLocator)
        {
            _databaseAccessor = databaseAccessor;
            _sessionDataProvider = sessionDataProvider;
        }

        public async override Task ValidateAsync(IValidationResult<Topic> validator)
        {
            await VerifyTopicKind(validator);
            await ValidateSlavesDatesAsync(validator);
        }

        private async Task ValidateSlavesDatesAsync(IValidationResult<Topic> validator)
        {
            var slavesDates = await
                    _databaseAccessor.Query<ActivityTopic>()
                    .Where(x => !x.IsDeleted
                              && x.Topic.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd })
                .UnionAll(
                    _databaseAccessor.Query<Entry>()
                    .Where(x => !x.IsDeleted
                            && x.Topic.IsEquivalent(validator.Entity))
                    .Select(x => new { DateStart = x.Date, DateEnd = (DateTime?)x.Date }))
                .UnionAll(
                    _databaseAccessor.Query<TopicUser>()
                    .Where(x => !x.IsDeleted
                            && x.Topic.IsEquivalent(validator.Entity))
                    .Select(x => new { x.DateStart, x.DateEnd }))
                .ToListAsync();

            if (!slavesDates.Any())
            {
                return;
            }

            var extremeSlavesDate = new
            {
                DateStart = slavesDates.Min(x => x.DateStart),
                DateEnd = slavesDates.Max(x => x.DateEnd)
            };

            validator
                .MustBeBeforeOrEqual(x => x.DateStart, extremeSlavesDate.DateStart)
                .MustBeAfterOrEqual(x => x.DateEnd, extremeSlavesDate.DateEnd);
        }

        private async Task VerifyTopicKind(IValidationResult<Topic> validator)
        {
            var topicKind = await _databaseAccessor.Query<TopicKind>()
                .Where(x => !x.IsDeleted && x.Scope.IsEquivalent(_sessionDataProvider.Scope) && x.IsEquivalent(validator.Entity.TopicKind))
                .Select(x => new DatedEntityModel { Id = x.Id, DateStart = x.DateStart, DateEnd = x.DateEnd })
                    .SingleOrDefaultAsync();
            if (topicKind == null)
            {
                validator.AddError(x => x.TopicKind, "NotFound");
            }
            else
            {
                validator.ValidateDatesConsistencyWithMasterEntity(topicKind);
            }
        }
    }
}