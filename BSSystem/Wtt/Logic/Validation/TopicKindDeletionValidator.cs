﻿using System.Data;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    public sealed class TopicKindDeletionValidator : DominantEntityDeletionValidator<ITopicKind>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public TopicKindDeletionValidator(IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        protected override async Task<bool> CheckSlaveExistanceAsync(ITopicKind entity)
        {
            var slaveExists = await
                    _databaseAccessor.Query<Topic>()
                    .Where(x => !x.IsDeleted
                              && x.TopicKind.IsEquivalent(entity))
                .AnyAsync();

            return slaveExists;
        }
    }
}