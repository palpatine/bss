﻿using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Environment;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    internal sealed class EntryLimitExclusionValidator : EntityValidator<EntryLimitExclusion>
    {
        private readonly IDatabaseAccessor _databaseAccessor;
        private readonly ISessionDataProvider _sessionDataProvider;

        public EntryLimitExclusionValidator(
            IServiceLocator serviceLocator,
            IDatabaseAccessor databaseAccessor,
            ISessionDataProvider sessionDataProvider)
            : base(serviceLocator)
        {
            _databaseAccessor = databaseAccessor;
            _sessionDataProvider = sessionDataProvider;
        }

        public async override Task ValidateAsync(IValidationResult<EntryLimitExclusion> validator)
        {
            var user = await _databaseAccessor.Query<User>()
                .Where(x => !x.IsDeleted && x.Scope.IsEquivalent(_sessionDataProvider.Scope) && x.IsEquivalent(validator.Entity.User))
                .Select(x => new DatedEntityModel { Id = x.Id, DateStart = x.DateStart, DateEnd = x.DateEnd })
                    .SingleOrDefaultAsync();
            if (user == null)
            {
                validator.AddError(x => x.User, "NotFound");
            }
            else
            {
                validator.ValidateDatesConsistencyWithMasterEntity(user);
            }
        }
    }
}