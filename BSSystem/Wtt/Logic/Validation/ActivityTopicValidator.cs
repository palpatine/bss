using System.Linq;
using System.Threading.Tasks;
using Qdarc.Modules.Common.QueryGeneration;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Udts;
using SolvesIt.BSSystem.Core.Abstraction;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Sql;
using SolvesIt.BSSystem.Core.Abstraction.Validation;
using SolvesIt.BSSystem.Core.Logic.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;

namespace SolvesIt.BSSystem.WttModule.Logic.Validation
{
    internal class ActivityTopicValidator : JunctionEntityValidator<ActivityTopic, IActivity, ITopic>
    {
        private readonly IDatabaseAccessor _databaseAccessor;

        public ActivityTopicValidator(
            IDatabaseAccessor databaseAccessor)
        {
            _databaseAccessor = databaseAccessor;
        }

        public async override Task ValidateAsync(IJunctionValidationResult<ITopic, AssignmentUdt<IActivity>> validator)
        {
            // todo: validate deleted elements
            var sets = await _databaseAccessor.Query<Entry>()
                .Where(x => x.Topic.IsEquivalent(validator.Master))
                .Join(
                    _databaseAccessor.Values(validator.Assignments),
                    x => x.Activity.Id,
                    x => x.Element.Id,
                    (e, v) => new
                    {
                        EntryDate = e.Date,
                        v.DateStart,
                        v.DateEnd,
                        v.Id
                    })
                .Where(x => x.EntryDate < x.DateStart || x.DateEnd > x.EntryDate)
                .Select(x => new { x.Id, x.EntryDate })
                .ToListAsync();

            var extremeSlaveDates = sets.GroupBy(x => x.Id)
                .Select(x => new { Id = x.Key, Min = x.Min(z => z.EntryDate), Max = x.Max(z => z.EntryDate) })
                .ToArray();

            foreach (var assignment in validator.Assignments)
            {
                validator.GetValidator(assignment).MustBeBeforeOrEqual(x => x.DateStart, extremeSlaveDates.Single(z => z.Id == assignment.Id).Min);
                validator.GetValidator(assignment).MustBeBeforeOrEqual(x => x.DateEnd, extremeSlaveDates.Single(z => z.Id == assignment.Id).Min);
            }
        }

        public async override Task ValidateAsync(IJunctionValidationResult<IActivity, AssignmentUdt<ITopic>> validator)
        {
            // todo: validate deleted elements
            var sets = await _databaseAccessor.Query<Entry>()
                .Where(x => x.Activity.IsEquivalent(validator.Master))
                .Join(
                    _databaseAccessor.Values(validator.Assignments),
                    x => x.Topic.Id,
                    x => x.Element.Id,
                    (e, v) => new
                    {
                        EntryDate = e.Date,
                        v.DateStart,
                        v.DateEnd,
                        v.Id
                    })
                .Where(x => x.EntryDate < x.DateStart || x.DateEnd > x.EntryDate)
                .Select(x => new { x.Id, x.EntryDate })
                .ToListAsync();

            var extremeSlaveDates = sets.GroupBy(x => x.Id)
                .Select(x => new { Id = x.Key, Min = x.Min(z => z.EntryDate), Max = x.Max(z => z.EntryDate) })
                .ToArray();

            foreach (var assignment in validator.Assignments)
            {
                validator.GetValidator(assignment).MustBeBeforeOrEqual(x => x.DateStart, extremeSlaveDates.Single(z => z.Id == assignment.Id).Min);
                validator.GetValidator(assignment).MustBeBeforeOrEqual(x => x.DateEnd, extremeSlaveDates.Single(z => z.Id == assignment.Id).Min);
            }
        }
    }
}