﻿using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Models;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.CommonModule.Abstraction.Validation;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Interfaces;
using SolvesIt.BSSystem.WttModule.Logic.Validation;

namespace SolvesIt.BSSystem.WttModule.Logic.Environment
{
    public sealed class Bootstrapper : IBootstrapper
    {
        public double Order => 1;

        public void Bootstrap(IRegistrar registrar)
        {
            registrar.RegisterMultiple<IRoleTargetsProvider, WttRoleTargetsProvider>();
            RegisterValidators(registrar);
        }

        private static void RegisterValidators(IRegistrar registrar)
        {
            registrar.RegisterMultiple<IEntityValidator<Activity>, ActivityValidator>();
            registrar.RegisterMultiple<IEntityValidator<ActivityKind>, ActivityKindValidator>();
            registrar.RegisterMultiple<IEntityValidator<EntryLimitExclusion>, EntryLimitExclusionValidator>();
            registrar.RegisterMultiple<IEntityValidator<Entry>, EntryValidator>();
            registrar.RegisterMultiple<IEntityValidator<Position>, PositionValidator>();
            registrar.RegisterMultiple<IEntityValidator<RoleDefinition>, RoleDefinitionValidator>();
            registrar.RegisterMultiple<IEntityValidator<Section>, SectionValidator>();
            registrar.RegisterMultiple<IEntityValidator<Topic>, TopicValidator>();
            registrar.RegisterMultiple<IEntityValidator<TopicKind>, TopicKindValidator>();
            registrar.RegisterMultiple<IEntityValidator<UserEditModel>, UserValidator>();

            registrar.RegisterMultiple<IEntityDeletionValidator<IActivity>, ActivityDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<IActivityKind>, ActivityKindDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<IPosition>, PositionDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<IRoleDefinition>, RoleDefinitionDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<ISection>, SectionDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<ITopic>, TopicDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<ITopicKind>, TopicKindDeletionValidator>();
            registrar.RegisterMultiple<IEntityDeletionValidator<IUser>, UserDeletionValidator>();
        }
    }
}