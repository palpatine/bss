﻿using System.Collections.Generic;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Entities;
using SolvesIt.BSSystem.CommonModule.Abstraction.DataAccess.Markers;
using SolvesIt.BSSystem.CommonModule.Abstraction.Services;
using SolvesIt.BSSystem.Core.Logic.DataAccess.ModelMetadata;
using SolvesIt.BSSystem.WttModule.Abstraction.DataAccess.Entities;

namespace SolvesIt.BSSystem.WttModule.Logic.Environment
{
    internal sealed class WttRoleTargetsProvider : IRoleTargetsProvider
    {
        public IEnumerable<RoleTargetDescriptor> Targets => new[]
        {
            new RoleTargetDescriptor
            {
                TargetType = typeof(Topic),
                RoleTarget = new RoleTargetMarker
                {
                    Key = StorableHelper.GetTableKey<Topic>()
                }
            }
        };
    }
}
