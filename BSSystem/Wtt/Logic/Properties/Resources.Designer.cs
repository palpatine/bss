﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SolvesIt.BSSystem.WttModule.Logic.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SolvesIt.BSSystem.WttModule.Logic.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activities management.
        /// </summary>
        internal static string ActivitiesAdministrationSectionGroup {
            get {
                return ResourceManager.GetString("ActivitiesAdministrationSectionGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activities.
        /// </summary>
        internal static string ActivitiesAdministrationSectionGroupShortName {
            get {
                return ResourceManager.GetString("ActivitiesAdministrationSectionGroupShortName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Work card.
        /// </summary>
        internal static string EntriesListManagementSectionGroup {
            get {
                return ResourceManager.GetString("EntriesListManagementSectionGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entries list.
        /// </summary>
        internal static string EntriesListManagementSectionGroupShortName {
            get {
                return ResourceManager.GetString("EntriesListManagementSectionGroupShortName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Work card.
        /// </summary>
        internal static string EntriesManagementSectionGroup {
            get {
                return ResourceManager.GetString("EntriesManagementSectionGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entries.
        /// </summary>
        internal static string EntriesManagementSectionGroupShortName {
            get {
                return ResourceManager.GetString("EntriesManagementSectionGroupShortName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entries.
        /// </summary>
        internal static string EntriesModuleSections {
            get {
                return ResourceManager.GetString("EntriesModuleSections", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entry limit exclusion management.
        /// </summary>
        internal static string EntryLimitExclusionEntriesSectionGroup {
            get {
                return ResourceManager.GetString("EntryLimitExclusionEntriesSectionGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entry limit exclusion.
        /// </summary>
        internal static string EntryLimitExclusionEntriesSectionGroupShortName {
            get {
                return ResourceManager.GetString("EntryLimitExclusionEntriesSectionGroupShortName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kinds management.
        /// </summary>
        internal static string TopicKindAdministrationSectionGroup {
            get {
                return ResourceManager.GetString("TopicKindAdministrationSectionGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Topics kinds.
        /// </summary>
        internal static string TopicKindAdministrationSectionGroupShortName {
            get {
                return ResourceManager.GetString("TopicKindAdministrationSectionGroupShortName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Topics management.
        /// </summary>
        internal static string TopicsAdministrationSectionGroup {
            get {
                return ResourceManager.GetString("TopicsAdministrationSectionGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Topics.
        /// </summary>
        internal static string TopicsAdministrationSectionGroupShortName {
            get {
                return ResourceManager.GetString("TopicsAdministrationSectionGroupShortName", resourceCulture);
            }
        }
    }
}
