﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using SolvesIt.BSSystem.Core.Abstraction.DataAccess.Attributes;

[assembly: AssemblyTitle("BestSolutions.BSSystem.WttModule.Abstraction")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("BestSolutions.BSSystem.WttModule.Abstraction")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: ComVisible(false)]

[assembly: Guid("17c15875-0892-4ad6-9181-38189690a2b7")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: DefaultSchemaName("Wtt")]