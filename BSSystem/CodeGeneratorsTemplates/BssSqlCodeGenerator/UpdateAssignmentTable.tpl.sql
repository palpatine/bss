CREATE PROCEDURE [##JunctionTableSchemaName##].[Update_##FirstTableName##_##SecondTableName##]
(
    @ScopeId INT,
    @ChangerId INT,
    @MasterColumnName VARCHAR(50),
    @MasterId INT,
    @NewList [Common].[AssignmentList] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

##UpdateJunctionTableCommonValidation()##
   
    DECLARE @NotAllovedDateValue SMALLDATETIME = '2079-06-06'

    ;WITH
    TargetCTE AS (
        SELECT
            [T].[Id]
          , [T].[ScopeId]
          , [T].[##FirstTableName##Id]
          , [T].[##SecondTableName##Id]
          , [T].[DateStart]
          , [T].[DateEnd]
          , [T].[ChangerId]
          , [T].[TimeEnd]
        FROM [##JunctionTableSchemaName##].[##FirstTableName##_##SecondTableName##] [T]
        WHERE [T].[IsDeleted] = 0
        AND (  @MasterColumnName = '[##FirstTableName##Id]' AND [T].[##FirstTableName##Id] = @MasterId
            OR @MasterColumnName = '[##SecondTableName##Id]' AND [T].[##SecondTableName##Id] = @MasterId)
    ),
    SourceCTE AS (
        SELECT
            [nl].[Id] AS [Id]
          , IIF(@MasterColumnName='[##FirstTableName##Id]', @MasterId, [nl].[ElementId]) AS [##FirstTableName##Id]
          , IIF(@MasterColumnName='[##SecondTableName##Id]', @MasterId, [nl].[ElementId]) AS [##SecondTableName##Id]
          , [nl].[DateStart] AS [DateStart]
          , [nl].[DateEnd] AS [DateEnd]
        FROM @NewList [nl]
    )
    MERGE TargetCTE AS [T]
    USING SourceCTE AS [S]
    ON [T].[Id] = [S].[Id]
    WHEN NOT MATCHED BY TARGET THEN
        INSERT ([##FirstTableName##Id], [##SecondTableName##Id], [DateStart], [DateEnd], [ChangerId], [ScopeId])
        VALUES ([S].[##FirstTableName##Id], [S].[##SecondTableName##Id], [S].[DateStart], [S].[DateEnd], @ChangerId, @ScopeId)
    WHEN MATCHED AND 
          (  [T].[##FirstTableName##Id] <> [S].[##FirstTableName##Id]
          OR [T].[##SecondTableName##Id] <> [S].[##SecondTableName##Id]
          OR [T].[DateStart] <> [S].[DateStart]
          OR COALESCE([T].[DateEnd], @NotAllovedDateValue) <> COALESCE([S].[DateEnd], @NotAllovedDateValue) ) THEN
        UPDATE SET
            [T].[##FirstTableName##Id] = [S].[##FirstTableName##Id]
          , [T].[##SecondTableName##Id] = [S].[##SecondTableName##Id]
          , [T].[DateStart] = [S].[DateStart]
          , [T].[DateEnd] = [S].[DateEnd]
          , [T].[ChangerId] = @ChangerId
    WHEN NOT MATCHED BY SOURCE THEN
        UPDATE SET
            [T].[ChangerId] = @ChangerId
          , [T].[TimeEnd] = GETUTCDATE();

    SET NOCOUNT OFF;
END
GO
