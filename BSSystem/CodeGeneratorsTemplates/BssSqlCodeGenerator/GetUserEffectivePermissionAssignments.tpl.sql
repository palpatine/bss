CREATE FUNCTION [Common].[GetUserEffectivePermissionAssignments](
    @UserId INT)
RETURNS  TABLE
    WITH SCHEMABINDING
AS
RETURN
    WITH [UserContextPermissionCTE] AS (
--##GetUserContextPermissionsCTE()##
    )
    SELECT
        [ucpc].[PermissionId]
      , [ucpc].[AssignmentTableKey]
      , [ucpc].[RelatedTableKey]
    FROM [Common].[GetUserEffectivePermissions](@UserId) AS guep
    JOIN [UserContextPermissionCTE] AS [ucpc] ON [ucpc].[PermissionId] = guep.Id
GO