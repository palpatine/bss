UNION
        SELECT
            [AT].[UserId]
          , [prd].[PermissionId]
          , CAST('##TableSchemaName##_##TableKey##' AS VARCHAR(100)) AS [AssignmentTableKey]
          , CAST('##RelatedTableSchemaName##_##RelatedTableKey##' AS VARCHAR(100)) AS [RelatedTableKey]
        FROM [##TableSchemaName##].[##TableName##] AS [AT]
        JOIN [Common].[RoleDefinition] AS [rd] ON [rd].[Id] = [AT].[RoleDefinitionId]
            AND [rd].[IsDeleted] = 0
            AND [rd].[EntityStatusId] = 2
        JOIN [Common].[Permission_RoleDefinition] AS [prd] ON [prd].[RoleDefinitionId] = [rd].[Id]
            AND [prd].[IsDeleted] = 0
        WHERE [AT].[IsDeleted] = 0
          AND [AT].[UserId] = @UserId
          AND [AT].[EntityStatusId] = 2