CREATE PROCEDURE [##JunctionTableSchemaName##].[Update_##FirstTableName##_##SecondTableName##]
(
    @ScopeId INT,
    @ChangerId INT,
    @MasterColumnName VARCHAR(50),
    @MasterId INT,
    @NewList [Common].[JunctionList] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

##UpdateJunctionTableCommonValidation()##
    
    DECLARE @NotAllovedDateValue SMALLDATETIME = '2079-06-06'

    ;WITH
    TargetCTE AS (
        SELECT
            [T].[Id]
          , [T].[##FirstTableName##Id]
          , [T].[##SecondTableName##Id]
##IF JunctionTableIsVersioned##
          , [T].[ChangerId]
          , [T].[TimeEnd]
##END##
##IF JunctionTableIsScoped##
          , [T].[ScopeId]
##END##
        FROM [##JunctionTableSchemaName##].[##FirstTableName##_##SecondTableName##] [T]
        WHERE (  @MasterColumnName = '[##FirstTableName##Id]' AND [T].[##FirstTableName##Id] = @MasterId
            OR @MasterColumnName = '[##SecondTableName##Id]' AND [T].[##SecondTableName##Id] = @MasterId)
##IF JunctionTableIsVersioned##
            AND [T].[IsDeleted] = 0
##END##
    ),
    SourceCTE AS (
        SELECT
            [nl].[Id] AS [Id]
          , IIF(@MasterColumnName='[##FirstTableName##Id]', @MasterId, [nl].[ElementId]) AS [##FirstTableName##Id]
          , IIF(@MasterColumnName='[##SecondTableName##Id]', @MasterId, [nl].[ElementId]) AS [##SecondTableName##Id]
        FROM @NewList [nl]
    )
    MERGE TargetCTE AS [T]
    USING SourceCTE AS [S]
    ON [T].[Id] = [S].[Id]
    WHEN NOT MATCHED BY TARGET THEN
        INSERT ([##FirstTableName##Id], [##SecondTableName##Id]##IF JunctionTableIsVersioned##, [ChangerId]##END####IF JunctionTableIsScoped##, [ScopeId]##END##)
        VALUES ([S].[##FirstTableName##Id], [S].[##SecondTableName##Id]##IF JunctionTableIsVersioned##, @ChangerId##END####IF JunctionTableIsScoped##, @ScopeId##END##)
    WHEN MATCHED AND 
          (  [T].[##FirstTableName##Id] <> [S].[##FirstTableName##Id]
          OR [T].[##SecondTableName##Id] <> [S].[##SecondTableName##Id]) THEN
        UPDATE SET
            [T].[##FirstTableName##Id] = [S].[##FirstTableName##Id]
          , [T].[##SecondTableName##Id] = [S].[##SecondTableName##Id]
##IF JunctionTableIsVersioned##
          , [T].[ChangerId] = @ChangerId
##END##
    WHEN NOT MATCHED BY SOURCE THEN
##IF JunctionTableIsVersioned##
        UPDATE SET
            [T].[ChangerId] = @ChangerId
          , [T].[TimeEnd] = GETUTCDATE();
##ELSE##
        DELETE
##END##
    SET NOCOUNT OFF;
END
GO
