CREATE FUNCTION [Common].[GetUserEffectivePermissions](
    @UserId INT)
RETURNS  TABLE
    WITH SCHEMABINDING
AS
RETURN
    WITH
    [SystemPermissionCTE] AS (
        SELECT
            [sp].[Id]
          , [sp].[Path]
          , [mo].[OrganizationId]
        FROM [Common].[SystemPermission] AS [sp]
        JOIN [Common].[Module_Organization] AS [mo] ON [mo].[ModuleId] = [sp].[ModuleId]
                    AND [mo].[IsDeleted] = 0
                    AND [mo].[EntityStatusId] = 2
    ),
    [CustomPermissionCTE] AS (
        SELECT
            [cp].[Id]
          , [cp].[ScopeId]
        FROM [Common].[CustomPermission] AS [cp]
        WHERE [cp].[IsDeleted] = 0
    ),
    [UserGlobalPermissionCTE] AS (
        SELECT
            [pu].[UserId]
            , [pu].[PermissionId]
        FROM [Common].[Permission_User] AS [pu]
        WHERE [pu].[IsDeleted] = 0
          AND [pu].[UserId] = @UserId
    ),
    [UserContextPermissionCTE] AS (
--##GetUserContextPermissionsCTE()##
    )
    SELECT
        [p].[Id]
      , [spc].[Path]
      , [UserPermissionsCTE].[IsGlobal]
    FROM [Common].[User] AS [u]
    JOIN [Common].[Organization] AS [o] ON [o].[Id] = [u].[ScopeId]
                AND [o].[IsDeleted] = 0
                AND [o].[EntityStatusId] = 2
    JOIN [Common].[Permission] AS [p] ON [p].[ScopeId] = [u].[ScopeId]
                AND [p].[IsDeleted] = 0
    LEFT JOIN [SystemPermissionCTE] AS [spc] ON [spc].[Id] = [p].[SystemPermissionId]
                AND [spc].[OrganizationId] = [u].[ScopeId]
    LEFT JOIN [CustomPermissionCTE] AS [cpc] ON [cpc].[Id] = [p].[CustomPermissionId]
                AND [cpc].[ScopeId] = [u].[ScopeId]
    JOIN (
            SELECT
                ISNULL([ugpc].[UserId], [ucpc].[UserId]) AS [UserId]
              , ISNULL([ugpc].[PermissionId], [ucpc].[PermissionId]) AS [PermissionId]
              , CAST(IIF([ugpc].[UserId] IS NOT NULL, 1, 0) AS BIT) AS [IsGlobal]
            FROM [UserGlobalPermissionCTE] AS [ugpc]
            FULL OUTER JOIN [UserContextPermissionCTE] AS [ucpc] ON [ugpc].[UserId] = [ucpc].[UserId] AND [ugpc].[PermissionId] = [ucpc].[PermissionId]
        ) AS [UserPermissionsCTE] ON [UserPermissionsCTE].[UserId] = [u].[Id]
                AND [UserPermissionsCTE].[PermissionId] = [p].[Id]
    WHERE [u].[IsDeleted] = 0
      AND [u].[Id] = @UserId
      AND [u].[EntityStatusId] = 2
      AND (([spc].[Id] IS NULL AND [cpc].[Id] IS NOT NULL) OR ([spc].[Id] IS NOT NULL AND [cpc].[Id] IS NULL))
GO