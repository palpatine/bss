﻿CREATE FUNCTION [Common].[GetUserMenu](
    @UserId INT)
RETURNS  TABLE
    WITH SCHEMABINDING
AS
RETURN
WITH
[MenuPositions1] AS (
    SELECT
        0-[mi].[Id] AS [Id]
      , [mi].[Key]
      , [mi].[MenuSectionId] AS [ParentId]
      , [mi].[Order]
      , [mi].[IconName]
      , [mi].[PermissionPath]
      , CAST(NULL AS TINYINT) AS [NavigationPosition]
    FROM [Common].[MenuItem] AS [mi]
    LEFT JOIN [Common].[GetUserEffectivePermissions](@UserId) AS guep ON [guep].[Path] = [mi].[PermissionPath]
    WHERE [mi].[PermissionPath] IS NULL OR [guep].[Id] IS NOT NULL
),
[MenuPositions2] AS (
    SELECT DISTINCT
        [ms].[Id]
      , [ms].[Key]
      , [ms].[ParentId]
      , [ms].[Order]
      , [ms].[IconName]
      , CAST(NULL AS VARCHAR(900)) AS [PermissionPath]
      , [ms].[NavigationPosition]
    FROM [Common].[MenuSection] AS [ms]
    JOIN [MenuPositions1] AS [m] ON [m].[ParentId] = [ms].[Id]
),
[MenuPositions3] AS (
    SELECT DISTINCT
        [ms].[Id]
      , [ms].[Key]
      , [ms].[ParentId]
      , [ms].[Order]
      , [ms].[IconName]
      , CAST(NULL AS VARCHAR(900)) AS [PermissionPath]
      , [ms].[NavigationPosition]
    FROM [Common].[MenuSection] AS [ms]
    JOIN [MenuPositions2] AS [m] ON [m].[ParentId] = [ms].[Id]
)
SELECT
    [Id], [Key], [ParentId], [Order], [IconName], [PermissionPath], [NavigationPosition]
FROM [MenuPositions1]
UNION ALL
SELECT
    [Id], [Key], [ParentId], [Order], [IconName], [PermissionPath], [NavigationPosition]
FROM [MenuPositions2]
UNION ALL
SELECT
    [Id], [Key], [ParentId], [Order], [IconName], [PermissionPath], [NavigationPosition]
FROM [MenuPositions3]
GO