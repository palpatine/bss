CREATE TRIGGER [##SchemaName##].[INST_TRIUD_##TableName##] ON [##SchemaName##].[##TableName##]
WITH EXECUTE AS CALLER
INSTEAD OF INSERT, UPDATE, DELETE
AS
BEGIN
    RAISERROR('[##SchemaName##].[##TableName##]: Data cannot be modified.', 16, 1);
    ROLLBACK TRAN;
    RETURN;
END;