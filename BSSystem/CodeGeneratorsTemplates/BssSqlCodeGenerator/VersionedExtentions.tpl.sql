ALTER TABLE [##SchemaName##].[##TableName##]
ADD CONSTRAINT [FK_##TableName##_ChangerId] FOREIGN KEY ([ChangerId])
REFERENCES [Common].[User] ([Id])
ON UPDATE NO ACTION
ON DELETE NO ACTION
GO

CREATE TRIGGER [##SchemaName##].[INST_TRD_##TableName##] ON [##SchemaName##].[##TableName##]
    WITH EXECUTE AS CALLER
    INSTEAD OF DELETE
AS
BEGIN
    DECLARE @row_count INT = @@rowcount;
    IF(@row_count = 0)
    BEGIN
        RETURN
    END;
    RAISERROR('[##SchemaName##].[##TableName##]: Entries cannot be deleted.', 16, 1);
    ROLLBACK TRAN;
END;
GO

CREATE TRIGGER [##SchemaName##].[INST_TRI_##TableName##] ON [##SchemaName##].[##TableName##]
    WITH EXECUTE AS CALLER
    INSTEAD OF INSERT
AS
BEGIN
##IF IsJunction##
    DECLARE @row_count INT = ((SELECT COUNT(*)FROM INSERTED) + (SELECT COUNT(*) FROM DELETED));
    IF(@row_count = 0)
    BEGIN
        RETURN
    END;

    IF NOT EXISTS(SELECT
                      [jl].*
                  FROM [Common].[JunctionLock] AS [jl]
                  JOIN [Common].[Session] AS [s] ON [s].[Id] = [jl].[SessionId]
                                                AND [jl].[UserId] = [s].[UserId]
                                                AND [jl].[TimeStart] >= [s].[TimeStart]
                                                AND [s].[TimeEnd] >= GETUTCDATE()
                  JOIN INSERTED AS [i] ON (##InsertLockJoinClausure##)
                                        AND [jl].[UserId] = [i].[ChangerId]
                  WHERE [jl].[TableName] = '[##SchemaName##].[##TableName##]')
    BEGIN
        RAISERROR('[##SchemaName##].[##TableName##]: Not locked data cannot be inserted.', 16, 1);
        ROLLBACK TRAN;
        RETURN;
    END;
##END##
    INSERT INTO [##SchemaName##].[##TableName##](
        ##InsertColumnsTemplate##
      , [ChangerId]
      , [TimeEnd])
    (SELECT
         ##InsertColumnsTemplate##
       , [ChangerId]
       , [TimeEnd]
     FROM INSERTED);
END;
GO

CREATE TRIGGER [##SchemaName##].[INST_TRU_##TableName##] ON [##SchemaName##].[##TableName##]
    WITH EXECUTE AS CALLER
    INSTEAD OF UPDATE
AS
BEGIN
    DECLARE @row_count INT = ((SELECT COUNT(*)FROM INSERTED) + (SELECT COUNT(*) FROM DELETED));
    IF(@row_count = 0)
    BEGIN
        RETURN
    END;

    IF EXISTS(SELECT * FROM DELETED WHERE [TimeEnd] IS NOT NULL)
    BEGIN
        RAISERROR('[##SchemaName##].[##TableName##]: Archival data cannot be changed.', 16, 1);
        ROLLBACK TRAN;
        RETURN;
    END;

    IF(UPDATE([Number]))
    BEGIN
        RAISERROR('[##SchemaName##].[##TableName##]: Column [Number] cannot be changed.', 16, 1);
        ROLLBACK TRAN;
        RETURN;
    END;

    IF NOT EXISTS(SELECT
                      [l].*
                  FROM [Common].[Lock] AS [l]
                  JOIN [Common].[Session] AS [s] ON [s].[Id] = [l].[SessionId]
                                                AND [l].[UserId] = [s].[UserId]
                                                AND [l].[TimeStart] >= [s].[TimeStart]
                                                AND [s].[TimeEnd] >= GETUTCDATE()
                  JOIN INSERTED AS [i] ON [i].[Id] = [l].[RecordId]
                                        AND [l].[UserId] = [i].[ChangerId]
                  WHERE [l].[TableName] = '[##SchemaName##].[##TableName##]')
##IF HasForeignKeys##
    AND NOT EXISTS(SELECT
                       [jl].*
                   FROM [Common].[JunctionLock] AS [jl]
                   JOIN [Common].[Session] AS [s] ON [s].[Id] = [jl].[SessionId]
                                                 AND [jl].[UserId] = [s].[UserId]
                                                 AND [jl].[TimeStart] >= [s].[TimeStart]
                                                 AND [s].[TimeEnd] >= GETUTCDATE()
                   JOIN INSERTED AS [i] ON (##InsertLockJoinClausure##)
                                         AND [jl].[UserId] = [i].[ChangerId]
                   WHERE [jl].[TableName] = '[##SchemaName##].[##TableName##]')
##END##
    BEGIN
        RAISERROR('BSS001:[##SchemaName##].[##TableName##]: Not locked data cannot be changed or given data are incomplete.', 16, 1);
        ROLLBACK TRAN;
        RETURN;
    END;


    BEGIN --UPDATE
        UPDATE [##SchemaName##].[##TableName##] SET
              ##UpdateColumnsTemplate##
            , [Number] = [i].[Number]
            , [ChangerId] = [i].[ChangerId]
            , [TimeStart] = GETUTCDATE()
        FROM [##SchemaName##].[##TableName##] AS [TBL]
        INNER JOIN INSERTED AS [i] ON [i].[Id] = [TBL].[Id]
                            AND [i].[TimeEnd] IS NULL;

        INSERT INTO [##SchemaName##].[##TableName##](
              ##InsertColumnsTemplate##
            , [Number]
            , [ChangerId]
            , [TimeStart]
            , [TimeEnd])
        (SELECT
              ##InsertColumnsTemplate##
            , [Number]
            , [ChangerId]
            , [TimeStart]
            , GETUTCDATE() AS [TimeEnd]
            FROM DELETED);
    END

    BEGIN --DELETE
        UPDATE [##SchemaName##].[##TableName##] SET
            [ChangerId] = [i].[ChangerId]
          , [TimeEnd] = GETUTCDATE()
        FROM [##SchemaName##].[##TableName##] AS [TBL]
        INNER JOIN INSERTED AS [i] ON [i].[Id] = [TBL].[Id]
                            AND [i].[TimeEnd] IS NOT NULL;
    END
END;
GO