CREATE PROCEDURE [##JunctionTableSchemaName##].[Update_##FirstTableName##_##SecondTableName##]
(
    @ScopeId INT,
    @ChangerId INT,
    @MasterColumnName VARCHAR(50),
    @MasterId INT,
    @NewList [Common].[AssignmentJobRoleList] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

##UpdateJunctionTableCommonValidation()##
   
    DECLARE @NotAllovedNumberValue int = -1
          , @NotAllovedDateValue SMALLDATETIME = '2079-06-06'

    ;WITH
    TargetCTE AS (
        SELECT
            [T].[Id]
          , [T].[ScopeId]
          , [T].[##FirstTableName##Id]
          , [T].[##SecondTableName##Id]
          , [T].[RoleDefinitionId]
          , [T].[JobTime]
          , [T].[JobHours]
          , [T].[DateStart]
          , [T].[DateEnd]
          , [T].[ChangerId]
          , [T].[TimeEnd]
        FROM [##JunctionTableSchemaName##].[##FirstTableName##_##SecondTableName##] [T]
        WHERE [T].[IsDeleted] = 0
        AND (  @MasterColumnName = '[##FirstTableName##Id]' AND [T].[##FirstTableName##Id] = @MasterId
            OR @MasterColumnName = '[##SecondTableName##Id]' AND [T].[##SecondTableName##Id] = @MasterId)
    ),
    SourceCTE AS (
        SELECT
            [nl].[Id] AS [Id]
          , IIF(@MasterColumnName='[##FirstTableName##Id]', @MasterId, [nl].[ElementId]) AS [##FirstTableName##Id]
          , IIF(@MasterColumnName='[##SecondTableName##Id]', @MasterId, [nl].[ElementId]) AS [##SecondTableName##Id]
          , [nl].[RoleId] AS [RoleDefinitionId]
          , [nl].[JobTime] AS [JobTime]
          , [nl].[JobHours] AS [JobHours]
          , [nl].[DateStart] AS [DateStart]
          , [nl].[DateEnd] AS [DateEnd]
        FROM @NewList [nl]
    )
    MERGE TargetCTE AS [T]
    USING SourceCTE AS [S]
    ON [T].[Id] = [S].[Id]
    WHEN NOT MATCHED BY TARGET THEN
        INSERT ([##FirstTableName##Id], [##SecondTableName##Id], [RoleDefinitionId], [JobTime], [JobHours], [DateStart], [DateEnd], [ChangerId], [ScopeId])
        VALUES ([S].[##FirstTableName##Id], [S].[##SecondTableName##Id], [S].[RoleDefinitionId], [S].[JobTime], [S].[JobHours], [S].[DateStart], [S].[DateEnd], @ChangerId, @ScopeId)
    WHEN MATCHED AND 
          (  [T].[##FirstTableName##Id] <> [S].[##FirstTableName##Id]
          OR [T].[##SecondTableName##Id] <> [S].[##SecondTableName##Id]
          OR [T].[RoleDefinitionId] <> [S].[RoleDefinitionId]
          OR ISNULL([T].[JobTime], @NotAllovedNumberValue) <>  ISNULL([S].[JobTime], @NotAllovedNumberValue)
          OR ISNULL([T].[JobHours], @NotAllovedNumberValue) <> ISNULL([S].[JobHours], @NotAllovedNumberValue)
          OR [T].[DateStart] <> [S].[DateStart]
          OR ISNULL([T].[DateEnd], @NotAllovedDateValue) <> ISNULL([S].[DateEnd], @NotAllovedDateValue) ) THEN
        UPDATE SET
            [T].[##FirstTableName##Id] = [S].[##FirstTableName##Id]
          , [T].[##SecondTableName##Id] = [S].[##SecondTableName##Id]
          , [T].[RoleDefinitionId] = [S].[RoleDefinitionId]
          , [T].[JobTime] = [S].[JobTime]
          , [T].[JobHours] = [S].[JobHours]
          , [T].[DateStart] = [S].[DateStart]
          , [T].[DateEnd] = [S].[DateEnd]
          , [T].[ChangerId] = @ChangerId
    WHEN NOT MATCHED BY SOURCE THEN
        UPDATE SET
            [T].[ChangerId] = @ChangerId
          , [T].[TimeEnd] = GETUTCDATE();

    SET NOCOUNT OFF;
END
GO
