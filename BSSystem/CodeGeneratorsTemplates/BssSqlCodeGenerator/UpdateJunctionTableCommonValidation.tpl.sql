    IF @ScopeId IS NULL
    OR @ChangerId IS NULL
    OR @MasterColumnName IS NULL
    OR @MasterId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 1

    DECLARE @ValidationResult [Common].[ValidationResultList]
    DECLARE @MainTableName NVARCHAR(MAX) = '[##JunctionTableSchemaName##].[##FirstTableName##_##SecondTableName##]'
          , @MessageCode NVARCHAR(MAX)
    
	SET @MessageCode = 'TargetNotFound'
	IF @MasterColumnName = '[##FirstTableName##Id]' AND NOT EXISTS (
            SELECT *
            FROM [##FirstTableSchemaName##].[##FirstTableName##]
            WHERE [Id]=@MasterId
                ##IF FirstTableIsVersioned##AND [IsDeleted] = 0##END##
                ##IF FirstTableIsScoped##AND [ScopeId] = @ScopeId##END##)
    OR @MasterColumnName = '[##SecondTableName##Id]' AND NOT EXISTS (
            SELECT *
            FROM [##SecondTableSchemaName##].[##SecondTableName##]
            WHERE [Id]=@MasterId
                ##IF SecondTableIsVersioned##AND [IsDeleted] = 0##END##
                ##IF SecondTableIsScoped##AND [ScopeId] = @ScopeId##END##)
    BEGIN
        SELECT
            CAST(NULL AS INT) AS [Id]
          , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
          , @MessageCode AS [MessageCode]
          , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
        RETURN
    END

    SET @MessageCode = 'NoLockError'
    IF NOT EXISTS (
        SELECT *
        FROM [Common].[JunctionLock] [jl]
        WHERE [jl].[TableName] = @MainTableName
          AND [jl].[ColumnName] = @MasterColumnName
          AND [jl].[ColumnValue] = @MasterId
          AND [jl].[UserId] = @ChangerId
    )
    BEGIN
        SELECT
            CAST(NULL AS INT) AS [Id]
          , CAST(NULL AS NVARCHAR(MAX)) AS [Field]
          , @MessageCode AS [MessageCode]
          , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
        RETURN
    END
    
	SET @MessageCode = 'ReferenceError'
    INSERT @ValidationResult
    SELECT
        [Elements].[Id]
      , 'Element' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM (
        SELECT [nl].[Id]
        FROM @NewList AS [nl]
        LEFT JOIN [##FirstTableSchemaName##].[##FirstTableName##] [t] ON [t].[Id] = [nl].[ElementId]
            ##IF FirstTableIsVersioned##AND [t].[IsDeleted] = 0##END##
            ##IF FirstTableIsScoped##AND [t].[ScopeId] = @ScopeId##END##
        WHERE @MasterColumnName <> '[##FirstTableName##Id]' AND [t].[Id] IS NULL
        UNION ALL
        SELECT [nl].[Id]
        FROM @NewList AS [nl]
        LEFT JOIN [##SecondTableSchemaName##].[##SecondTableName##] [t] ON [t].[Id] = [nl].[ElementId]
            ##IF SecondTableIsVersioned## AND [t].[IsDeleted] = 0 ##END##
            ##IF SecondTableIsScoped## AND [t].[ScopeId] = @ScopeId ##END##
        WHERE @MasterColumnName <> '[##SecondTableName##Id]' AND [t].[Id] IS NULL
    ) AS [Elements](Id)
    ##IF JunctionTableIsRoleAssignment##
    UNION ALL
    SELECT
        [nl].[Id]
      , 'Role' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM @NewList AS [nl]
    LEFT JOIN [Common].[RoleDefinition] AS [rd] ON [rd].[Id] = [nl].[RoleId]
                                                AND [rd].[IsDeleted] = 0
                                                AND [rd].[ScopeId] = @ScopeId
                                                AND [rd].[TableKey] = '##RoleRelatedTableKey##' 
    WHERE [rd].[Id] IS NULL
    ##END##

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END 
    
##IF JunctionTableIsAssignment##
    SET @MessageCode = 'DateRangeError'
    SELECT
        [nl].[Id]
      , 'DateEnd' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST(NULL AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM @NewList [nl]
    WHERE [nl].[DateEnd] IS NOT NULL AND [nl].[DateStart] > [nl].[DateEnd]

    DECLARE @MinDateValue SMALLDATETIME = '1900-01-01'

    -- 'DateStartError' / 'DateEndError'
    INSERT @ValidationResult
    SELECT
        [Q].[Id] AS [Id]
      , [ca].[Field] AS [Field]
      , [ca].[Field]+'Error' AS [MessageCode]
      , CONVERT(NVARCHAR(MAX), [ca].[Date], 126) AS [AdditionalValue]
    FROM (
        SELECT
            [nl].[Id]
          , [nl].[DateStart]
          , [nl].[DateEnd]

          , (SELECT MAX([DateSatrt])
            FROM ( VALUES
              (NULL)
              ##IF SecondTableIsDated##, ([mst].[DateStart]) ##END##
              ##IF FirstTableIsDated##, ([mft].[DateStart]) ##END##
              ##IF SecondTableIsDated##, ([est].[DateStart]) ##END##
              ##IF FirstTableIsDated##, ([eft].[DateStart]) ##END##
##IF JunctionTableIsRoleAssignment##
              , ([rd].[DateStart])
##END##
            ) MaxDate([DateSatrt])) AS [MinimumPossibleDate]

          , (SELECT MIN([DateEnd])
            FROM ( VALUES
              (NULL)
              ##IF SecondTableIsDated##, ([mst].[DateEnd]) ##END##
              ##IF FirstTableIsDated##, ([mft].[DateEnd]) ##END##
              ##IF SecondTableIsDated##, ([est].[DateEnd]) ##END##
              ##IF FirstTableIsDated##, ([eft].[DateEnd]) ##END##
##IF JunctionTableIsRoleAssignment##
              , ([rd].[DateEnd])
##END##
            ) MinDate([DateEnd])) AS [MaximumPossibleDate]
        FROM @NewList [nl]
        LEFT JOIN [##FirstTableSchemaName##].[##FirstTableName##] [mft] ON @MasterColumnName = '[##FirstTableName##Id]' AND [mft].[Id] = @MasterId
        LEFT JOIN [##FirstTableSchemaName##].[##FirstTableName##] [eft] ON @MasterColumnName <> '[##FirstTableName##Id]' AND [eft].[Id] = [nl].[ElementId]
        LEFT JOIN [##SecondTableSchemaName##].[##SecondTableName##] [mst] ON @MasterColumnName = '[##SecondTableName##Id]' AND [mst].[Id] = @MasterId
        LEFT JOIN [##SecondTableSchemaName##].[##SecondTableName##] [est] ON @MasterColumnName <> '[##SecondTableName##Id]' AND [est].[Id] = [nl].[ElementId]
##IF JunctionTableIsRoleAssignment##
        JOIN [Common].[RoleDefinition] [rd] ON [rd].[Id] = [nl].[RoleId]
##END##
    ) AS Q
    CROSS APPLY (
        VALUES ('DateStart', [MinimumPossibleDate])
             , ('DateEnd',   [MaximumPossibleDate])
    ) AS [ca]([Field], [Date])
    WHERE [ca].[Field] = 'DateStart' AND [Q].[DateStart] < [ca].[Date]
        OR [ca].[Field] = 'DateEnd' AND (
            [Q].[DateEnd] IS NULL AND [ca].[Date] IS NOT NULL
            OR [Q].[DateEnd] > COALESCE([ca].[Date], [Q].[DateEnd])
        )

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END  
    
    SET @MessageCode = 'DateOverlapError';
    INSERT @ValidationResult
    SELECT
        [nl1].[Id] AS [Id]
      , 'Element' AS [Field]
      , @MessageCode AS [MessageCode]
      , CAST([nl2].[Id] AS NVARCHAR(MAX)) AS [AdditionalValue]
    FROM @NewList AS [nl1]
    LEFT JOIN @NewList AS [nl2] ON @MasterColumnName = '[UserId]'
                            AND [nl1].[Id] <> [nl2].[Id]
                            AND [nl1].[ElementId] = [nl2].[ElementId]
##IF JunctionTableIsRoleAssignment##
                            AND [nl1].[RoleId] = [nl2].[RoleId]
##END##
    WHERE [nl1].[DateStart] <= ISNULL([nl2].[DateEnd], '2079-06-06')
      AND ISNULL([nl1].[DateEnd], '2079-06-06') >= [nl2].[DateStart];

    IF EXISTS(SELECT * FROM @ValidationResult)
    BEGIN
        SELECT [Id], [Field], [MessageCode], [AdditionalValue] FROM @ValidationResult
        RETURN
    END
##END##