ALTER TABLE [##SchemaName##].[##TableName##]
ADD CONSTRAINT [FK_##TableName##_ScopeId] FOREIGN KEY ([ScopeId])
REFERENCES [Common].[Organization] ([Id])
ON UPDATE NO ACTION
ON DELETE NO ACTION
GO
