IF COL_LENGTH('[##SchemaName##].[##TableName##]','IsDeleted') IS NOT NULL
BEGIN
    ALTER TABLE [##SchemaName##].[##TableName##]
    DROP COLUMN [IsDeleted]
END
ALTER TABLE [##SchemaName##].[##TableName##]
ADD [IsDeleted] AS (CONVERT([bit], CASE WHEN [TimeEnd] IS NULL THEN (0) ELSE (1) END)) PERSISTED NOT NULL
GO