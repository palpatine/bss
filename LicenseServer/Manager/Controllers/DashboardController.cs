﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Qdarc.LicenseServer.Manager.Security;

namespace Qdarc.LicenseServer.Manager.Controllers
{
    public class DashboardController : Controller
    {
        [RequireAuthorization]
        public ActionResult Index()
        {
            return View();
        }
	}
}