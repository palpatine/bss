﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Qdarc.LicenseServer.Logic.DataAccess;
using Qdarc.LicenseServer.Manager.Security;

namespace Qdarc.LicenseServer.Manager.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var context = new DatabaseContext();
            var clients = context.Clients.ToArray();
            context.Dispose();
            return View();
        }


        [RequireAuthorization]
        [ApiAuthorizationFilter]
        public ActionResult Edit(int id)
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}