﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Qdarc.LicenseServer.Manager.Startup))]
namespace Qdarc.LicenseServer.Manager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
