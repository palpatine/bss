﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Qdarc.LicenseServer.Manager.Properties;
using Qdarc.Modules.Common.Utils;
using System.Web.Mvc.Html;
using System.Globalization;
using System.Web.Routing;

namespace Qdarc.LicenseServer.Manager
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString LocalizedLabelFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.Label(LocalizedFieldDescription(propertyExpression), htmlAttributes);
        }

        public static MvcHtmlString LocalizedLabelFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression,
            object htmlAttributes)
        {
            return htmlHelper.Label(LocalizedFieldDescription(propertyExpression), htmlAttributes);
        }

        public static MvcHtmlString LocalizedLabelFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            return htmlHelper.Label(LocalizedFieldDescription(propertyExpression));
        }

        private static string LocalizedFieldDescription<TModel, TValue>(Expression<Func<TModel, TValue>> propertyExpression)
        {
            return LocalizedValueFor(propertyExpression) + ":";
        }

        public static string LocalizedDisplayNameFor<TModel, TValue>(this HtmlHelper<IEnumerable<TModel>> hepler, Expression<Func<TModel, TValue>> propertyExpression)
        {
            return LocalizedValueFor(propertyExpression);
        }

        public static string LocalizedValueFor<TModel, TValue>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            return LocalizedValueFor(propertyExpression);
        }
        
        public static string LocalizedValueFor<TModel>(
                   this HtmlHelper<TModel> htmlHelper,
                   string key)
        {
            return LocalizedValueFor(key);
        }

        public static string LocalizedValueFor(
            string key)
        {
            return Resources.ResourceManager.GetString(key, CultureInfo.CurrentUICulture);
        }

        public static string LocalizedValueFor<TModel, TValue>(
            Expression<Func<TModel, TValue>> propertyExpression)
        {
            var path = propertyExpression.GetProperties();
            var property = path.Last();
            var prefixedKey = string.Format("{0}_{1}", property.DeclaringType.Name, property.Name);
            var prefixedValue = LocalizedValueFor(prefixedKey);

            if (string.IsNullOrWhiteSpace(prefixedValue))
            {
                var value = LocalizedValueFor(property.Name);
                if (string.IsNullOrWhiteSpace(value))
                {
                    return property.Name;
                }

                return value;
            }

            return prefixedValue;
        }

        public static MvcHtmlString LocalizedActionLink(
            this HtmlHelper htmlHelper,
            string actionName,
            string controllerName)
        {
            return LocalizedActionLink(htmlHelper, actionName, controllerName, new RouteValueDictionary(), new RouteValueDictionary());
        }

        public static MvcHtmlString LocalizedActionLink(this HtmlHelper htmlHelper, string actionName, string controllerName, RouteValueDictionary routeValues, object htmlAttributes)
        {
            return LocalizedActionLink(htmlHelper, actionName, controllerName, routeValues, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString LocalizedActionLink(this HtmlHelper htmlHelper, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            var linkText = LocalizedValueFor(string.Format("{0}_{1}", controllerName, actionName));
            return MvcHtmlString.Create(HtmlHelper.GenerateLink(htmlHelper.ViewContext.RequestContext, htmlHelper.RouteCollection, linkText, null, actionName, controllerName, routeValues, htmlAttributes));
        }
    }
}