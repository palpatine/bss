﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Microsoft.Practices.ServiceLocation;
using Qdarc.LicenseServer.Logic.Services;

namespace Qdarc.LicenseServer.Manager.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class ApiAuthorizationFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var model = filterContext.ActionParameters["model"];

            //string param1 = (string)actionContext.ActionArguments["param1"];
            //int param2 = (int)actionContext.ActionArguments["param2"];

            //if (model.Id != "1")
            //    throw new HttpResponseException(System.Net.HttpStatusCode.Forbidden);

            base.OnActionExecuting(filterContext);
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class RequireAuthorizationAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Provides an entry point for custom authorization checks. Gets authentication ticket from
        /// cookie that name depends from server port.
        /// </summary>
        /// <param name="httpContext">The HTTP context, which encapsulates all HTTP-specific information about an individual HTTP request.</param>
        /// <returns>true if the user is authorized; otherwise, false.</returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authCookie = httpContext.Request.Cookies.Get("sessionId");
            if (authCookie == null)
            {
                return false;
            }

            if (authCookie.Value == null)
            {
                return false;
            }
            var sessionId = authCookie.Value;
            using (var authenticationService = ServiceLocator.Current.GetInstance<IAuthenticationService>())
            {
               var session =  authenticationService.VerifySession(sessionId);
               return session != null;
            }
        }       
    }

    class UserInformation :IPrincipal
    {

        public UserInformation()
        {

        }

        public IIdentity Identity
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}