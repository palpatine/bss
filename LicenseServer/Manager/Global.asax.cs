﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Qdarc.LicenseServer.Manager.Controllers;
using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc.Unity;

namespace Qdarc.LicenseServer.Manager
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = new UnityContainer();
            var registrar = new UnityRegistrar(container);
            var binFolder = ConfigurationManager.AppSettings["ModulesLocation"];
            var moduleDLLs = Directory.GetFiles(binFolder)
                 .Where(x => Path.GetExtension(x) == ".dll" && Path.GetFileNameWithoutExtension(x)
                     .StartsWith("Qdarc"))
                     .Select(x => Assembly.LoadFrom(x))
                     .ToArray();

            var bootstrapper = new SystemBootstrapper(registrar, moduleDLLs);


            bootstrapper.Bootstrap();
            var locator = new UnityServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => locator);

            var services = GlobalConfiguration.Configuration.Services;
            var controllerTypes = services.GetHttpControllerTypeResolver()
                .GetControllerTypes(services.GetAssembliesResolver());

            foreach (var controllerType in controllerTypes)
            {
                container.RegisterType(controllerType);
            }

            ControllerBuilder.Current.SetControllerFactory(new ControllerFactory(container));
        }
    }
}
