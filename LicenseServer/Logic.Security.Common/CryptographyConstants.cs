﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.Security.Common
{
    public class CryptographyConstants
    {
        public static readonly byte[] Salt = GetPublicKey();

        private static byte[] GetPublicKey()
        {
            var assembly = typeof(CryptographyConstants).Assembly;
            var key = assembly.GetName().GetPublicKey();
            return key;
        }
    }
}
