﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Qdarc.LicenseServer.Logic.Security.Common
{
    public interface IDecoder
    {
        Stream Decode(Stream value, string sharedSecret, string salt);
    }
}
