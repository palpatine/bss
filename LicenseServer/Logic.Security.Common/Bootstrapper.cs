﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc;

namespace Qdarc.LicenseServer.Logic.Security.Common
{
    public class Bootstrapper : IBootstrapper
    {
        public void Bootstrap(IRegistrar registrar)
        {
            registrar.RegisterSingle<IDecoder, Decoder>();
        }

        public double Order
        {
            get { return 0; }
        }
    }
}
