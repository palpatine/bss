﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qdarc.LicenseServer.Logic.DataAccess;
using Qdarc.LicenseServer.Logic.Services;
using Qdarc.Modules.Common.Bootstrap;
using Qdarc.Modules.Common.Ioc;

namespace Qdarc.LicenseServer.Logic
{
    public class Bootstrapper : IBootstrapper
    {
        public void Bootstrap(IRegistrar registrar)
        {
            registrar.RegisterSingle<IDatabaseContext, DatabaseContext>();
            registrar.RegisterSingle<IAuthenticationService, AuthenticationService>();
        }

        public double Order
        {
            get { return 0; }
        }
    }
}
