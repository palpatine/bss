﻿using System;
using System.Threading.Tasks;
using Qdarc.LicenseServer.Logic.Services.Models;

namespace Qdarc.LicenseServer.Logic.Services
{
    public interface IAuthenticationService : IDisposable
    {
        Task<Session> AuthenticateAsync(string userName, string password);

        Task LogOffAsync(Guid sessionId);

        Session VerifySession(string sessionId);
    }
}
