﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.Services.Models
{
    public class Session
    {
        public Guid SessionId { get; set; }

        public int UserId { get; set; }
    }
}
