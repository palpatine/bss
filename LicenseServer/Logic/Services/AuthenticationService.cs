﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.LicenseServer.Logic.DataAccess;
using Qdarc.LicenseServer.Logic.Security;
using Qdarc.LicenseServer.Logic.Services.Models;
using Qdarc.Modules.Common.Utils;
using System.Data.Entity;
using Qdarc.LicenseServer.Logic.DataAccess.Entities;
using Qdarc.LicenseServer.Logic.Security.Common;
using System.IO;

namespace Qdarc.LicenseServer.Logic.Services
{
    class AuthenticationService : IAuthenticationService
    {
        internal static string EncriptionKey = "bułeczka";

        private readonly IDatabaseContext _context;
        private readonly IDecoder _decoder;

        public AuthenticationService(IDatabaseContext context, IDecoder decoder)
        {
            _context = context;
            _decoder = decoder;
        }

        public async Task<Session> AuthenticateAsync(string userName, string password)
        {
            var user = await _context.Users.SingleOrDefaultAsync(x => x.Login == userName);

            if (user == null)
            {
                return null;
            }

            var decriptedPassword = new StreamReader(
                _decoder.Decode(new MemoryStream(Convert.FromBase64String(user.Password)),
                EncriptionKey, user.Salt)).ReadToEnd();

            if (password == decriptedPassword)
            {
                var sessison = new SessionEntity
                {
                    Identifier = Guid.NewGuid(),
                    User = user,
                    ExpirationDate = DateTime.Now.AddMinutes(15)
                };

                _context.Sessions.RemoveRange(user.Sessions);
                _context.Sessions.Add(sessison);

                await _context.SaveAsync();

                return new Session { SessionId = sessison.Identifier, UserId = user.Id };
            }

            return null;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public Task LogOffAsync(Guid sessionId)
        {
            return Task.FromResult<object>(null);
        }


        public Session VerifySession(string sessionId)
        {
            Guid guid;
            if (!Guid.TryParse(sessionId, out guid))
            {
                return null;
            }

            var session = _context.Sessions.FirstOrDefault(x => x.Identifier == guid);

            if (session == null || session.ExpirationDate < DateTime.Now)
            {
                return null;
            }

            session.ExpirationDate = DateTime.Now.AddMinutes(15);
            _context.Save();

            return new Session
            {
                SessionId = session.Identifier,
                UserId = session.User.Id,
            };
        }
    }
}
