﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.DataAccess.Entities
{
   public class TemplateItemEntity
    {
        public int Id { get; set; }

        public string FeatureName { get; set; }
    }
}
