﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.DataAccess.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }

        public string Salt { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public virtual ICollection<SessionEntity> Sessions { get; set; }

        public virtual ICollection<RightEntity> Rights { get; set; }
    }
}
