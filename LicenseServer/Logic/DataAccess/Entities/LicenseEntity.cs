﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.DataAccess.Entities
{
   public class LicenseEntity
    {

        public int Id { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public TemplateEntity Template { get; set; }

        public virtual ICollection<LicenseItemEntity> Items { get; set; }

        public virtual ClientEntity Client { get; set; }
    }
}
