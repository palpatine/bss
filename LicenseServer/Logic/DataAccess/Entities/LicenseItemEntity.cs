﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.DataAccess.Entities
{
   public class LicenseItemEntity
    {
        public int Id { get; set; }

        public TemplateItemEntity TemplateItem { get; set; }

        public bool IsAvailable { get; set; }
    }
}
