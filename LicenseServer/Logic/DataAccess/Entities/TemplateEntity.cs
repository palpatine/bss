﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.DataAccess.Entities
{
public    class TemplateEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<LicenseEntity> Licenses { get; set; }

        public virtual ICollection<TemplateItemEntity> Items { get; set; }
    }
}
