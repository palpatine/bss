﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.DataAccess.Entities
{
    public class SessionEntity
    {
        public int Id { get; set; }

        public Guid Identifier { get; set; }

        public DateTime ExpirationDate { get; set; }

        public virtual UserEntity User { get; set; }
    }
}
