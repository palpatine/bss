﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdarc.LicenseServer.Logic.DataAccess.Entities
{
    public class ClientEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string Address { get; set; }

        public string TaxID { get; set; }

        public virtual ICollection<LicenseEntity> Licenses { get; set; }
    }
}
