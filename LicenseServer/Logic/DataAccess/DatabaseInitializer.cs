﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.LicenseServer.Logic.DataAccess.Entities;
using Qdarc.LicenseServer.Logic.Security;
using Qdarc.LicenseServer.Logic.Services;
using Qdarc.Modules.Common.Utils;

namespace Qdarc.LicenseServer.Logic.DataAccess
{
#if DEBUG

    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            var salt = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes("sdfgcvioerkj"));
            var user = new UserEntity
            {
                Login = "admin",
                Salt = salt,
                Password = new Encoder().Encode("koteczek".ToStream(), AuthenticationService.EncriptionKey, salt)
            };

            context.Users.Add(user);

            context.SaveChanges();
        }
    }

#endif
}
