﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Qdarc.LicenseServer.Logic.DataAccess.Entities;
namespace Qdarc.LicenseServer.Logic.DataAccess
{
    interface IDatabaseContext : IDisposable
    {
        DbSet<ClientEntity> Clients { get; set; }
        DbSet<LicenseEntity> Licenses { get; set; }
        DbSet<TemplateEntity> Templates { get; set; }
        DbSet<UserEntity> Users { get; set; }

        DbSet<SessionEntity> Sessions { get; set; }

        Task SaveAsync();

        void Save();
    }
}
