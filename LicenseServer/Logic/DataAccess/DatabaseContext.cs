﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.LicenseServer.Logic.DataAccess.Entities;

namespace Qdarc.LicenseServer.Logic.DataAccess
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DatabaseContext()
            : base("LicenseServer")
        {
        }

        public DbSet<ClientEntity> Clients { get; set; }

        public DbSet<LicenseEntity> Licenses { get; set; }

        public DbSet<SessionEntity> Sessions { get; set; }

        public DbSet<TemplateEntity> Templates { get; set; }

        public DbSet<UserEntity> Users { get; set; }

        public void Save()
        {
            this.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await this.SaveChangesAsync();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>()
                .HasMany(x => x.Rights);

            modelBuilder.Entity<SessionEntity>()
                .HasRequired(x => x.User)
                .WithMany(x => x.Sessions);

            modelBuilder.Entity<ClientEntity>()
                .HasMany(x => x.Licenses)
                .WithRequired(x => x.Client);

            modelBuilder.Entity<LicenseEntity>()
                .HasMany(x => x.Items);

            modelBuilder.Entity<TemplateEntity>()
                .HasMany(x => x.Licenses)
                .WithRequired(x => x.Template);

            modelBuilder.Entity<TemplateEntity>()
                .HasMany(x => x.Items);

            modelBuilder.Entity<LicenseItemEntity>()
                .HasRequired(x => x.TemplateItem);
        }
    }
}